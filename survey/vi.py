from telephony.statemachine import BaseVappController
from telephony.utils import *
from survey.models import *
from survey.tasks import PushResultsTask, AssessmentResultsTask, PostSuveyCallTask, SendAudioToMlTask, SendAudioToQnaTask
from survey.stats import get_answer_label
from app_manager.models import get_report_creds, push_results
from vapp.utils import *
from mnews.models import News_state

from datetime import datetime
import json
import os
from log import get_logger
logger = get_logger()

THEME_PREFIX = 'theme_'


class SurveyController( BaseVappController ):

    @classmethod
    def generateResult( cls, surveyCdr, viData, totalQuesAnswered, quesCount ):
        cdr = surveyCdr.cdr
        result = {}
        result[ 'cdr' ] = {
            'id': str( cdr.id ),
            'start_time': str( cdr.start_time ),
            'callerid': cdr.callerid
        }
        result[ 'surveyId' ] = str( surveyCdr.survey.id )
        result[ 'conversationId' ] = str( viData.get( 'conversation_id' ) )
        result[ 'completed' ] = totalQuesAnswered == quesCount
        result[ 'records' ] = {}

        for record in Record.objects.filter( survey = surveyCdr.survey, cdr = surveyCdr.cdr ):
            surveyData = {}
            surveyData[ 'questionTime' ] = str( record.time )
            surveyData[ 'order' ] = str( record.order )
            surveyData[ 'questionText' ] = str( record.question.text )
            surveyData[ 'answer' ] = str( get_answer_label( record ) )
            result[ 'records' ][ str( record.id ) ] = surveyData
        return result

    def __init__(self, ai, sessionData, vi_data):
        super(SurveyController, self).__init__(ai, sessionData, vi_data)
        logger.info('Getting survey initialization data')
        self.curQuestion = None
        self.curAnswer = None
        self.next_question_order = None
        self.next_action = {}

        self.item_id = None
        self.sessionData = sessionData
        self.survey = Survey.objects.get( ai = self.ai, is_active = True )
        self.form = self.survey.form
        # self.formSurveyConditionalFlow = Survey_conditional_flow.objects.filter(form_id=self.form.id, question_order = -1)[0] if Survey_conditional_flow.objects.filter(form_id=self.form.id, question_order = -1).exists() else None
        self.surveyConditions = Survey_conditional_flow.objects.filter(form_id=self.form.id) if Survey_conditional_flow.objects.filter(form_id=self.form.id).exists() else None
        self.prompt_set = self.survey.form.prompt_set
        self.questions = [fq.question for fq in Form_question.objects.filter(form = self.form)]
        self.ques_count = len(self.questions)
        self.questions.sort(key = lambda x: x.order)
        logger.info('QUESTIONS ::: '+str(self.questions))
        self.cdr = self.sessionData.cdrs[0]
        self.tries = 0
        self.fillertries = 0
        self.max_tries = 2
        self.max_fillertries = 3

        self.reporting_credential = get_report_creds(self.ai)
        self.survey_response_entities = {}
        self.cred = None
        self.choices_text = ''
        if self.reporting_credential.exists():
            self.cred = self.reporting_credential[0]
        logger.info('Got survey initialization data')
        if vi_data.get( 'conversation_id' ):
            from helpline.models import Call, Conversation_feedback_log
            conversation = Call.objects.get( pk = vi_data.get( 'conversation_id' ) )
            cfl = Conversation_feedback_log( conversation = conversation, feedback_cdr = self.cdr )
            cfl.save()

    def isLastQuestion(self, questions, cur):
        return questions.index(cur) == (len(questions) - 1)

    def getNextStateEvent(self, next_state):
        if next_state == 'thanks':
            return 'survey_thank_you'

    def getQuestionTypeEvent(self, question):
        if isinstance(question, Question_mc):
            return 'survey_mcq_question'
        if isinstance(question, Question_qu):
            return 'survey_quantitative_question'
        if isinstance(question, Question_ff):
            return 'survey_freeform_question'
        if isinstance(question, Question_df):
            return 'survey_df_question'
        if isinstance(question, Question_qna):
            return 'survey_qna_question'

    def remove_prefix(self, text, prefix):
        if text.startswith(prefix):
            return text[len(prefix):]
        return text       

    def recordError(self):
        error = Record_error(survey = self.survey, form = self.form, content_type = self.questionType, 
                             question_id = self.curQuestion.id, order = self.curQuestion.order, cdr = self.sessionData.cdrs[0])
        error.save()


    def clearCurAnswer(self):
        if isinstance(self.curQuestion, Question_mc):
            self.curAnswer.mc_choice = None
        elif isinstance(self.curQuestion, Question_qu):
            self.curAnswer.qu_value = None
        elif isinstance(self.curQuestion, Question_ff):
            recording = self.curAnswer.ff_audio
            recording.delete()
            self.curAnswer.ff_audio = None
        elif isinstance(self.curQuestion, Question_df):
            recording = self.curAnswer.ff_audio
            recording.delete()
            self.curAnswer.ff_audio = None
            self.curAnswer.cf = None
            self.curAnswer.cf_criteria = ''
            self.curAnswer.entity_json = ''
        elif isinstance(self.curQuestion, Question_qna):
            recording = self.curAnswer.ff_audio
            recording.delete()
            self.curAnswer.ff_audio = None
	    self.curAnswer.cf = None
            self.curAnswer.cf_criteria = ''
            self.curAnswer.entity_json = ''	

        self.curAnswer.save()


    def removeCurAnswer(self):
        if isinstance(self.curQuestion, Question_ff):
            recording = self.curAnswer.ff_audio
            recording.delete()
            self.curAnswer.ff_audio = None
        elif isinstance(self.curQuestion, Question_df):
            recording = self.curAnswer.ff_audio
            recording.delete()
            self.curAnswer.ff_audio = None
        elif isinstance(self.curQuestion, Question_qna):
            recording = self.curAnswer.ff_audio
            recording.delete()
            self.curAnswer.ff_audio = None

        self.curAnswer.delete()

    def pushResults(self):
        if self.cred:
            results = self.generateResult(self.survey_cdr, self.vi_data, self.survey_response_summary.total_ques_answered, self.ques_count)
            push_results(self.cred, results, self.cdr)

    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid )

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def while_checksurveyenabled__sm_action_success(self, events, eventData):
        if self.survey.is_enabled:
            return 'survey_enabled'
        else:
            logger.error('Survey ' + str(self.survey) + ' is disabled. ai=' + str(self.ai) + '. callerid=' + str(self.sessionData.callerid))
            return 'survey_disabled'

    def pre_welcome(self):
        self.cdr = self.sessionData.cdrs[0]
        self.survey_cdr, created = Survey_cdr.objects.get_or_create( survey = self.survey, cdr = self.cdr )
        self.survey_response_summary = Survey_response_summary( survey_cdr = self.survey_cdr )
        self.survey_response_summary.save()
        self.next_question_order = 0
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_welcome' ) )

    def while_cfevaluate__sm_action_success(self, events, eventData):
        self.result_criteria_dict = {}
        if self.surveyConditions and self.surveyConditions.filter(question_order = self.curQuestion.order).exists():
            #Record is unique to cdr and question_order
            #self.curAnswer = Record.objects.get(cdr = self.sessionData.cdrs[0], order = self.curQuestion.order)
            conditional_flow = self.surveyConditions.filter(question_order = self.curQuestion.order)[0]
            self.curAnswer = Record.objects.get(pk = self.curAnswer.id)
            result_criteria_json = Survey_conditional_flow.getMatchingCriteria(self.curAnswer, conditional_flow.condition)
                
            self.result_criteria_dict = json.loads(result_criteria_json)
            self.curAnswer.cf_criteria = result_criteria_json
            self.curAnswer.save()

            if self.result_criteria_dict.get('loop_playback') and self.tries < self.max_tries:
                self.item_id = self.result_criteria_dict.get('loop_playback')
                return 'survey_cfplayback'

            if self.result_criteria_dict.get('playback'):
                self.item_id = self.result_criteria_dict.get('playback')                            
                return 'survey_cfplayback'
        
        return 'survey_cfjump'

    #TODO: item listenership check, dtmf options(skip mainly press 1)
    def pre_cfplayback(self):       
        self.curAnswer.playback_item = News.objects.get(pk = self.item_id) # take new object of self.curAnswer
        self.curAnswer.save()        
        return self.getSurveyAudioPlaybackParams(self.item_id)

    def while_cfjump__sm_action_success(self, events, eventData):
        if self.result_criteria_dict: # will not happen for no df type questions/no criterian matched/no default
            if self.result_criteria_dict.get('loop'):
                self.tries += 1
                if self.tries <= self.max_tries:
                    self.clearCurAnswer()
                    return self.getQuestionTypeEvent(self.curQuestion) # play question
            if self.result_criteria_dict.get('exit'):
                return 'survey_thank_you'
            if self.result_criteria_dict.get('next_ai'):
                #in case of qna type question
                if isinstance(self.curQuestion, Question_qna):
                    items_list = []
                    entity_json_dict = json.loads(self.curAnswer.entity_json)
                    entity = entity_json_dict.get('entity')[0]
                    items_list = entity.values()[0]
                    
                    ai_news_dict = {self.result_criteria_dict.get('next_ai') : items_list}
                    self.sessionData.app_data['virtual_mnews'].update(ai_news_dict)
                    #push the state to stack 'startquestion'
                    self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'startquestion') )
                    self.next_question_order = self.questions.index(self.curQuestion) + 1
                return '@ai_' + str(self.result_criteria_dict.get('next_ai')) + '_welcome'
            if self.result_criteria_dict.get('next_qu'):
                self.next_question_order = self.result_criteria_dict.get('next_qu') - 1
                return 'survey_start_question'       
        elif self.ques_count == 0 or (self.curQuestion and self.curQuestion.is_last) or (self.curQuestion and self.isLastQuestion(self.questions, self.curQuestion)):
            if self.surveyConditions and self.surveyConditions.filter(question_order = -1).exists():
                return 'survey_formlevel_jump'
            return 'survey_no_formlevel_question'

        self.next_question_order = self.questions.index(self.curQuestion) + 1

    def while_flevaluate__sm_action_success(self, events, eventData):
        self.item_id =  Survey_conditional_flow.checkConditionForAction(self.survey_response_entities,  'playback', self.form.id)
        if self.item_id:
            return 'survey_formlevel_audioplayback'

    def pre_flplayback(self):       
        self.survey_cdr.audio_playback_item = News.objects.get(pk = self.item_id)
        self.survey_cdr.save()
        return self.getSurveyAudioPlaybackParams(self.item_id)

    def while_fljump__sm_action_success(self, events, eventData):
        ai_id = Survey_conditional_flow.checkConditionForAction(self.survey_response_entities, 'next_ai', self.form.id)
        if ai_id:
            return '@ai_' + str(ai_id) + '_welcome'

    def while_startquestion__sm_action_success(self, events, eventData):
        #out of bound check
        if self.next_question_order < 0 or self.next_question_order >= len(self.questions):
            return 'survey_thank_you'
        self.tries = 0
        self.curQuestion = self.questions[self.next_question_order]   
        self.questionType = ContentType.objects.get_for_model(self.curQuestion.__class__)
        self.curAnswer = Record(survey = self.survey, form = self.form, content_type = self.questionType,
            question_id = self.curQuestion.id, order = self.curQuestion.order, cdr = self.sessionData.cdrs[0])
        self.curAnswer.save()

    def while_checkpreprompt__sm_action_success(self, events, eventData):
        if self.curQuestion.pre_prompt_audio and os.path.exists(self.getPromptParams(promptAudio = self.curQuestion.pre_prompt_audio)['filename']):
            return 'sm_action_success'
        return 'survey_no_preprompt'

    def pre_prepromptplay(self):
        return self.getPromptParams(promptAudio = self.curQuestion.pre_prompt_audio)

    def pre_ffquestion(self):
        return self.getPromptParams(promptAudio = self.curQuestion.prompt_audio)

    def pre_ffplayrecordinstr(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_record' ) )

    def pre_ffrecord(self):
        return self.getEmbeddedRecordingParams( self.curQuestion.max_duration, self.curAnswer, Record.ff_audio )

    def pre_dfquestion(self):
        return self.getPromptParams(promptAudio = self.curQuestion.prompt_audio)

    def pre_dfplayrecordinstr(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_record' ) )

    def pre_dfrecord(self):
        logger.info('IN df_record PRE')
        logger.info('cdr ===='+str(self.sessionData.cdrs[0].id))
        return self.getEmbeddedRecordingParams( self.curQuestion.max_duration, self.curAnswer, Record.ff_audio, extension="wav", silenceThreshold=600 , silenceHits=3)

    def while_dfrecord__fs_record_start(self, events, eventData):
        if self.surveyConditions and self.surveyConditions.filter(question_order = self.curQuestion.order).exists():
            conditional_flow = self.surveyConditions.filter(question_order = self.curQuestion.order)[0]
            self.curAnswer.cf = conditional_flow
            self.curAnswer.save()
            SendAudioToMlTask.apply_async([self.curAnswer.ff_audio.get_full_filename(), conditional_flow, self.sessionData.cdrs[0]], eta=datetime.now(), queue = 'df_query')

    def while_dfrecord__sm_action_success(self, events, eventData):
        endfile, ext = os.path.splitext(self.curAnswer.ff_audio.get_full_filename())
        endfile = endfile + '.end'
        with open(endfile, 'w') as fp: 
            pass

    def pre_fillerprompt(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_fillerprompt' ) )

    def while_fillerprompt__sm_action_success(self, events, eventData):
        self.fillertries += 1
        survey_record = Record.objects.filter(cdr = self.sessionData.cdrs[0], order = self.curQuestion.order).order_by('-id')
        if not survey_record[0].entity_json and self.fillertries < self.max_fillertries:
            return 'survey_repeat_filler'
        else:
            self.fillertries = 0
            return 'survey_next_question'

    def pre_qnaquestion(self):
        return self.getPromptParams(promptAudio = self.curQuestion.prompt_audio)        

    def pre_qnarecord(self):
        self.newNews = None
        title = 'Survey %d-%s, Question %d, %s' % (self.survey.id, self.survey.name, self.curQuestion.id, datetime.now().strftime('%H:%M, %d %B %Y'))
        if self.survey.misc_properties:
            misc_properties = json.loads(self.survey.misc_properties)
            self.save_to_channel =  Channel.objects.get(pk = misc_properties.get('save_to_channel'))
            self.newNews = News(ai_id=self.save_to_channel.ai.id, channel=self.save_to_channel,
                                    callerid=self.sessionData.callerid,
                                    state=News_state.UNM, is_comment=False,
                                    source=News.Source.VOICE_INTERFACE, title=title, creation_cdr=self.sessionData.cdrs[0])
            self.newNews.save()

        return self.getEmbeddedRecordingParams( self.curQuestion.max_duration, self.curAnswer, Record.ff_audio, extension="wav", silenceThreshold=600 , silenceHits=3)

    def while_qnarecord__fs_record_start(self, events, eventData):
        if self.surveyConditions and self.surveyConditions.filter(question_order = self.curQuestion.order).exists():
            conditional_flow = self.surveyConditions.filter(question_order = self.curQuestion.order)[0]
            self.curAnswer.cf = conditional_flow
            self.curAnswer.save()
            recording = self.createRecording(self.newNews, News.detail, True, self.save_to_channel.ai.id)
            SendAudioToQnaTask.apply_async([self.curAnswer.id, self.curAnswer.ff_audio.get_full_filename(), recording, self.sessionData.cdrs[0], self.sessionData.app_data['conversational_flow']['theme_selected'], self.survey, self.newNews.id], eta=datetime.now(), queue = 'df_query')

    def while_qnarecord__sm_action_success(self, events, eventData):
        endfile, ext = os.path.splitext(self.curAnswer.ff_audio.get_full_filename())
        endfile = endfile + '.end'
        with open(endfile, 'w') as fp: 
            pass

    def while_checkvalidate__sm_action_success(self, events, eventData):
        self.survey_response_summary.total_ques_answered += 1
        self.survey_response_summary.save()
        if self.curQuestion.validate:
            return 'survey_validation_required'
        else:
            #Survey_ff_response_channel_map_log.copy_ff_response_to_channels(self.curAnswer, True)
            return 'survey_no_validation_required'
        
    def pre_validateannounce(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_validate_announce' ) )

    def while_playbackanswer__sm_action_success(self, events, eventData):
        return self.getQuestionTypeEvent(self.curQuestion)

    def pre_playffanswer(self):
        return self.getPlaybackParams(self.curAnswer.ff_audio.get_full_filename())

    def pre_playdfanswer(self):
        return self.getPlaybackParams(self.curAnswer.ff_audio.get_full_filename())

    def pre_validatechoice(self):
        return self.getPromptPlayAndGetParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_validate_choice' ), tries = 3, maxDigits=1)

    def while_validatechoice__sm_get_digits_no_digits(self, events, eventData):
        self.recordError()
        self.removeCurAnswer()
        return 'survey_next_question'

    def while_validatechoice__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if digits == 2:
            self.recordError()
            self.clearCurAnswer()
            return 'survey_repeat_question'
        elif digits == 1:
            Survey_ff_response_channel_map_log.copy_ff_response_to_channels(self.curAnswer, True)
            return 'survey_next_question'
        else:
            return 'survey_repeat_validatechoice'

    def while_resettriesrepeatquestion__sm_action_success(self, events, eventData):
        self.tries = 0
        return 'sm_action_success'

    def while_incrementandchecktries__sm_action_success(self, events, eventData):
        self.tries += 1
        if self.tries <= self.max_tries:
            return self.getQuestionTypeEvent(self.curQuestion)
        else:
            return 'survey_next_question'

    def pre_ququestion(self):
        return self.getPromptPlayAndGetParams(promptAudio = self.curQuestion.prompt_audio, maxDigits = self.curQuestion.max_digits)

    def while_ququestion__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        logger.info("qu answer:" + str(digits))

        if digits is not None:
            self.curAnswer.qu_value = digits
            self.curAnswer.entity_json = json.dumps({"success": True, "entity": [{"dtmf": digits}]})
            self.curAnswer.save()
            return 'sm_action_success'
        else:
            return 'survey_invalid_digits'

    def pre_mcquestion(self):
        return self.getPromptPlayAndGetParams(promptAudio = self.curQuestion.prompt_audio)

    def while_mcquestion__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info("choice:" + str(digit))
        self.survey_response_entities[self.curQuestion.order] = {'dtmf':str(digit)}
        if digit and digit in range(1, self.curQuestion.num_choices + 1):
            self.curAnswer.mc_choice = digit
            self.curAnswer.entity_json = json.dumps({"success": True, "entity": [{"dtmf": digit}]})
            self.curAnswer.save()
            choices_dict = dict(enumerate([choice.strip() for choice in self.curQuestion.choices.split(',')]))
            if choices_dict.get(digit-1).startswith(THEME_PREFIX):
                self.sessionData.app_data['conversational_flow']['theme_selected'] = self.remove_prefix(choices_dict.get(digit-1), THEME_PREFIX)
            return 'sm_action_success'
        elif digit:
            self.invalid_choice = digit
            return 'survey_invalid_choice'
        else:
            return 'survey_invalid_digits'

    def while_mcquestion__sm_get_digits_no_digits(self, events, eventData):
        self.sessionData.app_data['conversational_flow']['theme_selected'] = None
        return 'survey_no_choice'	

    def pre_announceinvalidchoice(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_validate_announce' ) )

    def pre_playinvalidchoice(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_digit_' + str(self.invalid_choice) ) )

    def pre_choiceunavailable(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_choice_unavailable' ) )

    def pre_playmcanswer(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_digit_' + str(self.curAnswer.mc_choice) ) )

    def while_playquanswer__sm_action_success(self, events, eventData):
        self.qu_value_index = 0
        return 'sm_action_success'

    def pre_iteratequanswer(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_digit_' + str(self.curAnswer.qu_value)[self.qu_value_index]) )

    def while_iteratequanswer__sm_action_success(self, events, eventData):
        self.qu_value_index += 1
        if self.qu_value_index == len(str(self.curAnswer.qu_value)):
            return 'sm_action_success'
        else:
            return 'survey_iterate'



    def pre_thankyou(self):
        return self.getPromptParams( promptAudio = self.prompt_set.get_prompt_audio( 'survey_thank_you' ) )



    def while_thankyou__sm_action_success(self, events, eventData):
        logger.info('inside whilte thank you')
        logger.info(str(self.sessionData.embed_stack))
        if len(self.sessionData.embed_stack) == 0:
            logger.info('returning next ai')
            return '@nextai'

    def while_appexit__sm_action_success(self, events, eventsData):
        transition_string = self.sessionData.popEmbedStack()
        logger.info(str(transition_string))
        if transition_string:
            if self.survey.need_assessment:
                AssessmentResultsTask.delay( self.survey_cdr, self.ques_count )

            self.pushResults()
            return transition_string

    def post_stop(self, events):
        if Report_creds.objects.filter(ai = self.ai).exists():
            PushResultsTask.delay(self.ai.id, self.cdr.id)
        if self.survey.need_assessment:
            AssessmentResultsTask.delay( self.survey_cdr, self.ques_count )
        if Survey_feedback_call_config.objects.filter(survey = self.survey):
            PostSuveyCallTask.delay(Survey_feedback_call_config.objects.get(survey = self.survey))


        self.pushResults()

        
SurveyStateDescriptionMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checksurveyenabled':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checksurveyenabled':['sm_action_success'] 
            } 
        },
    {   'name':'checksurveyenabled',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'appexit':['survey_disabled'],
            'welcome':['survey_enabled'],
            },
        },
    {   'name':'welcome', 
        'action':'playback', 
        'transitions': {
            'stop':['sm_action_failure'],
            'startquestion':['sm_action_success'],
            },
        'dtmf': {
           '*':'@prevai',
           0:'@nextai',
           },
        },
   {   'name':'startquestion',
        'action':'none',
        'transitions': {
            'checkpreprompt':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'checkpreprompt',
        'action':'none',
        'transitions': {
            'prepromptplay':['sm_action_success'],
            'incrementandchecktries':['survey_no_preprompt'],
            },
        },    
    {   'name':'prepromptplay',
        'action':'playback',
        'transitions': {
            'incrementandchecktries':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'incrementandchecktries',
        'action':'none',
        'transitions': {
            'ffquestion':['survey_freeform_question'],
            'ququestion':['survey_quantitative_question'],
            'mcquestion':['survey_mcq_question'],
            'dfquestion':['survey_df_question'],
            'qnaquestion': ['survey_qna_question'],
            'cfevaluate':['survey_next_question'],
            'stop':['sm_action_failure'],
            },
        },       
    {   'name':'dfquestion',
        'action':'playback',
        'transitions': {
            'dfrecord':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {
        'name':'dfplayrecordinstr',
        'action':'playback',
        'transitions': {
            'dfrecord':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'dfrecord',
        'action':'record',
        'transitions': {
            'fillerprompt':['sm_action_success'],
            'stop':['sm_action_failure'],
            }
        },     
    {   'name': 'fillerprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'cfevaluate': ['sm_action_success','survey_next_question'],
            'fillerprompt':['survey_repeat_filler'],
            },
        },
     {   'name':'qnaquestion',
        'action':'playback',
        'transitions': {
            'qnarecord':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'qnarecord',
        'action':'record',
        'transitions': {
            'fillerprompt':['sm_action_success'],
            'stop':['sm_action_failure'],
            }
        },
    {   'name':'ffquestion',
        'action':'playback',
        'transitions': {
            'ffplayrecordinstr':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {
        'name':'ffplayrecordinstr',
        'action':'playback',
        'transitions': {
            'ffrecord':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'ffrecord',
        'action':'record',
        'transitions': {
            'checkvalidate':['sm_action_success'],
            'stop':['sm_action_failure'],
            }
        },
    {   'name': 'checkvalidate',
        'action': 'none',
        'transitions': {
            'validateannounce': ['survey_validation_required'],
            'cfevaluate':['survey_no_validation_required'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name': 'validateannounce',
        'action': 'playback',
        'transitions': {
            'playbackanswer': ['sm_action_success'],
            'stop': ['sm_action_failure'],
            },
        },
    {   'name': 'playbackanswer',
        'action': 'none',
        'transitions': {
            'playffanswer': ['survey_freeform_question'],
            'playquanswer': ['survey_quantitative_question'],
            'playmcanswer': ['survey_mcq_question'],
            'playdfanswer': ['survey_df_question'],
            'playqnaanswer': ['survey_qna_question'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name': 'playdfanswer',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'cfevaluate': ['sm_action_success'],
            },
        },    
    {   'name': 'playffanswer',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'validatechoice': ['sm_action_success'],
            },
        },
    {   'name': 'playmcanswer',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'validatechoice': ['sm_action_success'],
            },
        },
    {   'name': 'playquanswer',
        'action': 'none',
        'transitions': {
            'iteratequanswer': ['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name': 'iteratequanswer',
        'action': 'playback',
        'transitions': {
            'iteratequanswer': ['survey_iterate'],
            'validatechoice': ['sm_action_success'],
            'stop': ['sm_action_failure'],
            },
        },
    {   'name': 'validatechoice',
        'action':'play_and_get_digits',
        'transitions': {
            'resettriesrepeatquestion': ['survey_repeat_question'],
            'cfevaluate': ['survey_next_question'],
            'validatechoice':['survey_repeat_validatechoice'],
            'stop': ['sm_action_failure'],
            },
        'dtmf': {
            1: 'survey_next_question',
            },
        },
    {   'name': 'resettriesrepeatquestion',
        'action':'none',
        'transitions': {
            'incrementandchecktries': ['sm_action_success'],
            'stop': ['sm_action_failure'],
            },
        },   
    {   'name': 'ququestion',
        'action': 'play_and_get_digits',
        'transitions': {
            'checkvalidate': ['sm_action_success'],
            'incrementandchecktries': ['sm_get_digits_no_digits', 'survey_invalid_digits'],
            'stop': ['sm_action_failure'],
            },
        },  
    {   'name': 'mcquestion',
        'action':'play_and_get_digits',
        'transitions': {
            'checkvalidate': ['sm_action_success'],
            'announceinvalidchoice': ['survey_invalid_choice'],
            'incrementandchecktries': ['survey_invalid_digits', 'sm_get_digits_no_digits', 'survey_no_choice'],
            'stop': ['sm_action_failure'],
            },
        },
    {   'name':'announceinvalidchoice',
        'action':'playback',
        'transitions': {
            'playinvalidchoice': ['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'playinvalidchoice',
        'action':'playback',
        'transitions': {
            'choiceunavailable': ['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'choiceunavailable',
        'action':'playback',
        'transitions': {
            'incrementandchecktries': ['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'cfevaluate',
        'action':'none',
        'transitions': {
            'cfjump':['survey_cfjump'],
            'cfplayback':['survey_cfplayback'],
            'stop':'sm_action_failure'
            },
        },  
    {   'name':'cfplayback',
        'action':'playback',
        'transitions': {
            'cfjump':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },      
    {   'name':'cfjump',
        'action':'none',
        'transitions': {
            'startquestion':['sm_action_success', 'survey_start_question'],
            'flevaluate':['survey_formlevel_jump'],
            'ffquestion':['survey_freeform_question'],
            'ququestion':['survey_quantitative_question'],
            'mcquestion':['survey_mcq_question'],
            'dfquestion':['survey_df_question'],
            'qnaquestion':['survey_qna_question'],
            'thankyou':['survey_thank_you','survey_no_formlevel_question'],
            },
        },
    {   'name':'flevaluate',
          'action':'none',
         'transitions': {
            'flplayback':['survey_formlevel_audioplayback'],
            'fljump':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'flplayback',
        'action':'playback',
        'transitions': {
            'fljump':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },    
    {   'name':'fljump',
          'action':'none',
         'transitions': {
            'thankyou':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },            
    {   'name':'thankyou',
        'action':'playback',
        'transitions': {
           'stop':['sm_action_failure'],
           'appexit':['sm_action_success'],
           },
        'dtmf': {
           '*':'@prevai',
           0:'@nextai',
           },
        },

    {   'name':'appexit',
        'action':'none',
        'transitions' :{         
            'stop': ['sm_action_failure', 'sm_action_success'],           
            },
        },      
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
        },
    ]
            
