from models import Survey
from media.models import Prompt_audio

STANDARD_PROMPTS = ['survey_welcome', 'survey_thank_you', 'survey_validate_announce', 'survey_validate_choice', \
 'survey_record', 'survey_digit_0', 'survey_digit_1', 'survey_digit_2', 'survey_digit_3', 'survey_digit_4', \
 'survey_digit_5', 'survey_digit_6', 'survey_digit_7', 'survey_digit_8', 'survey_digit_9', 'survey_choice_unavailable', 'survey_fillerprompt' ]



def get_standard_prompts( survey_id ):
    survey = Survey.objects.get( id = survey_id )
    standard_prompt_audios = Prompt_audio.objects.filter( prompt_set = survey.form.prompt_set, info__name__in = STANDARD_PROMPTS )
    return standard_prompt_audios

def get_prompt_info_names():
    return STANDARD_PROMPTS
