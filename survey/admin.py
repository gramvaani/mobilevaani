from django.contrib import admin
from survey.models import *

admin.site.register(Form)
admin.site.register(Form_entity)
admin.site.register(Survey)
admin.site.register(Survey_custom_data_mapping)
admin.site.register(Report_creds)
admin.site.register(Survey_ff_response_to_mnews_channel_properties)
admin.site.register(Mcq_choice_weightage)
admin.site.register(Benchmark)
admin.site.register(Benchmark_choice)
admin.site.register(Assessment_ai_unit_map)
admin.site.register(Survey_feedback_call_config)
admin.site.register(Survey_conditional_flow)
admin.site.register(Df_properties)
