import sys, math

from log import get_request_logger
logger = get_request_logger()

from survey.models import Form, Form_question, Survey, Record, SurveyFields, \
Question_mc, Question_qu, Question_ff, Survey_cdr, Assessment, Assessment_status
from vapp.app_manager.models import Cdr, Transition_event, App_instance
from vapp.stats.models import Stats_type, Stats

from vapp.stats import stats as stats_stats
from vapp.app_manager import perms as app_manager_perms
from vapp.utils import is_start_of_week, is_start_of_month, all_same, send_email, \
get_total_seconds, get_ratio, generate_attachment, mp3_duration, LINE_BREAK

from vapp.app_selector.models import Choices
from vapp.callerinfo.utils import get_caller_profile_for_ai

from django.template.loader import render_to_string
from django.db.models import Q, Count
from django.contrib.contenttypes.models import ContentType
from django.db import models, connection
try:
    from ordereddict import OrderedDict
except ImportError:
    from collections import OrderedDict

from datetime import *
from vapp.local_settings import MSHAKTI_PROJECT_ID


def get_survey_ids(ai_ids):
    return Survey.objects.filter(ai__id__in = ai_ids, is_enabled = 1, is_active = True ).values_list('id', flat = True)


def check_ques_count(ques_count_list):
    if not all_same(ques_count_list):
        logger.info("Question_count_list contains different number of questions for survey_ids for given ai_ids.")
        return False
    return True


def check_ques_type(ques_type_list):
    if not all_same(ques_type_list):
        logger.info("Question type list contains different question types for survey_ids for given ai_ids.")
        return False
    return True


# Check required for surveys conducted in multiple languages
def check_quescount_questype_for_survey(survey_ids):
    ques_count_list = []
    ques_type_list = []
    questions = []

    for survey_id in survey_ids:
        questions = get_questions(survey_id)

        ques_type = []
        for question in questions:
            ques_type.append(type(question))
        ques_type_list.append(ques_type)
        ques_count_list.append(len(questions))

    if (not check_ques_count(ques_count_list)) or (not check_ques_type(ques_type_list)):
        return False

    return True


def get_ques_answered(survey_id, calls):
    records = {}
    for cdr in calls:
        record = Record.objects.filter(survey__id = survey_id, cdr__id = cdr.id).exclude(Q(ff_audio_id__isnull = True) & Q(qu_value__isnull = True) & Q(mc_choice__isnull = True))
        if record.exists():
            records[cdr.id] = record
    return records


def avg_ques_answered(survey_id, calls):
    total_answered =  sum([answers.count() for answers in get_ques_answered(survey_id, calls).values()])
    total_calls = calls.count()
    ques_count = len(get_questions(survey_id))
    avg_answered = '%.2f' % (total_answered / float(ques_count * total_calls) * 100)
    return avg_answered


def email_stats(ai_perms_dict, recipient_email):
    today = datetime.combine(datetime.today(), datetime.min.time())
    yesterday = today - timedelta(days = 1)
    ais = ai_perms_dict.keys()
    recipient_email = [recipient_email]

    generate_and_email_stats(ais, yesterday, today, recipient_email, Stats_type.DAILY)

    if is_start_of_week(today):
        week_start = today - timedelta(days = 7)
        generate_and_email_stats(ais, week_start, yesterday, recipient_email, Stats_type.WEEKLY)

def get_questions(survey_id):
    form = Form.objects.get(survey = survey_id)
    questions = [fq.question for fq in Form_question.objects.filter(form__survey = survey_id)]
    questions.sort(key = lambda x: x.order)
    return questions


def get_answer_label(record, labels = None):
    label = "-"
    question = record.question
    if isinstance(question, Question_mc):
        if not labels:
            labels = get_mc_labels(get_questions(record.survey_id))
        try:
            label = labels[question.order][int(record.mc_choice) - 1]
        except:
            pass
    elif isinstance(question, Question_qu) and record.qu_value:
        label = record.qu_value
    elif isinstance(question, Question_ff) and record.ff_audio:
        label = record.ff_audio.get_url()

    return label

def get_mc_labels(questions):
    labels = {}
    for question in questions:
        if isinstance(question, Question_mc):
            labels[question.order] = dict(enumerate(question.choices.split(',')))
    return labels

def get_report_header(survey, custom_data_fields=[], profile_fields=[]):
    header = ['Survey Date', 'Phone number', 'No of Questions Answered', 'Survey Cdr']
    header += get_default_report_header(survey, custom_data_fields, profile_fields)
    return header

def get_default_report_header(survey, custom_data_fields=[], profile_fields=[]):
    questions = get_questions(survey.id)
    header = []
    if survey.need_assessment:
        header.append('Correct Answers')

    for field in custom_data_fields:
        header.append(field.name.title())

    for each in profile_fields:
        header.append(each)

    for question in questions:
        header.append(str(question.text))
        if isinstance(question, Question_ff):
            header.extend(['Response Transcription', 'Response Duration'])

    return header

def get_cdrs_from_survey_cdrs( survey_id, start = None, end = None, callerids = None ):
    survey_cdrs = Survey_cdr.objects.filter( survey__id = survey_id )
    if start and end:
        survey_cdrs = survey_cdrs.filter( cdr__start_time__range = (start, end) )
    if callerids:
        survey_cdrs = survey_cdrs.filter( cdr__callerid__in = callerids )
    return ( survey_cdr.cdr for survey_cdr in survey_cdrs )


def get_advert_leads_stats(ai_id, start, end):
    return get_detailed_stats(ai_id, start, end)


def get_start_date_from_survey_cdr( survey ):
    records = Survey_cdr.objects.filter( survey = survey ).order_by('cdr__start_time')
    if records.exists():
        return records[0].cdr.start_time

    return None


def get_detailed_stats(ai_id, start=None, end=None, source_ai_ids=None, \
                       min_answers=None, one_row_per_callerid=False, \
                       callerid_result_selection_policy='latest', survey_id=None, \
                       wb_format=True, add_header=True, cdrs=None, \
                       add_custom_data=True):
    if survey_id:
        survey = Survey.objects.get(id=survey_id)
    else:
        survey = Survey.objects.get(ai_id=ai_id, is_active=True)

    custom_data_fields = []
    if add_custom_data and survey.custom_data:
        custom_data_fields = [field for field in survey.custom_data.model_class()._meta.fields if not field.name=='id']

    # This if-else block has been written to get purely customized data for mShakti, should be removed.
    ai = App_instance.objects.get(id=ai_id)
    project_ids = ai.project_ais.all().values_list('id', flat=True)
    if MSHAKTI_PROJECT_ID in project_ids:
        from vapp.customization.stats import get_caller_profile_for_ai_mshakti
        profile_data = get_caller_profile_for_ai_mshakti()
    else:
        profile_data = get_caller_profile_for_ai(ai_id)

    questions = get_questions(survey.id)
    labels = get_mc_labels(questions)

    if add_header:
        header = get_report_header(survey, custom_data_fields, profile_data['fields'])
    else:
        header = None

    if cdrs is None:
        cdrs = get_cdrs_for_survey(ai_id, one_row_per_callerid, \
                    callerid_result_selection_policy, source_ai_ids, \
                    start, end, survey)
    data = get_results_data(survey, questions, custom_data_fields, cdrs, \
                header, labels, min_answers, wb_format, profile_data)
    return data


def has_answered_survey(survey_id, cdr):
    if Survey_cdr.objects.filter(survey__id = survey_id, cdr = cdr).exists():
        return True
    return False


def get_cdrs_for_survey( ai_id, one_row_per_callerid = None, callerid_result_selection_policy = None, source_ai_ids = None, start = None, end = None, survey = None ):

    if survey is None:
        survey = Survey.objects.get( ai_id = ai_id, is_active = True )

    if source_ai_ids:
        cdrs = list( stats_stats.get_cdrs_from_transitions(source_ai_ids, ai_id, start, end) )
    else:
        start = start or get_start_date_from_survey_cdr( survey )
        if start is None:
            logger.error("Start date not found for survey instance with ai_id: %s" % ai_id)
            return []
        end = end or datetime.now()
        cdrs = get_cdrs_from_survey_cdrs(survey.id, start, end)

    if one_row_per_callerid == "true":
        if callerid_result_selection_policy is None:
            callerid_result_selection_policy = 'latest'

        caller_response_map = {}
        for cdr in cdrs:
            caller_response_map.setdefault( cdr.callerid, [] ).append( cdr )
        cdrs = filter_responses( survey, caller_response_map, callerid_result_selection_policy )

    return cdrs


def get_results_data(survey, questions, custom_data_fields, cdrs, header, \
                     labels, min_answers, wb_format=True, profile_data={}):
    data = [header] if header else []

    profile_field_count = len(profile_data.get('fields', []))
    profile_data_values = profile_data.get('values', {})

    for cdr in cdrs:
        records = Record.objects.filter(survey=survey, cdr=cdr).order_by('order')
        survey_cdrs = Survey_cdr.objects.filter(survey=survey, cdr=cdr)
        ans_ques_count = get_answered_ques_count(survey_cdrs[0]) if survey_cdrs.exists() else 0
        if ans_ques_count < min_answers:
            continue

        profile_info = profile_data_values.get(cdr.callerid, ['-']*profile_field_count)

        row = create_result_row(survey, questions, custom_data_fields, cdr, \
                    records, labels, ans_ques_count, wb_format, profile_info)
        data.append(row)
    return data


def filter_responses( survey, caller_response_map, callerid_result_selection_policy ):
    if callerid_result_selection_policy == 'earliest':
        cdrs = [ responses[ 0 ] for callerid, responses in caller_response_map.items() ]
    elif callerid_result_selection_policy in [ 'latest', 'merge' ]:
        cdrs = [ responses[ -1 ] for callerid, responses in caller_response_map.items() ]
    elif callerid_result_selection_policy == 'most_question':
        cdrs = [ get_cdr_with_max_ques_answered( responses, survey ) for callerid, responses in caller_response_map.items() ]

    return cdrs


def get_cdr_with_max_ques_answered(cdrs, survey):
    max_qs_ans = 0
    target_cdr = None
    for cdr in cdrs:
        survey_cdrs = Survey_cdr.objects.filter( survey = survey, cdr = cdr )
        ans_ques_count = get_answered_ques_count(survey_cdrs[0]) if survey_cdrs.exists() else 0
        if ans_ques_count > max_qs_ans:
            max_qs_ans = ans_ques_count
            target_cdr = cdr

    return target_cdr


def get_answered_records(survey_cdr):
    records = Record.objects.filter(survey = survey_cdr.survey, cdr = survey_cdr.cdr).exclude(mc_choice = 0)
    records = records.exclude(mc_choice__isnull = True, qu_value__isnull = True, ff_audio__isnull = True)
    return records


def get_answered_ques_count(survey_cdr, callerid_result_selection_policy = None, response_row = None):
    if callerid_result_selection_policy == 'merge' and response_row:
        ans_ques_count = 0
        for resp in response_row:
            if resp != '-':
                ans_ques_count += 1

        return ans_ques_count

    return get_answered_records(survey_cdr).count()


def create_result_row(survey, questions, custom_data_fields, cdr, records, \
                      labels, ans_ques_count, wb_format=True, profile_info=[]):
    if wb_format:
        row = [str(cdr.answered_time if cdr.answered_time else cdr.start_time)]
        row.extend([cdr.callerid, ans_ques_count, cdr.id])
        row.extend(get_records_row(survey, cdr, custom_data_fields, \
                        records, labels, questions, profile_info))
        return row
    else:
        return {cdr : list(records)}


def get_records_row(survey, cdr, custom_data_fields, records, labels, \
                    questions, profile_info=[]):
    record_row = []

    if survey.need_assessment:
        record_row.append(Assessment.objects.filter(record__cdr=cdr,
                                status=Assessment_status.CR).count())

    record_row.extend(get_custom_data(survey, custom_data_fields, cdr, records, labels))
    record_row.extend(profile_info)

    for question in questions:
        recs = records.filter(question_id=question.id)
        record_present = recs.exists()
        record_row.append(str(get_answer_label(recs[0], labels)) if record_present else '-')

        if isinstance(question, Question_ff):
            record_row.append(recs[0].ff_text if record_present and recs[0].ff_text else '-')
            if record_present and recs[0].ff_audio:
                record_row.append(\
                    mp3_duration(recs[0].ff_audio.get_full_filename()) / 1000.0)
            else:
                record_row.append('-')

    return record_row

def get_survey_value(field, cdr, records, labels = None):
    if field == SurveyFields.CALLERID:
        return cdr.callerid
    elif field == SurveyFields.Q1:
        answers = records.filter(order = 1)
        if answers:
            return get_answer_label(answers[0], labels)
    elif field == SurveyFields.Q2:
        answers = records.filter(order = 2)
        if answers:
            return get_answer_label(answers[0], labels)
    elif field == SurveyFields.Q3:
        answers = records.filter(order = 3)
        if answers:
            return get_answer_label(answers[0], labels)
    elif field == SurveyFields.Q4:
        answers = records.filter(order = 4)
        if answers:
            return get_answer_label(answers[0], labels)
    elif field == SurveyFields.Q5:
        answers = records.filter(order = 5)
        if answers:
            return get_answer_label(answers[0], labels)


def get_custom_data(survey, custom_data_fields, cdr, records, labels):
    if survey.custom_data:
        custom_data = survey.custom_data.model_class().objects.all()

        for mapping in survey.custom_data_mappings.all():
            survey_value = get_survey_value(mapping.survey_field, cdr, records, labels)
            custom_data = custom_data.filter(**{mapping.data_field: survey_value})

        if custom_data.exists():
            return [getattr(custom_data[0], field.name) for field in custom_data_fields]

    return ['-']*len(custom_data_fields)


def get_callerscount_no_ques_answered( cdrs ):
    callers = set()
    for cdr in cdrs:
        records = Record.objects.filter(cdr = cdr)
        if all( ( record.mc_choice is None) and ( record.qu_value is None ) and ( record.ff_audio is None ) for record in records ):
            callers.add(cdr.callerid)
    return len(callers)


def get_summary_stats( ai_id, cdrs = None, survey_id = None ):
    if not survey_id:
        survey_id = Survey.objects.get(ai_id = ai_id, is_active = True).id

    if cdrs is None:
        cdrs = set([ sc.cdr for sc in Survey_cdr.objects.filter(survey__id = survey_id) ])        
    
    records = Record.objects.filter(cdr__in = list(cdrs))
    total_calls = len(list(cdrs))
    record_ids = tuple( map(int, records.values_list('id', flat = True)) )
    callers_with_no_ques_ans = get_callerscount_no_ques_answered( cdrs )

    data = [['Question', '', '', '', 'Answer Summaries']]
    data.append([''])
    data.append(['Total Number of Calls', total_calls])
    data.append(['# of people who did not answer any questions', callers_with_no_ques_ans])
    data.append([''])
    data.append([''])
    data.append(['', 'No Answer', 'Total Answered'])

    questions = get_questions(survey_id)
    for q in questions:
        records_for_ques = records.filter(question_id = q.id)
        if isinstance(q, Question_mc):
            labels = dict(enumerate(q.choices.split(',')))
            choices, counts = get_mc_results(q.id, record_ids)
            ques_attribs = ['Choices', '', ''] + [labels[c - 1] for c in choices]
            calls_with_ques_ans = records_for_ques.filter(mc_choice__isnull = False).count()
        elif isinstance(q, Question_qu):
            bin_size = get_bin_size(q.id, 10, record_ids)
            bins, counts = get_qu_results(q.id, bin_size, record_ids)
            ques_attribs = ['Range', '', ''] + [str(b * bin_size) + '-' + str(b*bin_size + bin_size - 1) for b in bins if b is not None]
            calls_with_ques_ans = records_for_ques.filter(qu_value__isnull = False).count()
        elif isinstance(q, Question_ff):
            ques_attribs = ['Query']
            calls_with_ques_ans = records_for_ques.filter(ff_audio__isnull = False).count()
            counts = []

        calls_with_ques_not_ans = total_calls - calls_with_ques_ans
        data.append(ques_attribs)
        data.append([q.text, calls_with_ques_not_ans, calls_with_ques_ans] + counts)
        data.append([''])

    return data


def get_mc_results(question_id, record_ids=None):
    mc_type_id = ContentType.objects.get_for_model(Question_mc).id
    query1 = 'select mc_choice, count(mc_choice) as count from' + \
        ' survey_record where content_type_id = %s and question_id = %s' + \
        ' and mc_choice is not null and mc_choice <> 0 group by mc_choice'

    query2 = 'select mc_choice, count(mc_choice) as count from' + \
        ' survey_record where id in %s and content_type_id = %s and' + \
        ' question_id = %s and mc_choice is not null and mc_choice <> 0' + \
        ' group by mc_choice'

    cursor = connection.cursor()
    if record_ids is None:
        query = query1 % (mc_type_id, question_id)
    else:
        if len(record_ids) == 0:
            return [], []
        if len(record_ids) == 1:
            record_ids = '(%s)' % record_ids[0]
        query = query2 % (record_ids, mc_type_id, question_id)

    cursor.execute(query)
    results = cursor.fetchall()

    choices = []
    counts = []
    for result in results:
        choices.append(result[0])
        counts.append(result[1])

    return choices, counts


def get_bin_size( question_id, bin_count, record_ids = None ):
    if not bin_count:
        return None

    qu_type_id = ContentType.objects.get_for_model(Question_qu).id
    if record_ids is None:
        values = Record.objects.filter(content_type__id = qu_type_id, question_id = question_id).exclude(qu_value__isnull = True).values_list('qu_value', flat = True)
    else:
        if len(record_ids) == 0:
            return
        values = Record.objects.filter(id__in = record_ids, content_type__id = qu_type_id, question_id = question_id).exclude(qu_value__isnull = True).values_list('qu_value', flat = True)

    maxvalue = max(values) if values else 10
    minvalue = min(values) if values else 0

    return math.ceil(float(maxvalue - minvalue)/bin_count)


def get_qu_results( question_id, bin_size, record_ids = None ):
    qu_type_id = ContentType.objects.get_for_model(Question_qu).id

    cursor = connection.cursor() #@UndefinedVariable
    if record_ids is None:
        query = 'select floor(qu_value/%(bin_size)s) as bin, count(floor(qu_value/%(bin_size)s)) as count from survey_record where content_type_id = %(type_id)s and question_id = %(question_id)s and qu_value is not null group by bin order by \'bin\'' % {'type_id': qu_type_id, 'question_id': question_id, 'bin_size': bin_size}
    else:
        if len(record_ids) == 0:
            return [], []
        query = 'select floor(qu_value/%(bin_size)s) as bin, count(floor(qu_value/%(bin_size)s)) as count from survey_record where id in %(id)s and content_type_id = %(type_id)s and question_id = %(question_id)s and qu_value is not null group by bin order by \'bin\'' % {'id': record_ids, 'type_id': qu_type_id, 'question_id': question_id, 'bin_size': bin_size}

    cursor.execute(query)
    results = cursor.fetchall()
    results = sorted(results, key = lambda tup: tup[0])

    bins = []
    counts = []
    for result in results:
        bins.append(result[0])
        counts.append(result[1])

    return bins, counts


def get_answered_ques_distrib(ai_id, start_date = None, end_date = None):
    survey_id = Survey.objects.get(ai_id = ai_id, is_active = True).id
    ques_count = len(get_questions(survey_id))
    distrib = dict.fromkeys(range(1, ques_count + 1), 0)

    records = Record.objects.filter(survey_id = survey_id).exclude(mc_choice__isnull = True, qu_value__isnull = True, ff_audio__isnull = True)
    if start_date and end_date:
        records = records.filter(cdr__start_time__range = (start_date, end_date))

    cdr_pool = records.values('cdr').annotate(record_count = Count('cdr'))
    for each in cdr_pool:
        distrib[ each['record_count'] ] = distrib[ each['record_count'] ] + 1

    return distrib


def populate_stats(ai, start_date = None, end_date = None, force_update = False):
    stats_stats.populate_daily_cumulative_stats(ai, force_update = force_update)
    
def get_call_stats(ais, start_datetime, end_datetime):
        call_stats = []
        
        for ai in ais:
            stats = stats_stats.get_ai_call_stats(ai, start_datetime, end_datetime)
            survey_id = get_survey_ids([ai.id])
            stats.avg_ques_answered = 0.0
            if stats.calls > 0:
                stats.avg_ques_answered = avg_ques_answered(survey_id, stats.call_details)
            call_stats.append(stats)
        return call_stats

def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    data = get_call_stats(ais, start_datetime, end_datetime)
    reports = []
    return (data, reports)


def create_html_content(stats):
    return render_to_string('survey/stats.html', { 'data': stats }) + LINE_BREAK
