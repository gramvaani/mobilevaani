from django.db import models, connection

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models import Sum
from django.db.models.signals import pre_save, post_save, pre_delete
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from vapp.app_manager.models import App_instance, Cdr
from vapp.media.models import Prompt_audio, Prompt_set, Prompt_info, Recording, App_instance_prompt_set
from vapp.sms.models import SMS_message
from vapp.mnews.models import Channel, News, Transient_group_schedule
from vapp.callerinfo.models import DEFAULT_CALLERID, Contact_list

from vapp.events import Event

from datetime import datetime
import math
import RQMC
import json

from log import get_request_logger
logger = get_request_logger()

FORM_NAME_LENGTH    = 255
COMMENT_LENGTH      = 255
ROLE_LENGTH         = 32
QUESTION_TEXT_LENGTH= 255
FFA_TEXT_LENGTH     = 255
DFA_TEXT_LENGTH     = 255

DEFAULT_QUESTION_WEIGHTAGE = 1

class Form(models.Model):
    name        = models.CharField(max_length = FORM_NAME_LENGTH)
    comment     = models.CharField(max_length = COMMENT_LENGTH)
    is_frozen   = models.BooleanField(default = False)
    prompt_set  = models.ForeignKey(Prompt_set)

    @classmethod
    def create( cls, ai ):
        name = ai.name
        prompt_set = App_instance_prompt_set.objects.get( ai = ai ).prompt_set
        form = Form( name = name, prompt_set = prompt_set )
        form.save()
        return form

    def create_copy(self):
        prompt_set = self.prompt_set.create_copy()
        name=('Copy of' + self.name)[0:32]
        form = Form(name = name, comment = self.comment, prompt_set = prompt_set)
        form.save()
        for fe in Form_entity.objects.filter(form = self):
            Form_entity(form = form, perm = 'r', role = fe.role, content_type = fe.content_type, object_id = fe.object_id).save()

        conditional_flows = Survey_conditional_flow.objects.filter(form = self)
        for conditional_flow in conditional_flows:
            survey_conditional_flow = Survey_conditional_flow(condition = conditional_flow.condition, form = form, \
                question_order = conditional_flow.question_order, action = conditional_flow.action, object_id = conditional_flow.object_id, \
                execution_priority = conditional_flow.execution_priority,  is_df_enabled = conditional_flow.is_df_enabled, df_properties = conditional_flow.df_properties,\
                intent_name = conditional_flow.intent_name)            
            survey_conditional_flow.save() 

        for question in [fq.question for fq in Form_question.objects.filter(form = self.id)]:
            if isinstance(question, Question_mc):
                new_question = Question_mc(num_choices = question.num_choices, choices = question.choices)
                type = ContentType.objects.get_for_model(Question_mc)
            elif isinstance(question, Question_qu):
                new_question = Question_qu()
                type = ContentType.objects.get_for_model(Question_qu)
            elif isinstance(question, Question_ff):
                new_question = Question_ff(max_duration = question.max_duration)
                type = ContentType.objects.get_for_model(Question_ff)
            elif isinstance(question, Question_df):
                new_question = Question_df(max_duration = question.max_duration)
                type = ContentType.objects.get_for_model(Question_df)
            elif isinstance(question, Question_qna):
                new_question = Question_qna(max_duration = question.max_duration)
                type = ContentType.objects.get_for_model(Question_qna)     
            new_question.order = question.order
            new_question.text  = question.text
            new_question.validate = question.validate
            new_question.prompt_audio = Prompt_audio.objects.get(prompt_set = prompt_set.id, info__name = 'survey_question_num_' + str(question.order) )

            pre_prompt_info= Prompt_info.objects.get(name = 'survey_pre_question_num_' + str(question.order))
            try:
                pre_prompt_audio = Prompt_audio.objects.get( prompt_set = prompt_set, info = pre_prompt_info )
            except ObjectDoesNotExist:
                pre_prompt_audio = Prompt_audio.objects.create( prompt_set = prompt_set, info = pre_prompt_info )
            except MultipleObjectsReturned:
                pre_prompt_audio = Prompt_audio.objects.filter( prompt_set = prompt_set, info = pre_prompt_info )[0]

            new_question.pre_prompt_audio = pre_prompt_audio
            new_question.save()
            Form_question(form = form, content_type = type, object_id = new_question.id).save()
        return form

    def message_queue(self):
        return Form.message_queue_from_id( self.id )

    @classmethod
    def message_queue_from_id(cls, id):
        return '/topic/survey_form_%s' % id

    def __unicode__(self):
        return unicode(self.id) + '_' + unicode(self.name)

def form_save_handler(sender, **kwargs):
    form = kwargs['instance']
    form.prompt_set.name = ('form_' + str(form.id) + '_' + form.name)[0:32]
    form.prompt_set.save()
    if not kwargs['created']:
        RQMC.push_instance(form.message_queue(), form, Event.UPDATE)

post_save.connect(form_save_handler, Form, dispatch_uid = 'survey.form.save')

class Question(models.Model):
    order       = models.PositiveIntegerField()
    text        = models.CharField(max_length = QUESTION_TEXT_LENGTH)
    prompt_audio= models.ForeignKey(Prompt_audio, null = True, blank = True)
    validate    = models.BooleanField(default = False)
    pre_prompt_audio= models.ForeignKey(Prompt_audio, null = True, blank = True, related_name = '+')
    is_last = models.BooleanField(default = False)

    class Meta:
        abstract = True

    def __unicode__(self):
        return unicode(self.text)

class Question_mc(Question):
    num_choices = models.PositiveIntegerField()
    choices     = models.CharField(max_length = QUESTION_TEXT_LENGTH)

    def type(self):
        return 'mc'

def question_mc_save_handler(sender, **kwargs):
    question = kwargs['instance']
    question.num_choices = len(question.choices.split(","))

pre_save.connect(question_mc_save_handler, Question_mc, dispatch_uid = 'survey.question_mc.pre_save')

class Question_qu(Question):
    max_digits = models.PositiveSmallIntegerField(default = 9)

    def type(self):
        return 'qu'

class Question_ff(Question):
    max_duration = models.PositiveIntegerField(default = 120)

    def type(self):
        return 'ff'

    def __unicode__(self):
        return str(self.id) + '_' + unicode(self.text)

class Question_df(Question):
    max_duration = models.PositiveIntegerField(default = 30)

    def type(self):
        return 'df'

    def __unicode__(self):
        return str(self.id) + '_' + unicode(self.text)    

class Question_qna(Question):
    max_duration = models.PositiveIntegerField(default = 30)

    def type(self):
        return 'qna'

    def __unicode__(self):
        return str(self.id) + '_' + unicode(self.text)

class Df_properties(models.Model):
    language = models.CharField(max_length = 100)
    context_name = models.CharField(max_length = 255, null = True, blank = True)
    phrase_hints = models.CharField(max_length = 255)

    def __unicode__(self):
        return 'Df_properties_{0}_{1}'.format(self.language, self.context_name)

class Survey_conditional_flow(models.Model):
    ACTION_CHOICES = (
        ('playback', 'playback'),
        ('next_qu', 'nextQuestion'),
        ('next_ai', 'nextAi')
    )
    condition = models.CharField(max_length=6000)
    form = models.ForeignKey('Form')
    question_order = models.IntegerField(default = -1)
    action = models.CharField(max_length=255, choices=ACTION_CHOICES,null=True)
    object_id = models.CharField(max_length=255)
    execution_priority = models.PositiveIntegerField()
    is_df_enabled = models.BooleanField(default=False)
    df_properties = models.ForeignKey(Df_properties, null = True, blank = True)
    intent_name = models.CharField(max_length=255, null = True, blank = True)

    def __unicode__(self):
        return 'Survey_condtional_flow_{0}_{1}_{2}'.format(self.form.id,self.question_order,self.action)

    @staticmethod
    def checkConditionForAction(survey_data, action, form_id, question_order=-1):
        logger.info('starting check conditon for action....')
        survey_conditions = Survey_conditional_flow.objects.filter(form_id=form_id, action=action,
                                                                   question_order=question_order).order_by(
            'execution_priority')
        logger.info('condition priority' +str(survey_conditions))
        for survey_condition in survey_conditions:
            jump_condition = json.loads(survey_condition.condition)
            func_name = eval(jump_condition.get('function'))
            if func_name(jump_condition, survey_data):
                logger.info('Returning ' +str(survey_condition.object_id))
                return survey_condition.object_id
        return None

    @staticmethod
    def getMatchingCriteria(survey_record, survey_condition):
        entity_json = '{}'
        if survey_record.entity_json:
            entity_json = survey_record.entity_json

        jump_condition = json.loads(survey_condition)
        func_name = eval(jump_condition.get('function'))
        matched_criteria = func_name(jump_condition, entity_json)
        return matched_criteria

def is_result(operator, operand, value):
    if operator == 'gt':
       if value > operand:
           return True
    elif operator == 'gte':
       if value >= operand:
           return True
    elif operator == 'lt':
       if value < operand:
           return True
    elif operator == 'lte':
       if value <= operand:
           return True
    elif operator == 'eq':
       if value == operand:
           return True
    elif operator == 'neq':
       if value != operand:
           return True	
    elif operator == 'strlengt':
       if len(value) > len(operand):
           return True
    elif operator == 'strlengte':
       if len(value) >= len(operand):
           return True
    elif operator == 'strlenlt':
       if len(value) < len(operand):
           return True
    elif operator == 'strlenlte':
       if len(value) <= len(operand):
           return True
    elif operator == 'strleneq':
       if len(value) == len(operand):
           return True             

    return False       


def basic_relational_handler(condition, entity_json):
    entity_value = None
    logger.info('entity_json::'+str(entity_json))
    #entity_json = '{"success": false, "entity": [{"dtmf": 24}]}'
    data_dict = json.loads(entity_json)
    if data_dict.get('success'):
        entity = data_dict.get('entity')[0]
        entity_value = entity.values()[0]
    
    print(condition)

    criteria_dict_list = condition.get('criteria',[])
    default_dict = {}
    for criteria_dict in criteria_dict_list:
        operator,operand = None, None
        condition = criteria_dict.get('condition')
        if criteria_dict.get('default'):
            default_dict = criteria_dict
        if condition:
            operator,operand = condition.split('_')
        else:
            continue
        print("operator and operand: "+str(operator)+str(operand))
        logger.info('entity_value :: '+str(entity_value))
        if entity_value and is_result(operator, operand, str(entity_value).lower()):                  
            return json.dumps(criteria_dict)   

    return json.dumps(default_dict)

def combination(condition, survey_response_entities):
    logger.info('inside combination')
    relevant_order = condition.get('relevant_order')
    options_choices_list = condition.get('values')
    choices_length = len(relevant_order)

    # get the options_selected by the user which are relevant for matching
    options_selected = [survey_response_entities[int(i)].get('dtmf','-1') if int(i) in survey_response_entities else '-1' for i in relevant_order]

    # match with existing choices list
    for options_choices in options_choices_list:
        count = 0
        for i in range(choices_length):
            if not ((options_choices[i] == 'any') or (combination_in(options_choices[i], options_selected[i]))):
                break
            count = count + 1

        if (count == choices_length):
            return True
    return False


def combination_in(option_choices, options_selected):
    if options_selected in option_choices.split(','):
        return True
    return False

class Form_question(models.Model):
    form    = models.ForeignKey(Form)

    content_type= models.ForeignKey(ContentType)
    object_id   = models.PositiveIntegerField()
    question    = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return unicode(self.form) + '_' + unicode(self.question)

class Form_entity(models.Model):
    PERM_CHOICES = (
        ('r', 'Read'),
        ('w', 'Write'),
    )

    form = models.ForeignKey(Form)
    perm = models.CharField(max_length = 1, choices = PERM_CHOICES)
    role = models.CharField(max_length = ROLE_LENGTH)

    content_type= models.ForeignKey(ContentType)
    object_id   = models.PositiveIntegerField()
    entity      = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return unicode(self.form) + '_' + unicode(self.entity)

def form_entity_save_handler(sender, **kwargs):
    form_entity = kwargs['instance']
    queue = Form.message_queue_from_id( form_entity.form_id )
    RQMC.push_instance(queue, form_entity, Event.ADD)

def form_entity_delete_handler(sender, **kwargs):
    form_entity = kwargs['instance']
    queue = Form.message_queue_from_id( form_entity.form_id )
    RQMC.push_instance(queue, form_entity, Event.DELETE)

post_save.connect(form_entity_save_handler, Form_entity, dispatch_uid = 'survey.form_entity.save')
pre_delete.connect(form_entity_delete_handler, Form_entity, dispatch_uid = 'survey.form_entity.delete')


class Survey(models.Model):
    name        = models.CharField( max_length = 255 )
    ai          = models.ForeignKey(App_instance)
    form        = models.OneToOneField(Form, null = True, blank = True)
    is_enabled  = models.BooleanField(default = False)
    custom_data = models.ForeignKey(ContentType, null = True, blank = True)
    custom_data_mappings = models.ManyToManyField('Survey_custom_data_mapping', null = True, blank = True)
    is_active   = models.BooleanField( default = False )
    need_assessment = models.BooleanField( default = False )
    misc_properties = models.TextField()

    @classmethod
    def create( cls, ai, form ):
        try:
            survey = Survey( ai = ai, name = ai.name, is_enabled = True, form = form )
            survey.save()
            return survey
        except:
            logger.exception( "Could not create survey for app instance: " + str(ai.id) )

    def copy(self, name = None):
        copy = Survey()
        copy.name = name if name else 'Copy of ' + self.name
        copy.ai = self.ai
        copy.is_enabled = self.is_enabled
        copy.form = self.form.create_copy()
        copy.custom_data = self.custom_data        
        copy.save()
        for custom_data_mapping in self.custom_data_mappings.get_query_set():
            copy.custom_data_mappings.add( custom_data_mapping )
        return copy
    
    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.form)

def survey_save_handler(sender, **kwargs):
    survey = kwargs['instance']
    if survey.form:
        ai_prompt_set = App_instance_prompt_set.objects.get_or_create(ai_id = survey.ai.id, prompt_set_id = survey.form.prompt_set_id, perm = 'w', is_current = True)

post_save.connect(survey_save_handler, Survey, dispatch_uid = 'survey.survey.post_save')

class SurveyFields:
    CALLERID    = 'callerid'
    Q1          = 'q1'
    Q2          = 'q2'
    Q3          = 'q3'
    Q4          = 'q4'
    Q5          = 'q5'


class Survey_custom_data_mapping(models.Model):
    fields = [attr for attr in dir(SurveyFields) if not callable(attr) and not attr.startswith("__")]
    SURVEY_FIELD_CHOICES = tuple([ (getattr(SurveyFields, field), field) for field in fields ])

    survey_field = models.CharField(max_length = 20, choices = SURVEY_FIELD_CHOICES)
    data_field = models.CharField(max_length = 20)

    def __unicode__(self):
        return unicode(self.survey_field) + " <--> " + unicode(self.data_field)

class Status_history(models.Model):
    survey      = models.ForeignKey(Survey)
    is_enabled  = models.BooleanField()
    time        = models.DateTimeField(auto_now_add = True)

class Survey_cdr(models.Model):
    survey = models.ForeignKey( Survey )
    cdr = models.ForeignKey( Cdr )
    time = models.DateTimeField( default = datetime.now )
    audio_playback_item = models.ForeignKey(News, null=True)

    def save( self, *args, **kwargs ):
        if not self.time:
            self.time = self.cdr.answered_time
        super( Survey_cdr, self ).save( *args, **kwargs )
    

class Record(models.Model):
    survey      = models.ForeignKey(Survey)
    form        = models.ForeignKey(Form)
    time        = models.DateTimeField(auto_now_add = True)

    content_type= models.ForeignKey(ContentType)
    question_id = models.PositiveIntegerField()
    question    = generic.GenericForeignKey('content_type', 'question_id')

    order       = models.PositiveIntegerField()
    cdr         = models.ForeignKey(Cdr)

    mc_choice   = models.PositiveIntegerField(null = True)
    qu_value    = models.BigIntegerField(null = True)
    ff_audio    = models.ForeignKey(Recording, null = True, related_name='ff_audio')
    ff_text     = models.CharField(max_length = FFA_TEXT_LENGTH)
    cf = models.ForeignKey(Survey_conditional_flow, null = True, blank = True)
    cf_criteria = models.TextField()
    entity_json = models.TextField()
    playback_item = models.ForeignKey(News, null=True)

    def ff_audio_url(self):
        if self.ff_audio:
            return self.ff_audio.get_url()
        return None
    
    def ff_value(self):
        if self.ff_audio:
            return str( self.ff_audio.id ) + '.mp3'
        return None
    def ff_full_filename(self):
        if self.ff_audio:
            return self.ff_audio.get_full_filename()
        return None

    def df_audio_url(self):
        if self.ff_audio:
            return self.ff_audio.get_url()
        return None
    
    def df_value(self):
        if self.ff_audio:
            return str( self.ff_audio.id ) + '.mp3'
        return None
    def df_full_filename(self):
        if self.ff_audio:
            return self.ff_audio.get_full_filename()
        return None    

class Record_error(models.Model):
    survey      = models.ForeignKey(Survey)
    form        = models.ForeignKey(Form)
    time        = models.DateTimeField(auto_now_add = True)

    content_type= models.ForeignKey(ContentType)
    question_id = models.PositiveIntegerField()
    question    = generic.GenericForeignKey('content_type', 'question_id')

    order       = models.PositiveIntegerField()
    cdr         = models.ForeignKey(Cdr)

    mc_choice   = models.PositiveIntegerField(null = True)
    qu_value    = models.IntegerField(null = True)
    ff_audio    = models.ForeignKey(Recording, null = True)
    ff_text     = models.CharField(max_length = FFA_TEXT_LENGTH)

        
class Report_creds(models.Model):

    ai = models.ForeignKey(App_instance)
    url = models.CharField(max_length = 100)
    cred_key = models.CharField(max_length = 255)
    order = models.CharField(max_length = 255)

    def __unicode__(self):
        return str(self.ai) + ' s creds'

    
class Report_log(models.Model):

    cred = models.ForeignKey(Report_creds)
    report = models.CharField(max_length = 255)
    time = models.DateField(auto_now_add = True)
    error_code = models.SmallIntegerField()
    cdr = models.ForeignKey(Cdr)


class Survey_ff_response_to_mnews_channel_properties(models.Model):

    survey_ai = models.ForeignKey(App_instance)
    survey_ff_question = models.ForeignKey(Question_ff)
    mnews_channel = models.ForeignKey(Channel)
    allow_overwrite = models.BooleanField(default = False)
    copy_callerid = models.BooleanField(default = True)

    def __unicode__(self):
        return str(self.survey_ai) + '_' + str(self.survey_ff_question) + '_' + str(self.mnews_channel)

class Survey_ff_response_channel_map_log(models.Model):

    mapping_properties = models.ForeignKey(Survey_ff_response_to_mnews_channel_properties)
    ff_response = models.ForeignKey(Recording)
    item_copied = models.ForeignKey(News)
    copy_time = models.DateTimeField(default = datetime.now)
    is_overwritten = models.BooleanField()


    @classmethod
    def get_ff_response_already_copied(cls, mapping_property, ff_response_recording):
        return Survey_ff_response_channel_map_log.objects.filter(mapping_properties = mapping_property, \
                                                                 ff_response = ff_response_recording).order_by('-copy_time')

    @classmethod
    def are_input_params_correct(cls, newly_created, survey_ai, survey_ff_question, ff_response_recording ):

        return ((newly_created and survey_ai and survey_ff_question and ff_response_recording) and \
                (isinstance(survey_ai, App_instance) and isinstance(survey_ff_question, Question_ff) and isinstance(ff_response_recording, Recording)))

    @classmethod
    def create_mnews_item(cls, ff_response_recording, record, mapping_property, channel ):
        copy_time = datetime.now()
        title = 'News, Created from ff response %s at %s' % (str(ff_response_recording.id), copy_time.strftime('%H:%M, %d %b\'%y'), )
        callerid = record.cdr.callerid if mapping_property.copy_callerid else DEFAULT_CALLERID
        news_item = News(ai=channel.ai, channel=channel,
                         state='UNM', is_comment=False,
                         source=News.Source.VOICE_INTERFACE, title=title,
                         time=copy_time, callerid=callerid, creation_cdr=self.sessionData.cdrs[0])
        detail = ff_response_recording.clone(new_ai_id = channel.ai.id)
        news_item.detail = detail
        news_item.save()
        return news_item

    @classmethod
    def copy_ff_response_to_channels(cls, record, newly_created ):
        survey_ai = record.survey.ai
        survey_ff_question = record.question
        ff_response_recording = record.ff_audio

        if cls.are_input_params_correct(newly_created, survey_ai, survey_ff_question, ff_response_recording):
            try:
                mapping_properties = Survey_ff_response_to_mnews_channel_properties.objects.filter(survey_ai = survey_ai, survey_ff_question = survey_ff_question)
                for mapping_property in mapping_properties:
                    news_item = None
                    channel = mapping_property.mnews_channel

                    previous_ff_response_copies = cls.get_ff_response_already_copied(mapping_property, ff_response_recording)
                    if not mapping_property.allow_overwrite and previous_ff_response_copies.exists():
                        logger.info('copy_ff_response_to_channel: allow_overwrite is false and already copied')
                        continue

                    is_overwritten = False

                    if len(previous_ff_response_copies) > 0:
                        if mapping_property.allow_overwrite:
                            news_item = previous_ff_response_copies[0].item_copied
                            is_overwritten = True
                    else:
                        news_item = cls.create_mnews_item(ff_response_recording, record, mapping_property, channel)
                        copy_log = Survey_ff_response_channel_map_log(mapping_properties = mapping_property,
                                                                      ff_response = ff_response_recording,
                                                                      item_copied = news_item, is_overwritten = is_overwritten  )
                        copy_log.save()

            except Exception,e:
                logger.exception('Exception while applying ff response to mnews channel properties: %s' % (str(e)))


class Mcq_choice_weightage(models.Model):
    question       = models.OneToOneField(Form_question)
    correct_option = models.PositiveSmallIntegerField(null = True, blank = True)
    weightages      = models.CharField(max_length = 30, null = True, blank = True)

    def __unicode__(self):
        return unicode(self.question_id) + '_' + unicode(self.question) + '_' + unicode(self.correct_option) + '_' + unicode(self.weightages)


class SurveyEvents:
    SEND_ASSESSMENT_RESULT = 'SEND_ASSESSMENT_RESULT'


class SurveyContentTypes:
    MCQ = ContentType.objects.get_for_model(Question_mc)
    FFQ = ContentType.objects.get_for_model(Question_ff)
    QUQ = ContentType.objects.get_for_model(Question_qu)
    DFQ = ContentType.objects.get_for_model(Question_df)
    QNA = ContentType.objects.get_for_model(Question_qna)

class Assessment_status(object):
    CR  = 'CR'
    ICR = 'ICR'
    NA  = 'NA'


class Assessment(models.Model):

    ASSESSMENT_CHOICES = (
        (Assessment_status.CR, 'Correct'),
        (Assessment_status.ICR, 'Incorrect'),
        (Assessment_status.NA, 'Not Applicable'),
    )

    record = models.OneToOneField( Record )
    result = models.CharField( max_length = 32 )
    status = models.CharField( max_length = 32, null = True, choices = ASSESSMENT_CHOICES )


    @classmethod
    def map_record_assesment( cls, **kwargs ):
        records = Record.objects.filter( **kwargs )
        for record in records:
            try:
                assessment = Assessment.objects.get( record = record )
            except Assessment.DoesNotExist:
                assessment = Assessment( record = record )
            assessment.result = Benchmark.get_assessment_result( record )
            assessment.status = Benchmark.get_assessment_status( record )
            if assessment.result is not None:
                assessment.save()

    # We are currently generating assessment for only mc type questions.
    # We can replace following function altogether once assessment for all question types are done
    @classmethod
    def populate_assessment_data( cls, survey_id, cdr_id = None, content_type = None, from_date = None):
        kwargs = {}
        kwargs['survey_id'] = survey_id
        if cdr_id:
            kwargs['cdr_id'] = cdr_id
        if content_type:
            kwargs['content_type'] = content_type
        if from_date:
            kwargs['cdr__start_time__gte'] = from_date

        for content_type in [SurveyContentTypes.MCQ]:
            cls.map_record_assesment(**kwargs)


class Benchmark_choice(models.Model):

    CATEGORY_CHOICES = (
        ('A', 'Easy'),
        ('B', 'Average'),
        ('C', 'Difficult'),
    )

    CATEGORY_INDEX_CHOICES = (
        ('1', 1),
        ('3', 3),
        ('5', 5),
    )

    name      = models.CharField(max_length = 50)
    category  = models.CharField(max_length = 50, choices = CATEGORY_CHOICES)
    weightage = models.CharField(max_length = 10, choices = CATEGORY_INDEX_CHOICES)

    def __unicode__(self):
        return unicode(self.id) + '_' + unicode(self.name)


class Benchmark(models.Model):
    question         = models.OneToOneField(Form_question)
    benchmark_choice = models.ForeignKey(Benchmark_choice)

    @classmethod
    def get_assessment_result( cls, record ):
        try:
            benchmark = Benchmark.objects.filter( question__form = record.form, question__content_type = record.content_type, question__object_id = record.question.id )
            if benchmark.exists():
                benchmark = benchmark[0]
                question_weightage = benchmark.benchmark_choice.weightage
            else:
                question_weightage = DEFAULT_QUESTION_WEIGHTAGE

            if isinstance(record.question, Question_mc) and record.mc_choice:
            # Above if condition could be removed once we are generating assessment for all question types
                return float(question_weightage) * float(cls.get_answer_weightage(record))
        except Benchmark.DoesNotExist:
            logger.exception('Benchmark not implemented for question %s' % record.question)
        except:
            logger.exception('Exception while getting assessment result for the record %s' % record.id)
        return None

    @classmethod
    def get_assessment_status( cls, record ):
        try:
            if isinstance(record.question, Question_mc) and record.mc_choice:
                correct_option = Mcq_choice_weightage.objects.get( question__content_type = record.content_type, question__object_id = record.question.id ).correct_option
                if not correct_option:
                    return Assessment_status.NA
                return Assessment_status.CR if record.mc_choice == correct_option else Assessment_status.ICR
        except Exception, e:
            logger.exception('Exception in get_assessment_status: %s' % str(e))

    @classmethod
    def get_answer_weightage( cls, record ):
        weightage = 0

        if isinstance(record.question, Question_mc):
            weightage = cls.get_mcq_answer_weightage(record)
        else:
            # Weightage calculation for other question types may be added later on
            pass

        return weightage

    @classmethod
    def get_mcq_answer_weightage( cls, record ):
        try:
            weightages = Mcq_choice_weightage.objects.get( question__content_type = record.content_type, question__object_id = record.question.id ).weightages
            return weightages.replace( " ", "" ).split( ',' )[ record.mc_choice - 1 ]
        except Exception, e:
            logger.exception('Exception in get_mcq_answer_weightage. %s' % str(e))

    @classmethod
    def get_survey_weightage( cls, survey_id ):
        survey_weightage = 0.0
        questions = Form_question.objects.filter( form__survey = survey_id )
        for form_question in questions:
            benchmark = Benchmark.objects.filter( question = form_question )
            if benchmark.exists():
                survey_weightage += float(benchmark.benchmark_choice.weightage)
            elif isinstance(form_question.question, Question_mc):
                try:
                    weightages = Mcq_choice_weightage.objects.get( question = form_question).weightages
                    survey_weightage += float( max(weightages.replace( " ", "" ).split( ',' )) )
                except:
                    pass
            else:
                pass

        return survey_weightage

    def __unicode__(self):
        return str(self.question_id) +'_' + str(self.question)


class Cdr_event_sms_log(models.Model):
    cdr   = models.ForeignKey(Cdr)
    sms   = models.ForeignKey(SMS_message)
    event = models.CharField(max_length = 64, null = True)


class Assessment_ai_unit_map(models.Model):
    ai = models.OneToOneField(App_instance)
    unit_name = models.CharField(max_length = 20, null = True, blank = True)

    def __unicode__(self):
        return '%s_%s' % (self.ai, self.unit_name) if self.unit_name else str(self.ai)


def app_specific_creation_tasks( ai_id, user_id = None ):
    ai = App_instance.objects.get( pk = ai_id  )
    form = Form.create( ai )
    survey = Survey.create( ai, form )
    if user_id:
        user_type = ContentType.objects.get_for_model( User )
        Form_entity( form = form, perm = 'w', content_type = user_type, object_id = user_id ).save()

def get_survey_custom_fields(survey):
    return [ field for field in survey.custom_data.model_class()._meta.fields if not field.name == 'id' ]

def is_survey_accessed(survey_id, cdr):
    return ( Survey_cdr.objects.filter(survey__id = survey_id, cdr = cdr).count() > 0 )


class Survey_response_summary(models.Model):
    survey_cdr = models.ForeignKey(Survey_cdr)
    total_ques_answered = models.PositiveSmallIntegerField(default = 0)

class Survey_feedback_call_config(models.Model):
    survey = models.ForeignKey(Survey)
    schedule = models.ForeignKey(Transient_group_schedule)
    contact_list = models.ForeignKey(Contact_list)
    ff_quesion_order = models.PositiveIntegerField()
