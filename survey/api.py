from django.conf.urls import url
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.db.models import Sum, Q

from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from tastypie.contrib.contenttypes.fields import GenericForeignKeyField
from tastypie.resources import ModelResource, Resource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.contrib.contenttypes.fields import GenericForeignKeyField

from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from survey.models import Form, Survey, Question_mc, Question_df, Question_qna, Question_ff, Question_qu, Record, Form_question, Survey_cdr, Assessment, Benchmark

from vapp.api import AppInstanceAuthorization, apply_default_checks
from vapp.app_manager.api import AppInstanceResource
from vapp.app_manager.models import App_instance

from vapp.media.api import RecordingResource, PromptAudioResource
from vapp.media.models import Prompt_info, Prompt_audio, Recording
from vapp.callerinfo.models import Contact
from vapp.callerinfo.api import ContactResource
from vapp.callerinfo.common import lookup_contact_for_user
from vapp.customization.models import check_and_get_excel_format
from vapp.customer.models import Project
from vapp.location.models import Location
from vapp.location.api import LocationResource


from vapp.survey.stats import get_detailed_stats, get_answered_ques_distrib, get_questions, get_mc_results, get_bin_size, get_qu_results, get_cdrs_for_survey, get_cdrs_from_survey_cdrs, get_summary_stats, get_mc_labels, get_results_data
from vapp.survey.views import reorder_questions_impl, delete_question_impl
from vapp.survey.prompts import get_standard_prompts

from vapp.utils import generate_attachment, generate_workbook, datestr_to_datetime, get_ordered_distinct_list
from vapp.log import get_request_logger
import types, string


logger = get_request_logger()
user_type = ContentType.objects.get_for_model( User )

class FormResource(ModelResource):
    ps_id = fields.IntegerField( attribute = 'prompt_set__id' )
    class Meta:
        queryset = Form.objects.all()
        resource_name = 'survey_form'

        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def build_filters(self, filters = None):
        if filters is None:
            filters = {}

        orm_filters = super(FormResource, self).build_filters( filters )
        if "survey_id" in filters:
            orm_filters[ "survey" ] = filters[ "survey_id" ]
        return orm_filters

class SurveyResource(ModelResource):
    ai  = fields.OneToOneField(AppInstanceResource,'ai',full=True)
    form  = fields.OneToOneField(FormResource,'form',full=True)
    
    class Meta:
        queryset = Survey.objects.all() 
        resource_name = "survey_survey"
        filtering = {
                    'ai': ALL,
                    'is_active': ALL,
                    'need_assessment': ALL,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        extra_actions = [
                {
                    "name" : "prompts",
                    "http_method": "GET",
                    "resource_type": "list",
                    "summary": "Get all the survey prompts",
                    "fields": {
                        "survey_id": {
                            "type":"integer",
                            "required": True,
                            "description": "survey id - related"
                        },
                    }
                },
                {
                    "name" : "save",
                    "http_method": "POST",
                    "resource_type": "list",
                    "summary": "Save a new survey",
                    "fields": {
                        "survey_id": {
                            "type":"integer",
                            "required": True,
                            "description": "survey id - related"
                        },
                    }
                }
        ]

    def prepend_urls ( self ):
        return [
            url(r"^(?P<resource_name>%s)/prompts%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'prompts' ), name = "api_prompts" ),
            url(r"^(?P<resource_name>%s)/copy%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'copy' ), name = "api_copy" ),
            url(r"^(?P<resource_name>%s)/save%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'save' ), name = "api_save" ),
            ]

    def prompts( self, request, **kwargs ):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            survey_id = int( request.REQUEST[ 'survey_id' ] )
            standard_prompt_audios = get_standard_prompts( survey_id )
            prompt_bundles = []
            for prompt_audio in standard_prompt_audios:
                prompt_resource = PromptAudioResource()
                prompt_bundle = prompt_resource.build_bundle( obj = prompt_audio, request = request )
                prompt_bundles.append( prompt_resource.full_dehydrate( prompt_bundle ))
            self.log_throttled_access( request )
            return self.create_response( request, prompt_bundles )
        except Exception, e:
            logger.exception('exception in getting survey prompts: %s' %(str(e)))
        return self.create_response( request, { 'success': False, 'reason': "unable to get prompts" } )

    def save( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            survey_id = request.REQUEST[ 'survey_id' ]
            survey = Survey.objects.get( id = survey_id ) 
            selected_surveys = Survey.objects.filter( ai = survey.ai, is_active = True )

            for selected_survey in selected_surveys:
                selected_survey.is_active = False
                selected_survey.save()
            
            survey.is_active = True
            survey.is_enabled = True
            survey.form.is_frozen = True
            survey.form.save()
            survey.save()
            survey_resource = SurveyResource()
            survey_bundle = survey_resource.build_bundle( obj = survey, request = request )
            
            self.log_throttled_access( request )
            return self.create_response( request, survey_resource.full_dehydrate( survey_bundle ) )
    
        except Exception,e:
            logger.exception( 'exception in survey save: %s' %(str(e)) )
        return self.create_response( request, { 'success': False, 'error': "unable to save survey" } )

    def copy( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            survey_id = request.REQUEST[ 'survey_id' ]
            survey_name = request.REQUEST.get('survey_name', None)
            survey = Survey.objects.get( id = survey_id )
            survey_copy = survey.copy( name = survey_name )
            survey.is_active = False
            survey.save()
            
            survey_resource = SurveyResource()
            survey_bundle = survey_resource.build_bundle( obj = survey_copy, request = request )
            
            self.log_throttled_access( request )
            return self.create_response( request, survey_resource.full_dehydrate( survey_bundle ) )

        except Exception,e:
            logger.exception( 'exception in survey copy: %s' % (str(e)) )

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}

        orm_filters = super(SurveyResource, self).build_filters( filters )
        if "ai_id" in filters:
            orm_filters[ "ai" ] = filters[ "ai_id" ]
            orm_filters[ "is_enabled" ] = True if not filters.has_key("is_enabled") else ( filters["is_enabled"] == "true" )
        return orm_filters
        

class QuestionMcResource(ModelResource):
    type = fields.CharField( attribute = 'type' )
    prompt_audio = fields.ForeignKey( PromptAudioResource, attribute = 'prompt_audio', full = True, null = True )
    
    class Meta:
        queryset = Question_mc.objects.all()
        resource_name = "survey_question_mc"
        filtering = {
            'id': ALL_WITH_RELATIONS,
            }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class QuestionFfResource(ModelResource):
    type = fields.CharField( attribute = 'type' )
    prompt_audio = fields.ForeignKey( PromptAudioResource, attribute = 'prompt_audio', full = True, null = True )

    class Meta:
        queryset = Question_ff.objects.all()
        resource_name = "survey_question_ff"
        filtering = {
            'id': ALL_WITH_RELATIONS,
            }

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class QuestionDfResource(ModelResource):
    type = fields.CharField( attribute = 'type' )
    prompt_audio = fields.ForeignKey( PromptAudioResource, attribute = 'prompt_audio', full = True, null = True )

    class Meta:
        queryset = Question_df.objects.all()
        resource_name = "survey_question_df"
        filtering = {
            'id': ALL_WITH_RELATIONS,
            }

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class QuestionQnaResource(ModelResource):
    type = fields.CharField( attribute = 'type' )
    prompt_audio = fields.ForeignKey( PromptAudioResource, attribute = 'prompt_audio', full = True, null = True )

    class Meta:
        queryset = Question_qna.objects.all()
        resource_name = "survey_question_qna"
        filtering = {
            'id': ALL_WITH_RELATIONS,
            }

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()        

class QuestionQuResource(ModelResource):
    type = fields.CharField( attribute = 'type' )
    prompt_audio = fields.ForeignKey( PromptAudioResource, attribute = 'prompt_audio', full = True, null = True )
    
    class Meta:
        queryset = Question_qu.objects.all()
        resource_name = "survey_question_qu"
        filtering = {
        'id': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

def get_contact_for_user_callerid(user, callerid, request):
    contact = lookup_contact_for_user(user, callerid)
    contact_resource = ContactResource()
    contact_bundle = contact_resource.build_bundle(obj = contact, request = request)
    return contact_resource.full_dehydrate(contact_bundle)

class RecordResource(ModelResource):
    survey = fields.OneToOneField(SurveyResource, 'survey')
    ff_audio = fields.OneToOneField(RecordingResource, 'ff_audio', null=True)
    ff_audio_url = fields.CharField( attribute = 'ff_audio_url', null=True)
    ff_value = fields.CharField( attribute = 'ff_value', null=True)
    callerid = fields.CharField(attribute='cdr__callerid')
    survey_name = fields.CharField(attribute='survey__ai__name')
    cdr_id = fields.IntegerField(attribute='cdr__id')
    mc_choice_value = fields.CharField()
    question =  GenericForeignKeyField({
                    Question_mc: QuestionMcResource,
                    Question_ff: QuestionFfResource,
                    Question_qu: QuestionQuResource,
                    Question_df: QuestionDfResource,
                    Question_qna: QuestionQnaResource
                    }, 'question')
    contact = fields.CharField()

    class Meta:
        queryset = Record.objects.all()
        resource_name = "survey_record"
        filtering = {
            'survey': ALL_WITH_RELATIONS,
            'time' : ['lt', 'lte', 'gte', 'gt'],
            }

        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()

    def prepend_urls( self ):
        return [
        url(r"^(?P<resource_name>%s)/cdr_records%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'cdr_records' ), name = "api_cdr_records" ),
        url(r"^(?P<resource_name>%s)/ff_text%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'ff_text' ), name = "api_ff_text" ),
        ]

    def ff_text( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            transcript = request.REQUEST[ 'ff_text' ]
            record_id  = request.REQUEST[ 'record_id' ]
            record = Record.objects.get( id = record_id )
            record.ff_text = transcript
            record.save()
            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'reason': "transcription updated successfully." } )    
        except Exception,e:
            logger.exception('exception updating ff_text: {0}'.format(e))
        return self.create_response( request, { 'success': False, 'reason': "unable to transcription for survey record." } )

    def create_record_bundles( self, request, records, **kwargs ):
        bundles = []
        record_ids = Recording.objects.filter(id__in = [record.ff_audio_id for record in records if record.ff_audio_id]).values_list('id', flat = True)
        for record in records:
            record_ff_audio_id = record.ff_audio_id
            if (not record_ff_audio_id) or (record_ff_audio_id in record_ids):
                bundle_resource = RecordResource()
                bundle = bundle_resource.build_bundle( obj = record, request = request )
                bundles.append( bundle_resource.full_dehydrate( bundle ) )
        return bundles

    def cdr_records( self, request, **kwargs ):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_id = int( request.REQUEST[ 'ai_id' ] )
            survey_id = int( request.REQUEST[ 'survey_id' ] )
            page_no = int( request.REQUEST[ 'page' ] )
            limit = int( request.REQUEST[ 'limit' ] )
            survey = Survey.objects.get( id = survey_id )
            cdr_from = (page_no - 1)*limit
            cdr_to = page_no*limit
            sc_queryset = Survey_cdr.objects.filter( survey = survey ).order_by('-cdr__id')
            cdrs = [ sc.cdr for sc in sc_queryset ][cdr_from: cdr_to]
            questions = get_questions( survey.id )
            labels = get_mc_labels( questions )
            data = get_results_data( survey, questions, [], cdrs, None, labels, None, False )
            bundled_data = []
            for record_dict in data:
                data_dict = {}
                for cdr, records in record_dict.items():                    
                    data_dict['summary'] = [ cdr.id, str(cdr.answered_time if cdr.answered_time else cdr.start_time), 
                                             str(cdr.callerid), len(records) ]
                    data_dict['detail'] = self.create_record_bundles( request, records )
                    data_dict['contact'] = get_contact_for_user_callerid(request.user, cdr.callerid, request).data
                bundled_data.append( data_dict )
            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'data': bundled_data, 'count': sc_queryset.count() } )            
        except Exception,e:
            logger.exception('exception in cdr_grouped_records:%s' %(str(e)))
        return self.create_response( request, { 'success': False, 'reason': "unable to get cdr group survey records." } )

    def dehydrate_contact(self, bundle):
        return get_contact_for_user_callerid(bundle.request.user, bundle.obj.cdr.callerid, bundle.request)

    def dehydrate_mc_choice_value(self, bundle):
        if bundle.obj.mc_choice:
            mc_choice_value = bundle.obj.question.choices.split(",")[bundle.obj.mc_choice - 1]
            return mc_choice_value

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(RecordResource, self).build_filters( filters )
        if "ai" in filters:
            orm_filters[ "survey__ai" ] = filters[ "ai" ]
        return orm_filters



class SurveyStatsResource(Resource):
    class Meta:
        resource_name = 'survey_stats'
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [            
            url(r"^(?P<resource_name>%s)/detailed_results%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_detailed_results' ), name = "api_detailed_results" ),
            url(r"^(?P<resource_name>%s)/ans_distrib%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_answered_ques_distribution' ), name = "api_ans_distrib" ),
            url(r"^(?P<resource_name>%s)/get_questions%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_questions' ), name = "api_get_questions" ),
            url(r"^(?P<resource_name>%s)/mcq_results%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_mcq_results' ), name = "api_mcq_results" ),
            url(r"^(?P<resource_name>%s)/qu_results%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_qu_results' ), name = "api_qu_results" ),
        ]
        
    def get_detailed_results(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            survey_id = request.REQUEST.get('survey_id')
            if not survey_id:
                ai_id = request.REQUEST['ai_id']
                survey = Survey.objects.get( ai_id = ai_id, is_active = True ) 
            else:
                survey = Survey.objects.get( pk = survey_id)
                ai_id = survey.ai.id

            start_datetime = request.REQUEST.get('start_date')
            end_datetime = request.REQUEST.get('end_date')
            if start_datetime and end_datetime:
                start_datetime = datestr_to_datetime(start_datetime)
                end_datetime = datestr_to_datetime(end_datetime)
                if not (start_datetime and end_datetime):
                    logger.error( "Error while converting input date params to datetime for survey_id: " % survey.id )
                    return self.create_response( request, { 'error': "unable to get detailed survey results" } )

            min_ans = request.REQUEST.get('min_ans')
            min_ans = int(min_ans) if min_ans else min_ans

            one_row_per_callerid = request.REQUEST.get('one_row_per_caller')
            callerid_result_selection_policy = request.REQUEST.get('select_policy')
            logger.info('Surve testing.................')
            logger.info('survey cdr extracting')
            data = get_cdrs_for_survey( ai_id, one_row_per_callerid, callerid_result_selection_policy, start = start_datetime, end = end_datetime, survey = survey )
            logger.info('completed')
            if isinstance(data, types.GeneratorType):
                data = get_ordered_distinct_list(data)
            logger.info('get formateed result')
            formatted_results = get_detailed_stats( ai_id, start_datetime, end_datetime, None, min_ans, cdrs = data, survey_id = survey.id )
	    logger.info('formatted result completed')
            formatted_summary = get_summary_stats( ai_id, data, survey_id = survey.id )
            logger.info('summary pull')
 
            output = [ { 'name': 'Survey Results', 'data': formatted_results }, { 'name': 'Summary', 'data': formatted_summary } ]

            if request.REQUEST.get('format') == 'xls':
                ai_name = App_instance.objects.get( pk = ai_id ).name
                filename = '%s_%s_report.xlsx' % ( ai_id, ai_name )

                if request.REQUEST.get('download') == 'true' :
                    workbook = generate_workbook( output, in_memory = True )
                    response = HttpResponse( workbook.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' )
                    response['Content-Disposition'] = 'attachment; filename = %s' % filename                    
                    return response

            self.log_throttled_access( request )
            return self.create_response( request, { "stats": output } )
        except:
            logger.exception( "Exception while running: get_detailed_results: %s" % request )
        return self.create_response( request, { 'error': "unable to get detailed survey results" } )
       
    
    def get_answered_ques_distribution(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST['ai_id']
            start_datetime = request.REQUEST.get('start_date', None)
            end_datetime = request.REQUEST.get('end_date', None)
            if start_datetime and end_datetime:
                start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
                end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d')

            answer_distrib = get_answered_ques_distrib(ai_id, start_datetime, end_datetime)

            self.log_throttled_access( request )
            return self.create_response( request, { "answer_distribution": answer_distrib } )
        except:
            logger.exception( "Exception while running: get_answered_ques_distribution: %s" % request )
        return self.create_response( request, { 'error': "unable to get answered questions distribution for survey" } )


    def get_questions(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            survey_id = request.REQUEST.get('survey_id', None)
            if not survey_id:
                ai_id = request.REQUEST[ 'ai_id' ]
                survey_id = Survey.objects.get(ai_id = ai_id, is_active = True).id
            questions = []
            for question in get_questions(survey_id):
                if isinstance(question, Question_ff):
                    continue
                if isinstance(question, Question_mc):
                    type = 'mcq'
                elif isinstance(question, Question_qu):
                    type = 'qu'

                questions.append( { 'id': question.id, 'order': question.order, 'text': question.text, 'type': type } )

            self.log_throttled_access( request )
            return self.create_response( request, { "questions": questions } )
        except:
            logger.exception( "Exception while running: get_questions: %s" % request )
        return self.create_response( request, { 'error': "unable to get questions for survey" } )


    def get_mcq_results(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ques_id = request.REQUEST[ 'q_id' ]
            choices, counts = get_mc_results(ques_id)
            total_count = sum(counts)
            question = Question_mc.objects.get( pk = ques_id )
            labels = dict(enumerate(question.choices.split(',')))
            results = []
            for index, value in enumerate(choices):
                choice_str = '%s - (%s%%) - %s' % (labels[int(value)-1], int((counts[index]/float(total_count))*100), counts[index])
                results.append({'choice': choice_str, 'count': counts[index]})

            self.log_throttled_access( request )
            return self.create_response( request, { "results": results } )
        except:
            logger.exception( "Exception while running: get_mcq_results: %s" % request )
        return self.create_response( request, { 'error': "unable to get mcq results for survey" } )


    def get_qu_results(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ques_id = request.REQUEST[ 'q_id' ]
            results = []
            bin_size = get_bin_size(ques_id, bin_count = 10)
            bins, counts = get_qu_results(ques_id, bin_size)

            for each in range(len(bins)):
                results.append({ 'bin': bins[each], 'count': counts[each] })

            self.log_throttled_access( request )
            return self.create_response( request, { "results": results } )
        except:
            logger.exception( "Exception while running: get_qu_results: %s" % request )
        return self.create_response( request, { 'error': "unable to get qu results for survey" } )


class FormQuestionResource(ModelResource):
    
    form = fields.ForeignKey( FormResource, 'form', full = True)
    
    question = GenericForeignKeyField({
        Question_ff: QuestionFfResource,
        Question_qu: QuestionQuResource,
        Question_mc: QuestionMcResource,
        Question_df: QuestionDfResource,
        Question_qna: QuestionQnaResource
        }, 'question', full = True)
    
    class Meta:
        queryset = Form_question.objects.all()
        resource_name = 'form_question'
        filtering = {
                    'form': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        
    def prepend_urls( self ):
        return [
            url(r"^(?P<resource_name>%s)/reorder_questions%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'reorder_questions' ), name = "api_reorder_questions" ),
            url(r"^(?P<resource_name>%s)/add%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'add' ), name = "api_add" ),
            url(r"^(?P<resource_name>%s)/delete%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'delete' ), name = "api_delete" ),
        ]

    def reorder_questions( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:
            form_id = request.REQUEST[ 'form_id' ]
            source  = int( request.REQUEST[ 'source' ] )
            target  = int( request.REQUEST[ 'target' ] )
            reorder_questions_impl( form_id, source, target )
            
            self.log_throttled_access( request )

            return self.create_response( request, { 'success': True, 'message': "reorder completed" } )
        except Exception, e:
            logger.exception( "exception in reorder_questions: %s" %(str(e)) )
        return self.create_response( request, { 'success': False, 'error': "unable to reorder questions" } )

    def delete( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:
            fq_id = int( request.REQUEST['fq_id'] )
            form_question = Form_question.objects.get( id = fq_id )
            question = form_question.content_type.get_object_for_this_type( id = form_question.object_id )
            delete_question_impl( question )            

            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'reason': "question successfully deleted" } )
        except Exception,e:
            logger.exception( "exception in delete_question: %s" %(str(e)) )
        return self.create_response( request, { 'success': False, 'error': "unable to delete question." } )

    def get_question_object( self, question_type, question_options, question = None ):

        if question_type == 'qu':
            question = Question_qu() if not question else question
            question.max_digits = question_options
        elif question_type == 'mc':
            question = Question_mc() if not question else question
            question.choices = question_options
        elif question_type == 'ff':
            question = Question_ff() if not question else question
            question.max_duration = question_options
        elif question_type == 'df':
            question = Question_df() if not question else question
            question.max_duration = question_options
        elif question_type == 'qna':
            question = Question_qna() if not question else question
            question.max_duration = question_options    

        return question


    def add( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:
            form_id = int( request.REQUEST[ 'form_id' ] )
            validate = ( request.REQUEST.has_key('validate') and request.REQUEST[ 'validate' ] == 'true' )
            question_type = request.REQUEST[ 'type' ]
            question_title  = request.REQUEST[ 'title' ]
            question_options  = request.REQUEST[ 'options' ]
            fq_id = int( request.REQUEST['fq_id'] ) if request.REQUEST.has_key('fq_id') else None
            order = int( request.REQUEST['order'] ) if request.REQUEST.has_key('order') else ( Form_question.objects.filter(form = form_id).count() + 1 )

            form = Form.objects.get( id = form_id )
            question = None
            form_question = None

            if fq_id:
                form_question = Form_question.objects.get( id = fq_id )
                question = form_question.content_type.get_object_for_this_type( id = form_question.object_id )
                if question.type() != question_type:
                    question = None                    
                    fq_id = None
                    form_question = form_question.delete()
                question = self.get_question_object( question_type, question_options, question )
            else:
                question = self.get_question_object( question_type, question_options )

            if not question:
                raise Exception('Unknown question type:%s' (str(question_type)) )

            question.order = order
            question.text = question_title
            question.validate = validate
            
            if not fq_id:
                prompt_name = ( 'survey_question_num_%s' %( str(order) ) )
                try:
                    prompt_info = Prompt_info.objects.get( name = prompt_name )
                except ObjectDoesNotExist:
                    prompt_info = Prompt_info.objects.create( name = prompt_name, description = prompt_name, text = prompt_name )
                except MultipleObjectsReturned:
                    prompt_info = Prompt_info.objects.filter( name = prompt_name )[0]

                try:
                    prompt_audio = Prompt_audio.objects.get( prompt_set = form.prompt_set, info = prompt_info )
                except ObjectDoesNotExist:
                    prompt_audio = Prompt_audio.objects.create( prompt_set = form.prompt_set, info = prompt_info )
                except MultipleObjectsReturned:
                    prompt_audio = Prompt_audio.objects.filter( prompt_set = form.prompt_set, info = prompt_info )[0]

                question.prompt_audio = prompt_audio


                pre_prompt_name = ( 'survey_pre_question_num_%s' %( str(order) ) )
                try:
                    prompt_info = Prompt_info.objects.get( name = pre_prompt_name )
                except ObjectDoesNotExist:
                    prompt_info = Prompt_info.objects.create( name = pre_prompt_name, description = pre_prompt_name, text = pre_prompt_name )
                except MultipleObjectsReturned:
                    prompt_info = Prompt_info.objects.filter( name = pre_prompt_name )[0]

                try:
                    pre_prompt_audio = Prompt_audio.objects.get( prompt_set = form.prompt_set, info = prompt_info )
                except ObjectDoesNotExist:
                    pre_prompt_audio = Prompt_audio.objects.create( prompt_set = form.prompt_set, info = prompt_info )
                except MultipleObjectsReturned:
                    pre_prompt_audio = Prompt_audio.objects.filter( prompt_set = form.prompt_set, info = prompt_info )[0]

                question.pre_prompt_audio = pre_prompt_audio
            
            question.save()
            
            if not form_question:
                content_type = ContentType.objects.get_for_model( question.__class__ )
                form_question = Form_question(form_id = form_id, content_type = content_type, object_id = question.id)
                form_question.save()
                
            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'reason': "new question created" } )
        except Exception, e:
            logger.exception( "exception in add_questions: %s" %(str(e)) )
        return self.create_response( request, { 'success': False, 'reason': "unable to add question" } )


    def build_filters(self, filters = None):
        if filters is None:
            filters = {}

        orm_filters = super(FormQuestionResource, self).build_filters( filters )
        if "form_id" in filters:
            orm_filters[ "form" ] = filters[ "form_id" ]
        return orm_filters

    def apply_filters( self,  request, applicable_filters ):
        semi_filtered = super(FormQuestionResource, self).apply_filters( request, applicable_filters )
        semi_filtered = list(semi_filtered)
        semi_filtered.sort( key = lambda x: x.question.order )
        return semi_filtered

class AssessmentResource( ModelResource ):
    class Meta:
        queryset = Assessment.objects.all()
        resource_name = 'survey_assessment'

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def prepend_urls( self ):
        return [
            url(r"^(?P<resource_name>%s)/results%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'results' ), name = "api_results" ),
            url(r"^(?P<resource_name>%s)/result_download%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'result_download' ), name = "api_result_download" ),
        ] 

    @apply_default_checks
    def results( self, request, **kwargs ):
        survey_id = int( kwargs[ 'survey_id' ] )
        ai_id = int( kwargs[ 'location_ai_id' ] )
        location_ids = kwargs.get('village_ids', None)

        location_filters = {
            'ai_id': ai_id
        }

        if not location_ids:
            if kwargs.get('panchayat_ids', None):
                location_filters[ 'panchayat__in' ] = kwargs[ 'panchayat_ids' ].split(",")
            elif kwargs.get('block_ids', None):
                location_filters[ 'block__in' ] = kwargs[ 'block_ids' ].split(",")
            elif kwargs.get('district_ids', None):
                location_filters[ 'district__in' ] = kwargs[ 'district_ids' ].split(",")
            else:
                location_filters['state__in'] = kwargs[ 'state_ids' ].split(",") 
        else:
            location_ids = location_ids.split( "," )
            compared_locations = Location.objects.filter(pk__in = location_ids ).values('state', 'district', 'block', 'panchayat', 'village')
            for location in compared_locations:
                for field, value in location.items():
                    location_filters.setdefault( field + "__in", []).append(value)

        start_datetime = kwargs.get('start_date', None)
        end_datetime = kwargs.get('end_date', None)
        filter_complete_surveys = kwargs.get('filter_complete_surveys', True)

        locations = Location.objects.filter( **location_filters )

        if start_datetime and end_datetime:
            start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
            end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d') + timedelta( days = 1 )

        response = {}
        response_objects = []

        try:
            survey_questions_count = Form_question.objects.filter(form__survey = survey_id).count()
            survey_weightage = Benchmark.get_survey_weightage( survey_id )

            location_contacts = Contact.objects.filter( location_fk__in = locations, contact_list__contact_list_entity__content_type = user_type, contact_list__contact_list_entity__object_id = request.user.id)
            location_callerids = location_contacts.values_list( 'number', flat = True ).distinct()
            
            cdrs = get_cdrs_from_survey_cdrs( survey_id, start = start_datetime, end = end_datetime, callerids = location_callerids )
           
            response_object = {}
            response_object[ 'survey_id' ] = survey_id
            response_object[ 'survey_weightage' ] = survey_weightage
            response_object[ 'assessments' ] = []
    
            for cdr in cdrs:
                assessment_obj = {}
                records = Record.objects.filter( cdr = cdr, survey = survey_id ).exclude( mc_choice__isnull = True, qu_value__isnull = True, ff_audio__isnull = True )
                assessments = Assessment.objects.none()
                if (filter_complete_surveys and records.count() == survey_questions_count) or not filter_complete_surveys :
                    assessments = Assessment.objects.filter( record__in = records )

                if assessments.exists():
                    assessment_agg = assessments.aggregate( answer_weightage = Sum('result') ) 
                    assessment_obj[ 'callerid' ] = cdr.callerid
                    assessment_obj[ 'location_fk' ] = location_contacts.filter( number = cdr.callerid ).latest('id').location_fk.id
                    assessment_obj[ 'sum_answer_weightage' ] = assessment_agg[ 'answer_weightage' ]
                    response_object[ 'assessments' ].append( assessment_obj )
                    
            response_objects.append( response_object )
            response[ 'objects' ] = response_objects
            return self.create_response( request, response )
        except Exception, e:
            logger.exception( "exception in assessment_results: %s" %(str(e)) )
        return self.create_response( request, { 'success': False, 'reason': "unable to fetch results" } )

    @apply_default_checks
    def result_download( self, request, **kwargs  ):
        project_id = kwargs[ 'project_id' ]
        survey_ids = kwargs.get( 'survey_ids', None)
        start_datetime = kwargs.get('start_date', None)
        end_datetime = kwargs.get('end_date', None)

        if start_datetime and end_datetime:
            start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
            end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d') + timedelta( days = 1 )
        project = Project.objects.get( pk = project_id )
        
        if survey_ids:
            survey_ids = survey_ids.split(",")
            surveys = Survey.objects.filter( pk__in = survey_ids, is_active = True )
        else:
            survey_ais = [ proj_ai.id for proj_ai in project.ais.all() if proj_ai.app_id == 1 ]
            surveys = Survey.objects.filter(ai__in = survey_ais, is_active = True )

        try:
            locations = Location.objects.filter( ai__survey__in = surveys )
            location_contacts = Contact.objects.filter( location_fk__in = locations, contact_list__contact_list_entity__content_type = user_type, contact_list__contact_list_entity__object_id = request.user.id)
            queried_locations = location_contacts.values_list( 'location_fk', flat = True ).distinct()

            village_data = []
            header = [ 'State', 'District', 'Block', 'Village' ]
            location_header_len = len( header )

            for survey in surveys:
                header.extend( [ str(survey.name) + " - ( Percentage )" , str(survey.name) + " - ( number of responses )" ] )

            village_data.append( header )

            for location in queried_locations:
                row = []
                location = Location.objects.get( pk = location )
                row.extend( [ location.state.name, location.district.name, location.block.name, location.village ] ) 
                for survey in surveys:
                    survey_id = survey.id
                    assessment_aggregates = []
                    location_callerids =  location_contacts.filter(location_fk = location).values_list( 'number', flat = True  ).distinct()                     
                    survey_weightage = Benchmark.get_survey_weightage( survey_id )
                    cdrs = get_cdrs_from_survey_cdrs( survey_id, start = start_datetime, end = end_datetime, callerids = location_callerids)
                    survey_questions_count = Form_question.objects.filter(form__survey = survey_id).count()
                    for cdr in cdrs:
                        records = Record.objects.filter( cdr = cdr, survey = survey_id ).exclude( mc_choice__isnull = True, qu_value__isnull = True, ff_audio__isnull = True )
                        assessments = Assessment.objects.none()
                        if records.count() == survey_questions_count:
                            assessments = Assessment.objects.filter( record__in = records )
                        if assessments.exists():
                            assessment_agg = assessments.aggregate( answer_weightage = Sum('result') ) 
                            assessment_aggregates.append( assessment_agg[ 'answer_weightage' ] )
                    if assessment_aggregates:
                        row.extend( [(sum(assessment_aggregates) * 100) / (float(len(assessment_aggregates)) * survey_weightage), len(assessment_aggregates)] )
                    else:
                        row.extend( ["-", "-"] )
                village_data.append(row)

            filename = "assesment_results_" + project.name + ".xls"
            apply_format_to = list( string.ascii_uppercase )[ location_header_len : len(village_data[0]) : 2 ]
            data = [{ 'name': "Village-wise", 'header_row_count': 1, 'data': village_data, 'apply_format_to': apply_format_to}]
            conditional_formats = check_and_get_excel_format( int(project_id) )
            output = generate_workbook( data, in_memory = True, options = conditional_formats )
            response = HttpResponse( output.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' )
            response['Content-Disposition'] = 'attachment; filename = %s' % filename
            return response
        except Exception, e:
            logger.exception( "exception in add_questions: %s" %(str(e)) )
        return self.create_response( request, { 'success': False, 'reason': "unable to fetch results" } )
