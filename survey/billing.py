from vapp.billing.utils import get_common_billing_infos, remove_exclusion_duplicates

def get_billing_info(startdate, enddate, ai, composite = True, unit_size = 60, excludes = []):
    infos = get_common_billing_infos(startdate, enddate, ai, composite, unit_size, excludes)
    remove_exclusion_duplicates(infos)

    return infos
