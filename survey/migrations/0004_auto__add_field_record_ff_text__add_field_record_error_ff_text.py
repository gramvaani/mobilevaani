# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Record.ff_text'
        db.add_column('survey_record', 'ff_text', self.gf('django.db.models.fields.CharField')(default='None', max_length=255), keep_default=False)

        # Adding field 'Record_error.ff_text'
        db.add_column('survey_record_error', 'ff_text', self.gf('django.db.models.fields.CharField')(default='None', max_length=255), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Record.ff_text'
        db.delete_column('survey_record', 'ff_text')

        # Deleting field 'Record_error.ff_text'
        db.delete_column('survey_record_error', 'ff_text')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'media.prompt_audio': {
            'Meta': {'object_name': 'Prompt_audio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_info']"}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_set']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'media.prompt_info': {
            'Meta': {'object_name': 'Prompt_info'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.form': {
            'Meta': {'object_name': 'Form'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_frozen': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_set']"})
        },
        'survey.form_entity': {
            'Meta': {'object_name': 'Form_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'survey.form_question': {
            'Meta': {'object_name': 'Form_question'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'survey.question_ff': {
            'Meta': {'object_name': 'Question_ff'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.question_mc': {
            'Meta': {'object_name': 'Question_mc'},
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_choices': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.question_qu': {
            'Meta': {'object_name': 'Question_qu'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.record': {
            'Meta': {'object_name': 'Record'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'ff_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True'}),
            'ff_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mc_choice': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'qu_value': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'question_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.record_error': {
            'Meta': {'object_name': 'Record_error'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'ff_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True'}),
            'ff_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mc_choice': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'qu_value': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'question_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.status_history': {
            'Meta': {'object_name': 'Status_history'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.survey': {
            'Meta': {'object_name': 'Survey'},
            'ai': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app_manager.App_instance']", 'unique': 'True'}),
            'form': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['survey.Form']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['survey']
