# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting model 'Survey_question'
        db.delete_table('survey_survey_question')

        # Deleting model 'Question'
        db.delete_table('survey_question')

        # Deleting model 'Q_ffa'
        db.delete_table('survey_q_ffa')

        # Deleting model 'Q_mcq'
        db.delete_table('survey_q_mcq')

        # Deleting model 'Survey'
        db.delete_table('survey_survey')

        # Adding model 'Question_mc'
        db.create_table('survey_question_mc', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True)),
            ('validation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('num_choices', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('survey', ['Question_mc'])

        # Adding model 'Form'
        db.create_table('survey_form', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('prompt_set', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_set'])),
        ))
        db.send_create_signal('survey', ['Form'])

        # Adding model 'Form_question'
        db.create_table('survey_form_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('survey', ['Form_question'])

        # Adding model 'Question_qu'
        db.create_table('survey_question_qu', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True)),
            ('validation', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('survey', ['Question_qu'])

        # Adding model 'Question_ff'
        db.create_table('survey_question_ff', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True)),
            ('validation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('max_duration', self.gf('django.db.models.fields.PositiveIntegerField')(default=120)),
        ))
        db.send_create_signal('survey', ['Question_ff'])

        # Adding model 'Form_entity'
        db.create_table('survey_form_entity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('perm', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('survey', ['Form_entity'])


    def backwards(self, orm):
        
        # Adding model 'Survey_question'
        db.create_table('survey_survey_question', (
            ('q', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Question'])),
            ('index', self.gf('django.db.models.fields.IntegerField')()),
            ('s', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('survey', ['Survey_question'])

        # Adding model 'Question'
        db.create_table('survey_question', (
            ('text', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('survey', ['Question'])

        # Adding model 'Q_ffa'
        db.create_table('survey_q_ffa', (
            ('q', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Question'])),
            ('afile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'])),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('survey', ['Q_ffa'])

        # Adding model 'Q_mcq'
        db.create_table('survey_q_mcq', (
            ('count', self.gf('django.db.models.fields.IntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('choice', self.gf('django.db.models.fields.IntegerField')()),
            ('q', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Question'])),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('survey', ['Q_mcq'])

        # Adding model 'Survey'
        db.create_table('survey_survey', (
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('count', self.gf('django.db.models.fields.IntegerField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('survey', ['Survey'])

        # Deleting model 'Question_mc'
        db.delete_table('survey_question_mc')

        # Deleting model 'Form'
        db.delete_table('survey_form')

        # Deleting model 'Form_question'
        db.delete_table('survey_form_question')

        # Deleting model 'Question_qu'
        db.delete_table('survey_question_qu')

        # Deleting model 'Question_ff'
        db.delete_table('survey_question_ff')

        # Deleting model 'Form_entity'
        db.delete_table('survey_form_entity')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.form': {
            'Meta': {'object_name': 'Form'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_set']"})
        },
        'survey.form_entity': {
            'Meta': {'object_name': 'Form_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'survey.form_question': {
            'Meta': {'object_name': 'Form_question'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'survey.question_ff': {
            'Meta': {'object_name': 'Question_ff'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.question_mc': {
            'Meta': {'object_name': 'Question_mc'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_choices': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.question_qu': {
            'Meta': {'object_name': 'Question_qu'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validation': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['survey']
