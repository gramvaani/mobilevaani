# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Status_history'
        db.create_table('survey_status_history', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('survey', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('survey', ['Status_history'])

        # Adding model 'Record'
        db.create_table('survey_record', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('survey', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('question_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('mc_choice', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('qu_value', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('ff_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True)),
        ))
        db.send_create_signal('survey', ['Record'])

        # Adding model 'Record_error'
        db.create_table('survey_record_error', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('survey', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('question_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('mc_choice', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('qu_value', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('ff_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True)),
        ))
        db.send_create_signal('survey', ['Record_error'])

        # Adding model 'Survey'
        db.create_table('survey_survey', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app_manager.App_instance'], unique=True)),
            ('form', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['survey.Form'], unique=True, null=True, blank=True)),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('survey', ['Survey'])

        # Deleting field 'Question_mc.recording'
        db.delete_column('survey_question_mc', 'recording_id')

        # Deleting field 'Question_mc.validation'
        db.delete_column('survey_question_mc', 'validation')

        # Adding field 'Question_mc.prompt_audio'
        db.add_column('survey_question_mc', 'prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_audio'], null=True, blank=True), keep_default=False)

        # Adding field 'Question_mc.validate'
        db.add_column('survey_question_mc', 'validate', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Question_mc.choices'
        db.add_column('survey_question_mc', 'choices', self.gf('django.db.models.fields.CharField')(default=None, max_length=255), keep_default=False)

        # Deleting field 'Question_ff.recording'
        db.delete_column('survey_question_ff', 'recording_id')

        # Deleting field 'Question_ff.validation'
        db.delete_column('survey_question_ff', 'validation')

        # Adding field 'Question_ff.prompt_audio'
        db.add_column('survey_question_ff', 'prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_audio'], null=True, blank=True), keep_default=False)

        # Adding field 'Question_ff.validate'
        db.add_column('survey_question_ff', 'validate', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Deleting field 'Question_qu.recording'
        db.delete_column('survey_question_qu', 'recording_id')

        # Deleting field 'Question_qu.validation'
        db.delete_column('survey_question_qu', 'validation')

        # Adding field 'Question_qu.prompt_audio'
        db.add_column('survey_question_qu', 'prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_audio'], null=True, blank=True), keep_default=False)

        # Adding field 'Question_qu.validate'
        db.add_column('survey_question_qu', 'validate', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Form.is_frozen'
        db.add_column('survey_form', 'is_frozen', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)


    def backwards(self, orm):
        
        # Deleting model 'Status_history'
        db.delete_table('survey_status_history')

        # Deleting model 'Record'
        db.delete_table('survey_record')

        # Deleting model 'Record_error'
        db.delete_table('survey_record_error')

        # Deleting model 'Survey'
        db.delete_table('survey_survey')

        # Adding field 'Question_mc.recording'
        db.add_column('survey_question_mc', 'recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True), keep_default=False)

        # Adding field 'Question_mc.validation'
        db.add_column('survey_question_mc', 'validation', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Deleting field 'Question_mc.prompt_audio'
        db.delete_column('survey_question_mc', 'prompt_audio_id')

        # Deleting field 'Question_mc.validate'
        db.delete_column('survey_question_mc', 'validate')

        # Deleting field 'Question_mc.choices'
        db.delete_column('survey_question_mc', 'choices')

        # Adding field 'Question_ff.recording'
        db.add_column('survey_question_ff', 'recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True), keep_default=False)

        # Adding field 'Question_ff.validation'
        db.add_column('survey_question_ff', 'validation', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Deleting field 'Question_ff.prompt_audio'
        db.delete_column('survey_question_ff', 'prompt_audio_id')

        # Deleting field 'Question_ff.validate'
        db.delete_column('survey_question_ff', 'validate')

        # Adding field 'Question_qu.recording'
        db.add_column('survey_question_qu', 'recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True), keep_default=False)

        # Adding field 'Question_qu.validation'
        db.add_column('survey_question_qu', 'validation', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Deleting field 'Question_qu.prompt_audio'
        db.delete_column('survey_question_qu', 'prompt_audio_id')

        # Deleting field 'Question_qu.validate'
        db.delete_column('survey_question_qu', 'validate')

        # Deleting field 'Form.is_frozen'
        db.delete_column('survey_form', 'is_frozen')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'media.prompt_audio': {
            'Meta': {'object_name': 'Prompt_audio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_info']"}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_set']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'media.prompt_info': {
            'Meta': {'object_name': 'Prompt_info'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.form': {
            'Meta': {'object_name': 'Form'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_frozen': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_set']"})
        },
        'survey.form_entity': {
            'Meta': {'object_name': 'Form_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'survey.form_question': {
            'Meta': {'object_name': 'Form_question'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'survey.question_ff': {
            'Meta': {'object_name': 'Question_ff'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.question_mc': {
            'Meta': {'object_name': 'Question_mc'},
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_choices': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.question_qu': {
            'Meta': {'object_name': 'Question_qu'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'survey.record': {
            'Meta': {'object_name': 'Record'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'ff_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mc_choice': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'qu_value': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'question_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.record_error': {
            'Meta': {'object_name': 'Record_error'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'ff_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Form']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mc_choice': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'qu_value': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'question_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.status_history': {
            'Meta': {'object_name': 'Status_history'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.survey': {
            'Meta': {'object_name': 'Survey'},
            'ai': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app_manager.App_instance']", 'unique': 'True'}),
            'form': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['survey.Form']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['survey']
