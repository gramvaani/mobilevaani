# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Form'
        db.create_table(u'survey_form', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('is_frozen', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('prompt_set', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_set'])),
        ))
        db.send_create_signal(u'survey', ['Form'])

        # Adding model 'Question_mc'
        db.create_table(u'survey_question_mc', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_audio'], null=True, blank=True)),
            ('validate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('num_choices', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('choices', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'survey', ['Question_mc'])

        # Adding model 'Question_qu'
        db.create_table(u'survey_question_qu', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_audio'], null=True, blank=True)),
            ('validate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('max_digits', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=9)),
        ))
        db.send_create_signal(u'survey', ['Question_qu'])

        # Adding model 'Question_ff'
        db.create_table(u'survey_question_ff', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_audio'], null=True, blank=True)),
            ('validate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('max_duration', self.gf('django.db.models.fields.PositiveIntegerField')(default=120)),
        ))
        db.send_create_signal(u'survey', ['Question_ff'])

        # Adding model 'Form_question'
        db.create_table(u'survey_form_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'survey', ['Form_question'])

        # Adding model 'Form_entity'
        db.create_table(u'survey_form_entity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('perm', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'survey', ['Form_entity'])

        # Adding model 'Survey'
        db.create_table(u'survey_survey', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app_manager.App_instance'], unique=True)),
            ('form', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['survey.Form'], unique=True, null=True, blank=True)),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'survey', ['Survey'])

        # Adding model 'Status_history'
        db.create_table(u'survey_status_history', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('survey', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'survey', ['Status_history'])

        # Adding model 'Record'
        db.create_table(u'survey_record', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('survey', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('question_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('mc_choice', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('qu_value', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('ff_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True)),
            ('ff_text', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'survey', ['Record'])

        # Adding model 'Record_error'
        db.create_table(u'survey_record_error', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('survey', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Form'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('question_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('mc_choice', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('qu_value', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('ff_audio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True)),
            ('ff_text', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'survey', ['Record_error'])

        # Adding model 'Report_creds'
        db.create_table(u'survey_report_creds', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('cred_key', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('order', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'survey', ['Report_creds'])

        # Adding model 'Report_log'
        db.create_table(u'survey_report_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Report_creds'])),
            ('report', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('time', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('error_code', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
        ))
        db.send_create_signal(u'survey', ['Report_log'])


    def backwards(self, orm):
        # Deleting model 'Form'
        db.delete_table(u'survey_form')

        # Deleting model 'Question_mc'
        db.delete_table(u'survey_question_mc')

        # Deleting model 'Question_qu'
        db.delete_table(u'survey_question_qu')

        # Deleting model 'Question_ff'
        db.delete_table(u'survey_question_ff')

        # Deleting model 'Form_question'
        db.delete_table(u'survey_form_question')

        # Deleting model 'Form_entity'
        db.delete_table(u'survey_form_entity')

        # Deleting model 'Survey'
        db.delete_table(u'survey_survey')

        # Deleting model 'Status_history'
        db.delete_table(u'survey_status_history')

        # Deleting model 'Record'
        db.delete_table(u'survey_record')

        # Deleting model 'Record_error'
        db.delete_table(u'survey_record_error')

        # Deleting model 'Report_creds'
        db.delete_table(u'survey_report_creds')

        # Deleting model 'Report_log'
        db.delete_table(u'survey_report_log')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'media.prompt_audio': {
            'Meta': {'object_name': 'Prompt_audio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_info']"}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_set']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'media.prompt_info': {
            'Meta': {'object_name': 'Prompt_info'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'survey.form': {
            'Meta': {'object_name': 'Form'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_frozen': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_set']"})
        },
        u'survey.form_entity': {
            'Meta': {'object_name': 'Form_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'survey.form_question': {
            'Meta': {'object_name': 'Form_question'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'survey.question_ff': {
            'Meta': {'object_name': 'Question_ff'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'survey.question_mc': {
            'Meta': {'object_name': 'Question_mc'},
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_choices': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'survey.question_qu': {
            'Meta': {'object_name': 'Question_qu'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_digits': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '9'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_audio']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'validate': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'survey.record': {
            'Meta': {'object_name': 'Record'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'ff_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True'}),
            'ff_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mc_choice': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'qu_value': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'question_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'survey.record_error': {
            'Meta': {'object_name': 'Record_error'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'ff_audio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True'}),
            'ff_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mc_choice': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'qu_value': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'question_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'survey.report_creds': {
            'Meta': {'object_name': 'Report_creds'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cred_key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'survey.report_log': {
            'Meta': {'object_name': 'Report_log'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Report_creds']"}),
            'error_code': ('django.db.models.fields.SmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'survey.status_history': {
            'Meta': {'object_name': 'Status_history'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'survey': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['survey.Survey']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'survey.survey': {
            'Meta': {'object_name': 'Survey'},
            'ai': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.App_instance']", 'unique': 'True'}),
            'form': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['survey.Form']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['survey']