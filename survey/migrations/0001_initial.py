# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Survey'
        db.create_table('survey_survey', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('count', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('survey', ['Survey'])

        # Adding model 'Question'
        db.create_table('survey_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('survey', ['Question'])

        # Adding model 'Survey_question'
        db.create_table('survey_survey_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('s', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Survey'])),
            ('q', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Question'])),
            ('index', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('survey', ['Survey_question'])

        # Adding model 'Q_mcq'
        db.create_table('survey_q_mcq', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('q', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Question'])),
            ('choice', self.gf('django.db.models.fields.IntegerField')()),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('count', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('survey', ['Q_mcq'])

        # Adding model 'Q_ffa'
        db.create_table('survey_q_ffa', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('q', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['survey.Question'])),
            ('afile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'])),
        ))
        db.send_create_signal('survey', ['Q_ffa'])


    def backwards(self, orm):
        
        # Deleting model 'Survey'
        db.delete_table('survey_survey')

        # Deleting model 'Question'
        db.delete_table('survey_question')

        # Deleting model 'Survey_question'
        db.delete_table('survey_survey_question')

        # Deleting model 'Q_mcq'
        db.delete_table('survey_q_mcq')

        # Deleting model 'Q_ffa'
        db.delete_table('survey_q_ffa')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'survey.q_ffa': {
            'Meta': {'object_name': 'Q_ffa'},
            'afile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'q': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Question']"})
        },
        'survey.q_mcq': {
            'Meta': {'object_name': 'Q_mcq'},
            'choice': ('django.db.models.fields.IntegerField', [], {}),
            'count': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'q': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Question']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'survey.question': {
            'Meta': {'object_name': 'Question'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'survey.survey': {
            'Meta': {'object_name': 'Survey'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'count': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'survey.survey_question': {
            'Meta': {'object_name': 'Survey_question'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.IntegerField', [], {}),
            'q': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Question']"}),
            's': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['survey.Survey']"})
        }
    }

    complete_apps = ['survey']
