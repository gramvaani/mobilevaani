from models import Record, Survey, Survey_cdr
from survey.stats import get_questions, get_answered_ques_count
from log import get_logger
from customization.models import Atm, Alert_policy, Atm_status_transition_log
from customization.tasks import GetAtmInfo
from sms.models import SMS_message_received
from datetime import datetime
logger = get_logger()

def at_least_one_answered( ai, cdr, *args, **kwargs):
    try:
        survey = Survey.objects.get( ai = ai, is_active = True )
        records = Record.objects.raw('select id from survey_record where survey_id = %s \
                                        and cdr_id = %s and not (ff_audio_id is null and qu_value is null and mc_choice is null)' \
                                        %(survey.id, cdr.id) )
        records = [ record for record in records ]
        return len( records ) > 0
    except Exception,e:
        return False


def is_all_answered( ai, cdr, *args, **kwargs ):
    survey = Survey.objects.get( ai = ai, is_active = True )
    ques_count = len( get_questions(survey.id) )
    survey_cdr = Survey_cdr.objects.get( survey = survey, cdr = cdr )
    answered_ques_count = get_answered_ques_count(survey_cdr)
    return (answered_ques_count == ques_count)

def has_caller_opted_out(ai, cdr, *args, **kwargs):
    try:
        survey = Survey.objects.get(ai = ai, is_active = True)
        records = Record.objects.filter(survey = survey, cdr = cdr)
        if (len(get_questions(survey.id)) == records.count()) and (records.latest('id').mc_choice == 1):
            return True
        return False
    except Exception, e:
        return False

def is_valid_atm_code(ai, cdr, *args, **kwargs):
    try:
        survey_id = Survey.objects.get(ai_id=ai.id, is_active=True).id
        records = Record.objects.filter(survey_id=survey_id, cdr_id=cdr.id)
        if Atm.objects.filter(code=int(records.latest('id').qu_value)).exists():
            return True
        return False
    except Exception, e:
        return False

def update_atm_status(ai, cdr, *args, **kwargs):
    try:
        survey_id = Survey.objects.get(ai_id=ai.id, is_active=True).id
        records = Record.objects.filter(survey_id=survey_id, cdr_id=cdr.id, mc_choice__isnull=False)
        if (len(get_questions(survey_id)) == records.count()) and (records.latest('id').mc_choice == 1):
            atm_code = int(Record.objects.filter(cdr_id=cdr.id).exclude(survey_id=survey_id).latest('id').qu_value)
            atm = Atm.objects.get(code=atm_code)
            queue_size = records[0].mc_choice
            cash_dispense = records[1].mc_choice
            cash_deposit =records[2].mc_choice
            denominations = records[3].mc_choice
            prev_status = atm.status
            new_status = '{0}{1}{2}{3}'.format(queue_size, cash_dispense, cash_deposit, denominations)
            Atm_status_transition_log(cdr_id=cdr.id, atm_id=atm.id, new_status=new_status, prev_status=prev_status).save()
            atm.status = new_status
            atm.save()
            update_notif = False
            for each in Alert_policy.objects.all():
                for i in range(4):
                    if (each.prev_status[i] == '0' or prev_status[i] == each.prev_status[i]) and (each.curr_status[i] == '0' or new_status[i] == each.curr_status[i]):
                        update_notif = True
                    else:
                        update_notif = False
                        break
                if update_notif:
                    atm.send_update_notifs(ai.id, cdr.id)
                    break
            return True
    except Exception, e:
        logger.exception("exception while running processing method update_atm_status: %s" %(e))
        return False

def generate_atm_info(ai, cdr, *args, **kwargs):
    try:
        survey_id = Survey.objects.get(ai_id=ai.id,
                                       is_active=True).id
        record = Record.objects.filter(survey_id=survey_id,
                                       cdr_id=cdr.id,
                                       qu_value__isnull=False).latest('id')
        pin_code = record.qu_value
        query = SMS_message_received(sender=cdr.callerid,
                                     time=record.time,
                                     message=str(pin_code))
        query.save()
        eta = datetime.now()
        GetAtmInfo.apply_async([query.id, query.message, query.sender, 'ivr'], eta=eta)
        return True
    except Exception, e:
        return False