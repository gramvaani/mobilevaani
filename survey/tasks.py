from app_manager.models import *
from survey.models import *
from survey.stats import get_answered_ques_count
# It may be required to move Event_sms_template to app_manager
from mnews.models import Event_sms_template, Groups_call_log
from app_selector.models import Choices
from vapp.stats.stats import get_call_duration


from sms.tasks import SMSTask

from celery.task import Task
import suds, urllib2
from telephony import utils

from vapp.utils import get_qna_model_result, transcribe_streaming, upload_a_file_tg_group, generate_attachment, send_email, get_df_intent_from_stream
from mnews.tasks import CallAndPlayTask
from log import get_request_logger
from datetime import datetime
import time
import json
import pandas as pd
from pydub import AudioSegment
import os


logger = get_request_logger()

class PushResultsTask(Task):
    
    def format_phone_num(self, phone_number):
         if phone_number and len(phone_number) >= 10:
             return '91' + phone_number.strip()[-10:]
         else:             
             return phone_number
    
    def generate_record_string(self, records, start_time, phone_no, order ):
        
        record = ''
        record += ("local_date:" + str(start_time.date())) + "|"
                
        records = list(records)
        
        if len(records) > 0:
            counter = 0
            while counter < len(order):
                if counter < len(records):
                    record += ( order[counter] + ":" + str(records[counter].qu_value) + "|")
                else:
                    record += ( order[counter] + ":0" + "|")
                counter += 1
            
        record += ("phone_number:" + self.format_phone_num(phone_no))
        logger.info('the final output of the dict:' + str(record))
        return record
        
    def push_result(self, ai, cdr, out_string, cred):
                        
            logger.info('creating client with url...')
            http_proxy = utils.get_http_proxy()
            client = None
            if http_proxy:
                transport = suds.transport.http.HttpTransport()
                proxy = urllib2.ProxyHandler({'http': http_proxy})
                opener = urllib2.build_opener(proxy)
                transport.urlopener = opener
                client = suds.client.Client(cred.url, transport=transport) 
            else:
                client = suds.client.Client(cred.url)           
            error_code = client.service.sendFinancialTransactionForTheDay(cred.cred_key, out_string)
            logger.info('error code is:' + str(error_code))
            report = Report_log(cred = cred, report = out_string, cdr = cdr, error_code = error_code)
            report.save()
    
    def run(self, ai_id, cdr_id):
        
        try:
            logger.info('running the push result task..')
            cdr = Cdr.objects.get(pk = cdr_id)
            ai = App_instance.objects.get(pk = ai_id)
            records = Record.objects.filter(survey__ai = ai, cdr = cdr).order_by('order')
            logger.info('generating record string...')
            cred = Report_creds.objects.get(ai = ai)
            order = cred.order.strip().split(',')
            out_string = self.generate_record_string(records, cdr.start_time, cdr.callerid, order)
            logger.info('out string is:' + str(out_string))
            if out_string:
                self.push_result(ai, cdr, out_string, cred)
            
        except Exception as e:
            logger.info('exception while pushing results to remote server:' + str(e))


class AssessmentResultsTask(Task):

    def get_survey_unit_id(self, ai):
        ai_choices = Choices.objects.filter( ai = ai )
        if ai_choices.count() > 0:
            return ai_choices[0].choice

    def get_survey_unit_name(self, ai):
        unit_maps = Assessment_ai_unit_map.objects.filter(ai = ai)
        if unit_maps.exists():
            return unit_maps[0].unit_name

    def get_survey_module_id(self, ai):
        ai_choices = Choices.objects.filter( ai = ai )
        if ai_choices.count() > 0:
            selector_ai = ai_choices[0].selector_ai
            return self.get_survey_unit_id(selector_ai)

    def send_sms_message(self, ai, callerid, total_questions, total_answer_count, correct_answer_count):
        params = {}
        params['ques_count'] = total_questions
        params['total_answers'] = total_answer_count
        params['correct_answers'] = correct_answer_count
        params['destination'] = callerid

        unit = self.get_survey_unit_name(ai)
        if unit:
            params['unit'] = unit

        module_id = self.get_survey_module_id(ai)
        if module_id:
            params['module_id'] = module_id

        template = Event_sms_template.objects.get(template__ai = ai, event = SurveyEvents.SEND_ASSESSMENT_RESULT).template
        message = template.process_template(params)
        sms = SMSTask.create_send_msg(ai.id, message, callerid)
        return sms

    def run(self, survey_cdr, total_questions):
        try:
            logger.info('Running AssessmentResultsTask for survey_cdr id : '+str(survey_cdr.id))
            survey = survey_cdr.survey
            cdr = survey_cdr.cdr
            total_answers = get_answered_ques_count(survey_cdr)
            Assessment.populate_assessment_data(survey.id, cdr_id = cdr.id)
            ai = survey.ai
            event = SurveyEvents.SEND_ASSESSMENT_RESULT
            if Event_sms_template.objects.filter(template__ai=ai, event=event).exists():
                correct_ans_count = Assessment.objects.filter(record__cdr=cdr, status=Assessment_status.CR).count()
                sms = self.send_sms_message(ai, cdr.callerid, total_questions, total_answers, correct_ans_count)
                Cdr_event_sms_log(cdr=cdr, sms=sms, event=event).save()
        except Exception, e:
            logger.exception('Exception in AssessmentResultsTask: %s' % str(e))



class PostSuveyCallTask(Task):

    def run(self,  survey_config):
        try:
            start_time = datetime.now()
            recording = Record.objects.filter(survey = survey_config.survey, order = survey_config.ff_quesion_order).order_by('-id')[0]
            upload_a_file_tg_group(recording.ff_full_filename(), survey_config.schedule)
            contact_numbers = survey_config.contact_list.get_numbers()
            max_tries = 3
            ai = survey_config.schedule.ai_group.ai
            for number in contact_numbers:
                group_call_log = Groups_call_log(number = number, ai = ai, group_schedule = survey_config.schedule)
                group_call_log.save()
                print 'scheduling task for: ' + str(group_call_log)
                CallAndPlayTask.apply_async([group_call_log.id, max_tries], eta = datetime.now(), queue = 'push')
            end_time = datetime.now()
            logger.info('task mark')
            ReportGenerateTask.apply_async([group_call_log.id, start_time, end_time, ['kapil.dadheech@gramvaani.org']], eta = datetime.now() + timedelta(minutes = 5), queue = 'survey_stats')     
        except Exception,e:
            logger.exception("Exception in fetching News in Collection" + str(e))


class ReportGenerateTask(Task):
	
    def run(self,  last_group_call_log_id, start_time, end_time,  emails):
        try:
           last_group_call_log = Groups_call_log.objects.get(pk = last_group_call_log_id)
           logger.info('Inside task')
           logger.info(str(last_group_call_log))
           total_call_duration = 0.0
           data = [['date and time', 'callerid', 'Call pick up', 'Duration']]
           logger.info('Last GCL' +str(last_group_call_log.tries) + '--->' +str(last_group_call_log.id))
           if last_group_call_log.tries >= 1:
              logger.info('Inside if')
              logger.info(str(start_time))
              logger.info(str(end_time))
              gcl = Groups_call_log.objects.filter(group_schedule = last_group_call_log.group_schedule, last_cdr__start_time__gte = start_time, last_cdr__start_time__lte = end_time )
              logger.info(str(gcl))
              for g in gcl:
                  d = []
                  d.append(g.last_cdr.start_time)
                  logger.info(str(d))
                  d.append(g.number)
                  if g.success == True:
                     d.append('Yes')
                   
                     call_duration = get_call_duration(g.last_cdr)
                     if call_duration is not None:
                        total_call_duration += call_duration
                     else:
                        total_call_duration = 'Not Available'
                     d.append(total_call_duration)
                     
                  else:
                     d.append('No')
                     d.append('Not available')
                  data.append(d)
              op_file = '/tmp/jivika_call_stats'+str(start_time.date())+'.xlsx'
              generate_attachment(data, op_file)
              send_email('Call Stats', 'PFA', emails, op_file)   
           else:
              
              ReportGenerateTask.apply_async([last_group_call_log, start_time, end_time, emails], eta = datetime.now() + timedelta(minutes = 2), queue='survey_stats')
        except Exception,e:
              logger.error(str(e))


class SendAudioToMlTask(Task):
	
    def run(self, recording_path, conditional_flow, cdr):
        try:
            entity_json, transcript = '', ''
            logger.info("cdr::"+str(cdr))
            from os import path
            check = 0
            while True:
                time.sleep(1)
                if not path.exists(recording_path):
                    check = check +1           
                    if check > 4:
                        break    
                else:
                    break        
            #recording_path = '/home/bala/Downloads/age.mp3'
            logger.info('task recording:'+str(recording_path))

            phrase_hint_list = conditional_flow.df_properties.phrase_hints.split(',') if conditional_flow.df_properties.phrase_hints else []
            #print(conditional_flow.context_name)
            if conditional_flow.is_df_enabled:
                entity_json, transcript = get_df_intent_from_stream(recording_path, cdr.id, conditional_flow.df_properties.context_name, conditional_flow.df_properties.language, phrase_hint_list)
                # fetch entity from local api instead of df
            logger.info('returned entity_json'+entity_json)
            logger.info('returned transcript'+transcript)			

            if not transcript:
              entity_dict = {"success":False, "entity": [{"value": "NOSPEECH"}]}
            elif not entity_json and transcript:
              entity_dict = {"success":True, "entity": [{"value": "NOENTITY"}]}
            elif entity_json and transcript:
              entity_dict = json.loads(entity_json)

            query_text = {"query_text":transcript}
            entity_dict.update(query_text)
            survey_record = Record.objects.get(cdr = cdr, order = conditional_flow.question_order)
            survey_record.entity_json = json.dumps(entity_dict)
            survey_record.save()
            logger.info('survey_record intent'+str(survey_record.entity_json) + str(survey_record.id))
            ##Save intent into DB for some unique columns and then fetch the same intent in the survey flow

        except Exception as e:
            logger.info('Exception in SendAudioToMlTask : '+str(e))


class SendAudioToQnaTask(Task):

    def run(self, record_id, recording_path, dest_recording_obj, cdr, theme_selected, survey_obj, news_id):
        try:
            transcript = ''
	    qna_data_path = ''
            default_theme = ''
            if survey_obj.misc_properties:
                misc_properties = json.loads(survey_obj.misc_properties)
                qna_data_path = misc_properties.get('qna_data_path')
                default_theme = misc_properties.get('default_theme')
            theme_name = theme_selected if theme_selected else default_theme
            check = 0
            while True:
                time.sleep(1)
                if not os.path.exists(recording_path):
                    check = check +1           
                    if check > 4:
			logger.info("NO RECORDING STARTED")
                        break    
                else:
                    break        
            logger.info('task recording:'+str(recording_path))

            transcript = ' '.join(transcribe_streaming(recording_path))
            logger.info("transcript: "+transcript)
            ques_response = {}
            #call Jaccard model
            file_df = pd.read_excel(qna_data_path, sheet_name=theme_name).fillna('')
            syn_df = pd.read_excel(qna_data_path, sheet_name='synonyms').fillna('')

            ques_file = file_df['Relevant Topic'].tolist()
            relevant_questions = [x.strip().encode('utf-8') for x in ques_file]
            ids_file = file_df['Sanitized question ID'].tolist()
            sanitized_questions = file_df['Sanitized question'].tolist()
            sanitized_questions = [x.strip().encode('utf-8') for x in sanitized_questions]
            sanitized_question_ids = ids_file

            # preprocess the input query
            words = syn_df['Words'].fillna('').tolist()
            words = [x.strip().encode('utf-8') for x in words]

            syns = syn_df['Synonyms'].fillna('').tolist()
            syns = [x.strip().encode('utf-8') for x in syns]

            other_synonyms = syn_df['Other synonyms'].fillna('').tolist()
            other_synonyms = [x.strip().encode('utf-8') for x in other_synonyms]

            syn_word_map = {}
            for word, syn, other_syn in zip(words, syns, other_synonyms):
                syn_words = syn.split(',')
                syn_words.extend(other_syn.split(','))
                syn_words = [w.strip() for w in syn_words]
                for w in syn_words:
                    syn_word_map[w] = word

            # Remove the empty elements
            try:
                syn_word_map.pop('')
            except:
                pass

            final_in_ques = transcript
            for syn, word in syn_word_map.items():
                if syn in final_in_ques:
                    final_in_ques = final_in_ques.replace(syn, word)

            logger.info("transcript after preprocessing: "+final_in_ques)
            ques_response = get_qna_model_result(final_in_ques.strip().encode('utf-8'), relevant_questions, sanitized_question_ids, sanitized_questions)
            logger.info("ques_response : "+str(ques_response))

            ques_response.update({"query_text":transcript, "news_id":news_id})
                    
            survey_record = Record.objects.get(id = record_id)
            survey_record.entity_json = json.dumps(ques_response)
            survey_record.save()

	    #convert wav to mp3
            AudioSegment.from_wav(recording_path).export(dest_recording_obj.get_full_filename(), format="mp3")

        except Exception as e:
            logger.info('Exception in SendAudioToQnaTask : '+str(e))
