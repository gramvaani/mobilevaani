from django.http import HttpResponse, HttpResponseNotFound
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType

from excel_response import ExcelResponse
from xlsxwriter import Workbook

from vapp.utils import populate_spreadsheet
from vapp.perms import user_has_perms
import vapp.app_manager.perms as survey_perms 

from app_manager.decorators import instance_perm, instance_perm_per_obj
from log import get_request_logger

from app_manager.models import Cdr
from survey.models import *
from survey.stats import *
from media.models import Prompt_set, Recording, Prompt_info, Prompt_audio
from media.views import prompt_upload_impl

import json, math
import shutil
from datetime import datetime
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

logger = get_request_logger()
user_type = ContentType.objects.get_for_model(User)

@login_required
def load_forms(request):
    logger.debug(request)
    
    forms = Form.objects.filter(form_entity__content_type__pk = user_type.id, form_entity__object_id = request.user.id, is_frozen = False)
    
    js = serializers.get_serializer("json")()
    data = js.serialize(forms, ensure_ascii = False)
    return HttpResponse(data)

@login_required
def load_form(request, form_id):
    logger.debug(request)
    
    form = Form.objects.filter(id = form_id)
    
    js = serializers.get_serializer("json")()
    data = js.serialize(form, ensure_ascii = False)
    
    return HttpResponse(data)
    
    
@login_required
@csrf_exempt
@require_POST
def create_form(request):
    logger.debug(request)
    
    input = json.loads(request.POST['json'])[0]
    name = input['name']
    prompt_set = Prompt_set(name = 'form_' + name)
    prompt_set.save()
    form = Form(name = name, prompt_set = prompt_set)
    form.save()
    Form_entity(form = form, perm = 'w', content_type = user_type, object_id = request.user.id).save()
    return HttpResponse("ok")

@login_required
@csrf_exempt
@require_POST
def delete_form(request):
    logger.debug(request)
    
    form = serializers.deserialize('json', request.POST['json']).next().object
    
    for prompt_audio in Prompt_audio.objects.filter(prompt_set = form.prompt_set_id):
        prompt_audio.delete_media()
        prompt_audio.delete()
        
    form.prompt_set.delete()
    
    for fq in Form_question.objects.filter(form = form.id):
        try:
            fq.question.delete()
        except:
            logger.debug("Exception in deleting form: %s" % form)
    
    form.delete()
    
    return HttpResponse("ok")
    

@login_required
@csrf_exempt
@require_POST
def add_question(request, form_id):
    logger.debug(request)

    order = Form_question.objects.filter(form = form_id).count() + 1
    
    for question_wrapper in serializers.deserialize('json', request.POST['json']):
        question_wrapper.object.order = order
        question_wrapper.save()
    
    question = question_wrapper.object
    question_type = ContentType.objects.get_for_model(question.__class__)
    Form_question(form_id = form_id, content_type = question_type, object_id = question.id).save()
    return HttpResponse("ok")

@login_required
def load_questions(request, form_id, start_row, num_rows):
    logger.debug(request)
    
    prompt_set_id = Form.objects.get(pk = form_id).prompt_set_id
    questions = [fq.question for fq in Form_question.objects.filter(form = form_id)]
    questions.sort(key = lambda x: x.order)
    for q in questions:
        q.prompt_set_id = prompt_set_id
    
    js = serializers.get_serializer("json")()
    data = js.serialize(questions, ensure_ascii = False, extras = ('type', 'prompt_set_id'), relations = {'prompt_audio':{'extras':('info',)}})
    
    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
def save_question(request):
    logger.debug(request)
    
    for question_wrapper in serializers.deserialize('json', request.POST['json']):
        question_wrapper.save()
    
    return HttpResponse("ok")

def change_question_order(question, new_order):
    prompt_audio = question.prompt_audio
    prompt_name = 'survey_question_num_' + str(new_order)
    if prompt_audio:
        try:
            new_prompt_audio = Prompt_audio.objects.get(prompt_set = prompt_audio.prompt_set.id, info__name = prompt_name)
        except:
            info, created = Prompt_info.objects.get_or_create(name = prompt_name, description = prompt_name, text = prompt_name )
            new_prompt_audio = Prompt_audio.objects.create(prompt_set = prompt_audio.prompt_set, info= info)
        logger.debug("moving " + prompt_audio.get_full_filename() + " to: " + new_prompt_audio.get_full_filename())
        shutil.move(prompt_audio.get_full_filename(), new_prompt_audio.get_full_filename())
        question.prompt_audio = new_prompt_audio
        new_prompt_audio.time = datetime.now()
        new_prompt_audio.save()
    question.order = new_order
    question.save()


def delete_question_impl( question ):
    
    order = question.order
    question_type = ContentType.objects.get_for_model(question.__class__)
    form_question = Form_question.objects.get(content_type = question_type, object_id = question.id)
    form_question.delete()
    try:
        question.prompt_audio.delete_media()
    except:
        logger.exception('Unable to delete prompt_audio for question %s' % question)
    question.prompt_audio.delete()
    question.delete()
    
    questions = [fq.question for fq in Form_question.objects.filter(form = form_question.form.id) if fq.question.order > order]
    for offset, question in enumerate(questions):
        change_question_order(question, order + offset)

@login_required
@csrf_exempt
@require_POST
def delete_question(request):
    logger.debug(request)
    
    question_wrapper = serializers.deserialize('json', request.POST['json']).next()    
    question = question_wrapper.object

    delete_question_impl( question )

    return HttpResponse("ok")


def reorder_questions_impl( form_id, source, target ):
    if source < target:
        min = source
        max = target
        shift_up = True
        offset = -1
    else:
        min = target
        max = source
        shift_up = False
        offset = 1
    
    questions = [fq.question for fq in Form_question.objects.filter(form = form_id) if min <= fq.question.order <= max]
    questions.sort(key = lambda x: x.order) if shift_up else questions.sort(key = lambda x: -x.order)

    source_question = questions.pop(0) 

    temp_filename = '/tmp/' + form_id + '_survey_question.mp3'
    prompt_audio = source_question.prompt_audio
    
    if prompt_audio:
        shutil.move(prompt_audio.get_full_filename(), temp_filename)
        new_prompt_audio = Prompt_audio.objects.get(prompt_set = prompt_audio.prompt_set.id, info__name = 'survey_question_num_' + str(target))
        source_question.prompt_audio = new_prompt_audio
        new_prompt_audio.time = datetime.now()
        new_prompt_audio.save()
    source_question.order = target
    source_question.save()
    for question in questions:
        change_question_order(question, question.order + offset)
    if prompt_audio:
        shutil.move(temp_filename, source_question.prompt_audio.get_full_filename())

@login_required
@csrf_exempt
@require_POST
def reorder_questions(request):
    logger.debug(request)
    
    data = json.loads(request.POST['json'])[0]
    form_id = data['form_id']
    source = int(data['source'])
    target = int(data['target'])
    reorder_questions_impl( form_id, source, target )  
    
    return HttpResponse("ok")


@login_required
@csrf_exempt
@require_POST
def prompt_upload(request, form_id):
    logger.debug(request)

    prompt_set_id = Form.objects.get(pk = form_id).prompt_set_id
    if request.POST.has_key('prompt_type'):
        question = serializers.deserialize('json', '['+request.POST['prompt_value']+']' ).next().object
        prompt_name = 'survey_question_num_' + str(question.order)
        prompt_info = Prompt_info.objects.get_or_create(name = prompt_name)[0]
        prompt_audio = Prompt_audio.objects.get_or_create(prompt_set_id = prompt_set_id, info = prompt_info)[0]
        question.prompt_audio = prompt_audio
        prompt_audio.time = datetime.now()
        prompt_audio.save()
        question.save()
    else:
        prompt_name = request.POST['promptName']
        prompt_info = Prompt_info.objects.get_or_create(name = prompt_name)[0]
        prompt_set_id = request.POST['promptSetId']
    prompt_upload_impl(request, prompt_set_id = prompt_set_id, prompt_name = prompt_name)
    return HttpResponse("ok")

@login_required
def load_survey(request, ai_id):
    logger.debug(request)

    survey  = Survey.objects.get_or_create(ai_id = ai_id, is_active = True )[0]
    js      = serializers.get_serializer("json")()
    data    = js.serialize([survey], ensure_ascii = False, relations = ('form',))
    
    return HttpResponse(data)

@login_required
def download_spreadsheet(request, survey_id):
    logger.debug(request)
    form = Form.objects.get(survey = survey_id)
    questions = [fq.question for fq in Form_question.objects.filter(form__survey = survey_id)]
    questions.sort(key = lambda x: x.order)
    data = [['Order', 'Question Type', 'Question Text']]
    labels = {}
    for question in questions:
        data.append([question.order, question.type(), question.text])
        if isinstance(question, Question_mc):
            labels[question.order] = dict(enumerate(question.choices.split(',')))
            
    data.append([])
    headers = ['Call ID', 'Number', 'Question Order', 'Question Type', 'Choice', 'Choice Label', 'Value', 'Audio Url', 'Transcript', 'Time']
    data.append(headers)
    
    for record in Record.objects.filter(survey = survey_id):
        cdr = record.cdr
        question = record.question
        label = None
        try:
            if isinstance(question, Question_mc):
                label = labels[question.order][int(record.mc_choice) - 1]
        except:
            pass
        if record.ff_audio:
            url = record.ff_audio.get_url()
        else:
            url = None
        row = [cdr.id, cdr.callerid, question.order, question.type(), record.mc_choice, label, record.qu_value, url, record.ff_text, record.time]
        data.append(row)
    PREFIX = "Copy of "
    if form.name.startswith(PREFIX):
        filename = form.name[len(PREFIX):]
    else:
        filename = form.name
    return ExcelResponse(data, output_name = filename)



@login_required
def download_formatted_spreadsheet(request, survey_id):
    ai_id = Survey.objects.get(id = survey_id).ai.id
    logger.debug(request)

    if not user_has_perms(request, ai_id, survey_perms.app_use):
        return HttpResponse('Not Permitted')

    form = Form.objects.get(survey = survey_id)
    PREFIX = "Copy of "
    if form.name.startswith(PREFIX):
        filename = form.name[len(PREFIX):]
    else:
        filename = form.name

    filename = "%s.xlsx" % filename

    output = StringIO.StringIO()

    book = Workbook(output)
    complete_results = book.add_worksheet("Complete Results")
    populate_spreadsheet( complete_results, get_detailed_stats(ai_id) )
  
    summary_results = book.add_worksheet("Summary Results")
    populate_spreadsheet( summary_results, get_summary_stats(ai_id) )
    
    book.close()

    output.seek(0)
    response = HttpResponse(output.read(), mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename=%s" % filename

    return response


@login_required
@csrf_exempt
@require_POST
def save_survey(request):
    logger.debug(request)
    
    survey_wrapper = serializers.deserialize('json', request.POST['json']).next()
    current_object = Survey.objects.get(pk = survey_wrapper.object.id)
    
    if survey_wrapper.object.is_enabled != current_object.is_enabled:
        Status_history(survey_id = current_object.id, is_enabled = survey_wrapper.object.is_enabled).save()
    
    survey_wrapper.save()
    
    return HttpResponse("ok")

@login_required
@csrf_exempt
@require_POST
def initialize_survey(request):
    logger.debug(request)
    
    data = json.loads(request.POST['json'])[0]
    survey = Survey.objects.get(pk = data['survey_id'])    
    template_form = Form.objects.get(pk = data['form_id'])
    frozen_form = template_form.create_copy()
    frozen_form.is_frozen = True
    frozen_form.save()
    survey.form = frozen_form
    survey.save()
    
    return HttpResponse("ok")
    
@login_required
def load_prompt_audios(request, form_id):
    logger.debug(request)
    
    prompt_audios = Prompt_audio.objects.filter(prompt_set__form = form_id)
    
    js = serializers.get_serializer("json")()
    data = js.serialize(prompt_audios, ensure_ascii = False, extras=('info',))
    
    return HttpResponse(data)

@login_required
def load_prompt_audio(request, form_id, prompt_name):
    logger.debug(request)
    prompt_audio = Prompt_audio.objects.filter(prompt_set__form = form_id, info__name = prompt_name)
    
    js = serializers.get_serializer("json")()
    data = js.serialize(prompt_audio, ensure_ascii = False, extras=('info',))
    
    return HttpResponse(data)
    
@login_required
def load_result_mc(request, question_id):
    logger.debug(request)

    choices, counts = get_mc_results(question_id)
    data = json.dumps({'choices': choices, 'counts': counts})
    return HttpResponse(data)

@login_required
def load_result_ff(request, question_id, start_row, num_rows):
    logger.debug(request)
    
    if None in [question_id, start_row, num_rows]:
        return HttpResponseNotFound()
    
    ff_type_id = ContentType.objects.get_for_model(Question_ff).id
    
    records = Record.objects.filter(content_type = ff_type_id, question_id = question_id).exclude(ff_audio = None)
    
    js = serializers.get_serializer("json")()
    data = js.serialize(records, ensure_ascii = False)

    return HttpResponse(data)

@login_required
def load_result_qu(request, question_id, bin_size):
    logger.debug(request)

    bins, counts = get_qu_results(question_id, bin_size)
    data = json.dumps({'bins': bins, 'counts': counts})    
    return HttpResponse(data)

@login_required
def get_bins_qu(request, question_id):
    
    binsizes = []
    qu_type_id = ContentType.objects.get_for_model(Question_qu).id
    values = Record.objects.filter(content_type__id = qu_type_id, question_id = question_id).values_list('qu_value', flat = True)
    maxvalue = max(values) if values else 10
    binsize = 10
    
    while binsize <= maxvalue:
        binsizes.append(binsize)
        binsize *= 10
    
    binsizes.reverse()
    
    data = json.dumps({'binsizes': binsizes})
    return HttpResponse(data)

    
@login_required
@csrf_exempt
@require_POST
def save_record(request):
    logger.debug(request)

    for record_wrapper in serializers.deserialize('json', request.POST['json']):
        record_wrapper.save()
    
    return HttpResponse("ok")

def survey_results(request, survey_id):
    
    result = Record.objects.filter(survey__id=survey_id)
       
    js = serializers.get_serializer("json")()
    data = js.serialize(result, ensure_ascii = False, excludes=('survey','form','content_type','question','ff_text',\
                                                                'ff_audio','order',),relations={'cdr':{'fields':('callerid',)}})
    return HttpResponse(data)
