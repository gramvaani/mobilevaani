# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Recording'
        db.create_table(u'media_recording', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'media', ['Recording'])

        # Adding model 'Prompt_set'
        db.create_table(u'media_prompt_set', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='und', max_length=3)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'media', ['Prompt_set'])

        # Adding model 'App_instance_prompt_set'
        db.create_table(u'media_app_instance_prompt_set', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('prompt_set', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_set'])),
            ('perm', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('is_current', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'media', ['App_instance_prompt_set'])

        # Adding model 'Prompt_info'
        db.create_table(u'media_prompt_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'media', ['Prompt_info'])

        # Adding model 'App_prompt_info'
        db.create_table(u'media_app_prompt_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Application'])),
            ('prompt_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_info'])),
        ))
        db.send_create_signal(u'media', ['App_prompt_info'])

        # Adding model 'App_instance_prompt_info'
        db.create_table(u'media_app_instance_prompt_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('prompt_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_info'])),
        ))
        db.send_create_signal(u'media', ['App_instance_prompt_info'])

        # Adding model 'Prompt_audio'
        db.create_table(u'media_prompt_audio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('prompt_set', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_set'])),
            ('info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Prompt_info'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'media', ['Prompt_audio'])


    def backwards(self, orm):
        # Deleting model 'Recording'
        db.delete_table(u'media_recording')

        # Deleting model 'Prompt_set'
        db.delete_table(u'media_prompt_set')

        # Deleting model 'App_instance_prompt_set'
        db.delete_table(u'media_app_instance_prompt_set')

        # Deleting model 'Prompt_info'
        db.delete_table(u'media_prompt_info')

        # Deleting model 'App_prompt_info'
        db.delete_table(u'media_app_prompt_info')

        # Deleting model 'App_instance_prompt_info'
        db.delete_table(u'media_app_instance_prompt_info')

        # Deleting model 'Prompt_audio'
        db.delete_table(u'media_prompt_audio')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'media.app_instance_prompt_info': {
            'Meta': {'object_name': 'App_instance_prompt_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prompt_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_info']"})
        },
        u'media.app_instance_prompt_set': {
            'Meta': {'object_name': 'App_instance_prompt_set'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_current': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_set']"})
        },
        u'media.app_prompt_info': {
            'Meta': {'object_name': 'App_prompt_info'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prompt_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_info']"})
        },
        u'media.prompt_audio': {
            'Meta': {'object_name': 'Prompt_audio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_info']"}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_set']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'media.prompt_info': {
            'Meta': {'object_name': 'Prompt_info'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['media']