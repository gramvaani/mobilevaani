from django.db import models
from django.db.models.signals import post_save, post_delete

from vapp.app_manager.models import App_instance, Application
from vapp.log import get_logger
from vapp import settings
from vapp.utils import get_py_obj, mp3_duration

from paths import *
import os, shutil

logger = get_logger()

DEFAULT_EXTENSION = ".mp3"

AUDIO_URL = '/fsmedia/'

VALID_MEDIA_EXTENSIONS = ["mp3"]

MAX_PROMPT_FILE_SIZE = 100 * 1024 * 1024
DEFAULT_RECORDING_AI_ID = 10

def is_valid_media_extension(ext):
    return ext.lower().lstrip('.') in VALID_MEDIA_EXTENSIONS


class Recording(models.Model):
    time = models.DateTimeField(auto_now_add = True)
    ai   = models.ForeignKey(App_instance)
    duration = models.DecimalField(max_digits = 10, decimal_places = 2, null = True)
    
    def get_basefilename(self):
        if self.id == None:
            self.save()

        return str(self.id) + DEFAULT_EXTENSION

    def __unicode__(self):
        return unicode(self.id)

    def get_full_filename(self):
        return MEDIA_DIR + RECORDINGS_DIR + str(self.ai.id) + '/' + self.get_basefilename()
    
    def get_url(self, base_url = None):
        return (base_url or settings.JMR_SITE) + AUDIO_URL + RECORDINGS_DIR + str(self.ai.id) + '/' + self.get_basefilename()
    
    def delete_media(self):
        try:
            os.remove(self.get_full_filename())
            logger.debug('Deleted media file: ' + self.get_full_filename())
        except:
            logger.exception('Could not delete file: ' + self.get_full_filename())
            
    def file_exists(self):
        return os.path.isfile(self.get_full_filename())
    
    def clone(self, new_ai_id = None):
        clone = Recording.objects.get( pk = self.id )
        clone.id = None
        if new_ai_id:
            clone.ai_id = new_ai_id
        clone.save()
        try:
            shutil.copy( self.get_full_filename(), clone.get_full_filename() )
        except:
            logger.exception( "File contents not copied for recording: %s to clone: %s" % ( self.id, clone.id ) )
        return clone

    def get_duration(self, force_update = False):
        if (not self.duration) or (self.duration and force_update):
            self.duration = mp3_duration(self.get_full_filename())
            self.save()
        return self.duration

def recording_delete_handler(sender, **kwargs):
    recording = kwargs['instance']
    recording.delete_media()

post_delete.connect(recording_delete_handler, sender = Recording, dispatch_uid = 'media.recording')


class Prompt_set(models.Model):
    LANG_CHOICES = (
                    ('und', 'Default'),
                    ('eng', 'English'),
                    ('hin', 'Hindi'),
    )

    name = models.CharField(max_length = 32)
    lang = models.CharField(max_length = 3, choices = LANG_CHOICES, default = 'und')
    description = models.CharField(max_length = 32)
    
    def create_copy(self, copy_name = None):
        name = copy_name or ('Copy of ' + self.name)[0:32]
        copy = Prompt_set(name = name, lang = self.lang, description = self.description)
        copy.save()
        for audio in Prompt_audio.objects.filter(prompt_set = self.id):
            new_audio = Prompt_audio(prompt_set = copy, info = audio.info)
            new_audio.save()
            shutil.copy(audio.get_full_filename(), new_audio.get_full_filename())
        return copy

    @classmethod
    def create_prompts( cls, ai ):
        pkg =  ai.app.pkg_name
        prompt_set = Prompt_set( name = ai.name, description = ai.name )
        prompt_set.save()
        prompt_infos = get_py_obj( pkg + '.prompts.get_prompt_info_names' )()
        Prompt_audio.add_media( prompt_infos, prompt_set = prompt_set )
        prompt_perm = App_instance_prompt_set.Permissions.READABLE
        app_instance_prompt_set = App_instance_prompt_set( ai = ai, prompt_set = prompt_set, is_current = True, perm = prompt_perm )
        app_instance_prompt_set.save()

    def get_prompt_audio(self, prompt_name ):
        prompt_audio = None
        try:
            prompt_audio = Prompt_audio.objects.get( prompt_set = self, info__name = prompt_name )
        except Exception, e:
            logger.exception('exception in get_prompt_audio: %s' %(str(e)) )
        return prompt_audio

    def __unicode__(self):
        name = unicode(self.id) + '_' + unicode(self.name)
        if self.lang:
            return name + '_' + self.lang
        
        return name
    
def prompt_set_save_handler(sender, **kwargs):
    prompt_set = kwargs['instance']
    dir_path = MEDIA_DIR + PROMPTS_DIR + str(prompt_set.id)
    try:
        if kwargs['created'] and not os.path.exists(dir_path):
            os.makedirs(dir_path)
    except:
        logger.exception( "Could not create dir for prompt_set %s" % prompt_set )

post_save.connect(prompt_set_save_handler, sender = Prompt_set, dispatch_uid = 'media.prompt_set_save')
    
class App_instance_prompt_set(models.Model):
    class Permissions:
        READABLE = 'r'
        WRITABLE = 'w'

    PERMISSION_CHOICES = (
                          ( Permissions.READABLE, 'readable' ),
                          ( Permissions.WRITABLE, 'writable' ),
    )

    ai = models.ForeignKey(App_instance)
    prompt_set = models.ForeignKey(Prompt_set)
    perm = models.CharField(max_length = 3, choices = PERMISSION_CHOICES)
    is_current = models.BooleanField(default = False)

    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.prompt_set)

class Prompt_info(models.Model):
    name        = models.CharField(max_length = 64)
    description = models.CharField(max_length = 32)
    text        = models.CharField(max_length = 200)
    
    def __unicode__(self):
        return unicode(self.name)
    
class App_prompt_info(models.Model):
    app = models.ForeignKey(Application)
    prompt_info = models.ForeignKey(Prompt_info)
    
    def __unicode__(self):
        return unicode(self.app) + '_' + unicode(self.prompt_info)
    
class App_instance_prompt_info(models.Model):
    ai = models.ForeignKey(App_instance)
    prompt_info = models.ForeignKey(Prompt_info)
    
    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.prompt_info)
    
class Prompt_audio(models.Model):
    prompt_set = models.ForeignKey(Prompt_set)
    info       = models.ForeignKey(Prompt_info)
    time       = models.DateTimeField(auto_now_add = True)

    @classmethod
    def add_media( cls, prompt_infos, prompt_set = None, ai_id = None ):
        prompt_set = prompt_set if prompt_set else App_instance_prompt_set.objects.get( ai_id = ai_id ).prompt_set
        for prompt_info in prompt_infos:
            prompt_audio = Prompt_audio()
            prompt_audio.prompt_set = prompt_set
            prompt_audio.info = Prompt_info.objects.get( name = prompt_info )
            prompt_audio.save()

            try:
                file = prompt_audio.get_full_filename()  
                open(file, 'w+').close() 
            except:
                logger.exception('Could not create file: ' + prompt_audio.get_full_filename())

    def get_basefilename(self):
        return self.info.name + DEFAULT_EXTENSION
    
    def get_full_filename(self):
        return MEDIA_DIR + PROMPTS_DIR + str(self.prompt_set.id) + '/' + self.get_basefilename()

    def get_url(self, base_url = None):
        return (base_url or settings.JMR_SITE) + AUDIO_URL + PROMPTS_DIR + str(self.prompt_set.id) + '/' + self.get_basefilename()

    def delete_media(self):
        try:
            os.remove(self.get_full_filename())
            logger.debug('Deleted media file: ' + self.get_full_filename())
        except:
            logger.exception('Could not delete file: ' + self.get_full_filename())
    
    def __unicode__(self):
        return unicode(self.prompt_set) + '_' + unicode(self.info)

class Image(models.Model):
    name = models.CharField(max_length = 64)
    image = models.ImageField("image", upload_to="images/", blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name)
    
    def get_location(self):
        return self.image.path if self.image else None

    def get_image_url(self, base_url = None):
        if self.image:
            return (base_url or settings.JMR_SITE) + self.image.url


class Image_caption_map(models.Model):
    caption = models.TextField()
    image = models.ForeignKey(Image)

    def __unicode__(self):
        return unicode(self.image)

def get_ai_prompt_set_path( ai ):
    prompt_set = App_instance_prompt_set.objects.get( ai_id = ai ).prompt_set
    return '{0}{1}'.format( PROMPTS_DIR, prompt_set.id )