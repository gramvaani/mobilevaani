from django.http import HttpResponse
from django.core import serializers

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth.decorators import login_required

from datetime import datetime
import os

from app_manager.models import App_instance
from app_manager.decorators import instance_perm 

from models import is_valid_media_extension, Prompt_audio, Prompt_info, Recording, MAX_PROMPT_FILE_SIZE

from log import get_request_logger

logger = get_request_logger()

ERR_UPLOAD_REC = 'ERR_UPLOAD_REC'
ERR_INVALID_FILE = 'ERR_INVALID_FILE'

UPLOAD_ERRORS = [ERR_UPLOAD_REC, ERR_INVALID_FILE]

def is_valid_upload(uploaded):
    if uploaded.size > MAX_PROMPT_FILE_SIZE:
        return False
    
    basename, extension = os.path.splitext(uploaded.name)
    if not is_valid_media_extension(extension):
        return False
    
    return True


@login_required
@csrf_exempt
@require_POST
def prompt_upload(request):
    logger.info(request)
    prompt_upload_impl(request)
    return HttpResponse("ok")

def prompt_upload_impl(request, prompt_set_id = None, prompt_name = None):
    logger.info(request)
    uploaded = request.FILES['audioUpload']
    if not is_valid_upload(uploaded):
        logger.debug('not a valid upload')
        return HttpResponse(ERR_INVALID_FILE)
    
    prompt_set_id = prompt_set_id or request.POST['promptSetId']
    prompt_name = prompt_name or request.POST['promptName']

    try:
        logger.debug('trying to upload prompt')
        prompt_info = Prompt_info.objects.get(name = prompt_name) 
        prompt_audio = Prompt_audio.objects.get_or_create(prompt_set_id = prompt_set_id, info = prompt_info)[0]
        logger.debug('full path:' + str(prompt_audio.get_full_filename()))
        f = open(prompt_audio.get_full_filename(), 'w')
        for chunk in uploaded.chunks():
            f.write(chunk)
        f.close()
        prompt_audio.time = datetime.now()
        prompt_audio.save()
        return prompt_audio
    except:
        logger.exception("Could not upload prompt prompt_set: %s, prompt_name: %s" % (prompt_set_id, prompt_name))
    

@login_required
@csrf_exempt
@require_POST
@instance_perm
def recording_upload(request, ai_id):
    logger.info(request)
    result = recording_upload_impl(request, ai_id)
    if type(result) == Recording: 
        json = serializers.get_serializer('json')()
        return HttpResponse(json.serialize([result]))
    return HttpResponse(result)

def recording_upload_impl(request, ai_id):
    logger.info(request)
    uploaded = request.FILES['audioUpload']
    if not is_valid_upload(uploaded):
        return ERR_INVALID_FILE

    ai_id = int(ai_id)
    recording_id = int(request.POST['recordingId'])
    
    if recording_id == -1:
        recording = Recording(ai_id = ai_id)
    else:
        recording = Recording.objects.get(pk = recording_id, ai = ai_id)
        
    try:
        f = open(recording.get_full_filename(), 'w')
        for chunk in uploaded.chunks():
            f.write(chunk)
        f.close()
        recording.time = datetime.now()
        recording.save()
    except:
        logger.exception("Could not upload recording ai: %s, recording_id: %s" % (ai_id, recording_id))
    
    if recording:
        return recording
    else:
        return ERR_UPLOAD_REC
        
@login_required
def prompt_current_set(request, ai_id):
    json = serializers.get_serializer('json')()
    def_prompt_set_id = App_instance.objects.get(pk = ai_id).prompt_set()
    data = json.serialize(Prompt_audio.objects.filter(prompt_set = def_prompt_set_id), extras = ('info',))
    return HttpResponse(data)
