from django.conf.urls.defaults import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import ReadOnlyAuthorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields

from vapp.api import AppInstanceAuthorization
from app_manager.api import AnonymousReadAuthentication
from models import Recording, Prompt_audio, MAX_PROMPT_FILE_SIZE, is_valid_media_extension, Image, Image_caption_map, App_instance_prompt_set
from vapp.log import get_request_logger

logger = get_request_logger()

import os
from datetime import datetime

def is_valid_upload( uploaded_file ):
    if uploaded_file.size > MAX_PROMPT_FILE_SIZE:
        return False
    
    basename, extension = os.path.splitext( uploaded_file.name )
    if not is_valid_media_extension( extension ):
        return False
    
    return True

def recording_upload_impl( uploaded_file, ai_id, recording_id = None ):
    if not is_valid_upload( uploaded_file ):
        raise Exception("Invalid Upload")

    if recording_id:
        recording = Recording.objects.get(pk = recording_id, ai = ai_id)
    else:
        recording = Recording(ai_id = ai_id)
    
    with open( recording.get_full_filename(), 'w' ) as f:
        for chunk in uploaded_file.chunks():
            f.write( chunk )
        recording.time = datetime.now()
        recording.save()
    
    return recording

def prompt_upload_impl( uploaded_file, prompt_audio_id ):
    if not is_valid_upload( uploaded_file ):
        raise Exception( "Invalid Upload" )

    audio = Prompt_audio.objects.get( pk = prompt_audio_id )

    with open( audio.get_full_filename(), 'w' ) as f:
        for chunk in uploaded_file.chunks():
            f.write( chunk )
        audio.time = datetime.now()
        audio.save()

    return audio


class RecordingResource(ModelResource):
    url = fields.CharField()

    class Meta:
        queryset = Recording.objects.all()
        resource_name = "media_recording"
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def dehydrate_url(self, bundle):
        return bundle.obj.get_url(bundle.request.build_absolute_uri('/')) + "?time=%s" % bundle.obj.time

    def hydrate(self, bundle):
        del bundle.data[ 'url' ]
        return bundle

class PromptAudioResource(ModelResource):
    url = fields.CharField()
    prompt_text = fields.CharField(attribute = 'info__name', null = True, blank = True)

    class Meta:
        queryset = Prompt_audio.objects.all()
        resource_name = "media_prompt_audio"
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        extra_actions = [
                {
                    "name" : "upload_audio",
                    "http_method": "POST",
                    "resource_type": "list",
                    "summary": "Upload a new audio",
                    "fields": {
                        "audio_file": {
                            "type":"multipart/form-data",
                            "required": True,
                            "description": "mp3 file"
                        },
                        "prompt_audio_id": {
                            "type":"related",
                            "required": True,
                            "description": "prompt audio id - related"
                        }
                    }
                }
        ]

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/upload_audio%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'upload_audio' ), name = "api_upload_audio" ),
        ]

    def upload_audio(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            uploaded_file = request.FILES[ 'audio_file' ]
            prompt_audio_id = request.REQUEST[ 'prompt_audio_id' ]
            logger.debug( "upload_audio: user: %s, pa_id: %s" % ( request.user.id, prompt_audio_id ) )

            prompt_audio = prompt_upload_impl( uploaded_file, prompt_audio_id )
            pa_bundle = self.build_bundle( obj = prompt_audio, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, self.full_dehydrate( pa_bundle ) )
        except:
            logger.exception( "upload_audio: %s" % request )
        return self.create_response( request, {'error': "unable to upload audio"} )

    def dehydrate_url(self, bundle):
        return bundle.obj.get_url() + "?time=%s" % bundle.obj.time        

    def hydrate(self, bundle):
        del bundle.data[ 'url' ]
        return bundle

    def build_filters(self, filters = None):
        if filters is None:
            filters = {}

        orm_filters = super( PromptAudioResource, self ).build_filters( filters )
        if "ps_id" in filters:
            orm_filters[ "prompt_set__id" ] = filters[ "ps_id" ]
        return orm_filters


class ImageResource(ModelResource):
    image_url = fields.CharField( attribute = 'get_image_url', null = True, blank = True )

    class Meta:
        queryset = Image.objects.all()
        authentication = AnonymousReadAuthentication()
        authorization = ReadOnlyAuthorization()

class ImageCaptionMapResource(ModelResource):
    image = fields.ForeignKey(ImageResource, attribute='image', full=True)

    class Meta:
        queryset = Image_caption_map.objects.all()
        resource_name = "media_image_caption"
        authentication = AnonymousReadAuthentication()
        authorization = ReadOnlyAuthorization()

class AiPromptSetResource(ModelResource):
    ps_id = fields.IntegerField(attribute = 'prompt_set__id')

    class Meta:
        queryset = App_instance_prompt_set.objects.all()
        resource_name = "media_ai_prompt_set"
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def build_filters(self, filters = None):
        if filters is None:
            filters = {}

        orm_filters = super( AiPromptSetResource, self ).build_filters( filters )
        if "ai_id" in filters:
            orm_filters[ "ai__id" ] = filters[ "ai_id" ]
        return orm_filters
