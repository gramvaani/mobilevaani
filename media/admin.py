from media.models import *
from django.contrib import admin


class App_instance_prompt_set_inline(admin.TabularInline):
    from vapp.app_manager.models import App_instance
    model = App_instance
    extra = 1


class AIPS_Admin(admin.ModelAdmin):
    inlines = [
               App_instance_prompt_set_inline,
    ]

admin.site.register(Prompt_set)
admin.site.register(App_instance_prompt_set)
admin.site.register(Prompt_info)
admin.site.register(Prompt_audio)
admin.site.register(Image)
admin.site.register(Image_caption_map)
