from django.conf.urls import url

from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from app_manager.models import Cdr
from vapp.log import get_request_logger
from common import *
from vapp.utils import read_rows_from_csv
from datetime import datetime
from scheduler.models import Schedule
from utils import row_to_model_mapper, copy_item_to_channel
from mnews.models import Groups_call_log, Transient_group_schedule
from app_manager.common import get_queue_for_task
from mnews.tasks import CallAndPlayTask
from scheduler.tasks import ScheduleObdataTask, ScheduleObdTask

logger = get_request_logger()


class SchedulerResource(ModelResource):
    class Meta:
        resource_name = "scheduler"
        allowed_methods = ['get', 'post']
        queryset = Schedule.objects.all().order_by('id')

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/new_file%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('schedule_by_file'), name="api_schedule_by_file"),
            url(r"^(?P<resource_name>%s)/schedule_data%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('schedule_data'), name="api_schedule_data"),
            url(r"^(?P<resource_name>%s)/getOBDResult%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_obd_results'), name="api_get_obd_result"),
            url(r"^(?P<resource_name>%s)/getBulkUpdateOBDResult%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_bulk_obd_results'), name="api_get_bulk_obd_results"),

        ]

    def schedule_by_file(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        try:
            file_name = request.REQUEST['fileName']
            #            str_start_date = request.REQUEST['startDate']
            #            str_delay = request.REQUEST['delay']

            full_file_path = obd_file_generated_dir_path + file_name + ".csv"
            logger.info("File Path :- %s " % full_file_path)

            #queue = get_queue_for_task(ai, default_queue='push')
            ScheduleObdTask.apply_async([full_file_path], eta=datetime.now(), queue="bi_scheduler")
            return self.create_response(request, {'Success': True})

        except:
            logger.exception("schedule_by_file")
            return self.create_response(request, {'error': "Some error occurred "})


    def schedule_data(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        try:
            action_by = request.REQUEST['actionBy']
            msisdn_schedules = request.REQUEST['msisdnSchedules']
            transaction_id = request.REQUEST['transactionId']
            slot_id = request.REQUEST.get('slotId')
            project = request.REQUEST.get('project')
            ScheduleObdataTask.apply_async([slot_id, project, action_by, msisdn_schedules, transaction_id], eta=datetime.now(), queue="obdscheduler")
            return self.create_response(request, {'success': True, 'error': False})

        except:
            logger.exception("schedule_data")
            return self.create_response(request, {'success': False, 'error': "Some error occurred"})


    def get_obd_results(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        scheduler_resource = SchedulerResource()
        try:
            schedules = Schedule.objects.filter(status='SENT')
            response = []
            for schedule in schedules:

                group_call_log = Groups_call_log.objects.get(id=schedule.gcl_id)
                schedule.tries = group_call_log.tries
                last_cdr_id = group_call_log.last_cdr_id
                if last_cdr_id is not None:
                    last_cdr = Cdr.objects.get(id=last_cdr_id)
                    schedule.start_time = last_cdr.start_time
                    schedule.end_time = last_cdr.end_time
                    schedule.hangup_reason = last_cdr.hangup_cause
                    schedule.answered_time = last_cdr.answered_time
                    if last_cdr.answered_time is None:
                        schedule.status = "TRIED_BUT_FAILED"
                    else:
                        schedule.status = "SUCCESS"

                    if group_call_log.tries == 0:
                        schedule.status = "UNTRIED"
                    schedule.save()
                    scheduler_bundle = scheduler_resource.build_bundle(obj=schedule, request=request)
                    scheduler_bundle = scheduler_resource.full_dehydrate(scheduler_bundle)
                    response.append(scheduler_bundle)
        except:
            logger.exception("schedule_by_file")
        return self.create_response(request, {'result': response})

    def get_bulk_obd_results(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        scheduler_resource = SchedulerResource()
        strDate = request.REQUEST['date']
        # 2018 / 06 / 01
        date = datetime.strptime(strDate, "%Y-%m-%d").date()
        schedules = Schedule.objects.filter(execution_date=date);
        response = []
        for schedule in schedules:
            group_call_log = Groups_call_log.objects.get(id=schedule.gcl_id)
            schedule.tries = group_call_log.tries
            last_cdr_id = group_call_log.last_cdr_id
            if last_cdr_id is not None:
                last_cdr = Cdr.objects.get(id=last_cdr_id)
                schedule.start_time = last_cdr.start_time
                schedule.end_time = last_cdr.end_time
                schedule.hangup_reason = last_cdr.hangup_cause
                schedule.answered_time = last_cdr.answered_time
                if last_cdr.answered_time is None:
                    schedule.status = "TRIED_BUT_FAILED"
                else:
                    schedule.status = "SUCCESS"

                if group_call_log.tries == 0:
                    schedule.status = "UNTRIED"
                schedule.save()
                scheduler_bundle = scheduler_resource.build_bundle(obj=schedule, request=request)
                scheduler_bundle = scheduler_resource.full_dehydrate(scheduler_bundle)
                response.append(scheduler_bundle)
        return self.create_response(request, {'result': response})


class ItemChannel:
    item_id = 0
    channel_id = 0
    copy_comment = 'N'

    def __init__(self, item_id, channel_id, copy_comment):
        self.item_id = item_id
        self.channel_id = channel_id
        self.copy_comment = copy_comment
