from datetime import datetime, date

from models import Schedule
from vapp.log import get_request_logger
from mnews.models import News, Channel, News_state, reorder_published_items
from mnews.views import clone_news, clone_campaign
from mnews.models import Groups_call_log
from app_manager.models import Cdr


logger = get_request_logger()


def row_to_model_mapper(row):
    if len(row) > 0:
        try:
            s = Schedule.objects.get(bi_id=row[0])
            if s is not None:
                return None
        except:
            s = Schedule()
            s.bi_id = int(row[0])
            s.slot_id = int(row[1])
            s.msisdn = row[2].strip()
            s.execution_date = datetime.strptime(row[3], '%Y/%m/%d %H:%M:%S.%f')
            s.project = row[4].strip()
            s.rule_id = int(row[5])
            s.priority = row[7].strip()
            s.schedule_id = int(row[12])
            s.item_id = int(row[14])
            s.status = 'SENT'
            s.channel_id = int(row[15])
            s.is_replaceable = row[16].strip()
            return s

def schedule_mapper(counter, slot_id, project, schedule, transaction_id, action_by = 'BIDB'):
    s = Schedule()
    if "id" in schedule.keys():
        s.bi_id = schedule.get("id")
    else:                             #counter if schedule.get("id") if not found
        s.bi_id = counter
    if slot_id:
        s.slot_id = slot_id               #int HHMM if not present in high level
    else:
        s.slot_id = int(datetime.now().strftime("%H%M"))
    s.msisdn = schedule.get("msisdn")
    s.execution_date = datetime.now()
    if project:
        s.project = project           #actioby if project not found in high level
    else:
        s.project = action_by
    if "rule_id" in schedule.keys():
        s.rule_id = schedule.get("rule_id")
    else:
        s.rule_id = 0                 #0 if schedule get not found
    s.priority = "HIGH"
    s.schedule_id = schedule.get("scheduleId")
    s.item_id = None
    s.status = 'RECEIVED'             # RECEIVED when recieved and then same status
    s.channel_id = None
    s.is_replaceable = None
    if transaction_id:
        s.transaction_id = transaction_id # if not found str(actinby+bi_id+rule_id)
    else:
        s.transaction_id = action_by+str(bi_id)+str(rule_id)
    s.action_by = action_by # if not found str "BIDB"
    return s

def schedule_dict_map(schedule, calldata_dict):
    schedule_dict = {}
    schedule_dict['cdr_id'] = schedule_dict['start_time'] = schedule_dict['answered_time'] = schedule_dict['hangup_time'] = schedule_dict['hangup_reason'] = None
    schedule_dict['msisdn'] = str(schedule.msisdn)
    schedule_dict['status'] = str(schedule.status)
    schedule_dict['tries'] = int(schedule.tries)
    if schedule.start_time:
        schedule_dict['start_time'] =  schedule.start_time.strftime("%Y-%m-%dT%H:%M:%SZ")
    if schedule.answered_time:
        schedule_dict['answered_time'] = schedule.answered_time.strftime("%Y-%m-%dT%H:%M:%SZ")
    if schedule.end_time:
        schedule_dict['hangup_time'] = schedule.end_time.strftime("%Y-%m-%dT%H:%M:%SZ")
    if schedule.hangup_reason:
        schedule_dict['hangup_reason'] = str(schedule.hangup_reason)
    if schedule.cdr:
        schedule_dict['cdr_id'] = int(schedule.cdr_id)
    schedule_dict['schedule_id'] = int(schedule.schedule_id)
    calldata_dict.setdefault(schedule.transaction_id, []).append(schedule_dict)
    return calldata_dict

def schedule_status_update(action_by):
    schedules = Schedule.objects.filter(action_by = action_by, status='RECEIVED')
    for schedule in schedules:
        if schedule.answered_time is None:
            schedule.status = "TRIED_BUT_FAILED"
        else:
            schedule.status = "SUCCESS"

        if schedule.tries == 0:
            schedule.status = "UNTRIED_DND_WINDOW"
        schedule.save()

def copy_item_to_channel(item_id, is_replaced):
    if item_id > 0:
        news = News.objects.get(pk=item_id)
        if news.state != 'PUB':
            news.state = 'PUB'
            news.save()
            if is_replaced:
                news.pub_order = 0
                news.save()
            else:
                reorder_published_items(news, news.pub_order, 'PUB')

def update_schedules(action_by):
    try:
        schedules = Schedule.objects.filter(action_by = action_by, status='RECEIVED')
        for schedule in schedules:
            group_call_log = Groups_call_log.objects.get(id=schedule.gcl_id)
            schedule.tries = group_call_log.tries
            last_cdr_id = group_call_log.last_cdr_id
            if last_cdr_id is not None:
                last_cdr = Cdr.objects.get(id=last_cdr_id)
                schedule.cdr_id = last_cdr.id
                schedule.start_time = last_cdr.start_time
                schedule.end_time = last_cdr.end_time
                schedule.hangup_reason = last_cdr.hangup_cause
                schedule.answered_time = last_cdr.answered_time
                if schedule.answered_time is None:
                    schedule.status = "TRIED_BUT_FAILED"
                else:
                    schedule.status = "SUCCESS"
	    else:
                schedule.status = "UNTRIED"
            if group_call_log.no_dial_cause == 'DND_NUMBER':
                schedule.status = "UNTRIED_DND_USER"
            schedule.save() 
    except Exception as e:
        logger.exception("Exception in Populate call data : " + str(e))


def populate_schedules_to_dynamo(action_by):
    calldata_dict = {}
    today = date.today()
    schedules = Schedule.objects.filter(action_by = action_by,
        execution_date__year = today.year,
        execution_date__month = today.month,
        execution_date__day = today.day)
    for schedule in schedules:
        calldata_dict = schedule_dict_map(schedule, calldata_dict)
    return calldata_dict
