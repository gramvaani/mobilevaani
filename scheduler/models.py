from django.db import models
from app_manager.models import Cdr


class Schedule(models.Model):
    bi_id = models.IntegerField(null=False)
    slot_id = models.IntegerField(null=False)
    msisdn = models.CharField(max_length=50)
    execution_date = models.DateTimeField()
    project = models.CharField(max_length=100)
    rule_id = models.IntegerField(null=False)
    tries = models.IntegerField(default=0)
    priority = models.CharField(max_length=100, null=True)
    start_time = models.DateTimeField(null=True)
    answered_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True)
    hangup_reason = models.CharField(max_length=100)
    schedule_id = models.IntegerField(null=False)
    gcl_id = models.IntegerField(null=True)
    status = models.CharField(max_length=100, null=True)
    item_id = models.IntegerField(null=True, default=0)
    channel_id = models.IntegerField(null=True, default=0)
    is_replaceable = models.CharField(null=True, default='N', max_length=100)
    cdr = models.ForeignKey(Cdr, null = True)
    transaction_id = models.CharField(null=True, default = '', max_length = 200)
    action_by = models.CharField(null=True,default = '', max_length=100)