# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Schedule.copy_comment'
        db.delete_column(u'scheduler_schedule', 'copy_comment')

        # Adding field 'Schedule.is_replaceable'
        db.add_column(u'scheduler_schedule', 'is_replaceable',
                      self.gf('django.db.models.fields.CharField')(default='N', max_length=100, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Schedule.copy_comment'
        db.add_column(u'scheduler_schedule', 'copy_comment',
                      self.gf('django.db.models.fields.CharField')(default='N', max_length=100, null=True),
                      keep_default=False)

        # Deleting field 'Schedule.is_replaceable'
        db.delete_column(u'scheduler_schedule', 'is_replaceable')


    models = {
        u'scheduler.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'bi_id': ('django.db.models.fields.IntegerField', [], {}),
            'channel_id': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'execution_date': ('django.db.models.fields.DateTimeField', [], {}),
            'gcl_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'hangup_reason': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_replaceable': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '100', 'null': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'project': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rule_id': ('django.db.models.fields.IntegerField', [], {}),
            'schedule_id': ('django.db.models.fields.IntegerField', [], {}),
            'slot_id': ('django.db.models.fields.IntegerField', [], {}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'tries': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['scheduler']