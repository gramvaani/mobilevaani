# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Schedule'
        db.create_table(u'scheduler_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bi_id', self.gf('django.db.models.fields.IntegerField')()),
            ('slot_id', self.gf('django.db.models.fields.IntegerField')()),
            ('msisdn', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('execution_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('project', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('rule_id', self.gf('django.db.models.fields.IntegerField')()),
            ('tries', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('priority', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('answered_time', self.gf('django.db.models.fields.DateField')(null=True)),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('hangup_reason', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('schedule_id', self.gf('django.db.models.fields.IntegerField')()),
            ('gcl_id', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'scheduler', ['Schedule'])


    def backwards(self, orm):
        # Deleting model 'Schedule'
        db.delete_table(u'scheduler_schedule')


    models = {
        u'scheduler.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'answered_time': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'bi_id': ('django.db.models.fields.IntegerField', [], {}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'execution_date': ('django.db.models.fields.DateTimeField', [], {}),
            'gcl_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'hangup_reason': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'project': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rule_id': ('django.db.models.fields.IntegerField', [], {}),
            'schedule_id': ('django.db.models.fields.IntegerField', [], {}),
            'slot_id': ('django.db.models.fields.IntegerField', [], {}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'tries': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['scheduler']