# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Schedule.cdr'
        db.add_column(u'scheduler_schedule', 'cdr',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'], null=True),
                      keep_default=False)

        # Adding field 'Schedule.transaction_id'
        db.add_column(u'scheduler_schedule', 'transaction_id',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Schedule.cdr'
        db.delete_column(u'scheduler_schedule', 'cdr_id')

        # Deleting field 'Schedule.transaction_id'
        db.delete_column(u'scheduler_schedule', 'transaction_id')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'scheduler.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'bi_id': ('django.db.models.fields.IntegerField', [], {}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'channel_id': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'execution_date': ('django.db.models.fields.DateTimeField', [], {}),
            'gcl_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'hangup_reason': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_replaceable': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '100', 'null': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'project': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rule_id': ('django.db.models.fields.IntegerField', [], {}),
            'schedule_id': ('django.db.models.fields.IntegerField', [], {}),
            'slot_id': ('django.db.models.fields.IntegerField', [], {}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'null': 'True'}),
            'tries': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['scheduler']