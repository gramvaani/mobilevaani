
from celery.task import Task
from vapp.log import get_request_logger
from common import *

from vapp.utils import read_rows_from_csv
from datetime import datetime
from scheduler.models import Schedule
from utils import row_to_model_mapper, copy_item_to_channel, schedule_mapper
from mnews.models import Groups_call_log, Transient_group_schedule
from app_manager.common import get_queue_for_task
from mnews.tasks import CallAndPlayTask
from datetime import datetime
import ast
from vapp.log import get_request_logger

logger = get_request_logger()



class ScheduleObdataTask(Task):
	def run(self, slot_id, project, action_by, msisdn_schedules, transaction_id):
		try:
			schedules = None
			if msisdn_schedules:
				schedules = ast.literal_eval(msisdn_schedules)
			for counter, schedule in enumerate(schedules):
				s = schedule_mapper(counter + 1, slot_id, project, schedule, transaction_id, action_by)
				copy_item_to_channel(s.item_id,s.is_replaceable)
				logger.info("OBD Scheduler Number from Scheduler" + s.msisdn)
				tgs = Transient_group_schedule.objects.get(pk=int(s.schedule_id))
				if not (tgs.play_ai or tgs.prompt_file):
					logger.error(
					"Neither play_ai nor prompt_file provided in schedule %s. Not scheduling calls." % (
						s,))
					continue
				else:
					ai = tgs.ai_group.ai
					group_call_log = Groups_call_log(number=s.msisdn, ai=ai, group_schedule=tgs)
					group_call_log.save()
				#	queue = get_queue_for_task(ai, default_queue='push')
					logger.info('Obd time started. Doing dialouts')
					s.gcl_id = group_call_log.id
					s.save()
					CallAndPlayTask.apply_async([group_call_log.id, 3], eta=datetime.now(), queue='priority_push')                     
		except Exception as e:
			logger.info('exception thrown while running ScheduleObdataTask:' + str(e))


class ScheduleObdTask(Task):
    def run(self, full_file_path):
        rows = read_rows_from_csv(full_file_path)
        rows.pop(0)
        for row in rows:
            schedule = row_to_model_mapper(row)
            if schedule is not None:
                copy_item_to_channel(schedule.item_id,schedule.is_replaceable);
                logger.info("OBD Scheduler Number from Scheduler" + schedule.msisdn)
                tgs = Transient_group_schedule.objects.get(pk=int(schedule.schedule_id))
                if not (tgs.play_ai or tgs.prompt_file):
                    logger.error(
                        "Neither play_ai nor prompt_file provided in schedule %s. Not scheduling calls." % (
                            schedule,))
                    continue
                else:
                    ai = tgs.ai_group.ai
                    group_call_log = Groups_call_log(number=schedule.msisdn, ai=ai, group_schedule=tgs)
                    group_call_log.save()
                    queue = get_queue_for_task(ai, default_queue='obd_scheduler_push')
                    logger.info('Obd time started. Doing dialouts')
                    schedule.gcl_id = group_call_log.id
                    schedule.save()
                    CallAndPlayTask.apply_async([group_call_log.id, 3], eta=datetime.now(), queue=queue)   

