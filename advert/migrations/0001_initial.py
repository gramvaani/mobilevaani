# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Advertisement'
        db.create_table(u'advert_advertisement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ad_recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True)),
        ))
        db.send_create_signal(u'advert', ['Advertisement'])

        # Adding model 'Ad_impression_log'
        db.create_table(u'advert_ad_impression_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('vi', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.VI_conf'])),
            ('ad_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Advertisement'])),
            ('callerid', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('played_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('duration', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'advert', ['Ad_impression_log'])

        # Adding model 'Ad_lead_log'
        db.create_table(u'advert_ad_lead_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('impression', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Ad_impression_log'])),
        ))
        db.send_create_signal(u'advert', ['Ad_lead_log'])

        # Adding model 'Ad_ai_mapping'
        db.create_table(u'advert_ad_ai_mapping', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ad_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Advertisement'])),
        ))
        db.send_create_signal(u'advert', ['Ad_ai_mapping'])

        # Adding model 'Ad_impression_policy'
        db.create_table(u'advert_ad_impression_policy', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Advertisement'])),
            ('max_total_impressions', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('max_impressions_per_user', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('probability', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'advert', ['Ad_impression_policy'])

        # Adding model 'Ai_position_ad_count'
        db.create_table(u'advert_ai_position_ad_count', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('additional_data', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ad_count', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'advert', ['Ai_position_ad_count'])

        # Adding model 'Ad_impression_stats'
        db.create_table(u'advert_ad_impression_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Advertisement'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('total_impressions', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('unique_impressions', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'advert', ['Ad_impression_stats'])

        # Adding model 'Ad_target_reference'
        db.create_table(u'advert_ad_target_reference', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Advertisement'])),
            ('reference', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.VI_reference'])),
        ))
        db.send_create_signal(u'advert', ['Ad_target_reference'])

        # Adding model 'Ad_sms_template_settings'
        db.create_table(u'advert_ad_sms_template_settings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Advertisement'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('sms_template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_template'])),
            ('max_send_limit', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'advert', ['Ad_sms_template_settings'])

        # Adding model 'Ad_sms_log'
        db.create_table(u'advert_ad_sms_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad_setting', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Ad_sms_template_settings'])),
            ('sms_message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_message'])),
        ))
        db.send_create_signal(u'advert', ['Ad_sms_log'])


    def backwards(self, orm):
        # Deleting model 'Advertisement'
        db.delete_table(u'advert_advertisement')

        # Deleting model 'Ad_impression_log'
        db.delete_table(u'advert_ad_impression_log')

        # Deleting model 'Ad_lead_log'
        db.delete_table(u'advert_ad_lead_log')

        # Deleting model 'Ad_ai_mapping'
        db.delete_table(u'advert_ad_ai_mapping')

        # Deleting model 'Ad_impression_policy'
        db.delete_table(u'advert_ad_impression_policy')

        # Deleting model 'Ai_position_ad_count'
        db.delete_table(u'advert_ai_position_ad_count')

        # Deleting model 'Ad_impression_stats'
        db.delete_table(u'advert_ad_impression_stats')

        # Deleting model 'Ad_target_reference'
        db.delete_table(u'advert_ad_target_reference')

        # Deleting model 'Ad_sms_template_settings'
        db.delete_table(u'advert_ad_sms_template_settings')

        # Deleting model 'Ad_sms_log'
        db.delete_table(u'advert_ad_sms_log')


    models = {
        u'advert.ad_ai_mapping': {
            'Meta': {'object_name': 'Ad_ai_mapping'},
            'ad_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'advert.ad_impression_log': {
            'Meta': {'object_name': 'Ad_impression_log'},
            'ad_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'duration': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'played_time': ('django.db.models.fields.DateTimeField', [], {}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']"})
        },
        u'advert.ad_impression_policy': {
            'Meta': {'object_name': 'Ad_impression_policy'},
            'ad_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_impressions_per_user': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_total_impressions': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'probability': ('django.db.models.fields.FloatField', [], {}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'advert.ad_impression_stats': {
            'Meta': {'object_name': 'Ad_impression_stats'},
            'ad_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_impressions': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'unique_impressions': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'advert.ad_lead_log': {
            'Meta': {'object_name': 'Ad_lead_log'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'impression': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Ad_impression_log']"})
        },
        u'advert.ad_sms_log': {
            'Meta': {'object_name': 'Ad_sms_log'},
            'ad_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Ad_sms_template_settings']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_message']"})
        },
        u'advert.ad_sms_template_settings': {
            'Meta': {'object_name': 'Ad_sms_template_settings'},
            'ad_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_send_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'sms_template': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']"})
        },
        u'advert.ad_target_reference': {
            'Meta': {'object_name': 'Ad_target_reference'},
            'ad_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_reference']"})
        },
        u'advert.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'ad_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ad_recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.ai_position_ad_count': {
            'Meta': {'object_name': 'Ai_position_ad_count'},
            'ad_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'additional_data': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_reference': {
            'Meta': {'object_name': 'VI_reference'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'target_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vi'", 'to': u"orm['app_manager.VI_conf']"})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['advert']