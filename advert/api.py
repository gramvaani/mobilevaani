from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import ReadOnlyAuthorization
from tastypie.resources import Resource, ModelResource, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields

from vapp.app_manager.api import AppInstanceResource
from vapp.advert.models import Ad_ai_mapping, Ad_statistics_user_mapping, Ad_impression_log, Ad_lead_log, Advertisement
from vapp.advert.stats import is_ad_active
from datetime import *
from vapp.utils import daterange
import time
from vapp.log import get_request_logger

logger = get_request_logger()


class AdvertAppInstanceMapResource(ModelResource):
    ai = fields.ForeignKey(AppInstanceResource, 'ai')

    class Meta:
        queryset = Ad_ai_mapping.objects.all()
        fields = ['ad_item']
        resource_name = 'advert_ai_map'
        allowed_methods = ['get']
        filtering = {
            'ai' : ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def dehydrate(self, bundle):
        bundle.data['id'] = bundle.obj.ad_item.id
        bundle.data['name'] = bundle.obj.ad_item.ad_name
        bundle.data[ 'is_active' ] = is_ad_active( bundle.obj.ad_item )
        return bundle


class AdvertStatsResource(Resource):
    class Meta:
        resource_name = 'advert_stats'
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/ads_with_stats_perm%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view( 'get_ads_with_stats_perm' ), name = "api_ads_with_stats_perm" ),
            url(r"^(?P<resource_name>%s)/ad_stats%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view( 'get_impressions_and_leads' ), name = "api_ad_stats" ),
        ]

    def get_ads_with_stats_perm(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            user = request.user
            ad_stat_maps = Ad_statistics_user_mapping.objects.filter(user = user)
            ads = []
            for ad_stat_map in ad_stat_maps:
                if is_ad_active( ad_stat_map.ad ):
                    ad_ai_maps = Ad_ai_mapping.objects.filter(ad_item = ad_stat_map.ad)
                    ai_ids = []
                    for ad_ai_map in ad_ai_maps:
                        ai_ids.append({ 'id': ad_ai_map.ai_id, 'name': ad_ai_map.ai.name })

                    ads.append({ 'id': ad_stat_map.ad.id, 'name': ad_stat_map.ad.ad_name, 'ai_ids': ai_ids })

            self.log_throttled_access( request )
            return self.create_response( request, { "ads_with_perm": ads } )
        except:
            logger.exception( "Exception in get_ads_with_stats_perm: %s" % request )
        return self.create_response( request, { 'error': "unable to get ads with stats permission" } )

    def get_impressions_and_leads(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_id = request.REQUEST.get('ai_id', None)
            ad_id = request.REQUEST[ 'ad_id' ]

            start_date = request.REQUEST.get('start_date', None)
            end_date = request.REQUEST.get('end_date', None)
            if start_date and end_date:
                start_date = datetime.strptime(start_date, '%Y-%m-%d')
                end_date = datetime.strptime(end_date, '%Y-%m-%d')
            else:
                ad_imp_log = Ad_impression_log.objects.filter(ad_item_id = ad_id)
                if ai_id is not None:
                    ad_imp_log = ad_imp_log.filter(ai_id = ai_id)
                if not ad_imp_log.exists():
                    return self.create_response( request, { "data": [] } )
                start_date = ad_imp_log.order_by('id')[0].time.date()
                end_date = date.today() + timedelta(days = 1)

            data = [ { 'key': 'Impressions', 'values': [], 'total_impressions': 0 }, { 'key': 'Leads', 'values': [], 'total_leads': 0 } ]
            total_impressions = 0
            total_leads = 0

            for dt in daterange(start_date, end_date):
                to_date = dt + timedelta(days = 1)
                impressions = Ad_impression_log.objects.filter(ad_item_id = ad_id, time__range = (dt, to_date))
                leads = Ad_lead_log.objects.filter(impression__ad_item__id = ad_id, impression__time__range = (dt, to_date))
                if ai_id is not None:
                    impressions = impressions.filter(ai_id = ai_id)
                    leads = leads.filter(impression__ai__id = ai_id)

                total_impressions += impressions.count()
                total_leads += leads.count()

                to_datetime = datetime.combine(dt, datetime.min.time())
                time_in_seconds = int( time.mktime(to_datetime.timetuple() ) + 1e-6*to_datetime.microsecond ) * 1000
                data[0]["values"].append([ time_in_seconds, impressions.count() ])
                data[1]["values"].append([ time_in_seconds, leads.count() ])

            data[0]["total_impressions"] = total_impressions
            data[1]["total_leads"] = total_leads

            self.log_throttled_access( request )
            return self.create_response( request, { "data": data } )
        except:
            logger.exception( "Exception in get_impressions_and_leads: %s" % request )
        return self.create_response( request, { 'error': "unable to get impressions and leads" } )