from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from vapp.app_manager.models import *
from media.models import Recording
from sms.models import *


AD_MAX_LENGTH = 50
VIEW_NAME_LENGTH = 64
VIEW_URI_LENGTH = 255

class Advertisement(models.Model):
    
    ad_name = models.CharField(max_length = AD_MAX_LENGTH)
    ad_recording = models.ForeignKey(Recording, null = True, blank = True)
    
    def __unicode__(self):
        return unicode(self.ad_name)
    def get_full_filename(self):
        return self.ad_recording.get_full_filename() if self.ad_recording else None

class Ad_impression_log(models.Model):
    
    ai = models.ForeignKey(App_instance)
    vi = models.ForeignKey(VI_conf)
    ad_item = models.ForeignKey(Advertisement)
    callerid = CalleridField()
    position = models.CharField(max_length = 100)
    cdr = models.ForeignKey(Cdr)
    played_time = models.DateTimeField()
    duration = models.FloatField()
    time = models.DateTimeField(default = datetime.now)
    

class Ad_lead_log(models.Model):
    impression = models.ForeignKey(Ad_impression_log)

class Ad_ai_mapping(models.Model):
    
    ai = models.ForeignKey(App_instance)
    position = models.CharField(max_length = 100)
    ad_item = models.ForeignKey(Advertisement)
    is_skippable = models.BooleanField(default = False)

    def __unicode__(self):
        return  unicode(self.ai) + "-" + self.position + "-" + unicode(self.ad_item)
    
class Ad_impression_policy(models.Model):
    
    ad_item = models.ForeignKey(Advertisement)
    max_total_impressions = models.PositiveIntegerField()
    max_impressions_per_user = models.PositiveIntegerField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    probability = models.FloatField()
    min_ad_impression_time = models.PositiveIntegerField( default = 15 )
    
    def __unicode__(self):
        return unicode(self.ad_item)

class Ai_position_ad_count(models.Model):
    
    ai = models.ForeignKey(App_instance)
    position = models.CharField(max_length = 100)
    additional_data = models.CharField(max_length = 100, null = True, blank = True)
    ad_count = models.PositiveSmallIntegerField(default = 1)
    
    def __unicode__(self):
         return unicode(self.ai) + "-" + self.position + "-" + unicode(self.ad_count)
 
    
class Ad_leads_impressions_stats(models.Model):
    
    ad_ai_map = models.ForeignKey(Ad_ai_mapping)    
    unique_impressions = models.PositiveIntegerField(default = 0)
    unique_leads = models.PositiveIntegerField(default = 0)
    last_updated = models.DateTimeField(default = datetime.now)
    from_time = models.DateTimeField(null = True, blank = True)
    to_time = models.DateTimeField(null = True, blank = True)    
    
    
class Ad_target_reference(models.Model):    
    ad_item = models.ForeignKey(Advertisement)
    reference = models.ForeignKey(VI_reference)
    embeddable = models.BooleanField(default = False)
    generate_reference_data = models.BooleanField(default = True)

    def __unicode__(self):
        return unicode(self.ad_item) + "->" + unicode(self.reference)

class Ad_sms_template_settings(models.Model):
    
    ad_item = models.ForeignKey(Advertisement)
    ai = models.ForeignKey(App_instance)
    sms_template = models.ForeignKey(SMS_template)
    max_send_limit = models.PositiveSmallIntegerField(default = 1)
    
    def __unicode__(self):
        return unicode(self.ad_item) + '_' + unicode(self.sms_template)
    
    def clean(self):
        if self.sms_template.ai.id != self.ai.id:
            raise ValidationError('SMS_template.ai and ai must be the same')
        super(Ad_sms_template_settings, self).clean()

    
class Ad_sms_log(models.Model):
    ad_setting = models.ForeignKey(Ad_sms_template_settings)
    sms_message = models.ForeignKey(SMS_message)
    
    @classmethod
    def ad_sms_sent(cls, ad_setting, callerid):
        count = Ad_sms_log.objects.filter(ad_setting = ad_setting, sms_message__receiver_id = callerid, sms_message__sent_success = True).count() 
        return count >= ad_setting.max_send_limit
    
    
class Ad_statistics_user_mapping(models.Model):
    user = models.ForeignKey(User)
    ad = models.ForeignKey(Advertisement)
    stats_start_date = models.DateTimeField(null = True, blank = True)

    def __unicode__(self):
        return unicode(self.ad) + '_' + unicode(self.user)

class Stats_view( models.Model ):
    name        = models.CharField( max_length = VIEW_NAME_LENGTH )
    description = models.TextField()
    ads         = models.ManyToManyField( Advertisement )
    users       = models.ManyToManyField( User, null = True, blank = True )
    view_uri    = models.CharField( max_length = VIEW_URI_LENGTH )

    def __unicode__( self ):
        return '{0}'.format( self.name )
