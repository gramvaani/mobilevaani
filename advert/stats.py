from models import *

from django.template.loader import render_to_string

from vapp.stats.models import Stats, Stats_type

from datetime import *
from vapp.utils import generate_workbook, send_email, load_vapp_module, daterange, LINE_BREAK
from vapp.app_manager import perms as app_manager_perms


def populate_stats(ai, start_time, end_time):
    unique_impressions_map = {}

    ad_maps = Ad_ai_mapping.objects.filter(ai=ai)

    for ad_map in ad_maps:
        filters = dict(ai=ad_map.ai, ad_item=ad_map.ad_item,
                       position=ad_map.position, time__gte=start_time,
                       time__lte=end_time)
        unique_impressions = Ad_impression_log.objects.filter(**filters).values('callerid').distinct().count()

        filters = dict(impression__ai=ad_map.ai,
                       impression__ad_item=ad_map.ad_item,
                       impression__position=ad_map.position,
                       impression__time__gte=start_time,
                       impression__time__lte=end_time)
        unique_leads = Ad_lead_log.objects.filter(**filters).values('impression__callerid').distinct().count()

        ad_stat, created = Ad_leads_impressions_stats.objects.get_or_create(ad_ai_map=ad_map,
                                                                            from_time=start_time,
                                                                            to_time=end_time)
        ad_stat.unique_impressions = unique_impressions
        ad_stat.unique_leads = unique_leads
        if not created: ad_stat.last_updated = datetime.now()
        ad_stat.save()


def is_ad_active(ad):
    today = date.today()
    return Ad_impression_policy.objects.filter(ad_item = ad, end_time__gte = today).exists()

def get_active_ads_for_user(user):
    return [ ad_user_map.ad for ad_user_map in Ad_statistics_user_mapping.objects.filter(user = user) if is_ad_active(ad_user_map.ad) ]

def get_legends_data():
    legends = {
                'name': 'Legends',
                'header_row_count': 0,
                'data': []
               }

    legends[ 'data' ].append( [ 'Impressions - Number of times an advertisement was heard on a given day.' ] )
    legends[ 'data' ].append( [ 'Leads - From above, how many users showed further interest in service.' ] )
    return legends


def get_ad_stats_start_date(user, ad):
    try:
        return Ad_statistics_user_mapping.objects.get(user = user, ad = ad).stats_start_date.date()
    except:
        try:
            return Ad_impression_log.objects.filter(ad_item = ad).order_by('id')[0].time.date()
        except:
            return date.today()


def generate_ad_stats_datasheet(ad, start_date, end_date = None):
    end_date = end_date or date.today()

    ad_imp_dict = get_ad_impressions(ad, start_date, end_date)
    leads_data = []

    for target_ai, source_ai in get_target_source_ai_map(ad).items():
        ai_stats = load_vapp_module(target_ai.app.pkg_name, 'stats')
        if hasattr(ai_stats, 'get_advert_leads_stats'):
            detailed_data = ai_stats.get_advert_leads_stats(target_ai.id,
                                                            start_date,
                                                            end_date)
        else:
            detailed_data = ""
        detailed_data_dict = {
                              'name': 'Leads Data (%s)' % target_ai.name,
                              'header_row_count': 1,
                              'data': detailed_data
                              }
        leads_data.append(detailed_data_dict)

    output_file = '/tmp/%s_%s_%s_stats.xlsx' % (ad.ad_name, start_date, end_date)
    generate_workbook([ ad_imp_dict ] + leads_data + [ get_legends_data() ], output_file)
    return output_file


def get_ad_impressions(ad, start_date, end_date):
    ad_stats = {
                'name': 'Stats',
                'header_row_count': 1,
                'data': []
                }

    ad_stats['data'].append(['Date', 'Impressions', 'Leads'])

    for from_date in daterange(start_date, end_date):
        to_date = from_date + timedelta(days=1)
        dates = (from_date, to_date)
        impressions = Ad_impression_log.objects.filter(ad_item=ad,
                                                       time__range=dates).count()
        leads = Ad_lead_log.objects.filter(impression__ad_item=ad,
                                           impression__time__range=dates).count()
        ad_stats['data'].append([from_date, impressions, leads])

    return ad_stats


def get_target_source_ai_map(ad):
    target_source_ai_map = {}
    for target_reference in Ad_target_reference.objects.filter(ad_item = ad, generate_reference_data = True):
        target_source_ai_map[target_reference.reference.target_ai] = target_reference.reference.ai

    return target_source_ai_map


def get_stats(ads, start_datetime, end_datetime, stat_type = None):
    data = []
    reports = []
    if stat_type == Stats_type.DAILY:
        reports.extend([ generate_ad_stats_datasheet(ad, start_datetime.date()) for ad in ads ])

    for ad in ads:
        stats = Stats()
        stats.ad_name = ad.ad_name
        stats.total_impressions = Ad_impression_log.objects.filter(ad_item = ad, time__range = (start_datetime, end_datetime)).count()
        stats.total_leads = Ad_lead_log.objects.filter(impression__ad_item = ad, impression__time__range = (start_datetime, end_datetime)).count()
        data.append(stats)

    return (data, reports)


def create_html_content(stats):
    return render_to_string('advert/stats.html', { 'data': stats }) + LINE_BREAK
