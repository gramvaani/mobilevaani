from django.contrib import admin
from django import forms
from models import *
from media.models import Recording, DEFAULT_RECORDING_AI_ID
from app_manager.models import App_instance
from media.api import recording_upload_impl


class AdvertisementAdminForm(forms.ModelForm):
	ad_file = forms.FileField(required = True)
	
	class Meta:
		model = Advertisement


class AdvertisementAdmin(admin.ModelAdmin):
	form = AdvertisementAdminForm

	formfield_overrides = {
		models.ForeignKey: {'widget': forms.TextInput(attrs={'readonly':'True'})},
	}

	def save_model(self, request, obj, form, change):
		if request.method == 'POST':
			form = AdvertisementAdminForm(request.POST, request.FILES)
			if form.is_valid():
				recording_obj = Recording(ai = App_instance.objects.get(pk = DEFAULT_RECORDING_AI_ID))
				recording_obj.save()
				obj.ad_recording = recording_obj
				super(AdvertisementAdmin, self).save_model(request, obj, form, change)
				recording_upload_impl(request.FILES['ad_file'], DEFAULT_RECORDING_AI_ID, recording_obj.id)
		else:
			form = AdvertisementAdminForm()


admin.site.register(Advertisement, AdvertisementAdmin)
admin.site.register(Ad_sms_template_settings)
admin.site.register(Ad_impression_policy)
admin.site.register(Ad_ai_mapping)
admin.site.register(Ai_position_ad_count)
admin.site.register(Ad_target_reference)
admin.site.register(Ad_statistics_user_mapping)


class Stats_view_admin( admin.ModelAdmin ):
	filter_horizontal = ( 'ads', 'users' )

admin.site.register( Stats_view, Stats_view_admin )