from models import *
from vapp.utils import *

from sms.tasks import SMSTask

from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import random

MIN_AD_IMPRESSION_TIME = 15

def log_partial_impression(ad, ai, position, start_time, end_time):
    min_impression_time = ad.ad_impression_policy_set.all()[0].min_ad_impression_time if ad.ad_impression_policy_set.exists() else MIN_AD_IMPRESSION_TIME
    return get_total_seconds(end_time - start_time) > min_impression_time


def is_ad_skippable(ad, ai, position):
    try:
        return Ad_ai_mapping.objects.get(ai = ai, ad_item = ad, position = position).is_skippable
    except:
        return False

def get_weighted_choice(weights):
    totals = []
    running_total = 0
    
    for w in weights:
        running_total += w
        totals.append(running_total)
    
    rnd = random.random() * running_total
    for i, total in enumerate(totals):
        if rnd < total:
            return i

def get_ads_from_policies(ad_policies, ad_nums):
    if not ad_policies:
        return []

    weights = [ policy.probability for policy in ad_policies ]
    ad_count = 0
    ads = [] 
    while ad_count < ad_nums:
        index = get_weighted_choice(weights)
        ads.append(ad_policies[ index ].ad_item)
        ad_count += 1
    return ads


def log_impression(ad, position, cdr, start_time, end_time, ai, vi):
    duration = get_total_seconds(end_time - start_time)
    log = Ad_impression_log(ai = ai, vi = vi, ad_item = ad, callerid = cdr.callerid, cdr = cdr, played_time = start_time, duration = duration, position = position)
    log.save()
    return log

def log_ad_lead(impression_log):
    lead_log = Ad_lead_log(impression = impression_log)
    lead_log.save()

def get_target_reference_for_ad(ad, ai):
    try:
        ad_reference = Ad_target_reference.objects.get(ad_item = ad, reference__ai = ai)
        return ('@' + ad_reference.reference.reference, ad_reference.embeddable)
    except:
        logger.info("Could not find a target reference for ad %s and ai %s." % (ad, ai))


def get_ads_for_pos( ai, position, callerid, additional_data = None ):
    ads = []
    ad_policies = []

    try:
        if additional_data:
            ad_count = Ai_position_ad_count.objects.get( ai = ai, position = position, additional_data = additional_data ).ad_count
        else:
            ad_count = Ai_position_ad_count.objects.get( ai = ai, position = position ).ad_count

        ad_ids_at_position = Ad_ai_mapping.objects.filter(ai = ai, position = position).values_list('ad_item', flat = True)
        current_time = datetime.now()
        policies = Ad_impression_policy.objects.filter( start_time__lte = current_time, end_time__gte = current_time, ad_item__id__in = ad_ids_at_position )
        
        for policy in policies:
            logs = Ad_impression_log.objects.filter(ad_item = policy.ad_item)
            log_count = logs.count()
            caller_log_count = logs.filter(callerid = callerid).count()
            if caller_log_count < policy.max_impressions_per_user and log_count < policy.max_total_impressions:                
                ad_policies.append(policy)
        
        ads = get_ads_from_policies(ad_policies, ad_count)           
            
    except ObjectDoesNotExist, e:
        pass
            
    return ads


def send_ad_message(ad, ai, callerid):
    try:
        ad_setting = Ad_sms_template_settings.objects.get(ad_item = ad, ai = ai)
    
        if Ad_sms_log.ad_sms_sent(ad_setting, callerid):
            logger.info("SMS already sent to %s %s times for ad %s" % (callerid, ad_setting.max_send_limit, ad))
        else:
            message = ad_setting.sms_template.message
            sms_message = SMSTask.create_send_msg(ai.id, message, callerid)
            ad_sms_log = Ad_sms_log(ad_setting = ad_setting, sms_message = sms_message)
            ad_sms_log.save()
    except Exception, e:
        logger.info("No SMS template found for ad %s and ai %s." % (ad, ai))

