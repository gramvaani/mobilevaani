# Django settings foapp project.

import testenv

IS_PRODUCTION = testenv.is_production()
DEBUG = True
TEMPLATE_DEBUG = DEBUG

FREESWITCH_IP='192.168.1.1'

TRUNK_URLS = ['sofia/']

VOICESITE_ROOT='/usr/local/voicesite'
STATIC_ROOT =  VOICESITE_ROOT + '/static/'
STATIC_URL = '/static/'

#Mongodb creds
#MONGO_URL='122.99.125.76'
MONGO_URL='192.168.1.6'
MONGO_DB='gvdbf'
#macro
APP_NAME = 'VAPP1'

ALLOWED_HOSTS = [ 'localhost', 'voice.gramvaani.org', 'voice2.gramvaani.org', 'jukebox.astranow.com',
                'mobilevaani.in', 'mobilevoices.in' ]
MONGODB_HOST = '192.168.1.6'
MONGODB_DB = 'gvdbf'
ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)
#

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'vapp',                      # Or path to database file if using sqlite3.
        'USER': 'vapp',                      # Not used with sqlite3.
        'PASSWORD': 'vapp',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'gramvaani.stats@gmail.com'
EMAIL_HOST_PASSWORD = 'tbiuBlock1'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
#TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Asia/Calcutta'
USE_TZ = False

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = VOICESITE_ROOT+'/fsmedia/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/fsmedia/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

PLAYLIST_DIR = 'playlist/'

PLAYLIST_ROOT = MEDIA_ROOT + PLAYLIST_DIR

# Make this unique, and don't share it with anybody.
SECRET_KEY = '030ad1f-(h(m&+pg27fwgi@8xrjzl*cp!4!sjc%tiu^@de_6a@'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

LOGIN_URL='/vapp/'

ROOT_URLCONF = 'vapp.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    "/usr/local/voicesite/vapp/templates",
    "/usr/local/voicesite/vapp/templates/cvoice",
    "/usr/share/pyshared/django/contrib/auth/tests/templates",
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'social_auth.context_processors.social_auth_by_name_backends',
    'social_auth.context_processors.social_auth_backends',
    'social_auth.context_processors.social_auth_by_type_backends',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'vapp.context_processors.django_version',
)

SOCIAL_AUTH_DEFAULT_USERNAME = 'new_user'
SOCIAL_AUTH_CREATE_USERS = True

#auth for xforms data 
auth_user = 'bhavesh.bansal@gramvaani.org'
auth_key = 'bhavesh@123'

# number of cases for casemanager 
MAX_CASES = 100


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.comments',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    # Infra
    'djcelery',
    'tastypie',
    'south',
    #'tastypie_swagger',

    # Platform
    'app_manager',
    'media',
    'fblib',
    'sms',
    'callerinfo',
    'social_auth',
    'schedule',
    'stats',
    'location',
    'billing',
    'data',
    'advert',
    'campaign',
    'mvapp',
    'case_manager',
    
    # Applications
    'survey',
    'helpline',
    'tracker',
    'act_mcd',
    'mnews',
    'mward',
    'cvoice',
    'refer',
    'customization',
    'app_selector',
    #'mward_helpline',
    'vmiss',
    'field_mis',
    'pub_web',
    'registration',
    'social',
    'customer',
    'classifieds',
    'mvas',
    'xforms',
    #'django_extensions',
    'django_spaghetti',
    'scheduler',
    'caching_service',
)


AUTHENTICATION_BACKENDS = (
        'social_auth.backends.facebook.FacebookBackend',
        'django.contrib.auth.backends.ModelBackend',
)

FACEBOOK_APP_ID = '110325135758003'
FACEBOOK_API_SECRET = 'c8e4d4b860d075f33e3e26c7453ce8c3'
FACEBOOK_REQUEST_PERMISSIONS = 'user_about_me,user_location'


SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/vapp/cvoice/profile/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/vapp/cvoice/profile/'
SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/vapp/cvoice/profile/'
SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/vapp/cvoice/another'

AUTH_PROFILE_MODULE = 'cvoice.UserProfile'

SERIALIZATION_MODULES = {
 'json': 'wadofstuff.django.serializers.json'
}

## Settings for celery Job Scheduler
import djcelery
djcelery.setup_loader()

BROKER_HOST = "127.0.0.1"
BROKER_PORT = 5672
BROKER_USER = "guest"
BROKER_PASSWORD = "guest"
BROKER_VHOST = "/"

PICASA_USERNAME = "gramvaani@gmail.com"
PICASA_PASSWORD = "junoongv"

YT_USERNAME = "gramvaani@gmail.com"
YT_PASSWORD = "junoongv"
YT_DEV_KEY = 'AI39si71lUNJCqC1HNQBJ_SbF664MYhMUywHS5VgKQX-_oWIqFS1caVkSD_aRcReW51MwrH60wC_rTADI1ae_PsI4Ipi-7e2Lg'
YT_CLIENT_ID = '174829987369-jpfbp07gm3s97jkshuahr8m0al6p0rqj.apps.googleusercontent.com'

CELERY_IMPORTS = ('mnews.stats',)
CELERYD_STATE_DB = 'celery_state'
CELERY_REDIRECT_STDOUTS = False
CELERY_IGNORE_RESULT = True
CELERYD_ACKS_LATE = True
CELERYD_PREFETCH_MULTIPLIER = 0


CELERY_QUEUES = {
    'celery': {
        'exchange': 'celery',
        'exchange_type': 'direct',
        'binding_key': 'celery'
    },
    'push': {
        'exchange': 'push',
        'exchange_type': 'direct',
        'binding_key': 'push'
    }
}

CELERY_ROUTES = {
                    'survey.tasks.PushResultsTask': {'queue': 'stats'},
                    'app_manager.tasks.CallbackTask': {'queue': 'callback'},
                    'app_manager.tasks.IncomingCallTask': {'queue': 'incoming'},
                    'mnews.tasks.CallAndPlayTask': {'queue': 'push'},
                    'mnews.tasks.PushTransGroupCallsTask': {'queue': 'sms'},
                    'vapp.refer.tasks.PlayAiChainTask': {'queue': 'celery'},
                    'stats.tasks.PopulateStatsTask':{'queue':'stats'},
                    'stats.tasks.EmailStatsTask':{'queue':'stats'},
                    'mnews.tasks.DetailedStatsTask':{'queue':'stats'},
                    'social.tasks.PublishToFBTask':{'queue':'async'},
                    'social.tasks.PublishToTwitterTask':{'queue':'async'},
                    'mvas.tasks.SaveMvasDataTask': {'queue': 'celery'},
                    'field_mis.tasks.PopulateVolunteerStats':{'queue':'stats'},
                    'field_mis.tasks.PopulateVolunteerIncentiveStats': {'queue': 'stats'},
                    'billing.tasks.GenerateBillingInfoTask': {'queue': 'billing'},
                    'sms.tasks.SMSTask': {'queue': 'sms'},
                    'vapp.stats.tasks.PopulateSessionDataTask': {'queue': 'mv_stats'},
                    'survey.tasks.PostSuveyCallTask': {'queue': 'survey_feedback'},
                    'vapp.mnews.tasks.AutoRejectTask': {'queue' : 'async'},
                    'vapp.mnews.tasks.UploadtoS3Task':{'queue' : 's3_upload'},
                    'survey.tasks.ReportGenerateTask':{'queue': 'survey_stats'},
                    'mvapp.tasks.CreateResourceSyncRecordTask' : {'queue' : 'mvapp_resource_sync'},
                    'mvapp.tasks.CreateResSyncRecForResList' : {'queue':'mvapp_resource_sync_list'},
                    'mvapp.tasks.UpdateResGroupLibFoldersTask' : {'queue':'mvapp_resource_sync_list'},
		    'scheduler.tasks.ScheduleObdTask': {'queue': 'bi_scheduler'},
                    'scheduler.tasks.ScheduleObdataTask': {'queue': 'obdscheduler'},
                    'caching_service.tasks.SyncDynamoTask': {'queue': 'sync_data'},
                    'caching_service.tasks.SyncFailedTask': {'queue': 'sync_data'},
                     'mnews.tasks.PubOrderSyncTask': {'queue':'pub_order'}
                   
                    

                }

from celery.schedules import crontab
from datetime import datetime, timedelta

CELERYBEAT_SCHEDULE = {
    "tg_pushcalls" : {
        "task": "mnews.tasks.PushTransGroupCallsTask",
        "schedule": crontab(hour = 0, minute = 0) #timedelta(seconds=20)
    },
    "populate_stats":{
        "task": "stats.tasks.PopulateStatsTask",
        "schedule": crontab(hour = 0, minute = 10)
    },
    "email_stats":{
        "task": "stats.tasks.EmailStatsTask",
        "schedule": crontab(hour = 10, minute = 10)
    },
}

JMR_SITE = "http://voice.gramvaani.org"

FREETDM_URLS = {
    'TPH4': [ '{ignore_early_media=true}freetdm/1/A/','{ignore_early_media=true}freetdm/2/A/',
              '{ignore_early_media=true}freetdm/3/A/','{ignore_early_media=true}freetdm/4/A/',
              '{ignore_early_media=true}freetdm/5/A/','{ignore_early_media=true}freetdm/6/A/',
              '{ignore_early_media=true}freetdm/9/A/',
              '{ignore_early_media=true}freetdm/11/A/','{ignore_early_media=true}freetdm/12/A/',
              '{ignore_early_media=true}freetdm/15/A/','{ignore_early_media=true}freetdm/16/A/'],


    'Tme:FreeTDM/15:28/09934402230PH2': ['{ignore_early_media=true}freetdm/1/A/', '{ignore_early_media=true}freetdm/2/A/',
             '{ignore_early_media=true}freetdm/3/A/', '{ignore_early_media=true}freetdm/4/A/',
             '{ignore_early_media=true}freetdm/5/A/', '{ignore_early_media=true}freetdm/6/A/',
             '{ignore_early_media=true}freetdm/7/A/'],
    'DEV': [],
}

#HTTP_PROXY="http://10.22.4.30:3128/"
## End Settings for celery

SPAGHETTI_SAUCE = {
  'apps':['app_manager'],
  'show_fields':True,
}

try:
    from local_settings import *
except ImportError, e:
    pass
