'''
Created on 19-Apr-2011

@author: dineshk
'''

import stomp, socket

from django.db.models import Model
from django.core.serializers.base import DeserializedObject
from django.core import serializers
from log import get_logger

rabbitmq_connection = None
logger = get_logger()

def send_rabbitmq_message(queue, message=None, hosts=[('localhost', 61613)]):
                   
        global rabbitmq_connection
        
        if not rabbitmq_connection or not rabbitmq_connection.is_connected():
                                    
            count = 0
            while count < 3:
                try:                    
                    rabbitmq_connection = stomp.Connection(user='orbiteduser', passcode='orbited', host_and_ports=hosts)
                    rabbitmq_connection.start()
                    rabbitmq_connection.connect()
                    
                    if  rabbitmq_connection.is_connected():                        
                        break                    
                    
                except socket.error:
                    count = count + 1
                    pass
                
            if not rabbitmq_connection or not rabbitmq_connection.is_connected():
                raise socket.error        
        
        message = str(message)
        rabbitmq_connection.send(message, destination=queue)
        #rabbitmq_connection.disconnect()

def push_instance(queue, instance, event_type, extras=(), relations=(), excludes=()):
    cls = instance.__class__
    prefix = (cls.__module__.lstrip('vapp.').split('.')[0]+ '.' + cls.__name__).lower() + ':' + event_type
    iterable = []
    if isinstance(instance, Model):
        iterable = [instance]
    elif isinstance(instance, DeserializedObject):
        iterable = [instance.object]
    js = serializers.get_serializer("json")()
    data = prefix + ":" + js.serialize(iterable, extras=extras, relations=relations, excludes=excludes)
    
    logger.debug( "sending data: " + data + " to queue: " + queue )
    try:
        send_rabbitmq_message( queue, data )
    except:
        pass
        