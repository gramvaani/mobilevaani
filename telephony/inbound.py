'''
Created on 17-Jan-2011

@author: bchandra
'''

import uuid, threading
from threading import Thread, Condition, RLock, Lock
from datetime import datetime

from vapp.app_manager.models import Cdr
from vapp.log import get_logger

logger = get_logger()

HANGUP_DETECTED = "HANGUP_DETECTED"

class FreeswitchSession:
    login_lock = Condition()
    is_logged_in = False
    uuid_session = {}
    orig_uuid_session = {}
    
    @classmethod
    def get_session_by_uuid(cls, uuid):
        if cls.uuid_session.has_key(uuid):
            return cls.uuid_session[uuid]
        return None
    
    @classmethod
    def get_session_by_orig_uuid(cls, uuid):
        if cls.orig_uuid_session.has_key(uuid):
            return cls.orig_uuid_session[uuid]
        print cls.orig_uuid_session
        return None
    
    @classmethod
    def get_event_session(cls, event):
        #print "--------------------- get_event_session", threading.current_thread(), event['Event-Name']
        print "event", event['Event-Name'], threading.current_thread(), cls.uuid_session 
        if event['Event-Name'] == 'CHANNEL_HANGUP' and event['Channel-HIT-Dialplan'] == "false":
            print "ignoring event"
            return None
        try:
            orig_uuid = event.get('variable_origination_uuid')
            uuid = event['Unique-ID']
            if orig_uuid:                     
                session = cls.get_session_by_orig_uuid(orig_uuid)
                if event['Event-Name'] in ['CHANNEL_ANSWER', 'CHANNEL_PROGRESS']:
                    cls.uuid_session[uuid] = session
                if session: print "------------ found through ORIG_UUID"
                return session

            return cls.get_session_by_uuid(uuid)
        except:
            return None

    @classmethod
    def set_session(cls, uuid, session):
        uuid = uuid.strip()
        print "setting uuid --------", uuid
        cls.uuid_session[uuid] = session
        cls.orig_uuid_session[uuid] = session
        
    @classmethod
    def clear_session(cls, session):
        session_uuid = [uuid for uuid, s in cls.uuid_session.iteritems() if s == session]
        if session_uuid:
            cls.uuid_session.pop(session_uuid[0])
        session_orig_uuid = [uuid for uuid, s in cls.orig_uuid_session.iteritems() if s == session]
        if session_orig_uuid:
            cls.orig_uuid_session.pop(session_orig_uuid[0])
                
    @classmethod
    def login(klass):
        klass.login_lock.acquire()
        if klass.is_logged_in:
            klass.login_lock.release()
            return
        
        from twisted.internet import reactor, threads
        klass.reactor = reactor
        klass.threads = threads
        threading.current_thread().setName( "Celery Worker" )
        t = Thread( name = "Reactor Thread" )
        def run():
            from pyswitch import inbound
            f = inbound.InboundFactory("ClueCon")
            f.loginDeferred.addCallback(klass.onLogin)
            f.loginDeferred.addErrback(klass.onLoginFailed)
            reactor.suggestThreadPoolSize(1) #@UndefinedVariable
            reactor.callFromThread(reactor.suggestThreadPoolSize, 1) #@UndefinedVariable
            reactor.callFromThread(reactor.connectTCP, "127.0.0.1", 8021, f, timeout=1.0) #@UndefinedVariable
            reactor.run(installSignalHandlers = 0) #@UndefinedVariable
        t.run = run
        t.start()
        klass.login_lock.wait()
        klass.login_lock.release()

    @classmethod
    def onSocketAnswer(cls, event):
        session = cls.get_event_session(event)
        if not session:
            return
        session.handle_answer_event(event)
        
    @classmethod
    def onSocketDTMF(klass, event):
        dtmf = event["DTMF-Digit"]
        session = klass.get_event_session(event)
        if not session:
            return
        session.handleDTMF(dtmf)
    
    @classmethod
    def onSocketHangup(cls, event):
        print "------------hangup event"
        session = cls.get_event_session(event)
        if not session:
            return
        session.set_result(False, HANGUP_DETECTED)
        session.handle_hangup_event(event)
        if hasattr(session, 'hangupCallback'):
            session.hangupCallback()
        cls.clear_session(session)
    
    @classmethod
    def onSocketProgress(cls, event):
        session = cls.get_event_session(event)
        if not session:
            return
        session.handle_progress_event(event)
        
    @classmethod
    def onSocketRecordStop(cls, event):
        session = cls.get_event_session(event)
        if not session:
            return
        session.set_result(True, "Recording Completed")
    
    @classmethod
    def onSocketExecuteComplete(cls, event):
        session = cls.get_event_session(event)
        if not session:
            return
        if event['Application'] == 'playback' and not event['Application-Data'].startswith('tone_stream'):
            if event['Application-Response'] == 'FILE%20PLAYED':
                session.set_result(True, "Playback Completed")
            else:
                session.set_result(False, "Error in playback")
        else:
            print "Execute Complete, ignoring", event['Application'], threading.current_thread()

    @classmethod
    def onLogin(klass, protocol):
        klass.socket = protocol
        klass.reactor.callFromThread(klass.socket.registerEvent, "DTMF", True, klass.onSocketDTMF)
        klass.reactor.callFromThread(klass.socket.registerEvent, "CHANNEL_ANSWER", True, klass.onSocketAnswer)
        klass.reactor.callFromThread(klass.socket.registerEvent, "CHANNEL_HANGUP", True, klass.onSocketHangup)
        klass.reactor.callFromThread(klass.socket.registerEvent, "CHANNEL_EXECUTE_COMPLETE", True, klass.onSocketExecuteComplete)
        klass.reactor.callFromThread(klass.socket.registerEvent, "CHANNEL_PROGRESS", True, klass.onSocketProgress)
        klass.reactor.callFromThread(klass.socket.registerEvent, "RECORD_STOP", True, klass.onSocketRecordStop)
        klass.login_lock.acquire()
        klass.is_logged_in = True
        klass.login_lock.notifyAll()
        klass.login_lock.release()

    def __init__(self, ai = None):
        self.login()
        #self.primitive_lock = Lock()
        #self.lock = Condition(self.primitive_lock)
        self.lock = Condition()
        self.command_success = False
        self.ai = ai
        self.is_answered = False
        self.state = None
        self.uuid  = None
        self.dtmfLock = Condition()
        self.isHandlingDtmf = False
    
    def __del__(self):
        self.clear_session(self)
        
    @classmethod    
    def onLoginFailed(klass, error):
        logger.error("Login failed: %s" % (error,))
    
    @classmethod
    def onEventsSuccess(klass, event):
        logger.info("Events success: %s" % (event,))
        return event
    
    @classmethod
    def onEventsFailure(klass, error):
        logger.error("Events failure: %s" % (error,))
    
    def command_success(self):
        return self.command_success
    
    def answered(self):
        return self.is_answered
    
    def breakPlayback(self):
        self.threads.blockingCallFromThread(self.reactor, lambda: self.socket.apiUUIDBreak(self.uuid))
        
    def originate(self, url, callerid = None):
        self.uuid = str(uuid.uuid1())
        FreeswitchSession.set_session(self.uuid, self)
        
        self.cdr = Cdr(is_incoming = False, ai_id = self.ai.id, start_time = datetime.now())
        self.cdr.uuid = self.uuid
        self.cdr.callerid = callerid
        self.cdr.save()
        
        fn = lambda: self.socket.apiOriginate(url, "park", channelvars={"origination_uuid":self.uuid}, background=False)
        success, result = self.serialize(self.originate, fn)
        print "------- after originate", success, result.get_payload()
        if success:
            self.is_answered = True
            self.uuid = result.get_payload()[4:-1].strip()
        
        return result
    
    def playAndGetDigits(self, *args, **kwargs):
        kwargs['uuid'] = self.uuid
        fn = lambda: self.socket.playAndGetDigits(*args, **kwargs)
        success, result = self.serialize(self.playAndGetDigits, fn)
        return result
    
    def streamFile(self, filename):
        import threading
        fn = lambda: self.socket.playback(filename, uuid = self.uuid, lock=True)
        success, result = self.serialize(self.streamFile, fn)
        return result
    
    def recordFile(self, filename, time_limit, thresh, hits, terminators = '*#1234567890'):
        if time_limit > 1000:
            time_limit /= 1000
            
        fn = lambda: self.socket.record(filename, time_limit_secs = str(time_limit), silence_thresh = str(thresh), silence_hits = str(hits), uuid = self.uuid, terminators = terminators)
        success, result = self.serialize(self.recordFile, fn)
        return result
    
    def execute(self, cmd, args):
        fn = lambda: self.socket.sendCommand(cmd, args = args, uuid = self.uuid, lock=True)
        success, result = self.serialize(self.execute, fn)
        return result
    
    def speak(self, text):
        return self.execute('speak', 'flite|kal|' + str(text))
        
    def hangup(self):
        fn = lambda: self.socket.hangup(self.uuid)
        success, result = self.serialize(self.hangup, fn)
        return result
    
    def bridge(self, session):
        fn = lambda: self.socket.apiUUIDBridge(self.uuid, session.uuid)
        success, result = self.serialize(self.bridge, fn)
        return result
    
    def getCallerID(self):
        if self.cdr:
            return self.cdr.callerid
        else:
            return ''
    
    def getRemoteUuid(self):
        if self.channel_info:
            return self.channel_info['variable_uuid']
        else:
            return ''
    
    def getState(self):
        return self.state
    
    def getUuid(self):
        return self.uuid
    
    def isHungup(self):
        return self.state == 'CS_HANGUP'
            
    def set_result(self, success, value):
        print "---- set result, before acquire", value, threading.current_thread(), self.lock._Condition__waiters
        if value == HANGUP_DETECTED and self.command_name == self.playAndGetDigits:
            logger.error("Error for playandgetdigits %s, %s" % (success, value))
            return
        self.lock.acquire()
        print "------- set result, after acquire", value
        try:
            if not success and self.command_name == self.recordFile and value != HANGUP_DETECTED:
                logger.error("Error for record %s, %s " % (success, value))
                return
            
            if type(value) == str:
                self.command_result = value.strip()
            else:
                self.command_result = value
            self.command_success = success
            self.lock.notify()
        finally:
            self.lock.release()
            print "-------- set result, after release"
        
    def serialize(self, name, func):
        def callback(event):
            if type(event) == str:
                self.set_result(True, event)
                return
            
            if event:
                payload = event.get_payload()
                tokens = payload.split(' ')
                status = tokens[0].strip()
                value = ''.join(tokens[1:])
                if payload:
                    self.set_result(status == '+OK', value)
                else:
                    logger.error("Serialize: Callback: no payload. not moving ahead")
            else:
                self.set_result(False, None)
            
        def errback(error):
            self.set_result(False, error)
        
        self.lock.acquire()
        print "after lock.acquire ************************", threading.current_thread(), self.lock._Condition__waiters
        status = False
        result = ''
        try:
            self.command_name = name
            self.command_success = False
            self.command_result = None

            print "before blockingcallfromthread **********************", threading.current_thread(), self.lock._Condition__waiters
            result = self.threads.blockingCallFromThread(self.reactor, func)
            print "after blockingcallfromthread *************************", threading.current_thread(), self.lock._Condition__waiters
            if type(result) == str:
                status = True
            elif result:
                status = result.get_payload().startswith('+OK')
                
            if name == self.recordFile or name == self.streamFile:
                print "before lock.wait---------------------------------", threading.current_thread(), self.lock._Condition__waiters
                self.lock.wait()
                print "after lock.wait----------------------------------", threading.current_thread(), self.lock._Condition__waiters
                
        except:
            logger.exception("Encountered exception serializing %s" % (name,))
        finally:
            self.lock.release()
            print "after lock.release *****************************", threading.current_thread(), self.lock._Condition__waiters
            if threading.current_thread().getName() != 'DTMFThread':
                print "before dtmflock.acquire  %%%%%%%%%%%%%%%%%%", threading.current_thread(), self.dtmfLock._Condition__waiters
                self.dtmfLock.acquire()
                self.dtmfLock.release()
                print "after dtmflock.release %%%%%%%%%%%%%%%%%%%%", threading.current_thread(), self.dtmfLock._Condition__waiters
                
            return status, result
    
    def handleDTMF(self, dtmf):
        
        if hasattr(self, 'inputCallback') and self.lock._Condition__waiters:
            d = DTMFWrapper(dtmf)
            dtmfThread = DTMFThread(self)
            dtmfThread.handleDTMF(d)
#            try:
#                if self.inputCallbackUserData:
#                    result = self.inputCallback(self, "dtmf", d, self.inputCallbackUserData)
#                else:
#                    result = self.inputCallback(self, "dtmf", d)
#                if result == "break" or result == "false":
#                    self.socket.apiUUIDBreak(self.uuid)
#            except:
#                logger.exception("Exception in handleDTMF:")
                
    def process_event(self, event):
        state = event['Channel-State']
        if self.state != 'CS_HANGUP':
            self.state = state

    def handle_answer_event(self, event):
        self.channel_info = event
        self.cdr.answered_time = datetime.now()
        self.cdr.save()
        
    def handle_progress_event(self, event):
        self.channel_info = event
        self.cdr.callerid = event['Caller-Callee-ID-Number']
        self.cdr.save()
    
    def handle_hangup_event(self, event):
        self.cdr.hangup_cause = event['Hangup-Cause']
        self.cdr.end_time = datetime.now()
        self.cdr.save()
        
    def setHangupHook(self, func):
        self.hangupCallback = func
        
    def setInputCallback(self, func, user_data = None):
        self.inputCallback = func
        self.inputCallbackUserData = user_data
        
    def unsetInputCallback(self):
        if hasattr(self, 'inputCallback'):
            delattr(self, 'inputCallback')
        

class DTMFWrapper():
    def __init__(self, digit):
        self.digit = digit
        
class DTMFThread(Thread):
    def __init__(self, session):
        self.session = session
        self.d = None
        super(DTMFThread, self).__init__()
        self.setName('DTMFThread')
        
    def run(self):
        session = self.session
        if session.isHandlingDtmf:
            print "$$$$$$$$$$$$$ already handling dtmf, ignoring digit", self.d.digit, " ################# "
            return
        print "before dtmflock.acquire #################", self.d.digit, threading.current_thread(), session.dtmfLock._Condition__waiters
        session.dtmfLock.acquire()
        session.isHandlingDtmf = True
        print "after dtmflock.acquire #################", self.d.digit, threading.current_thread(), session.dtmfLock._Condition__waiters
        try:
            if hasattr(session, 'inputCallback'):
                if session.inputCallbackUserData:
                    result = session.inputCallback(session, "dtmf", self.d, session.inputCallbackUserData)
                else:
                    result = session.inputCallback(session, "dtmf", self.d)
                if result == "break" or result == "false":
                    #session.socket.apiUUIDBreak(session.uuid)
                    session.threads.blockingCallFromThread(session.reactor, lambda: session.socket.apiUUIDBreak(session.uuid))
        except:
            logger.exception("Exception in handleDTMF:")
        print "before dtmflock.release #################", self.d.digit, threading.current_thread(), session.dtmfLock._Condition__waiters
        session.isHandlingDtmf = False
        session.dtmfLock.release()
        print "after dtmflock.release #################", self.d.digit, threading.current_thread(), session.dtmfLock._Condition__waiters
        
    def handleDTMF(self, d):
        self.d = d
        self.start()
