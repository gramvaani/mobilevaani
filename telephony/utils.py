from vapp import settings

try:
    from freeswitch import *
except:
    pass

from vapp.media.models import Recording, Prompt_audio
from vapp.log import get_logger
from vapp.config import config #@UnresolvedImport
from vapp.app_manager.models import App_instance_settings, \
App_instance_line_ip, App_instance_settings, Cdr, Cdr_in_out_map, Ai_max_line
import vapp.settings
from vapp.settings import FREETDM_URLS
from vapp.local_settings import SERVER_ID, AI_GROUP_MAP

from django.db.models.fields.related import ReverseSingleRelatedObjectDescriptor

import random

logger = get_logger()

MAX_VALID_RETRIES = 2
MAX_INPUT_RETRIES = 2

ALL_DTMF  = '*#0123456789'
ALL_DIGITS= '0123456789'
MAX_PHONE_NUM_DIGITS = 10

def input_callback(session, type, obj):
    if type == "dtmf" and obj.digit in ALL_DTMF:
        return "break"
    return ""



def get_recording(session, duration):
    recording = Recording(ai_id = session.ai.id)
    if session.isHungup():
        logger.debug('Not recording for session %s' % (session.getUuid(),))
        return recording
    session.execute("playback", "tone_stream://%(500,0,620)")
    logger.debug("recording to file: " + recording.get_full_filename())
    session.setInputCallback(input_callback)
    session.recordFile(recording.get_full_filename(), int(duration) * 1000, 500, 8)
    session.unsetInputCallback()
    return recording

def get_embedded_recording(session, duration, wrapper, record_field, auto_save = True):
    if type(record_field) == ReverseSingleRelatedObjectDescriptor:
        record_field = record_field.field.name
        
    recording = Recording(ai_id = session.ai.id)    
    recording.save()
    setattr(wrapper, record_field, recording)
    
    if session.isHungup():
        logger.debug("Not recording file for session %s" % (session.getUuid(),))
    else:
        session.execute("playback", "tone_stream://%(500,0,620)")
        logger.debug("recording to file: " + recording.get_full_filename())
        session.setInputCallback(input_callback)
        session.recordFile(recording.get_full_filename(), int(duration), 10, 2)
        session.unsetInputCallback()
        
    if auto_save:
        wrapper.save()
    str(wrapper) #added to prevent crash.
    return wrapper
    
def get_prompt_filename(ai, prompt_name, lang = None):
    if lang:
        prompt = Prompt_audio.objects.get(prompt_set__app_instance_prompt_set__ai = ai.id,
                                          prompt_set__lang = lang,
                                          info__name = prompt_name)
    else:
        prompt = Prompt_audio.objects.get(prompt_set__app_instance_prompt_set__ai = ai.id, 
                                          prompt_set__app_instance_prompt_set__is_current = True, 
                                          info__name = prompt_name)
    return str(prompt.get_full_filename())

def set_prompt_lang(session, lang):
    session.prompt_lang = lang
        
def play_prompt(session, prompt_name = None, lang = None, prompt_audio = None):
    if session.isHungup():
        logger.debug("Not playing prompt for session %s" % (session.getUuid(),))
        return

    if not lang and hasattr(session, 'prompt_lang'):
        lang = session.prompt_lang
        
    try:
        if not session.answered():
            session.answer()
        if prompt_audio:
            prompt_filename = str(prompt_audio.get_full_filename())
        else:
            prompt_filename = get_prompt_filename(session.ai, prompt_name, lang)
        logger.debug("playing file: " + prompt_filename)
        session.streamFile(prompt_filename)
    except:
        logger.exception("Unable to play prompt: " + prompt_name)

def play_and_get(session, prompt_name = None, valid_input = ALL_DTMF, lang = None, num_digits = None, min_digits = 1, max_digits = 1, timeout = 5000, terminators = '#*', input_retries = MAX_INPUT_RETRIES, prompt_audio = None):
    
    digits = None
    if session.isHungup():
        logger.debug("Not playing prompt for session %s" % (session.getUuid(),))
        return digits

    if not lang and hasattr(session, 'prompt_lang'):
        lang = session.prompt_lang

    if num_digits:
        min_digits = num_digits
        max_digits = num_digits

    try:
        if prompt_audio:
            filename = str(prompt_audio.get_full_filename())
        else:
            filename = get_prompt_filename(session.ai, prompt_name, lang = lang)
        
        for i in range (input_retries):
            digits = None
            logger.debug(str(session.cdr.id) + " play and get file: " + filename)
            digits = session.playAndGetDigits(min_digits, max_digits, input_retries, timeout, terminators, filename, '', '')
            is_valid = True
            
            if digits:		
                for digit in digits:
                    if digit not in list(valid_input):                        		
                        is_valid = False
                        break         
                if is_valid:                    
                    return digits
    except:
        logger.exception(str(session.cdr.id) + ' Unable to get input for ' + str(prompt_name))
    
    return digits

def play_or_skip(session, prompt_name = None, lang = None, prompt_audio = None):
    digit = play_and_get(session, prompt_name, lang = lang, max_tries = 1, min_digits = 0, max_digits = 1, terminators = ALL_DTMF, input_retries = 1, prompt_audio = prompt_audio)
    return digit or False
    
def datetime_to_spoken_text(time):
    if time.strftime('%P') == 'am':
        ampm = 'a m'
    else:
        ampm = 'p m'
        
    return time.strftime('%H %M ' + ampm)


gsm_lines = None
def build_gsm_lines():
    if not config.has_section('gsmgw-modules'):
        return
    global gsm_lines
    gsm_lines = []
    ip_s = config.options('gsmgw-modules')
    ip_s.sort()
    for ip in ip_s:
        for module in range(config.getint('gsmgw-modules', ip)):
            gsm_lines.append((ip, str(module)))
    
def get_http_proxy():
    
    http_proxy = None
    if hasattr(vapp.settings,'HTTP_PROXY'):
        http_proxy = vapp.settings.HTTP_PROXY
    return http_proxy
         

def get_session_url_gsmgws(num, ai):
    
    if not gsm_lines:
        build_gsm_lines()
        
    ai_settings = App_instance_settings.get_by_aid(ai.id)
    ai_lines = gsm_lines[ai_settings.starting_line: ai_settings.starting_line + ai_settings.line_count]
    ai_lines.reverse()
    urls = []
    
    ai_ip_lines = App_instance_line_ip.objects.filter(ai = ai, for_callback = True).values('line')
    cb_lines = []
    
    for line in ai_ip_lines:
        cb_lines.append(line['line'])
        
    for ip, module in ai_lines:
        if ip in cb_lines:
            urls.append('{ignore_early_media=true}' + str('sofia/internal/'+module+num+'@'+ip))

    logger.info('gsmgwm url : ' + str(urls))
    return urls

def get_session_url_freetdm(num,  ai=None, action = None):
    logger.info(str(action))
    if str(num).startswith('1800'):
       number = num
    else:
       number = str(num)[-MAX_PHONE_NUM_DIGITS:]
    
    if ai and ai.id in AI_GROUP_MAP.keys():
        if action == 'callback':
            urls = AI_GROUP_MAP[ai.id]['CB_FREETDM_URLS']
        if action == 'obd':
            urls = AI_GROUP_MAP[ai.id]['OBD_FREETDM_URLS']
        if not str(number).startswith('1800'):
           urls = [ url + '0' + number for url in urls]
        else:
           urls = [ url +  number for url in urls ]

       
    else:
        if not str(num).startswith('1800'):
           urls = [ url + '0' + number for url in FREETDM_URLS[SERVER_ID] ]
        else:
           urls = [ url +  number for url in FREETDM_URLS[SERVER_ID] ] 

    random.shuffle(urls)
    
    if ai and hasattr( ai, 'ai_max_line' ):
        max_lines = ai.ai_max_line.max_lines
        urls = urls[ :max_lines ]
    return urls

def get_session_url_devel(num, ai = None):
    #return ['sofia/internal/1001%']
    return ['sofia/internal/'+ str(num) + '@127.0.0.1']

#if vapp.settings.IS_PRODUCTION:
#    try:
#        #interface = config.get('system', 'interface').lower()
#        #if interface == 'gsm':
#        if ai and ai.is_gsm:
#            get_session_url = get_session_url_gsmgws
#        #elif interface in ['tdm', 'freetdm', 'pri', 'bri']:
#        else:
#            get_session_url = get_session_url_freetdm
#    except:
#        logger.info('exception in get_session_url')
#        get_session_url = get_session_url_freetdm
#else:
#    get_session_url = get_session_url_devel
#    

def get_session_url(num, ai = None, action = None):
    logger.info(ai)
    
    if vapp.settings.IS_PRODUCTION:
        try:
            ais = App_instance_settings.objects.get(ai=ai)
            if ais and ais.is_gsm:
                return get_session_url_gsmgws(num, ai)
            else:
                return (get_session_url_freetdm(num, ai, action) + get_session_url_gsmgws(num, ai))
        except:
            return (get_session_url_freetdm(num, ai, action) + get_session_url_gsmgws(num, ai))
    else:
        return get_session_url_devel(num, ai)

def get_connected_session(url, ai):
    try:
        from freeswitch import Session
        return Session(url)
    except:
        from inbound import FreeswitchSession
        session = FreeswitchSession(ai)
        session.originate(url)
        return session

def bridge_sessions(session_1, session_2):
    try:
        import freeswitch
        freeswitch.bridge(session_1, session_2)
    except:
        session_1.bridge(session_2)
        
def get_formatted_number(num):
    logger.info('Number: ' +str(num))
    if str(num).startswith('1800'):
        return num
    number = None
    if num and len(num) >= MAX_PHONE_NUM_DIGITS:
        number = '91' + str(num)[-MAX_PHONE_NUM_DIGITS:]
        return number
    else:
        return num
        
        
              
class RestrictedDict(dict):
    
    DEFAULT_ALLOWED_KEY_TYPES = [ str, unicode, int, long ]
    DEFAULT_ALLOWED_VALUE_TYPES = [ str, unicode, int, long ]
    
    def __setitem__(self, key, value):            
                
      if (( type(key) in RestrictedDict.DEFAULT_ALLOWED_KEY_TYPES ) and ( type(value) in RestrictedDict.DEFAULT_ALLOWED_VALUE_TYPES )):
       super(RestrictedDict, self).__setitem__(key, value)
      else:
        raise KeyError("only %s types allowed in keys and %s types allowed in values"% ( str(RestrictedDict.DEFAULT_ALLOWED_KEY_TYPES), str(RestrictedDict.DEFAULT_ALLOWED_VALUE_TYPES) ))    
    
    @classmethod
    def convert_dict_to_rdict(cls, dictionary, filter = False):        
        
        converted_dictionary = RestrictedDict()  
        
        if not isinstance(dictionary, dict):  
            raise Exception("Invalid type (%s) passed for dictionary parameter, expected: %s"% ( str(type(dictionary)), "dict" ) )
                      
        for key, value in dictionary.items():
            if filter:                    
                if not ( (type(key) in cls.DEFAULT_ALLOWED_KEY_TYPES) and (type(value) in cls.DEFAULT_ALLOWED_VALUE_TYPES) ):                        
                    continue                    
            converted_dictionary[key] = value
                
        return converted_dictionary

    @classmethod
    def convert_rdict_to_dict(cls, restricted_dict):
        dictionary = {}
        for key, value in restricted_dict.items():
            dictionary[key] = value
        return dictionary


def detect_intent_from_audio(audio_file_path):
    session_id = None
    import dialogflow_v2 as dialogflow
    session_client = dialogflow.SessionsClient()
    audio_encoding = dialogflow.enums.AudioEncoding.AUDIO_ENCODING_LINEAR_16
    sample_rate_hertz = 8000
    session = session_client.session_path(settings.PROJECT_ID, session_id)
    with open(audio_file_path, 'rb') as audio_file:
        input_audio = audio_file.read()

    audio_config = dialogflow.types.InputAudioConfig(
        audio_encoding=audio_encoding, language_code=settings.DIALOGFLOW_LANGUAGE_CODE,
        sample_rate_hertz=sample_rate_hertz)
    query_input = dialogflow.types.QueryInput(audio_config=audio_config)

    response = session_client.detect_intent(
        session=session, query_input=query_input,
        input_audio=input_audio)


    query_text = response.query_result.query_text.encode('utf-8')
    detected_intent = response.query_result.intent.display_name.encode('utf-8')
    fullfilment_text = response.query_result.fulfillment_text.encode('utf-8')

    logger.info('Detected Intent' +str(detected_intent))
    logger.info('fullfilment_text'+str(fullfilment_text))
    data_dict = {}
    data_dict["query_text"] = str(query_text)
    data_dict["detected_intent"] = str(detected_intent)
    data_dict["fullfilment_text"] = str(fullfilment_text)
    return response
