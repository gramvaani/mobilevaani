from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import ReadOnlyAuthorization, Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields, http
from tastypie.exceptions import ImmediateHttpResponse

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

from vapp.api import AppInstanceAuthorization, apply_default_checks
from vapp.location.models import Location
from vapp.location.api import LocationResource
from vapp.log import get_request_logger
from vapp.app_manager.decorators import require_lock
from operator import and_, or_
import json
from models import Contact, Contact_list, Role, Contact_list_entity

logger = get_request_logger()

CONTENT_TYPE_USER = ContentType.objects.get_for_model( User )
LIST_NAME_MY_CONTACTS = 'My Contacts'

def create_contact_list_for_user(name, user, role = ''):
    contact_list = Contact_list( name = name )
    contact_list.save()
    create_contact_list_perms_for_user( contact_list, user, role )
    return contact_list

def create_contact_list_perms_for_user( contact_list, user, role = '' ):
    list_entity = Contact_list_entity( list_id = contact_list.id, perm = 'w', content_type = CONTENT_TYPE_USER, object_id = user.id, role = role )
    list_entity.save()
    return list_entity


def get_default_contact_list_for_user(user):
    try:
        list_entity = Contact_list_entity.objects.filter( content_type = CONTENT_TYPE_USER, object_id = user.id, role = Role.DEFAULT )[0]
        return list_entity.list
    except:
        logger.exception( "Encountered in get_default_contact_list for %s" % user )
        return create_contact_list_for_user( LIST_NAME_MY_CONTACTS, user, Role.DEFAULT )

class ContactListResource( ModelResource ):
    count = fields.IntegerField( attribute = 'contacts_count' )

    class Meta:
        queryset = Contact_list.objects.all()
        resource_name = "callerinfo_contact_list"
        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_default_contact_list%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_default_contact_list' ), name = "api_get_default_contact_list" ),
            url(r"^(?P<resource_name>%s)/get_contact_list_locations%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_contact_list_locations' ), name = "api_get_contact_list_locations" ),
        ]

    @apply_default_checks
    def get_contact_list_locations( self, request, **kwargs ):
        contact_list_id = kwargs['contact_list']
        try:
            list_entity = Contact_list_entity.objects.get( list_id = contact_list_id, content_type = CONTENT_TYPE_USER, object_id = request.user.id )
            contacts = list_entity.list.contacts
            location_ids = contacts.filter(location_fk__isnull = False).values_list('location_fk', flat = True).distinct()
            locations = Location.objects.filter( pk__in = location_ids )
            location_resource = LocationResource()
            
            to_be_serialized = {}
            bundles = []
            for location in locations:
                bundle = location_resource.build_bundle(obj = location, request = request)
                bundles.append(location_resource.full_dehydrate(bundle, for_list=True))

            to_be_serialized[self._meta.collection_name] = bundles
            to_be_serialized = self.alter_list_data_to_serialize(request, to_be_serialized)
            return self.create_response(request, to_be_serialized)

        except Contact_list_entity.DoesNotExist:
            raise ImmediateHttpResponse( response = http.HttpUnauthorized() )
        except Exception, e:
            logger.exception( "get_default_contact_list: %s" %  str(e)  )
        return self.create_response( request, { 'error': "unable to lookup locations in the contact list" } )


    def save(self, *args, **kwargs):
        ret_val = super( ContactListResource, self ).save( *args, **kwargs )
        try:
            bundle = args[ 0 ]  
            create_contact_list_perms_for_user( ret_val.obj, bundle.request.user )
        except:
            logger.exception( "callerinfo contact_list_resource save" )
        return ret_val

    def get_default_contact_list(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            clist = get_default_contact_list_for_user( request.user )
            clist_resource = ContactListResource()
            clist_bundle = clist_resource.build_bundle( obj = clist, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, clist_resource.full_dehydrate( clist_bundle ) )
        except:
            logger.exception( "get_default_contact_list: %s" % request )
        return self.create_response( request, { 'error': "unable to lookup default contact list" } )

    def get_object_list(self, request):
        return super( ContactListResource, self ).get_object_list( request ).filter( contact_list_entity__content_type = CONTENT_TYPE_USER, contact_list_entity__object_id = request.user.id )

    def hydrate(self, bundle):
        request_method=bundle.request.META['REQUEST_METHOD']

        if request_method == 'POST':
            if bundle.data.has_key( 'contact_ids' ) and bundle.data['contact_ids']:
                contact_ids = bundle.data['contact_ids'].split(",")
                bundle.data['contacts'] = Contacts.objects.filter(id__in = contact_ids)

        return bundle


class ContactResource(ModelResource):
    id = fields.IntegerField( 'id', null = True )
    location_fk = fields.ForeignKey( LocationResource, 'location_fk', null = True )
    contact_lists = fields.ToManyField( ContactListResource, 'contact_list_set', full = True, null = True )

    class Meta:
        queryset = Contact.objects.all()
        resource_name = "callerinfo_contact"
        fields = [ 'id', 'name', 'number', 'gender', 'location', 'location_fk' ]
        ordering = [ 'time', 'name', 'number', 'location' ]
        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/contact_save%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'contact_save' ), name = "api_contact_save" ),
        ]

    def contact_save(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        logger.info(str(request.body))
        contact_dict = json.loads(str(request.body))

        try:
            contact_list_ids = []
            if contact_dict.get('id'):
                contact = Contact.objects.get(pk = contact_dict.get('id') )
            else:
                contact = Contact()
            contact.name = contact_dict.get('name')
            contact.number = contact_dict.get('number')
            contact.gender = contact_dict.get('gender')
            if contact_dict.get('location_fk'):
                contact.location_fk = Location.objects.get(id  = contact_dict.get('location_fk'))
	    contact.save()

	    logger.info(str(contact))

            if contact_dict.has_key('clist_ids'):
                c_lists = Contact_list.objects.filter(contacts = contact)
                for c_list in c_lists:
                    logger.info(str(c_list))
                    c_list.contacts.remove(contact)

		if contact_dict.get('clist_ids'):
                    contact_list_ids = contact_dict['clist_ids'].split(",")
                    for clist_id in contact_list_ids:
                        c_list = Contact_list.objects.get(id = clist_id)
                        c_list.contacts.add(contact)

            get_default_contact_list_for_user( request.user ).contacts.add(contact)                        

        except Exception as e:
            logger.exception( "Error in saving contact: " +str(e) )
            return self.create_response( request, { 'error': "unable to lookup default contact list" } )

        return self.create_response( request, {'success': 'Contact saved successfully'} )

    def save(self, *args, **kwargs):
        ret_val = super( ContactResource, self ).save( *args, **kwargs )
        try:
            bundle = args[ 0 ]
            logger.info(str(bundle.obj))
            get_default_contact_list_for_user( bundle.request.user ).contacts.add( bundle.obj )
        except:
            logger.exception( "callerinfo contact_resource save" )
        return ret_val

    def get_object_list(self, request):
        return super( ContactResource, self ).get_object_list( request ).filter( contact_list__contact_list_entity__perm='w', contact_list__contact_list_entity__content_type = CONTENT_TYPE_USER, contact_list__contact_list_entity__object_id = request.user.id ).distinct()

    def dehydrate( self,bundle ):
        req = bundle.request
        if( req.user.is_anonymous() ):
            del bundle.data['number'] 
        return bundle

    def hydrate(self, bundle):
        request_method=bundle.request.META['REQUEST_METHOD']

        if request_method == 'POST':
            if bundle.data.has_key( 'clist_ids' ) and bundle.data['clist_ids']:
                contact_list_ids = bundle.data['clist_ids'].split(",")
                bundle.data['contact_lists'] = Contact_list.objects.filter(id__in = contact_list_ids)
        return bundle

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(ContactResource, self).build_filters( filters )
        if "contact_list" in filters:
            orm_filters[ "contact_list" ] = filters[ "contact_list" ]
        if "search_names" in filters:
            orm_filters[ "q_filters" ] = orm_filters.get( 'q_filters', [] ) + [ reduce( or_, (Q( name__icontains = name ) for name in filters["search_names"].split(','))) ]
        if "search_callers" in filters:
            orm_filters[ "number__in" ] = filters["search_callers"].split(',') 
        return orm_filters

    def apply_filters( self, request, applicable_filters ):

        q_filters = applicable_filters.pop( 'q_filters', None )
        semi_filtered = super( ContactResource, self).apply_filters( request, applicable_filters )

        if q_filters:
            semi_filtered = semi_filtered.filter( reduce(and_, q_filters) )

        return semi_filtered
