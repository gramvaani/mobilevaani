from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models.signals import post_save, pre_delete, m2m_changed

from vapp.app_manager.models import App_instance, CalleridField
from vapp.customer.models import Project
from vapp.telephony.utils import get_formatted_number
from vapp.events import Event
import vapp.RQMC as RQMC
from location.models import Location
from vapp.caching_service.utils import sync_object_data
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
import json
from log import get_request_logger
from local_settings import SERVER_ID
from vapp.dynamo_utils import sync_contact

logger = get_request_logger()

CALLERID_LENGTH     = 20
CONTACT_NAME_LENGTH = 300
ROLE_LENGTH         = 24
LOCATION_LENGTH     = 128
DEFAULT_CALLERID = '1111111111' 

class Role:
    DEFAULT  = 'default'
    INCOMING = 'incoming'
    
    
class Contact(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    BDAY_ACCURACY_CHOICES = (
        ('Y', 'Year'),
        ('M', 'Month'),
        ('D', 'Day'),
    )
    name    = models.CharField(max_length = CONTACT_NAME_LENGTH)
    gender  = models.CharField(max_length = 1, choices = GENDER_CHOICES, blank = True, null = True)
    #bday    = models.DateField(null = True, blank = True)
    #bday_accuracy = models.CharField(max_length = 1, choices = BDAY_ACCURACY_CHOICES)
    b_year  = models.IntegerField(null = True, blank = True)
    b_month = models.IntegerField(null = True, blank = True)
    b_day   = models.IntegerField(null = True, blank = True)
    email   = models.EmailField(null = True, blank = True)
    number  = CalleridField()
    location= models.CharField(max_length = LOCATION_LENGTH, blank = True)
    location_fk = models.ForeignKey(Location, null = True)

    def contact_lists(self):
        return [list.id for list in self.contact_list_set.all()]
    
    def __unicode__(self):
        return unicode(self.id) + '_' + unicode(self.name) + '_' + unicode(self.number)

    def SyncDynamo(self):
        sync_contact(self)
        sync_object_data(self.__class__.__name__, self.id, status='SUCCESS')


def contact_save_handler(sender, **kwargs):
    try:
        contact = kwargs['instance']
    except Exception as e:
        logger.info("Exception in contact_save_handler for Contact : "+str(e))


def contact_delete_handler(sender, **kwargs):
    contact = kwargs['instance']
    for contact_list in contact.contact_list_set.all():
        contact_list.contacts.remove(contact)
    #RQMC.push_instance(contact, Event.DELETE, extras = ('contact_lists',))

post_save.connect(contact_save_handler, Contact, dispatch_uid = 'callerinfo.contact.save')
pre_delete.connect(contact_delete_handler, Contact, dispatch_uid = 'callerinfo.contact.delete')

class Contact_list(models.Model):
    name    = models.CharField(max_length = CONTACT_NAME_LENGTH)
    contacts= models.ManyToManyField(Contact)
    
    def contacts_count(self):
        return self.contacts.count()

    def get_numbers(self):
        numbers = set()
        for contact in self.contacts.all():
            for num in contact.number.split(','):
                num = num.strip()
                if num:
                    numbers.add(get_formatted_number(num))
        return numbers

    def __unicode__(self):
        return unicode(self.name) + '_' + unicode(self.contacts.count())
    
    def add(self, callerid):
        if callerid and callerid.isdigit():
            if not ( callerid in self.get_numbers() ):
                contact = Contact(name = callerid, number = callerid)
                contact.save()
                self.contacts.add(contact)
            return True
        return False

def contact_list_save_handler(sender, **kwargs):
    contact_list = kwargs['instance']
    object_id = contact_list.id
    model_name = contact_list.__class__.__name__
#    sync_object_data(model_name, object_id)   
    if kwargs['created']:
        return
    #RQMC.push_instance(contact_list, Event.UPDATE, excludes=('contacts',), extras=('contacts_count',))
    
def contact_list_contacts_changed_handler(sender, instance, action, reverse, model, pk_set, **kwargs):
    if action not in ['post_add', 'post_remove']:
        return
    contact_list = instance
    object_id = contact_list.id
    model_name = contact_list.__class__.__name__

    try:
        logger.info("kwargs in contact_list_contacts: "+str(kwargs))
        logger.info('pk_set:'+str(pk_set))
        logger.info('action:'+str(action))
        for contact_id in pk_set:
            contact = Contact.objects.get(pk = contact_id)
            from caching_service.tasks import SyncDynamoTask
            SyncDynamoTask.delay(model_object = contact, payload_type = None, payload = None)
            logger.info("for m2m sync contact_list_contacts: " + str(contact.id))

    except Exception as e:
        sync_object_data(model_name, object_id, 'exception', str(e), status = 'FAILED')
        logger.info("Exception in sending comments on m2m_changed in contact_list_contacts: "+str(e)+str(contact_list.id))

    #RQMC.push_instance(instance, Event.UPDATE, excludes=('contacts',), extras=('contacts_count',))
        
post_save.connect(contact_list_save_handler, Contact_list, dispatch_uid = 'callerinfo.contact_list.save')
m2m_changed.connect(contact_list_contacts_changed_handler, sender = Contact_list.contacts.through, dispatch_uid = 'callerinfo.contact_list.contacts_changed')
        
class Contact_list_entity(models.Model):
    PERM_CHOICES = (
        ('r', 'Read'),
        ('w', 'Write'),
    )
    list        = models.ForeignKey(Contact_list)
    perm        = models.CharField(max_length = 1, choices = PERM_CHOICES)
    role        = models.CharField(max_length = ROLE_LENGTH, blank = True)
    
    content_type= models.ForeignKey(ContentType)
    object_id   = models.PositiveIntegerField()
    entity      = generic.GenericForeignKey('content_type', 'object_id')
   
    def __unicode__(self):
        return unicode(self.list) + '_' + unicode(self.entity)

def contact_list_entity_save_handler(sender, **kwargs):
    cle = kwargs['instance']
    object_id = cle.id
    #RQMC.push_instance(cle, Event.ADD)

def contact_list_entity_delete_handler(sender, **kwargs):
    cle = kwargs['instance']
    #RQMC.push_instance(cle, Event.DELETE)

post_save.connect(contact_list_entity_save_handler, Contact_list_entity, dispatch_uid = 'callerinfo.contact_list_entity.save')
pre_delete.connect(contact_list_entity_delete_handler, Contact_list_entity, dispatch_uid = 'callerinfo.contact_list_entity.delete')
    
class Number_list(models.Model):
    
    def __unicode__(self):
        return unicode('Number_list_') + unicode(self.id)

class Number(models.Model):
    list    = models.ForeignKey(Number_list)
    number  = CalleridField()
    
    class Meta:
        unique_together = ("list", "number")
        
    def __unicode__(self):
        return unicode(self.list) + '_' + unicode(self.number)
    
class Ai_number_list(models.Model):
    ai      = models.ForeignKey(App_instance)
    list    = models.ForeignKey(Number_list)
    role    = models.CharField(max_length = ROLE_LENGTH)
    
    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.list) + '_' + unicode(self.role)

class Number_list_entity(models.Model):
    PERM_CHOICES = (
        ('r', 'Read'),
        ('w', 'Write'),
    )
    
    list = models.ForeignKey(Number_list)
    perm = models.CharField(max_length = 3, choices = PERM_CHOICES)
    role = models.CharField(max_length = ROLE_LENGTH, blank = True)
    
    content_type= models.ForeignKey(ContentType)
    object_id   = models.PositiveIntegerField()
    entity      = generic.GenericForeignKey('content_type', 'object_id')
    
    def __unicode__(self):
        return unicode(self.list) + '_' + unicode(self.entity)
    
    
def get_contact_name( contact ):
    empty_name = "Not available"
    if not contact:
        return empty_name
    callerinfo_contacts = Contact.objects.filter( number__contains=contact ).values_list( 'name', flat=True ).distinct()
    name = ",".join( callerinfo_contacts )
    return name or empty_name

def get_latest_contact_id(number):
    contacts = Contact.objects.filter(number=number)
    if not contacts.exists():
        return
    return contacts.latest('id').id

def get_location_using_contact_id(number):
    contacts = Contact.objects.filter(number=number, location_fk__isnull = False)
    if not contacts.exists():
        return None
    return contacts.latest('id').location_fk

def get_distinct_numbers(contact_list_ids):
    distinct_numbers = set()
    for contact_list_id in contact_list_ids:
        contacts = Contact_list.objects.get(id=contact_list_id).contacts.get_query_set()
        for contact in contacts:
            distinct_numbers.add(contact.number)
            
    return distinct_numbers

class Ai_profile_field(models.Model):
    ai = models.ForeignKey( App_instance )
    field_name = models.CharField( max_length = 100 )
    unique = models.BooleanField( default = False )

    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.field_name)

    @classmethod
    def create_profile_fields( cls, ai, field_names_list, unique_field_name ):
        pf_list = []
        unique_count = 0
        for index, field_name in enumerate( field_names_list ):
            pf, created = Ai_profile_field.objects.get_or_create( ai = ai, field_name = field_name.strip() )
            if created and (field_name.lower() == unique_field_name.lower() ):
                pf.unique = True
                pf.save()
                unique_count = index
            pf_list.append( pf )
        return ( pf_list, unique_count )

class Ai_unique_field_value(models.Model):
    ai_unique_field = models.ForeignKey( Ai_profile_field )
    unique_field_value = models.CharField( max_length = 100 )

class Ai_profile_field_value(models.Model):
    ai_field = models.ForeignKey(Ai_profile_field)
    field_value = models.CharField( max_length = 100 )
    unique_field_value = models.ForeignKey( Ai_unique_field_value )
    
    @classmethod
    def add_values_from_row( cls, ai_fields, field_values_list, unique_field_value ):
        pfv_list = set([])
        for index, ai_field in enumerate( ai_fields ):
            if ai_field.unique:                
                continue
            pfv, created = Ai_profile_field_value.objects.get_or_create( ai_field = ai_field, unique_field_value = unique_field_value, field_value = field_values_list[ index ] )
            pfv_list.add( pfv )
        return list( pfv_list )

class Project_profile_field(models.Model):
    project = models.ForeignKey( Project )
    field_name = models.CharField( max_length = 100 )
    unique = models.BooleanField( default = False )

    def __unicode__(self):
        return unicode(self.project) + '_' + unicode(self.field_name)

    @classmethod
    def create_profile_fields( cls, project, field_names_list, unique_field_name ):
        pf_list = []
        unique_count = 0
        for index, field_name in enumerate( field_names_list ):
            pf, created = Project_profile_field.objects.get_or_create( project = project, field_name = field_name.strip() )
            if created and (field_name.lower() == unique_field_name.lower() ):
                pf.unique = True
                pf.save()
                unique_count = index
            pf_list.append( pf )
        return ( pf_list, unique_count )

class Project_unique_field_value(models.Model):
    project_unique_field = models.ForeignKey( Project_profile_field )
    unique_field_value = models.CharField( max_length = 100 )

class Project_profile_field_value(models.Model):
    project_field = models.ForeignKey(Project_profile_field)
    field_value = models.CharField( max_length = 100 )
    unique_field_value = models.ForeignKey( Project_unique_field_value )
    
    @classmethod
    def add_values_from_row( cls, project_fields, field_values_list, unique_field_value ):
        pfv_list = set([])
        for index, project_field in enumerate( project_fields ):
            if project_field.unique:                
                continue
            pfv, created = Project_profile_field_value.objects.get_or_create( project_field = project_field, unique_field_value = unique_field_value, field_value = field_values_list[ index ] )
            pfv_list.add( pfv )
        return list( pfv_list )
