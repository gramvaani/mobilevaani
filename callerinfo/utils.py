from callerinfo.models import Ai_profile_field, Ai_profile_field_value, Ai_unique_field_value, Project_profile_field, Project_profile_field_value, Project_unique_field_value
from vapp.utils import read_rows_from_file

def extract_ai_profiles_from_file( ai, filepath, unique_field_name, file_format = 'xls' ):
    data_rows_list = read_rows_from_file( filepath, file_format )
    fields = data_rows_list.pop( 0 )

    ai_profile_fields, unique_field_loc = Ai_profile_field.create_profile_fields( ai, fields, unique_field_name )
    for data_row in data_rows_list:
        unique_field_value, created = Ai_unique_field_value.objects.get_or_create( ai_unique_field = ai_profile_fields[ unique_field_loc ], unique_field_value = data_row[ unique_field_loc ] )
        value_entries = Ai_profile_field_value.add_values_from_row( ai_profile_fields, data_row, unique_field_value )
    

def extract_project_profiles_from_file( project, filepath, unique_field_name, file_format = 'xls'):
    data_rows_list = read_rows_from_file( filepath, file_format )
    fields = data_rows_list.pop( 0 )

    project_profile_fields, unique_field_loc = Project_profile_field.create_profile_fields( project, fields, unique_field_name )
    for data_row in data_rows_list:
        unique_field_value, created = Project_unique_field_value.objects.get_or_create( project_unique_field = project_profile_fields[ unique_field_loc ], unique_field_value = data_row[ unique_field_loc ] )
        value_entries = Project_profile_field_value.add_values_from_row( project_profile_fields, data_row, unique_field_value )


'''
Returns a list of lists containing the field names and their respective values in an ordered form:

Eg. [
    
    ['Name', 'Contact Number', 'Gender'],
    ['Mahabal Mahto', '9911991919', 'M']

]

'''
def get_caller_profile_for_project(project, ufv_ids=None):
    profile_fields = Project_profile_field.objects.filter(project=project).order_by('field_name')
    unique_field_value = Project_profile_field.objects.get(project=project, unique=True)
    field_value_list = []
    field_value_list.append([profile_field.field_name for profile_field in profile_fields])
    if ufv_ids is None:
        ufvs = list(Project_unique_field_value.objects.filter(project_unique_field=unique_field_value))
    else:
        ufvs = []
        for ufv_id in ufv_ids:
            ufv = Project_unique_field_value.objects.get(id=ufv_id)
            ufvs.append(ufv)

    field_name_value_list = get_project_profile_field_value(ufvs, field_value_list, profile_fields)
    return field_name_value_list


'''
Returns a dictionary in the following format:

Eg. {
    'fields': <field name list>,
    'values': {
        <unique_field_value>: <field value list>
    }
}

'''
def get_caller_profile_for_ai(ai_id):
    profile_data = {
        'fields': [],
        'values': {}
    }

    profile_fields = Ai_profile_field.objects.filter(ai_id=ai_id)
    if not profile_fields.exists():
        return profile_data

    profile_data['fields'] = [each.field_name for each in profile_fields]
    unique_field = profile_fields.get(unique=True)
    ufvs = list(Ai_unique_field_value.objects.filter(ai_unique_field=unique_field))

    for ufv in ufvs:
        field_values = []
        for profile_field in profile_fields:
            if profile_field.unique:
                f_val = ufv.unique_field_value
                profile_data['values'][f_val] = []
            else:
                profile_value = Ai_profile_field_value.objects.get(ai_field= \
                                        profile_field, unique_field_value=ufv)
                f_val = profile_value.field_value

            field_values.append(f_val)
        profile_data['values'][ufv.unique_field_value] = field_values

    return profile_data

def get_ai_profile_field_value(ufvs, field_value_list, profile_fields):
    for ufv in ufvs:
        field_values = []
        for profile_field in profile_fields:
            if profile_field.unique:            
                f_val = ufv.unique_field_value
            else:
                profile_value = Ai_profile_field_value.objects.get( ai_field = profile_field, unique_field_value = ufv )
                f_val = profile_value.field_value
            field_values.append( f_val )
        field_value_list.append( field_values )
    return field_value_list


def get_project_profile_field_value(ufvs, field_value_list, profile_fields):
    for ufv in ufvs:
        field_values = []
        for profile_field in profile_fields:
            if profile_field.unique:            
                f_val = ufv.unique_field_value
            else:
                profile_value = Project_profile_field_value.objects.get( project_field = profile_field, unique_field_value = ufv )
                f_val = profile_value.field_value
            field_values.append( f_val )
        field_value_list.append( field_values )
    return field_value_list


def populate_profile_for_project( project, filepath, unique_field_name ):
    extract_project_profiles_from_file( project, filepath, unique_field_name, file_format = 'xls' )
