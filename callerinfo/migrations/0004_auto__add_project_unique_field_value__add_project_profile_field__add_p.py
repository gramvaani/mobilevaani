# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Project_unique_field_value'
        db.create_table(u'callerinfo_project_unique_field_value', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_unique_field', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Project_profile_field'])),
            ('unique_field_value', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'callerinfo', ['Project_unique_field_value'])

        # Adding model 'Project_profile_field'
        db.create_table(u'callerinfo_project_profile_field', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.Project'])),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('unique', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'callerinfo', ['Project_profile_field'])

        # Adding model 'Project_profile_field_value'
        db.create_table(u'callerinfo_project_profile_field_value', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_field', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Project_profile_field'])),
            ('field_value', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('unique_field_value', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Project_unique_field_value'])),
        ))
        db.send_create_signal(u'callerinfo', ['Project_profile_field_value'])


    def backwards(self, orm):
        # Deleting model 'Project_unique_field_value'
        db.delete_table(u'callerinfo_project_unique_field_value')

        # Deleting model 'Project_profile_field'
        db.delete_table(u'callerinfo_project_profile_field')

        # Deleting model 'Project_profile_field_value'
        db.delete_table(u'callerinfo_project_profile_field_value')


    models = {
        u'advert.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'ad_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ad_recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'callerinfo.ai_number_list': {
            'Meta': {'object_name': 'Ai_number_list'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Number_list']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24'})
        },
        u'callerinfo.ai_profile_field': {
            'Meta': {'object_name': 'Ai_profile_field'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unique': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'callerinfo.ai_profile_field_value': {
            'Meta': {'object_name': 'Ai_profile_field_value'},
            'ai_field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Ai_profile_field']"}),
            'field_value': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unique_field_value': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Ai_unique_field_value']"})
        },
        u'callerinfo.ai_unique_field_value': {
            'Meta': {'object_name': 'Ai_unique_field_value'},
            'ai_unique_field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Ai_profile_field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unique_field_value': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'location_fk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'callerinfo.contact_list': {
            'Meta': {'object_name': 'Contact_list'},
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['callerinfo.Contact']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'callerinfo.contact_list_entity': {
            'Meta': {'object_name': 'Contact_list_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Contact_list']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        u'callerinfo.number': {
            'Meta': {'unique_together': "(('list', 'number'),)", 'object_name': 'Number'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Number_list']"}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'callerinfo.number_list': {
            'Meta': {'object_name': 'Number_list'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'callerinfo.number_list_entity': {
            'Meta': {'object_name': 'Number_list_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Number_list']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        u'callerinfo.project_profile_field': {
            'Meta': {'object_name': 'Project_profile_field'},
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.Project']"}),
            'unique': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'callerinfo.project_profile_field_value': {
            'Meta': {'object_name': 'Project_profile_field_value'},
            'field_value': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Project_profile_field']"}),
            'unique_field_value': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Project_unique_field_value']"})
        },
        u'callerinfo.project_unique_field_value': {
            'Meta': {'object_name': 'Project_unique_field_value'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_unique_field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Project_profile_field']"}),
            'unique_field_value': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'abstract': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'cover_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cover_image'", 'null': 'True', 'to': u"orm['media.Image_caption_map']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'images'", 'symmetrical': 'False', 'to': u"orm['media.Image_caption_map']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'show_in_ui': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'customer.organization': {
            'Meta': {'object_name': 'Organization'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'customer.project': {
            'Meta': {'object_name': 'Project'},
            'ads': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'project_ads'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['advert.Advertisement']"}),
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'project_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'bd_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'bd_contact'", 'to': u"orm['auth.User']"}),
            'campaigns': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'project_campaigns'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['campaign.Campaign']"}),
            'contact_persons': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'contact_persons'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'contract': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'org': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.Organization']"}),
            'project_value': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'stats_emails': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'project_stats_emails'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'panchayat'", 'null': 'True', 'to': u"orm['location.Panchayat']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Block']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.image': {
            'Meta': {'object_name': 'Image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'media.image_caption_map': {
            'Meta': {'object_name': 'Image_caption_map'},
            'caption': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']"})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'duration': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['callerinfo']