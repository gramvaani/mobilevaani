# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Contact'
        db.create_table('callerinfo_contact', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('b_year', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('b_month', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('b_day', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True)),
            ('number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
        ))
        db.send_create_signal('callerinfo', ['Contact'])

        # Adding model 'Contact_list'
        db.create_table('callerinfo_contact_list', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal('callerinfo', ['Contact_list'])

        # Adding M2M table for field contacts on 'Contact_list'
        db.create_table('callerinfo_contact_list_contacts', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contact_list', models.ForeignKey(orm['callerinfo.contact_list'], null=False)),
            ('contact', models.ForeignKey(orm['callerinfo.contact'], null=False))
        ))
        db.create_unique('callerinfo_contact_list_contacts', ['contact_list_id', 'contact_id'])

        # Adding model 'Contact_list_entity'
        db.create_table('callerinfo_contact_list_entity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('list', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Contact_list'])),
            ('perm', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('callerinfo', ['Contact_list_entity'])

        # Adding model 'Number_list'
        db.create_table('callerinfo_number_list', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('callerinfo', ['Number_list'])

        # Adding model 'Number'
        db.create_table('callerinfo_number', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('list', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Number_list'])),
            ('number', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('callerinfo', ['Number'])

        # Adding unique constraint on 'Number', fields ['list', 'number']
        db.create_unique('callerinfo_number', ['list_id', 'number'])

        # Adding model 'Ai_number_list'
        db.create_table('callerinfo_ai_number_list', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('list', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Number_list'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24)),
        ))
        db.send_create_signal('callerinfo', ['Ai_number_list'])

        # Adding model 'Number_list_entity'
        db.create_table('callerinfo_number_list_entity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('list', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['callerinfo.Number_list'])),
            ('perm', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('callerinfo', ['Number_list_entity'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Number', fields ['list', 'number']
        db.delete_unique('callerinfo_number', ['list_id', 'number'])

        # Deleting model 'Contact'
        db.delete_table('callerinfo_contact')

        # Deleting model 'Contact_list'
        db.delete_table('callerinfo_contact_list')

        # Removing M2M table for field contacts on 'Contact_list'
        db.delete_table('callerinfo_contact_list_contacts')

        # Deleting model 'Contact_list_entity'
        db.delete_table('callerinfo_contact_list_entity')

        # Deleting model 'Number_list'
        db.delete_table('callerinfo_number_list')

        # Deleting model 'Number'
        db.delete_table('callerinfo_number')

        # Deleting model 'Ai_number_list'
        db.delete_table('callerinfo_ai_number_list')

        # Deleting model 'Number_list_entity'
        db.delete_table('callerinfo_number_list_entity')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'callerinfo.ai_number_list': {
            'Meta': {'object_name': 'Ai_number_list'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['callerinfo.Number_list']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24'})
        },
        'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'callerinfo.contact_list': {
            'Meta': {'object_name': 'Contact_list'},
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['callerinfo.Contact']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'callerinfo.contact_list_entity': {
            'Meta': {'object_name': 'Contact_list_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['callerinfo.Contact_list']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        'callerinfo.number': {
            'Meta': {'unique_together': "(('list', 'number'),)", 'object_name': 'Number'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['callerinfo.Number_list']"}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'callerinfo.number_list': {
            'Meta': {'object_name': 'Number_list'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'callerinfo.number_list_entity': {
            'Meta': {'object_name': 'Number_list_entity'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['callerinfo.Number_list']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'perm': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['callerinfo']
