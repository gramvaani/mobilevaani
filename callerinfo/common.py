from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from models import Contact
from log import get_request_logger

logger = get_request_logger()
user_type = ContentType.objects.get_for_model( User )

DEFAULT_CALLERID = '1111111111'

def lookup_contact_for_user(user, number):
    try:
        if not user.id:
            return Contact.objects.filter(number = number, contact_list__contact_list_entity__content_type = user_type)[0]
        return Contact.objects.filter(number = number, contact_list__contact_list_entity__content_type = user_type, contact_list__contact_list_entity__object_id = user.id)[0]
    except:
        return None
