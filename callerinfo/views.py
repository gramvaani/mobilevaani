from django.http import HttpResponse, HttpResponseNotFound
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType

import json
from vapp.app_manager import perms as ai_perms
from vapp import perms

from app_manager.decorators import instance_perm, instance_perm_per_obj, require_lock
from log import get_request_logger

from callerinfo.models import *
from app_manager.models import User_permission    
from location.models import Location
from common import lookup_contact_for_user

logger = get_request_logger()
user_type = ContentType.objects.get_for_model(User)

ERROR_CONTACT_LIST_DUPLICATE_NAME = "error_contact_list_duplicate_name"
ERROR_CONTACT_LIST_DELETING_DEFAULT_LIST = "error_contact_list_deleting_default_list"
ERROR_CONTACT_REMOVING_FROM_DEFAULT_LIST = "error_contact_removing_from_default_list"

LIST_NAME_MY_CONTACTS = 'My Contacts'

def is_contact_list_present_for_user(name, user):
    return Contact_list.objects.filter(name__iexact = name, contact_list_entity__content_type__pk = user_type.id, contact_list_entity__object_id = user.id).exists()

def create_contact_list_for_user(name, user, role = ''):
    contact_list = Contact_list(name = name)
    contact_list.save()
    list_entity = Contact_list_entity(list_id = contact_list.id, perm = 'w', content_type = user_type, object_id = user.id, role = role)
    list_entity.save()
    return contact_list


def get_default_contact_list(user):
    try:
        list_entity = Contact_list_entity.objects.filter(content_type__pk = user_type.id, object_id = user.id, role = Role.DEFAULT)[0]
        return list_entity.list
    except:
        logger.exception("Encountered in get_default_contact_list for " + str(user))
        return create_contact_list_for_user(LIST_NAME_MY_CONTACTS, user, Role.DEFAULT)
    
@login_required
def load_contact_lists(request, start_row, num_rows):
    logger.debug(request)
    get_default_contact_list(request.user)
    user_type = ContentType.objects.get_for_model(User)
    lists = Contact_list.objects.filter(contact_list_entity__content_type__pk = user_type.id, contact_list_entity__object_id = request.user.id)
    
    js = serializers.get_serializer("json")()
    data = js.serialize(lists, excludes = ('contacts',), extras=('contacts_count',))
    
    return HttpResponse(data)

@login_required
def load_contact_list(request, list_id):
    logger.debug(request)
    lists = Contact_list.objects.filter(id = list_id)
    
    js = serializers.get_serializer('json')()
    data = js.serialize(lists, excludes = ('contacts',), extras=('contacts_count',))
    
    return HttpResponse(data)

@login_required
def load_default_contact_list(request):
    logger.debug(request)
    lists = [get_default_contact_list(request.user)]
    
    js = serializers.get_serializer('json')()
    data = js.serialize(lists, excludes = ('contacts',), extras=('contacts_count',))
    
    return HttpResponse(data)    

@login_required
def load_contacts(request, list_id, start_row, num_rows):
    logger.debug(request)
    
    if None in [list_id, start_row, num_rows]:
        return HttpResponseNotFound()
    
    start_row = int(start_row)
    end_row   = start_row + int(num_rows)
    contacts  = Contact_list.objects.get(pk = list_id).contacts.order_by('name')[start_row:end_row]
    
    js = serializers.get_serializer("json")()
    data = js.serialize(contacts, extras = ('contact_lists',))
    
    return HttpResponse(data)

@login_required
def load_contact(request, contact_id):
    logger.debug(request)
    
    if contact_id == None:
        return HttpResponseNotFound()
    
    contact = Contact.objects.filter(id = contact_id)
    js = serializers.get_serializer("json")()
    data = js.serialize(contact, extras = ('contact_lists',))
    
    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
def save_contact(request):
    logger.debug(request)
        
    contact_wrapper = serializers.deserialize('json', request.POST['json']).next()
    contact = contact_wrapper.object
    contact.save()
    
    get_default_contact_list(request.user).contacts.add(contact)
    js = serializers.get_serializer("json")()
    data = js.serialize([contact], extras = ('contact_lists',))
    
    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
def delete_contacts(request):
    logger.debug(request)
    
    for contact_wrapper in serializers.deserialize('json', request.POST['json']):
        try:
            contact_wrapper.object.delete()
        except:
            logger.exception('Encountered while trying to delete contact' + unicode(contact_wrapper.object))
    
    return HttpResponse("ok")

@login_required
@csrf_exempt
@require_POST
def create_contact_list(request):
    logger.debug(request)
    try:
        name = json.loads(request.POST['json'])[0]['name']
        if is_contact_list_present_for_user(name, request.user):
            return HttpResponse(ERROR_CONTACT_LIST_DUPLICATE_NAME)
        else:
            create_contact_list_for_user(name, request.user)
            return HttpResponse("ok")
    except:
        logger.exception("Encountered while creating new contact list")
        return HttpResponseNotFound()
    
@login_required
@csrf_exempt
@require_POST
def rename_contact_list(request):
    logger.debug(request)
    
    name = json.loads(request.POST['json'])[0]['name']
    list_id = json.loads(request.POST['json'])[0]['list_id']
    
    if is_contact_list_present_for_user(name, request.user):
        return HttpResponse(ERROR_CONTACT_LIST_DUPLICATE_NAME)
    else:
        try:
            list = Contact_list.objects.get(pk = list_id)
            if list.name == LIST_NAME_MY_CONTACTS:
                return HttpResponse(ERROR_CONTACT_LIST_DELETING_DEFAULT_LIST)
            list.name = name
            list.save()
            return HttpResponse("ok")
        except:
            logger.exception("Encountered while renaming contact list " + list_id + " to " + name)
            return HttpResponseNotFound()

@login_required
@csrf_exempt
@require_POST
def delete_contact_list(request):
    logger.debug(request)
    
    list_wrapper = [wrapper for wrapper in serializers.deserialize('json', request.POST['json'])][0]
    list = list_wrapper.object
    if list.name == LIST_NAME_MY_CONTACTS:
        return HttpResponseNotFound(ERROR_CONTACT_LIST_DELETING_DEFAULT_LIST)
    
    try:
        list.delete()
        return HttpResponse("ok")
    except:
        logger.exception("Encountered while deleting list " + str(list))
        return HttpResponseNotFound()

@login_required
@csrf_exempt
@require_POST
def update_contact_list_members(request):
    logger.debug(request)
    
    logger.debug(request.POST['json'])
    dict = json.loads(request.POST['json'])[0]
    list_id = dict['list_id']
    op = dict['op']
    contact_ids = dict['contact_ids'].split(",")
    
    list = Contact_list.objects.get(pk = list_id)
    if list.name == LIST_NAME_MY_CONTACTS:
        return HttpResponseNotFound(ERROR_CONTACT_REMOVING_FROM_DEFAULT_LIST)
    if op == "del":
        list.contacts.remove(*contact_ids)
    else:
        contact_ids = [id for id in contact_ids if not list.contacts.filter(id = id).exists()]
        list.contacts.add(*contact_ids)
    return HttpResponse("ok")

@login_required
def lookup_contact(request, number):
    logger.debug(request)

    contact = lookup_contact_for_user(request.user, number)
    if contact is None:
        data = "[null]"
    else:
        js = serializers.get_serializer("json")()
        data = js.serialize([contact], extras = ('contact_lists',))
        
    return HttpResponse(data)


@login_required
def get_locations_data(request):            
    
    ais = perms.user_perms_ai_ids(request.user.id, ai_perms.app_use)   
    locations = list(Location.objects.filter(ai__in = ais))
    locations.sort(key=str)
    js = serializers.get_serializer("json")()
    data = js.serialize(locations, extras=('__unicode__',), excludes=('country','state','district','block'))
    return HttpResponse(data)

        
    
