from django.contrib import admin
from callerinfo.models import *

admin.site.register(Contact_list_entity)

class ContactAdmin(admin.ModelAdmin):
    search_fields = ['name', 'number']
admin.site.register(Contact, ContactAdmin)
