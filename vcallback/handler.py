import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'vapp.settings'
from datetime import datetime

import vapp.settings
import vapp.app_manager
from vapp.app_manager.models import *
#from freeswitch import *

from telephony.utils import *
from log import get_logger
from vcallback.models import *
from app_manager.models import App_instance

import RQMC

import time
from datetime import datetime

def in_handler(session, args):
    
    logger.debug('Running voice callback at : ' + str(datetime.now()))
        
    callback_item = args['schedule']
    
    try:
        if callback_item and callback_item.play_time <= datetime.now() and not callback_item.success:
                    
            file_path = callback_item.call_template.vrecording.get_full_filename()
            logger.debug('in handler: %s' %(file_path))
            play_file(session,file_path)   
            
            callback_item.success = True
            callback_item.save()
            
    except:
        logger.debug('exception in handler of vcallback')
