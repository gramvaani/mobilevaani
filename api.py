from django.db.models.constants import LOOKUP_SEP
from django.db.models.sql.constants import QUERY_TERMS
from django.http import QueryDict

from tastypie import http
from tastypie.authorization import Authorization, ReadOnlyAuthorization
from tastypie.exceptions import Unauthorized, ImmediateHttpResponse

from vapp.perms import user_has_perms
from vapp.log import get_request_logger

import vapp.app_manager.perms as app_manager_perms
from vapp.mnews.models import Transient_group_schedule, News_settings

logger = get_request_logger()

class AppInstanceAuthorization(Authorization):
    logger.info('Inside APP authorization')
    """docstring for AppInstanceAuthorization"""
    def __init__( self, read_perms= [app_manager_perms.app_use], add_perms = [app_manager_perms.app_use], 
                    change_perms = [app_manager_perms.app_use], delete_perms = [app_manager_perms.app_use],
                    ai_field = 'ai' ):
        super(AppInstanceAuthorization, self).__init__()
        self.read_perms     = read_perms
        self.add_perms      = add_perms
        self.change_perms   = change_perms
        self.delete_perms   = delete_perms
        self.ai_field       = ai_field
        logger.info(self.ai_field)

    def anonymous_has_perms(self, ai):
        ai = News_settings.objects.filter( ai = ai, allow_public_access = True )
        return ai.exists()

    def is_read_allowed( self, bundle):
        logger.info('Inside is read allow')
        user = bundle.request.user
        logger.info(str(user)) 
        ai = bundle.request.REQUEST[ self.ai_field ]
        logger.info(ai)
        if (not user.is_anonymous() and user_has_perms( user, ai, *self.read_perms )) or  self.anonymous_has_perms( ai ):
            logger.info('about to return true')
            return True
        logger.info('about to return false')
        return False

    def read_list(self, object_list, bundle):
        try:
            if self.is_read_allowed( bundle ):            
                return object_list
        except:
            logger.exception( "Unable to read_list for bundle: %s" % bundle )
        return []

    def read_detail(self, object_list, bundle):
        try:
            if self.is_read_allowed( bundle ):
                return True
        except:
            logger.exception( "Unable to read_detail for bundle: %s" % bundle )
        return False

    def create_list(self, object_list, bundle):
        return []

    def create_detail(self, object_list, bundle):
        return False

    def update_list(self, object_list, bundle):
        return []

    def update_detail(self, object_list, bundle):
        try:
            user = bundle.request.user
            ai = bundle.data[ self.ai_field ]
            if ai.startswith('/'):
                ai = ai.split("/")[-2]
            return user_has_perms( user, ai, *self.change_perms )
        except:
            logger.exception( "Unable to update_detail for bundle: %s" % bundle )
        return False

    def delete_list(self, object_list, bundle):
        return []

    def delete_detail(self, object_list, bundle):
        return False


class ScheduleAuthorization(Authorization):
    def __init__(self, read_perms = [app_manager_perms.app_use], add_perms = [app_manager_perms.app_use], 
                    change_perms = [app_manager_perms.app_use], delete_perms = [app_manager_perms.app_use],
                    schedule_field = 'schedule_id'):
        super(ScheduleAuthorization, self).__init__()
        self.read_perms     = read_perms
        self.add_perms      = add_perms
        self.change_perms   = change_perms
        self.delete_perms   = delete_perms
        self.schedule_field = schedule_field


    def has_perms(self, schedule_id, user, perms):
        ai_group = Transient_group_schedule.objects.get(id = schedule_id).ai_group
        ai = ai_group.vi_reference.ai if ai_group.vi_reference else ai_group.ai

        return user_has_perms( user, ai, perms )
        

    def read_list(self, object_list, bundle):
        try:
            user = bundle.request.user
            schedule_id = bundle.request.REQUEST[ self.schedule_field ]
            if self.has_perms(schedule_id, user, *self.read_perms):
                return object_list
        except:
            logger.exception( "Unable to read_list for bundle: %s" % bundle )
        return []

    def read_detail(self, object_list, bundle):
        return False

    def create_list(self, object_list, bundle):
        return []

    def create_detail(self, object_list, bundle):
        return False

    def update_list(self, object_list, bundle):
        return []

    def update_detail(self, object_list, bundle):
        return False

    def delete_list(self, object_list, bundle):
        return []

    def delete_detail(self, object_list, bundle):
        return False

class AppManageAuthorization(Authorization):
    def __init__(self, perms= [app_manager_perms.app_manage], ai_field = 'ai'):
        super(AppManageAuthorization, self).__init__()
        self.perms     = perms
        self.ai_field  = ai_field

    def is_access_allowed( self, bundle):
        user = bundle.request.user
        try:
            ai = bundle.request.REQUEST[ self.ai_field ]
        except:
            if bundle.data.has_key( self.ai_field  ) and bundle.data[ self.ai_field ]:
                ai = bundle.data[ self.ai_field ]
            else:
                logger.exception( "Unable to read ai from bundle: %s" % bundle )

        if user_has_perms( user, ai, *self.perms ):
            return True
        return False

    def read_list(self, object_list, bundle):
        try:
            if self.is_access_allowed( bundle ):
                return object_list
        except:
            logger.exception( "Unable to read_list for bundle: %s" % bundle )
        raise Unauthorized("Sorry, Operation not allowed")

    def read_detail(self, object_list, bundle):
        try:
            return self.is_access_allowed( bundle )
        except:
            logger.exception( "Unable to read_detail for bundle: %s" % bundle )
        raise Unauthorized("Sorry, Operation not allowed")

    def create_list(self, object_list, bundle):
        raise Unauthorized("Sorry, Operation not allowed")

    def create_detail(self, object_list, bundle):
        try:
            return self.is_access_allowed( bundle )
        except:
            logger.exception( "Unable to create_detail for bundle: %s" % bundle )
        raise Unauthorized("Sorry, Operation not allowed")

    def update_list(self, object_list, bundle):
        raise Unauthorized("Sorry, Operation not allowed")

    def update_detail(self, object_list, bundle):
        try:
            return self.is_access_allowed( bundle )
        except:
            logger.exception( "Unable to update_detail for bundle: %s" % bundle )
        raise Unauthorized("Sorry, Operation not allowed")

    def delete_list(self, object_list, bundle):
        raise Unauthorized("Sorry, Operation not allowed")

    def delete_detail(self, object_list, bundle):
        try:
            return self.is_access_allowed( bundle )  
        except:
            logger.exception( "Unable to delete_detail for bundle: %s" % bundle )
        raise Unauthorized("Sorry, Operation not allowed")


permission_codes = {
    'get' : 'read_perms',
    'post': 'add_perms',
    'put': 'change_perms',
    'delete': 'delete_perms',
}

def is_inbuilt_authorization( authorization ):
    inbuilt_auths = [ Authorization, ReadOnlyAuthorization ]
    return type(authorization) in inbuilt_auths

def inbuilt_auth_check( resource, request, authorization ):
    if ( type(authorization) is Authorization ):
        return True
    elif ( type(authorization) is ReadOnlyAuthorization ) and resource.method_check( request, allowed = ['get'] ):
        return True
    return False

def custom_auth_check( resource, request, authorization, allowed_methods ):
    user = request.user
    request_method = resource.method_check( request, allowed = allowed_methods )
    perms = getattr( authorization, permission_codes[ request_method ], None  )

    if perms is None:
        raise ImmediateHttpResponse( response = http.HttpNotImplemented() )

    if ( type( authorization ) is AppInstanceAuthorization ) :
        ai = request.REQUEST[ authorization.ai_field ]
        if user_has_perms( user, ai, *perms ):
            return True
    elif ( type( authorization ) is ScheduleAuthorization ):
        schedule = request.REQUEST[ authorization.schedule_field ]
        if authorization.has_perms(schedule, user, *perms):
            return True
    else:
        raise ImmediateHttpResponse( response = http.HttpNotImplemented() )

    return False


def custom_build_filters( resource, request, allowed_methods ):
    filters = QueryDict('', mutable = True)
    for allowed_method in allowed_methods:
        allowed_method = allowed_method.upper()
        if hasattr(request, allowed_method):
            filters.update( getattr(request, allowed_method).copy() )
    
    if request.FILES and 'post' in allowed_methods:
        filters.update( request.FILES.copy() )

    if filters:
        qs_filters = {}
        query_terms = QUERY_TERMS

        for filter_expr, value in filters.items():
            filter_bits = filter_expr.split(LOOKUP_SEP)
            field_name = filter_bits.pop(0)
            filter_type = None
            if len(filter_bits) and filter_bits[-1] in query_terms:
                filter_type = filter_bits.pop()
            value = resource.filter_value_to_python(value, field_name, filters, filter_expr, filter_type)
            qs_filter = "%s%s%s" % (field_name, LOOKUP_SEP, filter_type) if filter_type else "%s" % field_name
            qs_filters[ qs_filter ] = value
        return qs_filters
    return filters

def apply_default_checks( *decorator_args, **decorator_kwargs ):
    no_args = False
    if len( decorator_args ) == 1 and not decorator_kwargs and callable( decorator_args[0] ):
        func = decorator_args[0]
        no_args = True

    def wrap( custom_api ):
        def func_wrap( resource, request, **kwargs ):
            resource.is_authenticated( request )
            resource.throttle_check( request )
            authorization = decorator_kwargs.get('authorization', resource._meta.authorization)
            allowed_methods = decorator_kwargs.get('allowed_methods', resource._meta.allowed_methods)

            if is_inbuilt_authorization( authorization ):
                is_authorized = inbuilt_auth_check( resource, request, authorization )
            else:
                is_authorized = custom_auth_check( resource, request, authorization, allowed_methods )

            if is_authorized:
                resource.log_throttled_access( request )
                filters = custom_build_filters( resource, request, allowed_methods )
                kwargs.update( filters )
                response = custom_api( resource, request, **kwargs )
            else:
                raise ImmediateHttpResponse( response = http.HttpUnauthorized() )
            return response
        return func_wrap

    if no_args:
        return wrap(func)
    else:
        return wrap



