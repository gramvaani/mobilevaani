from datetime import datetime, timedelta
from vapp.telephony.statemachine import BaseVappController, NO_REDIAL_CAUSES, HUPCAUSE
from mnews.tasks import CallAndPlayTask
from app_manager.tasks import CallbackTask
from app_manager.models import *
from app_manager.common import get_queue_for_task
from vapp.local_settings import SERVER_ID, CUSTOM_PROMPT_AI
from os import path

from vapp.log import get_logger
logger = get_logger()


class CallAndPlayController(BaseVappController):
    
    def __init__(self, ai, sessionData, vi_data):
        self.vi_data = vi_data
        self.max_tries = vi_data['max_tries']
        self.call_log = vi_data['group_call_log']
        try:
           self.is_trunk = vi_data['is_trunk']
        except:
           self.is_trunk = False

        self.call_log.tries += 1
        self.call_log.save()
        self.vi_ref = vi_data.get('vi_ref')
        self.unknown_orig_err_tries = 0
        self.max_unknown_orig_err_tries = 2
        logger.debug('CallAndPlayController init: vi_data=%s, max_tries=%s, gcl_id=%s ' %(self.vi_data, self.max_tries, self.call_log.id))
        super(CallAndPlayController, self).__init__( ai, sessionData, vi_data )

    def pre_outgoingstart(self):
        if self.is_trunk:
           call_params_dict = self.getTrunkStartCallParams(self.sessionData.callerid, Cdr_trigger.CALLANDPLAY)
           self.call_log.last_cdr = Cdr.objects.get(pk = call_params_dict['cdr_id'])
           self.call_log.save()
        else:    
           call_params_dict = self.getStartCallParams( self.sessionData.callerid, Cdr_trigger.CALLANDPLAY )
           self.call_log.last_cdr = Cdr.objects.get(pk = call_params_dict['cdr_id'])
           self.call_log.save()
           logger.debug('CallAndPlayController outgoingstart: vi_data=%s, max_tries=%s, gcl_id=%s, cdr_id=%s ' %(self.vi_data, self.max_tries, self.call_log.id, call_params_dict['cdr_id']))
        return call_params_dict

    def call_later(self):
        logger.debug('CallAndPlayController call_later: gcl_id=%s, tries:%s, max_tries: %s'%(self.call_log.id, self.call_log.tries, self.max_tries))
        if self.call_log.should_dial( int( self.max_tries ) ):
            time = datetime.now() + timedelta(minutes = 30)
            queue = get_queue_for_task(self.ai, default_queue='push')
            CallAndPlayTask.apply_async([self.call_log.id, self.max_tries, self.vi_data],
                                        eta=time, queue=queue)

    def while_outgoingstart__sm_next_originate_url(self, events, eventData):
        logger.debug('outgoing sm_next_originate_url, gcl_id: %s, tries:%s, max_tries:%s'%(self.call_log.id, self.call_log.tries, self.max_tries))
        logger.info('Inside while outgoing sm next originate url')
        if not self.call_log.should_dial( int( self.max_tries ) ):
            logger.debug('CallAndPlayController sm_next_originate_url should_dial failed for gcl_id %s'%(self.call_log.id))
            return 'sm_action_failure'

        hangup_cause = eventData.get('Hangup_Cause', '')
            

       
        logger.debug('hangup_cause not in normal uns nor user busy')
        self.call_log.tries += 1
        self.call_log.save()

        return 'sm_next_originate_url'

    def while_outgoingstart__sm_action_failure(self, events, eventData):
        logger.debug('inside outgoingstart sm_action_failure, gcl_id: %s, tries:%s, max_tries:%s'%(self.call_log.id, self.call_log.tries, self.max_tries))
        if not self.call_log.should_dial( int( self.max_tries ) ):
            logger.debug('CallAndPlayController sm_action_failure should_dial failed for gcl_id %s'%(self.call_log.id))

        hangup_cause = eventData.get('Hangup_Cause')
        logger.debug('gcl_id: %s, hangup_cause: %s'%(self.call_log.id, hangup_cause))
        if hangup_cause and hangup_cause not in NO_REDIAL_CAUSES:
            logger.debug('gcl_id: %s sm_action_failure calling later'%(self.call_log.id))
            self.call_later()

        return 'sm_action_failure'
    
    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.call_log.success = True
        self.call_log.save()
        logger.debug('CallAndPlayController outgoingstart sm_action_success: gcl_id=%s, cdr_id=%s, success=%s ' %(self.call_log.id, self.sessionData.cdrs[ 0 ].id, self.call_log.success))
        return 'sm_action_success'

    def while_checkprompt__sm_action_success(self, events, eventData):
        logger.debug('checkprompt sm_action_success')
        logger.info(self.ai.id)
        if self.call_log.group_schedule.prompt_file:
            return 'callandplay_play_prompt'
        else:
            return 'callandplay_no_prompt'

    def while_checkjump__sm_action_success(self, events, eventData):
        if self.call_log.group_schedule.play_ai==True:
            return 'play_ai'
        else:
            return 'sm_action_failure'

    def pre_playprompt(self):
        callerid = str(self.sessionData.callerid)[-10:]
        ai_id = int(self.ai.id)
        prompt_file_path = "/usr/local/voicesite/fsmedia/custom/"+str(ai_id)+"/"+callerid+".mp3"
        logger.info(str(prompt_file_path))
        logger.info(str(CUSTOM_PROMPT_AI))
        if SERVER_ID=="TPH4" and ai_id in CUSTOM_PROMPT_AI and path.exists(prompt_file_path):
            logger.info('custom play')
            return self.getPlaybackParams(prompt_file_path)
        else:
            return self.getPlaybackParams(self.call_log.group_schedule.get_prompt_file())

    def while_jump__sm_action_success(self, events, eventData):
        if self.vi_ref:
            logger.debug('CallAndPlayController vi_ref: gcl_id=%s, cdr_id=%s ' %(self.call_log.id, self.sessionData.cdrs[ 0 ].id))
            return '@' + self.vi_ref.reference
        else:
            return '@ai_' + str(self.ai.id) + '_welcome'


CallAndPlayStateDescriptionMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checkprompt':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name': 'playprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checkjump': ['sm_action_success'],
            },
        },
    {   'name': 'jump',
        'action': 'none',
        'transitions' : {}
        },
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
        },

    {  'name':'checkprompt',
       'action':'none',
       'transitions' :{
        'playprompt':['callandplay_play_prompt'],
        'stop':['sm_action_failure'],
        'checkjump':['callandplay_no_prompt'], 
        },
    },
    { 'name' : 'checkjump',
       'action' :'none',
       'transitions':{
        'jump' : ['play_ai'],
        'stop' :['sm_action_failure'],
        }
    },
]



class CallBackController(BaseVappController):
    
    def __init__(self, ai, sessionData, vi_data):
        self.callback_info = Callback_info.objects.get(id = vi_data['callback_info_id'])
        self.unknown_orig_err_tries = 0
        self.max_unknown_orig_err_tries = 2
        self.delayed_dials_allowed = vi_data.get('delayed_dials_allowed')
        super(CallBackController, self).__init__( ai, sessionData )

    def call_later(self):
        self.delayed_dials_allowed -= 1
        if self.delayed_dials_allowed:
            queue = get_queue_for_task(self.ai)
            CallbackTask.apply_async([self.callback_info.pk], {'delayed_dials_allowed': self.delayed_dials_allowed}, 
                                     queue=queue, eta=datetime.now()+timedelta(seconds=60))


    def pre_outgoingstart(self):
        call_params_dict = self.getStartCallParams( self.sessionData.callerid, trigger = Cdr_trigger.CALLBACK )
        cdr_in_out_map = Cdr_in_out_map(cdr = self.sessionData.cdrs[0], call_in_cdr = self.callback_info.call_in_cdr)
        cdr_in_out_map.save()
        return call_params_dict

    def pre_welcome(self):
        logger.info("dibyendu: app_manager vi pre_welcome start")
     
    def while_welcome__sm_action_success(self, events, eventData):
        logger.info("dibyendu: app_manager vi while_welcome__sm_action_success start")
        return '@ai_' + str(self.ai.id) + '_welcome'

    def while_outgoingstart__sm_next_originate_url(self, events, eventData):
        hangup_cause = eventData.get('Hangup_Cause', '')
        if hangup_cause == HUPCAUSE.NORMAL_UNSPECIFIED:
            self.unknown_orig_err_tries += 1

        if self.unknown_orig_err_tries == self.max_unknown_orig_err_tries:
            self.call_later()
            return 'sm_action_failure'
            
        if hangup_cause == HUPCAUSE.USER_BUSY:    
            self.call_later()
            return 'sm_action_failure'

        return 'sm_next_originate_url'
    

    def while_outgoingstart__sm_action_failure(self, events, eventData):
        hangup_cause = eventData.get('Hangup_Cause')
        if hangup_cause and hangup_cause not in NO_REDIAL_CAUSES:
            self.call_later()
            
        return 'sm_action_failure'


CallBackStateDescriptionMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name': 'welcome',
        'action': 'none',
        'transitions' : {}
        },
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
        },
    ]
