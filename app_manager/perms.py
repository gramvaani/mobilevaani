'''
Created on 06-Jan-2011

@author: bchandra
'''

PERMS = (
         'app_use',
         'recv_stats_email',
         'recv_adv_stats',
         'recv_CEDPA_daily_xls',
         'recv_refer_stats',
         'view_stats',
         'recv_ad_imp_stats',
         'app_manage',
         'app_create',
         )  
