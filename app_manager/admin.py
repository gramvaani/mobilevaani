from django.contrib import admin

from models import *

admin.site.register(Application)

class App_instance_admin(admin.ModelAdmin):
    list_filter = ('status', 'app__pkg_name')
    search_fields = ['name', 'id']
admin.site.register(App_instance, App_instance_admin)

class App_instance_settings_admin(admin.ModelAdmin):
    list_filter = ('ai__status', 'ai__app__pkg_name')
    search_fields = ['ai__name', 'ai__id']
admin.site.register(App_instance_settings, App_instance_settings_admin)


admin.site.register(VI_conf)

class VIReferenceAdmin(admin.ModelAdmin):
	raw_id_fields = ('ai', 'vi', 'target_ai', 'target_vi')
	search_fields = ('ai__name', 'target_ai__name')
	list_filter = ('ai__status', 'target_ai__status')
admin.site.register(VI_reference, VIReferenceAdmin)

class LineAdmin(admin.ModelAdmin):
    raw_id_fields = ("ai",)
    list_filter = ('ai__status', 'ai__app__pkg_name')
    search_fields = ['ai__name', 'number']
    
admin.site.register(Line, LineAdmin)
admin.site.register(Incoming_line_ai)
admin.site.register(App_instance_quota_filter)
admin.site.register(Rate_limit_filter)
admin.site.register(Schedule_filter)
admin.site.register(Ai_max_line)

admin.site.register(Permission)

class UserPermissionAdmin(admin.ModelAdmin):
    raw_id_fields = ("ai",)
    search_fields = ['user__username', 'user__email', 'ai__name', 'ai__app__pkg_name', 'perm__code']
admin.site.register(User_permission, UserPermissionAdmin)

admin.site.register(Group_permission)
admin.site.register(App_instance_line_ip)
admin.site.register(Ai_field_properties)
admin.site.register(Embedded_ai_map)


class AIFilterAdmin(admin.ModelAdmin):
    fields = ['ai']
    
admin.site.register(App_instance_caller_filter)

admin.site.register(Task_queue_map)

admin.site.register( VI_data )
admin.site.register( VI_data_entry )
admin.site.register( Call_context )
admin.site.register( CAP_context )
admin.site.register( CAPC_numlist )

class AiFilterExcludeAdmin(admin.ModelAdmin):
    filter_horizontal = ('contact_lists',)
admin.site.register(Ai_filter_exclude_list, AiFilterExcludeAdmin)

class ShortCodeAdmin(admin.ModelAdmin):   
    raw_id_fields = ("ai",)  
    list_filter = ('ai__status', 'ai__app__pkg_name') 
    search_fields = ['ai__name', 'number']   

admin.site.register(Short_code, ShortCodeAdmin)
admin.site.register(Data_sync)
admin.site.register(App_version)
admin.site.register(Mnews_app_ai)
admin.site.register( Report_creds )
admin.site.register( Translator_api_cred )
admin.site.register( Ai_translator_api_cred )
admin.site.register( Translation_method )
admin.site.register( Ai_translation_method )
