import os, sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'vapp.settings'
import vapp.settings

paths = [
    '/usr/local/voicesite/vapp',
    '/usr/local/voicesite/vapp/app_manager',
	]

def load_paths():
    for path in paths:
        if path not in sys.path:
            sys.path.append(path)


load_paths()

from vapp.perms import load_perms #@UnresolvedImport

load_perms(__name__)
