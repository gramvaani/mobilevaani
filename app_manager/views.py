from django.template import Context, loader
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.decorators import login_required

from vapp.app_manager.models import *
from vapp.app_manager import perms as app_manager_perms #@UnresolvedImport
from vapp.log import get_request_logger

logger = get_request_logger()

import itertools

@login_required
def userinfo(request):
    logger.info(request)
    js = serializers.get_serializer("json")()
    data = js.serialize(User.objects.filter(pk = request.user.id), ensure_ascii=False, fields=('username'), indent=4)
    return HttpResponse(data)

@login_required
def app_instances(request):
    logger.info(request)
    def get_app_type(self):
        return self.app.pkg_name

    App_instance.app_type = get_app_type

    u_instances = App_instance.objects.filter(user_permission__user = request.user.id, user_permission__perm__code = app_manager_perms.app_use) #@UndefinedVariable
    g_instances = App_instance.objects.filter(group_permission__group__user = request.user.id, group_permission__perm__code = app_manager_perms.app_use) #@UndefinedVariable
    ais = itertools.chain(u_instances, g_instances)
    js = serializers.get_serializer("json")()
    return HttpResponse(js.serialize(ais, ensure_ascii=False, fields=('name', 'status'), indent=4, extras=('app_type', 'prompt_set')))

