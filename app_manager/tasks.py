from django.core.mail import EmailMessage
from celery.task import Task

from models import *
from common import *

from vapp.telephony.utils import *
from vapp.telephony.statemachine import run_state_machine
from vapp.telephony.inbound import FreeswitchSession
from vapp.utils import send_email, load_object, if_dict_values_empty

from vapp.app_manager.common import get_queue_for_task
from vapp.mnews.models import Groups_call_log
from vapp.mnews.tasks import CallAndPlayTask
from vapp.stats.tasks import PopulateSessionDataTask
from local_settings import NO_DIAL_OUT_LIST

from vapp.sms.tasks import SMSTask

from vapp.vmiss.models import HTTP_log

import re
from datetime import datetime, timedelta

import urllib, urllib2, socket, httplib, base64, hashlib, requests, json


class CallbackTask(Task):

    def run(self, callback_info_id, delayed_dials_allowed = 2):
        if callback_info_id.callerid in NO_DIAL_OUT_LIST:
            logger.info('Number is in no dialout list')
            return

        logger = self.get_logger(propagate = True)
        callback_info = Callback_info.objects.get(pk = callback_info_id)
        app_instance = callback_info.ai
        app_instance_settings = App_instance_settings.objects.get( ai = app_instance )
        callback_threshold_mins = app_instance_settings.max_callback_delay

        current_time = datetime.now()
        if  callback_info.call_in_cdr.end_time:
            callout_delay = (current_time - callback_info.call_in_cdr.end_time)
            if callout_delay > timedelta(minutes = callback_threshold_mins):
                logger.info('callback delay %s exceeded the threshold of %s mins.'% (str(callout_delay), str(callback_threshold_mins)))
                delay_log = Callback_delay_log(callback_info = callback_info, callback_threshold = callback_threshold_mins, actual_callout_time = current_time)
                delay_log.save()
                return

        logger.info('Calling ' + callback_info.callerid + ' for ai: ' + unicode(app_instance) + ' cdr:' + str(callback_info.call_in_cdr_id))

        vi_data = { 'callback_info_id': callback_info.id,
                    'delayed_dials_allowed': delayed_dials_allowed,
                    }
        vis = VI_conf.objects.filter(controller = 'app_manager.vi.CallBackController',
                                     description = 'app_manager.vi.CallBackStateDescriptionMap')
        if vis.exists():
            session = run_state_machine( app_instance, False, callback_info.callerid, viConf = vis[0], vi_data = vi_data)
            if not if_dict_values_empty(session.data.app_data):
                PopulateSessionDataTask.delay(session.data)


class IncomingCallTask(Task):

    def run(self, ai_id, callerid, cdr_id, uuid ):
        try:
            if callerid in NO_DIAL_OUT_LIST:
                return
            app_instance = App_instance.objects.get( pk = ai_id )
            session = run_state_machine( app_instance, True, callerid, cdr_id, uuid )
            if not if_dict_values_empty(session.data.app_data):
                PopulateSessionDataTask.delay(session.data)
        except:
            logger.exception("Exception in Incoming task for ai: %s" % ai_id )


class EmailTask(Task):

    def run(self, subject, body, email_list):
        send_email(subject, body, email_list)


class HTTPTask(Task):

    @classmethod
    def create_send_request(cls,ai, url,max_tries=1,eta=datetime.now(), **params):
        http = HTTP_log.create_http(ai,url,**params)
        try:
            HTTPTask.apply_async([http.pk, max_tries], eta = eta)
        except Exception,e:
            logger.exception("Encountered Exception:" + str(e))
        return http

    def request_http(self,url):
        try:
            res = urllib2.urlopen(url)
            res_code = res.getcode()
            logger.info('Response sending http request' + str(res_code))
            if res_code != httplib.OK :
                raise Exception(res_code)
            is_success = True
        except Exception,e:
            logger.exception('Exception in sending http request' + str(e))
            res_code = str(e)
            is_success = False
        return is_success,res_code

    def run(self, http_pk, max_tries):
           http = HTTP_log.objects.get(pk = http_pk)
           http.tries += 1
           is_success,res_code = self.request_http(http.http_req)

           if http.tries < max_tries and not is_success :
                delay = 10
                logger.info('Unable to send the HTTP Request .. retrying in ' + str(delay) + ' minutes')
                eta = datetime.now() + timedelta(minutes = delay)
                HTTPTask.apply_async([http.id, max_tries], eta = eta)

           http.http_res = res_code
           http.sent_time = datetime.now()
           http.save()


def run_schedule(schedule, numbers, eta = datetime.now, max_tries = 3, vi_data = {}):
    ai = schedule.ai_group.ai
    for number in numbers:
        if schedule.onpush_sms_message:
            message = schedule.onpush_sms_message.message
            SMSTask.create_send_msg(ai.id, message, number, eta = eta)

        group_call_log = Groups_call_log(number = number, ai = ai, group_schedule = schedule)
        group_call_log.save()
        logger.info('Scheduling task for: %s' % (group_call_log,))
        queue = get_queue_for_task(ai, default_queue='push')
        CallAndPlayTask.apply_async([group_call_log.id, max_tries, vi_data], eta = eta, queue = queue)


class CallAndPlayTaskV1(Task):

    def run(self, capc_id, callerid):
        
        capc    = CAP_context.objects.get(pk = capc_id)
        ai      = capc.call_context.ai
        vi_conf = capc.call_context.vi_conf
        vi_data = capc.call_context.vi_data.get_dict()
        now     = datetime.now()

        if now > datetime.combine( now, capc.wnd_close_time ):
            return

        run_state_machine( ai, False, callerid, viConf = vi_conf, vi_data = vi_data )

def do_schedule_cap_tasks( capc_numlist_id ):

    capc_nl = CAPC_numlist.objects.get(pk = capc_numlist_id)
    capc = capc_nl.capc
    queue = get_queue_for_task(capc.call_context.ai, default_queue='push')
    numlist = capc_nl.numlist

    wnd_open_time = datetime.combine(datetime.now(), capc.wnd_open_time)

    for n in numlist.number_set.all():
        CallAndPlayTaskV1.apply_async([capc.id, str(n.number)], eta = wnd_open_time, queue = queue)

class ScheduleDataSyncTask(Task):

    def run(self, sync_id, user_id = None, schedule_id = None, uploaded_files_dict = None, **kwargs):

        try:
            logger.debug('sync_id: %s, schedule_id: %s' %(sync_id, schedule_id) )
            ds = Data_sync.objects.get( id = sync_id )
            dsl = Data_sync_schedule_log.objects.get( id = schedule_id) if schedule_id else Data_sync_schedule_log( ds = ds, tries = 0 )
            if not dsl.success and dsl.tries <= ds.max_tries:
                dsl.tries += 1
                dsl.save()
                ds_class = load_object( ds.data_sync_object )
                ds_object = ds_class()
                uploaded_files_dict = RestrictedDict.convert_rdict_to_dict( uploaded_files_dict ) if uploaded_files_dict else uploaded_files_dict
                dsl.success = ds_object.sync( ds, uploaded_files_dict = uploaded_files_dict, **kwargs )
                dsl.save()

                if not dsl.success:
                    kwargs[ 'schedule_id' ] = dsl.id
                    kwargs[ 'user_id' ] = user_id
                    ScheduleDataSyncTask.apply_async( args = [ sync_id ], kwargs = kwargs, eta = datetime.now() + timedelta(minutes = 30) )
            else:
                logger.debug('max_tries limit hit for schedule id: %s' % (schedule_id))

        except Exception,e:
            logger.exception( 'exception while scheduling sync: %s' %(e) )


class PushAppResultsTask( Task ):

    def run( self, cred, data, cdr ):
        try:
            data = json.dumps( data )
            if cdr:
                logger.info('running the push app results task for '+str(cdr.id))
            else:
                logger.info('running the push app results task...')
            response = requests.post( cred.url, data = data, headers = { 'Content-Type': 'application/json' } )
            report = Report_log( cred = cred, cdr = cdr, error_code = response.status_code, report = data )
            report.save()
        except Exception,e:
            logger.info( 'exception while pushing results to remote server:' + str( e ) )
