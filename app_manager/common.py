'''
Created on 27-Jun-2011

@author: bchandra
'''

from models import *
from vapp.log import get_logger
from telephony.utils import get_formatted_number
from vapp.local_settings import AI_GROUP_MAP

logger = get_logger()

base_pkg_name= "vapp"
handler_name = "handler"
sms_handler_name = "sms_handler"
vi_name = "vi"


def is_callback_set(app_instance):
    try:
        logger.info('app_instance for is_callback_set: ' + str(app_instance.id))
        return App_instance_settings.objects.get(ai = app_instance.id).is_callback
        
    except Exception, err:
        logger.info('ai settings not found: ' + str(err))
        return False
    

def call_incoming_handler(app_instance, session, args):
    try:
        app = load_vapp(app_instance)
        app.in_handler(session, args)
    except:
        logger.exception('Error in running app: ' + app_instance.name)


def filters_allow(session, call_response_type):
    ai_id = session.ai.id
    callerid = get_formatted_number(session.getCallerID())
    cdr_id = session.cdr.id
    return filters_allow_ai_caller(ai_id, callerid, cdr_id, call_response_type)


def filters_allow_ai_caller(ai_id, callerid, cdr_id, call_response_type = None):
    try:
        for cl in Ai_filter_exclude_list.objects.get(ai = ai_id).contact_lists.all():
            if callerid in cl.get_numbers():
                return True
    except:
        pass

    for each in App_instance_caller_filter.objects.filter(ai = ai_id):
        filter = each.filter
        is_allowed, cause = filter.allow(ai_id, callerid, cdr_id, each.call_type_for_filter, call_response_type)
        if not is_allowed:
            Caller_filter_log(cdr_id = cdr_id, filter_id = filter.caller_filter_ptr_id, cause = cause).save()
            return False

    return True


def load_vapp(app_instance):
    pkg_name = str(app_instance.app.pkg_name)
    pkg = __import__(base_pkg_name + '.' + pkg_name + "." + handler_name)
    app = getattr(getattr(pkg, pkg_name), handler_name)
    reload(app)
    return app


def load_sms_handler(app_instance):
    pkg_name = str(app_instance.app.pkg_name)
    pkg = __import__(base_pkg_name + '.' + pkg_name + "." + sms_handler_name)
    app = getattr(getattr(pkg, pkg_name), sms_handler_name)
    reload(app)
    return app


def load_module(app_instance, module_name):
    pkg_name = str(app_instance.app.pkg_name)
    pkg = __import__(base_pkg_name + '.' + pkg_name + "." + module_name)
    app = getattr(getattr(pkg, pkg_name), module_name)
    reload(app)
    return app


def is_state_machine_impl(app_instance):
    try:
        return App_instance_settings.objects.get(ai = app_instance.id).is_state_machine_impl
    except:
        return False

def get_queue_for_task(ai, action = None,  default_queue='celery'):
    if ai.id in AI_GROUP_MAP.keys():
        if action == 'callback':
           return AI_GROUP_MAP[ai.id]['CB_QUEUE']
        if action == 'obd':
            return AI_GROUP_MAP[ai.id]['OBD_QUEUE']
    return default_queue
