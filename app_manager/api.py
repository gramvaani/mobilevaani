from django.conf.urls import url
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q

from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization
from tastypie.http import HttpUnauthorized, HttpForbidden, HttpApplicationError
from tastypie.models import ApiKey
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields, http
from tastypie.exceptions import ImmediateHttpResponse, Unauthorized

from vapp.api import ScheduleAuthorization, AppManageAuthorization, AppInstanceAuthorization, apply_default_checks
from vapp.app_manager.models import App_instance, Application, VI_reference, User_permission, Group_permission, Permission, App_instance_settings, Line, Data_sync, App_version, Mnews_app_ai
from vapp.media.models import Prompt_set
from vapp.sms.models import SMS_template
from vapp.app_manager.tasks import run_schedule as tasks_run_schedule
from app_manager.tasks import ScheduleDataSyncTask
from vapp.app_manager import perms as app_manager_perms
from vapp.log import get_request_logger
from vapp.utils import get_py_obj
import ast
import uuid

from vapp.mnews.models import Transient_group_schedule, Ai_transient_group, Days, Transient_group
from vapp.telephony.utils import get_formatted_number, RestrictedDict

from datetime import datetime, timedelta
from decimal import Decimal

logger = get_request_logger()

FILTER_PERMS = { 
                 app_manager_perms.app_use: "use" , 
                 app_manager_perms.app_manage:"manage",
                 app_manager_perms.app_create: "create"
            }

def create_new_api_key(sender, **kwargs):
    if kwargs['created'] and not ApiKey.objects.filter( user = kwargs['instance'] ).exists():
        api_key = ApiKey(user = kwargs['instance'], created = datetime.now() )
        api_key.save()

models.signals.post_save.connect(create_new_api_key, sender=User)

class AnonymousReadAuthentication():
    def is_authenticated(self, request, **kwargs):
        if request.method == 'GET' and request.user.is_anonymous():
            return True
        return False
    def get_identifier(self, request):
        return request.user.username

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        fields = ['first_name', 'last_name', 'email']
        allowed_methods = ['get', 'post']
        resource_name = 'user'
        filtering = {
            'username': 'exact'
        }

    def build_filters( self, filters=None ):
        if filters is None:
            filters = {}

        orm_filters = super(UserResource, self).build_filters( filters )

        #If we need to serve the field username in User, the username in authentication info acts as a filter key. Removing it here.
        if orm_filters.has_key( "username__exact" ):
            orm_filters.pop( "username__exact" )

        if "ai" in filters:
            #Filter users by access to ai.
            orm_filters[ "ai" ] = filters["ai"]
        else:
            #Do not serve unfiltered requests
            orm_filters[ 'pk' ] = "-1"
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        ai_filter = applicable_filters.pop('ai', None)
        semi_filtered = super(UserResource, self).apply_filters( request, applicable_filters )
        filtered_users = []
        if ai_filter:
            user_ids = list(User_permission.objects.filter(ai__id=ai_filter, perm__code=app_manager_perms.app_use).values_list('user', flat=True))
            
            group_user_ids = []
            group_ids = Group_permission.objects.filter(ai__id=ai_filter, perm__code=app_manager_perms.app_use).values_list('group', flat=True)
            for user in User.objects.all():
                user_group_ids = user.groups.all().values_list('id', flat=True)
                if any(user_group_id in group_ids for user_group_id in user_group_ids):
                    group_user_ids.append(user.id)

            all_user_ids = set(user_ids+group_user_ids)
            filtered_users = [user for user in semi_filtered if user.id in all_user_ids]
        return filtered_users

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name='api_login'),
            url(r"^(?P<resource_name>%s)/logout%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
        ]

    def login(self, request, **kwargs):
        logger.info('Login api start')
        logger.debug( request )
        self.method_check(request, allowed=['post'])
        logger.info('method check')
        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))
        username = data.get('username', '')
        password = data.get('password', '')

        user = authenticate(username=username, password=password)
        logger.info(user.username)
        if user:
            if user.is_active:
                logger.info('inside login function')
                login(request, user)

                try:
                    logger.info('Key extraction start') 
                    key = ApiKey.objects.get(user=user)
                    logger.info('key fecthed')
                except ApiKey.DoesNotExist:
                    return self.create_response( request, { 'success': False, 'reason': 'missing key' }, HttpForbidden )

                return self.create_response( request, { 'success': True, 'username': user.username, 'key': key.key } )
            else:
                return self.create_response( request, { 'success': False, 'reason': 'disabled' }, HttpForbidden )
        else:
            return self.create_response( request, { 'success': False, 'reason': 'invalid login', 'skip_login_redir': True }, HttpUnauthorized )

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get','post'])
        if request.user and request.user.is_authenticated():
            logout(request)
            return self.create_response(request, {'success': True})
        else:
            return self.create_response(request, {'success': False}, HttpUnauthorized)


class AppResource(ModelResource):
    class Meta:
        queryset = Application.objects.all()
        resource_name = 'app'
        allowed_methods = ['get']

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class AppInstanceResource(ModelResource):
    class Meta:
        queryset = App_instance.objects.all()
        resource_name = 'app_instance'
        filtering = {
            'id' : ['exact'],
            'app': ['exact'],
            'status': ['exact'],
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
    def prepend_urls(self):
        return [
                url(r"^(?P<resource_name>%s)/create%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create' ), name = "api_create" ),
            ]

    def create(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            app_id = request.REQUEST[ 'app_id' ]
            name = request.REQUEST[ 'name' ]
            assign_perms = [ app_manager_perms.app_use, app_manager_perms.app_manage ]
            ai = App_instance( app_id = app_id, name = name, status = 1 )
            ai.save()
            pkg =  ai.app.pkg_name
            App_instance_settings.create( ai )
            User_permission.create( ai, request.user.id, assign_perms )
            Prompt_set.create_prompts( ai )
            
            try:
                get_py_obj( pkg + '.models.app_specific_creation_tasks')( ai.id, user_id = request.user.id )
            except:
                logger.exception( "No app specific tasks for ai: " + str( ai.id ) )

            ai_resource = AppInstanceResource()
            ai_bundle = ai_resource.build_bundle( obj = ai, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, ai_resource.full_dehydrate( ai_bundle ) )
        except:
            logger.exception( "create_new_instance: %s" % request )
        return self.create_response( request, {'error': "unable to create new instance"} )

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}

        orm_filters = super( AppInstanceResource, self ).build_filters( filters )
        if "app" in filters:
            orm_filters[ "app__name" ] = filters[ "app" ]
        if "perm" in filters:
            orm_filters[ "perm" ] = filters[ "perm" ]
        
        return orm_filters

    def apply_filters( self, request, applicable_filters ):
        if "perm" in applicable_filters:
            perm = applicable_filters.pop( "perm" )
        else:
            perm = None

        semi_filtered = super( AppInstanceResource, self ).apply_filters( request, applicable_filters )

        if perm:
            q_user_instances = Q( user_permission__user = request.user.id, user_permission__perm__code = perm )
            q_group_instances = Q( group_permission__group__user = request.user.id, group_permission__perm__code = perm )
            return semi_filtered.filter( q_user_instances | q_group_instances )
        else:
            return semi_filtered

    def get_object_list(self, request):
        queryset = super(AppInstanceResource, self).get_object_list(request)
        try:
            callerid = request.REQUEST['callerid']
            if callerid:
                q_user_instances = Q(user_permission__callerid=callerid,
                                     user_permission__perm__code=app_manager_perms.app_use)
            return queryset.filter(q_user_instances).distinct()
        except KeyError, e:
            q_user_instances = Q(user_permission__user=request.user.id,
                                     user_permission__perm__code=app_manager_perms.app_use)
            q_group_instances = Q(group_permission__group__user=request.user.id,
                                      group_permission__perm__code=app_manager_perms.app_use)
            return queryset.filter(q_user_instances | q_group_instances).distinct()

    def dehydrate( self, bundle ):
        bundle.data[ 'id' ]       = bundle.obj.id
        bundle.data[ 'app' ]      = bundle.obj.app.name
        bundle.data[ 'app_id' ]   = bundle.obj.app.id
        bundle.data[ 'pkg_name' ] = bundle.obj.app.pkg_name
        bundle.data[ 'name' ]     = bundle.obj.name
        return bundle

class AiTransientGroupResource(ModelResource):
    class Meta:
        _PERMSet = Ai_transient_group.objects.all()
        resource_name = 'ai_transient_group'
        allowed_methods = ['get']

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class ScheduleResource(ModelResource):
    ai = fields.ForeignKey(AppInstanceResource, 'ai_group__ai')
    ai_group = fields.ForeignKey(AiTransientGroupResource, 'ai_group')

    class Meta:
        queryset = Transient_group_schedule.objects.all()
        resource_name = 'schedule'
        filtering = {
            'id': ['exact'],
            'ai': 'exact',
        }
        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/create%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_schedule' ), name = "api_create_schedule" ),
            url(r"^(?P<resource_name>%s)/run%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'run_schedule' ), name = "api_run_schedule" )
        ]

    @apply_default_checks(authorization = AppInstanceAuthorization(), allowed_methods = ['post'])
    def create_schedule(self, request, **kwargs):
        try:
            group, created = Transient_group.objects.get_or_create(group_name = kwargs['group_name'], description = 'Generated from API', generating_code = 'default')
            ai = App_instance.objects.get(pk = kwargs['ai'])
            vi_reference = VI_reference.objects.get(pk = kwargs.get('ref_id')) if kwargs.get('ref_id') else None

            ai_transient_group, created = Ai_transient_group.objects.get_or_create(ai = ai, group = group, vi_reference = vi_reference)

            start_time = kwargs['start_time']
            end_time = kwargs['end_time']
            onpush_sms_message = kwargs.get('message_id')

            tg_schedule = Transient_group_schedule(ai_group = ai_transient_group)
            tg_schedule.name = kwargs['name']
            tg_schedule.prompt_file = kwargs.get('prompt_file')
            tg_schedule.play_ai = kwargs.get('is_play_ai', False)
            start_time = datetime.strptime(start_time, "%H:%M:%S")
            end_time = datetime.strptime(end_time, "%H:%M:%S")
            tg_schedule.start_time = datetime.strftime(start_time, "%H:%M:%S")
            tg_schedule.end_time = datetime.strftime(end_time, "%H:%M:%S")
            tg_schedule.repeat = kwargs['is_repeated']
            tg_schedule.active = kwargs['is_active']
            tg_schedule.onpush_sms_message = SMS_template.objects.get(id = onpush_sms_message) if onpush_sms_message else None
            tg_schedule.save()
            selected_days = Days.objects.filter(name__in = kwargs['days_to_repeat'].split(",")).distinct()
            tg_schedule.day_of_week = selected_days

            tg_bundle = self.build_bundle(obj = tg_schedule, request = request)
            return self.create_response(request, self.full_dehydrate(tg_bundle), response_class = http.HttpCreated)
        except KeyError, e:
            raise ImmediateHttpResponse(http.HttpBadRequest("Insufficient parameters passed"))
        except:
            logger.exception("create_schedule: %s" % request)
        return self.create_response(request, {'error': "unable to create schedule"}, response_class = http.HttpApplicationError)

    @apply_default_checks(authorization = ScheduleAuthorization(), allowed_methods = ['post'])
    def run_schedule(self, request, **kwargs):
        schedule_id = kwargs['schedule_id']
        try:
            schedule = Transient_group_schedule.objects.get(pk = schedule_id)
            number = kwargs['number']

            eta = datetime.now()
            delay = kwargs.get('delay')
            if delay:
                eta += timedelta(seconds = int(delay))

            vi_data = {}
            if kwargs.get('conversation_id'):
                vi_data['conversation_id'] = kwargs.get('conversation_id')

            numbers = [get_formatted_number(each.strip()) for each in number.split(',')]
            tasks_run_schedule(schedule, numbers, eta, vi_data = vi_data)

            tg_bundle = self.build_bundle(obj = schedule, request = request)
            return self.create_response(request,  self.full_dehydrate(tg_bundle))
        except KeyError, e:
            raise ImmediateHttpResponse(http.HttpBadRequest("Insufficient parameters passed"))
        except Exception,e:
            logger.exception("Run schedule: %s" % (e,))
        return self.create_response(request, {'error': "unable to run schedule"}, response_class = http.HttpApplicationError)

    def authorized_read_list(self, object_list, bundle):
        try:
            in_built_auth_result = self._meta.authorization.read_list(object_list, bundle)
            q_user_instances = Q( ai_group__ai__user_permission__user = bundle.request.user.id, ai_group__ai__user_permission__perm__code = app_manager_perms.app_use )
            q_group_instances = Q( ai_group__ai__group_permission__group__user = bundle.request.user.id, ai_group__ai__group_permission__perm__code = app_manager_perms.app_use )
            return in_built_auth_result.filter( q_user_instances | q_group_instances ).distinct()

        except Unauthorized as e:
            self.unauthorized_result(e)
        return []

    def authorized_read_detail(self, object_list, bundle):
        try:
            in_built_auth_check = self._meta.authorization.read_detail(object_list, bundle)
            auth_result = self.authorized_read_list( object_list, bundle )
            if not auth_result.exists():
                raise Unauthorized()
        except Unauthorized as e:
            self.unauthorized_result(e)
        return True

class ViReferenceResource( ModelResource ):
    source_ai = fields.ForeignKey(AppInstanceResource, 'ai')
    target_ai = fields.ForeignKey(AppInstanceResource, 'target_ai')
    
    class Meta:
        queryset = VI_reference.objects.all()
        resource_name = 'app_instance_vi_reference'
        fields = [ 'source_ai' ,'target_ai' ]
        authentication = ApiKeyAuthentication()
        authorization = AppManageAuthorization()

class UserPermsResource( ModelResource ):
    ai = fields.ForeignKey( AppInstanceResource, 'ai', null = True )
    user = fields.ForeignKey( UserResource, 'user' )

    class Meta:
        queryset = User_permission.objects.all()
        resource_name = 'app_instance_user_perms'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        filtering = {
                    'ai' : 'exact',
                    'user' : 'exact',
                }

    def get_object_list(self, request):
        queryset = super( UserPermsResource, self ).get_object_list( request )
        q_user_instances = Q( user = request.user.id, perm__code__in = FILTER_PERMS.keys() )
        return queryset.filter( q_user_instances )

    def dehydrate(self, bundle):
        bundle.data['perms'] = FILTER_PERMS[ bundle.obj.perm.code ]
        if bundle.obj.ai:
            bundle.data['ai'] = bundle.obj.ai.id
        return bundle


class LineResource( ModelResource ):
    ai = fields.ForeignKey( AppInstanceResource, 'ai', full = True )

    class Meta:
        queryset = Line.objects.all()
        resource_name = 'line'
        allowed_methods = [ 'get' ]
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        filtering = {
                    'ai' : ALL_WITH_RELATIONS,
                }
        
class DataSyncResource(ModelResource):
    ai = fields.ForeignKey( AppInstanceResource, 'ai' )

    class Meta:
        queryset = Data_sync.objects.all()
        resource_name = "data_sync"
        filtering = {
             'ai' : ALL_WITH_RELATIONS,
        }
        allowed_methods = [ 'get', 'post' ]
        exclude = ('data_sync_object',)
        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schedule_sync%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'schedule_sync' ), name = "api_schedule_sync" ),
        ]

    def save_file_to_tmp( self, file_name, in_memory_file ):
        tmp_loc = ( "/tmp/%s-%s" %(uuid.uuid1(), in_memory_file.name) )        
        with open(tmp_loc, 'wb+') as destination:
            for chunk in in_memory_file.chunks():
                destination.write(chunk)
        return (file_name, tmp_loc)

    @apply_default_checks( authorization = AppInstanceAuthorization(), allowed_methods = [ 'post' ] )
    def schedule_sync(self, request, **kwargs):
        
        try:
            sync_id = request.REQUEST[ 'id' ]
            uploaded_files = request.FILES
            file_loc_dict = {}
            title = request.REQUEST['title'] if request.REQUEST.has_key('title') else None
            for file_name, file_loc in uploaded_files.iteritems():
                file_name, tmp_loc = self.save_file_to_tmp( file_name, uploaded_files[ file_name ] )
                file_loc_dict[ file_name ] = tmp_loc
            file_loc_dict = RestrictedDict.convert_dict_to_rdict( file_loc_dict )
            
            sync_data = request.REQUEST.get( 'sync_data', "{}" )
            logger.debug('sync_data: %s' %(sync_data))
            sync_data = ast.literal_eval( sync_data )
            
            ScheduleDataSyncTask.delay( sync_id, user_id = request.user.id, uploaded_files_dict = file_loc_dict, title = title, **sync_data )
            return self.create_response( request, { 'success': True } )

        except Exception,e:
            exception_str = 'exception caught while schedule_sync id %s: %s' %(sync_id, e) 
            logger.exception( exception_str )
        return self.create_response( request, { 'error': exception_str } )


class AppVersionResource(ModelResource):    
    class Meta:
        queryset = App_version.objects.all()
        resource_name = "app_version"
        filtering = {
             'id': 'exact',
        }
        allowed_methods = [ 'get' ]
    
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/check_version%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'check_version' ), name = "api_check_version" ),
        ]

    def check_version(self, request, **kwargs):
        logger.debug(request)
        self.method_check(request, allowed=['get'])
        self.throttle_check(request)

        try:
            version_no = str(request.REQUEST['version_no'])
            if not App_version.objects.filter(version_no=version_no).exists():
                raise Exception("incorrect version no provided")

            version = App_version.objects.filter(version_no=version_no)[0]
            latest_version_no, latest_version_url = App_version.get_latest_version_detail()

            ### Check made to carry out smooth upgrade for app 2.9 version to latest version
            unsupported_versions = ["1.0", "2.0", "2.6", "2.7", "2.8", "2.9"]
            if version_no in unsupported_versions:
                latest_version_no = Decimal("1.1")
                ### 1.1 -> a dummy value

            response = {
                'success': True,
                'is_supported': version.supported,
                'version_location_url': version.version_location_url,
                'latest_version': latest_version_no,
                'latest_version_url': latest_version_url
            }
            return self.create_response(request, response)
        except Exception as e:
            exception_str = 'exception while check_version: {0}'.format(e)
            logger.exception(exception_str)
            response = {
                'success': False,
                'is_supported': False,
                'version_location_url': '',
                'error': exception_str
            }
            return self.create_response(request, response, HttpApplicationError)


class MVAppAiResource(ModelResource):
    ai = fields.IntegerField( attribute = "mnews_ai__id" )

    class Meta:
        queryset = Mnews_app_ai.objects.filter( is_active = True )
        resource_name = "mnews_app_ai"
        filtering = {
             'id' : ALL,
        }

        allowed_methods = [ 'get' ]
