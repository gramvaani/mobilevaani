# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Report_creds.policy'
        db.add_column(u'app_manager_report_creds', 'policy',
                      self.gf('django.db.models.fields.IntegerField')(max_length=3, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Report_creds.policy'
        db.delete_column(u'app_manager_report_creds', 'policy')


    models = {
        u'app_manager.ai_callers_call_stats': {
            'Meta': {'object_name': 'Ai_callers_call_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'call_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'caller_id': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'first_call_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.ai_callers_stats_weekly': {
            'Meta': {'object_name': 'Ai_callers_stats_weekly'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'total_callers': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'app_manager.ai_field_properties': {
            'Meta': {'object_name': 'Ai_field_properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'default_field_value': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'model': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'model_name'", 'to': u"orm['contenttypes.ContentType']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'app_manager.ai_filter_exclude_list': {
            'Meta': {'object_name': 'Ai_filter_exclude_list'},
            'ai': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.App_instance']", 'unique': 'True'}),
            'contact_lists': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['callerinfo.Contact_list']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.ai_max_line': {
            'Meta': {'object_name': 'Ai_max_line'},
            'ai': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.App_instance']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_lines': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'})
        },
        u'app_manager.ai_translation_method': {
            'Meta': {'object_name': 'Ai_translation_method'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'translation_method': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Translation_method']"})
        },
        u'app_manager.ai_translator_api_cred': {
            'Meta': {'object_name': 'Ai_translator_api_cred'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Translator_api_cred']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.ai_vi_start_event': {
            'Meta': {'object_name': 'Ai_vi_start_event'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'next_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'next_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'next_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'next_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'prev_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prev_ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'prev_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prev_vi'", 'null': 'True', 'to': u"orm['app_manager.VI_conf']"}),
            'trans_event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Transition_event']"})
        },
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.app_instance_caller_filter': {
            'Meta': {'object_name': 'App_instance_caller_filter'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'call_type_for_filter': ('django.db.models.fields.CharField', [], {'default': "'All'", 'max_length': '12'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'app_manager.app_instance_line_ip': {
            'Meta': {'object_name': 'App_instance_line_ip'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'for_callback': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'})
        },
        u'app_manager.app_instance_quota_filter': {
            'Meta': {'object_name': 'App_instance_quota_filter', '_ormbases': [u'app_manager.Caller_filter']},
            u'caller_filter_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.Caller_filter']", 'unique': 'True', 'primary_key': 'True'}),
            'choice_limit': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'}),
            'quota_period': ('django.db.models.fields.CharField', [], {'default': "'Daily'", 'max_length': '10'}),
            'quota_unit': ('django.db.models.fields.CharField', [], {'default': "'Calls'", 'max_length': '10'})
        },
        u'app_manager.app_instance_settings': {
            'Meta': {'object_name': 'App_instance_settings'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callback_delay': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_callback': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_gsm': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_state_machine_impl': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'max_callback_delay': ('django.db.models.fields.PositiveIntegerField', [], {'default': '30'}),
            'max_duration': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'min_recording_duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'starting_line': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vi_conf': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True', 'blank': 'True'})
        },
        u'app_manager.app_version': {
            'Meta': {'object_name': 'App_version'},
            'addition_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'supported': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'version_location_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'version_no': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.call_context': {
            'Meta': {'object_name': 'Call_context'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vi_conf': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']"}),
            'vi_data': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_data']"})
        },
        u'app_manager.call_event_recorder': {
            'Meta': {'object_name': 'Call_event_recorder'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True'}),
            'event_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'app_manager.callback_delay_log': {
            'Meta': {'object_name': 'Callback_delay_log'},
            'actual_callout_time': ('django.db.models.fields.DateTimeField', [], {}),
            'callback_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Callback_info']"}),
            'callback_threshold': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'app_manager.callback_info': {
            'Meta': {'object_name': 'Callback_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'call_in_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.caller_filter': {
            'Meta': {'object_name': 'Caller_filter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.caller_filter_log': {
            'Meta': {'object_name': 'Caller_filter_log'},
            'cause': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'filter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Caller_filter']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.cap_context': {
            'Meta': {'object_name': 'CAP_context'},
            'call_context': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Call_context']"}),
            'file_play': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'sms_tmpl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']", 'null': 'True', 'blank': 'True'}),
            'wnd_close_time': ('django.db.models.fields.TimeField', [], {}),
            'wnd_open_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'app_manager.cap_log': {
            'Meta': {'object_name': 'CAP_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'filename_play': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_sms_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'num_tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'vi_data': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_data']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'vic_ctrl': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'vic_desc': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.capc_numlist': {
            'Meta': {'object_name': 'CAPC_numlist'},
            'capc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.CAP_context']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Number_list']"})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.cdr_in_out_map': {
            'Meta': {'object_name': 'Cdr_in_out_map'},
            'ai_ip': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance_line_ip']", 'null': 'True'}),
            'call_in_cdr': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'call_in_cdr'", 'null': 'True', 'to': u"orm['app_manager.Cdr']"}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.data_sync': {
            'Meta': {'object_name': 'Data_sync'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'data_sync_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'data_sync_object': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_tries': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'sync_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'app_manager.data_sync_schedule_log': {
            'Meta': {'object_name': 'Data_sync_schedule_log'},
            'ds': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Data_sync']"}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'app_manager.embedded_ai_map': {
            'Meta': {'object_name': 'Embedded_ai_map'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'should_embed_func': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'vi_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_reference']"})
        },
        u'app_manager.group_instance': {
            'Meta': {'object_name': 'Group_instance'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.group_permission': {
            'Meta': {'object_name': 'Group_permission'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Permission']"})
        },
        u'app_manager.http_log': {
            'Meta': {'object_name': 'HTTP_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'http_req': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'http_res': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'app_manager.incoming_line_ai': {
            'Meta': {'object_name': 'Incoming_line_ai'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        u'app_manager.line': {
            'Meta': {'object_name': 'Line'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        u'app_manager.mnews_app_ai': {
            'Meta': {'object_name': 'Mnews_app_ai'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'app_manager.permission': {
            'Meta': {'object_name': 'Permission'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'app_manager.rate_limit_filter': {
            'Meta': {'object_name': 'Rate_limit_filter', '_ormbases': [u'app_manager.Caller_filter']},
            u'caller_filter_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.Caller_filter']", 'unique': 'True', 'primary_key': 'True'}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'per_caller': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'app_manager.report_creds': {
            'Meta': {'object_name': 'Report_creds'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'report_creds_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'cred_key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'policy': ('django.db.models.fields.IntegerField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'app_manager.report_log': {
            'Meta': {'object_name': 'Report_log'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'report_log_cdr'", 'null': 'True', 'to': u"orm['app_manager.Cdr']"}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Report_creds']"}),
            'error_code': ('django.db.models.fields.SmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'app_manager.schedule_filter': {
            'Meta': {'object_name': 'Schedule_filter', '_ormbases': [u'app_manager.Caller_filter']},
            u'caller_filter_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.Caller_filter']", 'unique': 'True', 'primary_key': 'True'}),
            'schedules': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['schedule.Schedule']", 'symmetrical': 'False'})
        },
        u'app_manager.short_code': {
            'Meta': {'object_name': 'Short_code'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'app_manager.task_queue_map': {
            'Meta': {'object_name': 'Task_queue_map'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'queue': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'task': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']"})
        },
        u'app_manager.transition_event': {
            'Meta': {'object_name': 'Transition_event'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'state_counter': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trans_event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True'})
        },
        u'app_manager.translation_method': {
            'Meta': {'object_name': 'Translation_method'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'signature': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'app_manager.translator_api_cred': {
            'Meta': {'object_name': 'Translator_api_cred'},
            'client_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'client_secret_key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'cred_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.user_permission': {
            'Meta': {'unique_together': "(('user', 'ai', 'perm'),)", 'object_name': 'User_permission'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Permission']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'app_manager.user_profile': {
            'Meta': {'object_name': 'User_profile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_data': {
            'Meta': {'object_name': 'VI_data'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_data_entry': {
            'Meta': {'unique_together': "(('vi_data', 'key'),)", 'object_name': 'VI_data_entry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'vi_data': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_data']"})
        },
        u'app_manager.vi_reference': {
            'Meta': {'object_name': 'VI_reference'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'target_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi_data': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'location_fk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'callerinfo.contact_list': {
            'Meta': {'object_name': 'Contact_list'},
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['callerinfo.Contact']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'callerinfo.number_list': {
            'Meta': {'object_name': 'Number_list'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'panchayat'", 'null': 'True', 'to': u"orm['location.Panchayat']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Block']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'schedule.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['schedule.Days']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repetition': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['app_manager']