# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Application'
        db.create_table('app_manager_application', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('desc', self.gf('django.db.models.fields.TextField')(max_length=200)),
            ('pkg_name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal('app_manager', ['Application'])

        # Adding model 'User_profile'
        db.create_table('app_manager_user_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal('app_manager', ['User_profile'])

        # Adding model 'App_instance'
        db.create_table('app_manager_app_instance', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Application'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('conf', self.gf('django.db.models.fields.CharField')(default='default', max_length=32)),
            ('status', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('app_manager', ['App_instance'])

        # Adding model 'App_instance_settings'
        db.create_table('app_manager_app_instance_settings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('is_callback', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('callback_delay', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('starting_line', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('line_count', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True, blank=True)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('app_manager', ['App_instance_settings'])

        # Adding model 'Callback_info'
        db.create_table('app_manager_callback_info', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('callerid', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('app_manager', ['Callback_info'])

        # Adding model 'Line'
        db.create_table('app_manager_line', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal('app_manager', ['Line'])

        # Adding model 'Permission'
        db.create_table('app_manager_permission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Application'], null=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal('app_manager', ['Permission'])

        # Adding model 'User_permission'
        db.create_table('app_manager_user_permission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user', to=orm['auth.User'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('perm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Permission'])),
        ))
        db.send_create_signal('app_manager', ['User_permission'])

        # Adding model 'Group_permission'
        db.create_table('app_manager_group_permission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'])),
            ('perm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Permission'])),
        ))
        db.send_create_signal('app_manager', ['Group_permission'])

        # Adding model 'Group_instance'
        db.create_table('app_manager_group_instance', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'], unique=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal('app_manager', ['Group_instance'])

        # Adding model 'Cdr'
        db.create_table('app_manager_cdr', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('answered_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('is_incoming', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('line', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('callerid', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('uuid', self.gf('django.db.models.fields.CharField')(max_length=36)),
            ('hangup_cause', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal('app_manager', ['Cdr'])

        # Adding model 'Caller_filter'
        db.create_table('app_manager_caller_filter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal('app_manager', ['Caller_filter'])

        # Adding model 'Rate_limit_filter'
        db.create_table('app_manager_rate_limit_filter', (
            ('caller_filter_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app_manager.Caller_filter'], unique=True, primary_key=True)),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('app_manager', ['Rate_limit_filter'])

        # Adding model 'App_instance_caller_filter'
        db.create_table('app_manager_app_instance_caller_filter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('app_manager', ['App_instance_caller_filter'])

        # Adding model 'Caller_filter_log'
        db.create_table('app_manager_caller_filter_log', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('filter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Caller_filter'])),
            ('cause', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal('app_manager', ['Caller_filter_log'])


    def backwards(self, orm):
        
        # Deleting model 'Application'
        db.delete_table('app_manager_application')

        # Deleting model 'User_profile'
        db.delete_table('app_manager_user_profile')

        # Deleting model 'App_instance'
        db.delete_table('app_manager_app_instance')

        # Deleting model 'App_instance_settings'
        db.delete_table('app_manager_app_instance_settings')

        # Deleting model 'Callback_info'
        db.delete_table('app_manager_callback_info')

        # Deleting model 'Line'
        db.delete_table('app_manager_line')

        # Deleting model 'Permission'
        db.delete_table('app_manager_permission')

        # Deleting model 'User_permission'
        db.delete_table('app_manager_user_permission')

        # Deleting model 'Group_permission'
        db.delete_table('app_manager_group_permission')

        # Deleting model 'Group_instance'
        db.delete_table('app_manager_group_instance')

        # Deleting model 'Cdr'
        db.delete_table('app_manager_cdr')

        # Deleting model 'Caller_filter'
        db.delete_table('app_manager_caller_filter')

        # Deleting model 'Rate_limit_filter'
        db.delete_table('app_manager_rate_limit_filter')

        # Deleting model 'App_instance_caller_filter'
        db.delete_table('app_manager_app_instance_caller_filter')

        # Deleting model 'Caller_filter_log'
        db.delete_table('app_manager_caller_filter_log')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.app_instance_caller_filter': {
            'Meta': {'object_name': 'App_instance_caller_filter'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'app_manager.app_instance_settings': {
            'Meta': {'object_name': 'App_instance_settings'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'callback_delay': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_callback': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'line_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'starting_line': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'app_manager.callback_info': {
            'Meta': {'object_name': 'Callback_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'callerid': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'app_manager.caller_filter': {
            'Meta': {'object_name': 'Caller_filter'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'app_manager.caller_filter_log': {
            'Meta': {'object_name': 'Caller_filter_log'},
            'cause': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Cdr']"}),
            'filter': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Caller_filter']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        'app_manager.group_instance': {
            'Meta': {'object_name': 'Group_instance'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.Group']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'app_manager.group_permission': {
            'Meta': {'object_name': 'Group_permission'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Permission']"})
        },
        'app_manager.line': {
            'Meta': {'object_name': 'Line'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app_manager.permission': {
            'Meta': {'object_name': 'Permission'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']", 'null': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'app_manager.rate_limit_filter': {
            'Meta': {'object_name': 'Rate_limit_filter', '_ormbases': ['app_manager.Caller_filter']},
            'caller_filter_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app_manager.Caller_filter']", 'unique': 'True', 'primary_key': 'True'}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'app_manager.user_permission': {
            'Meta': {'object_name': 'User_permission'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Permission']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user'", 'to': "orm['auth.User']"})
        },
        'app_manager.user_profile': {
            'Meta': {'object_name': 'User_profile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app_manager']
