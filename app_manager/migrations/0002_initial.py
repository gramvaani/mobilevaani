# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Application'
        db.create_table(u'app_manager_application', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('desc', self.gf('django.db.models.fields.TextField')(max_length=200)),
            ('pkg_name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'app_manager', ['Application'])

        # Adding model 'User_profile'
        db.create_table(u'app_manager_user_profile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal(u'app_manager', ['User_profile'])

        # Adding model 'App_instance'
        db.create_table(u'app_manager_app_instance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Application'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('conf', self.gf('django.db.models.fields.CharField')(default='default', max_length=32)),
            ('status', self.gf('django.db.models.fields.IntegerField')()),
            ('record_duration_limit_seconds', self.gf('django.db.models.fields.PositiveIntegerField')(default=120)),
        ))
        db.send_create_signal(u'app_manager', ['App_instance'])

        # Adding model 'VI_conf'
        db.create_table(u'app_manager_vi_conf', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('controller', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'app_manager', ['VI_conf'])

        # Adding model 'VI_reference'
        db.create_table(u'app_manager_vi_reference', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vi', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vi', to=orm['app_manager.VI_conf'])),
            ('reference', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('target_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='target_ai', to=orm['app_manager.App_instance'])),
            ('target_vi', self.gf('django.db.models.fields.related.ForeignKey')(related_name='target_vi', to=orm['app_manager.VI_conf'])),
            ('target_state', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'app_manager', ['VI_reference'])

        # Adding model 'App_instance_settings'
        db.create_table(u'app_manager_app_instance_settings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('is_callback', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('callback_delay', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('starting_line', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('line_count', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('is_gsm', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_state_machine_impl', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('vi_conf', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.VI_conf'], null=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True, blank=True)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'app_manager', ['App_instance_settings'])

        # Adding model 'Callback_info'
        db.create_table(u'app_manager_callback_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('callerid', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
        ))
        db.send_create_signal(u'app_manager', ['Callback_info'])

        # Adding model 'Line'
        db.create_table(u'app_manager_line', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'app_manager', ['Line'])

        # Adding model 'Permission'
        db.create_table(u'app_manager_permission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Application'], null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'app_manager', ['Permission'])

        # Adding model 'User_permission'
        db.create_table(u'app_manager_user_permission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user', to=orm['auth.User'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('perm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Permission'])),
        ))
        db.send_create_signal(u'app_manager', ['User_permission'])

        # Adding model 'Group_permission'
        db.create_table(u'app_manager_group_permission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'])),
            ('perm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Permission'])),
        ))
        db.send_create_signal(u'app_manager', ['Group_permission'])

        # Adding model 'Group_instance'
        db.create_table(u'app_manager_group_instance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'], unique=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'app_manager', ['Group_instance'])

        # Adding model 'Cdr'
        db.create_table(u'app_manager_cdr', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('answered_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('is_incoming', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('line', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('callerid', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
            ('uuid', self.gf('django.db.models.fields.CharField')(max_length=36)),
            ('hangup_cause', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'app_manager', ['Cdr'])

        # Adding model 'Caller_filter'
        db.create_table(u'app_manager_caller_filter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'app_manager', ['Caller_filter'])

        # Adding model 'Rate_limit_filter'
        db.create_table(u'app_manager_rate_limit_filter', (
            (u'caller_filter_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app_manager.Caller_filter'], unique=True, primary_key=True)),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'app_manager', ['Rate_limit_filter'])

        # Adding model 'App_instance_caller_filter'
        db.create_table(u'app_manager_app_instance_caller_filter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'app_manager', ['App_instance_caller_filter'])

        # Adding model 'Caller_filter_log'
        db.create_table(u'app_manager_caller_filter_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('filter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Caller_filter'])),
            ('cause', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'app_manager', ['Caller_filter_log'])

        # Adding model 'Incoming_line_ai'
        db.create_table(u'app_manager_incoming_line_ai', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('number', self.gf('django.db.models.fields.IntegerField')(unique=True)),
        ))
        db.send_create_signal(u'app_manager', ['Incoming_line_ai'])

        # Adding model 'App_instance_time_filter'
        db.create_table(u'app_manager_app_instance_time_filter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('start_time', self.gf('django.db.models.fields.TimeField')()),
            ('end_time', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'app_manager', ['App_instance_time_filter'])

        # Adding model 'App_instance_line_ip'
        db.create_table(u'app_manager_app_instance_line_ip', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('line', self.gf('django.db.models.fields.IPAddressField')(max_length=15)),
            ('for_callback', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'app_manager', ['App_instance_line_ip'])

        # Adding model 'Cdr_in_out_map'
        db.create_table(u'app_manager_cdr_in_out_map', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai_ip', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance_line_ip'], null=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'], null=True)),
            ('call_in_cdr', self.gf('django.db.models.fields.related.ForeignKey')(related_name='call_in_cdr', null=True, to=orm['app_manager.Cdr'])),
        ))
        db.send_create_signal(u'app_manager', ['Cdr_in_out_map'])

        # Adding model 'Call_event_recorder'
        db.create_table(u'app_manager_call_event_recorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('event_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
        ))
        db.send_create_signal(u'app_manager', ['Call_event_recorder'])

        # Adding model 'Transition_event'
        db.create_table(u'app_manager_transition_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('vi', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.VI_conf'], null=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('state_counter', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('prev_state', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('event', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('trans_event', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'app_manager', ['Transition_event'])

        # Adding model 'Reference_event'
        db.create_table(u'app_manager_reference_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('trans_event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Transition_event'])),
        ))
        db.send_create_signal(u'app_manager', ['Reference_event'])

        # Adding model 'Ai_field_properties'
        db.create_table(u'app_manager_ai_field_properties', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('model', self.gf('django.db.models.fields.related.ForeignKey')(related_name='model_name', to=orm['contenttypes.ContentType'])),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('field_type', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('default_field_value', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('is_required', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True, blank=True)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'app_manager', ['Ai_field_properties'])


    def backwards(self, orm):
        # Deleting model 'Application'
        db.delete_table(u'app_manager_application')

        # Deleting model 'User_profile'
        db.delete_table(u'app_manager_user_profile')

        # Deleting model 'App_instance'
        db.delete_table(u'app_manager_app_instance')

        # Deleting model 'VI_conf'
        db.delete_table(u'app_manager_vi_conf')

        # Deleting model 'VI_reference'
        db.delete_table(u'app_manager_vi_reference')

        # Deleting model 'App_instance_settings'
        db.delete_table(u'app_manager_app_instance_settings')

        # Deleting model 'Callback_info'
        db.delete_table(u'app_manager_callback_info')

        # Deleting model 'Line'
        db.delete_table(u'app_manager_line')

        # Deleting model 'Permission'
        db.delete_table(u'app_manager_permission')

        # Deleting model 'User_permission'
        db.delete_table(u'app_manager_user_permission')

        # Deleting model 'Group_permission'
        db.delete_table(u'app_manager_group_permission')

        # Deleting model 'Group_instance'
        db.delete_table(u'app_manager_group_instance')

        # Deleting model 'Cdr'
        db.delete_table(u'app_manager_cdr')

        # Deleting model 'Caller_filter'
        db.delete_table(u'app_manager_caller_filter')

        # Deleting model 'Rate_limit_filter'
        db.delete_table(u'app_manager_rate_limit_filter')

        # Deleting model 'App_instance_caller_filter'
        db.delete_table(u'app_manager_app_instance_caller_filter')

        # Deleting model 'Caller_filter_log'
        db.delete_table(u'app_manager_caller_filter_log')

        # Deleting model 'Incoming_line_ai'
        db.delete_table(u'app_manager_incoming_line_ai')

        # Deleting model 'App_instance_time_filter'
        db.delete_table(u'app_manager_app_instance_time_filter')

        # Deleting model 'App_instance_line_ip'
        db.delete_table(u'app_manager_app_instance_line_ip')

        # Deleting model 'Cdr_in_out_map'
        db.delete_table(u'app_manager_cdr_in_out_map')

        # Deleting model 'Call_event_recorder'
        db.delete_table(u'app_manager_call_event_recorder')

        # Deleting model 'Transition_event'
        db.delete_table(u'app_manager_transition_event')

        # Deleting model 'Reference_event'
        db.delete_table(u'app_manager_reference_event')

        # Deleting model 'Ai_field_properties'
        db.delete_table(u'app_manager_ai_field_properties')


    models = {
        u'app_manager.ai_field_properties': {
            'Meta': {'object_name': 'Ai_field_properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'default_field_value': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'model': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'model_name'", 'to': u"orm['contenttypes.ContentType']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.app_instance_caller_filter': {
            'Meta': {'object_name': 'App_instance_caller_filter'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'app_manager.app_instance_line_ip': {
            'Meta': {'object_name': 'App_instance_line_ip'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'for_callback': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'})
        },
        u'app_manager.app_instance_settings': {
            'Meta': {'object_name': 'App_instance_settings'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callback_delay': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_callback': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_gsm': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_state_machine_impl': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'line_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'starting_line': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vi_conf': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True', 'blank': 'True'})
        },
        u'app_manager.app_instance_time_filter': {
            'Meta': {'object_name': 'App_instance_time_filter'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.call_event_recorder': {
            'Meta': {'object_name': 'Call_event_recorder'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True'}),
            'event_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'app_manager.callback_info': {
            'Meta': {'object_name': 'Callback_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.caller_filter': {
            'Meta': {'object_name': 'Caller_filter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.caller_filter_log': {
            'Meta': {'object_name': 'Caller_filter_log'},
            'cause': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'filter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Caller_filter']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.cdr_in_out_map': {
            'Meta': {'object_name': 'Cdr_in_out_map'},
            'ai_ip': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance_line_ip']", 'null': 'True'}),
            'call_in_cdr': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'call_in_cdr'", 'null': 'True', 'to': u"orm['app_manager.Cdr']"}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.group_instance': {
            'Meta': {'object_name': 'Group_instance'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.group_permission': {
            'Meta': {'object_name': 'Group_permission'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Permission']"})
        },
        u'app_manager.incoming_line_ai': {
            'Meta': {'object_name': 'Incoming_line_ai'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        u'app_manager.line': {
            'Meta': {'object_name': 'Line'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        u'app_manager.permission': {
            'Meta': {'object_name': 'Permission'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'app_manager.rate_limit_filter': {
            'Meta': {'object_name': 'Rate_limit_filter', '_ormbases': [u'app_manager.Caller_filter']},
            u'caller_filter_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app_manager.Caller_filter']", 'unique': 'True', 'primary_key': 'True'}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'app_manager.reference_event': {
            'Meta': {'object_name': 'Reference_event'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'trans_event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Transition_event']"})
        },
        u'app_manager.transition_event': {
            'Meta': {'object_name': 'Transition_event'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'state_counter': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trans_event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True'})
        },
        u'app_manager.user_permission': {
            'Meta': {'object_name': 'User_permission'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Permission']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user'", 'to': u"orm['auth.User']"})
        },
        u'app_manager.user_profile': {
            'Meta': {'object_name': 'User_profile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_reference': {
            'Meta': {'object_name': 'VI_reference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'target_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vi'", 'to': u"orm['app_manager.VI_conf']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app_manager']