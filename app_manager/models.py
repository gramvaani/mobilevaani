from django.db import models
from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^vapp\.app_manager\.models\.CalleridField", "app_manager\.models\.CalleridField"])
from django.db.models.signals import post_save
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.core.exceptions import ObjectDoesNotExist

from abc import ABCMeta, abstractmethod
from datetime import datetime, timedelta
import os, traceback

import urllib, urllib2, socket, httplib, base64, hashlib

from vapp.caching_service.utils import sync_object_data

from vapp.app_manager import perms as app_manager_perms
from vapp.media.paths import MEDIA_DIR, RECORDINGS_DIR
from schedule.models import Schedule


from log import get_request_logger
logger = get_request_logger()

STATUS_ACTIVE = 1

CALLERID_LENGTH = 20
UUID_LENGTH     = 36
HANGUP_CAUSE_LENGTH= 32
EVENT_LENGTH = 50
DEFAULT_DATE_FORMAT = '%Y-%m-%d'
DEFAULT_DATE_TIME_FORMAT = '%Y-%m-%d'

FL_SHORT  = 32
FL_MEDIUM = 64
FL_LONG   = 128
FL_VLONG  = 256

URL_LENGTH = 255
URL_RESP_CODE_LENGTH = 255

SHORT_CODE_LENGTH = 255

TRANSLATOR_CRED_NAME_MAX_LENGTH = 100
TRANSLATOR_CLIENT_SECRET_MAX_LENGTH = 255
TRANSLATOR_CLIENT_ID_MAX_LENGTH = 255


# Callerid model Field. This should replace all the other caller id fields
class CalleridField(models.CharField):

    def __init__(self, *args, **kwargs):

        if not kwargs.has_key('max_length'):
            kwargs['max_length'] = CALLERID_LENGTH
        super(CalleridField,self).__init__(*args, **kwargs)

    @classmethod
    def is_valid_callerid(cls, num):
        return (num and len(num) >= 10)

    def pre_save(self, instance, add):

         from telephony.utils import get_formatted_number

         unformatted_callerid = getattr(instance, self.attname)
         formatted_callerid = get_formatted_number(unformatted_callerid)
         setattr(instance, self.attname, formatted_callerid)
         return super(CalleridField,self).pre_save(instance, add)


class Application(models.Model):
    name = models.CharField(max_length = 32)
    desc = models.TextField(max_length = 200)
    pkg_name = models.CharField(max_length = 32)

    def __unicode__(self):
        return unicode(self.name)

class User_profile(models.Model):
    user = models.ForeignKey(User, unique = True)

    def __unicode__(self):
        return unicode(self.user)

class App_instance(models.Model):
    app  = models.ForeignKey(Application)
    name = models.CharField(max_length = 32)
    name = models.CharField(max_length = 32)
    conf = models.CharField(max_length = 32, default = 'default')
    status = models.IntegerField()
    record_duration_limit_seconds = models.PositiveIntegerField(default = 120)

    def prompt_set(self):
        from vapp.media.models import App_instance_prompt_set
        try:
            return App_instance_prompt_set.objects.get(ai = self.id, is_current = True).prompt_set_id
        except:
            return 0

    def desc(self):
        return self.app.desc

    def message_queue(self):
        return App_instance.message_queue_from_id( self.id )

    @classmethod
    def message_queue_from_id(cls, id):
        return '/topic/ai_%s' % id

    def __unicode__(self):
        return unicode(self.id) + '_' + unicode(self.name)


def app_instance_save_handler(sender, **kwargs):
    ai = kwargs['instance']
    dir_path = MEDIA_DIR + RECORDINGS_DIR + str(ai.id)
    try:
        if kwargs['created'] and not os.path.exists(dir_path):
            os.makedirs(dir_path)
    except:
        #print "Could not create dir for ai recordings %s" % ai
        traceback.print_exc()

post_save.connect(app_instance_save_handler, sender = App_instance, dispatch_uid = 'app_manager.app_instance_save')

class VI_conf(models.Model):
    name        = models.CharField( max_length = FL_MEDIUM )
    controller  = models.CharField( max_length = FL_MEDIUM )
    description = models.CharField( max_length = FL_MEDIUM )

    @classmethod
    def create( cls, ai ):
        pkg_name = str( ai.app.pkg_name )
        formatted_pkg_name = pkg_name.title()
        vi_conf = VI_conf()
        vi_conf.name = ai.name
        vi_conf.controller  = pkg_name + ".vi." + formatted_pkg_name + "Controller"
        vi_conf.description = pkg_name + ".vi." + formatted_pkg_name + "StatesDescriptionMap"
        vi_conf.save()
        return vi_conf

    def __unicode__(self):
        return str( self.id ) + '_' + unicode( self.name )

class VI_reference(models.Model):
    ai          = models.ForeignKey(App_instance, related_name = 'ai', null = True) #allowing nulls to accomodate existing entries for now.
    vi          = models.ForeignKey(VI_conf, related_name = 'vi')
    reference   = models.CharField(max_length = 64)
    target_ai   = models.ForeignKey(App_instance, related_name = 'target_ai')
    target_vi   = models.ForeignKey(VI_conf, related_name = 'target_vi')
    target_state= models.CharField(max_length = 64)
    vi_data     = models.CharField(max_length = 128, null = True, blank = True)

    @classmethod
    def add_or_update( cls, source_ai, target_ai, news_id, vi_reference = None ):
        params = {}
        vis = App_instance_settings.objects.filter(ai__in=[ source_ai, target_ai ])
        params['ai_id'] = source_ai
        params['target_ai_id'] = target_ai
        params['vi'] = vis.get( ai = source_ai ).vi_conf
        params['target_vi'] = vis.get( ai = target_ai ).vi_conf
        params['reference'] = str(news_id) + "_jump_to_ai"
        params['target_state'] = "welcome"

        try:
            vi_reference = vi_reference or VI_reference.objects.get( reference = params.get( 'reference', None ) )
        except ObjectDoesNotExist:
            pass
        except Exception, e:
            logger.exception( "Not able to add_or_update VI_reference. %s" % str(e) )
            raise

        if vi_reference:
            for key, value in params.iteritems():
                setattr(vi_reference, key, value)
        else:
            vi_reference = VI_reference( **params  )
        vi_reference.save()
        return vi_reference

    def __unicode__(self):
        return unicode( self.vi ) + '_@' + unicode( self.reference )

class App_instance_settings(models.Model):
    ai              = models.ForeignKey(App_instance)
    is_callback     = models.BooleanField(default = False)
    callback_delay  = models.PositiveIntegerField(null = True, blank = True)
    starting_line   = models.PositiveIntegerField(null = True, blank = True)
    line_count      = models.PositiveIntegerField(null = True, blank = True)
    is_gsm          = models.BooleanField(default = False)
    is_state_machine_impl = models.BooleanField(default = True)
    vi_conf         = models.ForeignKey( VI_conf, null = True, blank = True)
    max_duration    = models.PositiveIntegerField(null = True, blank = True, verbose_name = "Max Duration (seconds)")
    min_recording_duration = models.PositiveIntegerField(default = 10, verbose_name = "Min Recording Duration (seconds)")
    max_callback_delay = models.PositiveIntegerField(default = 30, verbose_name = "Max Call-back Delay (Minutes)")

    content_type= models.ForeignKey(ContentType, null = True, blank = True)
    object_id   = models.PositiveIntegerField(null = True, blank = True)
    app_settings= generic.GenericForeignKey('content_type', 'object_id')

    @classmethod
    def create( cls, ai ):
        vi_conf = VI_conf.create( ai )
        ai_settings = App_instance_settings( ai = ai, is_callback = True, callback_delay = 20, starting_line = 0, line_count = 1, vi_conf = vi_conf )
        ai_settings.save()

    @classmethod
    def get_by_aid(cls, ai_id):
        try:
            return cls.objects.get(ai = ai_id)
        except:
            return None

    def __unicode__(self):
        return unicode(self.ai) + '_settings'


class Line(models.Model):
    number = models.IntegerField(unique = True)
    ai	   = models.ForeignKey(App_instance)

    def __unicode__(self):
        return unicode(self.number) + '_' + unicode(self.ai)

class Permission(models.Model):
    name = models.CharField(max_length = 128)
    app  = models.ForeignKey(Application, blank = True, null = True)
    code = models.CharField(max_length = 64)

    def __unicode__(self):
        return unicode(self.code)

class User_permission(models.Model):
    ROLE_CHOICES = (
        ('PARTNER', 'PARTNER'),
        ('COMMUNITY_MANAGER', 'COMMUNITY_MANAGER'),
        ('VOLUNTEER', 'VOLUNTEER'),
        ('DEFAULT', 'DEFAULT'),
    )

    user = models.ForeignKey(User)
    ai   = models.ForeignKey( App_instance, blank=True , null=True )
    perm = models.ForeignKey(Permission)
    callerid = models.CharField(max_length=12, blank=True)
    role = models.CharField(max_length=20, choices=ROLE_CHOICES, default='DEFAULT')

    @classmethod
    def create( cls, ai, user_id, assign_perms ):
        permissions = []
        try:
            for perm_code in assign_perms:
                perm = Permission.objects.get( code = perm_code )
                permission = User_permission( user_id = user_id, ai = ai, perm = perm )
                permission.save()
                permissions.append( permission )
            return permissions
        except:
            logger.exception("Could not assign permissions for app instance: " + str(ai_id) )
            return None

    class Meta:
        unique_together = ("user", "ai", "perm")

    def __unicode__(self):
        return unicode(self.user) + "_" + unicode(self.ai) + "_" + unicode(self.perm)

def user_perm_save_handler(sender, **kwargs):
    user_permission = kwargs['instance']
    object_id = user_permission.id

post_save.connect(user_perm_save_handler, sender=User_permission, dispatch_uid = 'app_manager.user_permission.save')

class Group_permission(models.Model):
    group = models.ForeignKey(Group)
    perm  = models.ForeignKey(Permission)
    ai = models.ForeignKey(App_instance, null = True, blank = True)

    #sync TPH4-username-vinod.maurya  

    def __unicode__(self):
        return unicode(self.group) + ' - ' + unicode(self.perm) + ' - ' + unicode(self.ai)

class Group_instance(models.Model):
    group = models.ForeignKey(Group, unique = True)
    ai    = models.ForeignKey(App_instance)
    role  = models.CharField(max_length = 64)

    def __unicode__(self):
        return unicode(self.group) + "_" + unicode(self.ai)

class Cdr_trigger():
    CALLBACK = 0
    CALLANDPLAY = 1

class Cdr(models.Model):

    class CdrEvents:
        INITIATE = 0
        CONNECT = 1
        INPROGRESS = 2
        END = 3
        RETRY = 4

    start_time = models.DateTimeField(auto_now_add = True)
    answered_time = models.DateTimeField(null = True)
    end_time = models.DateTimeField(null = True)

    is_incoming = models.BooleanField(default = True)
    trigger = models.PositiveSmallIntegerField(null = True)
    ai = models.ForeignKey(App_instance, null = True)
    line = models.IntegerField(null = True)
    callerid = CalleridField()
    uuid = models.CharField(max_length = UUID_LENGTH)
    hangup_cause = models.CharField(max_length = HANGUP_CAUSE_LENGTH)

    def __unicode__(self):
        return unicode(self.callerid) + "_" + unicode(self.start_time)

    class Type:
        FORWARD    = 'FWD'
        OBD        = 'OBD'
        ON_PUBLISH = 'ON_PUBLISH'
        ON_REJECT  = 'ON_REJECT'
        CALLBACK   = 'CALLBACK'
        RECEIVED   = 'RECEIVED'
        MISSED     = 'MISSED'
        HELPLINE   = 'HELPLINE'
        NA         = 'NA'


class Call_type(object):
    INCOMING = 'Incoming'
    OUTGOING = 'Outgoing'
    ALL      = 'All'


class Call_response_type():
    CALLBACK = 'CALLBACK'
    RECEIVE  = 'RECEIVE'


class Caller_filter(models.Model):
    name = models.CharField(max_length = 32)

    def allow(self, ai_id, number, cdr_id):
        raise NotImplementedError("Subclass must implement abstract method")

    def __unicode__(self):
        return self.name


class Rate_limit_filter(Caller_filter):
    count = models.PositiveIntegerField()
    duration = models.PositiveIntegerField()
    per_caller = models.BooleanField(default = False)

    def allow(self, ai_id, number, cdr_id, call_type_for_filter, call_response_type):
        now = datetime.now()
        start = now - timedelta(seconds = self.duration)
        cdrs = Cdr.objects.filter(ai_id = ai_id, start_time__range = (start, now), answered_time__isnull = False)
        if self.per_caller:
            cdrs = cdrs.filter(callerid = number)

        if call_type_for_filter == Call_type.ALL:
            pass
        elif (call_type_for_filter == Call_type.INCOMING) and (call_response_type == Call_response_type.RECEIVE):
            cdrs = cdrs.filter(is_incoming = True)
        elif (call_type_for_filter == Call_type.OUTGOING) and (call_response_type == Call_response_type.CALLBACK):
            cdrs = cdrs.filter(is_incoming = False)
        else:
            return True, None

        num_calls = cdrs.count()
        # if self.per_caller:
        #     num_calls = Ai_vi_start_event.objects.filter(next_ai_id = ai_id, trans_event__time__range = (start, now),
        #                 trans_event__cdr__callerid = number).values('trans_event__cdr_id').distinct().count()
        # else:
        #     num_calls = Ai_vi_start_event.objects.filter(next_ai_id = ai_id, trans_event__time__range = (start, now)).values('trans_event__cdr_id').distinct().count()

        if num_calls >= self.count:
            return False, "count: " + str(num_calls)
        else:
            return True, None

    def __unicode__(self):
        return unicode(self.id) + '_' + unicode(self.name)


class Schedule_filter(Caller_filter):
    schedules = models.ManyToManyField(Schedule)

    def allow(self, ai_id, number, cdr_id, call_type_for_filter, call_response_type):
        now = datetime.now()

        for schedule in self.schedules.all():
            if schedule.is_schedule_on(now):
                return True, None

        return False, "%s not in schedule" % (now,)

    def __unicode__(self):
        return unicode(self.name)


class App_instance_caller_filter(models.Model):

    CALL_TYPES = (
            (Call_type.INCOMING, 'Incoming'),
            (Call_type.OUTGOING, 'Outgoing'),
            (Call_type.ALL, 'All'),
            )

    ai = models.ForeignKey(App_instance)

    content_type= models.ForeignKey(ContentType)
    object_id   = models.PositiveIntegerField()
    filter = generic.GenericForeignKey('content_type', 'object_id')
    call_type_for_filter = models.CharField(max_length = 12, choices = CALL_TYPES, default = Call_type.ALL)

    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.filter) + '_' + unicode(self.call_type_for_filter)


class Ai_filter_exclude_list(models.Model):
    ai = models.OneToOneField('App_instance')
    contact_lists = models.ManyToManyField('callerinfo.Contact_list')

    def __unicode__(self):
        return unicode(self.ai)


class Caller_filter_log(models.Model):
    cdr = models.ForeignKey(Cdr)
    filter = models.ForeignKey(Caller_filter)
    cause = models.CharField(max_length = 64)


class Incoming_line_ai(models.Model):
    ai = models.ForeignKey(App_instance)
    number = models.IntegerField(unique = True)

    def __unicode__(self):
        return str(self.ai) + '_' + str(self.number)


class App_instance_line_ip(models.Model):

    ai = models.ForeignKey(App_instance)
    line = models.IPAddressField()
    for_callback = models.BooleanField(default=True)

class Cdr_in_out_map(models.Model):

    ai_ip = models.ForeignKey(App_instance_line_ip, null = True)
    cdr   = models.ForeignKey(Cdr, null = True)
    call_in_cdr = models.ForeignKey(Cdr, null = True, related_name = 'call_in_cdr')


class Call_event_recorder(models.Model):

    cdr = models.ForeignKey(Cdr)
    event_type = models.CharField(max_length = EVENT_LENGTH)
    timestamp = models.DateTimeField(auto_now_add = True)

    content_type= models.ForeignKey(ContentType, null = True)
    object_id   = models.PositiveIntegerField(null = True)
    data    = generic.GenericForeignKey('content_type', 'object_id')

    @classmethod
    def record_event(cls, session, cdr, event_type, data = None):

        if cdr and event_type:
            if isinstance(cdr, Cdr) and isinstance(event_type,str) :
                if ((not session.isHungup()) or (session.isHungup() and event_type == Events.HANGUP)):
                    event = Call_event_recorder(cdr = cdr, event_type = event_type, data = data)
                    event.save()


class Callback_info(models.Model):
    ai = models.ForeignKey(App_instance)
    callerid = CalleridField()
    call_in_cdr= models.ForeignKey(Cdr, null=True)

class Transition_event(models.Model):
    cdr         = models.ForeignKey(Cdr)
    ai          = models.ForeignKey(App_instance, null = True)
    vi          = models.ForeignKey(VI_conf, null=True)
    time        = models.DateTimeField( auto_now_add = True )
    state_counter = models.PositiveIntegerField()
    prev_state  = models.CharField( max_length = 64 )
    event       = models.CharField( max_length = 64 )
    trans_event = models.CharField( max_length = 64 )
    state       = models.CharField( max_length = 64 )
    action      = models.CharField( max_length = 32 )
    filename    = models.CharField( max_length = 64 )

class Ai_vi_start_event(models.Model):
    prev_ai     = models.ForeignKey(App_instance, null = True, related_name = 'prev_ai')
    prev_vi     = models.ForeignKey(VI_conf, null = True,  related_name = 'prev_vi')
    next_ai     = models.ForeignKey(App_instance, related_name = 'next_ai')
    next_vi     = models.ForeignKey(VI_conf, related_name = 'next_vi')
    trans_event = models.ForeignKey(Transition_event)

class Events:
    HANGUP = 'HANGUP'
    STARTED = 'STARTED'
    ANSWERED = 'ANSWERED'

class Ai_field_properties(models.Model):

    FIELD_TYPES = (
    ('C', 'Char Field'),
    ('I', 'Integer Field'),
    ('D', 'Date Field'),
    ('DT', 'Date Time Field'),
    ('FK', 'Foreign Key Field'),
    )

    ai = models.ForeignKey(App_instance)
    model = models.ForeignKey(ContentType, related_name = 'model_name')
    field_name = models.CharField(max_length = 20)
    field_type = models.CharField(max_length = 2, choices = FIELD_TYPES)
    default_field_value = models.CharField(max_length = 50, null = True, blank = True)
    is_required = models.BooleanField( default = False )

    content_type= models.ForeignKey(ContentType, null = True, blank = True)
    object_id = models.PositiveIntegerField(null = True, blank = True)
    default_fk = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return str(self.ai) + '_' + str(self.field_name)

    def get_default_field_value( self, date_format = None, datetime_format = None ):
        try:
            if self.field_type == 'C':
                 return str(self.default_field_value) if self.default_field_value else " "
            elif self.field_type == 'I':
                return int(self.default_field_value) if self.default_field_value.isdigit() else 0
            elif self.field_type == 'D':
                format = ( date_format if date_format else DEFAULT_DATE_FORMAT )
                return datetime.strptime(self.field_type, format) if self.default_field_value else datetime.now().date()
            elif self.field_type == 'DT':
                format = ( datetime_format if datetime_format else DEFAULT_DATE_TIME_FORMAT )
                return datetime.strptime(self.field_type, format) if self.default_field_value else datetime.now()
            elif self.field_type == 'FK':
                return self.content_type.get_object_for_this_type( pk = self.object_id )
        except:
            logger.exception('illegal default value put in for id:' + str(self.id))
        return None

class HTTP_log(models.Model):
    ai = models.ForeignKey(App_instance)
    http_req = models.CharField(max_length = URL_LENGTH)
    http_res = models.CharField(max_length = URL_RESP_CODE_LENGTH)
    sent_time   = models.DateTimeField(null = True)
    tries   = models.PositiveSmallIntegerField(default = 0)

    @classmethod
    def create_http(cls,ai,url,**params):
        http = HTTP_log()
        url = url + '?' + urllib.urlencode(params)
        http.http_req = url
        http.ai = ai
        http.save()
        return http

class App_instance_quota_filter(Caller_filter):
    class Quota_period:
        WEEKLY = 'Weekly'
        MONTHLY = 'Monthly'
        DAILY = 'Daily'

    class Quota_unit:
        MINUTES = 'Mins'
        CALLS = 'Calls'

    QUOTA_PERIODS = (
            (Quota_period.WEEKLY, 'Weekly'),
            (Quota_period.MONTHLY, 'Monthly'),
            (Quota_period.DAILY, 'Daily'),
            )

    QUOTA_UNITS = (
            (Quota_unit.MINUTES, 'Minute Based'),
            (Quota_unit.CALLS, 'Call Based'),
            )

    quota_period = models.CharField(max_length = 10, choices = QUOTA_PERIODS, default = Quota_period.DAILY)
    quota_unit = models.CharField(max_length = 10, choices = QUOTA_UNITS, default = Quota_unit.CALLS)
    choice_limit =  models.PositiveIntegerField(default = 100)

    def allow( self, ai_id, number, cdr_id, call_type_for_filter, call_response_type ):
        now = datetime.now()
        total_calls_till_yesterday =0
        total_duration_till_yesterday = 0

        if self.quota_period == App_instance_quota_filter.Quota_period.DAILY:
            begin = now - timedelta( days=1 )
        elif self.quota_period == App_instance_quota_filter.Quota_period.WEEKLY:
            begin = now - timedelta( days=7 )
        elif self.quota_period == App_instance_quota_filter.Quota_period.MONTHLY:
            begin = ( now - timedelta( days=1 ) ).replace( day=1 )

        from vapp.stats.stats import get_call_details, get_callscount_callerscount_avgdur
        if self.quota_period != App_instance_quota_filter.Quota_period.DAILY:
            total_calls_till_yesterday, total_duration_till_yesterday = get_call_details( begin, now, ai_id )

        (calls_today, callers_today, avg_duration_today) = get_callscount_callerscount_avgdur( ai_id, now.replace(hour = 0, minute = 0, second = 0, microsecond = 0), now )

        if self.quota_unit == App_instance_quota_filter.Quota_unit.CALLS:
            total_calls = calls_today + total_calls_till_yesterday
            if total_calls >= self.choice_limit:
                return False, "Count: " + str(total_calls)
        elif self.quota_unit == App_instance_quota_filter.Quota_unit.MINUTES:
            total_duration = (avg_duration_today * calls_today) + total_duration_till_yesterday
            tot_duration_in_mins = total_duration/60.0
            if tot_duration_in_mins >= self.choice_limit:
                return False, "Duration: " + str(tot_duration_in_mins)

        return True, None

    def __unicode__(self):
        return unicode(self.name)

class Callback_delay_log(models.Model):

    callback_info = models.ForeignKey(Callback_info)
    callback_threshold = models.PositiveIntegerField()
    actual_callout_time = models.DateTimeField()
    time_added = models.DateTimeField(default = datetime.now)

    def __unicode__(self):

        return str(self.callback_info) + '_' + str(self.callback_threshold)


class Task_queue_map(models.Model):
    ai = models.ForeignKey(App_instance)
    vi = models.ForeignKey(VI_conf)
    task = models.CharField(max_length = 256)
    queue = models.CharField(max_length = 128)

    def __unicode__(self):
        return str(self.ai) + ", " + str(self.task) + " -> " + str(self.queue)

class Embedded_ai_map(models.Model):

   position = models.CharField(max_length = 50)
   vi_ref = models.ForeignKey(VI_reference)
   should_embed_func = models.CharField(max_length = 50, null = True, blank = True)
   active = models.BooleanField()
   priority = models.PositiveSmallIntegerField(default = 1)

   def  __unicode__(self):
       return unicode(self.vi_ref.ai) + '_' + unicode(self.vi_ref.target_ai)

   @classmethod
   def get_embedded_maps_for_ai_state(cls, ai, state):
        return Embedded_ai_map.objects.filter( vi_ref__ai = ai, state = state, active = True)


class VI_data( models.Model ):
    name = models.CharField( max_length = 64 )

    def get_dict( self ):
        return dict( [ ( str( de.key ), str( de.value ) ) for de in self.vi_data_entry_set.all() ] )

    def __unicode__( self ):
        return '{0}_{1}'.format( self.id, self.name )

class VI_data_entry( models.Model ):
    vi_data = models.ForeignKey( VI_data )
    key     = models.CharField( max_length = 64 )
    value   = models.CharField( max_length = 64 )

    class Meta:
        unique_together = ( 'vi_data', 'key' )

    def __unicode__( self ):
        return '{0}_{1}={2}'.format( self.vi_data_id, self.key, self.value )

class Call_context( models.Model ):
    ai      = models.ForeignKey( App_instance )
    vi_conf = models.ForeignKey( VI_conf )
    vi_data = models.ForeignKey( VI_data )

    def __unicode__( self ):
        return '{0}_{1}_{2}'.format( self.id, self.ai_id, self.vi_conf_id )

class CAP_context( models.Model ):

    def get_play_filename( self, orig_name ):
        return "cap/{0}_{1}_{2}".format( self.call_context.ai_id, self.call_context.vi_conf_id, orig_name )

    # Context
    name           = models.CharField( max_length = 64 )
    file_play      = models.FileField( null = True, blank = True, upload_to = get_play_filename )
    call_context   = models.ForeignKey( Call_context )

    # Window
    wnd_open_time  = models.TimeField()
    wnd_close_time = models.TimeField()

    # SMS
    sms_tmpl       = models.ForeignKey( 'sms.SMS_template', null = True, blank = True )

    def __unicode__( self ):
        return '{0}_{1}_{2}'.format( self.id, self.name, unicode( self.call_context ) )

class CAPC_numlist( models.Model ):
    capc    = models.ForeignKey( CAP_context )
    numlist = models.ForeignKey( 'callerinfo.Number_list' )

    def __unicode__( self ):
        return '{0}_{1}_{2}'.format( self.id, self.capc.name, self.numlist.id )

class CAP_log( models.Model ):
    # Call context
    ai            = models.ForeignKey( App_instance )
    vic_ctrl      = models.CharField( max_length = FL_MEDIUM )
    vic_desc      = models.CharField( max_length = FL_MEDIUM )
    vi_data       = models.ForeignKey( VI_data, null = True, on_delete = models.SET_NULL )
    filename_play = models.CharField( max_length = FL_LONG )

    # Session
    callerid    = CalleridField()
    last_cdr    = models.ForeignKey( Cdr, null = True, on_delete = models.SET_NULL )
    is_success  = models.BooleanField( default = False )
    is_sms_sent = models.BooleanField( default = False )
    num_tries   = models.PositiveSmallIntegerField( default = 0 )

    def __unicode__( self ):
        return '{0}_{1}_{2}_{3}'.format( self.id, self.callerid, self.ai_id, self.vic_ctrl )


class Ai_callers_stats_weekly(models.Model):
    ai = models.ForeignKey(App_instance)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    total_callers = models.PositiveIntegerField(default = 0)

    def __unicode__(self):
        return unicode(self.ai) + " # " + unicode(self.start_date) + " # " + unicode(self.end_date) + " # "  + unicode(self.total_callers)


class Ai_callers_call_stats(models.Model):
    ai = models.ForeignKey(App_instance)
    caller_id = CalleridField(max_length = CALLERID_LENGTH)
    call_count = models.PositiveIntegerField(default = 0)
    first_call_date = models.DateTimeField()

    def __unicode__(self):
        return unicode(self.ai) + " # " + unicode(self.caller_id) + " # " + unicode(self.call_count) + " # " +  unicode(self.first_call_date)


class Short_code( models.Model ):
    number = models.CharField( max_length = SHORT_CODE_LENGTH, unique = True )
    ai     = models.ForeignKey( App_instance )

    def __unicode__( self ):
        return '{0}_{1}'.format( self.number, self.ai )

def get_ai_from_short_code( number ):
    return App_instance.objects.get( short_code__number = number )


'''
Additional model added to assign n lines to an ai. This would marry ais to a certain set of lines
to the ais and make rationing a bit better. If an entry exists for an ai in this model, the freetdm urls
will reduce to the max_lines value for that ai. Else, we will return all the freetdm_urls
'''
class Ai_max_line( models.Model ):
    ai     = models.OneToOneField( App_instance )
    max_lines = models.PositiveSmallIntegerField( default = 2 )

    def __unicode__(self):
        return unicode( self.ai ) + '_' + unicode( self.max_lines )


class Data_sync_base(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def sync(self, *args, **kwargs):
        pass

class Data_sync(models.Model):
    ai = models.ForeignKey( App_instance )
    data_sync_name = models.CharField( max_length = 100 )
    data_sync_object = models.CharField( max_length = FL_MEDIUM )
    sync_image = models.ImageField( null = False, upload_to = 'images' )
    max_tries = models.PositiveIntegerField( default = 3 )

    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode( self.data_sync_name )


class Data_sync_schedule_log(models.Model):
    ds = models.ForeignKey( Data_sync )
    start_time = models.DateTimeField( auto_now_add = True )
    end_time = models.DateTimeField( null = True )
    success = models.BooleanField( default = False )
    tries = models.PositiveIntegerField( default = 0 )


class App_version(models.Model):
    name                 = models.CharField(max_length=50)
    description          = models.TextField(null=True, blank=True)
    version_no           = models.CharField(max_length=10)
    version_location_url = models.URLField()
    addition_time        = models.DateTimeField(auto_now_add=True)
    supported            = models.BooleanField(default=True)

    def __unicode__(self):
        return unicode(self.name) if self.name else unicode(self.version_no)

    @classmethod
    def get_latest_version_detail(cls):
        versions = App_version.objects.filter(supported=True).order_by('-id')
        if versions.count() > 0:
            return versions[0].version_no, versions[0].version_location_url


class Mnews_app_ai( models.Model ):
    name = models.CharField( max_length = 50 )
    mnews_ai = models.ForeignKey( App_instance )
    is_active = models.BooleanField( default = True )

    def __unicode__(self):
        return unicode( self.mnews_ai )


class Report_creds(models.Model):

    Policy_choices = (
        (Cdr.CdrEvents.INITIATE, 'INITIATE'),
        (Cdr.CdrEvents.CONNECT, 'CONNECT'),
        (Cdr.CdrEvents.INPROGRESS, 'INPROGRESS'),
        (Cdr.CdrEvents.END, 'END'),
        (Cdr.CdrEvents.RETRY, 'RETRY'),
    )

    ai       = models.ForeignKey(App_instance, related_name='report_creds_ai')
    url      = models.CharField(max_length=100)
    cred_key = models.CharField(max_length=255)
    order    = models.CharField(max_length=255)
    policy   = models.IntegerField(max_length=3, choices=Policy_choices, null=True, blank=True)

    def __unicode__( self ):
        return str( self.ai ) + ' s creds'

class Report_log(models.Model):
    cred       = models.ForeignKey( Report_creds )
    report     = models.TextField()
    time       = models.DateTimeField( auto_now_add = True )
    error_code = models.SmallIntegerField()
    cdr        = models.ForeignKey( Cdr, null = True, related_name = 'report_log_cdr' )

def get_report_creds(ai):
    return Report_creds.objects.filter(ai=ai)

def push_results( cred, data, cdr ):
    from app_manager.tasks import PushAppResultsTask
    PushAppResultsTask.delay( cred, data, cdr )

class Translator_api_cred( models.Model ):
    cred_name         = models.CharField( max_length = TRANSLATOR_CRED_NAME_MAX_LENGTH )
    client_id         = models.CharField( max_length = TRANSLATOR_CLIENT_ID_MAX_LENGTH )
    client_secret_key = models.CharField( max_length = TRANSLATOR_CLIENT_SECRET_MAX_LENGTH )

    def __unicode__( self ):
        return unicode( self.cred_name ) + '_' + unicode( self.client_id )

class Ai_translator_api_cred( models.Model ):
    ai   = models.ForeignKey( App_instance )
    cred = models.ForeignKey( Translator_api_cred )

    def __unicode__( self ):
        return unicode( self.ai ) + '_' + unicode( self.cred )

class Translation_method( models.Model ):
    name        = models.CharField( max_length = 50 )
    description = models.TextField( null = True, blank = True )
    signature   = models.CharField( max_length = 50 )

    def __unicode__( self ):
        return self.name

class Ai_translation_method( models.Model ):
    ai                 = models.ForeignKey( App_instance )
    translation_method = models.ForeignKey( Translation_method )

    def __unicode__( self ):
        return unicode( self.ai ) + '_' + unicode( self.translation_method )

def is_successfull_call( cdr_ids ):
    for cdr_id in cdr_ids:
        state_count = Transition_event.objects.filter( cdr = cdr_id ).count()
        if state_count >= 2:
            return True
    return False

class Dialout_status(object):
    SUCCESS   = 'SUCCESS'
    SCHEDULED = 'SCHEDULED'
    INPROGRESS = 'INPROGRESS'
    INACTIVE = 'INACTIVE'

def get_dialout_status(gcl):
    if gcl.success:
        return Dialout_status.SUCCESS
    elif gcl.group_schedule.is_call_valid() and not gcl.tries:
        return Dialout_status.SCHEDULED
    elif gcl.group_schedule.is_call_valid() and gcl.tries:
        return Dialout_status.INPROGRESS
    elif not gcl.group_schedule.is_call_valid():
        return Dialout_status.INACTIVE
