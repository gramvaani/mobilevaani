import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'vapp.settings'
from datetime import datetime

import vapp.settings
import vapp.app_manager
from vapp.app_manager.models import *
from freeswitch import *
from vapp.telephony.utils import *
from vapp.log import get_logger
from common import *
from datetime import datetime, time, timedelta
from vapp.local_settings import AI_GROUP_MAP


try:
    from app_manager.tasks import CallbackTask, IncomingCallTask
except:
    try:
        from vapp.app_manager.tasks import CallbackTask, IncomingCallTask
    except:
        from tasks import CallbackTask, IncomingCallTask


logger = get_logger()

def handler(session, args):

    line_num = int(session.getVariable("destination_number"))
    try:
        app_instance = App_instance.objects.get(line__number = line_num)
        logger.info(app_instance.name)
    except:
        try:
            incoming_entry = Incoming_line_ai.objects.get(number = line_num)
            logger.info("an incoming mapping exists")
            app_instance = incoming_entry.ai
        except:
            logger.exception("Unable to load app_instance for call to %s" % (line_num,))
            handle_invalid_line(line_num)
            del session
            return

    update_session(session, app_instance, line_num)

    if Incoming_line_ai.objects.filter(ai = session.cdr.ai, number = session.cdr.line).exists():
        call_response_type = Call_response_type.RECEIVE
    else:
        call_response_type = Call_response_type.CALLBACK

    if session.cdr.is_incoming and not filters_allow(session, call_response_type):
        logger.debug("Deny call from: " + session.getCallerID())
        session.hangup()
        session.destroy()
        del session
        return

    if app_instance.status == STATUS_ACTIVE:
        if is_callback_set(app_instance) and not is_incoming_line(line_num):
            session.setAutoHangup( True )
            schedule_callback(app_instance, session.getCallerID(), session.cdr)
        elif is_state_machine_impl( app_instance ):
            session.setAutoHangup( False )
            start_incoming_task(app_instance, session.getCallerID(), session.cdr, session.getUuid())
            if session:
                session.destroy()
            del session
            return
        else:
            call_incoming_handler(app_instance, session, args)
    else:
        logger.info("Skipping: " + str(app_instance) + " since not active.")


    # Cleanup session

    if session.getState() != 'CS_HANGUP':
        hangup_hook(session, 'app_end')
    if session:
        session.destroy()
    del session

    return

def fsapi(session,args):

    stream.write("hello")


#temp function to add duplicate call in number
def is_incoming_line(line_num):
    try:
            incoming_entry = Incoming_line_ai.objects.get(number = line_num)
            return True
    except:
        return False

def handle_invalid_line(line_num):
    logger.error("Incoming call to invalid line %s" % line_num)


def schedule_callback(ai, callerid, cdr):
    logger.debug('schedule_callback cdr id:' + str(cdr.id))

    callback_info = Callback_info(ai = ai, callerid = callerid, call_in_cdr=cdr)
    callback_info.save()

    eta = datetime.now()

    ai_settings = App_instance_settings.objects.filter(ai = ai, callback_delay__isnull = False)
    if ais.exists():
        eta = eta + timedelta(seconds = ais[0].callback_delay)

    queue = get_queue_for_task(ai, 'callback')
    CallbackTask.apply_async([callback_info.pk], queue = queue, eta = eta) #@UndefinedVariable


def start_incoming_task(ai, callerid, cdr, uuid):
    queue = get_queue_for_task(ai, default_queue='incoming')
    IncomingCallTask.apply_async([ai.id, get_formatted_number(callerid), cdr.id, uuid], queue = queue)


def hangup_hook(session, opt):
    logger.debug("Hangup hook reason: %s" % opt)
    Call_event_recorder.record_event(session, session.cdr, Events.HANGUP, data = session.ai)
    session.cdr.end_time = datetime.now()
    session.cdr.hangup_cause = session.getVariable("proto_specific_hangup_cause") or opt or 'Unknown'
    session.cdr.save()

    try:
        session.ai_ip_cdr = None
        ai_ip = None

        try:
            ai_ip = App_instance_line_ip.objects.get(ai = session.ai, line = session.callerIP, for_callback=False)
        except Exception,e:
            logger.info('ai_line_ip not found for ip:' + str(session.callerIP) + ', exception:' + str(e))
            ai_ip = App_instance_line_ip(ai = session.ai, line = session.callerIP, for_callback = False)
            ai_ip.save()

        logger.debug('hangup_hook: session call_in_cdr:' + str(session.call_in_cdr))

        if session.call_in_cdr:
            session.ai_ip_cdr = Ai_ip_cdr(ai_ip = ai_ip, cdr = session.cdr, call_in_cdr = session.call_in_cdr)
        else:
            session.ai_ip_cdr = Ai_ip_cdr(ai_ip = ai_ip, cdr = session.cdr)

        session.ai_ip_cdr.save()

    except Exception,e:
        logger.info('exception in hangup_hook:' + str(e))

def update_session(session, ai, line_num):

    session.ai = ai
    session.getCallerID = lambda: session.getVariable("caller_id_number")
    session.callerIP = session.getVariable("sip_received_ip")
    session.isHungup = lambda: session.getState() == 'CS_HANGUP'
    session.setHangupHook(hangup_hook)
    session.cdr = Cdr(is_incoming = True, ai_id = ai.id, line = line_num, callerid = session.getCallerID(), uuid = session.getVariable("uuid"))
    session.cdr.save()
    session.getUuid = lambda: session.cdr.uuid
    session.breakPlayback = lambda: None    
    session.call_in_cdr = None
    Call_event_recorder.record_event(session, session.cdr, Events.STARTED, data = session.ai)

