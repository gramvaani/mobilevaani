'''
Created on 06-Jan-2011

@author: bchandra
'''
from django.http import HttpResponseForbidden, HttpResponse
from django.core import serializers
from django.db import connection

from vapp.app_manager.models import Permission, User_permission
from vapp.perms import user_has_perms #@UnresolvedImport
import vapp.app_manager.perms as app_manager_perms #@UnresolvedImport

def perms_required(*perms):
    def perms_required_inner(f):
        def wrapper(request, ai_id, *args, **kwargs):
            if user_has_perms(request, ai_id, *perms):
                return f(request, ai_id, *args, **kwargs)
            else:
                return HttpResponseForbidden()
        return wrapper
    return perms_required_inner

def instance_perm(f):
    return perms_required(app_manager_perms.app_use)(f) #@UndefinedVariable

def select_field(obj, field_name):
    for field in field_name.split('.'):
        if field == '':
            continue
        obj = getattr(obj, field)
    return obj

def perms_required_per_obj(field_name, *perms):
    def inner(f):
        def wrapper(request, *args, **kwargs):
            if field_name == '':
                field_name = 'id'
            else:
                field_name = field_name + '_id'
            return HttpResponse(str(field_name))
        
            for obj in serializers.deserialize('json', request.POST['json']):
                ai_id = select_field(obj.object, field_name)
                if not user_has_perms(request, ai_id, *perms):
                    return HttpResponseForbidden()
            return f(request, *args, **kwargs)
        return wrapper
    return inner

class Perm_required_per_obj(object):
    def __init__(self, field_name, *perms):
        self.field_name = field_name
        self.perms = perms
    
    def __call__(self, f):
        def wrapper(request, *args, **kwargs):
            field_name = self.field_name
            if field_name == '':
                field_name = 'id'
            else:
                field_name = field_name + '_id'

            for obj in serializers.deserialize('json', request.POST['json']):
                ai_id = select_field(obj.object, field_name)
                if not user_has_perms(request, ai_id, *self.perms):
                    return HttpResponseForbidden()
            return f(request, *args, **kwargs)
        return wrapper
        
    
def instance_perm_per_obj(field_name):
    return Perm_required_per_obj(field_name, app_manager_perms.app_use) #@UndefinedVariable


def require_lock(*models):
    def _lock(func):
        def _do_lock(*args,**kws):
            cursor = connection.cursor() #@UndefinedVariable
            tables = [model._meta.db_table + ' WRITE' for model in models]
            sql = "LOCK TABLES " + ','.join(tables)
            cursor.execute(sql)
            try:
                return func(*args,**kws)
            finally:
                cursor.execute("UNLOCK TABLES")
                cursor.close()

        return _do_lock
    return _lock

