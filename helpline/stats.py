from vapp.helpline.models import Call, Grievance, Grievance_state
from vapp.app_manager.models import Cdr, Transition_event
from vapp.mnews.models import Groups_call_log

from vapp.stats.models import Stats, Stats_type
from vapp.stats.stats import populate_daily_cumulative_stats

from vapp.utils import is_start_of_week, is_start_of_month, send_email, get_total_seconds, get_ratio, \
generate_attachment, get_or_none, LINE_BREAK, generate_workbook_xlwt

from vapp.telephony.utils import get_formatted_number
from datetime import *
import uuid
from django.template.loader import render_to_string


def get_cdrs_for_ai( ai, start_date, end_date ):
    return Cdr.objects.filter( ai = ai, start_time__range = (start_date, end_date), is_incoming = True ).values_list( 'id',flat = True ).distinct()

# Need to add stats filter where test numbers could be filtered
def get_handlers_for_ai( ai, start_date, end_date ):
    numbers = Call.objects.filter( ai = ai, media__time__range = (start_date, end_date) ).values_list( 'handler', flat = True )
    return set( [ get_formatted_number( number ) for number in numbers ] )


def get_calls_avgdur_ansmachine_as_handler( ai, start_date, end_date ):
    total_duration = 0
    duration_count = 0
    calls = Call.objects.filter( ai = ai, media__time__range = (start_date, end_date), handler = '__RECORDER__' )

    for call in calls:
        trans_event = get_or_none( Transition_event, ai = ai, prev_state = 'answeringmachine', state = 'recordmessage', filename__contains = call.media.id )
        if trans_event and trans_event.cdr.end_time and trans_event.cdr.answered_time:
            call_duration = get_total_seconds( trans_event.cdr.end_time - trans_event.cdr.answered_time )
            if call_duration < 0:
                continue
            total_duration += call_duration
            duration_count += 1

    avg_duration = 0
    if duration_count != 0:
        avg_duration = get_ratio( total_duration, duration_count )

    return ( calls.count(), avg_duration )


def get_call_stats(ais, start_datetime, end_datetime):
    call_stats = []

    for ai in ais:
        stats = Stats()
        stats.unattended_calls_count = get_unattended_call_stats(ai, start_datetime, end_datetime)['calls']
        stats.ai_name = ai.name
        stats.total_calls = stats.unattended_calls_count
        stats.handler_call_data = []

        for handler in sorted(get_handlers_for_ai(ai, start_datetime, end_datetime)):
            if 'RECORDER' in handler:
                calls, avg_call_dur = get_calls_avgdur_ansmachine_as_handler(ai, start_datetime, end_datetime)
                stats.handler_call_data.append({ 'calls': calls, 'avg_call_dur': avg_call_dur, 'handler': 'Recorded Call' })
            else:
                calls, callers, avg_call_dur = get_calls_callers_avgdur(ai.id, start_datetime, end_datetime, handler)
                stats.handler_call_data.append({ 'calls': calls, 'avg_call_dur': avg_call_dur, 'handler': handler })

            stats.total_calls += calls

        call_stats.append(stats)

    return call_stats


# Following function needs to be replaced with vapp/stats/stats.py/get_callscount_callerscount_avgdur as
# app_manager_transition_events din't log ai_id and vi_id previously
def get_calls_callers_avgdur( ai_id, start_date, end_date, callerid ):
    total_duration = 0
    duration_count = 0
    callers = set()

    cdrids = Cdr.objects.raw( "select C.id from app_manager_cdr C, app_manager_transition_event T where cdr_id = C.id and C.ai_id = %s and callerid = '%s'  and C.start_time >= '%s' and C.start_time < '%s' and is_incoming = false and answered_time is not null" % ( ai_id, callerid, start_date, end_date ) );
    cdrids = [ c.id for c in cdrids ]
    cdrs = Cdr.objects.filter( id__in = cdrids )
    cdr_ids = list( cdrs.values_list( 'id', flat = True ).distinct() )
    outbound_cdrs = list( Groups_call_log.objects.filter( ai__id = ai_id, last_cdr__id__in = cdr_ids, success = True, last_cdr__isnull = False ).values_list( 'last_cdr_id', flat = True ).distinct() )
    calls = ( cdrs if ( ( not cdr_ids ) or ( not outbound_cdrs ) ) else cdrs.exclude( id__in = outbound_cdrs ) )

    for call in calls:
        callers.add( call.callerid )
        if call.end_time and call.answered_time:
            call_duration = get_total_seconds( call.end_time - call.answered_time )
            if call_duration < 0:
                continue
            total_duration += call_duration
            duration_count += 1
    avg_duration = 0
    if duration_count != 0:
        avg_duration = get_ratio( total_duration, duration_count )

    return ( calls.count(), len( callers ), avg_duration )


def create_unattended_calls_report( data, output ):
    unattended_calls = [ [ 'Number', 'Time of call' ] ]
    unattended_calls += data
    generate_attachment( unattended_calls, output )
    return output


def get_unattended_call_stats( ai, start_date, end_date, generate_report = False ):
    unattended_calls_count = 0
    unattended_calls = []
    
    for cdr in get_cdrs_for_ai( ai, start_date, end_date ):
        if not Transition_event.objects.filter( cdr__id = cdr, action__in = ['record_call', 'record'], filename__isnull = False ):
            if generate_report:
                unattended_call_cdr = Cdr.objects.get( id = cdr )
                unattended_calls.append( [ unattended_call_cdr.callerid, str( unattended_call_cdr.start_time ) ] )
            else:
                unattended_calls_count += 1

    return ( { 'report': create_unattended_calls_report( unattended_calls, output = '/tmp/' + str( ai.name ) + '_unattended_calls.xlsx' ) } if generate_report else { 'calls': unattended_calls_count } )

def populate_stats(ai, start_date = None, end_date = None, force_update = False):
    populate_daily_cumulative_stats(ai, force_update = force_update)


def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    data = get_call_stats(ais, start_datetime, end_datetime)
    reports = []
    if stat_type == Stats_type.WEEKLY:
        start_datetime = end_datetime - timedelta(days = 7)
        reports = [get_unattended_call_stats(ai, start_datetime, end_datetime, True)['report'] for ai in ais]
    return (data, reports)


def create_html_content(stats):
    return render_to_string('helpline/stats.html', { 'data': stats }) + LINE_BREAK


def generate_handler_status_datasheet(ais, start_datetime = None, end_datetime = None, states = [ Grievance_state.PUB, Grievance_state.ARC ], output_file = None ):
    title = ['Grievance ID', 'Date Time', 'Title', 'Number', 'Location', 'Grievance State', 'Handler Assigned', 'Handler name', 'Handler number', 'Grievance Recording', 'Transcript']
    grievances = Grievance.objects.filter(ai__in = ais, state__in = states, is_update = False)
    if start_datetime and end_datetime:
        grievances.filter( time__range = (start_datetime, end_datetime))
    data = []
    data.append(title)
    for grievance in grievances:
        grievance_data = [grievance.id, datetime.strftime(grievance.time, '%d/%m/%Y %H:%M:%S'), grievance.title, grievance.beneficiary, str(grievance.location), str(grievance.state)]
        if hasattr(grievance, 'handler') and grievance.handler:
            grievance_data += ['Y', str(grievance.handler.name), str(grievance.handler.num)]
        else:
            grievance_data  += ['N', 'None', 'None']
        if hasattr(grievance, 'recording') and grievance.recording:
            grievance_data += [grievance.recording.get_url(), grievance.transcript]
        else:
            grievance_data += ['None', 'None']
        data.append(grievance_data)
    wb_data = [{ 'name': 'Handler status data', 'header_row_count': 1, 'data': data }]
    if not output_file:
        output_file = ("/tmp/%s.xls" %(uuid.uuid1()))
    generate_workbook_xlwt(wb_data, output_file)
    return output_file

def get_conversation_duration(calls):
    duration = 0.0
    for each in calls:
        duration += float(each.media.get_duration())

    return duration
