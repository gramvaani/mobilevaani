from django.contrib import admin
from models import *


admin.site.register(HelplineMnewsConnector)
admin.site.register(Properties)
admin.site.register(Handler)
admin.site.register(Beneficiary_action_setting)
admin.site.register(Handler_action_setting)
admin.site.register( Handler_action )
admin.site.register( Handler_moderation_action )
admin.site.register( Handler_moderation_action_setting )
admin.site.register(BeneficiaryRetryPolicy)