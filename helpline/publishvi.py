from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *

from helpline.models import Properties, Grievance
from mnews.tasks import CallAndPlayTask
from app_manager.models import Cdr_trigger
from app_manager.common import get_queue_for_task

from datetime import datetime, timedelta

from vapp.log import get_logger
logger = get_logger()

class OnUpdatePubCallBeneficiaryController(BaseVappController):

    def __init__(self, ai, sessionData, vi_data ):

        super(OnUpdatePubCallBeneficiaryController, self).__init__( ai, sessionData )
        self.grievance = Grievance.objects.get(id = vi_data['grievance_id'])
        self.update = Grievance.objects.get(id = vi_data['update_id'])
        self.properties = Properties.objects.get(id = vi_data['property_id'])
        self.call_log = vi_data['group_call_log']
        self.call_log.tries += 1
        self.call_log.save()
        self.max_tries = vi_data['max_tries']

    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid, Cdr_trigger.CALLANDPLAY )

    def while_outgoingstart__sm_action_failure(self, events, eventData):
        if self.call_log.tries < self.max_tries:
            time = datetime.now() + timedelta(minutes = 30)
            queue = get_queue_for_task(self.ai, default_queue='push')
            kwargs = {'vi_data': RestrictedDict.convert_dict_to_rdict(self.vi_data, filter=True) }
            CallAndPlayTask.apply_async(args=[self.call_log.id, self.max_tries], 
                                        kwargs=kwargs, eta=time, queue=queue)
        return 'sm_action_failure'

    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.call_log.success = True
        self.call_log.last_cdr = self.sessionData.cdrs[0]
        self.call_log.save()

    def pre_welcome(self, prevState):
        return self.getPlaybackParams( self.properties.get_start_prompt_filename() )

    def while_checkpreviewtime__sm_action_success(self, events, eventData):
        if int(self.properties.grievance_preview_secs) > 0:
            return 'pub_preview_time_set'
        else:
            return 'pub_preview_time_not_set'

    def pre_playfullgrievance(self, prevState):
        return self.getPlaybackParams( self.grievance.recording.get_full_filename() )

    def pre_playlimitedgrievance(self, prevState):
        return self.getPlaybackParams( self.grievance.recording.get_full_filename() )

    def pre_timedsleep(self, prevState):
        self.preview_duration = int(self.properties.grievance_preview_secs)
        return self.getTimedSleepParams(self.preview_duration)

    def pre_playendprompt(self, prevState):
        return self.getPlaybackParams( self.properties.get_end_prompt_filename() )

    def pre_playupdate(self, prevState):
        return self.getPlaybackParams( self.update.recording.get_full_filename() )


OnUpdatePubCallBeneficiaryStateMap = [

    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url'],
        },
    },
    {   'name':'welcome',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkpreviewtime':['sm_action_success'],
        },
    },
    {   'name':'checkpreviewtime',
        'action':'none',
        'transitions': {
            'playlimitedgrievance':['pub_preview_time_set'],
            'playfullgrievance':['pub_preview_time_not_set'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'playfullgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'playendprompt':['sm_action_success'],
        },
    },
    {   'name':'playlimitedgrievance',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'timedsleep':['sm_action_success'],
        },
    },
    {   'name':'timedsleep',
        'action':'timed_sleep',
        'transitions': {
            'stop':['sm_action_failure'],
            'timedbreak':['sm_action_success'],
        },
    },
    {   'name':'timedbreak',
        'action':'break_media',
        'transitions': {
            'stop':['sm_action_failure'],
            'playendprompt':['sm_action_success'],
        },
    },
    {   'name':'playendprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'playupdate':['sm_action_success'],
        },
    },
    {   'name':'playupdate',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure', 'sm_action_success'],
        },
    },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {},
    },
    ]