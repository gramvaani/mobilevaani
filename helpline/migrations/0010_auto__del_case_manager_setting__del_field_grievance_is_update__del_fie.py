# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Case_manager_setting'
        db.delete_table(u'helpline_case_manager_setting')

        # Deleting field 'Grievance.is_update'
        db.delete_column(u'helpline_grievance', 'is_update')

        # Deleting field 'Grievance.callerid'
        db.delete_column(u'helpline_grievance', 'callerid')

        # Deleting field 'Grievance.caller_type'
        db.delete_column(u'helpline_grievance', 'caller_type')

        # Adding field 'Grievance.status'
        db.add_column(u'helpline_grievance', 'status',
                      self.gf('django.db.models.fields.CharField')(default='PR', max_length=3),
                      keep_default=False)

        # Adding field 'Grievance.beneficiary'
        db.add_column(u'helpline_grievance', 'beneficiary',
                      self.gf('vapp.app_manager.models.CalleridField')(default='911111111111', max_length=20),
                      keep_default=False)

        # Adding field 'Grievance.update_from'
        db.add_column(u'helpline_grievance', 'update_from',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=12),
                      keep_default=False)

        # Adding field 'Grievance.conversation_type'
        db.add_column(u'helpline_grievance', 'conversation_type',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=25),
                      keep_default=False)

        # Adding field 'Properties.handler_schedule'
        db.add_column(u'helpline_properties', 'handler_schedule',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='handler_schedule', null=True, to=orm['schedule.Schedule']),
                      keep_default=False)

        # Adding field 'Properties.handler_sched_off_ans_mc'
        db.add_column(u'helpline_properties', 'handler_sched_off_ans_mc',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Properties.allow_grievance_autopublish'
        db.add_column(u'helpline_properties', 'allow_grievance_autopublish',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'Case_manager_setting'
        db.create_table(u'helpline_case_manager_setting', (
            ('handler_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='handler_ai', to=orm['app_manager.App_instance'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('beneficiary_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='beneficiary_ai', to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'helpline', ['Case_manager_setting'])

        # Adding field 'Grievance.is_update'
        db.add_column(u'helpline_grievance', 'is_update',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Grievance.callerid'
        raise RuntimeError("Cannot reverse this migration. 'Grievance.callerid' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Grievance.callerid'
        db.add_column(u'helpline_grievance', 'callerid',
                      self.gf('vapp.app_manager.models.CalleridField')(max_length=20),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Grievance.caller_type'
        raise RuntimeError("Cannot reverse this migration. 'Grievance.caller_type' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Grievance.caller_type'
        db.add_column(u'helpline_grievance', 'caller_type',
                      self.gf('django.db.models.fields.CharField')(max_length=12),
                      keep_default=False)

        # Deleting field 'Grievance.status'
        db.delete_column(u'helpline_grievance', 'status')

        # Deleting field 'Grievance.beneficiary'
        db.delete_column(u'helpline_grievance', 'beneficiary')

        # Deleting field 'Grievance.update_from'
        db.delete_column(u'helpline_grievance', 'update_from')

        # Deleting field 'Grievance.conversation_type'
        db.delete_column(u'helpline_grievance', 'conversation_type')

        # Deleting field 'Properties.handler_schedule'
        db.delete_column(u'helpline_properties', 'handler_schedule_id')

        # Deleting field 'Properties.handler_sched_off_ans_mc'
        db.delete_column(u'helpline_properties', 'handler_sched_off_ans_mc')

        # Deleting field 'Properties.allow_grievance_autopublish'
        db.delete_column(u'helpline_properties', 'allow_grievance_autopublish')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'data.eicher_delhi': {
            'Meta': {'object_name': 'Eicher_delhi'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'hcrcs': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '48'}),
            'ward': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'helpline.beneficiary_action_setting': {
            'Meta': {'object_name': 'Beneficiary_action_setting'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeric_key': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'helpline.call': {
            'Meta': {'object_name': 'Call'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'handler': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Eicher_delhi']", 'null': 'True'}),
            'media': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']"}),
            'urgency': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'helpline.grievance': {
            'Meta': {'object_name': 'Grievance'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'beneficiary': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'conversation_type': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'handler': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Handler']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True'}),
            'reference_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'UNM'", 'max_length': '3'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PR'", 'max_length': '3'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'transcript': ('django.db.models.fields.TextField', [], {}),
            'update_from': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'updates': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'parent'", 'symmetrical': 'False', 'to': u"orm['helpline.Grievance']"})
        },
        u'helpline.grievance_log': {
            'Meta': {'object_name': 'Grievance_log'},
            'curr_status': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'grievance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Grievance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_status': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'helpline.handler': {
            'Meta': {'object_name': 'Handler'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'num': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'priority': ('django.db.models.fields.IntegerField', [], {})
        },
        u'helpline.handler_action_setting': {
            'Meta': {'object_name': 'Handler_action_setting'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeric_key': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'helpline.helplinemnewsconnector': {
            'Meta': {'object_name': 'HelplineMnewsConnector'},
            'helpline_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_helpline_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_mnews_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'helpline.properties': {
            'Meta': {'object_name': 'Properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'helpline_properties_set'", 'to': u"orm['app_manager.App_instance']"}),
            'allow_grievance_autopublish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'handler_call_policy': ('django.db.models.fields.CharField', [], {'default': "'pri'", 'max_length': '3'}),
            'handler_sched_off_ans_mc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'handler_schedule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'handler_schedule'", 'null': 'True', 'to': u"orm['schedule.Schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prompt_handler_for_grievance_access': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sched_off_answering_mc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schedule.Schedule']", 'null': 'True', 'blank': 'True'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'panchayat'", 'null': 'True', 'to': u"orm['location.Panchayat']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Block']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'duration': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'schedule.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['schedule.Days']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repetition': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['helpline']