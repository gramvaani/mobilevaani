# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Call'
        db.create_table(u'helpline_call', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('media', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'])),
            ('callerid', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['data.Eicher_delhi'], null=True)),
            ('handler', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('urgency', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'helpline', ['Call'])

        # Adding model 'Handler'
        db.create_table(u'helpline_handler', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('num', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('priority', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'helpline', ['Handler'])

        # Adding model 'HelplineMnewsConnector'
        db.create_table(u'helpline_helplinemnewsconnector', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('helpline_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='connector_helpline_ai_set', to=orm['app_manager.App_instance'])),
            ('mnews_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='connector_mnews_ai_set', to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'helpline', ['HelplineMnewsConnector'])


    def backwards(self, orm):
        # Deleting model 'Call'
        db.delete_table(u'helpline_call')

        # Deleting model 'Handler'
        db.delete_table(u'helpline_handler')

        # Deleting model 'HelplineMnewsConnector'
        db.delete_table(u'helpline_helplinemnewsconnector')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'data.eicher_delhi': {
            'Meta': {'object_name': 'Eicher_delhi'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'hcrcs': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '48'}),
            'ward': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'helpline.call': {
            'Meta': {'object_name': 'Call'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'handler': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Eicher_delhi']", 'null': 'True'}),
            'media': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']"}),
            'urgency': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'helpline.handler': {
            'Meta': {'object_name': 'Handler'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'num': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'priority': ('django.db.models.fields.IntegerField', [], {})
        },
        u'helpline.helplinemnewsconnector': {
            'Meta': {'object_name': 'HelplineMnewsConnector'},
            'helpline_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_helpline_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_mnews_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['helpline']