# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Grievance_log'
        db.create_table(u'helpline_grievance_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('grievance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['helpline.Grievance'])),
            ('prev_status', self.gf('django.db.models.fields.CharField')(max_length=2, null=True)),
            ('current_status', self.gf('django.db.models.fields.CharField')(default='PR', max_length=2)),
            ('update_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'helpline', ['Grievance_log'])

        # Adding model 'Grievance'
        db.create_table(u'helpline_grievance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bookmark', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['mnews.Bookmark'], unique=True)),
            ('is_handler_assigned', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'helpline', ['Grievance'])

        # Adding model 'Grievance_handler_map'
        db.create_table(u'helpline_grievance_handler_map', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('grievance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['helpline.Grievance'])),
            ('handler', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['helpline.Handler'])),
            ('update_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'helpline', ['Grievance_handler_map'])

        # Adding model 'Grievance_action_setting'
        db.create_table(u'helpline_grievance_action_setting', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('numeric_key', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'helpline', ['Grievance_action_setting'])

        # Adding field 'Properties.handler_schedule'
        db.add_column(u'helpline_properties', 'handler_schedule',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='handler_schedule', null=True, to=orm['schedule.Schedule']),
                      keep_default=False)

        # Adding field 'Properties.handler_sched_off_ans_mc'
        db.add_column(u'helpline_properties', 'handler_sched_off_ans_mc',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Properties.prompt_handler_for_grievance_access'
        db.add_column(u'helpline_properties', 'prompt_handler_for_grievance_access',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Grievance_log'
        db.delete_table(u'helpline_grievance_log')

        # Deleting model 'Grievance'
        db.delete_table(u'helpline_grievance')

        # Deleting model 'Grievance_handler_map'
        db.delete_table(u'helpline_grievance_handler_map')

        # Deleting model 'Grievance_action_setting'
        db.delete_table(u'helpline_grievance_action_setting')

        # Deleting field 'Properties.handler_schedule'
        db.delete_column(u'helpline_properties', 'handler_schedule_id')

        # Deleting field 'Properties.handler_sched_off_ans_mc'
        db.delete_column(u'helpline_properties', 'handler_sched_off_ans_mc')

        # Deleting field 'Properties.prompt_handler_for_grievance_access'
        db.delete_column(u'helpline_properties', 'prompt_handler_for_grievance_access')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'abstract': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'cover_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cover_image'", 'null': 'True', 'to': u"orm['media.Image_caption_map']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'images'", 'symmetrical': 'False', 'to': u"orm['media.Image_caption_map']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'show_in_ui': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'campaign.campaign_category': {
            'Meta': {'object_name': 'Campaign_category'},
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign_category']", 'null': 'True', 'blank': 'True'})
        },
        u'data.eicher_delhi': {
            'Meta': {'object_name': 'Eicher_delhi'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'hcrcs': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '48'}),
            'ward': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'helpline.call': {
            'Meta': {'object_name': 'Call'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'handler': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Eicher_delhi']", 'null': 'True'}),
            'media': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']"}),
            'urgency': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'helpline.grievance': {
            'Meta': {'object_name': 'Grievance'},
            'bookmark': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['mnews.Bookmark']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_handler_assigned': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'helpline.grievance_action_setting': {
            'Meta': {'object_name': 'Grievance_action_setting'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeric_key': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'helpline.grievance_handler_map': {
            'Meta': {'object_name': 'Grievance_handler_map'},
            'grievance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Grievance']"}),
            'handler': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Handler']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'helpline.grievance_log': {
            'Meta': {'object_name': 'Grievance_log'},
            'current_status': ('django.db.models.fields.CharField', [], {'default': "'PR'", 'max_length': '2'}),
            'grievance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Grievance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_status': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'helpline.handler': {
            'Meta': {'object_name': 'Handler'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'num': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'priority': ('django.db.models.fields.IntegerField', [], {})
        },
        u'helpline.helplinemnewsconnector': {
            'Meta': {'object_name': 'HelplineMnewsConnector'},
            'helpline_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_helpline_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_mnews_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'helpline.properties': {
            'Meta': {'object_name': 'Properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'helpline_properties_set'", 'to': u"orm['app_manager.App_instance']"}),
            'handler_call_policy': ('django.db.models.fields.CharField', [], {'default': "'pri'", 'max_length': '3'}),
            'handler_sched_off_ans_mc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'handler_schedule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'handler_schedule'", 'null': 'True', 'to': u"orm['schedule.Schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prompt_handler_for_grievance_access': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sched_off_answering_mc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schedule.Schedule']", 'null': 'True', 'blank': 'True'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'panchayat'", 'null': 'True', 'to': u"orm['location.Panchayat']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Block']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.image': {
            'Meta': {'object_name': 'Image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'media.image_caption_map': {
            'Meta': {'object_name': 'Image_caption_map'},
            'caption': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']"})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'duration': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.bookmark': {
            'Meta': {'object_name': 'Bookmark'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'bookmark_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'news': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.category': {
            'Meta': {'object_name': 'Category'},
            'ai': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_subcategory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'Parent'", 'null': 'True', 'to': u"orm['mnews.Category']"}),
            'subcategories': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'subcategory'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['mnews.Category']"})
        },
        u'mnews.channel': {
            'Meta': {'object_name': 'Channel'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.format': {
            'Meta': {'object_name': 'Format'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.news': {
            'Meta': {'object_name': 'News'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'campaign_categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['campaign.Campaign_category']", 'symmetrical': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Category']", 'null': 'True'}),
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Channel']"}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'parent'", 'symmetrical': 'False', 'to': u"orm['mnews.News']"}),
            'detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'detail'", 'null': 'True', 'to': u"orm['media.Recording']"}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Format']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_advertisement': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_comment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_mod_flagged': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_src_caller': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True', 'blank': 'True'}),
            'location_text': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'pub_order': ('django.db.models.fields.SmallIntegerField', [], {'default': '9999'}),
            'qualifier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Qualifier']", 'null': 'True'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sm_image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']", 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'summary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'summary'", 'null': 'True', 'to': u"orm['media.Recording']"}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'transcript': ('django.db.models.fields.TextField', [], {})
        },
        u'mnews.qualifier': {
            'Meta': {'object_name': 'Qualifier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'schedule.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['schedule.Days']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repetition': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['helpline']