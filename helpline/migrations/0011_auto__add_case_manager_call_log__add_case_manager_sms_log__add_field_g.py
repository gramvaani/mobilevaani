# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Case_manager_call_log'
        db.create_table(u'helpline_case_manager_call_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('grievance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['helpline.Grievance'])),
            ('gcl', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Groups_call_log'])),
        ))
        db.send_create_signal(u'helpline', ['Case_manager_call_log'])

        # Adding model 'Case_manager_sms_log'
        db.create_table(u'helpline_case_manager_sms_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('grievance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['helpline.Grievance'])),
            ('sms_message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_message'])),
            ('recipient_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'helpline', ['Case_manager_sms_log'])

        # Adding field 'Grievance.is_update'
        db.add_column(u'helpline_grievance', 'is_update',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Grievance.pub_order'
        db.add_column(u'helpline_grievance', 'pub_order',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=9999),
                      keep_default=False)

        # Deleting field 'Properties.allow_grievance_autopublish'
        db.delete_column(u'helpline_properties', 'allow_grievance_autopublish')

        # Adding field 'Properties.allow_grievance_autopub'
        db.add_column(u'helpline_properties', 'allow_grievance_autopub',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Properties.allow_update_autopub'
        db.add_column(u'helpline_properties', 'allow_update_autopub',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Properties.beneficiary_grv_update_sched'
        db.add_column(u'helpline_properties', 'beneficiary_grv_update_sched',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Transient_group_schedule'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Properties.update_dial_out_max_tries'
        db.add_column(u'helpline_properties', 'update_dial_out_max_tries',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=3),
                      keep_default=False)

        # Adding field 'Properties.beneficiary_grv_update_sms'
        db.add_column(u'helpline_properties', 'beneficiary_grv_update_sms',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='beneficiary_grv_update_sms', null=True, to=orm['sms.SMS_template']),
                      keep_default=False)

        # Adding field 'Properties.beneficiary_grv_assigned_sms'
        db.add_column(u'helpline_properties', 'beneficiary_grv_assigned_sms',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='beneficiary_grv_assigned_sms', null=True, to=orm['sms.SMS_template']),
                      keep_default=False)

        # Adding field 'Properties.handler_grv_assigned_sms'
        db.add_column(u'helpline_properties', 'handler_grv_assigned_sms',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='handler_grv_assigned_sms', null=True, to=orm['sms.SMS_template']),
                      keep_default=False)

        # Adding field 'Properties.notficiation_start_prompt'
        db.add_column(u'helpline_properties', 'notficiation_start_prompt',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Properties.notficiation_end_prompt'
        db.add_column(u'helpline_properties', 'notficiation_end_prompt',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Properties.grievance_preview_secs'
        db.add_column(u'helpline_properties', 'grievance_preview_secs',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=10),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Case_manager_call_log'
        db.delete_table(u'helpline_case_manager_call_log')

        # Deleting model 'Case_manager_sms_log'
        db.delete_table(u'helpline_case_manager_sms_log')

        # Deleting field 'Grievance.is_update'
        db.delete_column(u'helpline_grievance', 'is_update')

        # Deleting field 'Grievance.pub_order'
        db.delete_column(u'helpline_grievance', 'pub_order')

        # Adding field 'Properties.allow_grievance_autopublish'
        db.add_column(u'helpline_properties', 'allow_grievance_autopublish',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Deleting field 'Properties.allow_grievance_autopub'
        db.delete_column(u'helpline_properties', 'allow_grievance_autopub')

        # Deleting field 'Properties.allow_update_autopub'
        db.delete_column(u'helpline_properties', 'allow_update_autopub')

        # Deleting field 'Properties.beneficiary_grv_update_sched'
        db.delete_column(u'helpline_properties', 'beneficiary_grv_update_sched_id')

        # Deleting field 'Properties.update_dial_out_max_tries'
        db.delete_column(u'helpline_properties', 'update_dial_out_max_tries')

        # Deleting field 'Properties.beneficiary_grv_update_sms'
        db.delete_column(u'helpline_properties', 'beneficiary_grv_update_sms_id')

        # Deleting field 'Properties.beneficiary_grv_assigned_sms'
        db.delete_column(u'helpline_properties', 'beneficiary_grv_assigned_sms_id')

        # Deleting field 'Properties.handler_grv_assigned_sms'
        db.delete_column(u'helpline_properties', 'handler_grv_assigned_sms_id')

        # Deleting field 'Properties.notficiation_start_prompt'
        db.delete_column(u'helpline_properties', 'notficiation_start_prompt')

        # Deleting field 'Properties.notficiation_end_prompt'
        db.delete_column(u'helpline_properties', 'notficiation_end_prompt')

        # Deleting field 'Properties.grievance_preview_secs'
        db.delete_column(u'helpline_properties', 'grievance_preview_secs')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_reference': {
            'Meta': {'object_name': 'VI_reference'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'target_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi_data': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'})
        },
        u'data.eicher_delhi': {
            'Meta': {'object_name': 'Eicher_delhi'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'hcrcs': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '48'}),
            'ward': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'helpline.beneficiary_action_setting': {
            'Meta': {'object_name': 'Beneficiary_action_setting'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeric_key': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'helpline.call': {
            'Meta': {'object_name': 'Call'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'handler': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Eicher_delhi']", 'null': 'True'}),
            'media': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']"}),
            'urgency': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'helpline.case_manager_call_log': {
            'Meta': {'object_name': 'Case_manager_call_log'},
            'gcl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Groups_call_log']"}),
            'grievance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Grievance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'helpline.case_manager_sms_log': {
            'Meta': {'object_name': 'Case_manager_sms_log'},
            'grievance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Grievance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'recipient_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_message']"})
        },
        u'helpline.grievance': {
            'Meta': {'object_name': 'Grievance'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'beneficiary': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'conversation_type': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'handler': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Handler']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_update': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'pub_order': ('django.db.models.fields.SmallIntegerField', [], {'default': '9999'}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True'}),
            'reference_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'UNM'", 'max_length': '3'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PR'", 'max_length': '3'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'transcript': ('django.db.models.fields.TextField', [], {}),
            'update_from': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'updates': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'parent'", 'symmetrical': 'False', 'to': u"orm['helpline.Grievance']"})
        },
        u'helpline.grievance_log': {
            'Meta': {'object_name': 'Grievance_log'},
            'curr_status': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'grievance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['helpline.Grievance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_status': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'helpline.handler': {
            'Meta': {'object_name': 'Handler'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'num': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'priority': ('django.db.models.fields.IntegerField', [], {})
        },
        u'helpline.handler_action_setting': {
            'Meta': {'object_name': 'Handler_action_setting'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeric_key': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'helpline.helplinemnewsconnector': {
            'Meta': {'object_name': 'HelplineMnewsConnector'},
            'helpline_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_helpline_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'connector_mnews_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'helpline.properties': {
            'Meta': {'object_name': 'Properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'helpline_properties_set'", 'to': u"orm['app_manager.App_instance']"}),
            'allow_grievance_autopub': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allow_update_autopub': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'beneficiary_grv_assigned_sms': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'beneficiary_grv_assigned_sms'", 'null': 'True', 'to': u"orm['sms.SMS_template']"}),
            'beneficiary_grv_update_sched': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']", 'null': 'True', 'blank': 'True'}),
            'beneficiary_grv_update_sms': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'beneficiary_grv_update_sms'", 'null': 'True', 'to': u"orm['sms.SMS_template']"}),
            'grievance_preview_secs': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '10'}),
            'handler_call_policy': ('django.db.models.fields.CharField', [], {'default': "'pri'", 'max_length': '3'}),
            'handler_grv_assigned_sms': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'handler_grv_assigned_sms'", 'null': 'True', 'to': u"orm['sms.SMS_template']"}),
            'handler_sched_off_ans_mc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'handler_schedule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'handler_schedule'", 'null': 'True', 'to': u"orm['schedule.Schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notficiation_end_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'notficiation_start_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'prompt_handler_for_grievance_access': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sched_off_answering_mc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schedule.Schedule']", 'null': 'True', 'blank': 'True'}),
            'update_dial_out_max_tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'panchayat'", 'null': 'True', 'to': u"orm['location.Panchayat']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Block']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'duration': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.ai_transient_group': {
            'Meta': {'object_name': 'Ai_transient_group'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vi_reference': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_reference']", 'null': 'True', 'blank': 'True'})
        },
        u'mnews.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'mnews.groups_call_log': {
            'Meta': {'object_name': 'Groups_call_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'group_schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'vi_conf': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True', 'blank': 'True'})
        },
        u'mnews.transient_group': {
            'Meta': {'object_name': 'Transient_group'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'generating_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mnews.transient_group_schedule': {
            'Meta': {'object_name': 'Transient_group_schedule'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ai_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Ai_transient_group']"}),
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mnews.Days']", 'symmetrical': 'False'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'onpush_sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']", 'null': 'True', 'blank': 'True'}),
            'play_ai': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'prompt_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'repeat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'schedule.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['schedule.Days']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repetition': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['helpline']