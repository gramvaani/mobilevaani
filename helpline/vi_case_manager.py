from django.db.models import Q

from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.helpline.models import *

from sms.tasks import SMSTask

from vapp.utils import Moderation_source
import stat
from datetime import datetime
from vapp.log import get_logger
logger = get_logger()


class HelplineController(BaseVappController):
    call = None
    newConversation = None

    ACTION_STATE_MAP = {
        Case_manager_action.RECORD_GRIEVANCE: 'record_grievance',
        Case_manager_action.CALL_HANDLER: 'call_handler',
        Case_manager_action.UPDATE: 'record_update',
        Case_manager_action.CALL_BENEFICIARY: 'call_beneficiary',
        Case_manager_action.REPLAY: 'play_grievance',
        Case_manager_action.INPUT: 'input_reference_id',
        Case_manager_action.ASSIGN_HANDLER: 'assign_handler',
        Case_manager_action.FLAG: 'flag_grievance',
    }

    @classmethod
    def saveGrievance(cls, grievance):
        grievance.save()

    def getActionState(self, digit):
        action = self.actionSettingsMap.get(digit)
        if not action:
            return 'invalid_action'
        return self.ACTION_STATE_MAP.get(action) or 'invalid_action'

    def getCallerSchedule(self):
        return self.properties.handler_schedule if self.callerType == Caller_type.HANDLER else self.properties.schedule

    def allowRecordingOnAnsMc(self):
        if self.callerType == Caller_type.HANDLER:
            return self.properties.handler_sched_off_answering_mc
        else:
            return self.properties.sched_off_answering_mc

    def isCallSaved(self, call):
        saved_call = Call.objects.filter(pk = call.id)
        return saved_call.exists()

    def initGrievances(self):
        self.grievances = Grievance.objects.filter(ai = self.ai, handler__isnull = True, state = Grievance_state.PUB, is_update = False, recording__isnull = False).exclude(status = Grievance_status.RS).order_by('-time')
        self.currentGrievance = 0

    def nextGrievanceEvent(self):
        if self.currentGrievance < len(self.grievances) - 1:
            self.currentGrievance += 1
            return 'next_grievance'
        else:
            return 'all_grievances_played'

    def prevGrievanceEvent(self):
        if self.currentGrievance > 0:
            self.currentGrievance -= 1
        else:
            self.currentGrievance = 0
        return 'prev_grievance'

    def sendGrievanceUpdateNotifications(self, update):
        msg = self.properties.create_beneficiary_grv_update_sms(self.grievance)
        sms_msg = SMSTask.create_send_msg(self.ai.id, msg, self.grievance.beneficiary)
        Case_manager_sms_log.objects.create(grievance = self.grievance, sms_message = sms_msg, recipient_type = Caller_type.BENEFICIARY)
        call_beneficiary_on_update_pub(self.properties, self.grievance, update)

    def sendHandlerAssignmentNotifications(self):
        handler_msg = self.properties.create_handler_grv_assigned_sms(self.grievance)
        handler_sms_msg = SMSTask.create_send_msg(self.ai.id, handler_msg, self.grievance.handler.num)
        Case_manager_sms_log.objects.create(grievance = self.grievance, sms_message = handler_sms_msg, recipient_type = Caller_type.HANDLER)

        beneficiary_msg = self.properties.create_beneficiary_grv_assigned_sms(self.grievance)
        beneficiary_sms_msg = SMSTask.create_send_msg(self.ai.id, beneficiary_msg, self.grievance.beneficiary)
        Case_manager_sms_log.objects.create(grievance = self.grievance, sms_message = beneficiary_sms_msg, recipient_type = Caller_type.BENEFICIARY)

    def __init__( self,  ai, sessionData, vi_data ):
        super(HelplineController, self).__init__( ai, sessionData, vi_data )
        self.sessionData = sessionData
        self.properties = Properties.objects.get(ai = self.ai)
        self.handlers = Handler.objects.filter(ai = self.ai)
        self.callerUuid = None

    def pre_outgoingstart(self):
        return self.getStartCallParams(self.sessionData.callerid)

    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.callerUuid = self.sessionData.uuids[0]
        return 'sm_action_success'

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def while_incomingstart__sm_action_success(self, events, eventData):
        self.callerUuid = self.sessionData.uuids[0]
        return 'sm_action_success'

    def pre_welcome(self):
        return self.getPromptParams('helpline_welcome')

    def while_checkcallertype__sm_action_success(self, events, eventData):
        try:
            self.handler = self.handlers.get(num = self.sessionData.callerid)
            self.callerType = Caller_type.HANDLER
            self.actionSettingsMap = Handler_action_setting.get_action_settings_map(self.ai)
        except:
            self.callerType = Caller_type.BENEFICIARY
            self.actionSettingsMap = Beneficiary_action_setting.get_action_settings_map(self.ai)

    def while_checkschedule__sm_action_success(self, events, eventData):
        schedule = self.getCallerSchedule()
        return 'schedule_off' if schedule and not schedule.is_schedule_on() else 'schedule_on'

    def pre_scheduleoff(self):
        return self.getPromptParams('helpline_schedule_off')

    def while_scheduleoff__sm_action_success(self, events, eventData):
        return 'record_msg' if self.allowRecordingOnAnsMc() else 'dont_record_msg'

    def pre_recordmessage(self):
        self.call = Call(ai = self.ai, handler = Handler.RECORDER, callerid = self.sessionData.callerid)
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.call, Call.media)

    def while_instructions__sm_action_success(self, events, eventData):
        return 'handler_instructions' if self.callerType == Caller_type.HANDLER else 'beneficiary_instructions'

    def pre_beneficiaryinstructions(self):
        return self.getPromptPlayAndGetParams(promptName = 'helpline_beneficiary_instruction', tries = 3)

    def while_beneficiaryinstructions__sm_action_success(self, events, eventData):
        return self.getActionState( self.getPlayAndGetDigits(eventData) )

    def pre_recordgrievanceprompt(self):
        return self.getPromptParams('helpline_record_grievance')

    def pre_recordgrievance(self):
        title = 'Grievance, %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        self.newGrievance = Grievance(ai = self.ai, beneficiary = self.sessionData.callerid, title = title, is_update = False)
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newGrievance, Grievance.recording)

    def while_recordgrievance__sm_action_success(self, events, eventData):
        try:
            if self.properties.allow_grievance_autopub:
                self.newGrievance.state = Grievance_state.PUB
            HelplineController.saveGrievance(self.newGrievance)
            return 'sm_action_success'
        except:
            return 'grievance_record_error'

    def pre_grievancerecorderror(self):
        return self.getPromptParams('helpline_grievance_record_error')

    def while_checkhandlers__sm_action_success(self, events, eventData):
        self.grievances = get_active_grievances_for_user(self.sessionData.callerid, self.ai)
        if not self.grievances.exists():
            return 'no_active_grievances'
        grievance_count = self.grievances.count()
        if grievance_count > 1:
            return 'multiple_grievances'
        elif grievance_count == 1:
            self.grievance = self.grievances.latest('id')
            self.handler = self.grievance.handler
            return 'handler_found'

    def pre_noactivegrievances(self):
        return self.getPromptParams('helpline_no_active_grievances')

    def pre_beneficiaryinputrefid(self):
        return self.getPromptPlayAndGetParams(promptName = 'helpline_input_reference_id', maxDigits = 9, tries = 3)

    def while_beneficiaryinputrefid__sm_action_success(self, events, eventData):
        try:
            reference_id = self.getPlayAndGetDigits(eventData)
            self.grievance = self.grievances.get(reference_id = reference_id)
            self.handler = self.grievance.handler
            return 'grievance_resolved' if self.grievance.status == Grievance_status.RS else 'active_grievance'
        except:
            return 'invalid_grievance'

    def pre_beneficiaryinvalidgrievance(self):
        return self.getPromptParams('helpline_invalid_grievance')

    def pre_beneficiaryresolvedgrievance(self):
        return self.getPromptParams('helpline_grievance_resolved')

    def pre_beneficiarywaitprompt(self):
        return self.getPromptParams('helpline_please_wait')

    def pre_callhandler(self):
        return self.getStartCallParams(self.handler.num)

    def while_callhandler__sm_action_failure(self, events, eventData):
        uuid = eventData.get('Unique_ID')
        if uuid and uuid == self.callerUuid:
            return 'caller_hung_up'
        else:
            return 'handler_hung_up'

    def pre_beneficiaryinvalidaction(self):
        return self.getPromptParams('helpline_invalid_action')

    def while_handlerinstructions__sm_action_success(self, events, eventData):
        if self.properties.prompt_handler_for_grievance_access:
            pending_grievances = Grievance.objects.filter( ai = self.ai, state = Grievance_state.PUB, is_update = False ).exclude( status = Grievance_status.RS )
            if pending_grievances.count() == 0:
                return 'handler_no_grievances'
            return 'input_reference_id'
        else:
            self.initGrievances()
            return 'play_grievance_instruction'

    def pre_handlerinputreferenceid(self):
        return self.getPromptPlayAndGetParams(promptName = 'helpline_input_reference_id', maxDigits = 9, tries = 3)

    def while_handlerinputreferenceid__sm_action_success(self, events, eventData):
        try:
            reference_id = self.getPlayAndGetDigits(eventData)
            self.grievance = Grievance.objects.get(ai = self.ai, reference_id = reference_id, handler = self.handler)
            return 'grievance_resolved' if self.grievance.status == Grievance_status.RS else 'valid_grievance'
        except:
            return 'invalid_grievance'

    def pre_handleractioninstructions(self):
        return self.getPromptParams('helpline_grievance_action_instruction')

    def pre_handlerinvalidgrievance(self):
        return self.getPromptParams('helpline_invalid_grievance')

    def pre_handlerresolvedgrievance(self):
        return self.getPromptParams('helpline_grievance_resolved')

    def pre_handlerplaygrievance(self):
        return self.getPlaybackParams(self.grievance.recording.get_full_filename())

    def while_handlerplaygrievance__dtmf_1(self, events, eventData):
        return self.getActionState(1)

    def while_handlerplaygrievance__dtmf_2(self, events, eventData):
        return self.getActionState(2)

    def while_handlerplaygrievance__dtmf_3(self, events, eventData):
        return self.getActionState(3)

    def while_handlerplaygrievance__dtmf_4(self, events, eventData):
        return self.getActionState(4)

    def while_handlerplaygrievance__dtmf_5(self, events, eventData):
        return self.getActionState(5)

    def while_handlerplaygrievance__dtmf_6(self, events, eventData):
        return self.getActionState(6)

    def while_handlerplaygrievance__sm_action_success(self, events, eventData):
        if self.properties.prompt_handler_for_grievance_access:
            return 'action_prompt'
        else:
            if self.currentGrievance < len(self.grievances) - 1:
                self.currentGrievance += 1
                return 'handler_continue_grievances'
            else:
                return 'handler_no_grievances'

    def pre_handlerrecordupdateprompt(self):
        return self.getPromptParams('helpline_record_update')

    def pre_handlerrecordupdate(self):
        title = 'Update from Handler, %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        self.newUpdate = Grievance(ai = self.ai, title = title, update_from = Caller_type.HANDLER, handler = self.handler, is_update = True)
        self.newUpdate.parent_id_value = self.grievance.id
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newUpdate, Grievance.recording)

    def while_handlerrecordupdate__sm_action_success(self, events, eventData):
        if self.properties.allow_update_autopub:
            self.newUpdate.state = Grievance_state.PUB
        HelplineController.saveGrievance(self.newUpdate)
        try:
            self.grievance.updates.add(self.newUpdate)
            if self.newUpdate.state == Grievance_state.PUB:
                self.sendGrievanceUpdateNotifications(self.newUpdate)
        except Exception,e:
            logger.exception('exception while updating grievance id %s - %s' %(self.grievance.id, e))

        return 'sm_action_success'

    def pre_handlerwaitprompt(self):
        return self.getPromptParams('helpline_please_wait')

    def pre_callbeneficiary(self):
        return self.getStartCallParams(self.grievance.beneficiary)

    def while_callbeneficiary__sm_action_failure(self, events, eventData):
        uuid = eventData.get('Unique_ID')
        if uuid and uuid == self.callerUuid:
            return 'caller_hung_up'
        else:
            return 'beneficiary_hung_up'

    def pre_handleractionprompt(self):
        return self.getPromptPlayAndGetParams(promptName = 'helpline_grievance_action', tries = 3)

    def while_handleractionprompt__sm_action_success(self, events, eventData):
        return self.getActionState( self.getPlayAndGetDigits(eventData) )

    def pre_handlerinvalidactionchoice(self):
        return self.getPromptParams('helpline_invalid_action')

    def while_handlerflaggrievance__sm_action_success(self, events, eventData):
        if self.grievance.status == Grievance_status.FG:
            return 'sm_action_success'
        self.grievance.status = Grievance_status.FG
        self.grievance.save()
        return 'sm_action_success'

    def while_assignhandler__sm_action_success(self, events, eventData):
        if not self.grievance.handler:
            self.grievance.handler = self.handler
            self.grievance.save()
            self.sendHandlerAssignmentNotifications()
            return 'sm_action_success'

        return 'handler_already_assigned'

    def pre_handleralreadyassigned(self):
        return self.getPromptParams('helpline_handler_already_assigned')

    def while_handleralreadyassigned__sm_action_success(self, events, eventData):
        if self.properties.prompt_handler_for_grievance_access:
            return 'action_prompt'
        else:
            return 'handler_continue_grievances'

    def pre_postactionprompt(self):
        return self.getPromptParams('helpline_post_action_prompt')

    def while_postactionprompt__sm_action_success(self, events, eventData):
        if self.properties.prompt_handler_for_grievance_access:
            return 'handler_input_reference_id'
        else:
            return 'handler_continue_grievances'

    def pre_handlernogrievances(self):
        return self.getPromptParams('helpline_no_grievances_available')

    def pre_handlerplaygrievanceinstruction(self):
        return self.getPromptParams('helpline_play_grievance_instruction')

    def while_listgrievances__sm_action_success(self, events, eventData):
        if self.grievances.exists():
            self.grievance = self.grievances[ self.currentGrievance ]
            self.grievance.moderator = self.handler
            self.grievance.moderation_source = Moderation_source.IVR
            return 'play_grievance'
        else:
            return 'handler_no_grievances'

    def while_nextgrievance__sm_action_success(self, events, eventData):
        return self.nextGrievanceEvent()

    def while_prevgrievance__sm_action_success(self, events, eventData):
        return self.prevGrievanceEvent()

    def pre_recordcall(self):
        if self.callerType == Caller_type.HANDLER:
            title = 'Conversation from Handler with Beneficiary,'
            conversation_type = Conversation_type.HANDLER_TO_BENEFICIARY
        else:
            title = 'Conversation from Beneficiary with Handler,'
            conversation_type = Conversation_type.BENEFICIARY_TO_HANDLER

        title += ' %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        self.newConversation = Grievance(ai = self.ai, handler = self.handler, beneficiary = self.grievance.beneficiary, title = title, conversation_type = conversation_type, is_update = True)
        self.newConversation.parent_id_value = self.grievance.id
        return self.getRecordCallParams(self.newConversation, Grievance.recording)

    def pre_bridgecalls(self):
        return self.getBridgeCallsParams(self.sessionData.uuids[0], self.sessionData.uuids[1])

    def pre_thankyou(self):
        return self.getPromptParams('helpline_thank_you')

    def post_stop(self, events):
        try:
            if self.newConversation:
                HelplineController.saveGrievance(self.newConversation)
                self.grievance.updates.add(self.newConversation)
        except Exception,e:
            logger.exception('exception while updating grievance id %s - %s' %(self.grievance.id, e))
        if not (self.call and self.call.media):
            return
        if not self.isCallSaved(self.call):
            return


HelplineStateDescriptionMap = [
    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url'],
        },
    },
    {   'name':'incomingstart',
        'action':'answer',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
        },
    },
    {   'name':'welcome',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkcallertype':['sm_action_success'],
        },
    },
    {   'name':'checkcallertype',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkschedule':['sm_action_success'],
        },
    },
    {   'name':'checkschedule',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'instructions':['schedule_on'],
            'scheduleoff':['schedule_off'],
        },
    },
    {   'name':'scheduleoff',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure', 'dont_record_msg'],
            'recordmessage':['record_msg'],
        },
    },
    {   'name':'recordmessage',
        'action':'record',
        'transitions': {
            'thankyou':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'instructions',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinstructions':['beneficiary_instructions'],
            'handlerinstructions':['handler_instructions'],
        },
    },
    {   'name':'handlerinstructions',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerplaygrievanceinstruction':['play_grievance_instruction'],
            'handlerinputreferenceid':['input_reference_id'],
            'handlernogrievances': [ 'handler_no_grievances' ],
        },
    },
    {   'name':'handlerinputreferenceid',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'handleractioninstructions':['valid_grievance'],
            'handlerinvalidgrievance':['invalid_grievance'],
            'handlerresolvedgrievance':['grievance_resolved'],
            'thankyou':['sm_get_digits_no_digits'],
        },
    },
    {   'name':'handleractioninstructions',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerplaygrievance':['sm_action_success'],
        },
    },
    {   'name':'handlerinvalidgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerinputreferenceid':['sm_action_success'],
        },
    },
    {   'name':'handlerresolvedgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerinputreferenceid':['sm_action_success'],
        },
    },
    {   'name':'handlerplaygrievanceinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'listgrievances':['sm_action_success'],
        },
    },
    {   'name':'listgrievances',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'prevgrievance':['prev_grievance'],
            'nextgrievance':['next_grievance'],
            'handlernogrievances':['handler_no_grievances'],
            'handlerplaygrievance':['play_grievance'],
        },
    },
    {   'name':'nextgrievance',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'listgrievances':['next_grievance'],
            'thankyou':['all_grievances_played'],
        },
    },
    {   'name':'prevgrievance',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'listgrievances':['prev_grievance'],
        },
    },
    {   'name':'handlernogrievances',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
        },
    },
    {   'name':'handlerplaygrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerrecordupdateprompt':['record_update'],
            'handlerwaitprompt':['call_beneficiary'],
            'handlerplaygrievance':['play_grievance'],
            'handlerflaggrievance':['flag_grievance'],
            'handlerinputreferenceid':['input_reference_id'],
            'assignhandler':['assign_handler'],
            'handleractionprompt':['action_prompt'],
            'listgrievances':['handler_continue_grievances'],
            'handlernogrievances':['handler_no_grievances'],
            'prevgrievance':['prev_grievance'],
            'nextgrievance':['next_grievance'],
        },
        'dtmf': {
            '#':'prev_grievance',
            '*':'next_grievance',
        },
    },
    {   'name':'handlerrecordupdateprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerrecordupdate':['sm_action_success'],
        },
    },
    {   'name':'handlerrecordupdate',
        'action':'record',
        'transitions': {
            'stop':['sm_action_failure'],
            'postactionprompt':['sm_action_success'],
        },
    },
    {   'name':'handlerwaitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'callbeneficiary':['sm_action_success'],
        },
    },
    {   'name':'callbeneficiary',
        'action':'originate',
        'transitions': {
            'beneficiaryanswered':['sm_action_success'],
            'stop':['caller_hung_up'],
            'thankyou':['beneficiary_hung_up'],
            'callbeneficiary':['sm_next_originate_url'],
        },
    },
    {   'name':'beneficiaryanswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'handleractionprompt',
        'action':'play_and_get_digits',
        'transitions': {
            'stop': ['sm_action_failure'],
            'handlerrecordupdateprompt':['record_update'],
            'handlerwaitprompt':['call_beneficiary'],
            'handlerplaygrievance':['play_grievance'],
            'handlerflaggrievance':['flag_grievance'],
            'handlerinputreferenceid':['input_reference_id'],
            'assignhandler':['assign_handler'],
            'handlerinvalidactionchoice': ['invalid_action'],
            'thankyou': ['sm_get_digits_no_digits'],
        },
    },
    {   'name':'handlerinvalidactionchoice',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handleractionprompt':['sm_action_success'],
        },
    },
    {   'name':'handlerflaggrievance',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'postactionprompt':['sm_action_success'],
        },
    },
    {   'name':'assignhandler',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'postactionprompt':['sm_action_success'],
            'handleralreadyassigned':['handler_already_assigned'],
        },
    },
    {   'name': 'handleralreadyassigned',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'handleractionprompt':['action_prompt'],
            'listgrievances':['handler_continue_grievances'],
        },
    },
    {   'name': 'postactionprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'handlerinputreferenceid':['handler_input_reference_id'],
            'nextgrievance':['handler_continue_grievances'],
        },
    },
    {   'name':'beneficiaryinstructions',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordgrievanceprompt':['record_grievance'],
            'checkhandlers':['call_handler'],
            'beneficiaryinvalidaction': ['invalid_action'],
            'thankyou':['sm_get_digits_no_digits'],
        },
    },
    {   'name':'beneficiaryinvalidaction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinstructions':['sm_action_success'],
        },
    },
    {   'name':'recordgrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordgrievance':['sm_action_success'],
        },
    },
    {   'name':'recordgrievance',
        'action':'record',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
            'grievancerecorderror':['grievance_record_error'],
        },
    },
    {   'name':'grievancerecorderror',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordgrievanceprompt':['sm_action_success'],
        },
    },
    {   'name':'checkhandlers',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'noactivegrievances':['no_active_grievances'],
            'beneficiaryinputrefid':['multiple_grievances'],
            'beneficiarywaitprompt':['handler_found'],
        },
    },
    {   'name':'noactivegrievances',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinstructions':['sm_action_success'],
        },
    },
    {   'name':'beneficiaryinputrefid',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinvalidgrievance':['invalid_grievance'],
            'beneficiaryresolvedgrievance':['grievance_resolved'],
            'beneficiarywaitprompt':['active_grievance'],
            'thankyou':['sm_get_digits_no_digits'],
        },
    },
    {   'name':'beneficiaryinvalidgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinputrefid':['sm_action_success'],
        },
    },
    {   'name':'beneficiaryresolvedgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinputrefid':['sm_action_success'],
        },
    },
    {   'name':'beneficiarywaitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'callhandler':['sm_action_success'],
        },
    },
    {   'name':'callhandler',
        'action':'originate',
        'transitions': {
            'handleranswered':['sm_action_success'],
            'stop':['caller_hung_up'],
            'thankyou':['handler_hung_up'],
        },
    },
    {   'name':'handleranswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'recordcall',
        'action':'record_call',
        'transitions': {
            'stop':['sm_action_failure'],
            'bridgecalls':['sm_action_success'],
        },
    },
    {   'name':'bridgecalls',
        'action':'bridge',
        'transitions': {
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'thankyou',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {},
    },
    ]
