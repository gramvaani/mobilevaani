from django.conf.urls import url
from django.db.models import Q

from tastypie.resources import ModelResource, ALL_WITH_RELATIONS, ALL, Resource
from tastypie import fields, http
from tastypie.authentication import ApiKeyAuthentication
from tastypie.utils import trailing_slash
from tastypie.authorization import Authorization, ReadOnlyAuthorization
from tastypie.exceptions import ImmediateHttpResponse

from datetime import datetime
from operator import and_

from vapp.helpline.models import Handler, Grievance, Caller_type, Grievance_status, Grievance_state, Call, Properties
from vapp.app_manager.common import get_queue_for_task
from vapp.app_manager.models import App_instance
from vapp.app_manager.api import AppInstanceResource
from vapp.location.api import LocationResource
from vapp.location.models import Location
from vapp.media.api import RecordingResource, recording_upload_impl
from vapp.api import AppInstanceAuthorization, apply_default_checks
from vapp.telephony.utils import get_formatted_number
from vapp.callerinfo.common import DEFAULT_CALLERID
from vapp.utils import Moderation_source

from mnews.models import Transient_group_schedule, Groups_call_log
from mnews.tasks import CallAndPlayTask

from vapp.log import get_request_logger
logger = get_request_logger()


class HandlerResource( ModelResource ):
    ai       = fields.ForeignKey(AppInstanceResource, 'ai')
    desc     = fields.CharField()
    location = fields.ForeignKey( LocationResource, 'location', full = True, null = True )

    class Meta:
        queryset = Handler.objects.all()
        resource_name = "helpline_handler"
        filtering = {
            'ai':  ALL_WITH_RELATIONS,
            'id':  [ 'exact' ],
            'num': [ 'contains' ],
            'ai_id': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/create%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view( 'create_handler' ), name = "api_create_handler" ),
        ]

    def create_handler( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'post' ] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_id = request.REQUEST[ 'ai_id' ]
            num = request.REQUEST[ 'num' ]
            name = request.REQUEST[ 'name' ]
            priority = request.REQUEST[ 'priority' ]
            location = request.REQUEST.get( 'location_id' )
            if location:
                location = Location.objects.get( pk = int( location ) )

            handler, created = Handler.objects.get_or_create( ai_id = ai_id, num = num, name = name, priority = priority, location = location )
            if created:
                handler_resource = HandlerResource()
                handler_bundle = handler_resource.build_bundle( obj = handler, request = request )
                self.log_throttled_access( request )
                return self.create_response( request, { 'handler': handler_resource.full_dehydrate( handler_bundle ), 'notification': 'Successfully created!' } )
            else:
                self.log_throttled_access( request )
                return self.create_response( request, { 'notification': 'Handler already exists for provided values' } )
        except:
            logger.exception( "create_handler: %s" % request )
        return self.create_response( request, {'error': "unable to create new handler"} )

    def dehydrate_desc( self, bundle ):
        handler = bundle.obj
        if not handler:
            return ""

        names = []
        if handler.location:
            location = handler.location
            names = [ location.state.name, location.district.name, location.block.name, location.panchayat.name, location.village ]

        names.extend( [ handler.name ] )
        while None in names:
            names.remove( None )
        return " - ".join( names ) if names else ""


class GrievanceResource( ModelResource ):
    ai        = fields.ForeignKey(AppInstanceResource, 'ai')
    location  = fields.ForeignKey(LocationResource, 'location', null = True)
    handler   = fields.ForeignKey(HandlerResource, 'handler', null = True)
    recording = fields.ForeignKey(RecordingResource, 'recording', full = True, null = True)
    parent_id = fields.IntegerField( attribute = 'parent_id', null = True )

    class Meta:
        queryset = Grievance.objects.all()
        resource_name = "helpline_grievance"
        filtering = {
            'ai': ALL_WITH_RELATIONS,
            'handler': ALL_WITH_RELATIONS,
            'is_update': ALL_WITH_RELATIONS,
            'state': ALL_WITH_RELATIONS,
            'status': ALL_WITH_RELATIONS,
            'time': ALL_WITH_RELATIONS,
	    'parent_id': ALL_WITH_RELATIONS,
            'conversation_type': ALL_WITH_RELATIONS,
            'title': ['exact', 'startswith', 'endswith', 'icontains'],
            'id': ['exact'],
            'beneficiary': ['startswith', 'contains'],
        }
        ordering = [ 'time', 'pub_order' ]
        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()

    def prepend_urls( self ):
        return [
            url(r"^(?P<resource_name>%s)/create%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_grievance' ), name = "api_create_grievance" ),
            url(r"^(?P<resource_name>%s)/create_update%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_update' ), name = "api_create_update" ),
            url(r"^(?P<resource_name>%s)/reparent_update%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'reparent_update' ), name = "api_reparent_update" ),
            url(r"^(?P<resource_name>%s)/upload_audio%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'upload_audio' ), name = "api_upload_audio" ),
            url(r"^(?P<resource_name>%s)/update_pub_order%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'update_pub_order' ), name = "api_update_pub_order" ),
            url(r"^(?P<resource_name>%s)/assign_handler%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'assign_handler' ), name = "api_assign_handler" ),
            url(r"^(?P<resource_name>%s)/call%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'initiate_call' ), name = "api_initiate_call" ),
        ]

    @apply_default_checks( allowed_methods = [ 'post' ] )
    def create_grievance(self, request, **kwargs):
        params = {}
        params[ 'ai_id' ] = kwargs[ 'ai' ]
        params[ 'location_id' ] = kwargs.get( 'location' )
        params[ 'beneficiary' ] = kwargs.get( 'beneficiary', DEFAULT_CALLERID )
        if kwargs.get('handler'):
            if not Handler.objects.filter(pk = kwargs.get('handler')).exists():
                raise Exception('provided handler does not exist.')
            if not Handler.objects.filter(pk = kwargs.get('handler'), ai_id = params['ai_id']).exists():
                raise Exception('handler should belong to same ai.')
            params['handler_id'] = kwargs.get('handler')

        time = datetime.now()
        params[ 'title' ] = kwargs.get( 'title', 'Grievance, Created at %s' % time.strftime( '%H:%M, %d %b\'%y' ) )
        params[ 'recording_id' ] = kwargs.get( 'recording' )
        params[ 'transcript' ] = kwargs.get( 'transcript', '' )
        params[ 'is_update' ] = False
        try:
            grievance = Grievance( **params )
            grievance.save()
            grievance_resource = GrievanceResource()
            grievance_bundle = grievance_resource.build_bundle( obj = grievance, request = request )
            return self.create_response( request, grievance_resource.full_dehydrate( grievance_bundle ), response_class = http.HttpCreated )
        except KeyError, e:
            raise ImmediateHttpResponse(http.HttpBadRequest("Insufficient parameters passed"))
        except:
            logger.exception( "create_grievance: %s" % request )
        return self.create_response(request, {'error': "unable to create new grievance"}, response_class = http.HttpApplicationError)

    @apply_default_checks( allowed_methods = [ 'post' ] )
    def create_update(self, request, **kwargs):
        parent = Grievance.objects.get( pk = kwargs[ 'grievance_id' ] )
        if parent.is_update:
            raise Exception('Update cannot be made to another update.')

        params = {}
        params[ 'ai_id' ] = parent.ai.id
        params[ 'location_id' ] = kwargs.get( 'location' )
        params[ 'beneficiary' ] = DEFAULT_CALLERID
        if kwargs.get('handler'):
            if not Handler.objects.filter(pk = kwargs.get('handler')).exists():
                raise Exception('provided handler does not exist.')
            if not Handler.objects.filter(pk = kwargs.get('handler'), ai_id = params['ai_id']).exists():
                raise Exception('handler should belong to same ai.')
            params['handler_id'] = kwargs.get('handler')
            params[ 'update_from' ] = Caller_type.HANDLER

        time = datetime.now()
        params[ 'title' ] = kwargs.get( 'title', 'Update, Created at %s' % time.strftime('%H:%M, %d %b\'%y') )
        params[ 'recording_id' ] = kwargs.get( 'recording' )
        params[ 'transcript' ] = kwargs.get( 'transcript', '' )
        params[ 'is_update' ] = True
        try:
            update = Grievance( **params )
            update.parent_id_value = parent.id
            update.save()
            parent.updates.add( update )
            grievance_resource = GrievanceResource()
            grievance_bundle = grievance_resource.build_bundle( obj = update, request = request )
            return self.create_response( request, grievance_resource.full_dehydrate( grievance_bundle ), response_class = http.HttpCreated )
        except KeyError, e:
            raise ImmediateHttpResponse(http.HttpBadRequest("Insufficient parameters passed"))
        except:
            logger.exception( "create_update: %s" % request )
        return self.create_response( request, {'error': "unable to create update for the grievance"}, response_class = http.HttpBadRequest)

    @apply_default_checks( allowed_methods = [ 'post' ] )
    def reparent_update(self, request, **kwargs):
        try:
            update = Grievance.objects.get( pk = kwargs[ 'update_id' ] )
            new_parent = Grievance.objects.get( pk = kwargs[ 'parent_id' ] )
            if update.is_update and not new_parent.is_update:
                old_parent = Grievance.objects.get( pk = update.parent_id() ) 
                old_parent.updates.remove( update )
                new_parent.updates.add( update )
                return self.create_response( request, { 'update_id': update.id, 'old_parent_id': old_parent.id, 'new_parent_id': new_parent.id } )
            else:
                return self.create_response( request, {'error': "Update can't be made to another update"}, response_class = http.HttpBadRequest )
        except:
            logger.exception( "reparent_comment: %s" % request )
        return self.create_response( request, { "error": "reparenting failed." } )

    def upload_audio(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            uploaded_file = request.FILES[ 'audio_file' ]
            ai_id = request.REQUEST[ 'ai_id' ]
            grievance_id = request.REQUEST[ 'grievance_id' ]
            recording_id = request.REQUEST.get( 'recording_id' )
            logger.debug( "upload_audio: user: %s, ai_id: %s, grievance_id: %s, rec_id: %s" % ( request.user.id, ai_id, grievance_id, recording_id ) )

            recording = recording_upload_impl( uploaded_file, ai_id, recording_id )
            grievance = Grievance.objects.get( pk = grievance_id )
            grievance.recording = recording
            grievance.save()
            recording_resource = RecordingResource()
            recording_bundle = recording_resource.build_bundle( obj = recording, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, recording_resource.full_dehydrate( recording_bundle ) )
        except:
            logger.exception( "upload_audio: %s" % request )
        return self.create_response( request, {'error': "unable to upload audio"} )

    def update_pub_order(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            grievance = Grievance.objects.get( pk = request.REQUEST[ 'grievance_id' ] )
            grievance.pub_order = int( request.REQUEST[ 'target_pub_order' ] )
            grievance.moderator = request.user
            grievance.save()  
            return self.create_response( request, { 'grievance_pub_order': grievance.pub_order } )
        except:
            logger.exception( "update_pub_order" )
        return self.create_response( request, { "error": "re-ordering failed." } )

    @apply_default_checks( authorization = Authorization(), allowed_methods = [ 'post' ] )
    def assign_handler(self, request, **kwargs):
        try:
            grievance = Grievance.objects.get( pk = kwargs[ 'grievance_id' ] )
            handler = Handler.objects.get( pk = kwargs[ 'handler_id' ] )
            if grievance.ai != handler.ai:
                return self.create_response( request, { "error": "grievance and handler should belong to same ai." } )
            grievance.handler = handler
            grievance.save()
            return self.create_response( request, { 'grievance_id': grievance.id, 'handler_id': handler.id } )
        except:
            logger.exception( "assign_handler: %s" % request )
        return self.create_response( request, { "error": "assigning handler." } )

    def initiate_call( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'post' ] )
        self.throttle_check( request )

        try:
            grievance_id = int( request.REQUEST[ 'grievance_id' ] )
        
            max_tries = int( request.REQUEST.get( 'max_tries', 1 ) )

            grievance = Grievance.objects.get( pk = grievance_id )
            properties = Properties.objects.get(ai = grievance.ai)
            if grievance.is_update:
                raise Exception( 'grievance type should not be update.' )
            if not grievance.handler:
                raise Exception( 'grievance does not have any handler assigned to it.' )
            if grievance.beneficiary == get_formatted_number( DEFAULT_CALLERID ):
                raise Exception( 'cannot initiate call as grievance is system generated.' )

            schedule = Transient_group_schedule.objects.get( pk = properties.feedback_ai_schedule )
            eta = datetime.now()
            vi_data = { 'grievance_id': grievance_id, 'beneficiary': grievance.beneficiary }
            ai = schedule.ai_group.ai
            gcl = Groups_call_log( number = grievance.handler.num, ai = ai, group_schedule = schedule )
            gcl.save()
            queue = get_queue_for_task(ai, default_queue='push')
            CallAndPlayTask.apply_async([gcl.id, max_tries, False,  vi_data], eta=eta, queue=queue)
            call_bundle = self.build_bundle( obj = schedule, request = request )
            self.log_throttled_access( request )
            return self.create_response( request, { 'scheduled_call_id': gcl.id }, response_class = http.HttpCreated )
        except KeyError, e:
            raise ImmediateHttpResponse( http.HttpBadRequest( "Insufficient parameters passed" ) )
        except Exception,e:
            exception_str = "exception while initiating call"
            logger.exception( exception_str )
        return self.create_response( request, { 'error': exception_str }, response_class = http.HttpApplicationError )

    def hydrate(self, bundle):
        if bundle.data.has_key( 'recording' ) and bundle.data['recording']:
            bundle.obj.recording_id = bundle.data['recording']['id']
            del bundle.data['recording']

        bundle.obj.moderator = bundle.request.user
        bundle.obj.moderation_source = Moderation_source.WEB
        return bundle

    def build_filters(self, filters = None):
        if filters is None:
            filters = {}

        orm_filters = super(GrievanceResource, self).build_filters( filters )
        if "parent" in filters:
            orm_filters[ "parent" ] = filters[ "parent" ]
        if "search_id" in filters:
            orm_filters[ "id__exact" ] = filters[ "search_id" ]
        if "search_title" in filters:
            orm_filters['q_filters'] = orm_filters.get('q_filters', []) + [ Q(title__icontains = title_kw) for title_kw in filters["search_title"].split(',') ]
        if "search_transcript" in filters:
            orm_filters['q_filters'] = orm_filters.get('q_filters', []) + [ Q(transcript__icontains = trans_kw) for trans_kw in filters["search_transcript"].split(',') ]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        q_filters = applicable_filters.pop('q_filters', None)

        semi_filtered = super(GrievanceResource, self).apply_filters( request, applicable_filters )

        if q_filters:
            semi_filtered = semi_filtered.filter( reduce(and_, q_filters) )

        return semi_filtered


class HelplineResource( Resource ):

    class Meta:
        resource_name = 'helpline'
        authentication = ApiKeyAuthentication()

    def prepend_urls( self ):
        return [
            url(r"^(?P<resource_name>%s)/call%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'initiate_call' ), name = "api_initiate_call" ),
        ]

    def initiate_call( self, request, **kwargs ):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            logger.info( "api request for initiate_call: {0}".format( request ) )
            handler = get_formatted_number( request.REQUEST[ 'handler' ] )
            beneficiary = get_formatted_number( request.REQUEST[ 'beneficiary' ] )
            
            if 'ai_id' in request.REQUEST:
                ai_id = int( request.REQUEST[ 'ai_id' ] )
                ai = App_instance.objects.get(pk = ai_id)
                schedule_id = Properties.objects.get(ai = ai).feedback_ai_schedule.id
            else:
                schedule_id = int(request.REQUEST['schedule_id'])
            schedule = Transient_group_schedule.objects.get( pk = schedule_id )
            
            if handler == beneficiary:
                raise Exception( 'numbers for handler and beneficiary should be different.' )

            max_tries = int( request.REQUEST.get( 'max_tries', 2 ) )
            eta = datetime.now()
            vi_data = { 'beneficiary': beneficiary }

            ai = schedule.ai_group.ai
            group_call_log = Groups_call_log( number = handler, ai = ai, group_schedule = schedule )
            group_call_log.save()
            queue = get_queue_for_task(ai, default_queue='push')
            CallAndPlayTask.apply_async([group_call_log.id, max_tries, vi_data], eta=eta, queue=queue)

            call_bundle = self.build_bundle( obj = schedule, request = request )
            self.log_throttled_access( request )
            return self.create_response( request, { 'scheduled_call_id': group_call_log.id }, response_class = http.HttpCreated )
        except KeyError, e:
            raise ImmediateHttpResponse( http.HttpBadRequest( "Insufficient parameters passed" ) )
        except Exception, e:
            exception_str = "exception while initiating call"
            logger.exception( exception_str )
        return self.create_response( request, { 'error': exception_str }, response_class = http.HttpApplicationError )



class HelplineCallResource( ModelResource ):
    media = fields.ForeignKey(RecordingResource, 'media', full = True, null = True)
    ai  = fields.ForeignKey(AppInstanceResource, 'ai')

    class Meta:
        queryset = Call.objects.all().order_by("-id")
        resource_name = "helpline_call_handler"
        filtering = {
            'media':  ALL_WITH_RELATIONS,
            'ai': ALL_WITH_RELATIONS
      
       
        }



