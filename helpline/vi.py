from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.utils import *
from vapp.helpline.models import Properties, Handler, Call, HelplineMnewsConnector, Caller_type, Grievance, \
Conversation_feedback_log, Conversation_type, BeneficiaryRetryPolicy
from vapp.mnews.models import News
from vapp.app_manager.models import get_report_creds, push_results
from vapp.app_manager.tasks import run_schedule
from customization.models import is_unsubscribed
import stat
from datetime import datetime
import time

from vapp.log import get_logger
logger = get_logger()


class HelplineController(BaseVappController):
    call = None
    handlers = None
    currentHandler = 0

    def __init__(self, ai, sessionData, vi_data):
        super(HelplineController, self).__init__(ai, sessionData, vi_data)
        logger.info('Inside init')
        self.sessionData = sessionData
        logger.info('Vi data')
        logger.info(str(vi_data))
        self.properties = Properties.objects.get(ai=self.ai)
        if vi_data.has_key('beneficiary'):
            customHandler = Handler()
            customHandler.num = vi_data['beneficiary']
            self.handlers = [customHandler]
        else:
            self.handlers = Handler.objects.filter(ai=self.ai).order_by('priority')

    def pre_outgoingstart(self):
        return self.getStartCallParams(self.sessionData.callerid)

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def pre_welcome(self):
        return self.getPromptParams('helpline_welcome')

    def pre_waitprompt(self):
        return self.getPromptParams('helpline_please_wait')

    def while_checkhandler__sm_action_success(self, events, eventData):
        if len(self.handlers) > self.currentHandler:
            return 'helpline_handler_available'
        else:
            return 'helpline_handler_unavailable'

    def while_nexthandler__sm_action_success(self, events, eventData):
        self.currentHandler += 1
        return 'sm_action_success'

    def pre_callhandler(self):
        callerid = self.handlers[self.currentHandler].num
        return self.getStartCallParams( callerid )

    def while_callhandler__sm_action_failure(self, events, eventData):
        uuid = eventData.get('Unique_ID')
        #TODO move this decision to a more explicit role based decision
        if uuid and uuid == self.callerUuid:
            return 'helpline_caller_hung_up'
        else:
            return 'helpline_handler_hung_up'

    def pre_recordcall(self):
        self.call = Call(ai = self.ai, handler = self.handlers[self.currentHandler].num, callerid = self.sessionData.callerid)
        return self.getRecordCallParams(self.call, Call.media)

    def pre_bridgecalls(self):
        return self.getBridgeCallsParams(self.sessionData.uuids[0], self.sessionData.uuids[1])

    def pre_answeringmachine(self):
        return self.getPromptParams('helpline_helpers_unavailable')

    def pre_recordmessage(self):
        self.call = Call(ai = self.ai, handler = Handler.RECORDER, callerid = self.sessionData.callerid)
        return self.getEmbeddedRecordingParams( self.ai.record_duration_limit_seconds, self.call, Call.media )

    def pre_thankyou(self):
        return self.getPromptParams('helpline_thank_you')

    def isCallSaved(self, call):
        saved_call = Call.objects.filter(pk = call.id)
        return saved_call.exists()
        
    def pushRecToMnewsAi(self):
        connectors = HelplineMnewsConnector.objects.filter(helpline_ai = self.ai)
        if connectors.exists():
            connector = connectors[0]
            recording = self.call.media.clone(connector.mnews_ai.id)
            news = News(ai=connector.mnews_ai,
                        callerid=self.sessionData.callerid,
                        state='UNM', is_comment=False,
                        source=News.Source.VOICE_INTERFACE,
                        title='Recording from helpline')
            news.detail = recording
            news.save()

    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.callerUuid = self.sessionData.uuids[0]
        return 'sm_action_success'

    def while_incomingstart__sm_action_success(self, events, eventData):
        self.callerUuid = self.sessionData.uuids[0]
        return 'sm_action_success'

    def while_checkschedule__sm_action_success(self, events, eventData):
        schedule = self.properties.schedule
        return 'helpline_schedule_off' if schedule and not schedule.is_schedule_on() else 'helpline_schedule_on'

    def pre_scheduleoff(self):
        return self.getPromptParams('helpline_schedule_off')

    def while_scheduleoff__sm_action_success(self, events, eventData):
        if self.properties.sched_off_answering_mc:
            return 'helpline_record_msg'
        else:
            return 'helpline_dont_record_msg'

    def post_stop(self, events):
        if not (self.call and self.call.media):
            return

        if not self.isCallSaved(self.call):
            return

        self.pushRecToMnewsAi()


HelplineStateDescriptionMap = [
    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url']
            }
        },
    {   'name':'incomingstart',
        'action':'answer',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success']
            }
        },
    {   'name':'welcome',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkschedule':['sm_action_success'],
            }
        },
    {   'name':'checkschedule',
        'action':'none',
        'transitions': {
            'waitprompt': ['helpline_schedule_on'],
            'scheduleoff': ['helpline_schedule_off'],
            },
        },
    {   'name':'scheduleoff',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure', 'helpline_dont_record_msg'],
            'recordmessage':['helpline_record_msg'],
            },
        },
    {   'name':'waitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkhandler':['sm_action_success'],
            },
        },
    {   'name':'checkhandler',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'callhandler':['helpline_handler_available'],
            'outofhandlers':['helpline_handler_unavailable'],
            }
        },
    {   'name':'callhandler',
        'action':'originate',
        'transitions': {
            'handleranswered':['sm_action_success'],
            'nexthandler':['helpline_handler_hung_up', 'sm_next_originate_url'],
            'stop':['helpline_caller_hung_up'],
            }
        },
    {   'name':'handleranswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
            },
        },
    {   'name':'recordcall',
        'action':'record_call',
        'transitions': {
            'stop':['sm_action_failure'],
            'bridgecalls':['sm_action_success'],
            },
        },
    {   'name':'bridgecalls',
        'action':'bridge',
        'transitions': {
            'stop':['sm_action_failure']
            }
        },
    {   'name':'nexthandler',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkhandler':['sm_action_success']
            },
        },
    {   'name':'outofhandlers',
        'action':'break_media',
        'transitions': {
            'answeringmachine':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'answeringmachine',
        'action':'playback',
        'transitions': {
            'recordmessage':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'recordmessage',
        'action':'record',
        'transitions': {
            'thankyou':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'thankyou',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_success', 'sm_action_failure'],
            },
        },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {}
        }
    ]


class HandlerBeneficiaryCallController( BaseVappController ):
    call = None
    newConversation = None

    @classmethod
    def saveConversation( cls, conversation ):
        conversation.save()

    def generateResult(self):
        result = {}
        cdr = {
            'id': str(self.beneficiaryRetryCdr.id),
            'startTime': str(self.beneficiaryRetryCdr.start_time),
            'errorCause': self.hangupCause
        }
        self.beneficiaryRetryCdrs.append(cdr)
        result[ 'cdrs' ] = self.beneficiaryRetryCdrs
        result[ 'totalTries' ] = len(self.beneficiaryRetryCdrs)
        result[ 'handler' ] = self.handler
        result[ 'beneficiary' ] = self.beneficiary
        result[ 'conversationId' ] = None
        result[ 'conversationStartTime' ] = None
        result[ 'conversationEndTime' ] = None
        result[ 'recordingUrl' ] = None
        result[ 'conversationSuccess' ] = False
        if self.call:
            result[ 'conversationId' ] = str(self.call.id) 
            result[ 'conversationStartTime' ] = str(self.call.media.time)
            result[ 'conversationEndTime' ] = str(datetime.now())
            result[ 'recordingUrl' ] = str(self.call.media.get_url())
            result[ 'conversationSuccess' ] = True
        result[ 'scheduledCallId' ] = str(self.gclId)
        if self.feedbackSubscribed:
            result[ 'feedbackSubscriptionStatus' ] = self.feedbackSubscribed
        return result

    def __init__(self, ai, sessionData, vi_data):
        super(HandlerBeneficiaryCallController, self).__init__(ai, sessionData, vi_data)
        self.sessionData = sessionData
        self.properties = Properties.objects.get(ai=self.ai)
        self.retryPolicy = get_or_none(BeneficiaryRetryPolicy, ai=self.ai)
        self.handler = self.sessionData.callerid
        self.beneficiary = vi_data['beneficiary']
        self.maxTriesForFeedbackCall = vi_data['max_tries']
        self.maxTries = 1
        self.retryDelay = None
        if self.retryPolicy:
            self.maxTries = self.retryPolicy.max_tries
            self.retryDelay = self.retryPolicy.delay
        self.cdr = self.sessionData.cdrs[ 0 ]
        self.feedbackSubscribed = None
        self.gclId = vi_data['group_call_log'].id
        self.tries = 0
        self.creds = get_report_creds(self.ai)
        self.call = None
        self.beneficiaryRetryCdrs = []

        if self.properties.feedback_ai_schedule:
            self.feedbackSubscribed = True
            if is_unsubscribed(self.properties.feedback_ai_schedule.ai_group.ai.id, self.beneficiary):
                self.feedbackSubscribed = False

        self.grievance = None
        if vi_data.get( 'grievance_id' ):
            self.grievance = Grievance.objects.get( pk = vi_data.get( 'grievance_id' ) )

        self.handlerCallUuid = None

    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid )

    def while_outgoingstart__sm_action_success( self, events, eventData ):
        self.handlerCallUuid = self.sessionData.uuids[ 0 ]
        return 'sm_action_success'

    def pre_incomingstart( self ):
        return self.getIncomingCallParams()

    def while_incomingstart__sm_action_success( self, events, eventData ):
        self.handlerCallUuid = self.sessionData.uuids[ 0 ]
        return 'sm_action_success'

    def pre_welcome( self ):
        self.handlerCallUuid = self.sessionData.uuids[ 0 ]
        return self.getPromptParams( 'helpline_welcome' )

    def pre_waitprompt( self ):
        return self.getPromptParams( 'helpline_please_wait' )

    def pre_beneficiaryoutgoingstart(self):
        self.beneficiaryCallParams = self.getStartCallParams(self.beneficiary)
        self.benCdrId = self.beneficiaryCallParams['cdr_id']
        return self.beneficiaryCallParams

    def generate_and_send_result(self, cred):
        self.beneficiaryRetryCdr = Cdr.objects.get(id=self.benCdrId)
        results = self.generateResult()
        push_results(cred, results, self.beneficiaryRetryCdr)

    def incrementAndCheckTries(self, events, eventData):
        self.hangupCause = eventData.get('Hangup_Cause', '')
        cred_qs = self.creds.filter(policy=Cdr.CdrEvents.RETRY)
        if cred_qs.exists():
            self.generate_and_send_result(cred_qs[0])
        uuid = eventData.get( 'Unique_ID' )
        if uuid and uuid == self.handlerCallUuid:
            return 'helpline_handler_hung_up'
        if self.tries <= self.maxTries:
            self.tries += 1
            return 'helpline_beneficiary_no_response'
        else:
            return 'max_tries_exceeded'

    def while_beneficiaryoutgoingstart__sm_next_originate_url(self, events, eventData):
        return self.incrementAndCheckTries(events, eventData)

    def while_beneficiaryoutgoingstart__sm_action_failure( self, events, eventData ):
        return self.incrementAndCheckTries(events, eventData)

    def pre_beneficiarynoresponse( self ):
        return self.getPromptParams('helpline_beneficiary_no_response')

    def while_benmaxretrycheck__sm_action_success(self, events, eventData):
        if self.tries < self.maxTries:
            if self.retryDelay:
                time.sleep(self.retryDelay)
            return 'tries_remaining'
        else:
            return 'max_tries_exceeded'

    def pre_recordcall( self ):
        if self.grievance:
            title = 'System initiated conversation, %s' % datetime.now().strftime( '%H:%M, %d %b\'%y' )
            self.newConversation = Grievance( ai = self.grievance.ai, handler = self.grievance.handler, beneficiary = self.grievance.beneficiary, title = title, conversation_type = Conversation_type.SYSTEM_INITIATED, is_update = True )
            self.newConversation.parent_id_value = self.grievance.id
            return self.getRecordCallParams( self.newConversation, Grievance.recording )
        else:
            self.call = Call(ai=self.ai, handler=self.handler, callerid=self.handler, beneficiary=self.beneficiary)
            return self.getRecordCallParams( self.call, Call.media )

    def pre_bridgecalls( self ):
        return self.getBridgeCallsParams( self.sessionData.uuids[ 0 ], self.sessionData.uuids[ 1 ] )

    def while_bridgecalls__sm_action_failure(self, events, eventData):
        self.hangupCause = eventData.get('Hangup_Cause', '')

    def pre_thankyou( self ):
        return self.getPromptParams( 'helpline_thank_you' )

    def isCallSaved( self, call ):
        saved_call = Call.objects.filter( pk = call.id )
        return saved_call.exists()

    def post_stop(self, events):
        try:
            if self.newConversation:
                HandlerBeneficiaryCallController.saveConversation( self.newConversation )
                self.grievance.updates.add( self.newConversation )
        except Exception, e:
            logger.exception( 'exception while updating grievance id %s - %s' % ( self.grievance.id, e ) )

        if not (self.call and self.call.media):
            return

        if not self.isCallSaved( self.call ):
            return

        cred_qs = self.creds.filter(policy=Cdr.CdrEvents.END)
        if cred_qs.exists():
            self.generate_and_send_result(cred_qs[0])

        if self.properties.feedback_ai_schedule:
            vi_data = { 'conversation_id': self.call.id }
            run_schedule(self.properties.feedback_ai_schedule, [self.beneficiary], datetime.now(), max_tries=self.maxTriesForFeedbackCall, vi_data=vi_data)


HandlerBeneficiaryCallStateDescriptionMap = [
    {   'name': 'outgoingstart',
        'action': 'originate',
        'transitions': {
            'stop': ['sm_action_failure'],
            'welcome': ['sm_action_success'],
            'outgoingstart': ['sm_next_originate_url'],
        },
    },
    {   'name': 'incomingstart',
        'action': 'answer',
        'transitions': {
            'stop': ['sm_action_failure' ],
            'welcome': ['sm_action_success'],
        },
    },
    {   'name': 'welcome',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'waitprompt': ['sm_action_success'],
        },
    },
    {   'name': 'waitprompt',
        'action': 'play_and_return',
        'transitions': {
            'stop': ['sm_action_failure'],
            'beneficiaryoutgoingstart': ['sm_action_success'],
        },
    },
    {   'name': 'beneficiaryoutgoingstart',
        'action': 'originate',
        'transitions': {
            'beneficiaryanswered': ['sm_action_success'],
            'stop': ['helpline_handler_hung_up', 'max_tries_exceeded'],
            'beneficiarynoresponse': ['helpline_beneficiary_no_response'],
            'benmaxretrycheck': ['sm_next_originate_url', 'sm_action_failure'],
        },
    },
    {   'name': 'beneficiaryanswered',
        'action': 'break_media',
        'transitions': {
            'recordcall': ['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name': 'recordcall',
        'action': 'record_call',
        'transitions': {
            'stop': ['sm_action_failure'],
            'bridgecalls': ['sm_action_success'],
        },
    },
    {   'name': 'bridgecalls',
        'action': 'bridge',
        'transitions': {
            'stop': ['sm_action_failure'],
        },
    },
    {   'name': 'beneficiarynoresponse',
        'action': 'playback',
        'transitions': {
            'benmaxretrycheck': ['sm_action_success'],
            'stop': ['sm_action_failure'],
        },
    },
    {   'name': 'benmaxretrycheck',
        'action': 'none',
        'transitions': {
            'beneficiaryoutgoingstart': ['tries_remaining'],
            'thankyou': ['max_tries_exceeded'],
        },
    },
    {   'name': 'thankyou',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure', 'sm_action_success'],
        },
    },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {},
    }
]
