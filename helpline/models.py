from django.db import models
from django.db.models.signals import post_save, post_delete, pre_delete, pre_save
from django.contrib.auth.models import User

import uuid, RQMC

from datetime import datetime

from vapp.app_manager.models import App_instance, CalleridField, VI_conf, Cdr
from vapp.media.models import Recording
from vapp.data.models import Eicher_delhi
from schedule.models import Schedule
from location.models import Location
from mnews.models import Transient_group_schedule, Groups_call_log
from sms.models import SMS_template, SMS_message
from sms.tasks import SMSTask

from vapp.utils import datetimefield_to_unix, Moderation_source

from events import Event

from log import get_request_logger

logger = get_request_logger()

CALLERID_LENGTH = 20
MAX_REFERENCE_NUMBER = 100000
MAX_REFERENCE_ID_GENERATION_TRIES = 10
GRIEVANCE_PUB_ORDER_MAX = 9999
GRV_FIELDS_CHANGED_LENGTH = 300
GRIEVANCE_EVENT_LENGTH = 100
ACTION_NAME_LENGTH = 100

class Caller_type():
    HANDLER     = 'HANDLER'
    BENEFICIARY = 'BENEFICIARY'

class Conversation_type( object ):
    HANDLER_TO_BENEFICIARY = 0
    BENEFICIARY_TO_HANDLER = 1
    SYSTEM_INITIATED       = 2
    NOT_APPLICABLE         = 3

class BeneficiaryRetryPolicy(models.Model):
    ai = models.ForeignKey(App_instance)
    max_tries = models.PositiveSmallIntegerField(default=1)
    delay = models.PositiveIntegerField(null=True, blank=True)

class Properties(models.Model):
    HANDLER_CALL_POLICIES = (
        ('pri', 'Priority'),
        ('lrc', 'Least Recently Called First'),
        ('par', 'Parallel'),
        )

    ai = models.ForeignKey(App_instance, related_name = "helpline_properties_set")
    handler_call_policy = models.CharField(max_length = 3, choices = HANDLER_CALL_POLICIES, default = 'pri')
    schedule = models.ForeignKey(Schedule, null = True, blank = True, verbose_name = "Beneficiary - Schedule")
    sched_off_answering_mc = models.BooleanField(default = False, verbose_name = "Beneficiary - Go to answering machine when call received outside schedule")
    handler_schedule = models.ForeignKey(Schedule, null = True, blank = True, verbose_name = "Handler - Schedule", related_name = 'handler_schedule')
    handler_sched_off_ans_mc = models.BooleanField(default = False, verbose_name = "Handler - Go to answering machine when call received outside schedule")
    prompt_handler_for_grievance_access = models.BooleanField(default = True)
    allow_grievance_autopub = models.BooleanField(default = False)
    allow_update_autopub = models.BooleanField(default = False)
    beneficiary_grv_update_sched = models.ForeignKey(Transient_group_schedule, null = True, blank = True)
    update_dial_out_max_tries = models.PositiveSmallIntegerField(default = 3)
    beneficiary_grv_update_sms = models.ForeignKey(SMS_template, null = True, blank = True, related_name = "beneficiary_grv_update_sms")
    beneficiary_grv_assigned_sms = models.ForeignKey(SMS_template, null = True, blank = True, related_name = "beneficiary_grv_assigned_sms")
    handler_grv_assigned_sms = models.ForeignKey(SMS_template, null = True, blank = True, related_name = "handler_grv_assigned_sms")
    notficiation_start_prompt = models.FileField(null = True, blank = True, upload_to = 'groups')
    notficiation_end_prompt = models.FileField(null = True, blank = True, upload_to = 'groups')
    grievance_preview_secs = models.PositiveSmallIntegerField(default = 10, help_text = "Set to 0 to play the full item.")
    feedback_ai_schedule = models.ForeignKey(Transient_group_schedule, null = True, blank = True, related_name = "feedback_ai_schedule")

    def __unicode__(self):
        return unicode(self.ai) + " Properties"

    def create_handler_grv_assigned_sms(self, grievance):
        if self.handler_grv_assigned_sms:
            params = {}
            params['reference_id'] = grievance.reference_id
            params['number'] = grievance.beneficiary
            return self.handler_grv_assigned_sms.message.format(**params)

    def create_beneficiary_grv_assigned_sms(self, grievance):
        if self.beneficiary_grv_assigned_sms:
            params = {}
            params['reference_id'] = grievance.reference_id
            params['number'] = grievance.handler.num
            params['name'] = grievance.handler.name
            return self.beneficiary_grv_assigned_sms.message.format(**params)

    def create_beneficiary_grv_update_sms(self, grievance):
        if self.beneficiary_grv_update_sms:
            params = {}
            params['reference_id'] = grievance.reference_id
            params['number'] = grievance.handler.num
            params['name'] = grievance.handler.name
            return self.beneficiary_grv_update_sms.message.format(**params)

    def get_start_prompt_filename(self):
        if self.notficiation_start_prompt:
            return self.notficiation_start_prompt.path
        return None

    def get_end_prompt_filename(self):
        if self.notficiation_end_prompt:
            return self.notficiation_end_prompt.path
        return None


class Call(models.Model):
    URGENCY_CHOICES = (
        ('lo', 'Low'),
        ('me', 'Medium'),
        ('hi', 'High'),
        )

    ai                = models.ForeignKey(App_instance)
    media             = models.ForeignKey(Recording)
    callerid          = CalleridField(max_length = CALLERID_LENGTH)
    location          = models.ForeignKey(Eicher_delhi, null = True)
    handler           = models.CharField(max_length = CALLERID_LENGTH)
    urgency           = models.CharField(max_length = 2, choices = URGENCY_CHOICES)
    address           = models.CharField(max_length = 80)
    comment           = models.CharField(max_length = 100)
    beneficiary       = CalleridField( max_length = CALLERID_LENGTH, null = True )

    def time(self):
        return datetimefield_to_unix(self.media.time)       

    def __unicode__(self):
        return str(self.ai.id) + "_" + unicode(self.callerid) + "_" + str(self.media.time) + "_" + unicode(self.handler) 

class Handler( models.Model ):
    RECORDER = "__RECORDER__"

    ai       = models.ForeignKey( App_instance )
    num      = CalleridField( max_length = CALLERID_LENGTH )
    name     = models.CharField( max_length = 32 )
    priority = models.IntegerField()
    location = models.ForeignKey( Location, null = True, blank = True )

    def __unicode__( self ):
        if self.location:
            return '{0}_{1}_{2}_{3}_{4}'.format( self.ai.id, self.num, self.name, self.priority, self.location.id )
        else:
            return '{0}_{1}_{2}_{3}'.format( self.ai.id, self.num, self.name, self.priority )

class HelplineMnewsConnector(models.Model):
    helpline_ai = models.ForeignKey(App_instance, related_name = 'connector_helpline_ai_set')
    mnews_ai = models.ForeignKey(App_instance, related_name = 'connector_mnews_ai_set')

    def __unicode__(self):
        return str(self.helpline_ai) + "-->" + str(self.mnews_ai)


class Grievance_status(object):
    RS = 'RS'
    PR = 'PR'
    FG = 'FG'
    CL = 'CL'


class Grievance_state(object):
    UNM = 'UNM'
    PUB = 'PUB'
    REJ = 'REJ'
    ARC = 'ARC'


class Grievance(models.Model):

    STATUS_CHOICES = (
        (Grievance_status.RS, 'Resolved'),
        (Grievance_status.PR, 'Processing'),
        (Grievance_status.FG, 'Flagged'),
        (Grievance_status.CL, 'Closed'),
    )

    STATE_CHOICES = (
        (Grievance_state.UNM, 'Unmoderated'),
        (Grievance_state.PUB, 'Published'),
        (Grievance_state.REJ, 'Rejected'),
        (Grievance_state.ARC, 'Archived'),
    )

    CALLER_TYPES = (
        (Caller_type.HANDLER, 'HANDLER'),
        (Caller_type.BENEFICIARY, 'BENEFICIARY'),
    )

    ai                = models.ForeignKey(App_instance)
    reference_id      = models.PositiveIntegerField()
    status            = models.CharField(max_length = 3, default = Grievance_status.PR, choices = STATUS_CHOICES)
    state             = models.CharField(max_length = 3, default = Grievance_state.UNM, choices = STATE_CHOICES)
    location          = models.ForeignKey(Location, null = True)
    beneficiary       = CalleridField(max_length = CALLERID_LENGTH)
    handler           = models.ForeignKey(Handler, null = True)
    time              = models.DateTimeField(auto_now_add = True)
    title             = models.CharField(max_length = 255)
    recording         = models.ForeignKey(Recording, null = True)
    transcript        = models.TextField()
    update_from       = models.CharField(max_length = 12, choices = CALLER_TYPES)
    conversation_type = models.PositiveSmallIntegerField( null = True )
    is_update         = models.BooleanField()
    updates           = models.ManyToManyField('Grievance', related_name = 'parent')
    pub_order         = models.SmallIntegerField(default = GRIEVANCE_PUB_ORDER_MAX)

    moderator         = None
    fields_changed    = None
    event_type        = None
    moderation_source = None

    def parent_id(self):
        if hasattr( self, 'parent_id_value' ):
            return self.parent_id_value
        return self.parent.latest('id').id if self.parent.exists() else None

    def get_changed_fields(self, grievance):
        attribs = [ 'state', 'title', 'location', 'transcript', 'pub_order', 'handler', 'status' ]
        if not grievance:
            return attribs

        changed_fields = []
        for attrib in attribs:
            if getattr(self, attrib) != getattr(grievance, attrib):
                changed_fields.append(attrib)
        return changed_fields

    def num_updates(self):
        return self.updates.filter(state__in = [Grievance_state.UNM, Grievance_state.PUB]).count()

    def num_unm_updates(self):
        return self.updates.filter(state = Grievance_state.UNM).count()


class Moderation_events:

    NEW_GRIEVANCE         = 'NEW_GRIEVANCE'
    NEW_UPDATE            = 'NEW_UPDATE'
    GRIEVANCE_PUBLISHED   = 'GRIEVANCE_PUBLISHED'
    UPDATE_PUBLISHED      = 'UPDATE_PUBLISHED'
    GRIEVANCE_ARCHIVED    = 'GRIEVANCE_ARCHIVED'
    UPDATE_ARCHIVED       = 'UPDATE_ARCHIVED'
    GRIEVANCE_UNPUBLISHED = 'GRIEVANCE_UNPUBLISHED'
    UPDATE_UNPUBLISHED    = 'UPDATE_UNPUBLISHED'
    GRIEVANCE_REJECTED    = 'GRIEVANCE_REJECTED'
    UPDATE_REJECTED       = 'UPDATE_REJECTED'
    GRIEVANCE_SAVED       = 'GRIEVANCE_SAVED'
    UPDATE_SAVED          = 'UPDATE_SAVED'
    HANDLER_CHANGED_PUBLISHED = 'HANDLER_CHANGED_PUBLISHED'


def is_published_grievances_reorder_needed(grievance):
    return (( not grievance.is_update ) and (( grievance.prev_state == Grievance_state.PUB and grievance.state != Grievance_state.PUB )
            or (grievance.prev_state != Grievance_state.PUB and grievance.state == Grievance_state.PUB)
            or (grievance.prev_state == Grievance_state.PUB and grievance.state == Grievance_state.PUB and grievance.prev_order != grievance.pub_order) ))

def get_grievances_to_be_reordered(ai, min_pub_order, max_pub_order, excluded_ids = []):
    return Grievance.objects.filter(ai = ai, state = Grievance_state.PUB, is_update = False, pub_order__gte = min_pub_order, pub_order__lte = max_pub_order).exclude(id__in = excluded_ids).order_by('pub_order')

def reorder_published_grievances(grievance, prev_order, prev_state):
    ai = grievance.ai
    pushed_up = ( grievance.pub_order > prev_order )
    max_order = grievance.pub_order if pushed_up else prev_order
    min_order = prev_order if pushed_up else grievance.pub_order
    grievances_to_be_reordered = get_grievances_to_be_reordered(ai, min_order, max_order, [grievance.id])
    pub_order_diff = ( -1 if len(grievances_to_be_reordered) >= 1 and pushed_up else 1 )
    post_save.disconnect(grievance_save_handler, sender = Grievance, dispatch_uid = 'helpline.grievance.save')
    logger.info('before reordering: [%s]'% str(grievances_to_be_reordered.values('id','pub_order')))
    for grievance in grievances_to_be_reordered:
        grievance.pub_order += pub_order_diff
        grievance.save()
    post_save.connect(grievance_save_handler, sender = Grievance, dispatch_uid = 'helpline.grievance.save')
    logger.info('after reordering: [%s]'% str(grievances_to_be_reordered.values('id','pub_order')))


def generate_unique_reference_num(ai):
    candidate_id = None
    tries = MAX_REFERENCE_ID_GENERATION_TRIES

    while(tries > 0):
        candidate_id = ( hash( str( uuid.uuid1() ) ) % MAX_REFERENCE_NUMBER )
        grievances = Grievance.objects.filter(ai = ai, reference_id = candidate_id)
        if not grievances.exists() and candidate_id > 0:
            return candidate_id
        tries -= 1


def grievance_pre_save_handler(sender, **kwargs):
    grievance = kwargs['instance']
    if not grievance:
        return
    if grievance.reference_id is None:
        reference_id = generate_unique_reference_num(grievance.ai)
        if reference_id:
            grievance.reference_id = reference_id
        else:
            logger.error('Unable to generate a unique reference id for the grievance: %s' % grievance.id)
            return

    prev_grievance = None
    grievance.prev_state = grievance.prev_order = None
    if grievance.pk:
        prev_grievance = Grievance.objects.get(pk = grievance.pk)
        grievance.prev_state = prev_grievance.state
        grievance.prev_order = prev_grievance.pub_order

    grievance.fields_changed = grievance.get_changed_fields(prev_grievance)
    grievance.event_type = Moderation_log.get_moderation_event(grievance, prev_grievance, grievance.fields_changed)

    if grievance.state == Grievance_state.PUB and grievance.pub_order < 0:
        grievance.pub_order = 0
    elif grievance.state in [Grievance_state.UNM, Grievance_state.ARC, Grievance_state.REJ]:
        grievance.pub_order = GRIEVANCE_PUB_ORDER_MAX


def grievance_save_handler(sender, **kwargs):
    grievance = kwargs['instance']

    if grievance.moderator:
        Moderation_log.record_event( grievance.moderator, grievance, grievance.event_type, grievance.fields_changed, grievance.moderation_source )

    if 'handler' in grievance.fields_changed:
        Moderation_log.send_event_message(grievance, grievance.event_type)

    event = Event.ADD if kwargs['created'] else Event.UPDATE

    if is_published_grievances_reorder_needed(grievance):
        reorder_published_grievances(grievance, grievance.prev_order, grievance.prev_state)

    queue = App_instance.message_queue_from_id(grievance.ai_id)
    RQMC.push_instance(queue, grievance, event, extras=('num_updates', 'num_unm_updates', 'parent_id'), relations=('recording',))
    if grievance.is_update:
        RQMC.push_instance(queue, Grievance.objects.get(pk = grievance.parent_id()), Event.UPDATE, extras=('num_unm_updates', 'parent_id'), relations=('recording',))

def grievance_pre_delete_handler(sender, **kwargs):
    grievance = kwargs['instance']
    grievance.parent_id_value = grievance.parent_id()

def grievance_delete_handler(sender, **kwargs):
    grievance = kwargs['instance']
    if grievance.recording:
        grievance.recording.delete()
    for update in grievance.updates.all():
        update.delete()

    queue = App_instance.message_queue_from_id( grievance.ai_id )
    RQMC.push_instance(queue, grievance, Event.DELETE, extras=('num_updates', 'num_unm_updates', 'parent_id'))
    if grievance.is_update:
        RQMC.push_instance(queue, Grievance.objects.get(pk = grievance.parent_id()), Event.UPDATE, extras=('num_unm_updates', 'parent_id'), relations=('recording',))

post_save.connect(grievance_save_handler, sender = Grievance, dispatch_uid = 'helpline.grievance.save')
pre_save.connect(grievance_pre_save_handler, sender = Grievance, dispatch_uid = 'helpline.grievance.pre_save')
post_delete.connect(grievance_delete_handler, sender = Grievance, dispatch_uid = 'helpline.grievance.delete')
pre_delete.connect(grievance_pre_delete_handler, sender = Grievance, dispatch_uid = 'helpline.grievance.pre_delete')

def call_beneficiary_on_update_pub(properties, grievance, update):
    vi = VI_conf.objects.get(controller = 'helpline.publishvi.OnUpdatePubCallBeneficiaryController', description = 'helpline.publishvi.OnUpdatePubCallBeneficiaryStateMap')
    vi_data = {
                'grievance_id': grievance.id,
                'property_id': properties.id,
                'max_tries': properties.update_dial_out_max_tries,
                'update_id': update.id,
                'vi_conf_id': vi.id
              }

    if properties.beneficiary_grv_update_sched:
        gcl = properties.beneficiary_grv_update_sched.callout_to_number(grievance.beneficiary, vi_data = vi_data)


def get_active_grievances_for_user(callerid, ai):
    return Grievance.objects.filter(ai = ai, beneficiary = callerid, state = Grievance_state.PUB, handler__isnull = False, is_update = False).exclude(status = Grievance_status.RS)


class Case_manager_action(object):
    UPDATE           = 'UPDATE'
    CALL_HANDLER     = 'CALL_HANDLER'
    CALL_BENEFICIARY = 'CALL_BENEFICIARY'
    REPLAY           = 'REPLAY'
    FLAG             = 'FLAG'
    INPUT            = 'INPUT'
    ASSIGN_HANDLER   = 'ASSIGN_HANDLER'
    RECORD_GRIEVANCE = 'RECORD_GRIEVANCE'

class Handler_action( models.Model ):
    name        = models.CharField( max_length = ACTION_NAME_LENGTH )
    description = models.TextField( null = True, blank = True )

    def __unicode__( self ):
        return self.name

class Handler_moderation_action( models.Model ):
    name        = models.CharField( max_length = ACTION_NAME_LENGTH )
    description = models.TextField( null = True, blank = True )

    def __unicode__( self ):
        return self.name

class Handler_moderation_action_setting( models.Model ):
    ai          = models.ForeignKey( App_instance )
    action      = models.ForeignKey( Handler_moderation_action )
    numeric_key = models.PositiveSmallIntegerField( help_text = 'Number between 1-9 can be provided.' )

    def __unicode__( self ):
        return '{0}-{1}-{2}'.format( self.ai, self.numeric_key, self.action )

class Handler_action_setting(models.Model):

    ACTION_CHOICES = (
        (Case_manager_action.UPDATE, 'UPDATE'),
        (Case_manager_action.CALL_BENEFICIARY, 'CALL_BENEFICIARY'),
        (Case_manager_action.REPLAY, 'REPLAY'),
        (Case_manager_action.FLAG, 'FLAG'),
        (Case_manager_action.INPUT, 'INPUT'),
        (Case_manager_action.ASSIGN_HANDLER, 'ASSIGN_HANDLER'),
    )

    ai          = models.ForeignKey(App_instance)
    action      = models.CharField(max_length = 64, choices = ACTION_CHOICES)
    numeric_key = models.PositiveSmallIntegerField(help_text = 'Number between 1-9 can be provided.')

    def __unicode__(self):
        return '%s - %s - %s' % (self.ai, self.numeric_key, self.action)

    @classmethod
    def get_action_settings_map(cls, ai):
        action_settings = {}
        for each in Handler_action_setting.objects.filter(ai = ai):
            action_settings[ each.numeric_key ] = each.action

        return action_settings


class Beneficiary_action_setting(models.Model):

    ACTION_CHOICES = (
        (Case_manager_action.CALL_HANDLER, 'CALL_HANDLER'),
        (Case_manager_action.RECORD_GRIEVANCE, 'RECORD_GRIEVANCE'),
    )

    ai          = models.ForeignKey(App_instance)
    action      = models.CharField(max_length = 64, choices = ACTION_CHOICES)
    numeric_key = models.PositiveSmallIntegerField(help_text = 'Number between 1-9 can be provided.')

    def __unicode__(self):
        return '%s - %s - %s' % (self.ai, self.numeric_key, self.action)

    @classmethod
    def get_action_settings_map(cls, ai):
        action_settings = {}
        for each in Beneficiary_action_setting.objects.filter(ai = ai):
            action_settings[ each.numeric_key ] = each.action

        return action_settings


class Case_manager_sms_log(models.Model):

    SMS_RECIPIENT_CHOICES = (
        (Caller_type.HANDLER, 'HANDLER'),
        (Caller_type.BENEFICIARY, 'BENEFICIARY'),
    )

    grievance      = models.ForeignKey(Grievance)
    sms_message    = models.ForeignKey(SMS_message)
    recipient_type = models.CharField(max_length = 50, choices = SMS_RECIPIENT_CHOICES)


class Moderation_log(models.Model):

    MODERATION_SOURCES = (
        (Moderation_source.IVR, 'IVR'),
        (Moderation_source.WEB, 'WEB'),
    )

    user = models.ForeignKey(User, null = True)
    grievance = models.ForeignKey(Grievance)
    event_type = models.CharField(max_length = GRIEVANCE_EVENT_LENGTH, null = False)
    timestamp = models.DateTimeField(default = datetime.now)
    fields_changed = models.CharField(max_length = GRV_FIELDS_CHANGED_LENGTH, null = True)
    source = models.CharField(max_length = 5, null = True, choices = MODERATION_SOURCES)
    handler = models.ForeignKey(Handler, null = True)

    @classmethod
    def get_messages_for_event(cls, grievance, event):
        handler_msg = None
        beneficiary_msg = None
        try:
            if event in [ Moderation_events.GRIEVANCE_PUBLISHED, Moderation_events.HANDLER_CHANGED_PUBLISHED ]:
                properties = Properties.objects.get(ai = grievance.ai)
                handler_msg = properties.create_handler_grv_assigned_sms(grievance)
                beneficiary_msg = properties.create_beneficiary_grv_assigned_sms(grievance)
        except Exception,e:
            logger.exception("get_message_for_event: Got exception getting message for ai:" + str(grievance.ai) + " event:" + str(event) + ". Exception:" + str(e))

        return { 'handler_msg': handler_msg, 'beneficiary_msg': beneficiary_msg }

    @classmethod
    def send_event_message(cls, grievance, event, max_tries = 3):
        logger.debug('send_event_message, events we got: %s' %(event))
        messages = cls.get_messages_for_event(grievance, event)
        if messages.get('handler_msg'):
            sms = SMSTask.create_send_msg(grievance.ai.id, messages.get('handler_msg'), grievance.handler.num, max_tries)
            Case_manager_sms_log.objects.create(grievance = grievance, sms_message = sms, recipient_type = Caller_type.HANDLER)
        if messages.get('beneficiary_msg'):
            sms = SMSTask.create_send_msg(grievance.ai.id, messages.get('beneficiary_msg'), grievance.beneficiary, max_tries)
            Case_manager_sms_log.objects.create(grievance = grievance, sms_message = sms, recipient_type = Caller_type.BENEFICIARY)

    @classmethod
    def record_event( cls, moderator, grievance, event_type, fields_changed, source ):
        event = Moderation_log( grievance = grievance, event_type = event_type, fields_changed = ", ".join( fields_changed ), source = source )
        if isinstance( moderator, User ):
            event.user = moderator
        elif isinstance( moderator, Handler ):
            event.handler = moderator
        event.save()

    @classmethod
    def get_moderation_event(cls, grievance, prev_grievance, changed_fields):
        logger.debug('changed_fields: %s, grievance.is_update: %s, grievance.state: %s' % (changed_fields, grievance.is_update, grievance.state ))
        if not prev_grievance:
            return Moderation_events.NEW_UPDATE if grievance.is_update else Moderation_events.NEW_GRIEVANCE
        elif 'state' in changed_fields:
            if grievance.state == Grievance_state.PUB:
                return Moderation_events.UPDATE_PUBLISHED if grievance.is_update else Moderation_events.GRIEVANCE_PUBLISHED
            elif grievance.state == Grievance_state.ARC:
                return Moderation_events.UPDATE_ARCHIVED if grievance.is_update else Moderation_events.GRIEVANCE_ARCHIVED
            elif grievance.state == Grievance_state.UNM:
                return Moderation_events.UPDATE_UNPUBLISHED if grievance.is_update else Moderation_events.GRIEVANCE_UNPUBLISHED
            elif grievance.state == Grievance_state.REJ:
                return Moderation_events.UPDATE_REJECTED if grievance.is_update else Moderation_events.GRIEVANCE_REJECTED
        elif 'handler' in changed_fields and ( not grievance.is_update ) and grievance.state == Grievance_state.PUB:
            return Moderation_events.HANDLER_CHANGED_PUBLISHED
        else:
            return Moderation_events.UPDATE_SAVED if grievance.is_update else Moderation_events.GRIEVANCE_SAVED


class Grievance_log(models.Model):

    STATUS_CHOICES = (
        (Grievance_status.RS, 'Resolved'),
        (Grievance_status.PR, 'Processing'),
        (Grievance_status.FG, 'Flagged'),
        (Grievance_status.CL, 'Closed'),
    )

    grievance   = models.ForeignKey(Grievance)
    prev_status = models.CharField(max_length = 2, null = True, choices = STATUS_CHOICES)
    curr_status = models.CharField(max_length = 2, null = True, choices = STATUS_CHOICES)
    update_time = models.DateTimeField(default = datetime.now)

class Conversation_feedback_log( models.Model ):
    conversation = models.ForeignKey( Call )
    feedback_cdr = models.ForeignKey( Cdr )
