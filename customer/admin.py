from django.contrib import admin

from models import Project, Organization, Project_stats_view


class ProjectAdmin(admin.ModelAdmin):
    def ai_count(self, proj):
        return proj.ais.count()

    def ad_count(self, proj):
        return proj.ads.count()

    def campaign_count(self, proj):
        return proj.campaigns.count()

    def bd_name(self, proj):
        if proj.bd_contact.first_name or proj.bd_contact.last_name:
            return "{0} {1}".format(proj.bd_contact.first_name, proj.bd_contact.last_name)
        else:
            return proj.bd_contact

    filter_horizontal = ('ais', 'campaigns', 'ads', 'stats_emails')
    search_fields = ['name', 'org__name']
    list_display = ('name', 'org', 'start_date', 'end_date', 'bd_name', 'ai_count', 'campaign_count', 'ad_count')
admin.site.register(Project, ProjectAdmin)

admin.site.register(Organization)

class ProjectStatsViewAdmin(admin.ModelAdmin):
    filter_horizontal = ('projects', 'users')
admin.site.register(Project_stats_view, ProjectStatsViewAdmin)

