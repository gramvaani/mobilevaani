from django.db import models
from django.contrib.auth.models import User
from app_manager.models import App_instance

from campaign.models import Campaign
from advert.models import Advertisement

from datetime import datetime

class Organization(models.Model):
    name = models.CharField(max_length = 100 )
    address = models.TextField( null = True, blank = True )

    def __unicode__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length = 100)
    org = models.ForeignKey(Organization)
    start_date = models.DateField()
    end_date = models.DateField()
    contract = models.FileField( upload_to = 'contracts', null = True, blank = True )
    project_value = models.DecimalField( max_digits = 10, decimal_places = 2, null = True, blank = True )
    contact_persons = models.ManyToManyField( User, null = True, blank = True, related_name = 'contact_persons' )
    bd_contact = models.ForeignKey( User, related_name = 'bd_contact' )
    ais = models.ManyToManyField( App_instance, null = True, blank = True, related_name = 'project_ais' )
    campaigns = models.ManyToManyField( Campaign, null = True, blank = True, related_name = 'project_campaigns' )
    ads = models.ManyToManyField( Advertisement, null = True, blank = True, related_name = 'project_ads' )
    stats_emails = models.ManyToManyField( User, null = True, blank = True, related_name = 'project_stats_emails' )

    def is_active(self):
        today = datetime.now().date()
        return (self.start_date <= today) and (self.end_date > today)

    def __unicode__(self):
        return self.name


def get_active_projects(project_ids = []):
    projects = Project.objects.exclude(stats_emails__isnull = True)
    if project_ids:
        projects = projects.filter(id__in = project_ids)

    return projects


class Project_stats_view(models.Model):
    name = models.CharField(max_length = 64)
    description = models.TextField()
    projects = models.ManyToManyField(Project)
    users = models.ManyToManyField(User, null = True, blank = True)
    view_uri = models.CharField(max_length = 256)
    for_all_users = models.BooleanField(default = False)

    def __unicode__(self):
        return '%s' % (self.name)
