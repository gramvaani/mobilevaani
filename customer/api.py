from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import ReadOnlyAuthorization

from customer.models import Project, Organization, Project_stats_view
from app_manager.api import UserResource, AppInstanceResource
from campaign.api import CampaignResource
from django.db.models import Q

from vapp.log import get_request_logger
logger = get_request_logger()


class ProjectViewAuthorization(ReadOnlyAuthorization):
    def __init__(self):
        super(ProjectViewAuthorization, self).__init__()

    def read_list(self, object_list, bundle):
        try:
            user = bundle.request.user
            views = Project_stats_view.objects.filter(Q(users=user) | Q(for_all_users=True)).order_by('id')
            return list(views)
        except:
            logger.exception("Unable to read_list for bundle: %s" % bundle)
        return []

    def read_detail(self, object_list, bundle):
        return False


class OrganizationResource(ModelResource):
    class Meta:
        queryset = Organization.objects.all()
        resource_name = "customer_organization"


class ProjectResource(ModelResource):
    organizations = fields.ForeignKey(OrganizationResource, 'org', full=True)
    contact_persons = fields.ToManyField(UserResource, 'contact_persons',
                                         null=True, full=True)
    bd_contact = fields.ForeignKey(UserResource, 'bd_contact', full=True)
    ais = fields.ToManyField(AppInstanceResource, 'ais', null=True, full=True)
    campaigns = fields.ToManyField(CampaignResource, 'campaigns', null=True,
                                   full=True)
    stats_emails = fields.ToManyField(UserResource, 'stats_emails', null=True,
                                      full=True)

    class Meta:
        queryset = Project.objects.all()
        resource_name = "customer_project"
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class ProjectStatsViewResource(ModelResource):
    projects = fields.ToManyField(ProjectResource, 'projects', null=True)

    class Meta:
        queryset = Project_stats_view.objects.all().order_by('id')
        resource_name = "project_stats_view"
        authentication = ApiKeyAuthentication()
        authorization = ProjectViewAuthorization()
