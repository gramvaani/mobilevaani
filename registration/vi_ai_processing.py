from telephony.statemachine import BaseVappController
from telephony.utils import *
from registration.models import *
from vapp.utils import get_ai_state_transition_string
from app_manager.common import load_module

from log import get_logger
logger = get_logger()


class RegistrationController( BaseVappController ):


    def __init__(self, ai, sessionData, vi_data):
        super(RegistrationController, self).__init__(ai, sessionData, vi_data = vi_data)
        self.registration = Registration.objects.get(ai = self.ai)
        self.registering_ai = vi_data['registering_ai'] if self.vi_data.has_key('registering_ai') else None
        self.reg_success = True
        self.cdr = self.sessionData.cdrs[0]
        self.callerid = self.cdr.callerid
        self.pm_map = Ai_processing_method.objects.filter( ai = self.ai, parent_ai = self.registration.parent_ai )
        self.registration_logs = Registration_log.objects.filter( registration = self.registration, registration_num = self.callerid )

    def is_status_pending( self ):
        return ( self.registration_logs and self.registration_logs.filter( reg_success = True, reg_verified = False ).exists() )

    def pre_welcome(self):
        return self.getPromptParams('registration_welcome')

    def while_welcome__sm_action_failure(self, events, eventsData):
        logger.info('Inside while welcome success')
        if not self.registration.skip_registered_check:
            if self.registration.is_caller_registered( self.callerid ):
                return 'reg_already_registered'
            elif not self.registration.is_auto and self.is_status_pending():
                return 'reg_pending'
            else:
                pass

        self.rlog = Registration_log(registration = self.registration, registration_num = self.callerid, \
                                    cdr = self.cdr, reg_success = self.registration.is_auto )
        self.rlog.save()

        if self.registration.is_auto:
            return 'reg_auto_register'
        else:
            return 'reg_manual_register'


    def while_welcome__sm_action_success(self, events, eventsData):
        logger.info('Inside while welcome success')
        if not self.registration.skip_registered_check:
            if self.registration.is_caller_registered( self.callerid ):
                return 'reg_already_registered'
            elif not self.registration.is_auto and self.is_status_pending():
                return 'reg_pending'
            else:
                pass

        self.rlog = Registration_log(registration = self.registration, registration_num = self.callerid, \
                                    cdr = self.cdr, reg_success = self.registration.is_auto )
        self.rlog.save()

        if self.registration.is_auto:
            return 'reg_auto_register'
        else:
            return 'reg_manual_register'

    def pre_alreadyregistered(self):
        return self.getPromptParams('registration_already_registered')

    def pre_regpending(self):
        return self.getPromptParams('registration_pending')

    def pre_autoregister(self):
        return self.getPromptParams('registration_auto_register')

    def pre_manualregister(self):
        return self.getPromptPlayAndGetParams(promptName='manual_register', \
                    maxDigits=1, tries=1, errorPromptName='registration_invalid_choice')

    def while_manualregister__sm_action_failure(self, events, eventsData):
        digits = self.getPlayAndGetDigits(eventsData)
        if digits and int(digits) == 1:
            return 'reg_manual_add'
        else:
            return 'reg_manual_fail'


    def while_manualregister__sm_action_success(self, events, eventsData):
        digits = self.getPlayAndGetDigits(eventsData)
        if digits and int(digits) == 1:
            return 'reg_manual_add'
        else:
            return 'reg_manual_fail'

    def pre_regfailed(self):
        return self.getPromptParams('registration_failed')

    def pre_afterregister(self):
        return self.getPromptParams('registration_jump_prompt')

    def while_afterregister__sm_action_success(self, events, eventsData):
        if self.registration.parent_ai:
            self.sessionData.pushEmbedStack(get_ai_state_transition_string(self.ai, 'processdata'))
            return '@ai_' + str(self.registration.parent_ai.id) + '_welcome'
        else:
            self.rlog.reg_success = True
            self.rlog.save()
            if self.registration.is_auto:
                self.rlog.reg_verified = True
                self.rlog.save()
            return 'reg_auto_success'

    def while_afterregister__sm_action_failure(self, events, eventsData):
        if self.registration.parent_ai:
            self.sessionData.pushEmbedStack(get_ai_state_transition_string(self.ai, 'processdata'))
            return '@ai_' + str(self.registration.parent_ai.id) + '_welcome'
        else:
            self.rlog.reg_success = True
            self.rlog.save()
            if self.registration.is_auto:
                self.rlog.reg_verified = True
                self.rlog.save()
            return 'reg_auto_success'
    def pre_incomingstart(self):
        return self.getIncomingCallParams() 

    def pre_regmanualsuccess(self):
        return self.getPromptParams('registration_manual_success')

    def pre_regautosuccess(self):
        return self.getPromptParams('registration_auto_success')

    def while_regautosuccess__sm_action_success(self, events, eventsData):
        self.registration.contact_list.add( self.callerid )

    def while_regautosuccess__sm_action_failure(self, events, eventsData):
        self.registration.contact_list.add( self.callerid )


    def while_processdata__sm_action_success(self, events, eventsData):
        reg_module = load_module( self.registration.parent_ai, 'processing' )
        success = False
        for processing_map in self.pm_map:
            signature = processing_map.processing_method.signature
            if hasattr(reg_module, signature ):
                try:
                    processing_method = getattr(reg_module, signature )
                    success = processing_method(self.registration.parent_ai, self.cdr)
                    pmlog = Ai_processing_method_log(ai_method=processing_map, cdr=self.cdr, retval=success)
                    pmlog.save()
                except Exception,e:
                    logger.exception("exception while calling processing method: %s" %(signature))
            else:
                logger.debug("processing method %s not found for map: %s." %( signature, processing_map ))
        self.rlog.reg_success = success
        self.rlog.save()

        if not success:
            return 'reg_failed'

        if self.registration.is_auto:
            self.rlog.reg_verified = True
            self.rlog.save()
            return 'reg_auto_success'
        else:
            return 'reg_manual_success'

    def while_jumpviref__sm_action_success(self, events, eventsData):
        if self.registration.vi_ref:
            return '@' + self.registration.vi_ref.reference

    def while_appexit__sm_action_success(self, events, eventsData):
        transition_string = self.sessionData.popEmbedStack()
        if transition_string:
            return transition_string
        return '@nextai'


RegistrationStateDescriptionMap = [

    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            }
        },

    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success']
            }
        },

    {   'name':'welcome', 
        'action':'playback', 
        'transitions': { 
            'appexit':['sm_action_failure'], 
            'regpending':['reg_pending'],
            'autoregister': ['reg_auto_register'],
            'manualregister': ['reg_manual_register'], 
            'alreadyregistered':['reg_already_registered'],
            'appexit': ['sm_action_success'],
        }
    },

    {   'name': 'regpending',
        'action': 'playback',
        'transitions' : {
            #'stop': ['sm_action_failure'],
            'appexit': ['sm_action_success', 'sm_action_failure'],
            },
    },
    {   'name': 'alreadyregistered',
        'action': 'playback',
        'transitions' : {
            #'stop': ['sm_action_failure'],
            'jumpviref': ['sm_action_success', 'sm_action_failure'],
            },
    },

    {  'name':'autoregister',
       'action':'playback',
       'transitions' :{

        #'stop': ['sm_action_failure'],
        'afterregister': ['sm_action_success', 'sm_action_failure'],

        },
    },

    {  'name':'manualregister',
       'action':'play_and_get_digits',
       'transitions' :{
        'appexit': ['sm_action_failure'],
        'afterregister': ['reg_manual_add'],
        'regfailed': ['reg_manual_fail', 'sm_get_digits_no_digits'],
        'appexit': ['sm_action_success']
        },
    },

    {  'name':'afterregister',
       'action':'playback',
       'transitions' :{
        #'stop': [ 'sm_action_failure' ],
        'processdata': ['sm_action_success', 'sm_action_failure'],
        'regautosuccess': ['reg_auto_success'],
        },
    },
    {  'name':'processdata',
       'action':'none',
       'transitions' :{
        'stop': ['sm_action_failure'], 
        'regautosuccess': [ 'reg_auto_success' ],
        'regmanualsuccess': [ 'reg_manual_success' ],
        'regfailed': [ 'reg_failed' ],
        'appexit': ['sm_action_success']
     },
    },

    {  'name':'regautosuccess',
       'action':'playback',
       'transitions' :{
        #'stop': ['sm_action_failure'], 
        'jumpviref': ['sm_action_success', 'sm_action_failure'],
        },
    },

    {  'name':'regmanualsuccess',
       'action':'playback',
       'transitions' :{
        #'stop': ['sm_action_failure'], 
        'appexit': ['sm_action_success', 'sm_action_failure'],
        },
    },

    {  'name':'jumpviref',
       'action':'none',
       'transitions' :{
        'stop': [ 'sm_action_failure' ],
        'appexit': ['sm_action_success'],
        },
    },

    {  'name':'regfailed',
       'action':'playback',
       'transitions' :{
        #'stop': [ 'sm_action_failure' ],
        'appexit': ['sm_action_success', 'sm_action_failure'],
        },
    },

    {  'name':'appexit',
       'action':'none',
       'transitions' :{
        'stop': ['sm_action_failure', 'sm_action_success'],
        },
    },

    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
    },
    ]
