from django.template.loader import render_to_string

from registration.models import Registration_log

from vapp.stats.models import Stats
from vapp.stats.stats import get_ai_call_stats

from vapp.utils import LINE_BREAK

def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    data = []
    reports = []

    for ai in ais:
        stats = Stats()
        stats.ai_name = ai.name
        call_stats = get_ai_call_stats(ai, start_datetime, end_datetime)
        stats.calls = call_stats.calls
        stats.callers_count = call_stats.callers
        stats.avg_duration = call_stats.avg_call_dur
        total_regs = Registration_log.objects.filter( registration__ai = ai, reg_time__range = (start_datetime, end_datetime) )
        stats.total_regs = total_regs.count()
        stats.reg_success = total_regs.filter( reg_success = True ).count()
        stats.reg_verified = total_regs.filter( reg_verified = True ).count()
        data.append(stats)
    return (data, reports)


def create_html_content(stats):
    return render_to_string('registration/stats.html', { 'data': stats }) + LINE_BREAK
