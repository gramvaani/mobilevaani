from django.db import models
from vapp.app_manager.models import App_instance, VI_reference, CalleridField, Cdr
from callerinfo.models import Contact_list
from datetime import datetime
from log import get_request_logger
logger = get_request_logger()


class  Registration(models.Model):

    ai = models.ForeignKey(App_instance, related_name = 'reg_ai')
    parent_ai = models.ForeignKey(App_instance, related_name='parent_ai', null=True, blank=True)
    vi_ref = models.ForeignKey(VI_reference, null = True, blank = True)
    contact_list = models.ForeignKey(Contact_list)
    is_auto = models.BooleanField(default = False)
    active = models.BooleanField(default = True)
    skip_registered_check = models.BooleanField(default = False)

    def  __unicode__(self):
        return unicode(self.ai) + "_" + unicode(self.contact_list)

    def is_caller_registered(self, callerid):
        return ( callerid in self.contact_list.get_numbers() )

class  Registration_log(models.Model):

    registration = models.ForeignKey(Registration)
    registration_num = CalleridField()
    cdr = models.ForeignKey(Cdr)
    reg_success = models.BooleanField(default = False)
    reg_verified = models.BooleanField(default = False)
    reg_time = models.DateTimeField(default = datetime.now)

    @classmethod
    def get_reg_log(cls, registration, callerid, success = False):
        (rlog, created) = (None, False)
        try:
            rlog, created = Registration_log.objects.get_or_create(registration = registration, registration_num = callerid, reg_success = success)
        except Exception, e:
            pass
        return (rlog, created)
    
    @classmethod
    def is_embedding_needed(cls, parent_ai, cdr, **kwargs):
        return ( not Registration_log.objects.filter(registration__parent_ai = parent_ai, registration_num = cdr.callerid).exists() )

class Processing_method(models.Model):
    name = models.CharField( max_length = 50 )
    description = models.TextField( null = True, blank = True )
    signature = models.CharField( max_length = 50 )

    def __unicode__(self):
        return self.name


class Ai_processing_method(models.Model):
    ai = models.ForeignKey( App_instance, related_name = 'apm_reg_ai' )
    parent_ai = models.ForeignKey( App_instance, related_name = 'apm_parent_ai' )
    processing_method = models.ForeignKey( Processing_method )

    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.processing_method)

class Ai_processing_method_log(models.Model):
    ai_method = models.ForeignKey( Ai_processing_method )
    cdr  =  models.ForeignKey(Cdr)
    retval = models.BooleanField()
    processing_time = models.DateTimeField( default = datetime.now )

