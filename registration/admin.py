from django.contrib import admin

from models import *

admin.site.register(Registration)
admin.site.register(Processing_method)
admin.site.register(Ai_processing_method)