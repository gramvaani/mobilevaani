# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Processing_method'
        db.create_table(u'registration_processing_method', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('signature', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'registration', ['Processing_method'])

        # Adding model 'Ai_processing_method'
        db.create_table(u'registration_ai_processing_method', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='apm_reg_ai', to=orm['app_manager.App_instance'])),
            ('parent_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='apm_parent_ai', to=orm['app_manager.App_instance'])),
            ('processing_method', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['registration.Processing_method'])),
        ))
        db.send_create_signal(u'registration', ['Ai_processing_method'])

        # Adding model 'Ai_processing_method_log'
        db.create_table(u'registration_ai_processing_method_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai_method', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['registration.Ai_processing_method'])),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('retval', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('processing_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'registration', ['Ai_processing_method_log'])

        # Adding field 'Registration_log.cdr'
        db.add_column(u'registration_registration_log', 'cdr',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['app_manager.Cdr']),
                      keep_default=False)

        # Adding field 'Registration_log.reg_verified'
        db.add_column(u'registration_registration_log', 'reg_verified',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Processing_method'
        db.delete_table(u'registration_processing_method')

        # Deleting model 'Ai_processing_method'
        db.delete_table(u'registration_ai_processing_method')

        # Deleting model 'Ai_processing_method_log'
        db.delete_table(u'registration_ai_processing_method_log')

        # Deleting field 'Registration_log.cdr'
        db.delete_column(u'registration_registration_log', 'cdr_id')

        # Deleting field 'Registration_log.reg_verified'
        db.delete_column(u'registration_registration_log', 'reg_verified')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_reference': {
            'Meta': {'object_name': 'VI_reference'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'target_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi_data': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'})
        },
        u'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'location_fk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'callerinfo.contact_list': {
            'Meta': {'object_name': 'Contact_list'},
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['callerinfo.Contact']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'registration.ai_processing_method': {
            'Meta': {'object_name': 'Ai_processing_method'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apm_reg_ai'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apm_parent_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'processing_method': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['registration.Processing_method']"})
        },
        u'registration.ai_processing_method_log': {
            'Meta': {'object_name': 'Ai_processing_method_log'},
            'ai_method': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['registration.Ai_processing_method']"}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'processing_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'retval': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'registration.processing_method': {
            'Meta': {'object_name': 'Processing_method'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'signature': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'registration.registration': {
            'Meta': {'object_name': 'Registration'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reg_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'contact_list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Contact_list']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_auto': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'parent_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'parent_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'vi_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_reference']", 'null': 'True', 'blank': 'True'})
        },
        u'registration.registration_log': {
            'Meta': {'object_name': 'Registration_log'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reg_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reg_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'reg_verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'registration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['registration.Registration']"}),
            'registration_num': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['registration']