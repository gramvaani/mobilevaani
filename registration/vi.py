from telephony.statemachine import BaseVappController
from telephony.utils import *
from registration.models import *

from log import get_logger
logger = get_logger()


class RegistrationController( BaseVappController ):


    def __init__(self, ai, sessionData, vi_data):
        
        super(RegistrationController, self).__init__(ai, sessionData, vi_data = vi_data)   
        self.registration = Registration.objects.get(ai = self.ai)                                 
        self.registering_ai = vi_data['registering_ai'] if self.vi_data.has_key('registering_ai') else None
        self.reg_success = True

    @classmethod
    def is_embedding_needed(cls, parent_ai, cdr, **kwargs):
        logger.info('inside is_embedding_needed....')
        result = Registration_log.is_embedding_needed(parent_ai, cdr, **kwargs)
        logger.info('the result is : ' + str(result))
        return result

    def while_welcome__sm_action_success(self, events, eventsData):

        if not RegistrationController.is_embedding_needed( self.registration.parent_ai, self.sessionData.cdrs[0] ):
            return 'reg_not_needed'
        self.callerid = self.sessionData.cdrs[0].callerid
        if self.registration.is_caller_registered(self.callerid):
                return 'reg_already_registered'
        
    def pre_regwelcome(self):
        return self.getPromptPlayAndGetParams( promptName = 'registration_welcome', maxDigits = 1, tries = 3, errorPromptName = 'registration_invalid_choice' )        
        
    
    def while_regwelcome__sm_action_success(self, events, eventsData):
        
        digits = self.getPlayAndGetDigits(eventsData)
        if int(digits) == 1:            
            self.manual_register = False    
            if self.registration.is_auto:
                return 'reg_auto_register'
            else:
                self.manual_register = True
                return 'reg_manual_register'
        return 'reg_not_selected'
    
    def pre_alreadyregistered(self):
        return self.getPromptParams('registration_already_registered')
    
    
    def while_autoregister__sm_action_success(self, events, eventsData):       
                               
        rlog, created = Registration_log.get_reg_log(self.registration, self.callerid)
        if rlog:
            if self.registration.contact_list.add(self.callerid):                
                self.reg_success = True 
                rlog.registration.success = True
                rlog.registration.save()
                rlog.reg_success = True
                rlog.save()
        else:
            self.reg_success = False               
            
            
    def while_manualregister__sm_action_success(self, events, eventsData):
        
        self.manual_register = True
        self.registration_pending = False
        rlog, created = Registration_log.get_reg_log(self.registration, self.callerid)

        if not rlog:
            self.reg_success = False

        elif not created and rlog.reg_success == False:
            self.registration_pending = True 
        else:
            self.registration_pending = False        
            
            
    
    def pre_afterregister(self, events, eventsData):
        
        if not self.reg_success:
            try:
              return self.getPromptParams('registration_failed')
            except:
              pass

        if not self.manual_register:
            return self.getPromptParams('registration_success')

        elif self.manual_register and self.registration_pending:
            return self.getPromptParams('registration_pending')

        elif self.manual_register and not self.registration_pending:
            return self.getPromptParams('registration_manual_success')

        if self.registration.vi_ref:
            return '@' + self.registration.vi_ref.reference

    def while_afterregister__sm_action_success(self, events, eventsData):

        if not self.reg_success:
            return 'reg_failure'        
        
    
    def pre_thankyou(self):
        return self.getPromptParams('registration_thank_you')
    
    def while_appexit__sm_action_success(self, events, eventsData):
        transition_string = self.sessionData.popEmbedStack()
        if transition_string:
            return transition_string
    
    
RegistrationStateDescriptionMap = [
    
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    
    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'] 
            } 
        },

    {   'name':'welcome', 
        'action':'none', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'regwelcome':['sm_action_success'],
            'alreadyregistered':['reg_already_registered'],
            'appexit': ['reg_not_needed'],
            } 
    },

    {   'name': 'regwelcome',
        'action': 'play_and_get_digits',
        'transitions': {
            #'stop': ['sm_action_failure'],            
            'alreadyregistered': [ 'sm_action_success','sm_action_failure'],
            'autoregister': ['reg_auto_register'],
            'manualregister': ['reg_manual_register'],            
            'appexit': ['reg_not_selected'],
            },
        },
    
    {   'name': 'alreadyregistered',
        'action': 'playback',
        'transitions' : {
            #'stop': ['sm_action_failure'],
            'thankyou': ['sm_action_success', 'sm_action_failure'],                        
            },
    },
                                  
    {  'name':'autoregister',
       'action':'none',
       'transitions' :{        
        
        'stop': ['sm_action_failure'],
        'afterregister': ['sm_action_success'],    
        
        },
    },
    
    {  'name':'manualregister',
       'action':'none',
       'transitions' :{        
        
        #'stop': ['sm_action_failure'],
        'afterregister': ['sm_action_success', 'sm_action_failure'],    
        
        },
    },

    {  'name':'afterregister',
       'action':'playback',
       'transitions' :{         
        #'stop': [ 'sm_action_failure' ],  
        'thankyou': ['sm_action_success', 'sm_action_failure'], 
        'appexit': ['reg_failure'],
        },
    },  

    {  'name':'thankyou',
       'action':'playback',
       'transitions' :{         
        #'stop': ['sm_action_failure'], 
        'appexit': [ 'sm_action_success', 'sm_action_failure' ],
        },
    },

    {  'name':'appexit',
       'action':'none',
       'transitions' :{         
        'stop': ['sm_action_failure', 'sm_action_success'],           
        },
    },      
        
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
    },   
    ]
