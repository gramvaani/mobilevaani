from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.utils import *
from vapp.sms.tasks import SMSTask
from vapp.sms import utils
from vapp.sms.models import SMS_message, SMS_template
from models import *
from vapp import sms
from app_manager.models import VI_reference

from app_manager.tasks import HTTPTask
from app_manager.models import HTTP_log

class VmissController(BaseVappController):

    def __init__(self, ai, sessionData, vi_data):
        super(VmissController, self).__init__( ai, sessionData )
        self.msg_prop = Properties.objects.get(ai = ai)
        self.call_log = Call_log(cdr_id = self.sessionData.cdrs[0].id, ai = ai)
        self.call_log.save()

    def pre_answercall(self):
        self.call_log.answered = True
        self.call_log.save()
        return self.getIncomingCallParams()

    def pre_playmsg(self):
        return self.getPromptParams('vmiss_play_prompt')

    def while_checkcall__sm_action_success(self, events, eventData):
        if self.msg_prop.answer_call:
            if self.call_log.cdr.answered_time:
                return 'vmiss_already_answered'
            return 'vmiss_answer_true'
        else:
            return 'vmiss_answer_false'

    def while_checksms__sm_action_success(self, events, eventData):
        if self.msg_prop.sms_template is None or self.msg_prop.sms_template.message == "":
            return 'vmiss_sendsms_false'
        else:
            return 'vmiss_sendsms_true'

    def while_checkhttp__sm_action_success(self, events, eventData):
         if self.msg_prop.http_url is None or self.msg_prop.http_url == "":
            return 'vmiss_callhttp_false'
         else:
            return 'vmiss_callhttp_true'

    def while_sendsms__sm_action_success(self, events, eventData):
        max_tries = 1
        sms = SMSTask.create_send_msg(self.ai.id, self.msg_prop.sms_template.message, self.sessionData.callerid, max_tries)
        self.call_log.sms_message = sms
        self.call_log.save()
        return 'sm_action_success'

    def while_callhttp__sm_action_success(self, events, eventData):
        max_tries = 1
        http = HTTPTask.create_send_request(self.ai, self.msg_prop.http_url, max_tries, ph = '0' + self.sessionData.callerid[-10:])
        self.call_log.http_req = http
        self.call_log.save()
        return 'sm_action_success'

    def while_appexit__sm_action_success(self, events, eventData):
        if len(self.sessionData.embed_stack) > 0:
            logger.info(str(self.sessionData.embed_stack))
            return self.sessionData.popEmbedStack()
        return '@nextai'

VmissStateDescriptionMap = [
      {'name' : 'incomingstart' ,
       'action' : 'none',
       'transitions': {
                 'stop':['sm_action_failure'],
                 'welcome':['sm_action_success'],
                 }
       },

       {'name' : 'welcome',
         'action' : 'none',
         'transitions': {
                 'appexit':['sm_action_failure'],
                 'checksms':['sm_action_success'],
                 }
        },
         { 'name' : 'checksms',
            'action' : 'none',
            'transitions': {
                        'sendsms':['vmiss_sendsms_true'],
                        'checkhttp':['vmiss_sendsms_false'],
                        'appexit' : ['sm_action_failure'],
                    }
          },
        {  'name' : 'sendsms',
            'action' : 'none',
            'transitions': {
                        'checkhttp':['sm_action_success'],
                        'appexit' : ['sm_action_failure'],
                    }
         },
        {  'name' : 'checkhttp',
            'action' : 'none',
            'transitions' : {
                        'callhttp':['vmiss_callhttp_true'],
                        'checkcall':['vmiss_callhttp_false'],
                         'appexit':['sm_action_failure'],
                    }

         },
        {  'name' : 'callhttp',
           'action' : 'none',
           'transitions' : {
                        'checkcall':['sm_action_success'],
                        'appexit':['sm_action_failure'],
                    }
         },

        {  'name':'checkcall',
           'action':'none',
           'transitions': {
                 'answercall':['vmiss_answer_true'],
                 'rejectcall':['vmiss_answer_false'],
                 'playmsg':['vmiss_already_answered'],
                 'appexit' : ['sm_action_failure'],
                 }
        },
        {
            'name' : 'rejectcall' ,
            'action' : 'hangup' ,
            'transitions' :{
                        'stop' : ['sm_action_failure','sm_action_success'],
                    }
        },
        {  'name' : 'answercall',
            'action' : 'answer',
            'transitions': {
                     'appexit' : ['sm_action_failure'],
                     'playmsg':['sm_action_success'],
                    }
         },
         { 'name' : 'playmsg',
            'action' : 'playback',
            'transitions': {
                    'stop':['sm_action_failure'],
                    'appexit':['sm_action_success'],
                    }
          },
        {  'name' : 'appexit',
           'action' : 'none',
           'transitions' : {
                        'stop':['sm_action_success', 'sm_action_failure'],
                    }
         },
        {  'name' : 'stop',
            'action' : 'hangup',
            'transitions' : {
                        }
         },
      ]
