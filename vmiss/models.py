from django.db import models
from django.contrib import admin

from media.models import Prompt_audio
from app_manager.models import CalleridField, App_instance
from sms.models import *
from vapp.sms.models import SMS_template
from vapp.app_manager.models import Cdr,HTTP_log


URL_LENGTH = 255
SMS_LENGTH = 160
URL_RESP_CODE_LENGTH = 255

class Properties(models.Model):
    ai = models.ForeignKey(App_instance,related_name = "vmiss_properties_set")
    http_url = models.CharField(max_length = URL_LENGTH, null = True, blank = True)
    sms_template = models.ForeignKey(SMS_template, null = True, blank = True)
    prompt = models.ForeignKey(Prompt_audio, null = True, blank = True)
    answer_call = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.ai) 

class Call_log(models.Model):
    ai = models.ForeignKey(App_instance)
    http_req = models.ForeignKey(HTTP_log, null=True)
    sms_message = models.ForeignKey(SMS_message, null=True)
    cdr = models.ForeignKey(Cdr)
    answered = models.BooleanField(default=False)
