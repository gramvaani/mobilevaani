from vapp.vmiss.models import Call_log
from vapp.app_manager.models import Cdr
from vapp.sms.models import SMS_log

from vapp.stats.models import Cumulative_call_stats, Stats, Stats_type
from vapp.stats.stats import yesterday_cumulative_stats_present, get_prev_cumulative_stats, \
get_start_datetime_for_cumulative_stats

from vapp.utils import is_start_of_week, is_start_of_month, send_email, get_total_seconds, get_ratio, \
generate_workbook, daterange, LINE_BREAK

from app_manager.models import App_instance

from datetime import *
from django.template.loader import render_to_string
import httplib
from log import get_logger
logger = get_logger()


def populate_cumulative_missed_call_stats(ai, start_datetime = None, end_datetime = None, force_update = False):
    mean_avg_dur = 0
    if not end_datetime:
        end_datetime = datetime.now().replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    start, total_calls, total_duration = get_prev_cumulative_stats(ai)
    if not start_datetime:
        if start:
            start_datetime = datetime.combine(start, datetime.min.time())
        else:
            start_datetime = get_start_datetime_for_cumulative_stats(ai)            
            if not start_datetime:
                logger.info("start_datetime not found for %s." % ai.name)
                return
    else:
        if force_update:
            total_calls = 0
            total_duration = 0
            start_datetime = datetime.combine(start_datetime, datetime.min.time())
            cumulative_call_stats_qs = Cumulative_call_stats.objects.filter(ai = ai, to_time = start_datetime)
            if cumulative_call_stats_qs.count() > 0:
                total_calls = cumulative_call_stats_qs[0].calls
                total_duration = total_calls * cumulative_call_stats_qs[0].avg_dur

    if not force_update and yesterday_cumulative_stats_present(ai, end_datetime):
        logger.info("populate_cumulative_call_stats: cumulative stats for ai %s already present. skipping recalculation." % ai.name)
        return

    for date_time in daterange(start_datetime, end_datetime):
        to_datetime = date_time + timedelta(days = 1)
        prev_stats = Cumulative_call_stats.objects.filter(ai = ai, to_time = to_datetime)
        if prev_stats.exists() and not force_update:
            continue

        calls, answered, callers, sms_sent, http_sent = get_missed_call_stats(ai, date_time, to_datetime)
        total_calls += calls
        total_callers = Call_log.objects.filter(ai = ai, cdr__start_time__lte = to_datetime).values('cdr__callerid').distinct().count()
        if prev_stats.exists() and force_update:
            prev_stats.update(calls = total_calls, callers = total_callers, avg_dur = 0)
        else:
            Cumulative_call_stats(ai = ai, to_time = to_datetime, calls = total_calls, callers = total_callers, avg_dur = 0).save()


def populate_stats(ai, start_date = None, end_date = None, force_update = False):
    populate_cumulative_missed_call_stats(ai, force_update = force_update)


def get_missed_call_stats(ai, start, end):
    call_log = Call_log.objects.filter(ai = ai, cdr__start_time__range = (start, end))
    total_calls = answered_calls = callers = sms_sent = http_sent = 0

    if call_log.exists():
        total_calls = call_log.count()
        answered_calls = call_log.filter(answered = True).count()
        callers =  call_log.values('cdr__callerid').distinct().count()
        sms_sent = call_log.filter(sms_message__sent_success = True).count()
        http_sent = call_log.filter(http_req__http_res = httplib.OK).count()

    return ( total_calls, answered_calls, callers, sms_sent, http_sent )


def get_advert_leads_stats(ai_id, start, end):
    ai = App_instance.objects.get(pk = ai_id)
    return get_missed_call_numbers(ai, start, end)

def get_missed_call_numbers(ai, start, end):
    call_logs = Call_log.objects.select_related('cdr').filter(ai = ai, cdr__start_time__range = (start, end))
    data = [['Number', 'Call Date/Time']]
    for call in call_logs:
        data.append([call.cdr.callerid, call.cdr.start_time])

    return data


def get_missed_call_numbers_attachment(ai, start, end):
    data = get_missed_call_numbers(ai, start, end)
    file_name = "/tmp/%s_missed_calls_%s_%s.xlsx" % (ai.id, start.date(), end.date())
    generate_workbook([{'name':'Missed Calls', 'data':data}], file_name)
    return file_name

def get_call_stats(ais, start_datetime, end_datetime, stat_type = None):
    call_stats = []

    for ai in ais:
        stats = Stats()
        stats.ai_name = ai.name
        stats.total_calls, stats.answered_calls, stats.callers, stats.sms_sent, stats.http_sent =  get_missed_call_stats(ai, start_datetime, end_datetime)

        if stat_type == Stats_type.DAILY:
            try:
                cumulative_call_stats = Cumulative_call_stats.objects.get(ai = ai, to_time = end_datetime)
                stats.cumulative_calls = cumulative_call_stats.calls
                stats.cumulative_callers = cumulative_call_stats.callers
            except:
                stats.cumulative_calls = 'Not Available'
                stats.cumulative_callers = 'Not Available'

        call_stats.append(stats)

    return call_stats

def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    data = get_call_stats(ais, start_datetime, end_datetime, stat_type)
    reports = [get_missed_call_numbers_attachment(ai, start_datetime, end_datetime) for ai in ais]
    return (data, reports)


def create_html_content(stats):
    return render_to_string('vmiss/stats.html', { 'data': stats }) + LINE_BREAK
