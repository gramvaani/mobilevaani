# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Call_log.http_req'
        db.alter_column(u'vmiss_call_log', 'http_req_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.HTTP_log'], null=True))

    def backwards(self, orm):

        # Changing field 'Call_log.http_req'
        db.alter_column(u'vmiss_call_log', 'http_req_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.HTTP_message'], null=True))

    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.http_log': {
            'Meta': {'object_name': 'HTTP_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'http_req': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'http_res': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'media.prompt_audio': {
            'Meta': {'object_name': 'Prompt_audio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_info']"}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_set']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'media.prompt_info': {
            'Meta': {'object_name': 'Prompt_info'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'vmiss.call_log': {
            'Meta': {'object_name': 'Call_log'},
            'answered': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'http_req': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.HTTP_log']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_message']", 'null': 'True'})
        },
        u'vmiss.properties': {
            'Meta': {'object_name': 'Properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vmiss_properties_set'", 'to': u"orm['app_manager.App_instance']"}),
            'answer_call': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'http_url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prompt': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_audio']"}),
            'sms_template': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']"})
        }
    }

    complete_apps = ['vmiss']