from vapp.billing.utils import get_common_billing_infos, Billing_info, Call_data, remove_exclusion_duplicates
from models import Call_log

import math

def get_billing_info(start_date, end_date, ai, composite = True, unit_size = 60, excludes = []):
    infos = get_common_billing_infos(start_date, end_date, ai, composite, unit_size, excludes)
    info = get_missed_calls_billing_info(start_date, end_date, ai)
    infos.append(info)

    ex_callerids = [ exclude for exclude in excludes if exclude.discount_type == exclude.CI ]
    for ex_callerid in ex_callerids:
        info = get_missed_calls_callerids_exclusion_info(start_date, end_date, ai, ex_callerid.get_formatted_values())
        info.discount_type = ex_callerid.discount_type
        info.discount_values = ex_callerid.discount_values
        infos.append(info)

    remove_exclusion_duplicates(infos)
    return infos

def make_info_from_missed_calls(missed_calls, name):
    info = Billing_info()
    info.name = name
    info.unit_size = 1

    for call in missed_calls:
        info.summary.count += 1
        info.detail.data[call.cdr.id] = Call_data(call.cdr.id, call.cdr.callerid, call.cdr.start_time, 0)

    info.summary.units = math.ceil(float(info.summary.count)/info.unit_size)

    return info


def get_missed_calls_callerids_exclusion_info(start_date, end_date, ai, ex_callerids):
    missed_calls = Call_log.objects.filter(ai = ai, cdr__start_time__range = (start_date, end_date), answered = False, cdr__callerid__in = ex_callerids)
    info = make_info_from_missed_calls(missed_calls, 'Missed Calls CallerIDs Discount')
    return info

def get_missed_calls_billing_info(start_date, end_date, ai):
    missed_calls = Call_log.objects.filter(ai = ai, cdr__start_time__range = (start_date, end_date), answered = False)
    return make_info_from_missed_calls(missed_calls, 'Missed Calls')
