from django.db import models

from app_manager.models import App_instance, Cdr, App_instance_settings
from media.models import Prompt_audio, get_ai_prompt_set_path

MAX_CHOICE_LENGTH = 5

class Settings(models.Model):
    selector_ai        = models.ForeignKey(App_instance, unique = True)
    max_prompt_retries = models.PositiveSmallIntegerField(default = 3)
    default_choice     = models.ForeignKey(App_instance, null = True, blank = True, related_name = 'selector_default_choice')
    log_choice_error   = models.BooleanField(default = False)
    min_choice         = models.PositiveSmallIntegerField(default = 1)
    max_choice         = models.PositiveSmallIntegerField(default = 9)
    validate_choice    = models.BooleanField(default = True)

    def __unicode__(self):
        return unicode(self.selector_ai) + "_selector_settings"

class Choices(models.Model):
    selector_ai = models.ForeignKey(App_instance, related_name = 'choices_selector_ai_set')
    choice = models.CharField(max_length =  MAX_CHOICE_LENGTH)
    choice_name = models.CharField(max_length = 32)
    ai = models.ForeignKey(App_instance, related_name = 'choices_selected_ai_set')

    class Meta:
        unique_together = (('selector_ai','choice'),)

    def __unicode__(self):
        return unicode(self.selector_ai) + "_" + str(self.choice) + "_" + unicode(self.ai)

class SelectionLog(models.Model):
    cdr = models.ForeignKey(Cdr)
    selector_ai = models.ForeignKey(App_instance, related_name = 'selectionlog_selector_ai_set')
    choice = models.CharField(max_length = MAX_CHOICE_LENGTH, null = True)
    selected_ai = models.ForeignKey(App_instance, null = True, related_name = 'selectionlog_selected_ai_set')

class SelectionError(models.Model):
    cdr = models.ForeignKey(Cdr)
    selector_ai = models.ForeignKey(App_instance)
    choice = models.CharField(max_length = 1)
    choices = models.CharField(max_length = 20)
    
class Call_stats(models.Model):
    ai = models.ForeignKey(App_instance, related_name = 'app_selector_call_stats_ai_set')
    start = models.DateTimeField()
    end = models.DateTimeField()
    calls = models.PositiveIntegerField()
    callers = models.PositiveIntegerField()
    avg_dur = models.DecimalField(max_digits = 10, decimal_places = 2)


def generate_node_data( ai, ai_node_list ):
    ai_settings = App_instance_settings.objects.get( ai = ai )
    settings = Settings.objects.get( selector_ai = ai )
    choices = Choices.objects.filter( selector_ai = ai )
    choice_list = list( choices.values_list( 'choice', flat = True ) )
    prompt_path = get_ai_prompt_set_path( ai )
    node_data = {
        "id": str( ( each[ 'node' ] for each in ai_node_list if each[ 'ai' ] == ai ).next() ),
        "ai": str( ai.id ),
        "vi": str( ai_settings.vi_conf.id ),
        "type": "dtmf",
        "welcomePrompt": '{0}/app_selector_welcome'.format( prompt_path ),
        "instructionPrompt": '{0}/app_selector_prompt'.format( prompt_path ),
        "invalidInputPrompt": '{0}/app_selector_choice_unavailable'.format( prompt_path ),
        "maxRetryCount": str( settings.max_prompt_retries ),
        "bargein": "true",
        "grammar": {
            "values": choice_list
        },
        "input": []
    }
    default_node_id = -1
    for choice in choices:
        next_node_id = str( ( each[ 'node' ] for each in ai_node_list if each[ 'ai' ] == choice.ai ).next() )
        node_data[ 'input' ].append( { 'value': choice.choice, 'nextId': next_node_id } )
        if settings.default_choice == choice.ai:
            default_node_id = next_node_id

    node_data[ "defaultNextId" ] = str( default_node_id )
    return node_data
