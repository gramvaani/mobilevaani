from vapp.app_selector.models import SelectionLog, Choices, Call_stats
from vapp.app_manager.models import App_instance, Transition_event

from vapp.stats.models import Cumulative_call_stats, Stats, Stats_type
from vapp.stats.stats import get_ai_call_stats, populate_daily_cumulative_stats

from vapp.utils import is_start_of_week, is_start_of_month, send_email, get_ratio, daterange, LINE_BREAK

from datetime import *
from django.template.loader import render_to_string

from log import get_request_logger
logger = get_request_logger()

import math

def get_stats_for_choices(ai, start = None, end = None):
    choices = {}
    choice_percent = {}
    selection_log = SelectionLog.objects.filter(selector_ai = ai)
    if start and end:
        selection_log = selection_log.filter(cdr__start_time__range = (start, end))

    for selection in selection_log:
        selected_ai = selection.selected_ai
        choice = selection.choice
         
        if selected_ai:
            choices[selected_ai.name] = choices.get(selected_ai.name, 0) + 1
        elif choice:
            try:
                chosen_ai_name = Choices.objects.get(selector_ai = ai, choice = choice).ai.name
                choices[chosen_ai_name] = choices.get(chosen_ai_name, 0) + 1
            except:
                pass
        else:
            choices['No Choices'] = choices.get('No Choices', 0) + 1
            
    for choice, count in choices.items():
        choice_percent[choice] = round( get_ratio(count,selection_log.count(),exact=True)*100, 2 )
        
    return choice_percent    


def get_daily_choices_stats_from_start_date_till_today(start_date, ai_id):
    start = get_date_from_str(start_date)
    end = (datetime.now() + timedelta(days = 1)).date()
    ai = App_instance.objects.get(pk = ai_id)
    
    data = {}
    for d in daterange(start, end):
        choice_percentages = get_stats_for_choices(ai, d, d + timedelta(days = 1))
        data[str(d)] = choice_percentages

    return data


def get_start_date(ai_id):
    stats = Call_stats.objects.filter(ai_id = ai_id).order_by('-id')
    if stats.exists():
        return stats[0].end.replace(hour = 0,minute = 0,second = 0, microsecond = 0)
    else:
        stats = Transition_event.objects.filter(ai_id = ai_id).order_by('id')
        if stats.exists():
            return stats[0].time.replace(hour = 0,minute = 0,second = 0, microsecond = 0)
    
    return None


def populate_stats(ai, start_date = None, end_date = None, force_update = False):
    start_date = start_date or get_start_date(ai.id)
    if not start_date:
        logger.info("Start date for populating stats for ai = %s not found." % ai.name)
        return
    end_date = datetime.now().replace(hour = 0,minute = 0,second = 0, microsecond = 0)
    
    for date in daterange(start_date, end_date):
        start_datetime = datetime.combine(date, datetime.min.time())
        end_datetime = datetime.combine(date + timedelta(days = 1), datetime.min.time())
        populate_call_stats(ai, start_datetime, end_datetime, force_update)

        if is_start_of_week(end_datetime.date()):
            start = end_datetime - timedelta(days = 7)
            populate_call_stats(ai, start, end_datetime, force_update)
            
        # if is_start_of_month(end_datetime.date()):
        #     start = (end_datetime - timedelta(days = 1)).replace(day = 1)
        #     populate_call_stats(ai, start, end_datetime)
        
    populate_daily_cumulative_stats(ai, force_update = force_update)
    
    
def populate_call_stats(ai, start_datetime, end_datetime, force_update = False):
    logger.info("populate_call_stats: ai = %s: %s -> %s" % (ai.name, start_datetime, end_datetime))
    prev_stat = Call_stats.objects.filter(ai = ai, start = start_datetime, end = end_datetime)

    if prev_stat.exists():
        if force_update:
            call_stats = get_ai_call_stats(ai, start_datetime, end_datetime)
            prev_stat.update(calls = call_stats.calls, callers = call_stats.callers, avg_dur = str(call_stats.avg_call_dur))
        else:
            logger.info("populate_call_stats: stats for ai %s already present. skipping recalculation." % ai.name)
    else:
        call_stats = get_ai_call_stats(ai, start_datetime, end_datetime)
        call_stat = Call_stats(ai = ai, start = start_datetime, end = end_datetime, calls = call_stats.calls, callers = call_stats.callers, avg_dur = str(call_stats.avg_call_dur))
        call_stat.save()

def get_call_stats(ais, start_datetime, end_datetime, cumulative = False):
    call_stats = []

    for ai in ais:
        stats = Stats()
        stats.ai_name = ai.name
        try:
            call_stat = Call_stats.objects.get(ai = ai, start = start_datetime, end = end_datetime)
            stats.calls, stats.callers, stats.avg_dur = call_stat.calls, call_stat.callers, call_stat.avg_dur
        except:
            call_stat = get_ai_call_stats(ai, start_datetime, end_datetime)
            stats.calls = call_stat.calls
            stats.callers = call_stat.callers
            stats.avg_dur = call_stat.avg_call_dur
            
        stats.choices = get_stats_for_choices(ai, start_datetime, end_datetime)
        if cumulative:
            try:
                cumulative_call_stats = Cumulative_call_stats.objects.get(ai = ai, to_time = end_datetime)
                stats.cumulative_calls = cumulative_call_stats.calls
                stats.cumulative_avg_dur = cumulative_call_stats.avg_dur
            except:
                stats.cumulative_calls = 'Not Available'
                stats.cumulative_avg_dur = 'Not Available'

        call_stats.append(stats)

    return call_stats

def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    cumulative = stat_type == Stats_type.DAILY
    data = get_call_stats(ais, start_datetime, end_datetime, cumulative)
    reports = []
    return (data, reports)

def create_html_content(stats):
    return render_to_string('app_selector/stats.html', { 'data': stats }) + LINE_BREAK

def get_app_selector_ais_names_choices(cdrs):
    selector_ais = set()
    for cdr in cdrs:
        selectionlog_objs = SelectionLog.objects.filter(cdr = cdr)
        for selectionlog_obj in selectionlog_objs:
            selector_ais.add(selectionlog_obj.selector_ai)
    selector_ai_name_choices_dict_list = []
    for selector_ai in selector_ais:
        selector_ai_name_choices_dict = {}
        selector_ai_name_choices_dict['name'] = selector_ai.name
        selector_ai_name_choices_dict['choice'] = []
        choices = [int(choice) for choice in Choices.objects.filter(selector_ai = selector_ai).values_list('choice', flat = True)]
        if len(choices) > 0:
            selector_ai_name_choices_dict['choice'] = choices
        selector_ai_name_choices_dict_list.append(selector_ai_name_choices_dict)
    return selector_ai_name_choices_dict_list