# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Settings.default_choice'
        db.add_column(u'app_selector_settings', 'default_choice',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='selector_default_choice', null=True, to=orm['app_manager.App_instance']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Settings.default_choice'
        db.delete_column(u'app_selector_settings', 'default_choice_id')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_selector.call_stats': {
            'Meta': {'object_name': 'Call_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'app_selector_call_stats_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            'avg_dur': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'callers': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'app_selector.choices': {
            'Meta': {'unique_together': "(('selector_ai', 'choice'),)", 'object_name': 'Choices'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'choices_selected_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            'choice': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'choice_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'choices_selector_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'app_selector.selectionerror': {
            'Meta': {'object_name': 'SelectionError'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'choice': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"})
        },
        u'app_selector.selectionlog': {
            'Meta': {'object_name': 'SelectionLog'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'choice': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'selected_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selectionlog_selected_ai_set'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selectionlog_selector_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'app_selector.settings': {
            'Meta': {'object_name': 'Settings'},
            'default_choice': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'selector_default_choice'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_choice_error': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_prompt_retries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'unique': 'True'})
        }
    }

    complete_apps = ['app_selector']