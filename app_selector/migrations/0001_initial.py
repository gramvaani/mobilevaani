# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Settings'
        db.create_table(u'app_selector_settings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('selector_ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], unique=True)),
            ('welcome_text', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('welcome_audio', self.gf('django.db.models.fields.related.ForeignKey')(related_name='app_selector_welcome_set', to=orm['media.Prompt_audio'])),
            ('prompt_text', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('prompt_audio', self.gf('django.db.models.fields.related.ForeignKey')(related_name='app_selector_prompt_set', to=orm['media.Prompt_audio'])),
            ('max_prompt_retries', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=3)),
            ('log_choice_error', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'app_selector', ['Settings'])

        # Adding model 'Choices'
        db.create_table(u'app_selector_choices', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('selector_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='choices_selector_ai_set', to=orm['app_manager.App_instance'])),
            ('choice', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('choice_name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='choices_selected_ai_set', to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'app_selector', ['Choices'])

        # Adding unique constraint on 'Choices', fields ['selector_ai', 'choice']
        db.create_unique(u'app_selector_choices', ['selector_ai_id', 'choice'])

        # Adding model 'SelectionLog'
        db.create_table(u'app_selector_selectionlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('selector_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='selectionlog_selector_ai_set', to=orm['app_manager.App_instance'])),
            ('choice', self.gf('django.db.models.fields.CharField')(max_length=1, null=True)),
            ('selected_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='selectionlog_selected_ai_set', null=True, to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'app_selector', ['SelectionLog'])

        # Adding model 'SelectionError'
        db.create_table(u'app_selector_selectionerror', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('selector_ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('choice', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('choices', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'app_selector', ['SelectionError'])


    def backwards(self, orm):
        # Removing unique constraint on 'Choices', fields ['selector_ai', 'choice']
        db.delete_unique(u'app_selector_choices', ['selector_ai_id', 'choice'])

        # Deleting model 'Settings'
        db.delete_table(u'app_selector_settings')

        # Deleting model 'Choices'
        db.delete_table(u'app_selector_choices')

        # Deleting model 'SelectionLog'
        db.delete_table(u'app_selector_selectionlog')

        # Deleting model 'SelectionError'
        db.delete_table(u'app_selector_selectionerror')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_selector.choices': {
            'Meta': {'unique_together': "(('selector_ai', 'choice'),)", 'object_name': 'Choices'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'choices_selected_ai_set'", 'to': u"orm['app_manager.App_instance']"}),
            'choice': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'choice_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'choices_selector_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'app_selector.selectionerror': {
            'Meta': {'object_name': 'SelectionError'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'choice': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"})
        },
        u'app_selector.selectionlog': {
            'Meta': {'object_name': 'SelectionLog'},
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'choice': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'selected_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selectionlog_selected_ai_set'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selectionlog_selector_ai_set'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'app_selector.settings': {
            'Meta': {'object_name': 'Settings'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_choice_error': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_prompt_retries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'prompt_audio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'app_selector_prompt_set'", 'to': u"orm['media.Prompt_audio']"}),
            'prompt_text': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'selector_ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'unique': 'True'}),
            'welcome_audio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'app_selector_welcome_set'", 'to': u"orm['media.Prompt_audio']"}),
            'welcome_text': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'media.prompt_audio': {
            'Meta': {'object_name': 'Prompt_audio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_info']"}),
            'prompt_set': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Prompt_set']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'media.prompt_info': {
            'Meta': {'object_name': 'Prompt_info'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'media.prompt_set': {
            'Meta': {'object_name': 'Prompt_set'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'und'", 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['app_selector']