from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import Authorization

from tastypie.resources import Resource
from tastypie.utils import trailing_slash
from tastypie import fields

from datetime import datetime

from models import Call_stats, Settings, Choices
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from vapp.app_manager.models import App_instance
from media.api import PromptAudioResource
from app_manager.api import AppInstanceResource
from vapp.api import AppInstanceAuthorization

from vapp.app_selector.stats import get_stats_for_choices
from vapp.stats.stats import get_callscount_callerscount_avgdur

from vapp.log import get_request_logger

logger = get_request_logger()

class AppSelectorStatsResource(Resource):
    class  Meta:
        resource_name = 'app_selector'        
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/call_stats%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_call_stats' ), name = "api_call_stats" ),
            url(r"^(?P<resource_name>%s)/choices%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_choices' ), name = "api_choices" ),
        ]

    def get_call_stats(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST['ai_id']
            start_datetime = datetime.strptime(request.REQUEST['start_date'], '%Y-%m-%d')
            end_datetime = datetime.strptime(request.REQUEST['end_date'], '%Y-%m-%d')
            try:
                calls, callers, avg_dur = Call_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime)
            except:
                calls, callers, avg_dur = get_callscount_callerscount_avgdur(ai_id, start_datetime, end_datetime)

            return self.create_response( request, { "calls": calls, "callers": callers, "avg_dur": avg_dur } )
        except:
            logger.exception( "Exception in get_call_stats: %s" % request )
        return self.create_response( request, { 'error': "unable to get call stats" } )
    
    def get_choices(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST['ai_id']
            ai = App_instance.objects.filter(pk = ai_id)
            start_datetime = request.REQUEST.get('start_date', None)
            end_datetime = request.REQUEST.get('end_date', None)
            if start_datetime and end_datetime:
                start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
                end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d')

            choices = get_stats_for_choices(ai, start_datetime, end_datetime)
            return self.create_response( request, { "choices": choices } )
        except:
            logger.exception( "Exception in get_choices: %s" % request )
        return self.create_response( request, { 'error': "unable to get choices" } )

class AppSelectorSettingsResource(ModelResource):
    
    ai = fields.ForeignKey(AppInstanceResource, 'selector_ai', full = True)
    default_choice = fields.ForeignKey(AppInstanceResource, 'default_choice', null = True)
    
    class Meta:
        queryset = Settings.objects.all()
        resource_name = 'as_settings'
        allowed_methods = ['post', 'get']
        filtering = {
            'ai': ALL_WITH_RELATIONS,
        }
        
        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()

class AppSelectorChoiceResource(ModelResource):
    
    ai = fields.ForeignKey(AppInstanceResource, 'selector_ai')
    ai_choice = fields.ForeignKey(AppInstanceResource, 'ai', full = True)
    
    class Meta:
        queryset = Choices.objects.all()
        resource_name = 'as_choices'    
        allowed_methods = ['post', 'get', 'delete']
        filtering = {
            'ai': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def prepend_urls(self):
       return [
          url(r"^(?P<resource_name>%s)/get_keys%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_keys' ), name = "api_keys" ),
        ]

    def hydrate(self, bundle):
        if bundle.data.has_key( 'ai_choice' ) and bundle.data['ai_choice']:
            bundle.obj.ai_id = bundle.data['ai_choice']['id']
            del bundle.data['ai_choice']

        if bundle.data.has_key( 'ai' ) and bundle.data['ai']:
            bundle.obj.selector_ai_id = bundle.data['ai']['id']
            del bundle.data['ai']

        return bundle

    def get_keys(self, request, **kwargs):
        values = []
        try:
            ai_id = request.REQUEST['ai_id']
            settings = Settings.objects.get( selector_ai__id = ai_id )
            values = range( settings.min_choice, settings.max_choice + 1 )
        except Exception, e:
            logger.exception('exception while generating keys for ai_id: %s - %s' % (ai_id, e) )
        return self.create_response( request, { 'keys': values } )       


