from app_selector.models import *
from telephony.statemachine import BaseVappController

from vapp.log import get_logger
logger = get_logger()

class AppSelectorController(BaseVappController):
    tries = 0
    invalid_choice = None
    selection_log = None
    settings = None

    def __init__(self, ai, sessionData, vi_data):
        super(AppSelectorController, self).__init__( ai, sessionData, vi_data )
        self.sessionData = sessionData
        self.settings = Settings.objects.get(selector_ai = self.ai)

    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid )
    
    def pre_incomingstart(self):
        return self.getIncomingCallParams()
        
    def pre_welcome(self):
        self.selection_log = SelectionLog(selector_ai = self.ai)
        self.selection_log.cdr = self.sessionData.cdrs[0]
        self.selection_log.selected_ai = None
        self.selection_log.choice = None
        self.selection_log.save()
        self.tries = 0
        return self.getPromptParams('app_selector_welcome')

    def while_incrementandchecktries__sm_action_success(self, events, eventData):
        self.tries += 1
        if self.tries <= self.settings.max_prompt_retries:
            return 'app_selector_play_prompt'
        else:
            return 'app_selector_max_tries_reached'

    def pre_prompt(self):
        return self.getPromptPlayAndGetParams('app_selector_prompt', minDigits = len(str(self.settings.min_choice)), maxDigits = len(str(self.settings.max_choice)) )

    def while_prompt__sm_action_failure(self, events, eventData):
        if 'fs_channel_hangup' in events:
            return 'sm_action_failure'
        else:
            return 'app_selector_invalid_digit'
    
    def while_prompt__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info("choice:" + str(digit))
        choices = Choices.objects.filter(selector_ai = self.ai).values_list('choice', flat = True)
        if digit and str(digit) in choices:
            selected_ai = Choices.objects.get(selector_ai = self.ai, choice = str(digit)).ai
            self.selection_log.selected_ai = selected_ai
            self.selection_log.choice = str(digit)
            self.selection_log.save()
            return '@ai_' + str(selected_ai.id) + '_welcome'
        elif digit:
            self.invalid_choice = digit
            if self.settings.log_choice_error:
                error = SelectionError(cdr = self.sessionData.cdrs[0], selector_ai = self.ai, choice = str(self.invalid_choice), choices = ','.join(choices))
                error.save()
            if self.settings.validate_choice:
                return 'app_selector_invalid_choice'
            else:
                return 'app_selector_invalid_choice_no_validation'
        else:
            return 'app_selector_invalid_digit'
    
    def pre_announceinvalidchoice(self):
        return self.getPromptParams('app_selector_validate_announce')

    def pre_playbackchoice(self):
        return self.getPromptParams('digit_' + str(self.invalid_choice))

    def pre_choiceunavailable(self):
        return self.getPromptParams('app_selector_choice_unavailable')

    def while_jumptodefaultchoice__sm_action_success(self, events, eventData):
        if self.settings.default_choice:
            return "@ai_{0}_welcome".format(self.settings.default_choice_id)
        else:
            return 'no_default_choice'

AppSelectorStateDescriptionMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'] 
            } 
        },
    {   'name':'welcome', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'incrementandchecktries':['sm_action_success'],
            }, 
        },
    {   'name':'incrementandchecktries',
        'action':'none',
        'transitions': {
            'prompt':['app_selector_play_prompt'],
            'jumptodefaultchoice':['app_selector_max_tries_reached'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'prompt',
        'action':'play_and_get_digits',
        'transitions': {
            'announceinvalidchoice': ['app_selector_invalid_choice'],
            'choiceunavailable': ['app_selector_invalid_choice_no_validation'],
            'incrementandchecktries': ['app_selector_invalid_digit','sm_get_digits_no_digits'],
            'stop': ['sm_action_failure'],
            },
        },
    {   'name':'announceinvalidchoice',
        'action':'playback',
        'transitions': {
            'playbackchoice':['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'playbackchoice',
        'action':'playback',
        'transitions': {
            'choiceunavailable': ['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name':'choiceunavailable',
        'action':'playback',
        'transitions': {
            'incrementandchecktries': ['sm_action_success'],
            'stop':['sm_action_failure'],
            },
        },
    {   'name': 'jumptodefaultchoice',
        'action': 'none',
        'transitions': {
            'stop': ['no_default_choice', 'sm_action_failure'],
            },
        },
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
        },
    ]
