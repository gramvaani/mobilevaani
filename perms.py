'''
Created on 06-Jan-2011

@author: bchandra
'''

import sys
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.core.handlers.wsgi import WSGIRequest
from log import get_request_logger
logger = get_request_logger()

def load_perms(name):
    try:
        name = name + '.perms'
        __import__(name)
        module = sys.modules[name]
        app_name = name.split('.')[1]
        for perm in module.PERMS:
            setattr(module, perm, app_name + '_' + perm)
    except:
        print "Could not load perms for: " + name


from django.contrib.auth.models import User
from vapp.app_manager.models import User_permission, Group_permission

def user_has_perms(user, ai_id, *perm_codes):
    logger.info('inside user has perms')
    #if type(user) == WSGIRequest:
    if hasattr(user, 'user') and hasattr(getattr(user, 'user'), 'id'):
        user_id = user.user.id
    elif type(user) == User:
        user_id = user.id
    elif type(user) == int:
        user_id = user

    for perm_code in perm_codes:
        if not User_permission.objects.filter(user = user_id, ai = ai_id, perm__code = perm_code).exists() and \
            not Group_permission.objects.filter(group__user = user_id, ai = ai_id, perm__code = perm_code).exists():
            logger.info('About to return false')
            return False
        
    return True

def user_perms_ai_ids(user, *perm_codes):
    
    ais = set()
    user_id = None

    if type(user) == User:
        user_id = user.id
    elif type(user) == long:
        user_id = long(user)

    for perm_code in perm_codes:        
        user_ais = User_permission.objects.filter(user = user_id, perm__code = perm_code).values_list('ai', flat = True)
        group_ais = Group_permission.objects.filter(group__user = user_id, perm__code = perm_code).values_list('ai', flat = True)
        ais = ais | set(list(user_ais) + list(group_ais))
    
    return list(ais)
        

def get_perms_for_ai(ai, user_id):
    user = User.objects.get(pk = user_id)
    perm_codes = []
    perm_codes = set( User_permission.objects.filter(user = user, ai = ai).values_list('perm__code', flat = True) )
    perm_codes.update( Group_permission.objects.filter(group__user = user, ai = ai).values_list('perm__code', flat = True) )    
    return perm_codes     
