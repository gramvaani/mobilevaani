import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'vapp.settings'

import_paths = [ '/usr/local/voicesite/vapp', '/usr/local/voicesite' ]
for path in import_paths:
    if path not in sys.path:
       sys.path.append(path)
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", vapp.settings")

# This application object is used by the development server
# as well as any WSGI server configured to use this file.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
