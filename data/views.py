from django.http import HttpResponse
from django.core import serializers
from log import get_request_logger

logger = get_request_logger()

from django.contrib.auth.decorators import login_required

from data.models import Eicher_delhi

@login_required
def eicher_delhi(request, ai_id):    
    logger.debug(request)
    
    if ai_id:
        ai_id = int(ai_id)
        result = Eicher_delhi.objects.filter(ai = ai_id)
    else:
        result = Eicher_delhi.objects.all()

    json = serializers.get_serializer("json")()
    return HttpResponse(json.serialize(result, fields=('code', 'name')))