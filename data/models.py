from django.db import models
from vapp.app_manager.models import App_instance

class Eicher_delhi(models.Model):
    name    = models.CharField(max_length = 48)
    code    = models.CharField(max_length = 32)
    district= models.CharField(max_length = 24)
    hcrcs   = models.CharField(max_length = 10)
    ward    = models.CharField(max_length = 16)
    ai      = models.ForeignKey(App_instance)
    
    def __unicode__(self):
        return unicode(self.name)