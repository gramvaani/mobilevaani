from django.conf.urls import url, patterns, include
from django.views.generic import RedirectView

from django.contrib.auth.views import login, logout

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

from mnews.models import JMRItemsFeed

from tastypie.api import Api
from app_manager.api import UserResource, AppInstanceResource, ScheduleResource, AppResource, ViReferenceResource, UserPermsResource, LineResource, \
DataSyncResource, AppVersionResource, MVAppAiResource, AiTransientGroupResource
from media.api import RecordingResource, PromptAudioResource, ImageResource, ImageCaptionMapResource, AiPromptSetResource
from mnews.api import CategoryResource, NewsResource, ChannelResource, ModerationEventResource,\
                         ModCommentResource, NewsFormatResource, NewsQualifierResource, MnewsPromptDescResource,\
                         MnewsStatsResource, SocialLogResource, CollectionResource, NewsCollectionResource, NewsViReferenceResource, \
                         PlaylistItemResource, PlaylistResource, MVAppResource, PlaylistItemCommentResource, AgeGroupResource, OccupationResource, GenderResource, RejectionReasonResource

from callerinfo.api import ContactListResource, ContactResource
from location.api import StateResource, DistrictResource, PanchayatResource,BlockResource, LocationResource
from survey.api import RecordResource, QuestionMcResource, QuestionFfResource, QuestionQuResource, SurveyResource,\
                         SurveyStatsResource, FormResource, FormQuestionResource, SurveyResource, AssessmentResource
from stats.api import StatsViewResource, StatsResource
from pub_web.api import WebInstanceResource, EmployeeResource, TeamResource, ContactRequestResource
from campaign.api import CampaignResource, CampaignCategoryResource, CampaignStatsResource
from sms.api import SMSMessageResource, SMSMessageUpdateResource

from social.api import SocialMediaResource
from app_selector.api import AppSelectorStatsResource, AppSelectorSettingsResource, AppSelectorChoiceResource
from advert.api import AdvertStatsResource, AdvertAppInstanceMapResource
from field_mis.api import VolunteerIncentiveStatsResource, VolunteerStatsResource, VolunteerCommunityMeetingsResource, PaymentSlabClubCallsResource, PaymentSlabGoodContribsResource, PaymentSlabCommunityMeetingsResource, PaymentSlabResource, VolunteerResource, VolunteerClubStatsResource, VolunteerClubResource, CommunityManagerResource, FieldLocationStatsResource, FieldCampaignStatsResource
from customer.api import OrganizationResource, ProjectResource, ProjectStatsViewResource
from helpline.api import HandlerResource, GrievanceResource, HelplineResource, HelplineCallResource
from refer.api import ReferralResource
from mvas.api import MvasDataResource
from customization.api import CustomSMSResource, CustomStatsResource
from xforms.api import XFormDataResource, GenericXFormDataResource
from mvapp.api import ResourceListResource, MVAppVersionResource, MVAppUserResource, ResourcelistItemCommentResource, ResourceGroupLibraryFoldersResource, NewsAppResource
from scheduler.api import SchedulerResource
from case_manager.api import ModeratorProfileResource, GrievanceStateResource, CaseManagerGrievanceResource, \
                             CaseManagerHandlerResource, CaseManagerGrievanceQuitReasonResource,CaseManagerGrievancePropertiesResource

v1_api = Api(api_name = 'v1')

v1_api.register(UserResource())
v1_api.register(AppInstanceResource())
v1_api.register(RecordingResource())
v1_api.register(PromptAudioResource())
v1_api.register(ImageResource())
v1_api.register(AiPromptSetResource())
v1_api.register(ImageCaptionMapResource())
v1_api.register(AppResource())
v1_api.register(UserPermsResource())
v1_api.register(SMSMessageResource())
v1_api.register(SMSMessageUpdateResource())


v1_api.register(StateResource())
v1_api.register(DistrictResource())
v1_api.register(BlockResource())
v1_api.register(PanchayatResource())
v1_api.register(LocationResource())

v1_api.register(CategoryResource())
v1_api.register(NewsResource())
v1_api.register(ChannelResource())
v1_api.register(ModerationEventResource())
v1_api.register(ModCommentResource())
v1_api.register(ContactListResource())
v1_api.register(ContactResource())
v1_api.register(NewsFormatResource())
v1_api.register(NewsQualifierResource())
v1_api.register(MnewsPromptDescResource())
v1_api.register(MnewsStatsResource())
v1_api.register(SocialLogResource())
v1_api.register(VolunteerIncentiveStatsResource())
v1_api.register(VolunteerStatsResource())
v1_api.register(VolunteerCommunityMeetingsResource())
v1_api.register(PaymentSlabCommunityMeetingsResource())
v1_api.register(PaymentSlabGoodContribsResource())
v1_api.register(PaymentSlabClubCallsResource())
v1_api.register(PaymentSlabResource())
v1_api.register(VolunteerResource())
v1_api.register(VolunteerClubStatsResource())
v1_api.register(VolunteerClubResource())
v1_api.register(CommunityManagerResource())
v1_api.register(FieldLocationStatsResource())
v1_api.register(FieldCampaignStatsResource())

v1_api.register(RecordResource())
v1_api.register(SurveyResource())
v1_api.register(QuestionMcResource())
v1_api.register(QuestionFfResource())
v1_api.register(QuestionQuResource())
v1_api.register(SurveyStatsResource())
v1_api.register(AssessmentResource())
v1_api.register(FormResource())
v1_api.register(FormQuestionResource())

v1_api.register(StatsViewResource())
v1_api.register(StatsResource())
v1_api.register(ProjectResource())
v1_api.register(OrganizationResource())
v1_api.register(ProjectStatsViewResource())

v1_api.register(ScheduleResource())
v1_api.register(AiTransientGroupResource())
v1_api.register(WebInstanceResource())
v1_api.register(EmployeeResource())
v1_api.register(TeamResource())
v1_api.register(ContactRequestResource())
v1_api.register(CollectionResource())
v1_api.register(NewsCollectionResource())
v1_api.register(NewsViReferenceResource())
v1_api.register(ViReferenceResource())
v1_api.register(LineResource())

v1_api.register(HandlerResource())
v1_api.register(GrievanceResource())
v1_api.register(HelplineResource())
v1_api.register(HelplineCallResource())
v1_api.register(CampaignResource())
v1_api.register(CampaignCategoryResource())
v1_api.register(CampaignStatsResource())

v1_api.register(AppSelectorStatsResource())
v1_api.register(AppSelectorSettingsResource())
v1_api.register(AppSelectorChoiceResource())

v1_api.register(SocialMediaResource())

v1_api.register(AdvertStatsResource())
v1_api.register(AdvertAppInstanceMapResource())

v1_api.register(ReferralResource())
v1_api.register(PlaylistResource())
v1_api.register(PlaylistItemResource())
v1_api.register(MVAppResource())
v1_api.register(MvasDataResource())
v1_api.register(DataSyncResource())
v1_api.register(PlaylistItemCommentResource())
v1_api.register(AppVersionResource())
v1_api.register(MVAppAiResource())
v1_api.register(AgeGroupResource())
v1_api.register(OccupationResource())
v1_api.register(GenderResource())
v1_api.register(RejectionReasonResource())

v1_api.register(CustomSMSResource())
v1_api.register(CustomStatsResource())
v1_api.register(XFormDataResource())
v1_api.register(GenericXFormDataResource())

v1_api.register(ResourceListResource())
v1_api.register(MVAppVersionResource())
v1_api.register(MVAppUserResource())
v1_api.register(ResourceGroupLibraryFoldersResource())

v1_api.register(SchedulerResource())
v1_api.register(ModeratorProfileResource())
v1_api.register(GrievanceStateResource())
v1_api.register(CaseManagerGrievanceResource())
v1_api.register(CaseManagerHandlerResource())
v1_api.register(CaseManagerGrievanceQuitReasonResource())
v1_api.register(CaseManagerGrievancePropertiesResource())

v1_api.register(ResourcelistItemCommentResource())
v1_api.register(NewsAppResource())

urlpatterns = patterns('',
    # Example:
    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:

    url(r'^api/doc/', include('tastypie_swagger.urls', namespace='tastypie_swagger'),
        kwargs = {
            "tastypie_api_module":"vapp.urls.v1_api",
            "namespace":"tastypie_swagger",
            "version": "1"
            }
        ),
    # url(r'^profiler/', include('profiler.urls')),
    url(r'^api/', include(v1_api.urls)),

    url(r'', include('social_auth.urls')),
    (r'^comments/', include('django.contrib.comments.urls')),
    url(r'^plate/', include('django_spaghetti.urls')),
    #(r'^survey/$', 'survey.views.demo'),
    (r'^admin/', include(admin.site.urls)),
    #(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html', }),
    (r'^logout/', logout, {'next_page': '/vapp/'}),
    #(r'^app/(.*)$',  'django.views.static.serve', {'document_root': '/var/www/voice'}),

    (r'^login_mnews', 'mnews.views.login'),
    (r'^logout_mnews', 'mnews.views.logout'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show$', 'mnews.views.show_news'),
    (r'^mnews/show/oursay/', 'mnews.views.show_news_oursay'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show/loc/(?P<location>[-_+a-zA-Z]+)$', 'mnews.views.show_news'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show/locid/(?P<location_id>\d+)$', 'mnews.views.show_news'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show/detail/(?P<news_id>\d+)/$', 'mnews.views.show_detail'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show/tags/(?P<tag>[-_+a-zA-Z0-9]+)/$', 'mnews.views.show_news'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show/qualifier/(?P<qualifier_id>[-_+a-zA-Z0-9]+)/$', 'mnews.views.show_news'),
    (r'^mnews/(?P<ai_id>([0-9])+)/show/format/(?P<format_id>[-_+a-zA-Z0-9]+)/$', 'mnews.views.show_news'),
    (r'^mnews/(?P<ai_id>([0-9])+)/embed/(?P<news_id>\d+)/$', 'mnews.views.get_embedded_news'),
    (r'^mnews/get_news_metadata/(?P<format>\w+)/(?P<news_id>\d+)$', 'mnews.views.get_news_metadata'),

    (r'^mnews/history/$', 'mnews.views.get_moderation_event_history'),
    (r'^mnews/history/(?P<user_id>([0-9])+)/$', 'mnews.views.get_moderation_event_history'),
    (r'^mnews/history/(?P<user_id>([0-9])+)/(?P<app_instance>([0-9])+)/$', 'mnews.views.get_moderation_event_history'),
    (r'^mnews/history/(?P<user_id>([0-9])+)/(?P<start_date>[-_+a-zA-Z0-9]+)/(?P<end_date>[-_+a-zA-Z0-9]+)/$', 'mnews.views.get_moderation_event_history'),
    (r'^mnews/history/(?P<user_id>([0-9])+)/(?P<app_instance>([0-9])+)/(?P<start_date>[-_+a-zA-Z0-9]+)/(?P<end_date>[-_+a-zA-Z0-9]+)/$', 'mnews.views.get_moderation_event_history'),

    (r'^case_manager/(?P<ai_id>([0-9])+)/0/0/show$', 'case_manager.views.show_news'),
    (r'^case_manager/(?P<ai_id>([0-9])+)/(?P<handler_id>\d+)/0/show$', 'case_manager.views.show_news'),
    (r'^case_manager/(?P<ai_id>([0-9])+)/0/(?P<state_id>\d+)/show$', 'case_manager.views.show_news'),
    (r'^case_manager/(?P<ai_id>([0-9])+)/(?P<handler_id>\d+)/(?P<state_id>\d+)/show$', 'case_manager.views.show_news'),
    (r'^case_manager/(?P<ai_id>([0-9])+)/show/tags/(?P<tag>[-_+a-zA-Z0-9]+)/$', 'case_manager.views.show_news'),
    (r'^case_manager/(?P<ai_id>([0-9])+)/show/qualifier/(?P<qualifier_id>[-_+a-zA-Z0-9]+)/$', 'case_manager.views.show_news'),
    (r'^case_manager/(?P<ai_id>([0-9])+)/show/format/(?P<format_id>[-_+a-zA-Z0-9]+)/$', 'case_manager.views.show_news'),

    )
