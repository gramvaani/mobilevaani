from django.http import HttpResponse, HttpResponseNotFound
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth.decorators import login_required

from vapp.helpline.models import *
from vapp.log import get_request_logger
from vapp.app_manager.decorators import perms_required, instance_perm, instance_perm_per_obj #@UnresolvedImport
import vapp.app_manager.perms as app_manager_perms #@UnresolvedImport

import json

logger = get_request_logger()

MAX_CALLS_PER_PAGE = 20

@login_required
@instance_perm
def calls(request, ai_id, start_row, num_rows):
    if start_row is None or num_rows is None:
        return HttpResponse(json.dumps({'num_rows': Call.objects.filter(ai = ai_id).count()}))
    start_row = int(start_row)
    num_rows  = int(num_rows)
    end_row = start_row + num_rows
    json_serializer = serializers.get_serializer("json")()
    data = json_serializer.serialize(Call.objects.filter(ai = ai_id).order_by('-media__time')[start_row: end_row], ensure_ascii = False, extras=('time',), relations=('location'))
    logger.debug(request)
    return HttpResponse(data)


@login_required
@instance_perm
def handlers(request, ai_id):
    json_serializer = serializers.get_serializer("json")()
    data = json_serializer.serialize(Handler.objects.filter(ai = ai_id).order_by('priority'), ensure_ascii = False)
    logger.debug(request)
    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
@instance_perm_per_obj('ai')
def save_call(request):
    for call in serializers.deserialize('json',request.POST['json']):
        call.save()
    logger.debug(request)
    return HttpResponse("ok")

@login_required
@csrf_exempt
@require_POST
@instance_perm_per_obj('ai')
def save_handlers(request):
    logger.debug(request)
    ai = None
    for obj in serializers.deserialize('json', request.POST['json']):
        ai = obj.object.ai
        break

    for handler in Handler.objects.filter(ai = ai):
        handler.delete()
    
    for obj in serializers.deserialize('json', request.POST['json']):
        obj.save()
        logger.debug('num: ' + str(obj.object.num) + ' priority: ' + str(obj.object.priority), request=request)
    return HttpResponse("ok")

@login_required
@instance_perm
def data_timewise(request, ai_id):
    logger.debug(request)
    calls = Call.objects.filter(ai = ai_id)
    bins = {}
    for i in range(24):
        bins[i] = 0
    for call in calls:
        bins[call.media.time.hour] += 1
    return HttpResponse(json.dumps(bins))

@login_required
@instance_perm
def delete_record(request, ai_id, recording_id):
    
    logger.debug(request)
    if ai_id and recording_id:        
        ai_id = int(ai_id)
        recording_id = int(recording_id)
        try:
            Call.objects.get(ai=ai_id, id=recording_id).delete()
        except:
            logger.exception('Encountered exception while deleting ai_id: %d, id: %d' % (ai_id, recording_id)) 
    return HttpResponse("ok")