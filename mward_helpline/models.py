from django.db import models

from vapp.app_manager.models import App_instance, CalleridField
from vapp.media.models import Recording
from vapp.data.models import Eicher_delhi
from vapp.utils import datetimefield_to_unix


CALLERID_LENGTH = 20

class Call(models.Model):
    URGENCY_CHOICES = (
        ('lo', 'Low'),
        ('me', 'Medium'),
        ('hi', 'High'),
    )

    ai       = models.ForeignKey(App_instance,related_name='mw_hl_ai')
    media    = models.ForeignKey(Recording)
    callerid = CalleridField(max_length = CALLERID_LENGTH)
    location = models.ForeignKey(Eicher_delhi, null = True)
    handler  = models.CharField(max_length = CALLERID_LENGTH)
    urgency  = models.CharField(max_length = 2, choices = URGENCY_CHOICES)
    address  = models.CharField(max_length = 80)
    comment  = models.CharField(max_length = 100)

    def time(self):
        return datetimefield_to_unix(self.media.time)

    def __unicode__(self):
        return str(self.ai.id) + "_" + unicode(self.callerid) + "_" + str(self.media.time) + "_" + unicode(self.handler) 

class Handler(models.Model):
    RECORDER = "__RECORDER__"

    ai = models.ForeignKey(App_instance)
    num = CalleridField(max_length = CALLERID_LENGTH)
    name = models.CharField(max_length = 32)
    priority = models.IntegerField()

    def __unicode__(self):
        return str(self.ai.id) + "_" + unicode(self.num) + "_" + unicode(self.name) + "_" + str(self.priority)
