# -*- coding: utf-8 -*-
import time as time_module
from datetime import *
import vapp.settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail.message import EmailMessage
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.files import File
from django.forms.models import model_to_dict

from boto3.dynamodb.conditions import Key, Attr
from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError
from django.core.serializers.json import DjangoJSONEncoder

from log import get_request_logger
import os, stat, mad
from pwd import getpwnam 
from xlwt import *
import xlwt
import xlrd
import csv
import xlsxwriter
import uuid
import subprocess
import settings
import local_settings
import psutil

from collections import OrderedDict
from random import randint
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import *
from operator import itemgetter
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

logger = get_request_logger()
from random import choice
from string import ascii_lowercase
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
import json
from PIL import Image
import os
import dialogflow_v2 as dialogflow
from subprocess import Popen

import io
import sys

import pandas as pd

import argparse

from typing import Tuple
import wave



os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'gv-survey-1-ca0ec2da85ce.json'

DIALOGFLOW_PROJECT_ID = 'gv-survey-1'
DIALOGFLOW_LANGUAGE_CODE = 'hi'
CHUNK_SIZE = 8192
SAMPLE_RATE_HERTZ = 8000

from mutagen.mp3 import MP3



ALPHABETS = ['%c' % i for i in range(65, 91)]
AUTO_GEN_MAIL_TEXT = 'Please do not reply to this mail as this is automated mail service. For any queries write to us at techsupport@oniondev.com'
LINE_BREAK = '<br /><br /><br /><br />'
COST_PER_SECOND = 0.0075
DEFAULT_ALERTS_RECIPIENT = 'techsupport@oniondev.com'
DATE_FORMATS = ['%Y-%m-%dT%H:%M:%S', '%Y-%m-%d']

EMPTY_ROW = [ '' ]

def integer_to_reference(value, alpha=2, num=5):
    alpha_prefix = ""
    while True:
        base = (26 ** (alpha - 1)) * (10 ** num)
        alpha_prefix += ALPHABETS[value / base]
        value = value % base
        alpha -= 1
        if alpha == 0:
            break
    return ("%s%0" + str(num) + "d") % (alpha_prefix, value)

def datetimefield_to_unix(field):
    return int(time_module.mktime(field.timetuple()))

def datetimefield_to_seconds(field):
    return int( time_module.mktime(field.timetuple() ) + 1e-6 * field.microsecond ) * 1000


def datestr_to_datetime(date):
    for date_format in DATE_FORMATS:
        try:
            return datetime.strptime(date, date_format)
        except ValueError:
            pass
    return None


from django.core import serializers
def get_deserialized_obj(request):
    vals = [obj for obj in serializers.deserialize('json', request.POST['json'])]
    return vals[0]

def get_deserialized_list(request):
    return [obj for obj in serializers.deserialize('json', request.POST['json'])]

def is_valid_num(num):
    try:
        int(num)
        if len(num) > 3:
            return True
        else:
            return False
    except:
        return False

from datetime import datetime, timedelta, date

def get_upcoming_datetime(hr, min):
    now = datetime.now()
    if now.hour < hr or now.hour == hr and now.minute < min:
        return now.replace(hour=hr).replace(minute=min)
    else:
        return datetime.fromordinal(now.toordinal() + 1).replace(hour=hr).replace(minute=min)

def get_total_seconds(td): 
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6) / 1e6

def daterange(begin, end, delta=timedelta(days=1)):
    """Form a range of dates and iterate over them.

    Arguments:
    begin -- a date (or datetime) object; the beginning of the range.
    end   -- a date (or datetime) object; the end of the range.
    delta -- (optional) a timedelta object; how much to step each iteration.
             Default step is 1 day.

    Usage:

    """
    if not isinstance(delta, timedelta):
        delta = timedelta(delta)

    ZERO = timedelta(0)

    if begin < end:
        if delta <= ZERO:
            raise StopIteration
        test = end.__gt__
    else:
        if delta >= ZERO:
            raise StopIteration
        test = end.__lt__

    while test(begin):
        yield begin
        begin += delta

def csv_to_dict(string, assignment_separator = ':'):
    dic = {} #:P

    if not string:
        return dic

    for kv_pair in [ kv.strip() for kv in string.split(',') ]:
        k, v = kv_pair.split(assignment_separator)
        dic[k] = v

    return dic

def bool_str_to_bool(string):
    try:
        return bool(int(string))
    except:
        return None

def date_str_to_date(date_str):
    try:
        arr = date_str.split('-')
        return date(int(arr[0]), int(arr[1]), int(arr[2]))
    except:
        return None

def is_thursday(date):
    return date.weekday() == 3

def get_start_of_week(date):
    return date - timedelta(days = date.weekday())

def is_start_of_week(date):
    return date.weekday() == 0

def get_start_of_month(date):
    return date.replace(day = 1)

def is_start_of_month(date):
    return date.day == 1

def send_html_email(to_emails, from_email, subject, template_file, template_args):
    html = render_to_string(template_file, template_args)
    text = strip_tags(html)

    msg = EmailMultiAlternatives(subject, text, from_email, to_emails)
    msg.attach_alternative(html, "text/html")
    msg.send()


def send_email(subject, body, to_emails, attachments = None, max_tries = 3):
    email = EmailMessage()
    email.subject = subject
    email.body = body + '<br /><br /><br /><br />' + '<small>' + AUTO_GEN_MAIL_TEXT + '</small>'
    email.from_email = vapp.settings.EMAIL_HOST_USER
    to_emails = list(to_emails)
    email.to = to_emails
    email.content_subtype = "html"

    if attachments:
        if not isinstance(attachments, list):
            attachments = [attachments]
        for attachment in attachments:
            email.attach_file(attachment)

    tries_left = max_tries
    while tries_left > 0:
        try:
            email.send()
            logger.info('Mail sent to %s: ' % to_emails)
            break
        except Exception, e:
            logger.exception("Could not send email to %s because %s" % (to_emails, e))
            tries_left -= 1
            if tries_left:
                time_module.sleep(300)


def change_file_owner(filepath, username):
    fd = os.open(filepath, os.O_RDONLY )
    user_detail = getpwnam(username)
    os.fchown( fd, user_detail.pw_uid, user_detail.pw_gid)
    os.close(fd)

def change_file_permissions(filepath, permission):
    fd = os.open(filepath, os.O_RDONLY)
    os.fchmod(fd, permission)
    os.close(fd)

def mp3_duration(filepath):
    if not filepath or not os.path.exists(filepath):
        return 0

    file_reader = mad.MadFile(filepath)
    return file_reader.total_time()

def populate_spreadsheet(sheet, data, conditional_formats = None, applicable_columns = None, header_row_count = 1):
    if isinstance( sheet, xlwt.Worksheet ):
        return populate_spreadsheet_xlwt(sheet, data)

    row_count = len( data )
    max_col_widths = {}
    
    row_num = 0
    for row in data:
        col_num = 0
        for cell in row:
            val = str(cell) if isinstance(cell, date) or isinstance(cell, datetime) else cell
            if ( not max_col_widths.has_key( col_num ) ) or ( max_col_widths[ col_num ] < len(str(val)) ):
                max_col_widths[ col_num ] = len( str(val) ) 
            sheet.write( row_num, col_num, val )
            col_num += 1
        row_num += 1

    if conditional_formats:
        for format in  conditional_formats:
            if not applicable_columns:
                sheet.conditional_format( 0, 0, col_num - 1, 1 , format)
            else:
                for column in applicable_columns:
                    sheet.conditional_format(column + str(header_row_count) + ":" + column + str(row_num), format)
        
    for col_no, col_width in max_col_widths.items():
        sheet.set_column( 0, col_no, col_width )
    return sheet


def populate_spreadsheet_xlwt(sheet, data):
    spreadsheet = sheet
    spreadsheet.row(0).height = 500
    col_width = spreadsheet.col(0).width
    
    max_col_widths = {}
    row_count = 0
    for row in data:
        col_count = 0
        for cell in row:
            max_col_widths[col_count] = col_width
            curr_cell_width = 256 * len(unicode(cell))
            if curr_cell_width > max_col_widths[col_count]:
                max_col_widths[col_count] = curr_cell_width
            spreadsheet.col(col_count).width = max_col_widths[col_count]
            val = str(cell) if isinstance(cell, date) or isinstance(cell, datetime) else cell
            spreadsheet.write(row_count,col_count,val)
            col_count += 1
        row_count += 1

    return spreadsheet


def freeze_row_col(sheet, col = 0, row = 1):
    if isinstance(sheet, xlwt.Workbook):
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.vert_split_pos = col
        sheet.horz_split_pos = row
    elif isinstance(sheet, xlsxwriter.worksheet.Worksheet):
        sheet.freeze_panes( row, col )
        sheet.split_panes( row, col )
    return sheet

'''
Assumes an array of dictionaries of the form:
[{
 'name': 'Name of Sheet',
 'header_row_count': 1,
 'data': [[row 1][row 2]...[row n]],
}]
One dictionary per sheet in the workbook.
'''
def generate_workbook( wb_data, output_file = None, in_memory = False, options = None ):
    if in_memory:
        output = StringIO.StringIO()
        book = xlsxwriter.Workbook( output )  
    else:
        book = xlsxwriter.Workbook( output_file )

    if options:
        formats = options.get('formats', None)
        if formats:
            for name, format in formats.iteritems():
                formats[ name ] = book.add_format( format )
            conditions = options.get('conditions', None)
            if conditions:
                for condition in conditions:
                    condition['format'] = formats[ condition['format'] ]

                conditional_formats = options.get('conditions', None)
    else:
        conditional_formats = None

    for sh_data in wb_data:
        #xlwt requires sheet name to be of maximum 31 characters
        sheet = book.add_worksheet(sh_data['name'][:30])
        header_row = sh_data.get('header_row_count', 1)
        freeze_row_col(sheet, row = header_row)
        apply_to = sh_data.get('apply_format_to', None)
        populate_spreadsheet(sheet, sh_data['data'], conditional_formats = conditional_formats, applicable_columns = apply_to, header_row_count = header_row )
    book.close()
    
    if in_memory:
        output.seek(0)
        return output
    
    return True


'''
Assumes an array of dictionaries of the form:
[{
 'name': 'Name of Sheet',
 'header_row_count': 1,
 'data': [[row 1][row 2]...[row n]],
}]
One dictionary per sheet in the workbook.
'''
def generate_workbook_xlwt(wb_data, output_file):
    book = xlwt.Workbook(encoding ='utf-8')
    for sh_data in wb_data:
        #xlwt requires sheet name to be of maximum 31 characters
        sheet = book.add_sheet(sh_data['name'][:30], cell_overwrite_ok = True)
        freeze_row_col(sheet, row = sh_data.get('header_row_count', 1))
        populate_spreadsheet(sheet, sh_data['data'])
    book.save(output_file)
    return True

def generate_attachment(data, output_file, use_multiple_sheets = False, sheet_names = None, freeze_req = False, freeze_order = None, in_memory = False):
    
    if in_memory:
        output = StringIO.StringIO()
        book = xlsxwriter.Workbook( output )
    else:
        book = xlsxwriter.Workbook( output_file )

    if use_multiple_sheets:
        data_list = data
        count = 0

        for elem in data_list:
            if sheet_names and sheet_names[count]:
                sheet = book.add_worksheet(sheet_names[count][:30])
                if freeze_req:
                    if freeze_order and freeze_order[count]:
                        sheet = freeze_row_col(sheet, freeze_order[count][0], freeze_order[count][1])
                    else:
                        sheet = freeze_row_col(sheet)
            else:
                sheet = book.add_worksheet('Sheet ' + str(count))
            sheet = populate_spreadsheet(sheet, elem)
            count += 1
    else:
        sheet = book.add_worksheet('Sheet 1')
        sheet = populate_spreadsheet(sheet, data)
    book.close()
    if in_memory:
        output.seek(0)
        return output

def generate_attachment_xlwt(data, output_file, use_multiple_sheets = False, sheet_names = None, freeze_req = False, freeze_order = None):
    book = xlwt.Workbook(encoding ='utf-8')
    if use_multiple_sheets:
        data_list = data
        count = 0

        for elem in data_list:
            if sheet_names and sheet_names[count]:
                sheet = book.add_sheet(sheet_names[count][:30], cell_overwrite_ok = True)
                if freeze_req:
                    if freeze_order and freeze_order[count]:
                        sheet = freeze_row_col(sheet, freeze_order[count][0], freeze_order[count][1])
                    else:
                        sheet = freeze_row_col(sheet)
            else:
                sheet = book.add_sheet('Sheet ' + str(count), cell_overwrite_ok = True)
            sheet = populate_spreadsheet(sheet, elem)
            count += 1
    else:
        sheet = book.add_sheet('Sheet 1', cell_overwrite_ok = True)
        sheet = populate_spreadsheet(sheet, data)

    book.save(output_file)

    return True


def get_ratio(num, den, exact = False):
    if den == 0:
        ratio = 0.0
    else:
        ratio = float(num) / den
        if not exact:
            ratio = int(ratio*100)/100.0
            
    return ratio


def all_same(items): return all(x == items[0] for x in items)

def get_date_from_str(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d').date()


def get_start_end_from_month_year(month, year):
    start = datetime(year, month, 1)
    end = start + relativedelta(months = +1)
    return (start, end)


def get_nth_date_from_date( date, n ):
    return date - timedelta( days=n )

def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None

def load_vapp_module(package_name, mod_name):
    mod = None
    base_pkg_name= "vapp"
    try:
        base_pkg = __import__(base_pkg_name + '.' + package_name + '.' + mod_name)
        mod = getattr(getattr(base_pkg, package_name), mod_name)
        reload(mod)
    except Exception,e:
        logger.exception("Exception in loading module %s.%s. \n%s" % (package_name, mod_name, e))

    return mod


def load_vapp_module_for_ai(ai, mod_name):
    package_name = str(ai.app.pkg_name)
    return load_vapp_module(package_name, mod_name)


def load_vapp_stats(app_instance = None, pkg_name = None):
    mod_name = 'stats'
    if app_instance:
        return load_vapp_module_for_ai(app_instance, mod_name)
    else:
        return load_vapp_module(pkg_name, mod_name)


def get_py_obj( name ):
    tokens = name.split( '.' )
    loaded_object = __import__( '.'.join( tokens[:-1] ) )
    for sub_module in tokens[1:]:
        loaded_object = getattr( loaded_object, sub_module )
    return loaded_object

# sorts a 2-D matrix, on col_index_sort
def get_sorted_data( data, col_index_sort, rev_sort_req=True ):
    return sorted( data, key=itemgetter( col_index_sort ), reverse=rev_sort_req )

def get_median(list):
    sorts = sorted(list)
    length = len(sorts)
    if not length % 2:
        return float((sorts[length / 2] + sorts[length / 2 - 1])) / 2.0
    return sorts[length / 2]

def get_ai_state_transition_string(ai, state):
    if ai and state:
        return '@ai_' + str(ai.id) + '_' + str(state)
    return None

def get_ai_state_from_transition_string(transition_string):
    ai_id, state = (None, None)
    if transition_string:
        start_str, ai_id, state = transition_string.split('_')
        if ai_id.isdigit():
            ai_id = int(ai_id)
    return (ai_id, state)

# Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
class CellTypes:
    EMPTY = 0
    TEXT = 1
    NUMBER = 2
    DATE = 3
    BOOL = 4
    ERROR = 5
    BLANK = 6

'''
    should remove the empty lines in the middle if any
    format expected in return
    [
        ['field1', 'field2'....],
        ['val11', 'val12'....],
        ['val21', 'val22'....],
        .
        .
        .
    ]
    '''
def read_rows_from_file( filepath, file_format ):
    if file_format in ['xls', 'xlsx' ]:
        return read_rows_from_xls( filepath )
    elif file_format == 'csv':
        return read_rows_from_csv( filepath )

def read_rows_from_xls( filepath, sheet_no = 0 ):
    try:
        workbook = xlrd.open_workbook( filepath )
        worksheet = workbook.sheets()[ sheet_no ]
        output = []
        num_rows = worksheet.nrows
        num_cols = worksheet.ncols

        for row in range( num_rows ):
            curr_row = worksheet.row( row )
            row_list = [ cell.value for cell in curr_row if cell.ctype not in [ CellTypes.EMPTY, CellTypes.ERROR, CellTypes.BLANK ] ]
            output.append( row_list )    
        return output
    except Exception,e:
        logger.exception('exception while reading rows from xls: %s' %(e))
    return None

def read_rows_from_csv( filepath, delimiter = ',' ):
    file_rows = []
    with open(filepath, 'rb') as f:
        reader = csv.reader( f, delimiter= delimiter )
        for row in reader:
            file_rows.append( row )
    return file_rows

def load_object(name):
        tokens = name.split('.')
        loaded_object = __import__( '.'.join( tokens[:-1] ) )
        for sub_module in tokens[1:]:
            loaded_object = getattr( loaded_object, sub_module )
        return loaded_object


def create_dirs_path( filepath ):
    directory = os.path.dirname(filepath)
    if not os.path.exists(directory):
        os.makedirs(directory)


def ffmpeg_modify_mp3(input_mp3, output_mp3 = None, sample_rate = '16000',  ac = '1', bit_rate = '16k', overwrite = True ):
    audio = MP3(input_mp3)
    if audio.info.bitrate > 16000 or audio.info.mode != 3:
        try:
            if not output_mp3:
                output_mp3 = ( '/tmp/%s.mp3' %( uuid.uuid1() ) )
            ffmpegString = ( "ffmpeg -i %s -y -ar %s -ac %s -b:a %s %s" %(input_mp3, sample_rate, ac, bit_rate, output_mp3 ) )
            retval = subprocess.call(ffmpegString.strip().split(' '))
            if os.path.isfile(output_mp3):
                return output_mp3
        except OSError, o:
            logger.error('OSError: ' + str(o))
        except ValueError:
            logger.error("ValueError: Couldn't call FFMPEG with these parameters")
        except Exception,e:
            logger.exception( 'exception while modifying mp3: %s ' %(str(e)) )
        return None
    else:
        return input_mp3

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def generate_random_user( username_len = 8, max_tries = 10 ):
    new_user = None
    tries = 0
    while ( not ( new_user or tries > max_tries ) ):
        try:
            new_username = ''.join(choice(ascii_lowercase) for i in range(username_len))
            logger.debug('new_user: %s, %s' %(new_user, new_username ))
            new_email = ( '%s@gmail.com'%(new_username) )
            new_user = User.objects.create_user( username = new_username, password = new_username, email = new_email )
        except Exception,e:
            logger.debug('exception:%s'%(e))
            tries += 1
    if not new_user:
        raise Exception('unable to generate random user')
    return new_user

class Moderation_source:
    IVR = 'IVR'
    WEB = 'WEB'


def is_english(message):
    if not message:
        return True
    try:
        message.encode('ascii')
    except UnicodeEncodeError:
        return False
    return True


def get_ordered_distinct_list(input_list):
    seen = set()
    seen_add = seen.add
    return [x for x in input_list if not (x in seen or seen_add(x))]
    
    
def if_dict_values_empty(dict):
    for key,value in dict.iteritems():
        if value:
            return False
    return True

def if_dict_value_exist_by_key(key, dict):
    try:
        value = dict[key]
        return True
    except KeyError:
        return False


def get_formated_datetime(start, end):
    fmt = '%Y-%m-%d %H:%M:%S'
    timediff = ((datetime.strptime(str(end), fmt)) - datetime.strptime(str(start), fmt))
    return str(timediff)

def upload_a_file_tg_group(filepath, schedule):
    local_file = open(filepath)
    djangofile = File(local_file)
    schedule.prompt_file.save('new_announcement.mp3', djangofile)
    local_file.close()

def convert_model_to_dict(model):
    try:
        return model_to_dict(model)
    except:
        return None


def get_dnd_status(call_log):
    try:
        from pymongo import MongoClient
        try:
          connection = MongoClient(settings.MONGO_URL)
        except:
           send_email('MongoDb Alert:', 'Team, Please check mongodb. Dialout will hamper', ['techsupport@oniondev.com'])
        db = connection.gvdbf
        n = call_log.number[2:]
        record = db.dnd.count_documents({'Phone Numbers':int(n)})
        print record
        if record > 0:
           return True
        else:
           return False

    except Exception as e:
        logger.error('Unable to connect mongodb'+str(e))



def create_dynamodb_connection():
    import boto3
    client = boto3.resource('dynamodb')
    return  client

def push_to_mongo(model_obj, app_name, model_name = None):
    if not model_name:
       model_name =  model_obj._meta.object_name
    else:
       model_name = model_name
    try:
       obj_dict = model_to_dict(model_obj)
    except:
       obj_dict = model_obj
    logger.info(str(obj_dict))
    import requests
    url = 'http://x7tapk1v46.execute-api.ap-northeast-1.amazonaws.com/dev/post/'
    obj_dict['app_name'] = app_name
   
    headers = {"Content-Type": "application/json"}
    params = {'model_name':model_name}
    logger.info(str(obj_dict))
    response = requests.post(url, params=params, data=json.dumps(obj_dict, cls=DjangoJSONEncoder), headers=headers)    
    logger.info(str(response))
   

def delete_from_dynamo(primary_key, primary_key_value, model_name = None, model_obj = None):
    if not model_name:
       model_name =  model_obj._meta.object_name
    else:
       model_name = model_name
    
    import requests
    url = 'http://x7tapk1v46.execute-api.ap-northeast-1.amazonaws.com/dev/delete/'


    headers = {"Content-Type": "application/json"}
    params = {'model_name':model_name ,'primary_key':primary_key, 'primary_key_value':primary_key_value}
    try:
      response = requests.post(url, params=params, headers=headers)
      logger.info(str(response))
    except Exception as e:
      logger.info(str(e))


def update_to_dynamo(model_name, pkey_value, skey_value, value_dict):
    key_dict = {}
    exp_dict = {}
    key_dict['pkey'] = pkey_value
    key_dict['skey'] = skey_value
    db = create_dynamodb_connection()
    table = db.Table(model_name)
    # updateexpression = "SET " +str(query_params['attr_name'] +" = :val1")
    # print (updateexpression)
    # exp_dict[':val1'] = query_params['attr_value']
    add_list = []
    exp_val_dict = {}
    exp_name_dict = {}
    query_params = json.loads(value_dict)
    for key, value in query_params.iteritems():
        if (value == None or value == ''):
            print ('Skipping none entry')
        else:
            s =  '#'+key+' = :'+key
            add_list.append(s)
            key_col = ":"+key
            exp_key = "#"+key
            exp_val_dict.update({key_col:value})
            exp_name_dict.update({exp_key:key})
    updateexpression = "SET "+",".join(add_list)
    response = table.update_item(
               Key=key_dict,
               UpdateExpression=updateexpression,
               ExpressionAttributeValues=exp_val_dict,
               ExpressionAttributeNames=exp_name_dict,
               ReturnValues="UPDATED_NEW"
               )
    if response['ResponseMetadata']['HTTPStatusCode'] == 200 and 'Attributes' in response:
        logger.info("Updated DynamoDB for : "+str(key_dict))




def delete_obj_in_dynamo(model_name, pkey_value, skey_value):
    key_dict = {}
    key_dict['pkey'] = pkey_value
    key_dict['skey'] = skey_value
    db = create_dynamodb_connection()
    table = db.Table(model_name)
    table.delete_item(Key = key_dict)
    logger.info('Item is deleted, delete_dict: '+ str(key_dict))

 
def upload_to_s3(news):
    import boto3
    s3_client = boto3.client('s3')
    try:
        if news.detail:
            file_name = str(news.detail.id)+'_'+str(datetime.now())+'.mp3'
            logger.info(str(file_name))
            s3_audio_url  = 'https://gramvaani.s3.ap-south-1.amazonaws.com/audios/'+str(file_name)
            try:

                response = s3_client.upload_file(news.detail.get_full_filename(), 'gramvaani/audios/', file_name)
                sync_log = S3_sync_log(sync_type = 'Audio', news=news, success=True)
                sync_log.save()
            except Exception as e:
                sync_log = S3_sync_log(sync_type='Audio', news=news, success=False, exception=str(e))
                sync_log.save()
                logger.info(str(e))


            news.misc['s3_audio_url'] =  s3_audio_url
            news.misc['s3_audio_modified_time'] = datetime.now()
        if news.sm_image:
            img_file_name = str(datetime.now())+'_'+str(news.sm_image.name)
            try:

                response = s3_client.upload_file(news.sm_image.get_location(), 'gramvaani', img_file_name)
                sync_log = S3_sync_log(sync_type = 'Image', news=news, success=True)
                sync_log.save()
            except Exception as e:
                logger.info(str(e))
                sync_log = S3_sync_log(sync_type='Image', news=news, success=False, exception=str(e))
                sync_log.save()

            news.misc['s3_image_url'] = img_file_name
            news.mist['s3_image_modified_time'] = datetime.now()
        news.save()



    except ClientError as e:
        logger.error(e)
def push_to_s3(local_file_path, bucket_name, s3_file_path, cType):
    import boto3
    s3_client = boto3.client('s3')
    try:
        s3_client.upload_file(local_file_path, bucket_name, s3_file_path, ExtraArgs={'ContentType': cType})
        exception = ''
        s3_url = 'https://gramvaani.s3.ap-south-1.amazonaws.com/'+str(s3_file_path)
        logger.info(str(s3_url))
        return True, s3_url, exception
    except Exception as e:
        logger.info(str(e))
        s3_url = ''
        return False, s3_url, e


def convert_image(filepath, file_identifier):
    img = Image.open(filepath)
    img_format = img.format
    hdpi_im_key = file_identifier+'_hdpi.'+img_format
    im_hdpi_file = '/tmp/'+hdpi_im_key
    width_0, height_0 = img.size
    hdpi_fixed_width_in_pixel = 480
    wpercent = hdpi_fixed_width_in_pixel / float(width_0)
    hsize = int(float(height_0) * float(wpercent))
    img.resize((hdpi_fixed_width_in_pixel, hsize), Image.ANTIALIAS).save(im_hdpi_file)
    return im_hdpi_file, hdpi_im_key


def get_file_creationtime(filepath):
    import os
    t = os.path.getmtime(filepath)
    return datetime.fromtimestamp(t)

def mp3_to_wav(filename):
    if filename.endswith(".mp3"):  # or .avi, .mpeg, whatever.
        [a, b] = filename.split('.')
        p = Popen([
            'ffmpeg -hide_banner -loglevel panic -i ' + filename + ' -acodec pcm_s16le -ac 1 -ar 16000 ' + a + '.wav'.format(
                filename)], shell=True)
        p.wait()


def has_handle(fpath):
    count = 0
    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid', 'name', 'cmdline'])
            print(pinfo)
            for item in proc.open_files():
                print "item path open : %s" %(item.path) 
                if fpath == item.path:
                    count = count + 1
                    if count == 2:
                        return True
                return True        
        except Exception:
            pass

    return False


def get_df_intent_from_stream(audio_file_path, cdr_id, context_name = None, language = None, phrase_hint_list = []):
    session_client = dialogflow.SessionsClient()
    audio_encoding = dialogflow.enums.AudioEncoding.AUDIO_ENCODING_LINEAR_16
    session_id = str(cdr_id)+'_'+str(randint(0,100))
    session_path = session_client.session_path(DIALOGFLOW_PROJECT_ID, session_id)
    logger.info('context = ',context_name)
    context=dialogflow.types.context_pb2.Context(name=session_path + '/contexts/'+context_name,lifespan_count=1)
    logger.info('Session path: {}\n'.format(session_path))
    
    
    def request_generator(audio_config, audio_file_path, query_parameter):
        query_input = dialogflow.types.QueryInput(audio_config=audio_config)
        yield dialogflow.types.StreamingDetectIntentRequest(
            session=session_path, query_input=query_input, query_params = query_parameter)
        check=0
        end_file_path, ext = os.path.splitext(audio_file_path)
        end_file_path = end_file_path+'.end'
        with open(audio_file_path, 'rb') as audio_file:
            while True: 
                chunk = audio_file.read(CHUNK_SIZE)
                date_time = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                logger.info("read %s of bytes and time : %s" %(len(chunk), date_time))
                if not chunk:
                    check = check+1
                    if os.path.exists(end_file_path) or check == 14:
                        logger.info("Finished")
                        #time_module.sleep(0.3)
                        try:
                            os.remove(end_file_path)
                        except OSError:
                            pass    
                        break	
                    time_module.sleep(0.5)                  
                else:
                    check = 0
                    time_module.sleep(0.05)
                    yield dialogflow.types.StreamingDetectIntentRequest(
                            input_audio=chunk)

    audio_config = dialogflow.types.InputAudioConfig(
        audio_encoding=audio_encoding, language_code=language,
        sample_rate_hertz=SAMPLE_RATE_HERTZ, phrase_hints = phrase_hint_list)
    query_parameter = dialogflow.types.QueryParameters(contexts=[context])

    requests = request_generator(audio_config, audio_file_path, query_parameter)
    responses = session_client.streaming_detect_intent(requests)

    logger.info('=' * 20)
    for response in responses:
        logger.info('Intermediate transcript: "{}".'.format(
                response.recognition_result.transcript.encode('utf-8')))

    # Note: The result from the last response is the final transcript along
    # with the detected content.
    data, transcript = '', ''
    query_result = response.query_result
    transcript = query_result.query_text.encode('utf-8')
    logger.info('transcript'+str(transcript))

    logger.info('=' * 20)
    logger.info('Detected intent: {} (confidence: {})\n'.format(
        query_result.intent.display_name,
        query_result.intent_detection_confidence))
    logger.info('Fulfillment text: {}\n'.format(
        query_result.fulfillment_text.encode('utf-8')))
    if response.query_result.fulfillment_text:
        data = response.query_result.fulfillment_text.encode('utf-8')
    return data, transcript



def read_wav_file(filename):#-> Tuple[bytes, int]:
    w = wave.open(filename, 'rb')
    rate = w.getframerate()
    frames = w.getnframes()
    buffer = w.readframes(frames)
    w.close()
    return buffer, rate


def response_stream_processor(responses):
    print('interim results: ')

    transcript = ''
    final_transcript = []
    num_chars_printed = 0
    for response in responses:
        print("response: "+str(response))

        if not response.results:
            continue

        result = response.results[0]
        if not result.alternatives:
            continue

        transcript = result.alternatives[0].transcript
        logger.info("bala::",result.alternatives[0].transcript)
        
        if result.is_final:
            final_transcript.append(transcript)
        else:
            print('not final')

        print("final_transcript: "+str(final_transcript))        

    return final_transcript

# [START speech_transcribe_streaming]
def transcribe_streaming(stream_file):
    """Streams transcription of the given audio file."""
    import io
    from google.cloud import speech
    from google.cloud.speech import enums
    from google.cloud.speech import types

    #buffer, rate = read_wav_file(stream_file)
    client = speech.SpeechClient()

    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=8000,
        language_code="hi-IN",
    )
    streaming_config = types.StreamingRecognitionConfig(config=config, interim_results=True)
    print(streaming_config)

    def request_generator(streaming_config, stream_file):
        #yield types.StreamingRecognizeRequest(streaming_config=streaming_config)
        end_file_path, ext = os.path.splitext(stream_file)
        end_file_path = end_file_path+'.end'	
	logger.info("END FILE PATH : "+str(end_file_path))
        check=0
        with open(stream_file, 'rb') as audio_file:
            while True:
                chunk = audio_file.read(CHUNK_SIZE)
                date_time = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                logger.info("read %s of bytes and time : %s" %(len(chunk), date_time))
                if not chunk:
                    check = check+1
		    logger.info("CHECK : %s and END FILE PATH : %s" %(check, os.path.exists(end_file_path)))	
                    if os.path.exists(end_file_path) or check == 14:
                        logger.info("Finished")
                        #time_module.sleep(0.3)
                        try:
                            os.remove(end_file_path)
                        except OSError:
                            pass
                        break
                    time_module.sleep(0.5)
	        else:
                    check = 0
                    time_module.sleep(0.05)
                    yield types.StreamingRecognizeRequest(audio_content=chunk)

    requests = request_generator(streaming_config, stream_file)  # buffer chunk generator
    #requests = (types.StreamingRecognizeRequest(audio_content=chunk) for chunk in audio_generator)
    print(requests)
    # streaming_recognize returns a generator.
    # [START speech_python_migration_streaming_response]
    # responses = client.streaming_recognize(config=streaming_config, requests=requests)
    responses = client.streaming_recognize(config=streaming_config, requests=requests)

    
    # transcript =  response_stream_processor(responses)
    
    # [END speech_python_migration_streaming_request]
    return response_stream_processor(responses)


def get_jaccard_sim(a, b):
    c = a.intersection(b)
    return float(len(c)) / (len(a) + len(b) - len(c))


def get_qna_model_result(in_ques, relevant_questions, sanitized_question_ids, sanitized_questions):
    # ans_file = open("Ans.txt", "r", encoding='utf-8')
    # answers = [x.strip() for x in ans_file.read().split("\n")]

    # test_file = open("Test.txt", "r")
    # test = test_file.read().strip()
    in_ques = in_ques.strip()
    ques_response = {}
    logger.info('IN QUES:'+str(in_ques))
    # sw = ["अंदर","अत","अदि","अप","अपना","अपनि","अपनी","अपने","अभि","अभी","आदि","आप","इंहिं","इंहें","इंहों","इतयादि","इत्यादि","इन","इनका","इन्हीं","इन्हें","इन्हों","इस","इसका","इसकि","इसकी","इसके","इसमें","इसि","इसी","इसे","उंहिं","उंहें","उंहों","उन","उनका","उनकि","उनकी","उनके","उनको","उन्हीं","उन्हें","उन्हों","उस","उसके","उसि","उसी","उसे","एक","एवं","एस","एसे","ऐसे","ओर","और","कइ","कई","कर","करता","करते","करना","करने","करें","कहते","कहा","का","काफि","काफ़ी","कि","किंहें","किंहों","कितना","किन्हें","किन्हों","किया","किर","किस","किसि","किसी","किसे","की","कुछ","कुल","के","को","कोइ","कोई","कोन","कोनसा","कौन","कौनसा","गया","घर","जब","जहाँ","जहां","जा","जिंहें","जिंहों","जितना","जिधर","जिन","जिन्हें","जिन्हों","जिस","जिसे","जीधर","जेसा","जेसे","जैसा","जैसे","जो","तक","तब","तरह","तिंहें","तिंहों","तिन","तिन्हें","तिन्हों","तिस","तिसे","तो","था","थि","थी","थे","दबारा","दवारा","दिया","दुसरा","दुसरे","दूसरे","दो","द्वारा","न","नहिं","नहीं","ना","निचे","निहायत","नीचे","ने","पर","पहले","पुरा","पूरा","पे","फिर","बनि","बनी","बहि","बही","बहुत","बाद","बाला","बिलकुल","भि","भितर","भी","भीतर","मगर","मानो","मे","में","यदि","यह","यहाँ","यहां","यहि","यही","या","यिह","ये","रखें","रवासा","रहा","रहे","ऱ्वासा","लिए","लिये","लेकिन","व","वगेरह","वरग","वर्ग","वह","वहाँ","वहां","वहिं","वहीं","वाले","वुह","वे","वग़ैरह","संग","सकता","सकते","सबसे","सभि","सभी","साथ","साबुत","साभ","सारा","से","सो","हि","ही","हुअ","हुआ","हुइ","हुई","हुए","हे","हें","है","हैं","हो","होता","होति","होती","होते","होना","होने","मैं","मुझको","मेरा","अपने","आप को","हमने","हमारा","अपना","हम","आप","आपका","तुम्हारा","अपने","आप","स्वयं","वह","इसे","उसके","खुद","को","कि","वह","उसकी","उसका","खुद","ही","यह","इसके","उन्होने","अपने","क्या","जो","किसे","किसको","कि","ये","हूँ","होता","है","रहे","थी","थे","होना","गया","किया","जा रहा है","किया","है","है","पडा","होने","करना","करता","है","किया","रही","एक","लेकिन","अगर","या","क्यूंकि","जैसा","जब","तक","जबकि","की","पर","द्वारा","के","लिए","साथ","के","बारे","में","खिलाफ","बीच","में","के","माध्यम","से","दौरान","से","पहले","के","बाद","ऊपर","नीचे","को","से","तक","से","नीचे","करने","में","निकल","बंद","से","अधिक","तहत","दुबारा","आगे","फिर","एक","बार","यहाँ","वहाँ","कब","कहाँ","क्यों","कैसे","सारे","किसी","दोनो","प्रत्येक","ज्यादा","अधिकांश","अन्य","में","कुछ","ऐसा","में","कोई","मात्र","खुद","समान","इसलिए","बहुत","सकता","जायेंगे","जरा","चाहिए","अभी","और","कर","दिया","रखें","का","हैं","इस","होता","करने","ने","बनी","तो","ही","हो","इसका","था","हुआ","वाले","बाद","लिए","सकते","इसमें","दो","वे","करते","कहा","वर्ग","कई","करें","होती","अपनी","उनके","यदि","हुई","जा","कहते","जब","होते","कोई","हुए","व","जैसे","सभी","करता","उनकी","तरह","उस","आदि","इसकी","उनका","इसी","पे","तथा","भी","परंतु","इन","कम","दूर","पूरे","गये","तुम","मै","यहां","हुये","कभी","अथवा","गयी","प्रति","जाता","इन्हें","गई","अब","जिसमें","लिया","बड़ा","जाती","तब","उसे","जाते","लेकर","बड़े","दूसरे","जाने","बाहर","स्थान","उन्हें","गए","ऐसे","जिससे","समय","दोनों","किए","रहती","इनके","इनका","इनकी","सकती","आज","कल","जिन्हें","जिन्हों","तिन्हें","तिन्हों","किन्हों","किन्हें","इत्यादि","इन्हों","उन्हों","बिलकुल","निहायत","इन्हीं","उन्हीं","जितना","दूसरा","कितना","साबुत","वग़ैरह","कौनसा","लिये","दिया","जिसे","तिसे","काफ़ी","पहले","बाला","मानो","अंदर","भीतर","पूरा","सारा","उनको","वहीं","जहाँ","जीधर","के","एवं","कुछ","कुल","रहा","जिस","जिन","तिस","तिन","कौन","किस","संग","यही","बही","उसी","मगर","कर","मे","एस","उन","सो","अत"]
    scores = []
    word_list1 = in_ques.split(' ')

    if len(word_list1) <= 2:
        default_ids = list(OrderedDict.fromkeys(sanitized_question_ids))     # Get only unique IDs
        default_ids = [int(i) for i in default_ids if i != '']
        default_ids = default_ids[:3]   # take only top 3
        ques_response['success'] = False
	ques_response['similarity_score'] = 0
        ques_response['entity'] = [{'list_items':default_ids}]
        return ques_response

    # word_list1 = {w for w in word_list1 if not w in sw}
    word_list1 = set(word_list1)

    # start_time = time.time()
    for i in range(len(relevant_questions)):
        word_list2 = relevant_questions[i].split(' ')      
        word_list2 = set(word_list2)
        # word_list2 = {w for w in word_list2 if not w in sw}   
        similarity = get_jaccard_sim(word_list1, word_list2)
        scores.append((i, similarity))
    # print("{}s for jaccard similarity".format(time.time()-start_time))

    sorted_score = sorted(scores, key=lambda x: -x[1])

    if sorted_score[0][1] < 0:      # Currently there is no minimum threshold
        ques_response['similarity_score'] = 0  
        ques_response['success'] = False
    else:
        visited_sanitized_questions = set()
        top_san_ques_ids = []
        top_san_ques = []
        for idx in range(len(sorted_score)):
            if sanitized_question_ids[sorted_score[idx][0]] not in visited_sanitized_questions:
                visited_sanitized_questions.add(sanitized_question_ids[sorted_score[idx][0]])
                top_san_ques_ids.append(sanitized_question_ids[sorted_score[idx][0]])
                top_san_ques.append(sanitized_questions[sorted_score[idx][0]])

        # top_item_ids = [int(re.search(r'\d+', re.search(r'detail/\d+', url).group()).group()) for url in top_san_ques_ids[:3]]
        top_item_ids = [int(i) for i in top_san_ques_ids if i != '']
        top_item_ids = top_item_ids[:3]             # Take only top 3
        ques_response['entity'] = [{'list_items':top_item_ids}]
	ques_response['similarity_score'] = sorted_score[0][1]
        ques_response['success'] = True
        # ques_response['answer_urls'] = top_ans_urls[:3]
        # ques_response['sanitized_questions'] = top_san_ques[:3]

    return ques_response
