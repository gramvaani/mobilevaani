'''
Created on 07-Jan-2011

@author: bchandra
'''

LOG_DIR = '/var/tmp'
DJANGO_LOG_FILENAME = 'vapp_django.log'
CELERY_LOG_FILENAME = 'vapp_celery.log'
FREESWITCH_LOG_FILENAME= 'vapp_freeswitch.log'

LOG_FILENAME = '/tmp/vapp_django.txt'

import traceback
import logging
from logging import LoggerAdapter
from logging.handlers import RotatingFileHandler
import threading
import sys
import vapp.log
from django.core.handlers.wsgi import WSGIRequest

def get_logger():
    if hasattr(vapp.log, 'logger'):
        return vapp.log.logger
    else:       
        if not hasattr(sys, 'argv'):
            LOG_FILENAME = LOG_DIR + '/' + FREESWITCH_LOG_FILENAME
            LOG_NAME = "FREESWITCH"
        elif sys.argv[0] == 'mod_wsgi':
            LOG_FILENAME = LOG_DIR + '/' + DJANGO_LOG_FILENAME
            LOG_NAME = "DJANGO"
        else:
            LOG_FILENAME = LOG_DIR + '/' + CELERY_LOG_FILENAME
            LOG_NAME = "CELERY"
        
        #logging.basicConfig(level = logging.DEBUG)
        logger  = logging.getLogger(LOG_NAME)
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        file_handler = RotatingFileHandler(LOG_FILENAME, maxBytes = 1024*1024*5, backupCount = 5)
        file_handler.setFormatter(formatter)
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        logger.addHandler(console_handler)
        setattr(vapp.log, 'logger', logger)
        return vapp.log.logger

class RequestLogger(LoggerAdapter):
    def process(self, msg, kwargs):
        if type(msg) == str or type(msg) == unicode:
            if 'request' in kwargs:
                request = kwargs.pop('request')
                msg = request.get_full_path() + ' user: ' + str(request.user.id) + ': ' + str(msg)
            return msg, kwargs
        #if type(msg) == WSGIRequest:
        msg = msg.get_full_path() + ' user: ' + str(msg.user.id)
        return msg, kwargs
    
def get_request_logger():
    if hasattr(vapp.log, 'request_logger'):
        return vapp.log.request_logger
    else:
        vapp.log.request_logger = RequestLogger(get_logger(), None)
        return vapp.log.request_logger
    
class LoggerMiddleware:
    def process_exception(self, request, exception):
        logger = get_logger()
        logger.error('Encountered exception: %s', exception, exc_info=1)
    