from django.contrib import admin

from models import Schedule, Days, SP_weekly_ft, SET_function, SET_context

admin.site.register(Schedule)
admin.site.register(Days)

admin.site.register(SP_weekly_ft)
admin.site.register(SET_function)
admin.site.register(SET_context)

