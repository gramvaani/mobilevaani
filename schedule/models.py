from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from datetime import datetime, timedelta, time

from vapp.utils import get_py_obj
from vapp.log import get_request_logger

logger = get_request_logger()

REPETITION_CHOICES = (
    ('ONE', 'One Time'),
    ('DA','Daily'),
    ('WE','Weekly'),
    ('MO','Monthly'),
    ('DOW','Specific Days of Week'),
)

class Repetition_choices(object):
    ONE = 'ONE'
    DA = 'DA'
    WE = 'WE'
    MO = 'MO'
    DOW = 'DOW'

class Days(models.Model):
    DAYS_OF_WEEK = (
        ('0', 'Monday'),
        ('1', 'Tuesday'),
        ('2', 'Wednesday'),
        ('3','Thursday'),
        ('4', 'Friday'),
        ('5', 'Saturday'),
        ('6', 'Sunday'),
    )
    name = models.CharField(max_length = 1, choices = DAYS_OF_WEEK)

    def __unicode__(self):
        return self.DAYS_OF_WEEK[int(self.name)][1]


class Schedule(models.Model):
    repetition_help_text = "One Time: Schedule will start at 'start_date + start_time' and end at 'start_date + end_time'\n" +\
        "Daily: Schedule will occur daily from start_time to end_time between start_date and end_date\n" +\
        "Weekly: Schedule will occur every week from start_time to end_time on dayofweek(start_date)" +\
        "Monthly: Schedule will occur every month from start_time to end_time on dateofmonth(start_date)" +\
        "Specific days of week: Schedule will occur on selected days of every week from start_time to end_time"

    start_date = models.DateField(null = True, blank = True, help_text="Start date is included in schedule. Leave start date empty to start schedule from the beginning of time.")
    end_date = models.DateField(null = True, blank = True, help_text="End date is included in schedule. Leave end date empty to indicate that schedule will last till the end of time.")
    name = models.CharField(max_length=255)
    repetition = models.CharField(max_length = 3, choices = REPETITION_CHOICES, help_text = repetition_help_text)
    day_of_week = models.ManyToManyField(Days, null = True, blank = True)
    start_time = models.TimeField()
    end_time = models.TimeField()
    
    def is_schedule_on(self, atdatetime = None):
        if not atdatetime:
            atdatetime = datetime.now()

        if self.repetition == 'DA':
            if self.start_date and self.start_date > atdatetime.date():
                return False
            if self.end_date and self.end_date < atdatetime.date():
                return False
            return (self.start_time <= atdatetime.time() and self.end_time >= atdatetime.time())
        return False

    def __unicode__(self):
        return unicode(self.name) + " from " + unicode(self.start_date) + " to " + unicode(self.end_date) + ". Repeats: " + unicode(self.repetition)


"""
Schedule Pattern:
-----------------

A partial capture of datetime such that:

get_events_from( SP, DateTime ) => next DateTime.

Examples of SP:
---------------

Hourly: At the start of every hour.
Weekly Fixed Time: Certain days of the week at a fixed time. 
Weekly Variable Time: Certain days of the week with possibly different time for each day.
Monthly Fixed Day of Month: Every 1st day of month and every 25th,
Monthly Fixed Relative Day: Last Friday of the month.
Yearly Holidays: All national holidays.

Code:
-----

All classes implementing SP can derive from SPMixin, and implement get_events_from.
"""

class SPMixin( object ):
    MIN_INTERVAL = timedelta( seconds = 1 )

    def get_iter_after( self, start_time ):
        return self.__iter__( start_time )

    def get_events_after( self, start_time ):
        return self.get_events_from( start_time + self.MIN_INTERVAL )

    def __iter__( self, start_time = None ):
        start_time = start_time or datetime.now()
        return self.get_events_from( start_time )


class SP_weekly_ft( SPMixin, models.Model ):
    name = models.CharField( max_length = 128 )
    time = models.TimeField()

    d_mon = models.BooleanField( default = True )
    d_tue = models.BooleanField( default = True )
    d_wed = models.BooleanField( default = True )
    d_thu = models.BooleanField( default = True )
    d_fri = models.BooleanField( default = True )
    d_sat = models.BooleanField( default = True )
    d_sun = models.BooleanField( default = True )

    def get_events_from( self, start_time = None ):
        days = [ self.d_mon, self.d_tue, self.d_wed, self.d_thu, self.d_fri, self.d_sat, self.d_sun ]
        if not any( days ):
            raise StopIteration()
        start_time = start_time or datetime.now()
        i = start_time.weekday()
        days_count = 0
        while True:
            if days[ i ] and not ( days_count == 0 and start_time.time() > self.time ):
                yield datetime.combine( start_time + timedelta( days = days_count ), self.time )
            days_count += 1
            i = ( i + 1 ) % 7

    def __unicode__( self ):
        return '{0}_{1}_{2}'.format( self.id, unicode( self.time ), unicode( self.name ) )

# SET_context: Persisted info for the scheduled event task
# SET_function: Callback executed by the task

class SET_function( models.Model ):
    name      = models.CharField( max_length = 64 )
    func_name = models.CharField( max_length = 128 )

    def call( self, context_id ):
        try:
            if not hasattr( self, 'func' ):
                self.func = get_py_obj( self.func_name )
            self.func( context_id )
        except:
            logger.exception( "Unable to execute ST_function: id: {0} name: {1}".format( self.id, self.name ) )

    def __unicode__( self ):
        return '{0}_{1}_{2}'.format( self.id, unicode( self.name ), unicode( self.func_name ) )

class SET_context( models.Model ):
    # Schedule Pattern
    sp_type = models.ForeignKey( ContentType )
    sp_id   = models.PositiveIntegerField()
    sp      = generic.GenericForeignKey( 'sp_type', 'sp_id' )

    # Activity
    is_active  = models.BooleanField( default = True )
    start_time = models.DateTimeField()
    end_time   = models.DateTimeField( null = True, blank = True )
    prev_ts    = models.DateTimeField( null = True, blank = True )

    # Celery
    task_id = models.CharField( max_length = 36, blank = True )

    # Context
    context_func = models.ForeignKey( SET_function )
    context_id   = models.PositiveIntegerField( null = True, blank = True )

    def __unicode__( self ):
        return '{0}_{1}_{2}'.format( self.id, self.context_func.name, unicode( self.sp ) )

