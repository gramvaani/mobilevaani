from celery.task import Task
from celery.result import AsyncResult

from vapp.log import get_request_logger

from models import SET_context
from common import is_stc_disabled

logger = get_request_logger()

def dummy_sched_func( cid ):
    logger.debug( "Dummy sched func called with context id: {0}".format( cid ) )

def set_sched_inactive( stc ):
    if not isinstance( stc, SET_context ):
        stc = SET_context.objects.get( pk = stc )
    stc.is_active = False
    stc.task_id = ''
    stc.save()

def set_sched_active( stc ):
    if not isinstance( stc, SET_context ):
        stc = SET_context.objects.get( pk = stc )
    eta = iter( stc.sp ).next()
    result = ScheduledEventTask.apply_async( [ stc.id ], eta = eta )
    stc.task_id = result.task_id
    stc.save()
    logger.debug( "Scheduling event for stc_id: {0}, task_id: {1}, eta: {2}".format( stc.id, result.task_id, eta ) )
    return eta, result.task_id

def update_task( stc ):
    if not isinstance( stc, ST_context ):
        stc = ST_context.objects.get( pk = stc )

    if is_stc_disabled( stc ):
        if not stc.task_id:
            return False
        result = AsyncResult( stc.task_id )
        result.revoke()
        set_sched_inactive( stc )
        return True
    else:
        set_sched_active( stc )
        return True

class ScheduledEventTask( Task ):
    ignore_result = False

    def execute_callback( self, stc ):
        try:
            stc.context_func.call( stc.context_id )
        except:
            logger.exception( "Error in executing context for id: {0}, func: {1}".format( stc_id, stc.context_func.func_name ) )
        
    def run( self, stc_id ):
        stc = SET_context.objects.get( pk = stc_id )
        if is_stc_disabled( stc ):
            set_sched_inactive( stc )
            logger.debug( "Skipping scheduled event since disabled for stc: {0}".format( stc_id ) )
            return
        if stc.task_id != self.request.id:
            logger.debug( "Skipping scheduled event due to task id mismatch for stc: {0}".format( stc_id ) )
            return
        
        self.execute_callback( stc )

        set_sched_active( stc )




