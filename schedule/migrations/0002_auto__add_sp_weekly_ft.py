# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SP_Weekly_ft'
        db.create_table(u'schedule_sp_weekly_ft', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('time', self.gf('django.db.models.fields.TimeField')()),
            ('d_mon', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('d_tue', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('d_wed', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('d_thu', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('d_fri', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('d_sat', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('d_sun', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'schedule', ['SP_Weekly_ft'])


    def backwards(self, orm):
        # Deleting model 'SP_Weekly_ft'
        db.delete_table(u'schedule_sp_weekly_ft')


    models = {
        u'schedule.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['schedule.Days']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repetition': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'schedule.sp_weekly_ft': {
            'Meta': {'object_name': 'SP_Weekly_ft'},
            'd_fri': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_mon': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_sat': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_sun': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_thu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_tue': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_wed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'time': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['schedule']