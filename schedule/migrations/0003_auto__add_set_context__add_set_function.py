# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SET_context'
        db.create_table(u'schedule_set_context', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sp_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('sp_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('prev_ts', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('task_id', self.gf('django.db.models.fields.CharField')(max_length=36)),
            ('context_func', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schedule.SET_function'])),
            ('context_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'schedule', ['SET_context'])

        # Adding model 'SET_function'
        db.create_table(u'schedule_set_function', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('func_name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'schedule', ['SET_function'])


    def backwards(self, orm):
        # Deleting model 'SET_context'
        db.delete_table(u'schedule_set_context')

        # Deleting model 'SET_function'
        db.delete_table(u'schedule_set_function')


    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schedule.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['schedule.Days']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repetition': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'schedule.set_context': {
            'Meta': {'object_name': 'SET_context'},
            'context_func': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schedule.SET_function']"}),
            'context_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'prev_ts': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sp_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'sp_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {}),
            'task_id': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'schedule.set_function': {
            'Meta': {'object_name': 'SET_function'},
            'func_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'schedule.sp_weekly_ft': {
            'Meta': {'object_name': 'SP_weekly_ft'},
            'd_fri': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_mon': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_sat': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_sun': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_thu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_tue': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'd_wed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'time': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['schedule']