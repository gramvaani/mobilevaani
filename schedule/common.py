from djcelery.models import TaskMeta

from datetime import datetime

from models import SET_context

# For task revokes to survive restarts, take care of this:
# http://docs.celeryproject.org/en/latest/userguide/workers.html#worker-persistent-revokes

def is_stc_disabled( stc ):
    if not stc.is_active:
        return True
    return datetime.now() > stc.end_time


