# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FB_credential'
        db.create_table(u'fblib_fb_credential', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('app_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('app_api_key', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('app_secret', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('access_token', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'fblib', ['FB_credential'])

        # Adding model 'AI_FB_credential'
        db.create_table(u'fblib_ai_fb_credential', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_credential'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24)),
        ))
        db.send_create_signal(u'fblib', ['AI_FB_credential'])

        # Adding model 'FB_target'
        db.create_table(u'fblib_fb_target', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('object_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal(u'fblib', ['FB_target'])

        # Adding model 'AI_FB_target'
        db.create_table(u'fblib_ai_fb_target', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_target'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24)),
        ))
        db.send_create_signal(u'fblib', ['AI_FB_target'])

        # Adding model 'FB_wall_post'
        db.create_table(u'fblib_fb_wall_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_credential'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('object_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('posted_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('post_success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_target'])),
        ))
        db.send_create_signal(u'fblib', ['FB_wall_post'])

        # Adding model 'FB_wall_post_recording'
        db.create_table(u'fblib_fb_wall_post_recording', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('post', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_wall_post'])),
            ('recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'fblib', ['FB_wall_post_recording'])

        # Adding model 'FB_comment'
        db.create_table(u'fblib_fb_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_credential'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('object_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('posted_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('post_success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_wall_post'])),
        ))
        db.send_create_signal(u'fblib', ['FB_comment'])


    def backwards(self, orm):
        # Deleting model 'FB_credential'
        db.delete_table(u'fblib_fb_credential')

        # Deleting model 'AI_FB_credential'
        db.delete_table(u'fblib_ai_fb_credential')

        # Deleting model 'FB_target'
        db.delete_table(u'fblib_fb_target')

        # Deleting model 'AI_FB_target'
        db.delete_table(u'fblib_ai_fb_target')

        # Deleting model 'FB_wall_post'
        db.delete_table(u'fblib_fb_wall_post')

        # Deleting model 'FB_wall_post_recording'
        db.delete_table(u'fblib_fb_wall_post_recording')

        # Deleting model 'FB_comment'
        db.delete_table(u'fblib_fb_comment')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'fblib.ai_fb_credential': {
            'Meta': {'object_name': 'AI_FB_credential'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24'})
        },
        u'fblib.ai_fb_target': {
            'Meta': {'object_name': 'AI_FB_target'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_target']"})
        },
        u'fblib.fb_comment': {
            'Meta': {'object_name': 'FB_comment'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'post_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'posted_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_wall_post']"})
        },
        u'fblib.fb_credential': {
            'Meta': {'object_name': 'FB_credential'},
            'access_token': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'app_api_key': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'app_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'app_secret': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'fblib.fb_target': {
            'Meta': {'object_name': 'FB_target'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'fblib.fb_wall_post': {
            'Meta': {'object_name': 'FB_wall_post'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'post_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'posted_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_target']"})
        },
        u'fblib.fb_wall_post_recording': {
            'Meta': {'object_name': 'FB_wall_post_recording'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_wall_post']"}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['fblib']