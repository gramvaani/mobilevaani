'''
Created on 23-Mar-2011

@author: bchandra
'''

from celery.task import Task
from facebook import GraphAPI
from datetime import datetime

from models import FB_credential, FB_wall_post, FB_wall_post_recording, FB_comment
from vapp.log import get_logger

logger = get_logger()

class TargetType:
    FEED     = 'feed'
    COMMENTS = 'comments'
    
class WallPostTask(Task):
    
    @classmethod
    def create_post_feed(cls, ai_id, message, target_id, recording_id = None, recording_title = None):
        post         = FB_wall_post()
        post.cred_id = FB_credential.objects.get(ai_fb_credential__ai = ai_id, ai_fb_credential__role = 'default').id
        post.message = message
        post.target_id= target_id
        post.save()
        
        if recording_id:
            post_recording = FB_wall_post_recording(post_id = post.pk, recording_id = recording_id, title=recording_title)
            post_recording.save()
        
        WallPostTask.post_feed(post.pk)
        return post

    @classmethod
    def create_post_comment(cls, ai_id, message, target_id):
        comment         = FB_comment()
        comment.cred_id = FB_credential.objects.get(ai_fb_credential__ai = ai_id, ai_fb_credential__role = 'default').id
        comment.message = message
        comment.target_id = target_id
        comment.save()
        
        WallPostTask.post_comment(comment.pk)
        return comment
    
    @classmethod
    def post_feed(cls, post_pk):
        post_rec = None
        try:
            post_rec = FB_wall_post_recording.objects.get(post = post_pk)
        except:
            pass
        
        attachment = None
        if post_rec:
            attachment = {'name': str(post_rec.title), 'link': post_rec.recording.get_url(), 'source': post_rec.recording.get_url(), 'type':'swf'}
        
        try:
            WallPostTask.delay(post_pk, TargetType.FEED, attachment)
        except:
            logger.exception("Exception in posting feed: " + str(post_pk))
    
    @classmethod
    def post_comment(cls, post_pk):
        try:
            WallPostTask.delay(post_pk, TargetType.COMMENTS)
        except:
            logger.exception("Exception in posting comment: " + str(post_pk))        
    
    def run(self, post_pk, type, attachment = {}):
        
        if type == TargetType.FEED:
            post = FB_wall_post.objects.get(pk = post_pk)
        elif type == TargetType.COMMENTS:
            post = FB_comment.objects.get(pk = post_pk)
            
        if post.post_success:
            return
        api = GraphAPI(post.cred.access_token)
        
        is_success = False
        
        try:
            attachment['message'] = post.message
            ret_val = api.put_object(post.target.object_id, type, **attachment)
            is_success = True
            logger.info("Posted message")
        except Exception:
            logger.exception("Unable to post")   
        
        if is_success:
            post.object_id = ret_val['id']
            post.posted_time = datetime.now()
            post.post_success = True
        
        post.save()
        