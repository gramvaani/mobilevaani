from django.db import models

from app_manager.models import App_instance
from media.models import Recording

ID_LENGTH   = 40
TOKEN_LENGTH= 100
URL_LENGTH  = 255
PHONE_LENGTH= 15
MSG_LENGTH  = 255
ROLE_LENGTH = 24

class FB_credential(models.Model):
    name        = models.CharField(max_length = ID_LENGTH)
    app_id      = models.CharField(max_length = ID_LENGTH)
    app_api_key = models.CharField(max_length = ID_LENGTH)
    app_secret  = models.CharField(max_length = ID_LENGTH)
    access_token= models.CharField(max_length = TOKEN_LENGTH)
    user_id     = models.CharField(max_length = ID_LENGTH)
    
    def __unicode__(self):
        if self.name:
            return unicode(self.name)
        elif self.app_id:
            return 'app ' + str(self.app_id)
        elif self.user_id:
            return 'user ' +str(self.user_id)
            

class AI_FB_credential(models.Model):
    ai  = models.ForeignKey(App_instance)
    cred= models.ForeignKey(FB_credential)
    role= models.CharField(max_length = ROLE_LENGTH)
    
    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.cred) + '_' + str(self.role)

class FB_target(models.Model):
    TYPE_CHOICES = (
                    ('profile', 'Profile'),
                    ('wallpost', 'Wall Post'),
    )
    object_id   = models.CharField(max_length = ID_LENGTH)
    name        = models.CharField(max_length = URL_LENGTH)
    type        = models.CharField(max_length = 16, choices = TYPE_CHOICES)
    
    def __unicode__(self):
        if self.name:
            return unicode(self.name) + '_' + unicode(self.type) + '_' + unicode(self.object_id)
        else:
            return unicode(self.type) + '_' + unicode(self.object_id)

class AI_FB_target(models.Model):
    ai      = models.ForeignKey(App_instance)
    target  = models.ForeignKey(FB_target)
    role    = models.CharField(max_length = ROLE_LENGTH)
    
    def __unicode__(self):
        return unicode(self.ai) + '_' + unicode(self.target) + '_' + unicode(self.role)
    
class FB_wall_post(models.Model):
    cred    = models.ForeignKey(FB_credential)
    message = models.CharField(max_length = MSG_LENGTH)
    object_id = models.CharField(max_length = URL_LENGTH)
    posted_time = models.DateTimeField(null = True)
    post_success= models.BooleanField()
    target  = models.ForeignKey(FB_target)
    
    def __unicode__(self):
        return unicode(self.target) + '_' + unicode(self.message) + '_' + unicode(self.post_success)

class FB_wall_post_recording(models.Model):
    post      = models.ForeignKey(FB_wall_post)
    recording = models.ForeignKey(Recording)
    title     = models.CharField(max_length = ID_LENGTH)

class FB_comment(models.Model):
    cred    = models.ForeignKey(FB_credential)
    message = models.CharField(max_length = MSG_LENGTH)
    object_id = models.CharField(max_length = URL_LENGTH)
    posted_time = models.DateTimeField(null = True)
    post_success= models.BooleanField()
    target  = models.ForeignKey(FB_wall_post)
    
    def __unicode__(self):
        return unicode(self.message) + '_' + unicode(self.post_success)