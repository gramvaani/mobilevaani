'''
Created on 16-Jan-2011

@author: bchandra
'''

from celery.task import Task
from celery.task.control import revoke

from models import Tracker_schedule, Tracker_settings, Tracker_response
from telephony.utils import *
from telephony.inbound import FreeswitchSession
from datetime import datetime, timedelta


MAX_CODE_ERRORS   = 2
MAX_CALL_RETRIES  = 2
MAX_ALERT_RETRIES = 5

CAUSE_NOT_PICKED = "CAUSE_NOT_PICKED"
CAUSE_CODE_ERR   = "CAUSE_CODE_ERR"

class TrackerTask(Task):
    def hangup_callback(self):
        print "hungup"
        
    def run(self, schedule_id):
        task_id = self.request.id
        retries = self.request.retries
        logger = self.get_logger(propagate = True)
        logger.info("Running task: " + task_id)
        
        try:
            schedule = Tracker_schedule.objects.get(pk = schedule_id)
            settings = Tracker_settings.objects.get(user = schedule.user, ai = schedule.ai)
            
            if (not schedule.enabled) or retries == 0: 
                self.cancel_schedule(schedule)

            if not schedule.enabled:
                return
            
            if retries == 0:
                schedule.task = task_id
                schedule.save()
                self.retry([schedule_id], eta = schedule.start)
        
            #Sanity Check
            if task_id != schedule.task:
                logger.info("Current task_id does not match scheduled task_id. Skipping task")
                return
            
            self.session = FreeswitchSession(schedule.ai)
            self.session.setHangupHook(self.hangup_callback)
        
            for i in range(MAX_CALL_RETRIES):
                self.session.originate(get_session_url(settings.num, schedule.ai))
                if self.session.answered():
                    break
            
            if not self.session.answered():
                AlertTask.delay(CAUSE_NOT_PICKED, settings.id, task_id)
                return
            
            play_prompt(self.session, 'announce')
            authorized = False
            for i in range(MAX_CODE_ERRORS):
                code = self.session.playAndGetDigits(4, 4, varname='dtmf_result', filename=get_prompt_filename(self.session.ai, 'input_code'))
                if settings.code == code:
                    authorized = True
                    break
            
            play_prompt(self.session, 'current_location')
            response = Tracker_response(user_id = schedule.user.id, ai_id = schedule.ai.id)
            response.task = task_id 
            recording = get_embedded_recording(self.session, 20, response, Tracker_response.media)
            
            stop_service = False
            if authorized:
                key_pressed = self.session.playAndGetDigits(1, 1, varname='dtmf_result', filename=get_prompt_filename(self.session.ai, 'stop_service'))
                stop_service = (key_pressed == "1")
            else:
                AlertTask.delay(CAUSE_CODE_ERR, settings.id, task_id)
                
            play_prompt(self.session, 'thank_you')
            
            if stop_service:
                schedule.enabled = False
                schedule.task = ''
                schedule.save()
                return
            
            eta = datetime.now() + timedelta(minutes = settings.interval)
            self.retry([schedule_id], eta = eta)
            
        except Exception:
            logger.exception("Encountered exception in tracker task")
        finally:
            if hasattr(self, 'session') and self.session.answered():
                self.session.hangup()
            
            
    def cancel_schedule(self, schedule):
        logger = self.get_logger()
        logger.info("Cancelling schedule")
        try:
            if schedule.task == '':
                logger.info("Empty Task")
                return
            logger.info("Revoking task: " + str(schedule.task))
            revoke(schedule.task)
            schedule.task = ''
            schedule.save()
        except:
            logger.exception("Encountered exception in cancel_schedule")

class AlertTask(Task):        
    def run(self, cause, settings_id, scheduled_task_id):
        logger = self.get_logger()
        logger.info("Triggering Alert task")
        settings = Tracker_settings.objects.get(pk = settings_id)
        alert_session = FreeswitchSession(settings.ai)
        try:
            for i in range(MAX_ALERT_RETRIES):
                alert_session.originate(get_session_url(settings.guardian, settings.ai))
                if alert_session.answered():
                    break
                
            if not alert_session.answered():
                #TODO Alert not picked up. Handle the situation
                return
            
            play_prompt(alert_session, 'alert_announce')
            num_list = []
            for i in settings.num:
                num_list.append(i)
            alert_session.speak(' '.join(num_list))
            
            if cause == CAUSE_CODE_ERR:
                play_prompt(alert_session, 'alert_code_err')
            elif cause == CAUSE_NOT_PICKED:
                play_prompt(alert_session, 'alert_not_picked')
            
            response_set = Tracker_response.objects.filter(user = settings.user.id, ai = settings.ai.id, task = scheduled_task_id).order_by('time')
            
            if not response_set:
                #play_prompt(alert_session, 'alert_no_responses')
                pass
            else:
                play_prompt(alert_session, 'alert_responses')
                while True:
                    for response in response_set:
                        alert_session.speak('at ' + datetime_to_spoken_text(response.time))
                        alert_session.streamFile(response.media.get_full_filename())
                    key_pressed = alert_session.playAndGetDigits(1, 1, varname='dtmf_result', filename=get_prompt_filename(settings.ai, 'alert_repeat_responses'))
                    if key_pressed != "1":
                        break
            
            play_prompt(alert_session, 'alert_take_care')
            
        except:
            logger.exception("Encountered exception in alert task")
        finally:
            if alert_session.answered():
                alert_session.hangup()


class TestTask(Task):
    def run(self):
        import time, os
        setting = Tracker_settings.objects.get(pk=1)
        session = FreeswitchSession(setting.ai)
        start = time.time()
        session.originate(get_session_url('', setting.ai))
        print "\noriginate time\t\t\t", time.time() - start, os.getpid()
        print "answered", session.answered()
        #print "after originate"
        #session.recordFile('/tmp/test.mp3', 10, 2, 2)
        #print "after record file"
        #from twisted.internet import reactor
        #import os
        #print reactor, os.getpid(), object()
