from django.db import models
from django.contrib.auth.models import User
from vapp.app_manager.models import App_instance, CalleridField
from vapp.media.models import Recording
from vapp.utils import datetimefield_to_unix

CALLERID_LENGTH = 20

class Tracker_settings(models.Model):
    user = models.ForeignKey(User)
    ai   = models.ForeignKey(App_instance)
    num  = CalleridField(max_length = CALLERID_LENGTH)
    name = models.CharField(max_length = 32)
    guardian = models.CharField(max_length = CALLERID_LENGTH)
    interval = models.IntegerField()
    code = models.CharField(max_length = 4)
    
    def __unicode__(self):
        return 'setting_' + unicode(self.user) + '_' + unicode(self.ai)

class Tracker_schedule(models.Model):
    user  = models.ForeignKey(User)
    ai    = models.ForeignKey(App_instance)
    start = models.DateTimeField(auto_now_add = True)
    stop  = models.DateTimeField(auto_now_add = True)
    enabled= models.BooleanField(default = False)
    task  = models.CharField(null = True, max_length = 36)
    
    def starttime(self):
        if self.start:
            return datetimefield_to_unix(self.start)
        return None
    
    def __unicode__(self):
        return 'schedule_' + unicode(self.user) + '_' + unicode(self.ai) + '_' + str(self.enabled)

class Tracker_response(models.Model):
    user    = models.ForeignKey(User)
    ai      = models.ForeignKey(App_instance)
    task    = models.CharField(max_length = 36)
    time    = models.DateTimeField(auto_now_add = True)
    media   = models.ForeignKey(Recording)
    
    def __unicode__(self):
        return 'response_' + unicode(self.user) + '_' + unicode(self.ai)