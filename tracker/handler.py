'''
Created on 06-Feb-2011

@author: bchandra
'''

from vapp.telephony.utils import *
from vapp.tracker.models import *
from tracker.tasks import TrackerTask, MAX_CODE_ERRORS
from vapp.log import get_logger

from datetime import datetime, timedelta

logger = get_logger()

def in_handler(session, args):
    logger.info("Running Tracker")
    num = session.getCallerID()
    try:
        setting = Tracker_settings.objects.get(num = num)
        schedule = Tracker_schedule.objects.get(user__id = setting.user.id, ai__id = setting.ai.id)
        
        play_prompt(session, 'welcome')

        if schedule.enabled:
            disable_schedule(session, setting, schedule)
        else:
            enable_schedule(session, setting, schedule)

        play_prompt(session, 'thank_you')

    except:
        logger.exception("Unable to lookup setting for number: " + num)
        
def disable_schedule(session, setting, schedule):
    choice = session.playAndGetDigits(1, 1, 1, 5000, '#', get_prompt_filename(session.ai, 'q_disable'), '', '')
    if choice != '1':
        return
    authorized = False
    for i in range(MAX_CODE_ERRORS):
        code = session.playAndGetDigits(4, 4, 1, 5000, '#', get_prompt_filename(session.ai, 'input_code'), '', '')
        if setting.code == code:
            authorized = True
            break
    if authorized:
        schedule.enabled = False
        schedule.save()
        TrackerTask.delay(schedule.pk)  #@UndefinedVariable
        play_prompt(session, 'confirm_disabled')
    else:
        play_prompt(session, 'error_disabling')

def enable_schedule(session, setting, schedule):
    choice = session.playAndGetDigits(1, 1, 1, 5000, '#', get_prompt_filename(session.ai, 'q_enable'), '', '')
    if choice != '1':
        return
    schedule.enabled = True
    schedule.start = datetime.now() + timedelta(minutes = setting.interval)
    schedule.save()
    TrackerTask.delay(schedule.pk) #@UndefinedVariable
    play_prompt(session, 'confirm_enabled')
