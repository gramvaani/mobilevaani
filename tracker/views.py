from django.http import HttpResponse
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.db import transaction
from django.template import Context, loader

from django.contrib.auth.models import User, Group

from vapp.utils import get_deserialized_obj, is_valid_num, get_upcoming_datetime 
from vapp.log import get_request_logger
from vapp.perms import user_has_perms #@UnresolvedImport
import vapp.tracker.perms as tracker_perms #@UnresolvedImport

from vapp.app_manager.decorators import perms_required, instance_perm, instance_perm_per_obj #@UnresolvedImport

from models import Tracker_settings, Tracker_schedule
from tasks import TrackerTask

import json

json_serializer = serializers.get_serializer("json")()

ERR_TRACKER_ADD_USER = "ERR_TRACKER_ADD_USER"
ERR_TRACKER_SAVE_SETTINGS = "ERR_TRACKER_SAVE_SETTINGS"
ERR_TRACKER_SAVE_SCHEDULE = "ERR_TRACKER_SAVE_SCHEDULE"

ROLE_TRACKER_ADMIN = 'tracker_admin'
ROLE_TRACKER_USER  = 'tracker_user'

logger = get_request_logger()

def help(request):
    t, origin = loader.find_template('help.html', dirs=['/usr/local/voicesite/vapp/tracker/templates'])
    return HttpResponse(t.render(Context()))

@login_required
@instance_perm
def instance_info(request, ai_id):
    if user_has_perms(request, ai_id, tracker_perms.create_user): #@UndefinedVariable
        user_type = "admin"
    else:
        user_type = "user"
    return HttpResponse(json.dumps({"user_type": user_type}))

@login_required
@csrf_exempt
@require_POST
@instance_perm
@transaction.commit_on_success
def add_user(request, ai_id):
    logger.info(request)
    for obj in serializers.deserialize('json', request.POST['json']):
        obj = obj
    data = obj.object
    
    if data.username == '' or data.password == '':
        return HttpResponse(ERR_TRACKER_ADD_USER)
    
    try:
        user = User.objects.create_user(data.username, data.email, data.password)
        user_group = Group.objects.get(group_instance__ai = ai_id, group_instance__role = ROLE_TRACKER_USER)
        user.groups.add(user_group)
        
        is_success = True
    except :
        is_success = False
    
    if is_success:
        logger.info('created user by username: ' + str(data.username), request=request)
        return HttpResponse("ok")
    else:
        logger.warn('user not created for username: ' + str(data.username), request=request)
        return HttpResponse(ERR_TRACKER_ADD_USER)


def is_valid_interval(interval):
    try:
        interval = int(interval)
        if 10 <= interval <= 30:
            return True
        else:
            return False
    except:
        return False
    
def is_valid_code(code):
    try:
        int(code)
        if len(code) == 4:
            return True
        else:
            return False
    except:
        return False
        
@login_required
@csrf_exempt
@require_POST
@instance_perm
def save_settings(request, ai_id):
    settings_new = get_deserialized_obj(request)
    data = settings_new.object
      
    if not (is_valid_num(data.num) and is_valid_num(data.guardian) and is_valid_interval(data.interval) and is_valid_code(data.code)):
        return HttpResponse(ERR_TRACKER_SAVE_SETTINGS)
    
    settings_set = Tracker_settings.objects.filter(user = request.user.id, ai = ai_id)
    
    if not settings_set:
        data.ai_id = ai_id
        data.user_id = request.user.id
        settings_new.save()
        return HttpResponse("ok")
    else:
        settings = settings_set[0]
        settings.name = data.name
        settings.num = data.num
        settings.guardian = data.guardian
        settings.interval = data.interval
        settings.code = data.code
        settings.save()
        logger.info('saving tracker settings: guardian: ' + settings.guardian, request=request)
        return HttpResponse("ok")
    
@login_required
@instance_perm
def load_settings(request, ai_id):
    logger.debug(request)
    settings_qs = Tracker_settings.objects.filter(user = request.user.id, ai = ai_id)

    if not settings_qs:
        settings = Tracker_settings(user_id = request.user.id, ai_id = ai_id)
        settings.interval = 15
        settings.save()
    
    data = json_serializer.serialize(settings_qs, ensure_ascii = False)
    return HttpResponse(data) 

@login_required
@instance_perm
def load_schedule(request, ai_id):
    logger.debug(request)
    schedule_qs = Tracker_schedule.objects.filter(user = request.user.id, ai = ai_id)
    
    if not schedule_qs:
        schedule = Tracker_schedule(user_id = request.user.id, ai_id = ai_id)
        schedule.save()
        
    data = json_serializer.serialize(schedule_qs, extras = ('starttime',), fields = ('user', 'ai', 'enabled',), ensure_ascii = False)   
    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
@instance_perm
def save_schedule(request, ai_id):
    logger.debug(request)
    data = json.loads(request.POST['json'])[0]
    try:
        schedule = Tracker_schedule.objects.get(user = request.user.id, ai = ai_id)
        schedule.enabled = data['enabled']
        schedule.start = get_upcoming_datetime(data['start_hour'], data['start_min'])
        schedule.save()

        TrackerTask.delay(schedule.pk) #@UndefinedVariable
        
        return HttpResponse("ok")
    except Exception, e:
        logger.error('got exception:' + str(e))
        return HttpResponse(ERR_TRACKER_SAVE_SCHEDULE)