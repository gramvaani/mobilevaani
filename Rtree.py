from rtree import index
from log import get_logger

logger = get_logger()

def build_rtree_index(queryset):
    
    id = 0
    idx = index.Index()
    
    while id < len(queryset):
        idx.insert(id, (queryset[id].lat,queryset[id].long,queryset[id].lat,queryset[id].long),queryset[id])
        id = id + 1
    return idx