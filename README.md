<p align="center"><img src="https://gramvaani.org/wp-content/uploads/2023/02/download.png"/></p>

# Mobile Vaani 
Mobile Vaani is an innovative voice-based, federated network
of participatory media platforms where people whose voices
are often unheard, can share their opinions, hear from each
other and build communities through simple mobile phones.

'Mobile Vaani' is Gram Vaani’s answer to building a free
social-media platform, equivalent to Twitter for rural areas,
offering a rich voice stack – accessible using even a basic
feature phone and other web or app-based channels. Such
inclusive, bottom-up platforms enable low-income communities
in ‘media-dark’ regions to bring positive changes in their lives.

## About Gram Vaani
Gram Vaani enables communities to use technologies and share information that
can equip them to bring positive changes in their lives.

## About the Application
Gram Vaani solutions enable last mile information distribution in rural and
urban India as well as other developing nations. The Mobile Vaani App is part
of a suite of solutions that jump literacy and Internet connectivity barriers.
The communities can use the App, along with supporting processes around
community generated content, to increase user engagement for offline field
teams and drive user adoption.

<img src="http://gramvaani.org/wp-content/uploads/2023/02/MV-How-it-works.png"/>

## What do I need?

Mobilevaani application runs on

	* Python 2.7.12
    * Django 1.5
    * celery==3.1.23
    * django-tastypie==0.9.16
    * Soundcloud
    * python_twitter
    * django-celery==3.1.17
    * Freeswitch 1.4.6


## Installation procedure
 i) Install apache and wsgi module for apache2 by using following commands
```  sudo apt-get update
        sudo apt-get install apache2 libapache2-mod-wsgi
 	
	sudo a2enmod proxy proxy_http
 	
	service apache2 restart
```

ii) Installation of dependencies 
  ```
  sudo apt-get install python-setuptools mysql-server python-mysqldb rabbitmq-server build-essential automake libtool git-core python-dev python-pip python-pymad python-rtree python-software-properties python g++ make

```

iii) Installation of python dependencies 
```
 sudo pip install xlrd xlwt xlutils django-social-auth==0.7.28 stomp.py python-Levenshtein GchartWrapper django==1.5.0 django-excel-response==1.0 south suds celery==3.1.23 soundcloud python_twitter
django-celery==3.1.17 django-tastypie==0.9.16

```

iv) Installation of serializers
```
git clone --branch 1.1.2 https://github.com/gem/wadofstuff-django-serializers.git
cd wadofstuff-django-serializers/
sudo python setup.py install
 
```

v) Clone mobilevaani Folder from git

```git clone git@gitlab.com:gramvaani/mobilevaani.git```

vi) Installation of facebook sdk

```
git clone https://github.com/pythonforfacebook/facebook-sdk.git
cd facebook-sdk/
sudo python setup.py install
 
```

vii) Setup prompts folders
```commandline
 sudo mkdir /usr/local/voicesite
 sudo mkdir /usr/local/voicesite/fsmedia/
 sudo mkdir /usr/local/voicesite/fsmedia/prompts
 sudo mkdir /usr/local/voicesite/fsmedia/recordings

```

viii) Setup application code
```commandline
sudo cp -r mobilevaani/vapp /usr/local/voicesite/ 	
sudo cp -r mobilevaani/vWeb /usr/local/voicesite/
sudo ln -s /usr/local/voicesite/vapp /usr/local/lib/python2.7/dist-packages/vapp

```

ix) Creating Database in MySQL
```commandline
mysql -uroot -p
create database vapp;
create user 'vapp'@'localhost' identified by 'vapp';
GRANT ALL PRIVILEGES ON *.* TO 'vapp'@'localhost' WITH GRANT OPTION;
exit

```

x) Create all application tables
```commandline

```


To make it easier to make changes to the app or build your own local version
of the app, simply download the app from the git repository.

Create a folder for the Android project and the run the following commands to
get the code.

```
git init
git remote add origin https://gitlab.com/graamvaani/mobilevaani.git
git branch -M main
git push -uf origin main
```

Open that App as an existing Project in the IDE of your choice and build the
Android App.



## Support
If you have any questions about the App or it's features, please feel free to
send us an email at support@graamvaani.org

## Roadmap
Coming soon ...

## License
This is an Open Source Project published under an AGPL v3 License. Please see
the License file for details

## Contributing
The purpose of making this an open source project is to get more people
involved with the project and bringing new ideas to the ecosystem. If you are
interested in contributing to the project and/or adding a feature,
we recommend first creating an Issue in the repository, reach an agreement on
how to add the feature, and then work on the changes for the merge request.


Please do make sure that you run lint on the changed files and run tests as
well. We'll publish a detailed document on how to run tests and the template to
 use to format the code.

## Project status
This is an Active Project maintained by  Gram Vaani and OnionDev Technologies.
