from django.db import models
from django.contrib import admin
from django.db.models.signals import post_save, pre_save

from mnews.models import News, Category, Collection
from app_manager.models import App_instance
from media.models import Image, Image_caption_map
from campaign.models import Campaign
from vapp.log import get_request_logger
from location.models import Location

logger = get_request_logger()

DISPLAY_ORDER_CHOICES = [(x, x) for x in range(1, 40)]

class Web_instance(models.Model):
    name = models.CharField( max_length = 32 )
    ais = models.ManyToManyField(App_instance, null = True, blank = True ) 
    display_name = models.CharField( max_length = 32 )
    campaigns = models.ManyToManyField(Campaign, null = True, blank = True )
    collections = models.ManyToManyField(Collection, null = True, blank = True )    

    def __unicode__(self):
        return unicode(self.id) + '_' + unicode( self.name )


class Web_settings( models.Model ):
    wi = models.OneToOneField( Web_instance )
    fb_app_id = models.CharField( max_length = 64 )
    contact_request_emails = models.CharField(max_length = 1016, null = True, blank = True)

    def __unicode__(self):
        return unicode( self.wi )

class Team(models.Model):
    name = models.CharField(max_length = 32)
    public_display = models.BooleanField(default = False)
    description = models.TextField(null = False)
    display_order = models.PositiveIntegerField(max_length = 2, choices = DISPLAY_ORDER_CHOICES , null = False, unique = True)

    def __unicode__(self):
        return unicode(self.display_order) + '_' + unicode( self.name ) 

class Employee( models.Model ):
    name = models.CharField( max_length = 32, unique = True )
    designation = models.CharField( max_length = 48 )
    description = models.TextField( null = False )
    public_display = models.BooleanField( default = False )
    team = models.ForeignKey( Team )
    display_order = models.PositiveIntegerField( max_length=2, choices = DISPLAY_ORDER_CHOICES , null = False )
    employee_quote = models.CharField( max_length = 48, null = True, blank = True, help_text="One line super-awesome quote by employee" )
    employee_url = models.URLField( null = True, blank = True)
    facebook_url = models.URLField( null = True, blank = True)
    twitter_url = models.URLField( null = True, blank = True)
    google_url = models.URLField( null = True, blank = True)
    avatar = models.ForeignKey(Image, null = True, blank = True)

    def __unicode__(self):
        return unicode(self.team) + '_' + unicode(self.display_order) + '_' + unicode( self.name )

class Contact_request(models.Model):
    web_instance = models.ForeignKey(Web_instance)
    requestor_name = models.CharField(max_length = 128)
    requestor_org = models.CharField(max_length = 128)
    requestor_email = models.EmailField()
    time = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return "%s: From %s, %s" %s (self.web_instance, self.requestor_name, self.requestor_org)

class Contact_request_data(models.Model):
    request = models.ForeignKey(Contact_request)
    key = models.CharField(max_length = 32)
    value = models.CharField(max_length = 128)
