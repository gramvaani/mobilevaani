from django.contrib import admin

from models import Web_instance, Web_settings, Team, Employee


class WebInstanceAdmin(admin.ModelAdmin):
    filter_horizontal = ('ais', 'campaigns','collections')
admin.site.register(Web_instance, WebInstanceAdmin)

admin.site.register(Web_settings)
admin.site.register(Team)
admin.site.register(Employee)


