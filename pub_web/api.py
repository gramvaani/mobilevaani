from django.conf.urls import url

from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication, Authentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields

from models import Web_instance, Web_settings, Team, Employee, Contact_request, Contact_request_data
from mnews.models import News
from mnews.api import NewsResource, CategoryResource, CollectionResource
from location.api import LocationResource
from media.api import ImageResource, ImageCaptionMapResource
from campaign.api import CampaignResource
from app_manager.api import AppInstanceResource, AnonymousReadAuthentication
from vapp.api import AppInstanceAuthorization
from app_manager.tasks import EmailTask

from vapp.log import get_request_logger

import json

logger = get_request_logger()

class WebInstanceResource(ModelResource): 
    location = fields.CharField()
    ais = fields.ToManyField(AppInstanceResource, attribute='ais',full=True)
    campaigns = fields.ToManyField(CampaignResource, attribute='campaigns')
    collections = fields.ToManyField( CollectionResource, attribute='collections', null= True)
    
    class Meta:
        queryset = Web_instance.objects.all()
       	resource_name = "pub_web_instance"
        filtering = {
            'ais':ALL_WITH_RELATIONS,
        }
        authentication = AnonymousReadAuthentication()
        authorization = ReadOnlyAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_fb_app_id%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_fb_app_id' ), name = "api_get_fb_app_id" ),
         ]
    
    def get_fb_app_id(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:
            wi_id = request.REQUEST[ 'wi_id' ]   
            web_setting =  Web_settings.objects.get( wi_id = wi_id )

            self.log_throttled_access( request )
            return self.create_response( request, { 'fbAppId': web_setting.fb_app_id } )
        except:
            logger.exception( "get_fb_app_id" )
        return self.create_response( request, { "error": "getting facebook id failed." } )

class TeamResource(ModelResource):
    class Meta:
        queryset = Team.objects.all()
        resource_name = "pub_web_team"
        authentication = AnonymousReadAuthentication()
        authorization = ReadOnlyAuthorization()

    
class EmployeeResource(ModelResource): 
    team = fields.ForeignKey(TeamResource, attribute='team', full=True )
    avatar = fields.ForeignKey(ImageResource, attribute='avatar' ,full=True)

    class Meta:
        queryset = Employee.objects.all()
        resource_name = "pub_web_employee"
        authentication = AnonymousReadAuthentication()
        authorization = ReadOnlyAuthorization()
    
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_employees%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_employees' ), name = "api_get_employees" ),
        ]

    def get_employees( self, request, **kwargs ):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:
            employees = Employee.objects.filter(  team__public_display  = True, public_display = True ).order_by( 'team__display_order' , 'display_order')
            objects = []
            for result in employees:
                bundle = self.build_bundle(obj=result, request=request)
                bundle = self.full_dehydrate(bundle)
                objects.append(bundle)
           
            return self.create_response( request, {'employees':objects} )
        except:
            logger.exception( "get_employees" )
        return self.create_response( request, { "error": "Fetching employess failed." } )

class ContactRequestResource(ModelResource):
    class RequestDataKeys():
        NAME            = 'name'
        ORG             = 'org'
        WEBSITE         = 'website'
        EMAIL           = 'email'
        PHONE           = 'phone'
        REACH           = 'reachInterest'
        ACQUISITION     = 'acquisitionInterest'
        RETENTION       = 'retentionInterest'
        GROUP           = 'groupInterest'
        MESSAGE         = 'message'

    data_to_verbose_name = {
        RequestDataKeys.NAME        : 'Name',
        RequestDataKeys.ORG         : 'Organization',
        RequestDataKeys.WEBSITE     : 'Website',
        RequestDataKeys.EMAIL       : 'Email Address',
        RequestDataKeys.PHONE       : 'Phone No',
        RequestDataKeys.REACH       : 'Interested in Increasing Customer Reach',
        RequestDataKeys.ACQUISITION : 'Interested in Increasing Customer Acquisition',
        RequestDataKeys.RETENTION   : 'Interested in Increasing Customer Retention',
        RequestDataKeys.GROUP       : 'Interested in Starting Own Group',
        RequestDataKeys.MESSAGE     : 'Message'
    }

    class Meta:
        queryset = Contact_request.objects.all()
        resource_name = 'pub_web_contact_request'
        authentication = Authentication()
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/request_contact%s$" % (self._meta.resource_name, trailing_slash()), 
                    self.wrap_view('request_contact'), name = 'api_request_contact')
        ]

    def send_contact_request_email(self, request_data, email_list):
        subject = "Contact Request from %s, %s" % (request_data['name'], request_data['org'])
        body = "Additional Information:\n"
        for key in request_data:
            value = request_data[key]
            value = {'0':'No', '1':'Yes'}.get(value, value)
            body += "%s: %s\n" % (self.data_to_verbose_name[key], value)

        EmailTask.delay(subject = subject, body = body, email_list = email_list)

    def request_contact(self, request, **kwargs):
        self.method_check(request, allowed = ['post'])
        self.throttle_check(request)

        wi_id = request.REQUEST['wi_id']
        request_data = json.loads(request.REQUEST['request_data'])

        web_instance = Web_instance.objects.get(pk = wi_id)
        contact_request = Contact_request(web_instance = web_instance, requestor_name = request_data['name'],
                            requestor_org = request_data['org'], requestor_email = request_data['email'])
        contact_request.save()

        for key in request_data:
            if key in ['name', 'org', 'email' ]:
                continue
            contact_request_data = Contact_request_data(request = contact_request, key = key, value = request_data[key])
            contact_request_data.save()

        email_ids = web_instance.web_settings.contact_request_emails
        if email_ids:
            email_list = [ email.strip() for email in email_ids.split(',') ]
            self.send_contact_request_email(request_data, email_list)

        return self.create_response( request, { "success": "Request successful." } )

    
