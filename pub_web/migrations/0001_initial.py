# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Web_instance'
        db.create_table(u'pub_web_web_instance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('display_name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'pub_web', ['Web_instance'])

        # Adding M2M table for field ais on 'Web_instance'
        m2m_table_name = db.shorten_name(u'pub_web_web_instance_ais')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('web_instance', models.ForeignKey(orm[u'pub_web.web_instance'], null=False)),
            ('app_instance', models.ForeignKey(orm[u'app_manager.app_instance'], null=False))
        ))
        db.create_unique(m2m_table_name, ['web_instance_id', 'app_instance_id'])


    def backwards(self, orm):
        # Deleting model 'Web_instance'
        db.delete_table(u'pub_web_web_instance')

        # Removing M2M table for field ais on 'Web_instance'
        db.delete_table(db.shorten_name(u'pub_web_web_instance_ais'))


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'pub_web.web_instance': {
            'Meta': {'object_name': 'Web_instance'},
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['pub_web']