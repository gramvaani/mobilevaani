# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contact_request'
        db.create_table(u'pub_web_contact_request', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('web_instance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pub_web.Web_instance'])),
            ('requestor_name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('requestor_org', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('requestor_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'pub_web', ['Contact_request'])

        # Adding model 'Contact_request_data'
        db.create_table(u'pub_web_contact_request_data', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('request', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pub_web.Contact_request'])),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'pub_web', ['Contact_request_data'])

        # Adding field 'Web_settings.contact_request_emails'
        db.add_column(u'pub_web_web_settings', 'contact_request_emails',
                      self.gf('django.db.models.fields.CharField')(max_length=1016, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Contact_request'
        db.delete_table(u'pub_web_contact_request')

        # Deleting model 'Contact_request_data'
        db.delete_table(u'pub_web_contact_request_data')

        # Deleting field 'Web_settings.contact_request_emails'
        db.delete_column(u'pub_web_web_settings', 'contact_request_emails')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'abstract': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'cover_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cover_image'", 'null': 'True', 'to': u"orm['media.Image_caption_map']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'images'", 'symmetrical': 'False', 'to': u"orm['media.Image_caption_map']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'media.image': {
            'Meta': {'object_name': 'Image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'media.image_caption_map': {
            'Meta': {'object_name': 'Image_caption_map'},
            'caption': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']"})
        },
        u'pub_web.contact_request': {
            'Meta': {'object_name': 'Contact_request'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'requestor_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'requestor_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'requestor_org': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'web_instance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pub_web.Web_instance']"})
        },
        u'pub_web.contact_request_data': {
            'Meta': {'object_name': 'Contact_request_data'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'request': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pub_web.Contact_request']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'pub_web.employee': {
            'Meta': {'object_name': 'Employee'},
            'avatar': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'designation': ('django.db.models.fields.CharField', [], {'max_length': '48'}),
            'display_order': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '2'}),
            'employee_quote': ('django.db.models.fields.CharField', [], {'max_length': '48', 'null': 'True', 'blank': 'True'}),
            'employee_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'facebook_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'google_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'public_display': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pub_web.Team']"}),
            'twitter_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'pub_web.team': {
            'Meta': {'object_name': 'Team'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'display_order': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'public_display': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'pub_web.web_instance': {
            'Meta': {'object_name': 'Web_instance'},
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'campaigns': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['campaign.Campaign']", 'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'pub_web.web_settings': {
            'Meta': {'object_name': 'Web_settings'},
            'contact_request_emails': ('django.db.models.fields.CharField', [], {'max_length': '1016', 'null': 'True', 'blank': 'True'}),
            'fb_app_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'wi': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['pub_web.Web_instance']", 'unique': 'True'})
        }
    }

    complete_apps = ['pub_web']