import django
def django_version(request):
    return { 'django_version': django.VERSION[0],
             'django_subversion': django.VERSION[1]}
