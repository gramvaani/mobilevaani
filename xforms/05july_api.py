
from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.resources import Resource
from tastypie.utils import trailing_slash

from models import XForm, populate_form_data, Generic_XForm, populate_generic_form_data


import json

from vapp.log import get_request_logger

logger = get_request_logger()

class XFormDataResource(Resource):
    class Meta:
        resource_name = 'xform_data'
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/save%s$" % (self._meta.resource_name, \
                trailing_slash()), self.wrap_view('save_xform_data'),
                name="api_save_xform_data"),
        ]

    def save_xform_data(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)
        try:
            form_data = json.loads(request.body)
            form_id = form_data.get('formId')
            form_version = form_data.get('formVersion')

            for data in form_data.get('data'):
                if XForm.objects.filter(uuid=data.get('*meta-instance-id*')).exists():
                    continue
                try:
                    form = XForm.create_xform(form_id, form_version, **data)
                    populate_form_data(form, **data)
                except Exception as e:
                    logger.exception("Exception: creating records - save_xform_data: %s" % str(e))

            self.log_throttled_access(request)
            return self.create_response(request, {'success': True})
        except Exception as e:
            logger.exception("Exception while running: save_xform_data: %s" % request)
        return self.create_response(request, {'success': False, \
                'error': "Unable to run save_xform_data. Error: %s" % str(e)})




class GenericXFormDataResource(Resource):

    class Meta:
        resource_name = 'generic_xform_data'
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/save%s$" % (self._meta.resource_name, \
                trailing_slash()), self.wrap_view('save_generic_xform_data'),
                name="api_save_generic_xform_data"),
        ]
    

    def save_generic_xform_data(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)
        try:
            form_data = json.loads(request.body)
            form_id = form_data.get('formId')
            form_version = form_data.get('formVersion')

            for data in form_data.get('data'):
                if Generic_XForm.objects.filter(uuid=data.get('*meta-instance-id*')).exists():
                    continue
                try:
                    form = Generic_XForm.create_generic_xform(form_id, form_version, **data)
                    populate_generic_form_data(data)
                except Exception as e:
                    logger.exception("Exception: creating records - save_generic_xform_data: %s" % str(e))

            self.log_throttled_access(request)
            return self.create_response(request, {'success': True})
        except Exception as e:
            logger.exception("Exception while running: save_generic_xform_data: %s" % request)
        return self.create_response(request, {'success': False, \
                'error': "Unable to run save_generic_xform_data. Error: %s" % str(e)})
