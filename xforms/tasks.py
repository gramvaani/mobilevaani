from celery.task import Task

from models import XForm, VerificationStatus

from log import get_request_logger

logger = get_request_logger()


class SaveXFormDataTask(Task):

    def run(self, xform_id):
    	xform = XForm.objects.get(pk=xform_id)
    	if xform.status == VerificationStatus.VERIFIED:
    		return
    	survey_json = xform.survey_json
