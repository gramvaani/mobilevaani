from django.contrib import admin

from models import *


class PanchayatAdmin(admin.ModelAdmin):
    exclude = ('panchayat_id',)


class VOAdmin(admin.ModelAdmin):
    exclude = ('vo_id',)


class CommunityManagerAdmin(admin.ModelAdmin):
    exclude = ('cm_id',)
    list_filter = ('cluster__name',)
    search_fields = ['id', 'msisdn', 'name', 'panchayat__name', 'village',
        'vo__name', 'cluster__name']


admin.site.register(Block)
admin.site.register(Employee)
admin.site.register(Cluster)
admin.site.register(Qualification)
admin.site.register(CMInterest)
admin.site.register(MeetingType)
admin.site.register(CMActivity)
admin.site.register(SHGActivity)
admin.site.register(CMCategory)
admin.site.register(HouseholdIncomeDetail)
admin.site.register(Panchayat, PanchayatAdmin)
admin.site.register(VillageOffice, VOAdmin)
admin.site.register(CommunityManager, CommunityManagerAdmin)
admin.site.register(SHG)
admin.site.register(SHGMember)
