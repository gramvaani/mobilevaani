from django.db import models
from django.db.models import Max
from django.forms.models import model_to_dict

from vapp.app_manager.models import CalleridField, FL_MEDIUM, FL_SHORT

import json
from datetime import datetime, date
from vapp.log import get_request_logger

logger = get_request_logger()

# Mapping of block name with block id
BLOCKS = {
    'harnaut': 1,
    'harnauth': 1,
    'chandi': 2,
    'nagarnausa': 3,
    'hilsa': 4,
    'ekangsarai': 5,
    'ekangarsarai': 5,
    'islampur': 6
}

def get_block_id(block_identifier):
    block_identifier = block_identifier.lower()
    for block, block_id in BLOCKS.items():
        if block in block_identifier:
            return block_id


class VerificationStatus:
    NOT_VERIFIED = 0
    VERIFIED     = 1


class SurveyType:
    CMTG = "CMTG"
    SHGM = "SHGM"
    CMTR = "CMTR"
    CMDB = "CMDB"
    SHGD = "SHGD"


class Block(models.Model):
    block_id = models.IntegerField()
    name     = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.block_id, self.name)


class Employee(models.Model):
    emp_id = models.IntegerField()
    name   = models.CharField(max_length=FL_MEDIUM)
    msisdn = CalleridField()
    block  = models.ManyToManyField(Block)

    def __unicode__(self):
        return '{0}_{1}_{2}'.format(self.emp_id, self.name, self.msisdn)


class Cluster(models.Model):
    cl_id = models.IntegerField()
    name  = models.CharField(max_length=FL_MEDIUM)
    block = models.ForeignKey(Block)

    def __unicode__(self):
        return '{0}_{1}_{2}'.format(self.cl_id, self.name, unicode(self.block))


class Qualification(models.Model):
    qual_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.qual_id, self.name)

class TrainingDuration(models.Model):
    training_duration_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.training_duration_id, self.name)

class MeetingFrequency(models.Model):
    meeting_freq_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.meeting_freq_id, self.name)

class TrainingType(models.Model):
    trainingtype_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.trainingtype_id, self.name)

class Jtsp(models.Model):
    jtsp_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.jtsp_id, self.name)

class Wdcg(models.Model):
    wdcg_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.wdcg_id, self.name)

class SmartPhone(models.Model):
    smart_phone_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.smart_phone_id, self.name)

class PhonetoMeeting(models.Model):
    phone_to_meeting_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.phone_to_meeting_id, self.name)

class CMInterest(models.Model):
    interest_id = models.IntegerField()
    name        = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.interest_id, self.name)


class MeetingType(models.Model):
    type_id = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.type_id, self.name)


class CMActivity(models.Model):
    activity_id = models.IntegerField()
    name        = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.activity_id, self.name)


class SHGActivity(models.Model):
    activity_id = models.IntegerField()
    name        = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.activity_id, self.name)


class CMCategory(models.Model):
    category_id = models.IntegerField()
    name        = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.category_id, self.name)


class HouseholdIncomeDetail(models.Model):
    hid_id = models.IntegerField()
    name   = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.hid_id, self.name)

class PhoneHandling(models.Model):
    phonehandling_id = models.IntegerField()
    name   = models.CharField(max_length=FL_MEDIUM)

    def __unicode__(self):
        return '{0}_{1}'.format(self.phonehandling_id, self.name)

class XForm(models.Model):
    form_id      = models.CharField(max_length=FL_MEDIUM)
    form_version = models.CharField(max_length=FL_MEDIUM)
    uuid         = models.CharField(max_length=FL_MEDIUM)
    imei         = models.CharField(max_length=FL_MEDIUM)
    date         = models.DateField()
    emp_id       = models.IntegerField()
    cluster_id   = models.IntegerField()
    block_id     = models.IntegerField()
    survey_id    = models.CharField(max_length=FL_MEDIUM)
    survey_json  = models.TextField()
    survey_date  = models.DateField()
    status       = models.PositiveSmallIntegerField()

    @classmethod
    def create_xform(cls, *args, **kwargs):
        params = {}
        params['form_id'] = args[0]
        params['form_version'] = args[1]
        params['uuid'] = kwargs.get('*meta-instance-id*')
        params['imei'] = kwargs.get('imei')
        params['date'] = datetime.strptime(kwargs.get('today'), '%Y-%m-%d').date()
        params['emp_id'] = int(kwargs.get('EMP_ID'))
        params['cluster_id'] = int(kwargs.get('CLUSTER_ID'))
        params['block_id'] = get_block_id(params['form_id'])
        params['survey_id'] = kwargs.get('SURVEY_ID')
        params['survey_json'] = json.dumps(kwargs)
        if not kwargs.get('SURVEY_DATE'):
            params['survey_date'] = params['date']
        else:
            params['survey_date'] = datetime.strptime(\
        kwargs.get('SURVEY_DATE'), '%Y-%m-%d').date()
        params['status'] = VerificationStatus.NOT_VERIFIED

        xform = XForm(**params)
        xform.save()
        return xform

    def get_block_cluster_ids(self):
        block_id = Block.objects.get(block_id=self.block_id).id
        cluster_id = Cluster.objects.get(cl_id=self.cluster_id,
                                         block_id=block_id).id
        return (block_id, cluster_id)


class Panchayat(models.Model):
    form         = models.ForeignKey(XForm, null=True, blank=True)
    panchayat_id = models.IntegerField(null=True, blank=True)
    name         = models.CharField(max_length=FL_MEDIUM)
    block        = models.ForeignKey(Block, null=True, blank=True)
    cluster      = models.ForeignKey(Cluster, null=True, blank=True)
    status       = models.PositiveSmallIntegerField()

    def __unicode__(self):
        block = self.block.name if self.block else ''
        cluster = self.cluster.name if self.cluster else ''
        return '{0}_{1}_{2}'.format(block, cluster, self.name)

    @classmethod
    def get_panchayat_info(cls, pch_id, pch_name):
        if pch_id is None:
            return (None, '')
        if pch_id == "0":
            return (None, pch_name or '')
        else:
            pch = Panchayat.objects.get(id=pch_id)
            return (pch.id, pch.name)

    @classmethod
    def get_verified_panchayat(cls, *args, **kwargs):
        name = args[0]
        block = args[1]
        cluster = args[2]
        panchayat = None
        created = False

        if not name:
            return panchayat, created

        pch_qs = Panchayat.objects.filter(
                    name=name,
                    block=block,
                    cluster=cluster)
        if pch_qs.exists():
            panchayat = pch_qs[0]
            if panchayat.status == VerificationStatus.NOT_VERIFIED:
                panchayat.status = VerificationStatus.VERIFIED
                panchayat.save()
        else:
            panchayat = Panchayat.objects.create(
                            name=name,
                            block=block,
                            cluster=cluster,
                            status=VerificationStatus.VERIFIED)
            created = True

        return panchayat, created


class VillageOffice(models.Model):
    form    = models.ForeignKey(XForm, null=True, blank=True)
    vo_id   = models.IntegerField()
    name    = models.CharField(max_length=FL_MEDIUM)
    cluster = models.ForeignKey(Cluster)
    status  = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return '{0}_{1}'.format(self.cluster.name, self.name)

    def save(self, *args, **kwargs):
        self.vo_id = VillageOffice.objects.filter(
            cluster_id=self.cluster.id).aggregate(Max('vo_id'))['vo_id__max'] + 1
        super(VillageOffice, self).save(*args, **kwargs)

    @classmethod
    def get_vo_info(cls, vo_id, vo_name, cluster_id):
        if vo_id is None:
            return (None, '')
        if vo_id == "0":
            return (None, vo_name or '')
        else:
            vo = VillageOffice.objects.get(vo_id=vo_id, cluster_id=cluster_id)
            return (vo.id, vo.name)

    @classmethod
    def get_verified_vo(cls, *args, **kwargs):
        name = args[0]
        cluster = args[1]
        vo = None
        created = False

        if not name:
            return vo, created

        vo_qs = VillageOffice.objects.filter(name=name, cluster=cluster)
        if vo_qs.exists():
            vo = vo_qs[0]
            if vo.status == VerificationStatus.NOT_VERIFIED:
                vo.status = VerificationStatus.VERIFIED
                vo.save()
        else:
            vo = VillageOffice.objects.create(
                    name=name,
                    cluster=cluster,
                    status=VerificationStatus.VERIFIED)
            created = True

        return vo, created


class CMConsolidationLog(models.Model):
    original_cm_id = models.IntegerField()
    merged_cm_id   = models.IntegerField()
    time           = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, original_cm_id, merged_cm_ids):
        for merged_cm_id in merged_cm_ids:
            CMConsolidationLog.objects.create(
                original_cm_id=original_cm_id,
                merged_cm_id=merged_cm_id)


class CommunityManager(models.Model):
    form           = models.ForeignKey(XForm, null=True, blank=True)
    cm_id          = models.IntegerField(null=True, blank=True)
    msisdn         = CalleridField()
    alt_msisdn     = CalleridField()
    name           = models.CharField(max_length=FL_MEDIUM)
    creation_date  = models.DateField(default=date.today)
    age            = models.PositiveIntegerField(null=True, blank=True)
    panchayat      = models.ForeignKey(Panchayat, null=True, blank=True)
    panchayat_name = models.CharField(max_length=FL_MEDIUM, null=True, blank=True)
    village        = models.CharField(max_length=FL_MEDIUM, null=True, blank=True)
    vo             = models.ForeignKey(VillageOffice, null=True, blank=True)
    vo_name        = models.CharField(max_length=FL_MEDIUM, null=True, blank=True)
    qualification  = models.ForeignKey(Qualification, null=True, blank=True)
    age_jv         = models.PositiveIntegerField(null=True, blank=True)
    cm_jtsp        = models.ForeignKey(Jtsp, null=True, blank=True, help_text="Whether CM has received JTSP training")
    cm_wdcg        = models.ForeignKey(Wdcg, null=True, blank=True, help_text="Whether CM was part of Women Development Corporation Groups")
    cm_smart_phone = models.ForeignKey(SmartPhone, null=True, blank=True)
    cm_interest    = models.ManyToManyField(CMInterest, null=True, blank=True)
    block          = models.ForeignKey(Block)
    cluster        = models.ForeignKey(Cluster)
    mv_suggestion  = models.TextField(null=True, blank=True)
    status         = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return '{0}_{1}_{2}_{3}_{4}_{5}_{6}'.format(self.id, self.block.name,
            self.cluster.name, self.panchayat_name, self.vo_name, self.msisdn,
            self.name)

    @classmethod
    def get_cm(cls, cm_id, **kwargs):
        if cm_id is None:
            return None

        cm = None
        if cm_id == "0":
            cm_qs = CommunityManager.objects.filter(
                block_id=kwargs.get('block_id'),
                cluster_id=kwargs.get('cluster_id'),
                name__iexact=kwargs.get('name'),
                msisdn__contains=kwargs.get('msisdn'))
            if cm_qs.exists():
                cm = cm_qs[0]
        else:
            try:
                cm = CommunityManager.objects.get(id=cm_id)
            except:
                try:
                    orig_cm_id = CMConsolidationLog.objects.get(
                        merged_cm_id=int(cm_id)).original_cm_id
                    cm = CommunityManager.objects.get(
                        id=orig_cm_id)
                except:
                    pass

        return cm

    def get_changed_fields(self, **kwargs):
        attribs = ['name', 'alt_msisdn', 'age', 'panchayat_id',
            'panchayat_name', 'village', 'vo_id', 'vo_name', 'qualification_id',
            'age_jv', 'cm_jtsp_id', 'cm_wdcg_id', 'cm_smart_phone_id', 'mv_suggestion']
        changed_fields = {}

        for attrib in attribs:
            if kwargs.get(attrib) and \
                kwargs.get(attrib) != getattr(self, attrib):
                changed_fields[attrib] = kwargs.get(attrib)

        return changed_fields

    @classmethod
    def create(cls, **kwargs):
        cm = CommunityManager.objects.create(
            form_id=kwargs.get('form_id'),
            block_id=kwargs.get('block_id'),
            cluster_id=kwargs.get('cluster_id'),
            msisdn=kwargs.get('msisdn') or '',
            name=kwargs.get('name') or '',
            creation_date=kwargs.get('creation_date'),
            alt_msisdn=kwargs.get('alt_msisdn') or '',
            age=kwargs.get('age'),
            panchayat_id=kwargs.get('panchayat_id'),
            panchayat_name=kwargs.get('panchayat_name'),
            village=kwargs.get('village'),
            vo_id=kwargs.get('vo_id'),
            vo_name=kwargs.get('vo_name'),
            qualification_id=kwargs.get('qualification_id'),
            age_jv=kwargs.get('age_jv'),
            cm_jtsp_id=kwargs.get('cm_jtsp_id'),
            cm_wdcg_id=kwargs.get('cm_wdcg_id'),
            cm_smart_phone_id=kwargs.get('cm_smart_phone_id'),
            mv_suggestion=kwargs.get('mv_suggestion'),
            status=VerificationStatus.NOT_VERIFIED)
        return cm


class CMProfileUpdationLog(models.Model):
    form            = models.ForeignKey(XForm)
    cm              = models.ForeignKey(CommunityManager, null=True)
    original_cm_id  = models.IntegerField(null=True, blank=True)
    prev_profile    = models.TextField()
    updated_profile = models.TextField()


class SHG(models.Model):
    form      = models.ForeignKey(XForm, null=True)
    shg_id    = models.IntegerField()
    name      = models.CharField(max_length=FL_MEDIUM)
    key       = models.CharField(max_length=FL_MEDIUM)
    cm        = models.ForeignKey(CommunityManager)
    block     = models.ForeignKey(Block)
    cluster   = models.ForeignKey(Cluster)
    panchayat = models.ForeignKey(Panchayat, null=True)
    status    = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return '{0}_{1}_{2}'.format(self.shg_id, self.name, unicode(self.cm))


class SHGMember(models.Model):
    form           = models.ForeignKey(XForm, null=True, blank=True)
    name           = models.CharField(max_length=FL_MEDIUM)
    msisdn         = CalleridField()
    creation_date  = models.DateField(default=date.today)
    block          = models.ForeignKey(Block, null=True, blank=True)
    cluster        = models.ForeignKey(Cluster, null=True, blank=True)
    village        = models.CharField(max_length=FL_MEDIUM, null=True, blank=True)
    age            = models.PositiveIntegerField(null=True, blank=True)
    shg            = models.ForeignKey(SHG, null=True, blank=True)
    shg_name       = models.CharField(max_length=FL_MEDIUM)
    cm_id          = models.IntegerField(null=True, blank=True)
    qualification  = models.ForeignKey(Qualification, null=True, blank=True)
    hh_inc_detail  = models.ManyToManyField(HouseholdIncomeDetail, null=True, blank=True)
    working_status = models.PositiveSmallIntegerField(null=True, blank=True)
    kids_count     = models.PositiveIntegerField(null=True, blank=True)
    age_jv         = models.PositiveIntegerField(null=True, blank=True)
    pregnancy_details = models.PositiveSmallIntegerField(null=True, blank=True, help_text="Whether anyone in family is pregnant")
    child_details = models.PositiveSmallIntegerField(null=True, blank=True, help_text="Whether member has child less than 2 yrs of age in family")
    shg_smart_phone = models.ForeignKey(SmartPhone, null=True, blank=True)
    shg_phoneto_meeting = models.ForeignKey(PhonetoMeeting, null=True, blank=True)
    phone_handling  = models.ManyToManyField(PhoneHandling, null=True, blank=True)
    mv_suggestion  = models.TextField(null=True, blank=True)
    is_leader      = models.PositiveSmallIntegerField(null=True, blank=True)
    is_active      = models.PositiveSmallIntegerField(null=True, blank=True)
    status         = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return '{0}_{1}_{2}_{3}_{4}'.format(self.block.name, self.cluster.name,
            self.shg_name, self.name, self.msisdn)

    @classmethod
    def get_member(cls, **kwargs):
        shgm_qs = SHGMember.objects.filter(
            block_id=kwargs.get('block_id'),
            cluster_id=kwargs.get('cluster_id'),
            msisdn__contains=kwargs.get('msisdn'))
        if shgm_qs.exists():
            return shgm_qs[0]

    @classmethod
    def get_leader(cls, **kwargs):
        shgm_qs = SHGMember.objects.filter(
            block_id=kwargs.get('block_id'),
            cluster_id=kwargs.get('cluster_id'),
            msisdn__contains=kwargs.get('msisdn'),
            is_leader=1)
        if shgm_qs.exists():
            return shgm_qs[0]

    def get_changed_fields(self, **kwargs):
        attribs = ['village', 'name', 'age', 'shg_name', 'qualification_id', 'working_status', 'kids_count', 'age_jv', 
                   'pregnancy_details', 'child_details', 'shg_smart_phone_id', 'shg_phoneto_meeting_id' , 'mv_suggestion', 'is_active']
        changed_fields = {}

        for attrib in attribs:
            if kwargs.get(attrib) and \
                kwargs.get(attrib) != getattr(self, attrib):
                changed_fields[attrib] = kwargs.get(attrib)

        return changed_fields


class SHGLeaderInfo(models.Model):
    form   = models.ForeignKey(XForm, null=True)
    name   = models.CharField(max_length=FL_MEDIUM)
    msisdn = CalleridField()
    shg    = models.ForeignKey(SHG, null=True)
    status = models.PositiveSmallIntegerField()


class CMTrainingRecord(models.Model):
    form             = models.ForeignKey(XForm, null=True)
    block            = models.ForeignKey(Block, null=True)
    cluster          = models.ForeignKey(Cluster, null=True)
    creation_date    = models.DateField(default=date.today)
    meeting_type     = models.ForeignKey(MeetingType)
    training_type    = models.ForeignKey(TrainingType, null=True, blank=True)
    vo               = models.ForeignKey(VillageOffice, null=True)
    vo_name          = models.CharField(max_length=FL_MEDIUM, null=True, blank=True)
    panchayat        = models.ForeignKey(Panchayat, null=True)
    panchayat_name   = models.CharField(max_length=FL_MEDIUM, null=True, blank=True)
    vo_shg_count     = models.IntegerField(null=True)
    shg_present      = models.IntegerField(null=True, help_text="Total no.of SHGs whose members are present in meeting")
    shg_leaders_present= models.IntegerField(null=True)
    training_duration = models.ForeignKey(TrainingDuration, null=True, blank=True, help_text="Duration of JMV training")
    cm               = models.ManyToManyField(CommunityManager, null=True)
    cm_activity      = models.ManyToManyField(CMActivity, null=True)
    pci_wshop_module = models.CharField(max_length=FL_MEDIUM)
    pci_location     = models.CharField(max_length=FL_MEDIUM)
    #shg_leader_info is obsolete
    shg_leader_info  = models.ManyToManyField(SHGLeaderInfo, null=True)
    shg_member_info  = models.ManyToManyField(SHGMember, null=True)
    status           = models.PositiveSmallIntegerField()

    @classmethod
    def create(cls, **kwargs):
        cmtr = CMTrainingRecord.objects.create(
            form_id=kwargs.get('form_id'),
            block_id=kwargs.get('block_id'),
            cluster_id=kwargs.get('cluster_id'),
            creation_date=kwargs.get('creation_date'),
            training_type_id=kwargs.get('training_type_id'),
            meeting_type_id=kwargs.get('meeting_type_id'),
            vo_id=kwargs.get('vo_id'),
            vo_name=kwargs.get('vo_name'),
            panchayat_id=kwargs.get('panchayat_id'),
            panchayat_name=kwargs.get('panchayat_name'),
            vo_shg_count=kwargs.get('vo_shg_count'),
            shg_present=kwargs.get('shg_present'),
            shg_leaders_present=kwargs.get('shg_leaders_present'),
            training_duration_id=kwargs.get('training_duration_id'),
            pci_wshop_module=kwargs.get('pci_wshop_module'),
            pci_location=kwargs.get('pci_location'),
            status=VerificationStatus.NOT_VERIFIED)

        # Mandatory CM information received from form
        cm_first = CommunityManager.get_cm(kwargs.get('cm_id_first'))
        if cm_first:
            cmtr.cm.add(cm_first)

        for each in kwargs.get('cm_info'):
            if each.get('CMTG_CM_ID') == kwargs.get('cm_id_first'):
                continue

            kwargs['name'] = each.get('CM_NAME') or ''
            kwargs['msisdn'] = each.get('CMTG_CM_PHONE') or ''

            cm = CommunityManager.get_cm(each.get('CMTG_CM_ID'), **kwargs)
            if not cm:
                cm = CommunityManager.create(**kwargs)

            cmtr.cm.add(cm)

        if all(kwargs.get('cm_activities')):
            for each in kwargs.get('cm_activities'):
                cmtr.cm_activity.add(CMActivity.objects.get(activity_id=each))

        for each in kwargs.get('shg_leader_info'):
            kwargs['msisdn'] = each.get('CMTG_SHG_LEADER_PHONE') or ''
            kwargs['name'] = each.get('CMTG_SHG_LEADER_NAME') or ''
            shgm = SHGMember.get_leader(**kwargs)
            if not shgm:
                shgm = SHGMember.objects.create(
                    form_id=kwargs.get('form_id'),
                    block_id=kwargs.get('block_id'),
                    cluster_id=kwargs.get('cluster_id'),
                    name=kwargs.get('name'),
                    msisdn=kwargs.get('msisdn'),
                    creation_date=kwargs.get('creation_date'),
                    is_leader=1,
                    status=VerificationStatus.NOT_VERIFIED)
            else:
                if shgm.name != kwargs.get('name'):
                    shgm.name = kwargs.get('name')
                    shgm.save()
                
            cmtr.shg_member_info.add(shgm)


class SHGMeetingRecord(models.Model):
    form              = models.ForeignKey(XForm, null=True)
    block             = models.ForeignKey(Block, null=True)
    cluster           = models.ForeignKey(Cluster, null=True)
    creation_date     = models.DateField(default=date.today)
    training_type     = models.ForeignKey(TrainingType, null=True, blank=True)
    cm                = models.ForeignKey(CommunityManager)
    shg               = models.ForeignKey(SHG, null=True, blank=True)
    shg_name          = models.CharField(max_length=FL_MEDIUM)
    total_member_count= models.IntegerField(null=True)
    member_count      = models.IntegerField(null=True)
    member_with_phone = models.IntegerField(null=True)
    meeting_frequency  = models.ForeignKey(MeetingFrequency, null=True, blank=True)
    pregnancy_count   = models.IntegerField(null=True) 
    child_lt_two_count= models.IntegerField(null=True, help_text="No.of SHG members who have children less than 2 years of age")
    shg_member        = models.ManyToManyField(SHGMember)
    shg_activity      = models.ManyToManyField(SHGActivity)
    status            = models.PositiveSmallIntegerField()

    @classmethod
    def create(cls, **kwargs):
        shgmr = SHGMeetingRecord.objects.create(
            form_id=kwargs.get('form_id'),
            block_id=kwargs.get('block_id'),
            cluster_id=kwargs.get('cluster_id'),
            creation_date=kwargs.get('creation_date'),
            training_type_id=kwargs.get('training_type_id'),
            cm_id=kwargs.get('cm_id'),
            shg_name=kwargs.get('shg_name'),
            total_member_count=kwargs.get('total_member_count'),
            member_count=kwargs.get('member_count'),
            member_with_phone=kwargs.get('member_with_phone'),
            meeting_frequency_id=kwargs.get('meeting_freq_id'),
            pregnancy_count=kwargs.get('pregnancy_count'),
            child_lt_two_count=kwargs.get('child_lt_two_count'),
            status=VerificationStatus.NOT_VERIFIED)

        for each in kwargs.get('shg_member_info'):
            kwargs['msisdn'] = each.get('SHGM_SHG_MEMBER_PHONE') or ''
            kwargs['name'] = each.get('SHGM_SHG_MEMBER_NAME') or ''
            kwargs['is_active'] = each.get('SHGM_SHG_MEMBER_ACTIVE')
            kwargs['child_details'] = each.get('SHGM_SHG_CHILD_DETAILS')
            if each.get('SHGM_SHG_SMART_PHONE'):
                kwargs['shg_smart_phone_id'] = SmartPhone.objects.get(
                smart_phone_id=each.get('SHGM_SHG_SMART_PHONE')).id

            shgm = SHGMember.get_member(**kwargs)
            if shgm:
                changed_fields = shgm.get_changed_fields(**kwargs)
                if changed_fields:
                    for field, value in changed_fields.items():
                        setattr(shgm, field, value)
                    shgm.save()
                if each.get('SHGM_SHG_PHONE_HANDLING') and \
                    all(each.get('SHGM_SHG_PHONE_HANDLING')):
                    shgm_phone_handlings=shgm.phone_handling.all()
                    for shgm_phone_handling in shgm_phone_handlings:
                        shgm.phone_handling.remove(shgm_phone_handling)
                    for phnhandling in each.get('SHGM_SHG_PHONE_HANDLING'):
                        shgm.phone_handling.add(
                            PhoneHandling.objects.get(phonehandling_id=phnhandling))

            else:
                shgm = SHGMember.objects.create(
                    form_id=kwargs.get('form_id'),
                    block_id=kwargs.get('block_id'),
                    cluster_id=kwargs.get('cluster_id'),
                    name=kwargs.get('name'),
                    msisdn=kwargs.get('msisdn'),
                    creation_date=kwargs.get('creation_date'),
                    shg_name=kwargs.get('shg_name'),
                    cm_id=kwargs.get('cm_id'),
                    is_active=kwargs.get('is_active'),
                    child_details=kwargs.get('child_details'),
                    shg_smart_phone_id=kwargs.get('shg_smart_phone_id'),
                    status=VerificationStatus.NOT_VERIFIED)
            
                if each.get('SHGM_SHG_PHONE_HANDLING') and \
                    all(each.get('SHGM_SHG_PHONE_HANDLING')):
                    for phnhandling in each.get('SHGM_SHG_PHONE_HANDLING'):
                        shgm.phone_handling.add(
                            PhoneHandling.objects.get(phonehandling_id=phnhandling))

            shgmr.shg_member.add(shgm)
            
        
        if kwargs.get('shg_activities') and \
            all(kwargs.get('shg_activities')):
            for each in kwargs.get('shg_activities'):
                shgmr.shg_activity.add(SHGActivity.objects.get(activity_id=each))


class CMTrackerRecord(models.Model):
    form              = models.ForeignKey(XForm, null=True)
    block             = models.ForeignKey(Block, null=True)
    cluster           = models.ForeignKey(Cluster, null=True)
    creation_date     = models.DateField(default=date.today)
    cm                = models.ForeignKey(CommunityManager)
    meeting_type      = models.ForeignKey(MeetingType)
    cm_training_count = models.IntegerField()
    shg_meeting_count = models.IntegerField()
    q1_response       = models.IntegerField()
    q2_response       = models.IntegerField()
    q3_response       = models.IntegerField()
    q4_response       = models.IntegerField()
    q5_response       = models.IntegerField()
    q6_response       = models.CharField(max_length=FL_SHORT)
    q7_response       = models.IntegerField()
    cm_category       = models.ForeignKey(CMCategory, null=True)
    status            = models.PositiveSmallIntegerField()

    @classmethod
    def create(cls, **kwargs):
        record = CMTrackerRecord.objects.create(
            form_id=kwargs.get('form_id'),
            block_id=kwargs.get('block_id'),
            cluster_id=kwargs.get('cluster_id'),
            creation_date=kwargs.get('creation_date'),
            cm_id=kwargs.get('cm_id'),
            meeting_type_id=kwargs.get('meeting_type_id'),
            cm_training_count=kwargs.get('cm_training_count'),
            shg_meeting_count=kwargs.get('shg_meeting_count'),
            q1_response=kwargs.get('q1_response'),
            q2_response=kwargs.get('q2_response'),
            q3_response=kwargs.get('q3_response'),
            q4_response=kwargs.get('q4_response'),
            q5_response=kwargs.get('q5_response'),
            q6_response=kwargs.get('q6_response'),
            q7_response=kwargs.get('q7_response'),
            cm_category_id=kwargs.get('cm_category_id'),
            status=VerificationStatus.NOT_VERIFIED)

class Generic_XForm(models.Model):
    form_id      = models.CharField(max_length=FL_MEDIUM)
    uuid         = models.CharField(max_length=FL_MEDIUM)
    date         = models.DateField()
    survey_json  = models.TextField()
    status       = models.PositiveSmallIntegerField()

    @classmethod
    def create_generic_xform(cls, *args, **kwargs):
        params = {}
        params['form_id'] = args[0]
        params['uuid'] = kwargs.get('*meta-instance-id*')
        date_time = kwargs.get("*meta-submission-date*")
        params['date'] = date_time.split("T", 1)[0]
        params['survey_json'] = json.dumps(kwargs)
        params['status'] = VerificationStatus.NOT_VERIFIED
        generic_xform = Generic_XForm(**params)
        generic_xform.save()
        return generic_xform
   
class Generic_form_data(models.Model):
    form_id = models.TextField(null=True, blank=True)
    root_id = models.TextField(null=True, blank=True)
    parent_id = models.IntegerField(null=True, blank=True)
    parent_tag = models.TextField(null=True, blank=True)
    tag = models.TextField(null=True, blank=True)
    value = models.TextField(null=True, blank=True)
    child_list = models.TextField(null=True, blank=True)

    def flatten_json(self, form_id, survey_json):
        parent_id = flatten(form_id, survey_json.get("__id"), survey_json.get("intro_odk"), None, '')
    

def create_generic_object(form, root, parent, parent_tags, tags, values, children=''):
    parent =Generic_form_data(form_id = form, root_id=root, parent_id=parent, parent_tag=parent_tags, tag=tags, value=values, child_list = children)        
    parent.save()
    return parent.id


def flatten(form_id, root, form, parent_id, tags=''):
        if type(form) is dict:
            parent_tag = None
            if parent_id:
                parent_tag = Generic_form_data.objects.filter(id=parent_id)[0].tag
            parent_id =create_generic_object(form_id, str(root), parent_id, parent_tag, tags, str(form))        
            child_list = []
            for key in form:
                child_id = flatten(form_id, root, form[key], parent_id, key) 
                child_list.append(child_id)
            #update parent id object with child list
            Generic_form_data.objects.filter(id=parent_id).update(child_list= ','.join(str(id) for id in child_list))
            return parent_id 
        elif type(form) is list:
            parent_tag = None
            if parent_id:
                parent_tag = Generic_form_data.objects.filter(id=parent_id)[0].tag
            parent_id =create_generic_object(form_id, str(root), parent_id, parent_tag, tags, str(form))
            child_list = []
            for key in form:
                if type(key) is dict:
                    #multiple participants 
                    child_id = flatten(form_id, root, key, parent_id, tags + '_child') 
                    child_list.append(child_id)
            Generic_form_data.objects.filter(id=parent_id).update(child_list= ','.join(str(id) for id in child_list))
            return parent_id
        else:
            parent_tag = None
            if parent_id:
                parent_tag = Generic_form_data.objects.filter(id=parent_id)[0].tag
            leaf_id =create_generic_object(form_id, str(root), parent_id, parent_tag, tags, str(form))  
            return leaf_id  

def populate_generic_form_data(form_id, form):
    form_data = Generic_form_data()
    form_data.flatten_json(form_id, form)
   

def populate_form_data(form, **kwargs):
    survey_type = kwargs.get('SURVEY_ID')
    (block_id, cluster_id) = form.get_block_cluster_ids()
    kwargs['creation_date'] = form.survey_date
    kwargs['form_id'] = form.id
    kwargs['block_id'] = block_id
    kwargs['cluster_id'] = cluster_id

    if survey_type == SurveyType.CMTG:
        if kwargs.get('CMTG_TRAINING'):
            kwargs['training_type_id'] = TrainingType.objects.get(
                trainingtype_id=kwargs['CMTG_TRAINING']).id
        kwargs['meeting_type_id'] = MeetingType.objects.get(
            type_id=kwargs.get('CMTG_MEETING_TYPE_ID')).id
        (kwargs['vo_id'], kwargs['vo_name']) = VillageOffice.get_vo_info(
            kwargs.get('CMTG_VO_ID'), kwargs.get('CMTG_VO_NAME'),
            kwargs.get('cluster_id'))
        (kwargs['panchayat_id'], kwargs['panchayat_name']) = \
            Panchayat.get_panchayat_info(kwargs.get('CMTG_PCH_ID'),
                kwargs.get('CMTG_PANCHAYAT_NAME'))
        kwargs['vo_shg_count'] = kwargs.get('CMTG_SHG_COUNT_VO')
        kwargs['shg_present'] = kwargs.get('CMTG_SHG_PRESENT')
        kwargs['shg_leaders_present'] = kwargs.get('CMTG_SHG_LEADERS_PRESENT')
        kwargs['pci_wshop_module'] = kwargs.get('CMTG_PCI_WORKSHOP_MODULE') or ''
        kwargs['pci_location'] = kwargs.get('CMTG_PCI_LOCATION') or ''
        kwargs['shg_leader_info'] = kwargs.get('CMTG_SHG_LEADER_INFO') or []
        kwargs['cm_info'] = kwargs.get('CMTG_CM_INFO') or []
        kwargs['cm_id_first'] = kwargs.get('CMTG_CM_ID_FIRST')
        if kwargs.get('CMTG_JMV_TRAINING_DURATION'):
            kwargs['training_duration_id'] = TrainingDuration.objects.get(
                training_duration_id=kwargs['CMTG_JMV_TRAINING_DURATION']).id
        kwargs['cm_activities'] = kwargs.get('CMTG_CM_ACTIVITIES_DONE') or []
        CMTrainingRecord.create(**kwargs)
    elif survey_type == SurveyType.SHGM:
        if kwargs.get('SHGM_SHG_TRAINING'):
            kwargs['training_type_id'] = TrainingType.objects.get(
                trainingtype_id=kwargs['SHGM_SHG_TRAINING']).id
        kwargs['name'] = kwargs.get('SHGM_CM_NAME') or ''
        kwargs['msisdn'] = kwargs.get('SHGM_CM_PHONE') or ''

        cm = CommunityManager.get_cm(kwargs.get('SHGM_CM_ID'), **kwargs)
        if not cm:
            cm = CommunityManager.create(**kwargs)

        kwargs['cm_id'] = cm.id
        kwargs['shg_name'] = kwargs.get('SHGM_SHG_NAME') or ''
        kwargs['total_member_count'] = kwargs.get('SHGM_TOTAL_COUNT')
        kwargs['member_count'] = kwargs.get('SHGM_COUNT_MEMBERS')
        kwargs['member_with_phone'] = kwargs.get('SHGM_COUNT_MEMBERS_WITH_PHONE')
        if kwargs.get('SHGM_SHG_MEETING_FREQUENCY'):
            kwargs['meeting_freq_id'] = MeetingFrequency.objects.get(
                meeting_freq_id=kwargs['SHGM_SHG_MEETING_FREQUENCY']).id
        kwargs['pregnancy_count'] = kwargs.get('SHGM_SHG_PREGNANCY_COUNT')
        kwargs['child_lt_two_count'] = kwargs.get('SHGM_SHG_CHILD_LT_TWO_COUNT')
        kwargs['shg_member_info'] = kwargs.get('SHGM_SHG_MEMBERS') or []
        kwargs['shg_activities'] = kwargs.get('SHGM_SHG_ACTIVITIES_DONE') or []
        SHGMeetingRecord.create(**kwargs)
    elif survey_type == SurveyType.CMTR:
        kwargs['name'] = kwargs.get('CMTR_CM_NAME') or ''
        kwargs['msisdn'] = kwargs.get('CMTR_CM_PHONE') or ''

        cm = CommunityManager.get_cm(kwargs.get('CMTR_CM_ID'), **kwargs)
        if not cm:
            cm = CommunityManager.create(**kwargs)

        kwargs['cm_id'] = cm.id
        kwargs['meeting_type_id'] = MeetingType.objects.get(
            type_id=kwargs.get('CMTR_MEETING_TYPE_ID')).id
        kwargs['cm_training_count'] = kwargs.get('CMTR_COUNT_CM_TRAININGS')
        kwargs['shg_meeting_count'] = kwargs.get('CMTR_COUNT_SHG_MEETINGS')
        kwargs['q1_response'] = kwargs.get('CMTR_Q1')
        kwargs['q2_response'] = kwargs.get('CMTR_Q2')
        kwargs['q3_response'] = kwargs.get('CMTR_Q3')
        kwargs['q4_response'] = kwargs.get('CMTR_Q4')
        kwargs['q5_response'] = kwargs.get('CMTR_Q5')
        kwargs['q6_response'] = kwargs.get('CMTR_Q6')
        kwargs['q7_response'] = kwargs.get('CMTR_Q7')
        kwargs['cm_category_id'] = CMCategory.objects.get(
            category_id=kwargs.get('CMTR_CM_CATEGORY_ID')).id
        CMTrackerRecord.create(**kwargs)
    elif survey_type == SurveyType.CMDB:
        (kwargs['vo_id'], kwargs['vo_name']) = VillageOffice.get_vo_info(
            kwargs.get('CMDB_VO_ID'), kwargs.get('CMDB_VO_NAME'),
            kwargs.get('cluster_id'))
        (kwargs['panchayat_id'], kwargs['panchayat_name']) = \
            Panchayat.get_panchayat_info(kwargs.get('CMDB_PCH_ID'),
                kwargs.get('CMDB_PANCHAYAT_NAME'))

        if kwargs.get('CMDB_CM_QUALIFICATION_ID'):
            kwargs['qualification_id'] = Qualification.objects.get(
                qual_id=kwargs['CMDB_CM_QUALIFICATION_ID']).id
        
        if kwargs.get('CMDB_CM_JTSP_TRAINING'):
            kwargs['cm_jtsp_id'] = Jtsp.objects.get(
                jtsp_id=kwargs['CMDB_CM_JTSP_TRAINING']).id
        
        if kwargs.get('CMDB_CM_WDCG'):
            kwargs['cm_wdcg_id'] = Wdcg.objects.get(
                wdcg_id=kwargs['CMDB_CM_WDCG']).id    
        
        if kwargs.get('CMDB_CM_SMART_PHONE'):
            kwargs['cm_smart_phone_id'] = SmartPhone.objects.get(
                smart_phone_id=kwargs['CMDB_CM_SMART_PHONE']).id    
        
        kwargs['name'] = kwargs.get('CMDB_CM_NAME') or ''
        kwargs['msisdn'] = kwargs.get('CMDB_CM_PHONE') or ''
        kwargs['alt_msisdn'] = kwargs.get('CMDB_CM_PHONE_2') or ''
        kwargs['age'] = kwargs.get('CMDB_CM_AGE')
        kwargs['village'] = kwargs.get('CMDB_VILLAGE_NAME') or ''
        kwargs['age_jv'] = kwargs.get('CMDB_CM_AGE_JV')
        kwargs['mv_suggestion'] = kwargs.get('CMDB_MV_SUGGESTIONS')
        
        cm = CommunityManager.get_cm(kwargs.get('CMDB_CM_ID'), **kwargs)
        if not cm:
            cm = CommunityManager.create(**kwargs)
        else:
            prev_profile = model_to_dict(cm)
            changed_fields = cm.get_changed_fields(**kwargs)
            if changed_fields:
                for field, value in changed_fields.items():
                    setattr(cm, field, value)

                cm.save()
                CMProfileUpdationLog.objects.create(
                    form_id=kwargs.get('form_id'),
                    original_cm_id=cm.id,
                    prev_profile=prev_profile,
                    updated_profile=model_to_dict(cm))

        if kwargs.get('CMDB_CM_INTEREST_ID') and \
            all(kwargs.get('CMDB_CM_INTEREST_ID')):
            cm_interests = cm.cm_interest.all()
            for cm_interest in cm_interests:
                cm.cm_interest.remove(cm_interest)
            for int_id in kwargs.get('CMDB_CM_INTEREST_ID'):
                cmi = CMInterest.objects.get(interest_id=int_id)
                cm.cm_interest.add(cmi)
    elif survey_type == SurveyType.SHGD:
        kwargs['msisdn'] = kwargs.get('SHGD_CM_PHONE') or ''
        kwargs['name'] = kwargs.get('SHGD_CM_NAME') or ''
        kwargs['alt_msisdn'] = kwargs.get('SHGD_CM_PHONE_2') or ''
        kwargs['village'] = kwargs.get('SHGD_VILLAGE_NAME') or ''
        (kwargs['vo_id'], kwargs['vo_name']) = VillageOffice.get_vo_info(
            kwargs.get('SHGD_VO_ID'), kwargs.get('SHGD_VO_NAME'),
            kwargs.get('cluster_id'))
        (kwargs['panchayat_id'], kwargs['panchayat_name']) = \
            Panchayat.get_panchayat_info(kwargs.get('SHGD_PCH_ID'),
                kwargs.get('SHGD_PANCHAYAT_NAME'))

        cm = CommunityManager.get_cm(kwargs.get('SHGD_CM_ID'), **kwargs)
        if not cm:
            cm = CommunityManager.create(**kwargs)
        else:
            prev_profile = model_to_dict(cm)
            changed_fields = cm.get_changed_fields(**kwargs)
            if changed_fields:
                for field, value in changed_fields.items():
                    setattr(cm, field, value)

                cm.save()
                CMProfileUpdationLog.objects.create(
                    form_id=kwargs.get('form_id'),
                    original_cm_id=cm.id,
                    prev_profile=prev_profile,
                    updated_profile=model_to_dict(cm))

        kwargs['shg_name'] = kwargs.get('SHGD_SHG_NAME') or ''
        kwargs['village'] = kwargs.get('SHGD_SHG_MEMBER_VILLAGE') or ''
        for each in (kwargs.get('SHGD_SHG_MEMBERS') or []):
            kwargs['msisdn'] = each.get('SHGD_SHG_MEMBER_PHONE') or ''
            kwargs['name'] = each.get('SHGD_SHG_MEMBER_NAME') or ''
            kwargs['age'] = each.get('SHGD_SHG_MEMBER_AGE')
            if each.get('SHGD_SHG_QUALIFICATION_ID'):
                kwargs['qualification_id'] = Qualification.objects.get(
                    qual_id=each.get('SHGD_SHG_QUALIFICATION_ID')).id
            kwargs['working_status'] = each.get('SHGD_SHG_WORKING_STATUS')
            kwargs['kids_count'] = each.get('SHGD_COUNT_SHG_KIDS')
            kwargs['age_jv'] = each.get('SHGD_SHG_AGE_JV')
            kwargs['pregnancy_details'] = each.get('SHGD_SHG_PREGNANCY_DETAILS')
            kwargs['child_details'] = each.get('SHGD_SHG_CHILD_DETAILS')
            if each.get('SHGD_SHG_SMART_PHONE'):
                kwargs['shg_smart_phone_id'] = SmartPhone.objects.get(
                    smart_phone_id=each.get('SHGD_SHG_SMART_PHONE')).id 
            if each.get('SHGD_SHG_PHONE_TO_MEETING'):
                kwargs['shg_phoneto_meeting_id'] = PhonetoMeeting.objects.get(
                    phone_to_meeting_id=each.get('SHGD_SHG_PHONE_TO_MEETING')).id 
            kwargs['mv_suggestion'] = each.get('SHGD_MV_SUGGESTIONS')
  
            shgm = SHGMember.get_member(**kwargs)
            if not shgm:
                shgm = SHGMember.objects.create(
                    form_id=kwargs.get('form_id'),
                    name=kwargs.get('name'),
                    msisdn=kwargs.get('msisdn'),
                    creation_date=kwargs.get('creation_date'),
                    block_id=kwargs.get('block_id'),
                    cluster_id=kwargs.get('cluster_id'),
                    village=kwargs.get('village'),
                    age=kwargs.get('age'),
                    shg_name=kwargs.get('shg_name'),
                    cm_id=cm.id,
                    qualification_id=kwargs.get('qualification_id'),
                    working_status=kwargs.get('working_status'),
                    kids_count=kwargs.get('kids_count'),
                    age_jv=kwargs.get('age_jv'),
                    pregnancy_details=kwargs.get('pregnancy_details'),
                    child_details=kwargs.get('child_details'),
                    shg_smart_phone_id=kwargs.get('shg_smart_phone_id'),
                    shg_phoneto_meeting_id=kwargs.get('shg_phoneto_meeting_id'),
                    mv_suggestion=kwargs.get('mv_suggestion'),
                    is_leader=0,
                    status=VerificationStatus.NOT_VERIFIED)
            else:
                changed_fields = shgm.get_changed_fields(**kwargs)
                if changed_fields:
                    for field, value in changed_fields.items():
                        setattr(shgm, field, value)

                    shgm.save()
            
            if each.get('SHGD_SHG_PHONE_HANDLING') and \
                    all(each.get('SHGD_SHG_PHONE_HANDLING')):
                    shgm_phone_handlings=shgm.phone_handling.all()
                    for shgm_phone_handling in shgm_phone_handlings:
                        shgm.phone_handling.remove(shgm_phone_handling)
                    for phnhandling in each.get('SHGD_SHG_PHONE_HANDLING'):
                        shgm.phone_handling.add(
                            PhoneHandling.objects.get(phonehandling_id=phnhandling))
                           
            if each.get('SHGD_SHG_HOUSEHOLD_INCOME') and \
                    all(each.get('SHGD_SHG_HOUSEHOLD_INCOME')):
                    shgm_hh_incs=shgm.hh_inc_detail.all()
                    for shgm_hhi in shgm_hh_incs:
                        shgm.hh_inc_detail.remove(shgm_hhi)
                    for hhi in each.get('SHGD_SHG_HOUSEHOLD_INCOME'):
                        shgm.hh_inc_detail.add(
                            HouseholdIncomeDetail.objects.get(hid_id=hhi))
                    
