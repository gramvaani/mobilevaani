# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Block'
        db.create_table(u'xforms_block', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('block_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['Block'])

        # Adding model 'Employee'
        db.create_table(u'xforms_employee', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('emp_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('msisdn', self.gf('app_manager.models.CalleridField')(max_length=20)),
        ))
        db.send_create_signal(u'xforms', ['Employee'])

        # Adding M2M table for field block on 'Employee'
        m2m_table_name = db.shorten_name(u'xforms_employee_block')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('employee', models.ForeignKey(orm[u'xforms.employee'], null=False)),
            ('block', models.ForeignKey(orm[u'xforms.block'], null=False))
        ))
        db.create_unique(m2m_table_name, ['employee_id', 'block_id'])

        # Adding model 'Cluster'
        db.create_table(u'xforms_cluster', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cl_id', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'])),
        ))
        db.send_create_signal(u'xforms', ['Cluster'])

        # Adding model 'Qualification'
        db.create_table(u'xforms_qualification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('qual_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['Qualification'])

        # Adding model 'CMInterest'
        db.create_table(u'xforms_cminterest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('interest_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['CMInterest'])

        # Adding model 'MeetingType'
        db.create_table(u'xforms_meetingtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['MeetingType'])

        # Adding model 'CMActivity'
        db.create_table(u'xforms_cmactivity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('activity_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['CMActivity'])

        # Adding model 'SHGActivity'
        db.create_table(u'xforms_shgactivity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('activity_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['SHGActivity'])

        # Adding model 'CMCategory'
        db.create_table(u'xforms_cmcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['CMCategory'])

        # Adding model 'HouseholdIncomeDetail'
        db.create_table(u'xforms_householdincomedetail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hid_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'xforms', ['HouseholdIncomeDetail'])

        # Adding model 'XForm'
        db.create_table(u'xforms_xform', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form_id', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('form_version', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('uuid', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('imei', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('emp_id', self.gf('django.db.models.fields.IntegerField')()),
            ('cluster_id', self.gf('django.db.models.fields.IntegerField')()),
            ('block_id', self.gf('django.db.models.fields.IntegerField')()),
            ('survey_id', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('survey_json', self.gf('django.db.models.fields.TextField')()),
            ('survey_date', self.gf('django.db.models.fields.DateField')()),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['XForm'])

        # Adding model 'Panchayat'
        db.create_table(u'xforms_panchayat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('panchayat_id', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('village', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['Panchayat'])

        # Adding model 'VillageOffice'
        db.create_table(u'xforms_villageoffice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('vo_id', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('cluster', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'])),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['VillageOffice'])

        # Adding model 'CommunityManager'
        db.create_table(u'xforms_communitymanager', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('cm_id', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('msisdn', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('age', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('panchayat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Panchayat'])),
            ('qualification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Qualification'], null=True)),
            ('age_jv', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('cm_interest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CMInterest'], null=True)),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'])),
            ('cluster', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'])),
            ('mv_suggestion', self.gf('django.db.models.fields.TextField')()),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['CommunityManager'])

        # Adding model 'SHG'
        db.create_table(u'xforms_shg', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('shg_id', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('cm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CommunityManager'])),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'])),
            ('cluster', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'])),
            ('panchayat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Panchayat'], null=True)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['SHG'])

        # Adding model 'SHGMember'
        db.create_table(u'xforms_shgmember', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('msisdn', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('age', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('shg', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.SHG'])),
            ('qualification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Qualification'], null=True)),
            ('working_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('kids_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('age_jv', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('mv_suggestion', self.gf('django.db.models.fields.TextField')()),
            ('is_leader', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('is_active', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['SHGMember'])

        # Adding M2M table for field hh_inc_detail on 'SHGMember'
        m2m_table_name = db.shorten_name(u'xforms_shgmember_hh_inc_detail')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('shgmember', models.ForeignKey(orm[u'xforms.shgmember'], null=False)),
            ('householdincomedetail', models.ForeignKey(orm[u'xforms.householdincomedetail'], null=False))
        ))
        db.create_unique(m2m_table_name, ['shgmember_id', 'householdincomedetail_id'])

        # Adding model 'SHGLeaderInfo'
        db.create_table(u'xforms_shgleaderinfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('msisdn', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('shg', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.SHG'])),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['SHGLeaderInfo'])

        # Adding model 'CMTrainingRecord'
        db.create_table(u'xforms_cmtrainingrecord', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('meeting_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.MeetingType'])),
            ('vo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.VillageOffice'], null=True)),
            ('panchayat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Panchayat'])),
            ('pci_wshop_module', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('pci_location', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['CMTrainingRecord'])

        # Adding M2M table for field cm on 'CMTrainingRecord'
        m2m_table_name = db.shorten_name(u'xforms_cmtrainingrecord_cm')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cmtrainingrecord', models.ForeignKey(orm[u'xforms.cmtrainingrecord'], null=False)),
            ('communitymanager', models.ForeignKey(orm[u'xforms.communitymanager'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cmtrainingrecord_id', 'communitymanager_id'])

        # Adding M2M table for field cm_activity on 'CMTrainingRecord'
        m2m_table_name = db.shorten_name(u'xforms_cmtrainingrecord_cm_activity')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cmtrainingrecord', models.ForeignKey(orm[u'xforms.cmtrainingrecord'], null=False)),
            ('cmactivity', models.ForeignKey(orm[u'xforms.cmactivity'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cmtrainingrecord_id', 'cmactivity_id'])

        # Adding M2M table for field shg_leader_info on 'CMTrainingRecord'
        m2m_table_name = db.shorten_name(u'xforms_cmtrainingrecord_shg_leader_info')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cmtrainingrecord', models.ForeignKey(orm[u'xforms.cmtrainingrecord'], null=False)),
            ('shgleaderinfo', models.ForeignKey(orm[u'xforms.shgleaderinfo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cmtrainingrecord_id', 'shgleaderinfo_id'])

        # Adding model 'SHGMeetingRecord'
        db.create_table(u'xforms_shgmeetingrecord', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('cm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CommunityManager'])),
            ('shg', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.SHG'])),
            ('member_count', self.gf('django.db.models.fields.IntegerField')()),
            ('member_with_phone', self.gf('django.db.models.fields.IntegerField')()),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['SHGMeetingRecord'])

        # Adding M2M table for field shg_member on 'SHGMeetingRecord'
        m2m_table_name = db.shorten_name(u'xforms_shgmeetingrecord_shg_member')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('shgmeetingrecord', models.ForeignKey(orm[u'xforms.shgmeetingrecord'], null=False)),
            ('shgmember', models.ForeignKey(orm[u'xforms.shgmember'], null=False))
        ))
        db.create_unique(m2m_table_name, ['shgmeetingrecord_id', 'shgmember_id'])

        # Adding M2M table for field shg_activity on 'SHGMeetingRecord'
        m2m_table_name = db.shorten_name(u'xforms_shgmeetingrecord_shg_activity')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('shgmeetingrecord', models.ForeignKey(orm[u'xforms.shgmeetingrecord'], null=False)),
            ('shgactivity', models.ForeignKey(orm[u'xforms.shgactivity'], null=False))
        ))
        db.create_unique(m2m_table_name, ['shgmeetingrecord_id', 'shgactivity_id'])

        # Adding model 'CMTrackerRecord'
        db.create_table(u'xforms_cmtrackerrecord', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.XForm'], null=True)),
            ('cm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CommunityManager'])),
            ('meeting_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.MeetingType'])),
            ('cm_training_count', self.gf('django.db.models.fields.IntegerField')()),
            ('shg_meeting_count', self.gf('django.db.models.fields.IntegerField')()),
            ('q1_response', self.gf('django.db.models.fields.IntegerField')()),
            ('q2_response', self.gf('django.db.models.fields.IntegerField')()),
            ('q3_response', self.gf('django.db.models.fields.IntegerField')()),
            ('q4_response', self.gf('django.db.models.fields.IntegerField')()),
            ('q5_response', self.gf('django.db.models.fields.IntegerField')()),
            ('q6_response', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('q7_response', self.gf('django.db.models.fields.IntegerField')()),
            ('cm_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CMCategory'])),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'xforms', ['CMTrackerRecord'])


    def backwards(self, orm):
        # Deleting model 'Block'
        db.delete_table(u'xforms_block')

        # Deleting model 'Employee'
        db.delete_table(u'xforms_employee')

        # Removing M2M table for field block on 'Employee'
        db.delete_table(db.shorten_name(u'xforms_employee_block'))

        # Deleting model 'Cluster'
        db.delete_table(u'xforms_cluster')

        # Deleting model 'Qualification'
        db.delete_table(u'xforms_qualification')

        # Deleting model 'CMInterest'
        db.delete_table(u'xforms_cminterest')

        # Deleting model 'MeetingType'
        db.delete_table(u'xforms_meetingtype')

        # Deleting model 'CMActivity'
        db.delete_table(u'xforms_cmactivity')

        # Deleting model 'SHGActivity'
        db.delete_table(u'xforms_shgactivity')

        # Deleting model 'CMCategory'
        db.delete_table(u'xforms_cmcategory')

        # Deleting model 'HouseholdIncomeDetail'
        db.delete_table(u'xforms_householdincomedetail')

        # Deleting model 'XForm'
        db.delete_table(u'xforms_xform')

        # Deleting model 'Panchayat'
        db.delete_table(u'xforms_panchayat')

        # Deleting model 'VillageOffice'
        db.delete_table(u'xforms_villageoffice')

        # Deleting model 'CommunityManager'
        db.delete_table(u'xforms_communitymanager')

        # Deleting model 'SHG'
        db.delete_table(u'xforms_shg')

        # Deleting model 'SHGMember'
        db.delete_table(u'xforms_shgmember')

        # Removing M2M table for field hh_inc_detail on 'SHGMember'
        db.delete_table(db.shorten_name(u'xforms_shgmember_hh_inc_detail'))

        # Deleting model 'SHGLeaderInfo'
        db.delete_table(u'xforms_shgleaderinfo')

        # Deleting model 'CMTrainingRecord'
        db.delete_table(u'xforms_cmtrainingrecord')

        # Removing M2M table for field cm on 'CMTrainingRecord'
        db.delete_table(db.shorten_name(u'xforms_cmtrainingrecord_cm'))

        # Removing M2M table for field cm_activity on 'CMTrainingRecord'
        db.delete_table(db.shorten_name(u'xforms_cmtrainingrecord_cm_activity'))

        # Removing M2M table for field shg_leader_info on 'CMTrainingRecord'
        db.delete_table(db.shorten_name(u'xforms_cmtrainingrecord_shg_leader_info'))

        # Deleting model 'SHGMeetingRecord'
        db.delete_table(u'xforms_shgmeetingrecord')

        # Removing M2M table for field shg_member on 'SHGMeetingRecord'
        db.delete_table(db.shorten_name(u'xforms_shgmeetingrecord_shg_member'))

        # Removing M2M table for field shg_activity on 'SHGMeetingRecord'
        db.delete_table(db.shorten_name(u'xforms_shgmeetingrecord_shg_activity'))

        # Deleting model 'CMTrackerRecord'
        db.delete_table(u'xforms_cmtrackerrecord')


    models = {
        u'xforms.block': {
            'Meta': {'object_name': 'Block'},
            'block_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cluster': {
            'Meta': {'object_name': 'Cluster'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cl_id': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmactivity': {
            'Meta': {'object_name': 'CMActivity'},
            'activity_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmcategory': {
            'Meta': {'object_name': 'CMCategory'},
            'category_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cminterest': {
            'Meta': {'object_name': 'CMInterest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interest_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmtrackerrecord': {
            'Meta': {'object_name': 'CMTrackerRecord'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'cm_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CMCategory']"}),
            'cm_training_count': ('django.db.models.fields.IntegerField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'q1_response': ('django.db.models.fields.IntegerField', [], {}),
            'q2_response': ('django.db.models.fields.IntegerField', [], {}),
            'q3_response': ('django.db.models.fields.IntegerField', [], {}),
            'q4_response': ('django.db.models.fields.IntegerField', [], {}),
            'q5_response': ('django.db.models.fields.IntegerField', [], {}),
            'q6_response': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'q7_response': ('django.db.models.fields.IntegerField', [], {}),
            'shg_meeting_count': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.cmtrainingrecord': {
            'Meta': {'object_name': 'CMTrainingRecord'},
            'cm': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CommunityManager']", 'null': 'True', 'symmetrical': 'False'}),
            'cm_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CMActivity']", 'null': 'True', 'symmetrical': 'False'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']"}),
            'pci_location': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'pci_wshop_module': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg_leader_info': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGLeaderInfo']", 'null': 'True', 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.VillageOffice']", 'null': 'True'})
        },
        u'xforms.communitymanager': {
            'Meta': {'object_name': 'CommunityManager'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm_id': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'cm_interest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CMInterest']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']"}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.employee': {
            'Meta': {'object_name': 'Employee'},
            'block': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.Block']", 'symmetrical': 'False'}),
            'emp_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.householdincomedetail': {
            'Meta': {'object_name': 'HouseholdIncomeDetail'},
            'hid_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.meetingtype': {
            'Meta': {'object_name': 'MeetingType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        u'xforms.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.qualification': {
            'Meta': {'object_name': 'Qualification'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qual_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        u'xforms.shg': {
            'Meta': {'object_name': 'SHG'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'shg_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgactivity': {
            'Meta': {'object_name': 'SHGActivity'},
            'activity_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.shgleaderinfo': {
            'Meta': {'object_name': 'SHGLeaderInfo'},
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmeetingrecord': {
            'Meta': {'object_name': 'SHGMeetingRecord'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member_count': ('django.db.models.fields.IntegerField', [], {}),
            'member_with_phone': ('django.db.models.fields.IntegerField', [], {}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'shg_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGActivity']", 'symmetrical': 'False'}),
            'shg_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGMember']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmember': {
            'Meta': {'object_name': 'SHGMember'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            'hh_inc_detail': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.HouseholdIncomeDetail']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'is_leader': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'kids_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'working_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.villageoffice': {
            'Meta': {'object_name': 'VillageOffice'},
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo_id': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'xforms.xform': {
            'Meta': {'object_name': 'XForm'},
            'block_id': ('django.db.models.fields.IntegerField', [], {}),
            'cluster_id': ('django.db.models.fields.IntegerField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'emp_id': ('django.db.models.fields.IntegerField', [], {}),
            'form_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'form_version': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'survey_date': ('django.db.models.fields.DateField', [], {}),
            'survey_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'survey_json': ('django.db.models.fields.TextField', [], {}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['xforms']