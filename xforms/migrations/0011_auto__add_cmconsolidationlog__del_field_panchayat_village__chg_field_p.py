# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CMConsolidationLog'
        db.create_table(u'xforms_cmconsolidationlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('original_cm_id', self.gf('django.db.models.fields.IntegerField')()),
            ('merged_cm_id', self.gf('django.db.models.fields.IntegerField')()),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'xforms', ['CMConsolidationLog'])

        # Deleting field 'Panchayat.village'
        db.delete_column(u'xforms_panchayat', 'village')


        # Changing field 'Panchayat.panchayat_id'
        db.alter_column(u'xforms_panchayat', 'panchayat_id', self.gf('django.db.models.fields.IntegerField')(null=True))
        # Adding field 'CommunityManager.alt_msisdn'
        db.add_column(u'xforms_communitymanager', 'alt_msisdn',
                      self.gf('app_manager.models.CalleridField')(default='', max_length=20),
                      keep_default=False)

        # Adding field 'CommunityManager.creation_date'
        db.add_column(u'xforms_communitymanager', 'creation_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)

        # Adding field 'CommunityManager.panchayat_name'
        db.add_column(u'xforms_communitymanager', 'panchayat_name',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding field 'CommunityManager.village'
        db.add_column(u'xforms_communitymanager', 'village',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding field 'CommunityManager.vo_name'
        db.add_column(u'xforms_communitymanager', 'vo_name',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)


        # Changing field 'CommunityManager.cm_id'
        db.alter_column(u'xforms_communitymanager', 'cm_id', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'CommunityManager.mv_suggestion'
        db.alter_column(u'xforms_communitymanager', 'mv_suggestion', self.gf('django.db.models.fields.TextField')(null=True))
        # Adding field 'SHGMember.creation_date'
        db.add_column(u'xforms_shgmember', 'creation_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)

        # Adding field 'SHGMember.block'
        db.add_column(u'xforms_shgmember', 'block',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'SHGMember.cluster'
        db.add_column(u'xforms_shgmember', 'cluster',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'SHGMember.village'
        db.add_column(u'xforms_shgmember', 'village',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding field 'SHGMember.shg_name'
        db.add_column(u'xforms_shgmember', 'shg_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=64),
                      keep_default=False)

        # Adding field 'SHGMember.cm_id'
        db.add_column(u'xforms_shgmember', 'cm_id',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)


        # Changing field 'SHGMember.mv_suggestion'
        db.alter_column(u'xforms_shgmember', 'mv_suggestion', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'SHGMember.shg'
        db.alter_column(u'xforms_shgmember', 'shg_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.SHG'], null=True))
        # Adding field 'CMProfileUpdationLog.original_cm_id'
        db.add_column(u'xforms_cmprofileupdationlog', 'original_cm_id',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)


        # Changing field 'CMProfileUpdationLog.cm'
        db.alter_column(u'xforms_cmprofileupdationlog', 'cm_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CommunityManager'], null=True))
        # Adding field 'CMTrainingRecord.block'
        db.add_column(u'xforms_cmtrainingrecord', 'block',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'], null=True),
                      keep_default=False)

        # Adding field 'CMTrainingRecord.cluster'
        db.add_column(u'xforms_cmtrainingrecord', 'cluster',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'], null=True),
                      keep_default=False)

        # Adding field 'CMTrainingRecord.creation_date'
        db.add_column(u'xforms_cmtrainingrecord', 'creation_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)

        # Adding field 'CMTrainingRecord.vo_name'
        db.add_column(u'xforms_cmtrainingrecord', 'vo_name',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding field 'CMTrainingRecord.panchayat_name'
        db.add_column(u'xforms_cmtrainingrecord', 'panchayat_name',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding M2M table for field shg_member_info on 'CMTrainingRecord'
        m2m_table_name = db.shorten_name(u'xforms_cmtrainingrecord_shg_member_info')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cmtrainingrecord', models.ForeignKey(orm[u'xforms.cmtrainingrecord'], null=False)),
            ('shgmember', models.ForeignKey(orm[u'xforms.shgmember'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cmtrainingrecord_id', 'shgmember_id'])

        # Adding field 'SHGMeetingRecord.block'
        db.add_column(u'xforms_shgmeetingrecord', 'block',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'], null=True),
                      keep_default=False)

        # Adding field 'SHGMeetingRecord.cluster'
        db.add_column(u'xforms_shgmeetingrecord', 'cluster',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'], null=True),
                      keep_default=False)

        # Adding field 'SHGMeetingRecord.creation_date'
        db.add_column(u'xforms_shgmeetingrecord', 'creation_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)

        # Adding field 'SHGMeetingRecord.shg_name'
        db.add_column(u'xforms_shgmeetingrecord', 'shg_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=64),
                      keep_default=False)


        # Changing field 'SHGMeetingRecord.shg'
        db.alter_column(u'xforms_shgmeetingrecord', 'shg_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.SHG'], null=True))
        # Adding field 'CMTrackerRecord.block'
        db.add_column(u'xforms_cmtrackerrecord', 'block',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Block'], null=True),
                      keep_default=False)

        # Adding field 'CMTrackerRecord.cluster'
        db.add_column(u'xforms_cmtrackerrecord', 'cluster',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.Cluster'], null=True),
                      keep_default=False)

        # Adding field 'CMTrackerRecord.creation_date'
        db.add_column(u'xforms_cmtrackerrecord', 'creation_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'CMConsolidationLog'
        db.delete_table(u'xforms_cmconsolidationlog')

        # Adding field 'Panchayat.village'
        db.add_column(u'xforms_panchayat', 'village',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=64),
                      keep_default=False)


        # Changing field 'Panchayat.panchayat_id'
        db.alter_column(u'xforms_panchayat', 'panchayat_id', self.gf('django.db.models.fields.IntegerField')(default=''))
        # Deleting field 'CommunityManager.alt_msisdn'
        db.delete_column(u'xforms_communitymanager', 'alt_msisdn')

        # Deleting field 'CommunityManager.creation_date'
        db.delete_column(u'xforms_communitymanager', 'creation_date')

        # Deleting field 'CommunityManager.panchayat_name'
        db.delete_column(u'xforms_communitymanager', 'panchayat_name')

        # Deleting field 'CommunityManager.village'
        db.delete_column(u'xforms_communitymanager', 'village')

        # Deleting field 'CommunityManager.vo_name'
        db.delete_column(u'xforms_communitymanager', 'vo_name')


        # Changing field 'CommunityManager.cm_id'
        db.alter_column(u'xforms_communitymanager', 'cm_id', self.gf('django.db.models.fields.IntegerField')(default=''))

        # Changing field 'CommunityManager.mv_suggestion'
        db.alter_column(u'xforms_communitymanager', 'mv_suggestion', self.gf('django.db.models.fields.TextField')(default=''))
        # Deleting field 'SHGMember.creation_date'
        db.delete_column(u'xforms_shgmember', 'creation_date')

        # Deleting field 'SHGMember.block'
        db.delete_column(u'xforms_shgmember', 'block_id')

        # Deleting field 'SHGMember.cluster'
        db.delete_column(u'xforms_shgmember', 'cluster_id')

        # Deleting field 'SHGMember.village'
        db.delete_column(u'xforms_shgmember', 'village')

        # Deleting field 'SHGMember.shg_name'
        db.delete_column(u'xforms_shgmember', 'shg_name')

        # Deleting field 'SHGMember.cm_id'
        db.delete_column(u'xforms_shgmember', 'cm_id')


        # Changing field 'SHGMember.mv_suggestion'
        db.alter_column(u'xforms_shgmember', 'mv_suggestion', self.gf('django.db.models.fields.TextField')(default=''))

        # Changing field 'SHGMember.shg'
        db.alter_column(u'xforms_shgmember', 'shg_id', self.gf('django.db.models.fields.related.ForeignKey')(default='', to=orm['xforms.SHG']))
        # Deleting field 'CMProfileUpdationLog.original_cm_id'
        db.delete_column(u'xforms_cmprofileupdationlog', 'original_cm_id')


        # Changing field 'CMProfileUpdationLog.cm'
        db.alter_column(u'xforms_cmprofileupdationlog', 'cm_id', self.gf('django.db.models.fields.related.ForeignKey')(default='', to=orm['xforms.CommunityManager']))
        # Deleting field 'CMTrainingRecord.block'
        db.delete_column(u'xforms_cmtrainingrecord', 'block_id')

        # Deleting field 'CMTrainingRecord.cluster'
        db.delete_column(u'xforms_cmtrainingrecord', 'cluster_id')

        # Deleting field 'CMTrainingRecord.creation_date'
        db.delete_column(u'xforms_cmtrainingrecord', 'creation_date')

        # Deleting field 'CMTrainingRecord.vo_name'
        db.delete_column(u'xforms_cmtrainingrecord', 'vo_name')

        # Deleting field 'CMTrainingRecord.panchayat_name'
        db.delete_column(u'xforms_cmtrainingrecord', 'panchayat_name')

        # Removing M2M table for field shg_member_info on 'CMTrainingRecord'
        db.delete_table(db.shorten_name(u'xforms_cmtrainingrecord_shg_member_info'))

        # Deleting field 'SHGMeetingRecord.block'
        db.delete_column(u'xforms_shgmeetingrecord', 'block_id')

        # Deleting field 'SHGMeetingRecord.cluster'
        db.delete_column(u'xforms_shgmeetingrecord', 'cluster_id')

        # Deleting field 'SHGMeetingRecord.creation_date'
        db.delete_column(u'xforms_shgmeetingrecord', 'creation_date')

        # Deleting field 'SHGMeetingRecord.shg_name'
        db.delete_column(u'xforms_shgmeetingrecord', 'shg_name')


        # Changing field 'SHGMeetingRecord.shg'
        db.alter_column(u'xforms_shgmeetingrecord', 'shg_id', self.gf('django.db.models.fields.related.ForeignKey')(default='', to=orm['xforms.SHG']))
        # Deleting field 'CMTrackerRecord.block'
        db.delete_column(u'xforms_cmtrackerrecord', 'block_id')

        # Deleting field 'CMTrackerRecord.cluster'
        db.delete_column(u'xforms_cmtrackerrecord', 'cluster_id')

        # Deleting field 'CMTrackerRecord.creation_date'
        db.delete_column(u'xforms_cmtrackerrecord', 'creation_date')


    models = {
        u'xforms.block': {
            'Meta': {'object_name': 'Block'},
            'block_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cluster': {
            'Meta': {'object_name': 'Cluster'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cl_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmactivity': {
            'Meta': {'object_name': 'CMActivity'},
            'activity_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmcategory': {
            'Meta': {'object_name': 'CMCategory'},
            'category_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmconsolidationlog': {
            'Meta': {'object_name': 'CMConsolidationLog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'merged_cm_id': ('django.db.models.fields.IntegerField', [], {}),
            'original_cm_id': ('django.db.models.fields.IntegerField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'xforms.cminterest': {
            'Meta': {'object_name': 'CMInterest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interest_id': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmprofileupdationlog': {
            'Meta': {'object_name': 'CMProfileUpdationLog'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original_cm_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'prev_profile': ('django.db.models.fields.TextField', [], {}),
            'updated_profile': ('django.db.models.fields.TextField', [], {})
        },
        u'xforms.cmtrackerrecord': {
            'Meta': {'object_name': 'CMTrackerRecord'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True'}),
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'cm_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CMCategory']", 'null': 'True'}),
            'cm_training_count': ('django.db.models.fields.IntegerField', [], {}),
            'creation_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'q1_response': ('django.db.models.fields.IntegerField', [], {}),
            'q2_response': ('django.db.models.fields.IntegerField', [], {}),
            'q3_response': ('django.db.models.fields.IntegerField', [], {}),
            'q4_response': ('django.db.models.fields.IntegerField', [], {}),
            'q5_response': ('django.db.models.fields.IntegerField', [], {}),
            'q6_response': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'q7_response': ('django.db.models.fields.IntegerField', [], {}),
            'shg_meeting_count': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.cmtrainingrecord': {
            'Meta': {'object_name': 'CMTrainingRecord'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True'}),
            'cm': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CommunityManager']", 'null': 'True', 'symmetrical': 'False'}),
            'cm_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CMActivity']", 'null': 'True', 'symmetrical': 'False'}),
            'creation_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'panchayat_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'pci_location': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'pci_wshop_module': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg_leader_info': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGLeaderInfo']", 'null': 'True', 'symmetrical': 'False'}),
            'shg_member_info': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGMember']", 'null': 'True', 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.VillageOffice']", 'null': 'True'}),
            'vo_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'xforms.communitymanager': {
            'Meta': {'object_name': 'CommunityManager'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'alt_msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cm_interest': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['xforms.CMInterest']", 'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True', 'blank': 'True'}),
            'panchayat_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'vo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.VillageOffice']", 'null': 'True', 'blank': 'True'}),
            'vo_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'xforms.employee': {
            'Meta': {'object_name': 'Employee'},
            'block': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.Block']", 'symmetrical': 'False'}),
            'emp_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.householdincomedetail': {
            'Meta': {'object_name': 'HouseholdIncomeDetail'},
            'hid_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.meetingtype': {
            'Meta': {'object_name': 'MeetingType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'xforms.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True', 'blank': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.qualification': {
            'Meta': {'object_name': 'Qualification'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qual_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'xforms.shg': {
            'Meta': {'object_name': 'SHG'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'shg_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgactivity': {
            'Meta': {'object_name': 'SHGActivity'},
            'activity_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.shgleaderinfo': {
            'Meta': {'object_name': 'SHGLeaderInfo'},
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']", 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmeetingrecord': {
            'Meta': {'object_name': 'SHGMeetingRecord'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True'}),
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'creation_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member_count': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'member_with_phone': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']", 'null': 'True', 'blank': 'True'}),
            'shg_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGActivity']", 'symmetrical': 'False'}),
            'shg_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGMember']", 'symmetrical': 'False'}),
            'shg_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmember': {
            'Meta': {'object_name': 'SHGMember'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True', 'blank': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True', 'blank': 'True'}),
            'cm_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True', 'blank': 'True'}),
            'hh_inc_detail': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['xforms.HouseholdIncomeDetail']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'is_leader': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'kids_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True', 'blank': 'True'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']", 'null': 'True', 'blank': 'True'}),
            'shg_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'working_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'xforms.villageoffice': {
            'Meta': {'object_name': 'VillageOffice'},
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'xforms.xform': {
            'Meta': {'object_name': 'XForm'},
            'block_id': ('django.db.models.fields.IntegerField', [], {}),
            'cluster_id': ('django.db.models.fields.IntegerField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'emp_id': ('django.db.models.fields.IntegerField', [], {}),
            'form_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'form_version': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'survey_date': ('django.db.models.fields.DateField', [], {}),
            'survey_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'survey_json': ('django.db.models.fields.TextField', [], {}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['xforms']