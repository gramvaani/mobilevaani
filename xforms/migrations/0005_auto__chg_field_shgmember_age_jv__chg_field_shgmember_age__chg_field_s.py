# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SHGMember.age_jv'
        db.alter_column(u'xforms_shgmember', 'age_jv', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

        # Changing field 'SHGMember.age'
        db.alter_column(u'xforms_shgmember', 'age', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

        # Changing field 'SHGMember.is_active'
        db.alter_column(u'xforms_shgmember', 'is_active', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True))

        # Changing field 'SHGMember.working_status'
        db.alter_column(u'xforms_shgmember', 'working_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True))

        # Changing field 'SHGMember.kids_count'
        db.alter_column(u'xforms_shgmember', 'kids_count', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

        # Changing field 'SHGMember.is_leader'
        db.alter_column(u'xforms_shgmember', 'is_leader', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SHGMember.age_jv'
        raise RuntimeError("Cannot reverse this migration. 'SHGMember.age_jv' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SHGMember.age_jv'
        db.alter_column(u'xforms_shgmember', 'age_jv', self.gf('django.db.models.fields.PositiveIntegerField')())

        # User chose to not deal with backwards NULL issues for 'SHGMember.age'
        raise RuntimeError("Cannot reverse this migration. 'SHGMember.age' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SHGMember.age'
        db.alter_column(u'xforms_shgmember', 'age', self.gf('django.db.models.fields.PositiveIntegerField')())

        # User chose to not deal with backwards NULL issues for 'SHGMember.is_active'
        raise RuntimeError("Cannot reverse this migration. 'SHGMember.is_active' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SHGMember.is_active'
        db.alter_column(u'xforms_shgmember', 'is_active', self.gf('django.db.models.fields.PositiveSmallIntegerField')())

        # User chose to not deal with backwards NULL issues for 'SHGMember.working_status'
        raise RuntimeError("Cannot reverse this migration. 'SHGMember.working_status' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SHGMember.working_status'
        db.alter_column(u'xforms_shgmember', 'working_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')())

        # User chose to not deal with backwards NULL issues for 'SHGMember.kids_count'
        raise RuntimeError("Cannot reverse this migration. 'SHGMember.kids_count' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SHGMember.kids_count'
        db.alter_column(u'xforms_shgmember', 'kids_count', self.gf('django.db.models.fields.PositiveIntegerField')())

        # User chose to not deal with backwards NULL issues for 'SHGMember.is_leader'
        raise RuntimeError("Cannot reverse this migration. 'SHGMember.is_leader' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SHGMember.is_leader'
        db.alter_column(u'xforms_shgmember', 'is_leader', self.gf('django.db.models.fields.PositiveSmallIntegerField')())

    models = {
        u'xforms.block': {
            'Meta': {'object_name': 'Block'},
            'block_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cluster': {
            'Meta': {'object_name': 'Cluster'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cl_id': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmactivity': {
            'Meta': {'object_name': 'CMActivity'},
            'activity_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmcategory': {
            'Meta': {'object_name': 'CMCategory'},
            'category_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cminterest': {
            'Meta': {'object_name': 'CMInterest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interest_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmtrackerrecord': {
            'Meta': {'object_name': 'CMTrackerRecord'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'cm_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CMCategory']"}),
            'cm_training_count': ('django.db.models.fields.IntegerField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'q1_response': ('django.db.models.fields.IntegerField', [], {}),
            'q2_response': ('django.db.models.fields.IntegerField', [], {}),
            'q3_response': ('django.db.models.fields.IntegerField', [], {}),
            'q4_response': ('django.db.models.fields.IntegerField', [], {}),
            'q5_response': ('django.db.models.fields.IntegerField', [], {}),
            'q6_response': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'q7_response': ('django.db.models.fields.IntegerField', [], {}),
            'shg_meeting_count': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.cmtrainingrecord': {
            'Meta': {'object_name': 'CMTrainingRecord'},
            'cm': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CommunityManager']", 'null': 'True', 'symmetrical': 'False'}),
            'cm_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CMActivity']", 'null': 'True', 'symmetrical': 'False'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'pci_location': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'pci_wshop_module': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg_leader_info': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGLeaderInfo']", 'null': 'True', 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.VillageOffice']", 'null': 'True'})
        },
        u'xforms.communitymanager': {
            'Meta': {'object_name': 'CommunityManager'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm_id': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'cm_interest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CMInterest']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.employee': {
            'Meta': {'object_name': 'Employee'},
            'block': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.Block']", 'symmetrical': 'False'}),
            'emp_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.householdincomedetail': {
            'Meta': {'object_name': 'HouseholdIncomeDetail'},
            'hid_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.meetingtype': {
            'Meta': {'object_name': 'MeetingType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        u'xforms.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.qualification': {
            'Meta': {'object_name': 'Qualification'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qual_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        u'xforms.shg': {
            'Meta': {'object_name': 'SHG'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'shg_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgactivity': {
            'Meta': {'object_name': 'SHGActivity'},
            'activity_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.shgleaderinfo': {
            'Meta': {'object_name': 'SHGLeaderInfo'},
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmeetingrecord': {
            'Meta': {'object_name': 'SHGMeetingRecord'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member_count': ('django.db.models.fields.IntegerField', [], {}),
            'member_with_phone': ('django.db.models.fields.IntegerField', [], {}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'shg_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGActivity']", 'symmetrical': 'False'}),
            'shg_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGMember']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmember': {
            'Meta': {'object_name': 'SHGMember'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            'hh_inc_detail': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.HouseholdIncomeDetail']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'is_leader': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'kids_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'working_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'})
        },
        u'xforms.villageoffice': {
            'Meta': {'object_name': 'VillageOffice'},
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo_id': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'xforms.xform': {
            'Meta': {'object_name': 'XForm'},
            'block_id': ('django.db.models.fields.IntegerField', [], {}),
            'cluster_id': ('django.db.models.fields.IntegerField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'emp_id': ('django.db.models.fields.IntegerField', [], {}),
            'form_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'form_version': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'survey_date': ('django.db.models.fields.DateField', [], {}),
            'survey_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'survey_json': ('django.db.models.fields.TextField', [], {}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['xforms']