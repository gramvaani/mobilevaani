# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'CommunityManager.cm_interest'
        db.delete_column(u'xforms_communitymanager', 'cm_interest_id')

        # Adding M2M table for field cm_interest on 'CommunityManager'
        m2m_table_name = db.shorten_name(u'xforms_communitymanager_cm_interest')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('communitymanager', models.ForeignKey(orm[u'xforms.communitymanager'], null=False)),
            ('cminterest', models.ForeignKey(orm[u'xforms.cminterest'], null=False))
        ))
        db.create_unique(m2m_table_name, ['communitymanager_id', 'cminterest_id'])


    def backwards(self, orm):
        # Adding field 'CommunityManager.cm_interest'
        db.add_column(u'xforms_communitymanager', 'cm_interest',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xforms.CMInterest'], null=True),
                      keep_default=False)

        # Removing M2M table for field cm_interest on 'CommunityManager'
        db.delete_table(db.shorten_name(u'xforms_communitymanager_cm_interest'))


    models = {
        u'xforms.block': {
            'Meta': {'object_name': 'Block'},
            'block_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cluster': {
            'Meta': {'object_name': 'Cluster'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cl_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmactivity': {
            'Meta': {'object_name': 'CMActivity'},
            'activity_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmcategory': {
            'Meta': {'object_name': 'CMCategory'},
            'category_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cminterest': {
            'Meta': {'object_name': 'CMInterest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interest_id': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.cmprofileupdationlog': {
            'Meta': {'object_name': 'CMProfileUpdationLog'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_profile': ('django.db.models.fields.TextField', [], {}),
            'updated_profile': ('django.db.models.fields.TextField', [], {})
        },
        u'xforms.cmtrackerrecord': {
            'Meta': {'object_name': 'CMTrackerRecord'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'cm_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CMCategory']", 'null': 'True'}),
            'cm_training_count': ('django.db.models.fields.IntegerField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'q1_response': ('django.db.models.fields.IntegerField', [], {}),
            'q2_response': ('django.db.models.fields.IntegerField', [], {}),
            'q3_response': ('django.db.models.fields.IntegerField', [], {}),
            'q4_response': ('django.db.models.fields.IntegerField', [], {}),
            'q5_response': ('django.db.models.fields.IntegerField', [], {}),
            'q6_response': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'q7_response': ('django.db.models.fields.IntegerField', [], {}),
            'shg_meeting_count': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.cmtrainingrecord': {
            'Meta': {'object_name': 'CMTrainingRecord'},
            'cm': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CommunityManager']", 'null': 'True', 'symmetrical': 'False'}),
            'cm_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CMActivity']", 'null': 'True', 'symmetrical': 'False'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.MeetingType']"}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'pci_location': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'pci_wshop_module': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg_leader_info': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGLeaderInfo']", 'null': 'True', 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.VillageOffice']", 'null': 'True'})
        },
        u'xforms.communitymanager': {
            'Meta': {'object_name': 'CommunityManager'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm_id': ('django.db.models.fields.IntegerField', [], {}),
            'cm_interest': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.CMInterest']", 'null': 'True', 'symmetrical': 'False'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.employee': {
            'Meta': {'object_name': 'Employee'},
            'block': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.Block']", 'symmetrical': 'False'}),
            'emp_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.householdincomedetail': {
            'Meta': {'object_name': 'HouseholdIncomeDetail'},
            'hid_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.meetingtype': {
            'Meta': {'object_name': 'MeetingType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'xforms.panchayat': {
            'Meta': {'object_name': 'Panchayat'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']", 'null': 'True'}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']", 'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.qualification': {
            'Meta': {'object_name': 'Qualification'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qual_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'xforms.shg': {
            'Meta': {'object_name': 'SHG'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Block']"}),
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'panchayat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Panchayat']", 'null': 'True'}),
            'shg_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgactivity': {
            'Meta': {'object_name': 'SHGActivity'},
            'activity_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'xforms.shgleaderinfo': {
            'Meta': {'object_name': 'SHGLeaderInfo'},
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']", 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmeetingrecord': {
            'Meta': {'object_name': 'SHGMeetingRecord'},
            'cm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.CommunityManager']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member_count': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'member_with_phone': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'shg_activity': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGActivity']", 'symmetrical': 'False'}),
            'shg_member': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.SHGMember']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'xforms.shgmember': {
            'Meta': {'object_name': 'SHGMember'},
            'age': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'age_jv': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            'hh_inc_detail': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['xforms.HouseholdIncomeDetail']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'is_leader': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'kids_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'msisdn': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'mv_suggestion': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'qualification': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Qualification']", 'null': 'True'}),
            'shg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.SHG']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'working_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'})
        },
        u'xforms.villageoffice': {
            'Meta': {'object_name': 'VillageOffice'},
            'cluster': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.Cluster']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xforms.XForm']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'vo_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'xforms.xform': {
            'Meta': {'object_name': 'XForm'},
            'block_id': ('django.db.models.fields.IntegerField', [], {}),
            'cluster_id': ('django.db.models.fields.IntegerField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'emp_id': ('django.db.models.fields.IntegerField', [], {}),
            'form_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'form_version': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'survey_date': ('django.db.models.fields.DateField', [], {}),
            'survey_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'survey_json': ('django.db.models.fields.TextField', [], {}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['xforms']