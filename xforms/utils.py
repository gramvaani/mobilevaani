from models import CMProfileUpdationLog, SHG, CMTrainingRecord,\
SHGMeetingRecord, CMTrackerRecord, SHGMember


def update_cm_references(cm_original, cm_duplicate):
    try:
        CMProfileUpdationLog.objects.filter(cm=cm_duplicate)\
            .update(cm=cm_original)
        SHG.objects.filter(cm=cm_duplicate).update(cm=cm_original)

        for record in CMTrainingRecord.objects.filter(cm=cm_duplicate):
            record.cm.remove(cm_duplicate)
            if cm_original not in record.cm.all():
                record.cm.add(cm_original)

        SHGMeetingRecord.objects.filter(cm=cm_duplicate).update(cm=cm_original)
        CMTrackerRecord.objects.filter(cm=cm_duplicate).update(cm=cm_original)
        SHGMember.objects.filter(cm_id=cm_duplicate.id).update(cm_id=cm_original.id)
    except Exception as e:
        print('Updating references for cm: {0}, error: {1}'.format(\
            cm_original.id, str(e)))