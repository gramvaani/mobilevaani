from django.db import models

from vapp.app_manager.models import Cdr, Short_code, CalleridField, SHORT_CODE_LENGTH
from vapp.customer.models import Project
from vapp.mnews.models import CALLERID_LENGTH
from vapp.media.models import Recording

from vapp.utils import load_vapp_module

CALL_ID_LENGTH = 50
OPERATOR_LENGTH = 20
CIRCLE_LENGTH = 50

OPERATORS = {
    'airtel': 'Airtel',
    'idea': 'Idea',
    'bsnl': 'BSNL',
    'vodafone': 'Vodafone',
    'reliance': 'Reliance'
}

CIRCLES = {
    'delhi': 'Delhi'
}

class Request_api_log( models.Model ):
    callerid      = CalleridField( max_length = CALLERID_LENGTH )
    called_number = models.CharField( max_length = SHORT_CODE_LENGTH )
    operator      = models.CharField( max_length = OPERATOR_LENGTH )
    circle        = models.CharField( max_length = CIRCLE_LENGTH )
    timestamp     = models.DateTimeField( auto_now_add = True )

class Access_api_log( models.Model ):
    called_number         = models.CharField( max_length = SHORT_CODE_LENGTH )
    cdr                   = models.ForeignKey( Cdr )
    call_id               = models.CharField( max_length = CALL_ID_LENGTH )
    operator              = models.CharField( max_length = OPERATOR_LENGTH )
    circle                = models.CharField( max_length = CIRCLE_LENGTH )
    processing_start_time = models.DateTimeField( null = True )
    processing_end_time   = models.DateTimeField( null = True )


def generate_ai_node_graph( parent_ai ):
    graph = { 'Nodes': [] }
    node_id = 0
    ai_node_list = [ { 'ai': parent_ai, 'node': node_id } ]
    project = parent_ai.project_ais.all()[ 0 ]
    for child_ai in project.ais.exclude( pk = parent_ai.id ):
        node_id += 1
        ai_node_list.append( { 'ai': child_ai, 'node': node_id } )
    
    generator_map = {}
    
    for each in ai_node_list:
        try:
            ai = each[ 'ai' ]
            pkg_name = ( ai.app.pkg_name ).lower()
    
            if not generator_map.has_key( pkg_name ):
                node_generator = getattr( load_vapp_module( pkg_name, 'models' ), 'generate_node_data' )
                generator_map[ pkg_name ] = node_generator
            else:
                node_generator = generator_map[ pkg_name ]
    
            graph[ 'Nodes' ].append( node_generator( ai, ai_node_list ) )
        except:
            pass
    
    return graph


class Recording_map( models.Model ):
    recording         = models.ForeignKey( Recording )
    mvas_recording_id = models.CharField( max_length = 20 )
    call_id           = models.CharField( max_length = CALL_ID_LENGTH )
