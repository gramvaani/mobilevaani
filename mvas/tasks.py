from celery.task import Task

from vapp.mvas.models import Access_api_log
from vapp.telephony.statemachine import generate_call_info, log_call_data
from vapp.log import get_request_logger

from datetime import datetime

logger = get_request_logger()

class SaveMvasDataTask( Task ):

    def run( self, call_trace ):
        try:
            processing_start_time = datetime.now()
            call_start_trace = call_trace[ 0 ]
            call_end_trace = call_trace[ -1 ]
            call_info = generate_call_info( call_start_trace, call_end_trace )
            log_call_data( call_info[ 'cdr' ], call_trace )
            call_info[ 'processing_start_time' ] = processing_start_time
            call_info[ 'processing_end_time' ] = datetime.now()
            access_log = Access_api_log( **call_info )
            access_log.save()
        except Exception, e:
            logger.exception( 'exception while saving mvas data: %s' %( e ) )