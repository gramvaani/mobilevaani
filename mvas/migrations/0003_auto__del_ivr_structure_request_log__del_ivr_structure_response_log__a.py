# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'IVR_structure_request_log'
        db.delete_table(u'mvas_ivr_structure_request_log')

        # Deleting model 'IVR_structure_response_log'
        db.delete_table(u'mvas_ivr_structure_response_log')

        # Adding model 'Access_api_log'
        db.create_table(u'mvas_access_api_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('called_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('call_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('processing_start_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('processing_end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal(u'mvas', ['Access_api_log'])

        # Adding model 'Request_api_log'
        db.create_table(u'mvas_request_api_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('callerid', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
            ('called_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'mvas', ['Request_api_log'])


    def backwards(self, orm):
        # Adding model 'IVR_structure_request_log'
        db.create_table(u'mvas_ivr_structure_request_log', (
            ('called_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('callerid', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('call_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mvas', ['IVR_structure_request_log'])

        # Adding model 'IVR_structure_response_log'
        db.create_table(u'mvas_ivr_structure_response_log', (
            ('called_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('processing_end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('processing_start_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('call_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mvas', ['IVR_structure_response_log'])

        # Deleting model 'Access_api_log'
        db.delete_table(u'mvas_access_api_log')

        # Deleting model 'Request_api_log'
        db.delete_table(u'mvas_request_api_log')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'mvas.access_api_log': {
            'Meta': {'object_name': 'Access_api_log'},
            'call_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'called_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'circle': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'processing_end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'processing_start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        },
        u'mvas.request_api_log': {
            'Meta': {'object_name': 'Request_api_log'},
            'called_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'circle': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['mvas']