# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'API_status_log'
        db.delete_table(u'mvas_api_status_log')

        # Deleting model 'Access_log'
        db.delete_table(u'mvas_access_log')

        # Adding model 'IVR_structure_request_log'
        db.create_table(u'mvas_ivr_structure_request_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('callerid', self.gf('vapp.app_manager.models.CalleridField')(max_length=20)),
            ('called_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('call_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'mvas', ['IVR_structure_request_log'])

        # Adding model 'IVR_structure_response_log'
        db.create_table(u'mvas_ivr_structure_response_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('called_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('call_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('processing_start_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('processing_end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal(u'mvas', ['IVR_structure_response_log'])


    def backwards(self, orm):
        # Adding model 'API_status_log'
        db.create_table(u'mvas_api_status_log', (
            ('status', self.gf('django.db.models.fields.CharField')(max_length=2, null=True, blank=True)),
            ('mvas_access_log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mvas.Access_log'])),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('processing_start_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('processing_end_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'mvas', ['API_status_log'])

        # Adding model 'Access_log'
        db.create_table(u'mvas_access_log', (
            ('short_code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Short_code'])),
            ('call_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('operator', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=50)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'mvas', ['Access_log'])

        # Deleting model 'IVR_structure_request_log'
        db.delete_table(u'mvas_ivr_structure_request_log')

        # Deleting model 'IVR_structure_response_log'
        db.delete_table(u'mvas_ivr_structure_response_log')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'mvas.ivr_structure_request_log': {
            'Meta': {'object_name': 'IVR_structure_request_log'},
            'call_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'called_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'circle': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mvas.ivr_structure_response_log': {
            'Meta': {'object_name': 'IVR_structure_response_log'},
            'call_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'called_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'circle': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'processing_end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'processing_start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        }
    }

    complete_apps = ['mvas']