from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.resources import Resource
from tastypie.utils import trailing_slash

import json

from models import generate_ai_node_graph, Request_api_log
from mvas.tasks import SaveMvasDataTask

from vapp.app_manager.models import get_ai_from_short_code
from vapp.telephony.utils import get_formatted_number

from vapp.log import get_request_logger

logger = get_request_logger()

class MvasDataResource( Resource ):
    class Meta:
        resource_name = 'mvas_data'
        authentication = ApiKeyAuthentication()

    def prepend_urls( self ):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_mvas_data' ), name = "api_get_mvas_data" ),
            url(r"^(?P<resource_name>%s)/save%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'save_mvas_data' ), name = "api_save_mvas_data" ),
        ]

    def get_mvas_data( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'get', 'post' ] )
        self.throttle_check( request )

        try:
            request_params = json.loads( request.body )
            number = get_formatted_number( request_params[ 'callerNumber' ] )
            if not number:
                raise Exception( "number not in correct format." )

            params = {}
            params[ 'callerid' ] = number
            params[ 'called_number' ] = request_params[ 'calledNumber' ]
            params[ 'operator' ] = request_params.get( 'operator', '' )
            params[ 'circle' ] = request_params.get( 'circle', '' )

            ai = get_ai_from_short_code( params[ 'called_number' ] )
            request_log = Request_api_log( **params )
            request_log.save()
            data = generate_ai_node_graph( ai )
            self.log_throttled_access( request )
            return self.create_response( request, { "data": data } )
        except Exception, e:
            logger.exception( "Exception while running: get_mvas_data: %s" % request )
        return self.create_response( request, { 'error': "Unable to get mvas data. Error: %s" % str( e ) } )

    def save_mvas_data( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'post' ] )
        self.throttle_check( request )

        try:
            request_params = json.loads( request.body )
            call_trace = request_params[ 'cdr' ]
            SaveMvasDataTask.delay( call_trace )
            self.log_throttled_access( request )
            return self.create_response( request, { "success": "MVAS data saved successfully." } )
        except Exception, e:
            logger.exception( "Exception while running: save_mvas_data: %s" % request )
        return self.create_response( request, { 'error': "Unable to save mvas data. Error: %s" % str( e ) } )