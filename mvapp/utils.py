import os, random

import boto3

from models import Library_folders, Resource_groups, Synced_item_info
from sync import convert_image, upload_image_to_s3, DEFAULT_BUCKET_NAME, IMAGE_DIR, get_res_lists_name_images_dict_list, is_duplicate_item, get_item_image, get_item_title_for_app
from vapp.mnews.models import News

SPECIAL_ITEMS_COUNT = 4



def upload_hdpi_images():
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(DEFAULT_BUCKET_NAME)
    s3_objs = bucket.objects.filter(Prefix= 'images/')
    for s3_obj in s3_objs:
        key_original = s3_obj.key
        if 'hdpi' in key_original:
            filepath_original = '/tmp/' + key_original[7:]
            key = key_original.replace('_hdpi', '')
            file_identifier = key[7:]
            filepath = '/tmp/' + file_identifier
            image_extension = file_identifier.split('.')[-1]
            
            try:
                s3.Bucket(DEFAULT_BUCKET_NAME).download_file(key, filepath)
                convert_image(filepath, file_identifier.replace('.'+image_extension, ''), image_extension)
                s3.Bucket(DEFAULT_BUCKET_NAME).upload_file(filepath_original, key_original)
            except:
                print(key + " could not convert or upload")
                continue


def upload_library_folders_images():
	library_folders = Library_folders.objects.all().order_by('id')
	for library_folder in library_folders:
		image = library_folder.image
		if image:
			image_path = image.get_location()
			image_name_ext = image_path.split('/')[-1]
			image_name = image_name_ext.split('.')[0]
			try:
				upload_image_to_s3(image_path, image_name, IMAGE_DIR, DEFAULT_BUCKET_NAME, in_folder=True)
			except Exception as exception:
				logger.info('exception while uploading image for media_image id : ' + str(image.id) + str(exception))
        

def get_res_group_special_items(resource_groups):
    res_groups_top_items = {}
    all_top_items = []
    for resource_group in resource_groups:
        res_lists_items_dict_list = []
        for resource_list in resource_group.resource_list.all():
            synced_meta_info_qs = Synced_item_info.objects.filter(resource_list=resource_list)
            if synced_meta_info_qs.exists():
                res_list_items_dict = {}
                res_list_items_dict['resource_list'] = resource_list
                items_list = str(synced_meta_info_qs[0].items_list).split(',')
                items_list = filter(None, items_list)
                res_list_items = [int(x) for x in items_list]
                res_list_items_rem_satisfying = []
                res_list_items_satisfying = []
                res_list_items_notsatisfying = []
                for res_list_item in res_list_items:
                    item = News.objects.get(id=res_list_item)
                    if not item.is_comment and item.rating>=4 and not item.autopublished:
                        # Recent four items are taken so that very old items do not come
                        if len(res_list_items_satisfying)<SPECIAL_ITEMS_COUNT :
                            res_list_items_satisfying.append(item)
                        else:
                            res_list_items_rem_satisfying.append(item)
                    else:
                        res_list_items_notsatisfying.append(item)
                random.shuffle(res_list_items_satisfying)
                random.shuffle(res_list_items_notsatisfying)
                res_list_items_all = res_list_items_satisfying + res_list_items_rem_satisfying + res_list_items_notsatisfying
                res_list_items_dict['items'] = res_list_items_all
                res_lists_items_dict_list.append(res_list_items_dict)
        random.shuffle(res_lists_items_dict_list)
        
        item_index = 0
        special_items = []
        special_items_dict_list = []
        while(len(special_items)!=SPECIAL_ITEMS_COUNT):
            for res_list_index in range(0, len(res_lists_items_dict_list)):
                items_list = res_lists_items_dict_list[res_list_index]['items']
                items_list = filter(None, items_list)
                if item_index<len(items_list) and (items_list[item_index] not in special_items and not is_duplicate_item(items_list[item_index], special_items)) and items_list[item_index] not in all_top_items:
                    special_items.append(items_list[item_index])
                    all_top_items.append(items_list[item_index])
                    item_dict = {}
                    item = items_list[item_index]
                    resource_list = res_lists_items_dict_list[res_list_index]['resource_list']
                    item_dict["item_id"] = str(item.id)
                    item_dict["title"] = get_item_title_for_app(item)
                    item_dict["res_id"] = str(resource_list.id)
                    item_dict["image"] = get_item_image(item, resource_list.name)
                    special_items_dict_list.append(item_dict)
                if len(special_items)==SPECIAL_ITEMS_COUNT:
                    break
                if res_list_index == len(res_lists_items_dict_list) - 1:
                    item_index = item_index + 1
        res_groups_top_items[resource_group] = special_items_dict_list
    return res_groups_top_items


def get_res_group_lib_folders(project=None, mv_app_user=None):
    resource_groups = Resource_groups.objects.all().order_by('id')
    library_folders = Library_folders.objects.all().order_by('id')
    if project:
        resource_groups = resource_groups.filter(project=project)
        library_folders = library_folders.filter(project=project)
    else:
        resource_groups = resource_groups.filter(project__isnull=True)
        library_folders = library_folders.filter(project__isnull=True)

    data = {}
    data["root_url"] = "https://s3.ap-south-1.amazonaws.com/gramvaani/"
    data["resource_group_map"] = []
    data["libraries_map"] = []

    res_groups_top_items = get_res_group_special_items(resource_groups)
    for resource_group in resource_groups:
        resource_group_dict = {}
        resource_group_dict["id"] = str(resource_group.id)
        resource_group_dict["name"] = resource_group.name
        resource_group_dict["resource_lists"] = get_res_lists_name_images_dict_list(resource_group, is_library=False)
        resource_group_dict["top_items"] = res_groups_top_items[resource_group]
        data["resource_group_map"].append(resource_group_dict)

    for library_folder in library_folders:
        library_folders_dict = {}
        library_folders_dict["id"] = str(library_folder.id)
        library_folders_dict["root_folder"] = library_folder.root_folder
        library_folders_dict["name"] = library_folder.name
        library_folders_dict["image"] = None
        if library_folder.image:
            library_folders_dict["image"] = library_folder.image.get_location().split('/')[-1]
        library_folders_dict["resource_lists"] = get_res_lists_name_images_dict_list(library_folder, is_library=True)
        library_folders_dict["folders"] = map(int, library_folder.folders.all().order_by('id').values_list('id', flat=True))
        data["libraries_map"].append(library_folders_dict)
    return data