from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import os, json, requests
from celery.task import Task
from django.db.models import Q

from mnews.models import News
from models import Synced_item_info, Meta_sync_record, Resource_list, Resource_sync_record, SYNC_MAX_TRIES_LIMIT, Resourcelist_image_sync_log, Res_group_lib_folders_log
from sync import should_sync_image, upload_image_to_s3, create_resource_meta, upload_meta, upload_to_s3, DEFAULT_BUCKET_NAME, fetch_items_for_sync, CASSANDRA_REST_IP, HEADERS, IMAGE_DIR
from utils import get_res_group_lib_folders

from log import get_request_logger
logger = get_request_logger()


class CreateWeeklySyncForResListTask(Task):
    def run(self):
        logger.info('Running sync weekly task')
        try:
            resource_list_objs = Resource_list.objects.all().filter(visible=True)
            date_two_months = datetime.today() - relativedelta(months=2)
            for resource_list_obj in resource_list_objs:
                resource_sync_records = Resource_sync_record.objects.filter(resource_list=resource_list_obj, creation_date__gte= date_two_months)
                if resource_sync_records:
                    try:
                        upload_to_s3(resource_list_obj, resource_sync_records)
                    except Exception as exception:
                        logger.info('exception thrown while running UploadToS3ForResListTask: '+str(exception))
        except Exception as exception:
            logger.info('exception thrown while running CreateImageSyncForResListTask: '+str(exception))


class UpdateResGroupLibFoldersTask(Task):
    def run(self):
        logger.info('Updating resource_group library_folders')
        try:
            res_group_lib_folders_logs = Res_group_lib_folders_log.objects.all()
            for res_group_lib_folders_log in res_group_lib_folders_logs:
                data = get_res_group_lib_folders(res_group_lib_folders_log.project, res_group_lib_folders_log.mv_app_user)
                res_group_lib_folders_log.data = data
                res_group_lib_folders_log.add_time = datetime.now()
                res_group_lib_folders_log.save()
        except Exception as exception:
            logger.info('exception thrown while running UpdateResGroupLibFoldersTask: '+str(exception))


class CreateResourceSyncRecordTask(Task):
    def run(self):
        logger.info('Resource sync record creation task starting')
        try:
            resource_list_objs = Resource_list.objects.all().filter(visible=True)
            for resource_list_obj in resource_list_objs:
                resource_list_id = resource_list_obj.id
                CreateResSyncRecForResList.delay(resource_list_id)
        except Exception as exception:
            logger.info('exception thrown while running CreateResourceSyncRecordTask: '+str(exception))


class CreateResSyncRecForResList(Task):
    def run(self, resource_list_id):
        resource_list_obj = Resource_list.objects.get(id=resource_list_id)
        logger.info('Resource sync record creation task starting for resource_list '+str(resource_list_obj.id))
        items=[]
        try:
            items, comments = fetch_items_for_sync(resource_list_obj)
            items_comments = items + list(comments)
            
            for item in items_comments:
                res_sync_rec_objs = Resource_sync_record.objects.filter(resource_list=resource_list_obj, item=item)
                if res_sync_rec_objs.exists():
                    res_sync_rec_obj = res_sync_rec_objs[0]
                    recording = res_sync_rec_obj.item.detail
                    res_sync_rec_recording_path = recording.get_full_filename()
                    item_image = res_sync_rec_obj.item.sm_image
                    audio_modified_date=None
                    image_new_modified_date=None
                    image_old_modified_time=''
                    if os.path.isfile(res_sync_rec_recording_path):
                        audio_modified_date = datetime.fromtimestamp(os.path.getmtime(res_sync_rec_recording_path))
                    if item_image:
                        sync_response = res_sync_rec_obj.sync_response
                        if sync_response:
                            sync_response_json = json.loads(sync_response)
                            if "image_modified_time" in sync_response_json:
                                image_old_modified_time = str(sync_response_json["image_modified_time"])
                        
                        item_image_path = item_image.get_location()
                        image_new_modified_date = str(datetime.fromtimestamp(os.path.getmtime(item_image_path)))
                    if res_sync_rec_obj.synced_date and ((audio_modified_date and audio_modified_date > res_sync_rec_obj.synced_date) or (image_new_modified_date!=image_old_modified_time and should_sync_image(item) and image_new_modified_date)):
                        res_sync_rec_obj.delete()
                        Resource_sync_record(resource_list=resource_list_obj, item=item).save()
                else:
                    Resource_sync_record(resource_list=resource_list_obj, item=item).save()   
        except Exception as exception:
            logger.info('exception thrown while running CreateResSyncRecForResList: '+str(exception))

        logger.info('Running upload to s3 task')
        try:
            # We need to sync the reporter generated content because those items got created by this reporter only.
            # Inorder to achieve this, items which did not get fetched now for sync and got failed someday ago while syncing
            # will be retried now for syncing.
            resource_sync_records = Resource_sync_record.objects.filter(Q(tries__isnull=True) | Q(tries__lt=SYNC_MAX_TRIES_LIMIT), resource_list=resource_list_obj, status__in=('CREATED', 'FAILED'))
            if resource_sync_records:
                try:
                    upload_to_s3(resource_list_obj, resource_sync_records)
                except Exception as exception:
                    logger.info('exception thrown while running UploadToS3ForResListTask: '+str(exception))
        except Exception as exception:
            logger.info('exception thrown while running UploadToS3Task: '+str(exception))

        logger.info('Running sync meta task')
        try:
            meta_sync_record = Meta_sync_record(resource_list=resource_list_obj)
            meta_sync_record.save()
            if not meta_sync_record.tries:
                meta_sync_record.tries = 1
            else:
                meta_sync_record.tries += 1
            meta_sync_record.save()
            try:
                upload_meta(meta_sync_record, items)
                meta_sync_record.synced_date = datetime.now()
                meta_sync_record.status = 'SYNCED'
                meta_sync_record.save()
            except Exception as exception:
                logger.info('exception thrown while running SyncMetaForResListTask for resource list '+str(meta_sync_record.resource_list.id)+' : '+str(exception))
                meta_sync_record.status = 'FAILED'
                meta_sync_record.save()
        except Exception as exception:
            logger.info('exception thrown while running SyncMetaTask: '+str(exception))
