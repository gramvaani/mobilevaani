from datetime import datetime, timedelta

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from app_manager.models import UUID_LENGTH
from callerinfo.models import Contact, Contact_list
from campaign.models import Campaign_category
from customer.models import Project
from media.models import Image
from mnews.models import Channel, Transient_group_schedule, News, Groups_call_log, Tag, App_instance, Format
from sms.models import SMS_template, SMS_message
from sms.tasks import SMSTask

from log import get_request_logger
logger = get_request_logger()

DEFAULT_UPLOAD_LIMIT = 20
SYNC_MAX_TRIES_LIMIT = 5


class Resource_list_access(models.Model):
    type = models.CharField(max_length=32)

    def __unicode__(self):
        return unicode(self.type)


class Resource_list_perm(models.Model):
    type = models.CharField(max_length=32)

    def __unicode__(self):
        return unicode(self.type)


class Resource_list(models.Model):
    RES_LIST_TYPE_CHOICES = (
        ('CAMPAIGN', 'CAMPAIGN'),
        ('CHANNEL', 'CHANNEL'),
        ('JKR', 'JKR'),
        ('GENERAL', 'GENERAL'),
    )

    name = models.CharField(max_length=32, unique=True)
    desc = models.TextField(null=True, blank=True)
    type = models.CharField(max_length=10, choices=RES_LIST_TYPE_CHOICES, \
                             default='CHANNEL', help_text="Type of the resource list must be defined")
    linked_rs = models.ManyToManyField('self', symmetrical=False, null=True, blank=True, \
                                        help_text='Resource_lists similiar to this resource_list')
    perms = models.CharField(max_length=10, default='11111', help_text='First bit signifies if download allowed, \
                             Second bit signifies if forwarding allowed, Third bit signifies if recording allowed, \
                             Fourth bit signifies if sharing allowed, Fifth bit signifies if triggering OBD allowed')
    image = models.ForeignKey(Image, null=True, blank=True)
    upload_limit = models.PositiveSmallIntegerField(default=DEFAULT_UPLOAD_LIMIT, \
                                                    blank=True, \
                                                    help_text="Number of items to be uploaded to S3")
    visible = models.BooleanField(default=True)
    old_item_added = models.BooleanField(default=True)
    library_path = models.CharField(max_length=255, null=True, blank=True, \
                                    help_text="Absolute path of the directory to be uploaded in case Type is selected to be 'Library'")
    tags = models.ManyToManyField(Tag, null=True, blank=True)
    campaign_categories = models.ManyToManyField(Campaign_category, null=True, blank=True)
    formats = models.ManyToManyField(Format, null=True, blank=True)
    channels = models.ManyToManyField(Channel, null=True, blank=True)
    rating = models.PositiveSmallIntegerField(default=0, blank=True)
    created_at = models.DateTimeField(default=datetime.now)
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    order = models.PositiveIntegerField()
    access_to = models.ManyToManyField(Resource_list_access, null=True, blank=True)
    user_perm = models.ManyToManyField(Resource_list_perm, null=True, blank=True)
    schedule = models.ForeignKey(Transient_group_schedule, null=True, blank=True)
    channel_for_upload = models.ForeignKey(Channel, related_name = 'channel_for_upload', null=True, blank=True)
    modified_date = models.DateTimeField(auto_now=True , null=True)

    def __unicode__(self):
        return str(self.id)+'_'+str(self.name)
    
    def clean(self):
        if self.type=='LIBRARY' and self.library_path=='':
            raise ValidationError("'Library path' value for absolute path must be given for the directory to be uploaded to S3")
        super(Resource_list, self).clean()

class Resourcelist_image_sync_log(models.Model):
    resource_list = models.ForeignKey(Resource_list)
    synced_date = models.DateTimeField(null=True)

class Channel_obd_setting(models.Model):
    channel = models.ForeignKey(Channel)
    schedule = models.ForeignKey(Transient_group_schedule)

    def __unicode__(self):
        return unicode(self.channel)+'_'+unicode(self.schedule)


class MV_app_user(models.Model):
    app_user = models.ForeignKey(User, related_name='user_mv_app_user')
    contact = models.ForeignKey(Contact, related_name='contact_mv_app_user')
    is_registered = models.BooleanField(default = False)


class MV_SMS_detail(models.Model):
    name = models.CharField(max_length=50)
    sms_ai = models.ForeignKey(App_instance, related_name='ai_mv_sms_detail')
    sms_template = models.ForeignKey(SMS_template, related_name='sms_template_mv_sms_detail')
    max_tries = models.PositiveSmallIntegerField(default=3)


class Channel_obd_log(models.Model):
    channel = models.ForeignKey(Channel)
    app_user = models.ForeignKey(MV_app_user)
    gcl = models.ForeignKey(Groups_call_log)
    creation_time = models.DateTimeField(auto_now_add=True)
    device_id = models.CharField(max_length=50, null=True)
    app_version = models.CharField(max_length=10, null=True)

class Call_log(models.Model):
    resource_list = models.ForeignKey(Resource_list)
    app_user = models.ForeignKey(MV_app_user)
    gcl = models.ForeignKey(Groups_call_log)
    creation_time = models.DateTimeField(auto_now_add=True)
    device_id = models.CharField(max_length=50, null=True)
    app_version = models.CharField(max_length=10, null=True)


class Resourcelist_item_comment(models.Model):
    item = models.ForeignKey(News)
    user = models.ForeignKey(User, null=True)
    comment = models.TextField()
    add_date = models.DateTimeField(default=datetime.now)
    resource_list = models.ForeignKey(Resource_list)

    def __unicode__(self):
        return unicode(self.item)+'_' + unicode(self.user)

    def comment_user(self):
        if MV_app_user.objects.filter(app_user=self.user).exists():
            mv_app_user = MV_app_user.objects.get(app_user=self.user)
            return mv_app_user.contact.name or "mobile app user"
        else:
            return self.user.username


class MV_user_registration_detail(models.Model):
    mv_user = models.ForeignKey(MV_app_user)
    sms_message = models.ForeignKey(SMS_message, null=True, related_name=\
                                    'sms_message_mv_user_registration_detail')
    reg_code_expected = models.CharField(max_length=5)
    add_time = models.DateTimeField(default=datetime.now)
    expiry_time = models.DateTimeField()
    query_type = models.PositiveSmallIntegerField(null=True)

    class QueryType:
        NEW_USER_REGISTRATION      = 0
        EXISTING_USER_REGISTRATION = 1
        UPDATE_NAME                = 2
        UPDATE_NUMBER              = 3
        UPDATE_NAME_AND_NUMBER     = 4

    @classmethod
    def send_registration_message(cls, mv_user, number, query_type, version_code=None):
        from vapp.utils import random_with_N_digits
        mv_user_reg_detail = MV_user_registration_detail.objects.filter(mv_user=mv_user)
        if mv_user_reg_detail.exists():
            reg_detail_check = mv_user_reg_detail.order_by('-id')[0]
            if MV_registration_log.objects.filter(reg_detail_id=reg_detail_check.id).exists():
                code_expected = random_with_N_digits(5)
            else:
                code_expected = reg_detail_check.reg_code_expected
                if not code_expected:
                    code_expected = random_with_N_digits(5)
        else:
            code_expected = random_with_N_digits(5)

        if version_code and version_code>='1.0.0':
            sms_details = MV_SMS_detail.objects.all()[1]
            #app_hash = App_version.objects.get(version_no=version_code).app_hash
            app_hash = 'jWWMy8j1yYa'
            template_args = {'code_expected': code_expected, 'app_hash': app_hash}
        else:
            sms_details = MV_SMS_detail.objects.all()[0]
            template_args = {'number': number, 'code_expected': code_expected}

        sms_ai = sms_details.sms_ai
        message = sms_details.sms_template.process_template(template_args)
        logger.debug('message generated: %s' %(message))
        sms = SMSTask.create_send_msg(sms_ai.id,
                                      message,
                                      number,
                                      sms_details.max_tries, sender_id='GMVAPP')
        reg_log =  MV_user_registration_detail(mv_user=mv_user,
                                               sms_message=sms,
                                               reg_code_expected=code_expected,
                                               query_type=query_type)
        reg_log.expiry_time = datetime.now() + timedelta(days=2)
        reg_log.save()
        return reg_log


class MV_registration_log( models.Model ):
    reg_detail = models.ForeignKey(MV_user_registration_detail)
    reg_code_entered = models.CharField(max_length=5, null=True)
    time = models.DateTimeField(default=datetime.now)

    @classmethod
    def check_and_log_attempt(cls, reg_details, reg_code_entered ):
        success = False
        if reg_details:
            reg_detail = reg_details.order_by('-id')[0]
            success = (reg_detail.reg_code_expected == reg_code_entered)
            if success:
                MV_registration_log(reg_detail=reg_detail, reg_code_entered=reg_code_entered).save()
        return success


class App_access_log(models.Model):
    mv_app_user = models.ForeignKey(MV_app_user, null=True)
    access_time = models.DateTimeField(auto_now_add=True)
    device_id = models.CharField(max_length=50, null=True)
    app_version = models.CharField(max_length=10, null=True)

    class Meta:
        abstract = True


class Session_log(App_access_log):
    session_json = models.TextField()
    meta_file_version = models.CharField(max_length=20)
    uuid = models.CharField(max_length=UUID_LENGTH)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)

    @classmethod
    def add_access_log(cls, session_json, meta_file_version, uuid, start, end, mv_app_user=None, access_time=None, device_id=None, app_version=None):
        session_log = Session_log.objects.create(session_json=session_json,
                                                 meta_file_version=meta_file_version,
                                                 uuid=uuid,
                                                 start=start,
                                                 end=end,
                                                 mv_app_user=mv_app_user,
                                                 access_time=access_time,
                                                 device_id=device_id,
                                                 app_version=app_version)
        session_log.save()
        return session_log


class Item_file_access_log(models.Model):
    news = models.ForeignKey(News)
    s3_url = models.CharField(max_length=100)
    duration = models.PositiveSmallIntegerField()
    resource_list = models.ForeignKey(Resource_list)
    latency = models.PositiveSmallIntegerField()
    session_log = models.ForeignKey(Session_log)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)


class On_screen_log(models.Model):
    session_log = models.ForeignKey(Session_log)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)


class Download_log(models.Model):
    TRIGGER_OPTIONS = (
        ('MANUAL', 'MANUAL'),
        ('AUTO', 'AUTO')
    )
    news = models.ForeignKey(News)
    s3_url = models.CharField(max_length=100)
    local_url = models.CharField(max_length=100)
    content_type = models.CharField(max_length=10)
    resource_list = models.ForeignKey(Resource_list)
    success = models.BooleanField()
    latency = models.PositiveSmallIntegerField()
    trigger = models.CharField(max_length=10, choices=TRIGGER_OPTIONS, default='AUTO')
    session_log = models.ForeignKey(Session_log)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)


class Obd_scheduling_log(models.Model):
    tap = models.PositiveSmallIntegerField()
    timestamp = models.DateTimeField()
    resource_list = models.ForeignKey(Resource_list)
    session_log = models.ForeignKey(Session_log)


class Offline_sharing_log(models.Model):
    pass
    # news = models.ForeignKey(News)
    # s3_url = models.CharField(100)
    # resource_list = models.ForeignKey(Resource_list)
    # success = models.BooleanField()
    # latency = models.PositiveSmallIntegerField()
    # shared_to = models.CharField(64)
    # shared_from = models.CharField(64)
    # medium = models.TextField()
    # session_log = models.ForeignKey(Session_log)
    # start = models.DateTimeField(null=True)
    # end = models.DateTimeField(null=True)


class Resource_sync_record(models.Model):
    SYNC_STATUS_CHOICES = (
        ('CREATED', 'CREATED'),
        ('INPROGRESS', 'INPROGRESS'),
        ('SYNCED', 'SYNCED'),
        ('FAILED', 'FAILED')
    )
    resource_list = models.ForeignKey(Resource_list)
    item = models.ForeignKey(News)
    creation_date = models.DateTimeField(auto_now_add=True)
    synced_date = models.DateTimeField(null=True)
    status = models.CharField(max_length=10, choices=SYNC_STATUS_CHOICES, default='CREATED')
    sync_response = models.TextField()
    sync_url = models.TextField()
    tries = models.PositiveSmallIntegerField(null=True)


class Meta_sync_record(models.Model):
    META_SYNC_STATUS_CHOICES = (
        ('INPROGRESS', 'INPROGRESS'),
        ('SYNCED', 'SYNCED'),
        ('FAILED', 'FAILED')
    )
    resource_list = models.ForeignKey(Resource_list)
    creation_date = models.DateTimeField(auto_now_add=True)
    synced_date = models.DateTimeField(null=True)
    status = models.CharField(max_length=10, choices=META_SYNC_STATUS_CHOICES, default='INPROGRESS')
    tries = models.PositiveSmallIntegerField(null=True)


class App_version(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    version_no = models.CharField(max_length=10)
    version_location_url = models.URLField()
    addition_time = models.DateTimeField(default=datetime.now)
    supported = models.BooleanField(default=True)

    def __unicode__(self):  
        return unicode(self.name) if self.name else unicode(self.version_no)

    @classmethod
    def get_latest_version_detail(cls):
        versions = App_version.objects.filter(supported=True).order_by('-id')
        if versions.count()>0:
            return versions[0].version_no, versions[0].version_location_url


class Resource_groups(models.Model):
    name = models.CharField(max_length=100)
    resource_list = models.ManyToManyField(Resource_list, null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True)
    contact_list = models.ForeignKey(Contact_list, null=True, blank=True)
    order = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return str(self.id)+'__'+str(self.name)+'__'+str(self.order)


class Library_folders(models.Model):
    root_folder = models.BooleanField(default=False)
    name = models.CharField(max_length=100)
    resource_list = models.ManyToManyField(Resource_list, null=True, blank=True)
    folders = models.ManyToManyField('self', symmetrical=False, null=True, blank=True)
    image = models.ForeignKey(Image, null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True)
    contact_list = models.ForeignKey(Contact_list, null=True, blank=True)

    def __unicode__(self):
        return str(self.name)


class Synced_item_info(models.Model):
    resource_list = models.ForeignKey(Resource_list, unique=True)
    items_list = models.TextField(null=True, blank=True)
    init_time = models.DateTimeField(default=datetime.now)
    all_items_list = models.TextField(null=True, blank=True)
    last_run_time = models.DateTimeField(default=datetime.now)
    misc = models.TextField()


class Res_group_lib_folders_log(models.Model):
    project = models.ForeignKey(Project, null=True)
    mv_app_user = models.ForeignKey(MV_app_user, null=True)
    data = models.TextField(null=True)
    add_time = models.DateTimeField(default=datetime.now)
