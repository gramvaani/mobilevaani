# Create your views here.
from datetime import datetime, timedelta

def app_authetication(user_id, timestamp, hash_from_app):
    if datetime.now() <= (timestamp - timedelta(seconds = 60)) :
        logger.info('Old request. Rejecting authentication')
        return False
    else:
        hash_str = str(timestamp) + str(user_id)
        auth_hash = hash(hash_str)
        if auth_hash == hash_from_app:
            logger.info('Authentication Successfull')
            return True
        else:
            logger.info('Hash Mismatch')
            return False