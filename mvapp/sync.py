import uuid, Image, os
from datetime import datetime
import requests, json
from django.db.models import Q

import boto3
from cassandra.cluster import Cluster

from vapp.utils import ffmpeg_modify_mp3
from callerinfo.models import Contact
from media.models import Recording
from mnews.models import News, Bookmark, ModerationEventRecorder, ModerationEvents, News_state
from models import MV_app_user, Resource_list, Resource_sync_record, Resourcelist_item_comment, Resourcelist_image_sync_log, Synced_item_info
from django.db.models import Min
from pathlib import Path

from log import get_request_logger
logger = get_request_logger()


client = boto3.client('s3')
app_img_frmts = ['_ldpi.', '_mdpi.', '_hdpi.']
DEFAULT_BUCKET_NAME = 'gramvaani'
GV_LOGO_PATH = '/usr/local/voicesite/vWeb/app/apple-touch-icon.png'
AWS_PATH = 'https://s3.ap-south-1.amazonaws.com/'
RES_LIST_ITEMS_LIMIT = 20
EC2_PUBLIC_IP = '54.175.33.18'
EC2_PUBLIC_IP_PORT = 9042
CASSANDRA_KEYSPACE_NAME = 'mvapp'
#CASSANDRA_REST_IP='http://54.205.109.26:80/'
CASSANDRA_REST_IP='http://api3.gramvaani.org/'
AUDIO_DIR='audios/'
IMAGE_DIR='images/'
HEADERS={"Content-Type": "application/json"}
SGC_episode_format_id = 15
UGC_episode_format_id = 16
RGC_episode_format_id = 7


def get_contributor_name(number):
    contacts = Contact.objects.filter(number=number).order_by('-id')
    name = None
    for contact in contacts:
        contact_name = contact.name.strip()
        if contact_name and not contact_name.isdigit():
            name = str(contact_name)
            break
    return name


def get_item_title_for_app(item):
    title = item.title
    if ';' in title:
        index = title.rfind(';')
        right_substring = title[index+1:len(title)].strip(' ;')
        if right_substring:
            return right_substring
        return title[0:index].strip(' ;')
    else:
        return title.strip()


def should_sync_image(item):
    flag = False
    if item.misc:
        misc = json.loads(item.misc)
        if 'sync_image' in misc and bool(misc['sync_image'])==True:
            flag = True
    return flag


def modify_mp3(filepath):
    output_file = '/tmp/%s.mp3' %(uuid.uuid1())
    output_file = ffmpeg_modify_mp3(filepath, output_file)
    return output_file


def convert_image(filepath, file_identifier, image_extension):
    img = Image.open(filepath)
    hdpi_im_key = file_identifier+'_hdpi.'+image_extension
    im_hdpi_file = '/tmp/'+hdpi_im_key
    width_0, height_0 = img.size
    hdpi_fixed_width_in_pixel = 480
    wpercent = hdpi_fixed_width_in_pixel / float(width_0)
    hsize = int(float(height_0) * float(wpercent))
    img.resize((hdpi_fixed_width_in_pixel, hsize), Image.ANTIALIAS).save(im_hdpi_file)
    return [im_hdpi_file], [hdpi_im_key]


def upload_app_images(filepath, bucket_name, resource_list_dir, file_identifier, image_extension):
    files_keys = convert_image(filepath, file_identifier, image_extension)
    files = files_keys[0]
    keys = files_keys[1]
    for index in range(len(files)):
        if file_identifier=='GramVaani':
            key = keys[index]
        else:
            key = resource_list_dir+keys[index]
        try:
            image = Image.open(filepath)
            img_format = str(image.format)
            cType = 'image/' + img_format.strip().lower()
            client.upload_file(files[index], bucket_name, key, ExtraArgs={'ContentType': cType})
        except Exception as exception:
            logger.info("Not an image file : " + str(exception))


def if_key_in_bucket(key, objs):
    for obj in objs:
        if obj.key==key:
            return True
    return False


def upload_image_to_s3(image_path, file_identifier, resource_list_dir, bucket_name, in_folder):
    #if ((s3_objs_count==0) or (s3_objs_count>0 and not if_key_in_bucket(orig_image_key, s3_objs))):
    try:
        image = Image.open(image_path)
        img_format = str(image.format)
        cType = 'image/' + img_format.strip().lower()
        base_image_path = image_path.split('/')[-1]
        image_extension = base_image_path.split('.')[-1]
        orig_image_key = file_identifier+'.'+image_extension
        if in_folder==True:
            orig_image_key = resource_list_dir+orig_image_key
        client.upload_file(image_path, bucket_name, orig_image_key, ExtraArgs={'ContentType': cType})
        upload_app_images(image_path, bucket_name, resource_list_dir, file_identifier, image_extension)
    except Exception as exception:
        logger.info("Not an image file : " + str(exception))


def get_s3_objs_count(bucket_name=DEFAULT_BUCKET_NAME):
    bucket = boto3.resource('s3').Bucket(bucket_name)
    s3_objs = bucket.objects.all()
    s3_objs_count = sum(1 for _ in s3_objs)
    return s3_objs, s3_objs_count


def upload_to_s3(resource_list, resource_sync_records, bucket_name=DEFAULT_BUCKET_NAME):
    #s3_objs, s3_objs_count = get_s3_objs_count(bucket_name)
    res_sync_recs_for_res_list = resource_sync_records.filter(resource_list=resource_list)
    resource_list_dir=IMAGE_DIR
    
    for res_sync_rec in res_sync_recs_for_res_list:
        tries = 1
        if res_sync_rec.tries:
            tries = res_sync_rec.tries+1
        res_sync_rec.tries = tries
        res_sync_rec.save()
        try:
            res_sync_rec.status = 'INPROGRESS'
            res_sync_rec.save()
            
            recording = res_sync_rec.item.detail
            res_sync_rec_recording_path = recording.get_full_filename()
            base_recording_path = res_sync_rec_recording_path.split('/')[-1]
            key = AUDIO_DIR+base_recording_path
            logger.info('res_sync_rec_recording_path='+res_sync_rec_recording_path)
            logger.info('resource_list_id='+str(resource_list.id)+'------'+'base_recording_path='+base_recording_path+'-----'+'key='+key)
            #logger.info('s3_objs_count='+str(s3_objs_count))
            #logger.info('if_key_in_bucket='+str(if_key_in_bucket(key, s3_objs)))
            if base_recording_path.endswith('.mp3') :
                logger.info('Inside the decider upload If')
                output_file = modify_mp3(res_sync_rec_recording_path)
                cType='audio/mp3'
                client.upload_file(output_file, bucket_name, key, ExtraArgs={'ContentType': cType})
            item_aws_path = AWS_PATH+bucket_name+'/'+key
            item_image = res_sync_rec.item.sm_image
            if item_image and should_sync_image(res_sync_rec.item):
                item_image_path = item_image.get_location()
                image_name=str(recording.id)
                image_mod_time = ''
                if os.path.isfile(item_image_path):
                    image_mod_time = datetime.fromtimestamp(os.path.getmtime(item_image_path))
                    image_modified_date = str(image_mod_time).replace(' ','_')
                    if image_modified_date:
                        image_name=image_name+'_'+image_modified_date
                try:
                    upload_image_to_s3(item_image_path, image_name, resource_list_dir, bucket_name, in_folder=True)
                    sync_response = {}
                    sync_response["image_modified_time"] = str(image_mod_time)
                    res_sync_rec.sync_response = json.dumps(sync_response)
                except Exception as exception:
                    logger.info('exception in image filename for resource_list '+str(resource_list.id)+' for item_id '+str(res_sync_rec.item.id)+' : '+ + str(exception))
            #upload_image_to_s3(GV_LOGO_PATH, 'GramVaani', resource_list_dir, bucket_name, in_folder=True)
            res_sync_rec.status = 'SYNCED'
            res_sync_rec.sync_url = item_aws_path
            res_sync_rec.synced_date = datetime.now()
            res_sync_rec.save()
        except Exception as exception:
            res_sync_rec.status = 'FAILED'
            res_sync_rec.save()
            logger.info('exception thrown while uploading to s3 for resource_list '+str(resource_list.id)+' for item_id '+str(res_sync_rec.item.id)+' : '+str(exception))


def get_library_items(path):
    items = []
    for dir in [entry[0] for entry in os.walk(path)]:
        for content in os.listdir(dir):
            if not content.endswith('.mp3'):
                continue
            try:
                item = News.objects.get(detail__id=content[:-4])
            except:
                pass
            else:
                items.append(item)
    return items


def is_duplicate_item(news, items):
    for item in items:
        if item.checksum == news.checksum:
            return True
    return False


def compose_query_filter_for_tags(tags):
    q_objects = Q()
    for tag in tags:
        q_objects.add(Q(tags__startswith=tag+','), Q.OR)
        q_objects.add(Q(tags__endswith=','+tag), Q.OR)
        q_objects.add(Q(tags__contains=','+tag+','), Q.OR)
        q_objects.add(Q(tags__exact=tag), Q.OR)
    return q_objects


def filter_items_with_tags(items=None, include_tags=[], exclude_tags=[]):
    if not items:
        return News.objects.none()
    q_objects_include = compose_query_filter_for_tags(include_tags)
    q_objects_exclude = compose_query_filter_for_tags(exclude_tags)
    return items.filter(q_objects_include).exclude(q_objects_exclude)


def fetch_items_for_sync(resource_list_obj):
    logger.info('fetching items for resource_list_id : '+str(resource_list_obj.id))
    last_run_time = datetime.now()
    to_be_refreshed = False
    synced_item_info = None
    all_modified_items = None
    rating = resource_list_obj.rating
    formats = resource_list_obj.formats.all()
    channels = resource_list_obj.channels.all()
    campaign_categories = resource_list_obj.campaign_categories.all()
    tags = resource_list_obj.tags.all()

    synced_item_info_qs = Synced_item_info.objects.filter(resource_list=resource_list_obj)
    if synced_item_info_qs.exists():
        synced_item_info = synced_item_info_qs[0]
        if resource_list_obj.modified_date>synced_item_info.init_time or not synced_item_info.all_items_list:
            to_be_refreshed = True
            synced_item_info.init_time = datetime.now()
            items = News.objects.filter(rating__gte=rating, is_comment=False, detail__isnull=False, checksum__isnull = False, state__in=(News_state.PUB, News_state.ARC))
        else:
            all_modified_items = News.objects.filter(modified_date__gte=synced_item_info.last_run_time)
            items = all_modified_items.filter(rating__gte=rating, is_comment=False, detail__isnull=False, checksum__isnull = False, state__in=(News_state.PUB, News_state.ARC))
    else:
        to_be_refreshed = True
        synced_item_info.init_time = datetime.now()
        items = News.objects.filter(rating__gte=rating, is_comment=False, detail__isnull=False, checksum__isnull = False, state__in=(News_state.PUB, News_state.ARC))
        synced_item_info = Synced_item_info(resource_list=resource_list_obj)

    if formats:
        items = items.filter(format__in=formats)

    item_sub=News.objects.none()
    sticky_order=False
    if channels:
        for channel in channels:
            ai=channel.ai
            item_sub = item_sub | items.filter(channel=channel, ai=ai)
            if(channel.sticky):
                sticky_order=True
        items=item_sub

    if campaign_categories:
        items = items.filter(campaign_categories__in=campaign_categories.all()).distinct()

    if tags:
        tags = tags.values_list('name', flat=True)
        items = filter_items_with_tags(items=items, include_tags=tags, exclude_tags=['nfa'])
    else:
        items = filter_items_with_tags(items=items, include_tags=[], exclude_tags=['nfa'])

    if to_be_refreshed:
        synced_item_info.all_items_list = ','.join(str(item.id) for item in items)
    else:
        old_all_items_list = synced_item_info.all_items_list
        items_list = str(old_all_items_list).split(',')
        items_list = filter(None, items_list)
        res_list_item_ids = [int(x) for x in items_list]
        res_list_items = News.objects.filter(id__in=res_list_item_ids)
        modified_items_not_selected = set(all_modified_items) - set(items)
        updated_old_all_items_list = set(res_list_items) - modified_items_not_selected
        updated_all_items_list = updated_old_all_items_list.union(set(items))
        synced_item_info.all_items_list = ','.join(str(item.id) for item in updated_all_items_list)

    synced_item_info.last_run_time = last_run_time
    synced_item_info.save()
    item_ids = Synced_item_info.objects.filter(resource_list=resource_list_obj)[0].all_items_list.split(',')
    item_ids = filter(None, item_ids)
    item_ids_list = [int(id) for id in item_ids]
    items = News.objects.filter(id__in=item_ids_list).order_by('-time')
    logger.info('basic filtering done while fetching items for resource_list_id : '+str(resource_list_obj.id))

    if str(resource_list_obj.type)=='CAMPAIGN':
        items_filtered = []
        sgc_episodes_qs = items.filter(format_id=SGC_episode_format_id)
        ugc_episodes_qs = items.filter(format_id=UGC_episode_format_id)
        rgc_episodes_qs = items.filter(format_id=RGC_episode_format_id)
                
        items_count = 0
        while len(items_filtered) < resource_list_obj.upload_limit:
            for sgc_episode_qs in sgc_episodes_qs:
                sgc_episode_tags_set = set(sgc_episode_qs.tags.split(","))
                if 'episode' in sgc_episode_tags_set:
                    if sgc_episode_qs in items_filtered or is_duplicate_item(sgc_episode_qs, items_filtered):
                        continue
                    else:
                        items_filtered.append(sgc_episode_qs)
                        break

            if len(items_filtered) < resource_list_obj.upload_limit:
                for ugc_episode_qs in ugc_episodes_qs:
                    if ugc_episode_qs in items_filtered or is_duplicate_item(ugc_episode_qs, items_filtered):
                        continue
                    else:
                        items_filtered.append(ugc_episode_qs)
                        break

            if len(items_filtered) < resource_list_obj.upload_limit:
                count = 0
                for rgc_episode_qs in rgc_episodes_qs:
                    if rgc_episode_qs in items_filtered or is_duplicate_item(rgc_episode_qs, items_filtered):
                        continue
                    else:
                        if count == 8:
                            break
                        else:
                            if len(items_filtered) < resource_list_obj.upload_limit:
                                count += 1
                                items_filtered.append(rgc_episode_qs)
                            else:
                                break
                            
            if items_count==len(items_filtered):
                break
            items_count = len(items_filtered)

    elif str(resource_list_obj.type)=='GENERAL':
        if(sticky_order):
            items = items.order_by('-channel__sticky', '-time')

        items_filtered = []
        for item in items:
            if len(items_filtered) == resource_list_obj.upload_limit:
                break
            elif item in items_filtered or is_duplicate_item(item, items_filtered):
                continue
            else:
                items_filtered.append(item)

    elif str(resource_list_obj.type)=='JKR':
        items_filtered = []
        rgc_episodes = []
        for item in items:
            item_tags = set(item.tags.split(","))
            if 'promo' in item_tags:
                items_filtered.append(item)
                break

        for item in items:
            if item.format and item.format.id==UGC_episode_format_id and item not in items_filtered and not is_duplicate_item(item, items_filtered):
                items_filtered.append(item)
                break

        for item in items:
            if item.format and item.format.id==RGC_episode_format_id:
                if len(rgc_episodes) < resource_list_obj.upload_limit-len(items_filtered) and item not in rgc_episodes and not is_duplicate_item(item, rgc_episodes):
                    rgc_episodes.append(item)

        rgc_episodes_non_special_tagged = []
        rgc_episodes_special_tagged = []
        for rgc_episode in rgc_episodes:
            rgc_episode_tags = set(rgc_episode.tags.split(","))
            if any(x in rgc_episode_tags for x in ['entertainment', 'folk song', 'song', 'couplet']):
                rgc_episodes_special_tagged.append(rgc_episode)
            else:
                rgc_episodes_non_special_tagged.append(rgc_episode)
        items_filtered.extend(rgc_episodes_non_special_tagged)
        items_filtered.extend(rgc_episodes_special_tagged)
        
    else:
        items = items.filter(state=News_state.PUB)
        if(sticky_order):
            items = items.order_by('-channel__sticky', 'pub_order', '-time')
        else:
            items = items.order_by('pub_order', '-time')

        items_filtered = []
        for item in items:
            if len(items_filtered) == resource_list_obj.upload_limit:
                break
            elif item in items_filtered or is_duplicate_item(item, items_filtered):
                continue
            else:
                items_filtered.append(item)

    items = items_filtered
    comments = News.objects.none()
    for item in items:
        item_comments = item.comments.filter(state=News_state.PUB, detail__isnull=False)
        comments = comments | item_comments

    logger.info('fetched items for resource_list_id : '+str(resource_list_obj.id))
    return items, comments


def create_resource_meta(resource_list, bucket_name):
    data = {}
    bucket_path = AWS_PATH+bucket_name+'/'
    resource_name = str(resource_list.name)
    resource_list_path = bucket_path
    resource_image_path=None
    resource_image_name=resource_name
    if resource_list.image :
        resource_image_path = resource_list.image.get_location()
    if not resource_image_path:
        resource_image_path = GV_LOGO_PATH
    if os.path.isfile(resource_image_path):
        image_modified_date = str(datetime.fromtimestamp(os.path.getmtime(resource_image_path))).replace(' ','_')
        if image_modified_date:
            resource_image_name=resource_name+'_'+image_modified_date
        
    resource_image_extension = resource_image_path.split('.')[-1]
    resource_list_desc = resource_list.desc
    if resource_list_desc:
        resource_list_desc = str(resource_list_desc)
    linked_resource_lists = resource_list.linked_rs.values_list('id', flat=True)
    linked_resource_lists_ids = [int(linked_resource_list) for linked_resource_list in linked_resource_lists]

    data["id"] = int(resource_list.id)
    data["resource_url"] = resource_list_path
    data["name"] = resource_name
    data["resource_image_name"]=resource_image_name
    data["image"] = resource_image_name+'.'+resource_image_extension
    data["description"] = resource_list.desc
    data["type"] = str(resource_list.type)
    data["position"] = int(resource_list.order)
    data["linked_rs"] = linked_resource_lists_ids
    data["perms"] = str(resource_list.perms)
    return data


def upload_meta(meta_sync_rec, items_fetched, bucket_name=DEFAULT_BUCKET_NAME):
    resource_list = meta_sync_rec.resource_list
    data = create_resource_meta(meta_sync_rec.resource_list, bucket_name)
    res_sync_recs = Resource_sync_record.objects.filter(resource_list=resource_list, status='SYNCED')
    item_ids_synced = res_sync_recs.filter(item__is_comment=False).values_list('item', flat=True)
    comment_ids_synced = res_sync_recs.filter(item__is_comment=True).values_list('item', flat=True)
    new_synced_items = []
    item_comment_ids_for_meta = []

    resource_list_dir=IMAGE_DIR
    resource_image_name = resource_list.name
    res_sync_rec_image = resource_list.image
    resource_list_image_sync = Resourcelist_image_sync_log.objects.get_or_create(resource_list=resource_list)[0]
    if res_sync_rec_image and (not resource_list_image_sync.synced_date or resource_list_image_sync.synced_date < resource_list.modified_date):
        res_sync_rec_image_path = res_sync_rec_image.get_location()
        if os.path.isfile(res_sync_rec_image_path):
            image_modified_date = str(datetime.fromtimestamp(os.path.getmtime(res_sync_rec_image_path))).replace(' ','_')
            if image_modified_date:
                resource_image_name=resource_image_name+'_'+image_modified_date
        logger.info('uploading images for resource_list_id : '+str(resource_list.id))
        upload_image_to_s3(res_sync_rec_image_path, resource_image_name, resource_list_dir, DEFAULT_BUCKET_NAME, in_folder=True)
        resource_list_image_sync.synced_date=datetime.now()
        resource_list_image_sync.save()
        
    for item in items_fetched:
        if(len(new_synced_items)<resource_list.upload_limit):
            if (item.id in item_ids_synced):
                new_synced_items.append(item)
                item_comment_ids_for_meta.append(item.id)
                for comment in item.comments.filter(state=News_state.PUB, detail__isnull=False):
                    if comment.id in comment_ids_synced:
                        item_comment_ids_for_meta.append(comment.id)
        else:
            break

    old_synced_comment_ids = []
    if resource_list.old_item_added:
        synced_item_info_qs = Synced_item_info.objects.filter(resource_list=resource_list)
        if synced_item_info_qs.exists():
            synced_item_info_items_list = str(synced_item_info_qs[0].items_list).split(',')
            synced_item_info_items_list = filter(None, synced_item_info_items_list)
            old_synced_item_comment_ids = [int(x) for x in synced_item_info_items_list]
            old_synced_item_comments = News.objects.filter(id__in=old_synced_item_comment_ids, detail_id__isnull = False).order_by('-time')
            old_synced_item_ids = old_synced_item_comments.filter(is_comment=False).values_list('id', flat=True)
            old_synced_comment_ids = old_synced_item_comments.filter(is_comment=True).values_list('id', flat=True)
            for old_synced_item_id in old_synced_item_ids:
                if(len(new_synced_items)<resource_list.upload_limit):
                    old_synced_item = News.objects.get(id=old_synced_item_id)
                    if old_synced_item not in new_synced_items and not is_duplicate_item(old_synced_item, new_synced_items):
                        item_comment_ids_for_meta.append(old_synced_item_id)
                        new_synced_items.append(old_synced_item)
                        for comment in News.objects.get(id=old_synced_item_id).comments.filter(state=News_state.PUB, detail__isnull=False):
                            if comment.id in old_synced_comment_ids:
                                item_comment_ids_for_meta.append(comment.id)
                else:
                    break

    item_list = [int(x) for x in item_comment_ids_for_meta]
    items = News.objects.filter(id__in=list(item_comment_ids_for_meta))
    resource_image_name=data["resource_image_name"]
    
    url=CASSANDRA_REST_IP+"resourcelists"
    resource_list_meta_dic={"id":data["id"], "description":data["description"], "image":data["image"], "items":item_list, "name":data["name"], "position":data["position"], "resource_url":data["resource_url"], "type":data["type"], "linked_rs":data["linked_rs"], "perms":data["perms"]}
    logger.info('resource_list_meta_dic='+json.dumps(resource_list_meta_dic))
    upload_meta_for_item_comment(items, bucket_name, resource_image_name, resource_list)
    response = requests.post(url, data=json.dumps(resource_list_meta_dic),headers=HEADERS)
    if not (response.ok):
        raise Exception

    items = ','.join(str(item) for item in item_list)
 
    resource_list_meta_dic['resource_image_name'] = resource_image_name
    data = json.dumps( resource_list_meta_dic )
    meta_data, created = Synced_item_info.objects.get_or_create(resource_list=resource_list, defaults={'items_list': items, 'misc': data})
    if not created:
        meta_data.items_list = items
        meta_data.misc = data
        meta_data.save()


def upload_meta_for_item_comment(items, bucket_name, resource_name, resource_list):
    resource_list_path = AWS_PATH+bucket_name+'/'+resource_name+'/'
    for item in items:
        item_id = int(item.id)
        audio = str(item.detail.id)+'.mp3'
        bucket_path = AWS_PATH+bucket_name+'/'
        #audio_url = bucket_path+resource_name+'/'+audio

        msisdn = int(item.callerid)
        contributor = get_contributor_name(msisdn)
        item_created_dt = str(item.time)
        item_published_dt = None
        if item.autopublished:
            item_published_dt = str(item_created_dt)
        else:
            mod_events = ModerationEventRecorder.objects.filter(item=item, event_type=ModerationEvents.ITEM_PUBLISHED).order_by('id')
            if mod_events.exists():
                item_published_dt = str(mod_events[0].timestamp)
        item_image = item.sm_image
        item_image_path=None
        item_image_name=None
        if item_image and should_sync_image(item):
            extension = item_image.get_location().split('.')[-1]
            image_file=item_image.get_location()
            if os.path.isfile(image_file):
                image_modified_date = str(datetime.fromtimestamp(os.path.getmtime(image_file))).replace(' ','_')
                if image_modified_date:
                    item_image_path = resource_list_path+str(item.detail.id)+'_'+image_modified_date+'.'+extension
        if item_image_path:
            item_image_name = item_image_path.split('/')[-1]
        #if os.path.isfile(item_image_path):
        #    image_modified_date = str(datetime.fromtimestamp(os.path.getmtime(item_image_path))).replace(' ','_')
        #    if image_modified_date:
        #       item_image_name=item_image_name+'_'+image_modified_date
    
        likes = Bookmark.objects.filter(news=item).count()
        item_location = item.location
        if item_location:
            item_location = str(item_location)
        ratings = int(item.rating)
        title = get_item_title_for_app(item)
        item_transcript = item.transcript
        if item_transcript:
            item_transcript = str(item_transcript)

        misc_final = {}
        if not item.misc:
            user_id = None
        else:
            misc_data = json.loads(item.misc)
            for key, value in misc_data.iteritems():
                if type(value) is dict:
                    misc_final.update(value)
                else:
                    misc_final[key] = value
            user_id = misc_final.get('user_id')

        checksum = item.checksum
        if checksum:
            checksum = str(checksum)

        ccs = []
        for cc in item.campaign_categories.all():
            ccs.append(str(cc.id))
        ccs = ','.join(ccs)

        item_meta_dic={"itemId":item_id, "checksum":checksum, "audio":audio, "contributor":contributor, "misc":misc_final, 
        "creation_datetime":item_created_dt, "image":item_image_name, "likes":likes, "location":item_location, "msisdn":msisdn, "published_datetime":item_published_dt, 
        "rating":ratings, "title":title, "transcription":item_transcript, "qualifier":item.qualifier.name if item.qualifier else None, 
        "duration":str(int(item.detail.get_duration())), "ai_id":item.ai_id, "is_comment":item.is_comment, "parent_id":item.parent_id(), "user_id":user_id,
        "channel_id":item.channel.id, "state":item.state, "source":item.source, "tags":item.tags, "format":item.format.name if item.format else None,
        "cc":ccs, "category":item.category.name if item.category else None, "age_group":item.age_group.name if item.age_group else None,
        "occupation":item.occupation.name if item.occupation else None, "gender":item.gender.gender if item.gender else None, "autopublished":item.autopublished}
        logger.info('item_meta_dic='+json.dumps(item_meta_dic))
        url=CASSANDRA_REST_IP+"items"
        response = requests.post(url, data=json.dumps(item_meta_dic),headers=HEADERS)
        if not (response.ok):
            raise Exception

def get_res_lists_name_images_dict_list(res_group_lib_folder_obj, is_library):
    resource_lists_images = []
    resource_lists = []
    if not is_library and res_group_lib_folder_obj.order:
        res_list_ids_ordered = [int(res_list_id) for res_list_id in res_group_lib_folder_obj.order.split(',')]
        mapped_resource_lists = res_group_lib_folder_obj.resource_list.all().order_by('id')
        mapped_resource_lists_ids = mapped_resource_lists.values_list('id', flat=True)
        resource_lists = [Resource_list.objects.get(id=resource_list_id) for resource_list_id in res_list_ids_ordered if resource_list_id in mapped_resource_lists_ids]
        for resource_list in mapped_resource_lists:
            if resource_list not in resource_lists:
                resource_lists.append(resource_list)
    else:
        resource_lists = res_group_lib_folder_obj.resource_list.all().order_by('id')

    for resource_list in resource_lists:
        resource_lists_dict = {}
        resource_lists_dict["id"] = str(resource_list.id)
        resource_lists_dict["name"] = resource_list.name
        if resource_list.image:
            res_image_path = resource_list.image.get_location()
            resource_image_name=resource_list.name
            if os.path.isfile(res_image_path):
                image_modified_date = str(datetime.fromtimestamp(os.path.getmtime(res_image_path))).replace(' ','_')
                if image_modified_date:
                    resource_image_name=resource_image_name+'_'+image_modified_date
            resource_lists_dict["image"] = resource_image_name+"."+resource_list.image.get_location().split('.')[-1]
        else:
            resource_lists_dict["image"] = "GramVaani.png"
        resource_lists_images.append(resource_lists_dict)
    return resource_lists_images

def get_item_image(item, resource_name):
    resource_list_path = AWS_PATH+DEFAULT_BUCKET_NAME+'/'+resource_name+'/'
    item_image = item.sm_image
    item_image_path=None
    item_image_name=None
    if item_image:
        extension = item_image.get_location().split('.')[-1]
        image_file=item_image.get_location()
        if os.path.isfile(image_file):
            image_modified_date = str(datetime.fromtimestamp(os.path.getmtime(image_file))).replace(' ','_')
            if image_modified_date:
                item_image_path = resource_list_path+str(item.detail.id)+'_'+image_modified_date+'.'+extension
    if item_image_path:
        item_image_name = item_image_path.split('/')[-1]
    return item_image_name
