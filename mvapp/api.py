from datetime import datetime, timedelta
import os
import shutil, ast
import subprocess
from mnews.models import Channel
from django.conf.urls import url
from django.http import HttpResponse
from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication, MultiAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from tastypie.http import HttpApplicationError, HttpCreated
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.models import ApiKey
from pydub import AudioSegment

from models import Resource_list, MV_app_user, Channel_obd_setting, Call_log, App_version,\
Resourcelist_item_comment, MV_user_registration_detail, MV_registration_log, Resource_groups, Library_folders, Synced_item_info, Res_group_lib_folders_log
from sync import get_item_image, is_duplicate_item
from utils import get_res_group_special_items, get_res_group_lib_folders
from vapp.app_manager.common import get_queue_for_task
from vapp.callerinfo.models import Contact
from vapp.media.models import Image
from vapp.mnews.tasks import CallAndPlayTask
from vapp.mnews.models import Groups_call_log, News, News_settings
from vapp.telephony.utils import get_formatted_number
from vapp.utils import generate_random_user
from vapp.log import get_request_logger
from vapp.media.api import recording_upload_impl
from vapp.api import AppInstanceAuthorization
from vapp.app_manager.api import AnonymousReadAuthentication
from django.core import serializers
from vapp.app_manager.models import App_instance
from datetime import *
from dateutil.relativedelta import relativedelta
import json
import random
logger = get_request_logger()

SPECIAL_ITEMS_COUNT = 4


class ResourceListResource(ModelResource):
    class Meta:
        queryset = Resource_list.objects.all().order_by('order')
        resource_name = "mvapp/resource_list"
        filtering = {
            'id' : ALL,
        }
        allowed_methods = ['get']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/obd%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('schedule_obd'), name="api_schedule_obd"),
        ]

    def schedule_obd(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mvapp/resource_list/schedule_obd with request: {}'.format(request))
            success = False
            error = ''
            obd_limit_exceeded = False
            number = request.REQUEST.get('contact')
            if not number:
                user_id = request.REQUEST['mv_app_user_id']
                number = MV_app_user.objects.get(pk=user_id).contact.number
            resource_list_id = int(request.REQUEST['resource_list_id'])
            device_id = request.REQUEST.get('device_id')
            app_version = request.REQUEST.get('app_version')
            schedule = Resource_list.objects.get(id=resource_list_id).schedule
            number = get_formatted_number(number)

            if Call_log.objects.filter(resource_list_id=resource_list_id,
                        app_user_id=user_id,
                        creation_time__gte=datetime.now() - timedelta(seconds=300)).count() >= 1:
                logger.debug('Already received 1 or more OBD requests.')
                obd_limit_exceeded = True
            else:
                ai = schedule.ai_group.ai
                gcl = Groups_call_log(number=number,
                                      ai=ai,
                                      group_schedule=schedule)
                gcl.save()
                Call_log(resource_list_id=resource_list_id,
                                app_user_id=user_id,
                               	gcl_id=gcl.id,
                                device_id=device_id,
                                app_version=app_version).save()
                queue = get_queue_for_task(ai, default_queue='priority_push')
                CallAndPlayTask.apply_async([gcl.id], eta=datetime.now(), queue=queue)
                success = True
        except Exception as e:
            error = 'exception while scheduling obd: {}'.format(e)
            logger.exception(error)
        response_class = HttpCreated
        response = {'success': success, 'error': error, 'obd_limit_exceeded': obd_limit_exceeded}
        self.log_throttled_access(request)
        return self.create_response(request, response, response_class=response_class)


class MVAppVersionResource(ModelResource):    
    class Meta:
        queryset = App_version.objects.all()
        resource_name = "mvapp/app_version"
        filtering = {
             'id': 'exact',
        }
        allowed_methods = ['get']
    
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/check_version%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('check_version'), name="api_check_version"),
        ]

    def check_version(self, request, **kwargs):
        logger.debug(request)
        self.method_check(request, allowed=['get'])
        self.throttle_check(request)

        try:
            version_no = str(request.REQUEST['version_no'])
            if not App_version.objects.filter(version_no=version_no).exists():
                raise Exception("incorrect version no provided")

            version = App_version.objects.filter(version_no=version_no)[0]
            latest_version_no, latest_version_url = App_version.get_latest_version_detail()

            ### Check made to carry out smooth upgrade for app 2.9 version to latest version
            unsupported_versions = ["1.0", "2.0", "2.6", "2.7", "2.8", "2.9"]
            if version_no in unsupported_versions:
                latest_version_no = Decimal("1.1")
                ### 1.1 -> a dummy value

            response = {
                'success': True,
                'is_supported': version.supported,
                'version_location_url': version.version_location_url,
                'latest_version': latest_version_no,
                'latest_version_url': latest_version_url
            }
            return self.create_response(request, response)
        except Exception as e:
            exception_str = 'exception while check_version: {0}'.format(e)
            logger.exception(exception_str)
            response = {
                'success': False,
                'is_supported': False,
                'version_location_url': '',
                'error': exception_str
            }
            return self.create_response(request, response, HttpApplicationError)


class MVAppUserResource(ModelResource):

    class Meta:
        queryset = MV_app_user.objects.all()
        resource_name = "mv_app_user"
        filtering = {
            'contact' : ALL_WITH_RELATIONS,
        }
        allowed_methods = ['get']
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/register%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('register'),
                name="api_register"),
            url(r"^(?P<resource_name>%s)/update_profile%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('update_profile'),
                name="api_update_profile"),
            url(r"^(?P<resource_name>%s)/verify_registration%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('verify_registration'),
                name="api_verify_registration"),
            url(r"^(?P<resource_name>%s)/verify_update%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('verify_update'),
                name="api_verify_update"),
        ]

    def register(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/register with request: {}'.format(request))
            mv_app_user_id = None
            success = False
            version_code = request.REQUEST.get('version_name')
            number = get_formatted_number(request.REQUEST['number'])
            if not number:
                raise Exception("number not in correct format.")
            name = request.REQUEST['name']

            mv_app_user_set = MV_app_user.objects.filter(contact__number=number)
            if mv_app_user_set.filter(contact__name=name).exists():
                mv_app_user = mv_app_user_set.filter(contact__name=name).latest('id')
                mv_app_user.is_registered = False
                mv_app_user.save()
                logger.debug('User already exists for the credentials provided-UserId ::' + str(mv_app_user.id))
                query_type = MV_user_registration_detail.QueryType.EXISTING_USER_REGISTRATION
            else:
                logger.debug('Creating new mv_app_user.')
                contact = Contact.objects.create(name=name,
                                                 number=number)
                mv_app_user = MV_app_user(contact=contact)
                mv_app_user.app_user = generate_random_user()
                mv_app_user.save()
                query_type = MV_user_registration_detail.QueryType.NEW_USER_REGISTRATION

            mv_app_user_id = mv_app_user.id
            success = True
            MV_user_registration_detail.send_registration_message(mv_app_user,
                                                                  number,
                                                                  query_type, version_code)
        except Exception as e:
            error = 'exception while registering user: {}'.format(e)
            logger.exception(error)

        response = {'mv_app_user_id': mv_app_user_id, 'success': success}
        self.log_throttled_access(request)
        return self.create_response(request, response)

    def update_profile(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/update_profile with request: {}'.format(request))
            success = False
            version_code = request.REQUEST.get('version_name')
            mv_app_user = MV_app_user.objects.get(pk=request.REQUEST['userId'])
            number = get_formatted_number(request.REQUEST['number'])
            if not number:
                raise Exception("number not in correct format.")
            name = request.REQUEST['name']

            contact = mv_app_user.contact
            if contact.number != number:
                if contact.name != name:
                    query_type = MV_user_registration_detail.QueryType.UPDATE_NAME_AND_NUMBER
                else:
                    query_type = MV_user_registration_detail.QueryType.UPDATE_NUMBER

                MV_user_registration_detail.send_registration_message(mv_app_user,
                                                                      number,
                                                                      query_type, version_code)
                success = True
            elif contact.name != name:
                mv_app_user.contact.name = name
                mv_app_user.contact.save()
                query_type = MV_user_registration_detail.QueryType.UPDATE_NAME
                MV_user_registration_detail(mv_user=mv_app_user,
                                            query_type=query_type,
                                            expiry_time=datetime.now()).save()
                success = True
        except Exception as e:
            logger.exception('exception while updating profile: {}'.format(e))

        response = {'success': success}
        self.log_throttled_access(request)
        return self.create_response(request, response)

    def verify_registration(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/verify_registration with request: {}'.format(request))
            api_key = None
            success = False

            mv_user_id = request.REQUEST['userId']
            input_code = request.REQUEST['code']

            mv_user = MV_app_user.objects.get(pk=mv_user_id)
            user_details = MV_user_registration_detail.objects.filter(mv_user=mv_user,
                                                         expiry_time__gte=datetime.now())
            success = MV_registration_log.check_and_log_attempt(user_details, str(input_code))
            if success:
                mv_user.is_registered = True
                mv_user.save()
                api_key = ApiKey.objects.get(user=mv_user.app_user).key
        except Exception as e:
            logger.exception('exception while verify_registration: {}'.format(e))

        response = {'api_key': api_key, 'success': success}
        self.log_throttled_access(request)
        return self.create_response(request, response)

    def verify_update(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/verify_update with request: {}'.format(request))
            api_key = None
            success = False

            mv_user_id = request.REQUEST['userId']
            input_code = request.REQUEST['code']
            number = get_formatted_number(request.REQUEST['number'])
            if not number:
                raise Exception("number not in correct format.")
            name = request.REQUEST['name']

            mv_user = MV_app_user.objects.get(pk=mv_user_id)
            user_details = MV_user_registration_detail.objects.filter(mv_user=mv_user,
                                                         expiry_time__gte=datetime.now())
            success = MV_registration_log.check_and_log_attempt(user_details, input_code)
            if success:
                mv_user.contact.name = name
                mv_user.contact.number = number
                mv_user.contact.save()
                api_key = ApiKey.objects.get(user=mv_user.app_user).key
        except Exception as e:
            message = 'exception while verify_update: {}'.format(e)
            logger.exception(message)

        response = {'api_key': api_key, 'success': success, 'name':name, 'number': number[2:]}
        self.log_throttled_access(request)
        return self.create_response(request, response)


class ResourcelistItemCommentResource(ModelResource):
    # playlist_item_id = fields.IntegerField(attribute='playlist_item__id')
    # comment_user = fields.CharField(attribute="comment_user", null=True)
    # user_id = fields.IntegerField(attribute="user__id", null=True)

    class Meta:
        queryset = Resourcelist_item_comment.objects.all()
        resource_name = "mvapp/resourcelist_item_comment"
        filtering = {
            'playlist_item_id': 'exact',
        }
        allowed_methods = ['get', 'post']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/comment%s$" % (self._meta.resource_name, \
                trailing_slash()), self.wrap_view('comment'), name="api_comment"),
        ]

    def comment(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            success = False
            api_key = request.REQUEST['api_key']
            comment = request.REQUEST['comment']
            item_id = request.REQUEST['playlist_item_id']

            if (api_key and not ApiKey.objects.filter(key=api_key).exists()):
                raise Exception('api key doesnt exist.')

            user = ApiKey.objects.get(key=api_key).user
            item = Playlist_item.objects.get(id=item_id)

            comment = Playlist_item_comment.objects.create( \
                                            user=user,
                                            comment=comment,
                                            playlist_item=item)
            success = True
            comment_resource = PlaylistItemCommentResource()
            comment_bundle = comment_resource.build_bundle(obj=comment, request=request)

            self.log_throttled_access(request)
            return self.create_response(request, {'success': success, \
                    'comment': comment_resource.full_dehydrate(comment_bundle)})
        except Exception as e:
                success = False
                exception_str = ("exception while creating comment: %s"%(e))
                logger.exception(exception_str)
        return self.create_response(request, {'success': success, 'error': exception_str})


class ResourceGroupLibraryFoldersResource(ModelResource):
    class Meta:
        queryset = Resource_groups.objects.all()
        resource_name = "mvapp/res_group_lib_folders"
        allowed_methods = ['get']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_res_group_lib_folder'), name = "api_res_group_lib_folders" ),
        ]

    def create_response(self, request, data, response_class=HttpResponse, **response_kwargs):
        desired_format = self.determine_format(request)
        serialized = self.serialize(request, data, desired_format)
        response = response_class(content=serialized, content_type='application/json;charset=UTF-8', **response_kwargs)
        response['Content-Length'] = len(response.content)
        return response

    def get_res_group_lib_folder(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            project_id = request.REQUEST.get('project_id')
            mv_app_user_id = request.REQUEST.get('mv_app_user_id') # For future use
            
            project = None
            if project_id:
                project = Project.objects.get(id=project_id)
                res_group_lib_folders_logs = Res_group_lib_folders_log.objects.filter(project=project)
            else:
                res_group_lib_folders_logs = Res_group_lib_folders_log.objects.filter(project__isnull=True)

            mv_app_user = None
            # if mv_app_user_id:
            #     mv_app_user = MV_app_user.objects.get(id=mv_app_user_id)
            #     res_group_lib_folders_logs = res_group_lib_folders_logs.filter(mv_app_user=mv_app_user)
            # else:
            #     res_group_lib_folders_logs = res_group_lib_folders_logs.filter(mv_app_user__isnull=True)

            if res_group_lib_folders_logs.count()>0:
                data = res_group_lib_folders_logs[0].data
                data = ast.literal_eval(data)
            else:
                data = get_res_group_lib_folders(project, mv_app_user)
                Res_group_lib_folders_log(data=data).save()
            return self.create_response(request, {'success': True, 'data': data})
        except:
            logger.exception("Exception in get_res_group_lib_folders: %s" % request)
        return self.create_response(request, {'success': False, 'reason': "unable to get resource group and library folders"})


class NewsAppResource(ModelResource):
    class Meta:
        queryset = News.objects.all()
        resource_name = "mvapp_item"
        allowed_methods = ['post']

        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/create%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create' ), name = "api_create" ),
            url(r"^(?P<resource_name>%s)/create_mvapp_v2%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('create_mvapp_v2'), name="api_create_mvapp_v2"),
        ] 

    def create(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:
            title = request.REQUEST.get( 'title' ) or 'News, Created at %s' % time.strftime('%H:%M, %d %B %Y')
            transcript = request.REQUEST.get( 'transcript' )
            msisdn = ''
            msisdn = request.REQUEST.get('contact')
            number = msisdn
            if not msisdn:
                user_id = int(request.REQUEST['mv_app_user_id'].replace('"', ''))
                number = MV_app_user.objects.get(pk=user_id).contact.number
            number = get_formatted_number(number)
            time = datetime.now()
            if News.objects.filter(title=title, transcript=transcript, callerid=number, time__gte=time.date()).exists():
                return self.create_response( request, {'error': "content already uploaded"} )

            parent_id = request.REQUEST.get('parent_id')
            image_file = request.FILES.get('image_file')
            resource_list_id = int(request.REQUEST['resource_list_id'].replace('"', ''))
            if parent_id:
                parent_id = int(parent_id.replace('"', ''))
                is_comment = True
                parent = News.objects.get(id=parent_id)
                channel_id = parent.channel.id
                ai_id = parent.channel.ai.id
            else:
                is_comment = False
                channel = Resource_list.objects.get(id=resource_list_id).channel_for_upload
                if channel:
                    channel_id=channel.id
                    ai_id=channel.ai_id
                else:
                    logger.info("Channel for upload not configured")


            state='UNM'            
            news = News(ai_id=ai_id, channel_id=channel_id, is_comment=is_comment, 
                        state=state, source=News.Source.APP_INTERFACE, 
                        title=title, transcript=transcript, 
                        time=time, callerid=number, modified_date=time)

            if parent_id:
                news.parent_id_value = parent_id

            news.save()

            if parent_id:
                parent.comments.add(news)

            ai=App_instance.objects.get(id=ai_id)
            try:
                if ai.news_settings.autopublish:
                    state='PUB'
                for cl in ai.news_settings.autopublish_lists.all():
                    if number in cl.get_numbers():
                        state='PUB'
            except:
                logger.info( "Error" )	    
            if state=='PUB' :
                news.pub_order = 0
                news.autopublished = True
                news.tags = "autopub"
                news.state='PUB'
            uploaded_file = request.FILES[ 'audio_file' ]
            news_id = news.id
            recording_id = request.REQUEST.get( 'recording_id' )
            logger.debug( "upload_audio: user: %s, ai_id: %s, news_id: %s, rec_id: %s" % ( request.user.id, ai_id, news_id, recording_id ) )
            recording = recording_upload_impl( uploaded_file, ai_id, recording_id )
            
            rec_full_path = recording.get_full_filename()
            rec_export_path = "/tmp/"+rec_full_path.split('/')[-1]
            bit_rate='48k'
            ffmpegString = ( "ffmpeg -i %s -acodec libmp3lame -b:a %s %s" %(rec_full_path, bit_rate , rec_export_path) )
            retval = subprocess.call(ffmpegString.strip().split(' '))
            shutil.move(rec_export_path, rec_full_path)
            if not msisdn:
                data_dict = {'user_id': user_id}
                data = json.dumps(data_dict)
                news.misc = data
            news.detail = recording
            news.modified_date = datetime.now()

            if image_file:
                img = Image(name = os.path.basename(image_file.name))
                img.image = image_file
                img.save()
                news.sm_image = img

            news.save()
            newsapp_resource = NewsAppResource()
            newsapp_bundle = newsapp_resource.build_bundle( obj = news, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, newsapp_resource.full_dehydrate( newsapp_bundle ) )
        except:
            logger.exception("mvapp - create audio : %s" % request)
        return self.create_response( request, {'error': "unable to create recording"} )






    def create_mvapp_v2(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            title = request.REQUEST.get('title') or 'News, Created at %s' % time.strftime('%H:%M, %d %B %Y')
            transcript = request.REQUEST.get('transcript')
            number = request.REQUEST.get('contact')
            number = get_formatted_number(number)
            time = datetime.now()
            if News.objects.filter(title=title, transcript=transcript, callerid=number, time__gte=time.date()).exists():
                return self.create_response(request, {'error': "content already uploaded"})
            image_file = request.FILES.get('image_file')
            parent_id = request.REQUEST.get('parent_id')
            if parent_id:
                parent_id = int(parent_id.replace('"', ''))
                is_comment = True
                parent = News.objects.get(id=parent_id)
                channel_id = parent.channel.id
                ai_id = parent.channel.ai.id
            else:
                channel_id = request.REQUEST.get('channelid')
                ai_id = Channel.objects.get(id=channel_id).ai_id

            state = 'UNM'
            news = News(ai_id=ai_id, channel_id=channel_id, is_comment=is_comment,
                        state=state, source=News.Source.APP_INTERFACE,
                        title=title, transcript=transcript,
                        time=time, callerid=number, modified_date=time)

            if parent_id:
                news.parent_id_value = parent_id

            news.save()

            if parent_id:
                parent.comments.add(news)

            ai = App_instance.objects.get(id=ai_id)
            try:
                if ai.news_settings.autopublish:
                    state = 'PUB'
                for cl in ai.news_settings.autopublish_lists.all():
                    if number in cl.get_numbers():
                        state = 'PUB'
            except:
                logger.info("Error")
            if state == 'PUB':
                news.pub_order = 0
                news.autopublished = True
                news.tags = "autopub"
                news.state = 'PUB'
            uploaded_file = request.FILES['audio_file']
            news_id = news.id
            recording_id = request.REQUEST.get('recording_id')
            logger.debug("upload_audio: user: %s, ai_id: %s, news_id: %s, rec_id: %s" % (
            request.user.id, ai_id, news_id, recording_id))
            recording = recording_upload_impl(uploaded_file, ai_id, recording_id)

            rec_full_path = recording.get_full_filename()
            rec_export_path = "/tmp/" + rec_full_path.split('/')[-1]
            bit_rate = '48k'
            ffmpegString = ("ffmpeg -i %s -acodec libmp3lame -b:a %s %s" % (rec_full_path, bit_rate, rec_export_path))
            retval = subprocess.call(ffmpegString.strip().split(' '))
            shutil.move(rec_export_path, rec_full_path)
            news.detail = recording
            news.modified_date = datetime.now()

            if image_file:
                img = Image(name=os.path.basename(image_file.name))
                img.image = image_file
                img.save()
                news.sm_image = img

            news.save()
            newsapp_resource = NewsAppResource()
            newsapp_bundle = newsapp_resource.build_bundle(obj=news, request=request)

            self.log_throttled_access(request)
            return self.create_response(request, newsapp_resource.full_dehydrate(newsapp_bundle))
        except:
            logger.exception("mvapp - create audio : %s" % request)
        return self.create_response(request, {'error': "unable to create recording"})