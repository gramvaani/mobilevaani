from django.contrib import admin
from django import forms

from models import Channel_obd_setting, Resource_list, Resource_list_access, Resource_list_perm, Resource_groups, Library_folders

from django.contrib.admin.models import LogEntry
admin.site.register(LogEntry)


class ResourcelistAdmin(admin.ModelAdmin):
	filter_horizontal = ('tags', 'campaign_categories', 'formats', 'channels')

admin.site.register(Resource_list, ResourcelistAdmin)
admin.site.register(Channel_obd_setting)
admin.site.register(Resource_list_access)
admin.site.register(Resource_list_perm)
admin.site.register(Resource_groups)
admin.site.register(Library_folders)