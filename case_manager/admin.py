from django.contrib import admin

from models import Handler, Profile, Properties, Grievance_state, Action_setting, Grievance_quit_reason, Grievance_quit_reason_group, Grievance_ai_quit_reason_group 

class Handler_admin(admin.ModelAdmin):
    raw_id_fields=('location', )

admin.site.register(Handler, Handler_admin)
admin.site.register(Profile)
admin.site.register(Properties)
admin.site.register(Grievance_state)
admin.site.register(Action_setting)
admin.site.register(Grievance_quit_reason)
admin.site.register(Grievance_quit_reason_group)
admin.site.register(Grievance_ai_quit_reason_group)
