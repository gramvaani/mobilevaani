from case_manager.models import *
from case_manager.util import update_grievance_state_handler
from vapp.helpline.models import Call
from vapp.mnews.models import Channel, News_state, News, Forwarding_log, Forwarding_ai_properties, Forwarding_group_call_log, Groups_call_log, Event_sms_template, CallEvents
from vapp.sms.tasks import SMSTask
from vapp.telephony.statemachine import BaseVappController
from django.db.models import Q
from vapp.app_manager.models import VI_conf
from vapp.telephony.utils import *
from app_manager.common import get_queue_for_task
from vapp.mnews.tasks import *
from util import populate_item_misc_field
import json

from vapp.log import get_logger
logger = get_logger()


class CaseManagerController(BaseVappController):

    def getCallerSchedule(self):
        return self.properties.han_schedule if self.callerType==ACTIONEE_CHOICES.HANDLER else self.properties.ben_schedule

    def allowRecordingOnAnsMc(self):
        if self.callerType == ACTIONEE_CHOICES.HANDLER:
            return self.properties.han_access_ans_mc
        else:
            return self.properties.ben_access_ans_mc

    def scheduleForwardingCall(self, forwarding_log):

        vi_conf = VI_conf.objects.get(controller = 'mnews.forwardingvi.ForwardingController',
                                            description = 'mnews.forwardingvi.ForwardingStateDescriptionMap')

        properties = Forwarding_ai_properties.objects.get( ai = self.properties.mnews_ai )
        vi_data = RestrictedDict.convert_dict_to_rdict({ 'forwarding_log_id': forwarding_log.id, 'forwarding_properties_id': properties.id, 'vi_conf_id': vi_conf.id })
        ai = properties.ai_to_jump
        group_call_log = Groups_call_log(number = forwarding_log.forwarded_to, group_schedule = properties.forwarding_schedule, ai = ai)
        group_call_log.save()

        forwarding_gc_log = Forwarding_group_call_log(forwarding_log = forwarding_log, group_call_log = group_call_log )
        forwarding_gc_log.save()
        queue = get_queue_for_task(ai, default_queue='push')
        CallAndPlayTask.apply_async(args=[group_call_log.id, properties.max_tries],
                                    kwargs={'vi_data': vi_data}, eta=datetime.now(), queue=queue)


    def smsOnItemForwarding(self, callerid):
        try:
            params = {}
            params['destination'] = callerid
            params['reference_id'] = self.currgrievs.reference_id
            params['handler_name'] = self.currgrievs.handler.name
            template = Event_sms_template.objects.get(template__ai = self.properties.mnews_ai, event = SmsEvents.GRIEVANCE_FORWARDED).template
            message = template.process_template(params)
            sms = SMSTask.create_send_msg(self.properties.mnews_ai.id, message, callerid)
            return sms
        except Exception, e:
            logger.exception("Error in smsOnItemForwarding %s" % str(e))


    def setEventAfterRecordingInputref(self, prevState):
        if prevState == 'hanupdateinstructions':
            self.eventAfterRecordingInputRef = 'hanupdategrievscheckevent'
        elif prevState == 'handlermainbucketinstructions':
            self.eventAfterRecordingInputRef = 'handlermaincheckevent'
        elif prevState == 'nohanupdategrievsprompt':
            self.eventAfterRecordingInputRef = 'noupdateassignedgrievsinstructionevent'
        elif prevState == 'nobenupdategrievsprompt':
            self.eventAfterRecordingInputRef = 'noupdatebyhandleronassigngrievsinstructionevent'
        elif prevState == 'handlerupdatesavailablegrievs':
            self.eventAfterRecordingInputRef = 'handlerupdatesavailablegrievsevent'
        elif prevState == 'handlermainbucket':
            self.eventAfterRecordingInputRef = 'handlermaincheckevent'
        elif prevState == 'noupdateassignedgrievsinstruction':
            self.eventAfterRecordingInputRef = 'noupdateassignedgrievscheckevent'
        elif prevState == 'noupdatebyhandleronassigngrievsinstruction':
            self.eventAfterRecordingInputRef = 'noupdatebyhandlerassignedgrievscheckevent'
        elif prevState == 'nonoupdateassignedgrievsprompt':
            self.eventAfterRecordingInputRef = 'unassignedgrievsinstructionevent'
        elif prevState == 'nonoupdatebyhandlerassignedgrievsprompt':
            self.eventAfterRecordingInputRef = 'unassignedgrievstohandlerinstructionevent'
        elif prevState == 'noupdateassignedgrievs':
            self.eventAfterRecordingInputRef = 'noupdateassignedgrievsevent'
        elif prevState == 'noupdatebyhandleronassigngrievs':
            self.eventAfterRecordingInputRef = 'noupdatebyhandleronassigngrievsevent'
        elif prevState == 'unassignedgrievsinstruction':
            self.eventAfterRecordingInputRef = 'unassignedgrievscheckevent'
        elif prevState == 'unassignedgrievstohandlerinstruction':
            self.eventAfterRecordingInputRef = 'unassignedgrievstohandlercheckevent'
        elif prevState in ('nounassignedgrievsprompt','nounassignedgrievstohandlerprompt'):
            self.eventAfterRecordingInputRef = 'thankyouevent'
        elif prevState == 'unassignedgrievs':
            self.eventAfterRecordingInputRef = 'unassignedgrievsevent'
        elif prevState == 'unassignedgrievstohandler':
            self.eventAfterRecordingInputRef = 'unassignedgrievstohandlerevent'
        elif prevState == 'unresolvedgrievancesduringvalidation':
            self.eventAfterRecordingInputRef = 'unresolvedgrievancesduringvalidationevent'
	elif prevState == 'handlervalidactivegrievanceplay':
            self.eventAfterRecordingInputRef = 'handlervalidactivegrievanceplayevent'
        return self.eventAfterRecordingInputRef

    def __init__(self, ai, sessionData, vi_data):
        super(CaseManagerController, self).__init__(ai, sessionData, vi_data)
        self.sessionData = sessionData
        self.handlers = Handler.objects.filter(ai=self.ai)
        logger.info("IN INIT")
        self.properties = Properties.objects.filter(ai=self.ai)[0]
        self.actionSettings = Action_setting.objects.filter(profile=self.ai.ai_profile.all()[0])
        self.callerUuid = None

    def pre_outgoingstart(self):
        return self.getStartCallParams(self.sessionData.callerid)

    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.callerUuid = self.sessionData.uuids[0]
        return 'sm_action_success'

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def while_incomingstart__sm_action_success(self, events, eventData):
        self.callerUuid = self.sessionData.uuids[0]
        return 'sm_action_success'

    def pre_welcome(self):
        self.validation_index = 0
        self.handler_updates_index = 0
        self.handler_main_index = -1
        self.handler_main_jump_to_index = -1
        self.no_update_assigned_han_index = 0
        self.no_update_assigned_ben_index = 0
        self.ben_unassigned_index = 0
        self.han_unassigned_index = 0
        self.unresolved_index = 0
        self.grievance_item_mappings = Grievance_item_mapping.objects.all()
        self.grievance_objects = Grievance.objects.filter(ai=self.ai)
        self.ben_grievances = Grievance.objects.filter(beneficiary=self.sessionData.callerid, ai=self.ai)
        self.han_grievances = Grievance.objects.filter(handler__number=self.sessionData.callerid, ai=self.ai)
        self.han_unassigngrievances = Grievance.objects.filter(current_state__state='UNASSIGNED', ai=self.ai)
        self.pending_ben_validation_grievances = Grievance.objects.none()
        self.handler_id = Handler.objects.filter(number=self.sessionData.callerid)
        self.isPlayComment = False
        self.handlerupdatesavailablegrievscomment = False
        if len(self.handler_id):
            self.handler_id = self.handler_id[0].id
        else:
            self.handler_id = None

        if self.handler_id:
            self.samePartyType = ACTIONEE_CHOICES.HANDLER
        else:
            self.samePartyType = ACTIONEE_CHOICES.BENEFICIARY

        if self.properties.validation_type in ('BEN_VALIDATE', 'BEN_MOD_VALIDATE'):
            self.pending_ben_validation_grievances = self.ben_grievances.filter(current_state__state='MARKED_RESOLVED').order_by('-last_modified_datetime')[:self.properties.pending_ben_validation_limit]
        elif self.properties.validation_type=='MOD_BEN_VALIDATE':
            self.pending_ben_validation_grievances = self.ben_grievances.filter(current_state__state='VALIDATION').order_by('-last_modified_datetime')[:self.properties.pending_ben_validation_limit]

        self.pending_han_validation_grievances = Grievance.objects.none()

        if self.properties.validation_type in ('BEN_VALIDATE', 'MOD_VALIDATE', 'BEN_MOD_VALIDATE'):
            activity_objects = Activity_log.objects.filter(prev_state__state='MARKED_RESOLVED',next_state__state='INPROGRESS')
        elif self.properties.validation_type in ('MOD_BEN_VALIDATE'):
            activity_objects = Activity_log.objects.filter(Q(prev_state__state='MARKED_RESOLVED',next_state__state='INPROGRESS', actionee_type='MODERATOR') | Q(prev_state__state='INPROGRESS',next_state__state='VALIDATION', actionee_type='BENEFICIARY'))
        
        if self.handler_id:
            for activity_object in activity_objects:
                pending_grievances = Grievance.objects.filter(id=activity_object.grievance_id,handler__number=self.sessionData.callerid, ai=self.ai)
                if pending_grievances:
                    if str(pending_grievances[0].current_state) != 'CLOSED':
                        self.pending_han_validation_grievances = self.pending_han_validation_grievances | pending_grievances
        #self.pending_han_validation_grievances = self.han_grievances.filter(current_state__state='MARKED_RESOLVED').order_by('-last_modified_datetime')[:self.properties.pending_han_validation_limit]

        #elif self.properties.validation_type=='MOD_HAN_VALIDATE':
        #self.pending_han_validation_grievances = self.han_grievances.filter(current_state__state='VALIDATION').order_by('-last_modified_datetime')
        #    self.pending_han_validation_grievances = self.han_grievances.filter(current_state__state='VALIDATION').order_by('-last_modified_datetime')

        self.han_updates_available_grievances = self.ben_grievances.filter(current_state__state='INPROGRESS')
        self.list_handler_grievances = list(self.han_grievances.filter(current_state__state__in=['ASSIGNED','INPROGRESS']).order_by('-creation_datetime'))
        self.no_update_assigned_grievs_ben = []
        self.no_update_assigned_grievs_han = []
        self.last_update_by_han_assigned_grievs = []
        self.last_update_by_ben_assigned_grievs = []

        for griev in self.ben_grievances.filter(current_state__state='ASSIGNED').order_by('-creation_datetime'):
            if Grievance_item_mapping.objects.filter(grievance=griev, contributor_type=ACTIONEE_CHOICES.HANDLER).count()==0:
                self.no_update_assigned_grievs_ben.append(griev)
        for griev in self.ben_grievances.filter(current_state__state='INPROGRESS').order_by('-creation_datetime'):
            self.last_update_by_ben_assigned_grievs.append(griev)

        self.last_update_by_ben_assigned_grievs_dict = self.updates_on_available_grievances(self.last_update_by_ben_assigned_grievs, self.samePartyType)
        if len(self.last_update_by_ben_assigned_grievs_dict) > 0:
            for griev in self.last_update_by_ben_assigned_grievs_dict:
                self.no_update_assigned_grievs_ben.append(griev)
        self.no_update_assigned_grievs_ben = self.no_update_assigned_grievs_ben[:self.properties.no_update_assigned_grievs_ben_limit]

        for griev in self.han_grievances.filter(current_state__state='ASSIGNED').order_by('-creation_datetime'):
            if Grievance_item_mapping.objects.filter(grievance=griev, contributor_type=ACTIONEE_CHOICES.BENEFICIARY).count()==1:
                logger.info("no_update_assigned_grievs_han ::" + str(griev.__dict__))
                self.no_update_assigned_grievs_han.append(griev)
        for griev in self.han_grievances.filter(current_state__state='INPROGRESS').order_by('-creation_datetime'):
            self.last_update_by_han_assigned_grievs.append(griev)

        self.last_update_by_han_assigned_grievs_dict = self.updates_on_available_grievances(self.last_update_by_han_assigned_grievs, self.samePartyType)
        if len(self.last_update_by_han_assigned_grievs_dict) > 0:
            for griev in self.last_update_by_han_assigned_grievs_dict:
                self.no_update_assigned_grievs_han.append(griev)
        self.no_update_assigned_grievs_han = self.no_update_assigned_grievs_han[:self.properties.no_update_assigned_grievs_han_limit]

        self.ben_unassigned_grievs = self.ben_grievances.filter(current_state__state='UNASSIGNED').order_by('-creation_datetime')[:self.properties.ben_unassigned_grievs_limit]
        self.han_unassigned_grievs = self.han_unassigngrievances.order_by('-creation_datetime')[:self.properties.han_unassigned_grievs_limit]
        return self.getPromptParams('case_manager_welcome')

    def updates_on_available_grievances(self, updates_available_grievances, PartyType):
        logger.info("updates_available_grievances :: " + str(updates_available_grievances))
        updates_available_griev_update_dict = {}
        grievance_item_mappings = Grievance_item_mapping.objects.none()
        logger.info(str(updates_available_grievances))
        for each_grievance in updates_available_grievances:
            logger.info(str(each_grievance)+'-----')
            grievance_item_mapping_objs = Grievance_item_mapping.objects.filter(grievance=each_grievance, is_parent=False).order_by('-creation_time')
            logger.info(str(grievance_item_mapping_objs))
            if grievance_item_mapping_objs.exists() and grievance_item_mapping_objs[0].contributor_type==PartyType:
                recentmost_grievance_item_mapping = Grievance_item_mapping.objects.filter(id=grievance_item_mapping_objs[0].id)
                grievance_item_mappings = grievance_item_mappings | recentmost_grievance_item_mapping
            logger.info(str(grievance_item_mappings))
        if grievance_item_mappings.exists():
            for grievance_item_mapping in grievance_item_mappings.order_by('-creation_time'):
                updates_available_griev_update_dict[grievance_item_mapping.grievance] = grievance_item_mapping.item
        logger.info('updates_on_available_grievances' + str(updates_available_griev_update_dict))
        return updates_available_griev_update_dict

    def last_update_on_available_grievances(self, updates_available_grievances, PartyType):
        logger.info("last_update_on_available_grievances :: " + str(updates_available_grievances))
        updates_available_griev_update_dict = {}
        grievance_item_mappings = Grievance_item_mapping.objects.none()
        logger.info(str(updates_available_grievances))
        for each_grievance in updates_available_grievances:
            logger.info(str(each_grievance)+'-----')
            grievance_item_mapping_objs = Grievance_item_mapping.objects.filter(grievance=each_grievance, contributor_type=PartyType, is_parent=False).order_by('-creation_time')
            logger.info(str(grievance_item_mapping_objs))
            #logger.info('grievance_item_mapping_objs[0].id :: ' + str(grievance_item_mapping_objs[0].id))
            logger.info('grievance_item_mapping_objs.exists()' + str(grievance_item_mapping_objs.exists()))
            if grievance_item_mapping_objs.exists():
                recentmost_grievance_item_mapping = Grievance_item_mapping.objects.filter(id=grievance_item_mapping_objs[0].id)
                #logger.info('grievance_item_mapping_objs[0].id :: ' + str(grievance_item_mapping_objs[0].id))
                logger.info('recentmost_grievance_item_mapping :: ' + str(recentmost_grievance_item_mapping))
                grievance_item_mappings = grievance_item_mappings | recentmost_grievance_item_mapping
            logger.info(str(grievance_item_mappings))
        if grievance_item_mappings.exists():
            for grievance_item_mapping in grievance_item_mappings.order_by('-creation_time'):
                updates_available_griev_update_dict[grievance_item_mapping.grievance] = grievance_item_mapping.item
        logger.info('updates_on_available_grievances' + str(updates_available_griev_update_dict))
        return updates_available_griev_update_dict


    def updates_on_grievances(self, updates_available_grievances):
        logger.info("updates_on_grievances :: " + str(updates_available_grievances))
        updates_available_griev_update_dict = {}
        for each_grievance in updates_available_grievances:
            items_list = []
            grievance_item_mappings = Grievance_item_mapping.objects.filter(grievance=each_grievance).order_by('creation_time')
            if grievance_item_mappings.exists():
                for item_mapping in grievance_item_mappings:
                    items_list.append(item_mapping.item) 
            updates_available_griev_update_dict[each_grievance] = items_list
        return updates_available_griev_update_dict    

    def getReasonState(self, currentgrievs, digit):
        logger.info("digit :: " + str(digit))
        grievance_quit_reason_group = Grievance_ai_quit_reason_group.objects.filter(ai=self.ai)
        quit_reason_group = grievance_quit_reason_group[0].group
        quit_reasons = quit_reason_group.quit_reason

        reason = quit_reasons.filter(numeric_key=digit, available_to__in = [self.callerType,'HAN_MOD'])[0]
        if reason:
            update_grievance_state_handler(currentgrievs.id, Grievance_state.objects.get(state='HALTED_PROGRESS').id, ACTIONEE_CHOICES.HANDLER, Moderation_source.IVR, self.handler_id, self.handler_id, reason.id)

    def while_checkcallertype__sm_action_success(self, events, eventData):
        self.han_updates_available_griev_update_dict = {}
        self.dict_handler_grievances = {}
        try:
            self.handler = self.handlers.get(number=self.sessionData.callerid)
            self.callerType = ACTIONEE_CHOICES.HANDLER
            self.otherPartyType = ACTIONEE_CHOICES.BENEFICIARY
            self.dict_handler_grievances = self.updates_on_grievances(self.list_handler_grievances)
            self.dict_handler_grievances = {key:value for key,value in self.dict_handler_grievances.items()[0:self.properties.ben_updates_limit]}
            logger.info("in check_callertype__han_updates_available_griev_update_dict:"+str(self.han_updates_available_griev_update_dict) )
        except:
            self.callerType = ACTIONEE_CHOICES.BENEFICIARY
            self.otherPartyType = ACTIONEE_CHOICES.HANDLER
            self.han_updates_available_griev_update_dict = self.updates_on_available_grievances(self.han_updates_available_grievances, self.otherPartyType)
            self.han_updates_available_griev_update_dict = {key:value for key,value in self.han_updates_available_griev_update_dict.items()[0:self.properties.han_updates_limit]}
        finally:
            self.actionSettingsMap = self.actionSettings.filter(actionee=self.callerType)

    def while_checkschedule__sm_action_success(self, events, eventData):
        schedule = self.getCallerSchedule()
        return 'schedule_off' if schedule and not schedule.is_schedule_on() else 'schedule_on'

    def pre_scheduleoff(self):
        return self.getPromptParams('case_manager_schedule_off')

    def while_scheduleoff__sm_action_success(self, events, eventData):
        return 'record_msg' if self.allowRecordingOnAnsMc() else 'dont_record_msg'

    def pre_recordmessage(self):
        self.call = Call(ai = self.ai, handler = Handler.RECORDER, callerid = self.sessionData.callerid)
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.call, Call.media, ai = self.properties.mnews_ai.id)

    def while_instructions__sm_action_success(self, events, eventData):
        if self.callerType==ACTIONEE_CHOICES.HANDLER:
            if len(self.dict_handler_grievances)>0:
                return 'updates_instructions_handler_end'
            return 'noupdate_on_assigngrievsinstruction'
        else:
            if Action_setting.objects.filter(prev_state__state='MARKED_RESOLVED').filter(Q(actionee__isnull=True) | Q(actionee='BENEFICIARY')).exists() \
                  and self.pending_ben_validation_grievances.exists():
                return 'ben_validateinvalidate_instructions'
            else:
                if len(self.han_updates_available_griev_update_dict)>0:
                    return 'handler_updates_instructions'
                return 'noupdateassignedgrievsinstruction'

    def pre_noupdategrievanceprompt(self):
        return self.getPromptPlayAndGetParams(promptName='case_manager_no_update_on_grievances', tries=3)            

    def pre_benvalidateinstructions(self):
        return self.getPromptPlayAndGetParams(promptName='case_manager_beneficiary_instruction', tries=3)

    def pre_hanvalidateinstructions(self):
        return self.getPromptPlayAndGetParams(promptName='case_manager_handler_instruction', tries=3)

    def pre_benvalidateinvalidate(self, events, eventData):
        self.currGrievForBenValidation = self.pending_ben_validation_grievances[self.validation_index]
        self.grievItem = Grievance_item_mapping.objects.get(grievance=self.currGrievForBenValidation, is_parent=True).item
        return self.getPlaybackParams(self.grievItem.detail.get_full_filename())

    def pre_lastupdatebyhandlerprompt(self):
        return self.getPromptPlayAndGetParams(promptName='case_manager_griev_update_play_post_prompt', tries=3)

    def pre_lastupdatebyhandler(self, events, eventData):
        logger.info("self.currGrievForBenValidation " + str(self.currGrievForBenValidation))
        lastupdatebyhan_dict = self.last_update_on_available_grievances([self.currGrievForBenValidation], self.otherPartyType)
        self.lastupdate = lastupdatebyhan_dict.get(self.currGrievForBenValidation)
        logger.info(str(self.lastupdate) + 'self.lastupdate')
        return self.getPromptPlayAndGetParams( promptAudio= self.lastupdate.detail, timeout = 10000, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)
        
    def while_lastupdatebyhandler__sm_get_digits_no_digits(self, events, eventData):
        if self.validation_index>=self.pending_ben_validation_grievances.count():
            if len(self.han_updates_available_griev_update_dict)>0:
                return 'no_ben_pending_validation_grievance'
            return 'noupdateassignedgrievsinstruction'
        return 'next_ben_pending_validation_grievance'

    def while_lastupdatebyhandler__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit ::' + str(digit))
        if str(digit) == '2':
            return 'invalidate_resolution'
        elif str(digit) == '*':
            return 'validate_resolution'

    def pre_unresolvedgrievancesduringvalidation(self):
        logger.info("unresolvedgrievancesduringvalidation")
        logger.info("self.pending_han_validation_grievances :: " + str(self.pending_han_validation_grievances.count()))
        if self.unresolved_index < len(self.pending_han_validation_grievances):
            self.currBenUpdatedGriev = self.pending_han_validation_grievances[self.unresolved_index]
            self.current_item = Grievance_item_mapping.objects.get(grievance=self.currBenUpdatedGriev, is_parent=True).item
            #return self.getPlaybackParams(self.current_item.detail.get_full_filename())
            return self.getPromptPlayAndGetParams( promptAudio= self.current_item.detail, timeout = 10000, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)

    def pre_benvalidateresolution(self, events, eventData):
        return self.getPromptPlayAndGetParams(promptName='case_manager_ben_validate_resolution', maxDigits=9, tries=3)

    def check_ben_updates_available_grievs(self):
        if self.handler_main_index>=len(self.dict_handler_grievances):
            logger.info("no_ben_update_griev_available ::" + "update_index_ben_updates_available_grievs")
            return 'no_ben_update_griev_available'
        else:
            logger.info("ben_update_griev_available ::" + "update_index_ben_updates_available_grievs")
            return 'ben_update_griev_available'

    def pre_hanmarkresolution(self, prevState):
        return self.getPromptPlayAndGetParams(promptName='case_manager_han_mark_resolution', maxDigits=9, tries=3)

    def pre_beninvalidateresolution(self, events, eventData):
        return self.getPromptPlayAndGetParams(promptName='case_manager_ben_invalidate_resolution', maxDigits=9, tries=3)

    def while_benvalidateinvalidate__sm_action_success(self, events, eventData):
        self.validation_index += 1
        return 'sm_action_success'

    def update_index_han_unresolvedgrievances(self):
        logger.info("update_index_han_unresolvedgrievances")
        self.unresolved_index += 1
        logger.info("unresolved_index" + str(self.unresolved_index))
        if self.unresolved_index>=len(self.pending_han_validation_grievances):
            return 'no_han_pending_validation_grievance'
        else:
            return 'han_pending_validation_grievance'

    def while_unresolvedgrievancesduringvalidation__dtmf_1(self, events, eventData):
        logger.info("while_unresolvedgrievancesduringvalidation__dtmf_1")
        return self.update_index_han_unresolvedgrievances()

    def while_unresolvedgrievancesduringvalidation__dtmf_4(self, events, eventData):
        logger.info("unresolvedgrievancesduringvalidation__dtmf_4")
        logger.info("unresolved_index" + str(self.unresolved_index))
        if self.unresolved_index!=0:
            self.unresolved_index -= 1

    def while_unresolvedgrievancesduringvalidation__sm_action_success(self, events, eventData):
        logger.info("while_unresolvedgrievancesduringvalidation__sm_action_success")
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))

        if str(digit) == '4':
            if self.unresolved_index!=0:
                self.unresolved_index -= 1
            if self.unresolved_index>=len(self.pending_han_validation_grievances):
                return 'no_han_pending_validation_grievance'
            else:
                return 'han_pending_validation_grievance'
        else:
            if str(digit) == '1':
                return self.update_index_han_unresolvedgrievances()
            elif str(digit) == '2':
                return 'give_update'
            elif str(digit) == '3':
                return 'quit_grievance'
            elif str(digit) == '5':
                return 'forward_current_grievance'
            elif str(digit) == '6':
                return 'random_access_grievance'
            elif str(digit) == '9':
                return 'call_beneficiary'
            elif str(digit) == '*':
                return 'mark_greviance_resolved'
            elif str(digit) == '0':
                return 'skip_bucket'

    def while_unresolvedgrievancesduringvalidation__sm_get_digits_no_digits(self, events, eventData):
        return self.update_index_han_unresolvedgrievances()
    
    def while_unresolvedgrievancescheck__sm_action_success(self, events, eventData):
        if self.unresolved_index < len(self.pending_han_validation_grievances):
            return 'han_pending_validation_grievance'
        return 'no_han_pending_validation_grievance'

    def pre_nounresolvedgrievancesprompt(self):
        return self.getPromptParams('case_manager_no_han_resolved_grievances_exists')

    def pre_benconfirmvalidate(self):
        if self.properties.validation_type in ('BEN_VALIDATE', 'MOD_BEN_VALIDATE'):
            update_grievance_state_handler(self.currGrievForBenValidation.id, Grievance_state.objects.get(state='CLOSED').id, self.callerType, News.Source.VOICE_INTERFACE)
        elif self.properties.validation_type=='BEN_MOD_VALIDATE':
            update_grievance_state_handler(self.currGrievForBenValidation.id, Grievance_state.objects.get(state='VALIDATION').id, self.callerType, News.Source.VOICE_INTERFACE)
        return self.getPromptParams('case_manager_ben_validation_thank_you')

    def pre_benconfirminvalidate(self):
        if self.properties.validation_type in ('BEN_VALIDATE', 'MOD_BEN_VALIDATE', 'BEN_MOD_VALIDATE'):
            update_grievance_state_handler(self.currGrievForBenValidation.id, Grievance_state.objects.get(state='INPROGRESS').id, self.callerType, News.Source.VOICE_INTERFACE)
        return self.getPromptParams('case_manager_ben_invalidation_thank_you')

    def while_benconfirmvalidate__sm_action_success(self, events, eventData):
        self.validation_index += 1
        if self.validation_index>=self.pending_ben_validation_grievances.count():
            return 'hanupdateinstructionsevent'
        else:
            return 'nextpendingbenvalidationgrievs'

    def while_benconfirminvalidate__sm_action_success(self, events, eventData):
        return self.while_benconfirmvalidate__sm_action_success(events, eventData)
    
    def while_bencancelinvalidate__sm_action_success(self, events, eventData):
        return self.while_benconfirmvalidate__sm_action_success(events, eventData)

    def while_bencancelvalidate__sm_action_success(self, events, eventData):
        return self.while_benconfirmvalidate__sm_action_success(events, eventData)
        
    def pre_hanconfirmresolve(self):
        return self.getPromptParams('case_manager_ben_validation_thank_you')

    def while_hanconfirmresolve__sm_action_success(self, events, eventData):
        update_grievance_state_handler(self.list_handler_grievances[self.handler_main_index].id, Grievance_state.objects.get(state='MARKED_RESOLVED').id, ACTIONEE_CHOICES.HANDLER, Moderation_source.IVR, self.handler_id, self.handler_id)
        return 'handlermaincheckevent'

    def pre_hancancelresolve(self):
        return self.getPromptParams('case_manager_ben_invalidation_thank_you')

    def while_hancancelresolve__sm_action_success(self, events, eventData):
        return 'handlermaincheckevent'

    def pre_hanupdateinstructions(self):
        return self.getPromptPlayAndGetParams(promptName='case_manager_handler_updates_instruction', tries=3)

    def pre_handlermainbucketinstructions(self):
        return self.getPromptPlayAndGetParams(promptName='case_manager_updates_on_grievs_instruction', tries=3)

    def while_hanupdategrievscheck__sm_action_success(self, events, eventData):
        if self.handlerupdatesavailablegrievscomment:
            return 'han_update_comment'
        elif self.handler_updates_index < len(self.han_updates_available_griev_update_dict):
            return 'han_update_grievs_exist'
        return 'han_update_grievs_no_exist'
    
            
    def while_handlermaincheck__sm_action_success(self, events, eventData):

        self.handler_main_index += 1    
        if self.handler_main_jump_to_index >= 0:
            self.handler_main_index = self.handler_main_jump_to_index
        self.handler_main_jump_to_index = -1

        self.isPlayComment = False

        if self.handler_main_index < len(self.list_handler_grievances):
            return 'handler_main_next_grievs_exist'
        self.handler_main_index = -1    
        return 'handler_main_no_next_grievs_exist'

    def pre_nohanupdategrievsprompt(self):
        return self.getPromptParams('case_manager_no_han_update_grievances_exists')

    def pre_nobenupdategrievsprompt(self):
        return self.getPromptParams('case_manager_no_ben_update_grievances_exists')

    def update_index_han_updates_available_grievs(self):
        if self.handlerupdatesavailablegrievscomment == False:
            self.handler_updates_index += 1
            
        if self.handler_updates_index>=len(self.han_updates_available_griev_update_dict):
            logger.info("han_update")
            return 'no_han_update_griev_available'
        else:
            logger.info("han")
            return 'han_update_griev_available'

    def update_index_ben_updates_available_grievs(self):
        if self.isPlayComment == False:
            self.handler_main_index += 1
            
        if self.handler_main_index>=len(self.dict_handler_grievances):
            logger.info("no_ben_update_griev_available ::" + "update_index_ben_updates_available_grievs")
            return 'no_ben_update_griev_available'
        else:
            logger.info("ben_update_griev_available ::" + "update_index_ben_updates_available_grievs")
            return 'ben_update_griev_available'

    def pre_handlerupdatesavailablegrievs(self):
        if self.handlerupdatesavailablegrievscomment:
            self.currHanUpdatedGrievUpdate = self.han_updates_available_griev_update_dict[self.currHanUpdatedGriev]
            #return self.getPlaybackParams(self.currHanUpdatedGrievUpdate.detail.get_full_filename())
            return self.getPromptPlayAndGetParams( promptAudio= self.currHanUpdatedGrievUpdate.detail, timeout = 10000, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)
        elif self.handler_updates_index < len(self.han_updates_available_griev_update_dict):
            logger.info(str(self.han_updates_available_griev_update_dict)+'++++++++')
            logger.info(str(self.handler_updates_index))
            self.currHanUpdatedGriev = list(self.han_updates_available_griev_update_dict.keys())[self.handler_updates_index]
            self.currGrievItemHanUpdated = Grievance_item_mapping.objects.get(grievance=self.currHanUpdatedGriev, is_parent=True).item
            #return self.getPlaybackParams(self.currGrievItemHanUpdated.detail.get_full_filename())
            return self.getPromptPlayAndGetParams( promptAudio= self.currGrievItemHanUpdated.detail, timeout = 1, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)
    
    def while_handlerupdatesavailablegrievs__sm_get_digits_no_digits(self, events, eventData):
        if self.handlerupdatesavailablegrievscomment == False:
            self.handler_updates_index += 1
            return 'play_comment'
        if self.handlerupdatesavailablegrievscomment:
            self.grievs_available = self.update_index_han_updates_available_grievs()
            self.handlerupdatesavailablegrievscomment = False
            return self.grievs_available

    def while_handlerupdatesavailablegrievs__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))
        if str(digit) == '4':
            if self.handler_updates_index !=0:
                self.handler_updates_index -= 1
            if self.handlerupdatesavailablegrievscomment:
                self.handlerupdatesavailablegrievscomment = False
            return 'play_previous_grievance'
        else:
            if str(digit) == '1':
                self.grievs_check = self.update_index_han_updates_available_grievs()
                if self.grievs_check == 'han_update_griev_available':
                    if self.handlerupdatesavailablegrievscomment:
                        self.handlerupdatesavailablegrievscomment = False
                    return 'play_next_grievance'
                else:
                    return self.grievs_check
            elif str(digit) == '2':
                return 'give_update'
            elif str(digit) == '3':
                return 'record_grievance'
            elif str(digit) == '6':
                return 'random_access_grievance'
            elif str(digit) == '9':
                return 'call_handler'
            elif str(digit) == '0':
                return 'skip_bucket'


    def while_handlerupdatesavailablegrievs__dtmf_1(self, events, eventData):
        self.grievs_check = self.update_index_han_updates_available_grievs()
        if self.grievs_check == 'han_update_griev_available':
            if self.handlerupdatesavailablegrievscomment:
                self.handlerupdatesavailablegrievscomment = False
            return 'play_next_grievance'
        else:
            return self.grievs_check

    def while_handlerupdatesavailablegrievs__dtmf_4(self, events, eventData):
        if self.handler_updates_index !=0:
            self.handler_updates_index -= 1
        if self.handlerupdatesavailablegrievscomment:
            self.handlerupdatesavailablegrievscomment = False
        return 'play_previous_grievance'
            
    def pre_handlermainbucket(self):
        self.current_grievance = self.list_handler_grievances[self.handler_main_index]
        if self.isPlayComment:
            self.current_update = self.dict_handler_grievances[self.current_grievance][-1]
            return self.getPromptPlayAndGetParams( promptAudio= self.current_update.detail, timeout = 10000, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)
        else:
            self.current_item = self.dict_handler_grievances[self.current_grievance][0]
            return self.getPromptPlayAndGetParams( promptAudio= self.current_item.detail, timeout = 1, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)

    def while_handlermainbucket__sm_get_digits_no_digits(self, events, eventData):
        if self.isPlayComment == False and len(self.dict_handler_grievances[self.current_grievance]) > 1:
            self.isPlayComment = True
            return 'play_comment'
        else:    
            self.isPlayComment = False
            return "play_next_grievance"
 
    def while_handlermainbucket__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))
        
        if str(digit) == '1':
            self.handler_main_jump_to_index = self.handler_main_index +1
            return 'play_next_grievance'
        elif str(digit) == '2':
            return 'give_update'
        elif str(digit) == '3':
            return 'quit_grievance'
        elif str(digit) == '4':
            self.handler_main_jump_to_index = self.handler_main_index -1
            return 'play_previous_grievance'
        elif str(digit) == '5':
            return 'forward_current_grievance'
        elif str(digit) == '6':
            return 'random_access_grievance'
        elif str(digit) == '9':
            return 'call_beneficiary'
        elif str(digit) == '0':
            return 'skip_bucket'

    def while_handlermainbucket__dtmf_0(self, events, eventData):
        return 'skip_bucket'

    def while_handlermainbucket__dtmf_1(self, events, eventData):
        self.handler_main_jump_to_index = self.handler_main_index +1 
        return 'play_next_grievance'
       

    def while_handlermainbucket__dtmf_4(self, events, eventData):
        self.handler_main_jump_to_index = self.handler_main_index - 1
        return 'play_previous_grievance'

    def while_handlermainbucket__dtmf_2(self, events, eventData):
        return 'give_update'

    def while_handlermainbucket__dtmf_3(self, events, eventData):
        return 'quit_grievance'

    def while_handlermainbucket__dtmf_5(self, events, eventData):
        return 'forward_current_grievance'

    def while_handlermainbucket__dtmf_6(self, events, eventData):
        return 'random_access_grievance'
    
    def while_handlermainbucket__dtmf_9(self, events, eventData):
        return 'call_beneficiary'

    def pre_handlerupdateavaibleprompt(self):
        return self.getPromptParams(promptName='case_manager_griev_update_play')   

    def pre_updategrievplaypostprompt(self):
        return self.getPromptParams(promptName='case_manager_griev_update_play')

    def while_grievupdateplay(self, events, eventData):
        if self.callerType==ACTIONEE_CHOICES.HANDLER:
            self.isPlayComment = True
            return 'han_update_grievs'
        else:
            self.handlerupdatesavailablegrievscomment = True
            return 'ben_update_grievs'

    def pre_beneficiaryrecordupdateprompt(self, prevState):
        if prevState == 'checkifresolvedgrievance':
            self.updateOnGrievanceItem = self.benReferencedGrievanceItem
            self.updateOnGrievance = self.benReferencedGrievance
        else:
            self.updateOnGrievanceItem = self.currGrievItemHanUpdated
            self.updateOnGrievance = self.currHanUpdatedGriev

        if prevState=='handlerupdatesavailablegrievs':
            event = self.update_index_han_updates_available_grievs()
            if event:
                self.postbeneficiaryupdateevent = event
            else:
                self.postbeneficiaryupdateevent = 'han_update_griev_available'
        elif prevState=='noupdateassignedgrievs':
            event = self.update_index_ben_noupdateassignedgrievs()
            if event:
                self.postbeneficiaryupdateevent = event
            else:
                self.postbeneficiaryupdateevent = 'no_han_update_griev_available'
        else:
            event = self.update_index_ben_unassignedgrievs()
            if event:
                self.postbeneficiaryupdateevent = event
            else:
                self.postbeneficiaryupdateevent = 'unassigned_grievs'

        return self.getPromptParams('case_manager_record_update')

    def pre_handlerrecordupdateprompt(self, prevState):
        logger.info("handlerrecordupdateprompt prevState :: " + str(prevState))
        if prevState == 'checkifhandlerresolvedgrievance':
            self.updateOnGrievanceItem = self.hanReferencedGrievanceItem
            self.updateOnGrievance = self.hanReferencedGrievance
            self.posthandlerupdateevent = self.eventAfterRecordingInputRef
        else:
            self.updateOnGrievanceItem = self.current_item
            self.updateOnGrievance = self.list_handler_grievances[self.handler_main_index]

        if prevState=='handlermainbucket':
            self.posthandlerupdateevent = 'handlermaincheckevent'
       
        return self.getPromptParams('case_manager_record_update')

    def pre_beneficiaryrecordupdate(self):
        title = 'Update from Beneficiary, %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        news_misc = populate_item_misc_field(self.updateOnGrievance, True, False)
        self.newUpdate = News(ai_id=self.properties.mnews_ai.id, channel=self.updateOnGrievanceItem.channel,
                              callerid=self.sessionData.callerid,
                              state=News_state.UNM, is_comment=True,
                              source=News.Source.VOICE_INTERFACE, title=title, cm_properties=json.dumps(news_misc))
        self.newUpdate.parent_id_value = self.updateOnGrievanceItem.id
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newUpdate, News.detail, ai = self.properties.mnews_ai.id)

    def while_beneficiaryrecordupdate__sm_action_success(self, events, eventData):
        if self.properties.allow_ben_update_autopub:
            self.newUpdate.state = 'PUB'
        self.newUpdate.save()
        if self.newUpdate.is_comment:
            parent = News.objects.get(pk = self.newUpdate.parent_id_value)
            parent.comments.add(self.newUpdate)

        Grievance_item_mapping(grievance=self.updateOnGrievance, item=self.newUpdate, creation_time=self.newUpdate.time, contributor=self.newUpdate.callerid, contributor_type=ACTIONEE_CHOICES.BENEFICIARY).save()
        if self.updateOnGrievance.current_state.state=='ASSIGNED' and self.callerType==ACTIONEE_CHOICES.BENEFICIARY:
            update_grievance_state_handler(self.updateOnGrievance.id, Grievance_state.objects.get(state='INPROGRESS').id, self.callerType, News.Source.VOICE_INTERFACE)
        try:
            #if self.properties.sms_template_handler_on_update:
            params = {}
            params['reference_id'] = self.updateOnGrievance.reference_id
            template = Event_sms_template.objects.get(template__ai = self.properties.mnews_ai, event = SmsEvents.GRIEVANCE_UPDATE_BY_BENEFICIARY).template
            message = template.process_template(params)
            sms_msg = SMSTask.create_send_msg(self.properties.mnews_ai.id, message, self.updateOnGrievance.handler.number)
            Sms_log.objects.create(grievance=self.updateOnGrievance, sms_message=sms_msg, recipient_type=ACTIONEE_CHOICES.HANDLER)
        except Exception as e:
            logger.exception('exception while sending grievance update notification %s - %s' %(self.updateOnGrievance.id, e))
        return 'sm_action_success'

    def post_beneficiaryrecordupdate(self):
        self.newUpdate = None

    def pre_handlerrecordupdate(self):
        title = 'Update from Handler, %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        news_misc = populate_item_misc_field(self.updateOnGrievance, True, False)
        self.newUpdate = News(ai_id=self.properties.mnews_ai.id, channel=self.updateOnGrievanceItem.channel,
                              callerid=self.sessionData.callerid,
                              state=News_state.UNM, is_comment=True,
                              source=News.Source.VOICE_INTERFACE, title=title, cm_properties=json.dumps(news_misc))
        self.newUpdate.parent_id_value = self.updateOnGrievanceItem.id
        self.newUpdate.save()
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newUpdate, News.detail, ai = self.properties.mnews_ai.id)

    def while_handlerrecordupdate__sm_action_success(self, events, eventData):
        if self.properties.allow_han_update_autopub:
            self.newUpdate.state = 'PUB'
        self.newUpdate.save()

        if self.newUpdate.is_comment:
            parent = News.objects.get(pk = self.newUpdate.parent_id_value)
            parent.comments.add(self.newUpdate)

        Grievance_item_mapping(grievance=self.updateOnGrievance, item=self.newUpdate, creation_time=self.newUpdate.time, contributor=self.newUpdate.callerid, contributor_type=ACTIONEE_CHOICES.HANDLER).save()
        #if self.updateOnGrievance.current_state.state=='ASSIGNED' and self.callerType==ACTIONEE_CHOICES.HANDLER:
        update_grievance_state_handler(self.updateOnGrievance.id, Grievance_state.objects.get(state='INPROGRESS').id, ACTIONEE_CHOICES.HANDLER, Moderation_source.IVR, self.handler_id, self.handler_id)
        try:
            #if self.sms_template_beneficiary_on_update:
            params = {}
            params['reference_id'] = self.updateOnGrievance.reference_id
            template = Event_sms_template.objects.get(template__ai = self.properties.mnews_ai, event = SmsEvents.GRIEVANCE_UPDATE_BY_HANDLER).template
            message = template.process_template(params)
            sms_msg = SMSTask.create_send_msg(self.properties.mnews_ai.id, message, self.updateOnGrievance.beneficiary)
            Sms_log.objects.create(grievance=self.updateOnGrievance, sms_message=sms_msg, recipient_type=ACTIONEE_CHOICES.BENEFICIARY)
        except Exception,e:
            logger.exception('exception while sending grievance update notification %s - %s' %(self.updateOnGrievance.id, e))
        return 'sm_action_success'

    def post_handlerrecordupdate(self):
        self.newUpdate = None

    def pre_postbeneficiaryupdate(self, prevState):
        return self.getPromptParams('case_manager_record_update_thank_you')

    def while_postbeneficiaryupdate__sm_action_success(self, events, eventData):
        self.currHanUpdatedGriev.current_state = Grievance_state.objects.get(state='INPROGRESS')
        if prevState == "handlerupdatesavailablegrievs":
            event = self.update_index_han_updates_available_grievs()
            if event:
                return event
            logger.info("event" + str(event))
            return "han_update_griev_available"
        elif prevState == "noupdateassignedgrievsinstruction":
            event = self.update_index_ben_noupdateassignedgrievs()
            if event:
                return event
            return "no_han_update_griev_available"
            logger.info("event" + str(event))
        else:
            event = self.update_index_ben_unassignedgrievs()
            if event:
                return event
            logger.info("unassigned_grievs")
            return "unassigned_grievs"

    def while_handlerassgntoinprogressenabled__sm_action_success(self, events, eventData):
        self.currBenUpdatedGriev.current_state = Grievance_state.objects.get(state='INPROGRESS')
        if prevState == "handlermainbucket":
        	return 'ben_update_griev_available'
        elif prevState == "noupdatebyhandleronassigngrievsinstruction":
        	return 'no_ben_update_griev_available'
        elif prevState == "unassignedgrievstohandlerinstruction":
        	return 'unassigned_grievs_tohan'

    def while_postbeneficiaryupdate__sm_action_success(self, events, eventData):
        return self.postbeneficiaryupdateevent

    def post_postbeneficiaryupdate(self):
        self.postbeneficiaryupdateevent = None

    def pre_posthandlerupdate(self, prevState):
        return self.getPromptParams('case_manager_record_update_thank_you')

    def while_posthandlerupdate__sm_action_success(self, events, eventData):
        logger.info(str(self.posthandlerupdateevent) + "self.posthandlerupdateevent")
        return self.posthandlerupdateevent

    def while_handlerassgntoinprogressdisabled__sm_action_success(self, events, eventData):
        if prevState == "handlermainbucket":
            event = self.while_handlermainbucket__play_next_grievance()
            if event:
                return event
            return "ben_update_griev_available"
        elif prevState == "noupdatebyhandleronassigngrievsinstruction":
            event = self.while_noupdatebyhandleronassigngrievsinstruction__play_next_grievance()
            if event:
                return event
            return "no_ben_update_griev_available"
        else:
            return "unassigned_grievs_tohan"

    def pre_beneficiarywaitprompt(self, prevState):
        if prevState in ('noupdateassignedgrievs', 'handlerupdatesavailablegrievs', 'unassignedgrievs'):
            self.callOnGrievanceItem = self.currGrievItemHanUpdated
            self.callOnGrievance = self.currHanUpdatedGriev
        return self.getPromptParams('case_manager_please_wait')

    def pre_handlerwaitprompt(self, prevState):
        if prevState in ('unresolvedgrievancesduringvalidation', 'handlermainbucket', 'noupdatebyhandleronassigngrievs'):
            self.callOnGrievanceItem = self.current_item
            self.callOnGrievance = self.list_handler_grievances[self.handler_main_index]
        return self.getPromptParams('case_manager_please_wait')

    def pre_callhandler(self):
        return self.getStartCallParams(self.currHanUpdatedGriev.handler.number)

    def pre_callbeneficiary(self):
        logger.info("Number :: " + str(self.list_handler_grievances[self.handler_main_index].beneficiary))
        return self.getStartCallParams(self.list_handler_grievances[self.handler_main_index].beneficiary)

    def while_callhandler__sm_action_failure(self, events, eventData):
        logger.info("while_callhandler__sm_action_failure eventData"+str(eventData))
        uuid = eventData.get('Unique_ID')
        if uuid and uuid == self.callerUuid:
            return 'caller_hung_up'
        else:
            return 'handler_hung_up'

    def while_callbeneficiary__sm_action_success(self, events, eventData):
        logger.info('callbeneficiary__sm_action_success eventData:'+str(eventData))

    def while_callbeneficiary__sm_action_failure(self, events, eventData):
        logger.info('callbeneficiary__sm_action_failure eventData:'+str(eventData))
        uuid = eventData.get('Unique_ID')
        if uuid and uuid == self.callerUuid:
            return 'caller_hung_up'
        else:
            return 'beneficiary_hung_up'

    def pre_nobenavailable(self):
        logger.info("pre_nobenavailable: "+str(self.callerUuid))
        logger.info("pre_nobenavailable: "+str(self.sessionData.uuids[0]))
        return self.getPromptParams('case_manager_ben_unavailable', uniqueID = self.sessionData.uuids[0])

    def pre_recordcall(self):
        if self.callerType==ACTIONEE_CHOICES.HANDLER:
            title = 'Conversation from Handler with Beneficiary,'
        else:
            title = 'Conversation from Beneficiary with Handler,'

        title += ' %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        default_channel = Channel.get_default_channel(self.properties.mnews_ai.id)
        news_misc = populate_item_misc_field(self.callOnGrievance, True, True)
        self.newConversation = News(ai_id=self.properties.mnews_ai.id, channel=default_channel,
                                    callerid=self.sessionData.callerid,
                                    state=News_state.UNM, is_comment=True,
                                    source=News.Source.VOICE_INTERFACE, title=title, cm_properties=json.dumps(news_misc))
        self.newConversation.parent_id_value = self.callOnGrievanceItem.id

        self.newConversation.save()
	
	if self.newConversation.is_comment:
            parent = News.objects.get(pk = self.newConversation.parent_id_value)
            parent.comments.add(self.newConversation)

        Grievance_item_mapping(grievance=self.callOnGrievance, item=self.newConversation,
                               creation_time=self.newConversation.time,
                               contributor=self.newConversation.callerid,
                               contributor_type=self.callerType,
                               is_conversation=True).save()
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newConversation, News.detail, ai = self.properties.mnews_ai.id)

    def pre_bridgecalls(self):
        logger.info("pre_bridgecalls sessionData.uuids:"+str(self.sessionData.__dict__))
        return self.getBridgeCallsParams(self.sessionData.uuids[0], self.sessionData.uuids[1])

     
       

    def pre_recordgrievanceprompt(self, prevState):
        self.setEventAfterRecordingInputref(prevState)
        return self.getPromptParams('case_manager_record_grievance')

    def pre_recordgrievance(self):
        title = 'Grievance, %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        default_channel = Channel.get_default_channel(self.properties.mnews_ai.id)
        self.newGrievance = Grievance(ai=self.ai,
                                      current_state=Grievance_state.objects.get(state='UNMODERATED'),
                                      beneficiary=self.sessionData.callerid)
        self.newGrievance.save()
        news_misc = populate_item_misc_field(self.newGrievance, False, False)

        self.newGrievanceItem = News(ai_id=self.properties.mnews_ai.id,
                                 channel=default_channel,
                                 callerid=self.sessionData.callerid,
                                 state=News_state.UNM, is_comment=False,
                                 source=News.Source.VOICE_INTERFACE, title=title, cm_properties=json.dumps(news_misc))
        self.newGrievanceItem.save()
        Grievance_item_mapping(grievance=self.newGrievance, item=self.newGrievanceItem,
                               is_parent=True, creation_time=self.newGrievanceItem.time,
                               contributor=self.newGrievanceItem.callerid,
                               contributor_type=ACTIONEE_CHOICES.BENEFICIARY,
                               is_conversation=False).save()
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newGrievanceItem, News.detail, ai = self.properties.mnews_ai.id)

    def while_recordgrievance__sm_action_success(self, events, eventData):
        try:
            self.newGrievanceItem.save()
            return 'sm_action_success'
        except:
            return 'grievance_record_error'

    def pre_recordgrievancethankyou(self):
        return self.getPromptParams('case_manager_record_grievance_thank_you')

    def while_recordgrievancethankyou__sm_action_success(self, events, eventData):
        return self.eventAfterRecordingInputRef

    def post_recordgrievancethankyou(self):
        self.eventAfterRecordingInputRef = None

    def pre_handlerforwardgrievs(self, prevState):
        return self.getPromptPlayAndGetParams( 'case_manager_forward_input_number', timeout = 10000, tries = 3, minDigits = 10, maxDigits = 10, errorPromptName = 'case_manager_forward_invalid_number' )

    def while_handlerforwardgrievs__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        logger.info('digits :: '+str(digits))

        self.currgrievs=self.list_handler_grievances[self.handler_main_index]

        callerid = '91' + str(digits)
        griev_item = Grievance_item_mapping.objects.get(grievance=self.currgrievs,is_parent=True).item
       
        logger.info("Handler__Forward")
        if self.currgrievs:
            forwarded_sms = self.smsOnItemForwarding(str(digits))
            forwarding_log = Forwarding_log(ai = self.ai, forwarding_cdr = self.sessionData.cdrs[0], forwarded_item = griev_item, forwarded_to = str(digits), forwarding_event = "FFC", forwarded_sms = forwarded_sms)
            forwarding_log.save()
            self.forwarding_log = forwarding_log

    def pre_handlerrecordpmprompt(self, prevState):
        return self.getPromptParams( 'case_manager_forward_pm_prompt' )

    def pre_handlerrecordpm(self, prevState):
        self.forwarding_properties = Forwarding_ai_properties.objects.get( ai = self.properties.mnews_ai)
        return self.getEmbeddedRecordingParams(self.forwarding_properties.personal_msg_time_secs, self.forwarding_log, Forwarding_log.forwarder_personal_recording, ai = self.properties.mnews_ai.id)

    def pre_forwardthankyou(self, prevState):
        self.scheduleForwardingCall(self.forwarding_log)
        return self.getPromptParams( 'case_manager_forward_thank_you' )

    def pre_nothandlerprompt(self, prevState):
        return self.getPromptParams('case_manager_not_a_handler')

    def while_nothandlerprompt__sm_action_success(self, events, eventData):
        if self.prevForwardState == 'unresolvedgrievsduringvalidation':
            return 'unresolvedgrievancesduringvalidationevent'
        elif self.prevForwardState == 'handlermainbucket':
            return 'handlermaincheckevent'
        elif self.prevForwardState == 'noupdatebyhandleronassigngrievs':
            return 'noupdatebyhandleronassigngrievsevent'
        elif self.prevForwardState == 'handlervalidactivegrievanceplay':
            return self.eventAfterRecordingInputRef

    def pre_quitgrievanceprompt(self, prevState):
        return self.getPromptPlayAndGetParams(promptName = 'case_manager_quit_grievance_instruction', tries = 3)

    def while_quitgrievanceprompt__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info("digit :: " + str(digit))
        grievance_quit_reason_group = Grievance_ai_quit_reason_group.objects.filter(ai=self.ai)
        quit_reason_group = grievance_quit_reason_group[0].group
        quit_reasons = quit_reason_group.quit_reason
        reason_queryset = quit_reasons.filter(numeric_key=digit, available_to__in = [self.callerType,'HAN_MOD'])
        reason = None    
        if reason_queryset.exists():
            reason = reason_queryset[0]
            update_grievance_state_handler(currentgrievs.id, Grievance_state.objects.get(state='HALTED_PROGRESS').id, ACTIONEE_CHOICES.HANDLER, Moderation_source.IVR, self.handler_id, self.handler_id, reason.id)
        else:
            return 'not_a_valid_reason_choice'

    def pre_handlerinprogresstohaltenabled(self, prevState):
        if prevState == "unresolvedgrievancesduringvalidation":
            self.unresolved_index += 1
            self.handlerinprogresstohaltenabledevent = 'unresolvedgrievances_duringvalidation'
        if prevState == "handlermainbucket":
            #self.handler_main_index += 1
            self.handlerinprogresstohaltenabledevent = 'beneficiaryupdates_availablegrievs'
        elif prevState == "noupdatebyhandleronassigngrievs":
            self.no_update_assigned_han_index += 1
            self.handlerinprogresstohaltenabledevent = 'noupdatebyhandler_onassigngrievs'

    def pre_handlerinprogresstohaltdisabled(self, prevState):
        if prevState == "unresolvedgrievancesduringvalidation":
            self.unresolved_index += 1
            self.handlerinprogresstohaltdisabled = 'unresolvedgrievances_duringvalidation'
        if prevState == "handlermainbucket":
            #self.handler_main_index += 1
            self.handlerinprogresstohaltdisabled = 'beneficiaryupdates_availablegrievs'
        elif prevState == "noupdatebyhandleronassigngrievs":
            self.no_update_assigned_han_index += 1
            self.handlerinprogresstohaltdisabled = 'noupdatebyhandler_onassigngrievs'

    def pre_beneficiaryinputrefid(self, prevState):
        self.setEventAfterRecordingInputref(prevState)
        return self.getPromptPlayAndGetParams(promptName='case_manager_input_reference_id', maxDigits=9, tries=3)

    def pre_handlerinputrefid(self, prevState):
        return self.getPromptPlayAndGetParams(promptName='case_manager_input_reference_id', maxDigits=9, tries=3)

    def while_beneficiaryinputrefid__sm_action_success(self, events, eventData):
        try:
            reference_id = self.getPlayAndGetDigits(eventData)
            self.benReferencedGrievance = self.ben_grievances.get(reference_id=reference_id)
            self.benReferencedGrievanceItem = Grievance_item_mapping.objects.get(grievance=self.benReferencedGrievance, is_parent=True).item
            if str(self.benReferencedGrievance.current_state) in ('CLOSED', 'CLOSED_UNRESOLVED'):
                return 'valid_closed_grievance'
            return 'valid_active_grievance'
        except:
            return 'invalid_grievance'

    def while_handlerinputrefid__sm_action_success(self, events, eventData):
        try:
            reference_id = self.getPlayAndGetDigits(eventData)
            self.hanReferencedGrievance = self.grievance_objects.get(reference_id=int(reference_id))
            if self.hanReferencedGrievance.handler_id is not None:
                if self.hanReferencedGrievance.handler_id == Handler.objects.get(number = self.sessionData.callerid).id:
                    self.hanReferencedGrievanceItem = Grievance_item_mapping.objects.get(grievance=self.hanReferencedGrievance, is_parent=True).item
                    if str(self.hanReferencedGrievance.current_state) in ('CLOSED', 'CLOSED_UNRESOLVED'):
                        return 'valid_closed_grievance'
                    self.handler_main_jump_to_index = self.list_handler_grievances.index(self.hanReferencedGrievance)      
                    return 'valid_active_grievance'
                else:
                    return 'non_assigned_grievance_to_you'
            else:
                return 'non_assigned_grievance_to_anyone'
        except  Exception as e:
            logger.info("handlerinputrefid exception:"+str(e))
            return 'invalid_grievance'

    def while_handlerinputrefid__sm_get_digits_no_digits(self, events, eventData):
        return self.eventAfterRecordingInputRef

    def pre_beneficiaryinvalidgrievance(self):
        return self.getPromptParams('case_manager_invalid_grievance')

    def pre_handlerinvalidgrievance(self):
        return self.getPromptParams('case_manager_invalid_grievance')

    def pre_beneficiaryvalidactivegrievanceplayinstruction(self):
        return self.getPromptParams('case_manager_accessed_valid_active_grievance')

    def pre_handlervalidactivegrievanceplayinstruction(self):
        return self.getPromptParams('case_manager_accessed_valid_active_grievance')

    def pre_beneficiaryvalidclosedgrievanceplayinstruction(self):
        return self.getPromptParams('case_manager_accessed_valid_closed_grievance')

    def pre_handlervalidclosedgrievanceplayinstruction(self):
        return self.getPromptParams('case_manager_accessed_valid_closed_grievance')

    def pre_beneficiaryvalidactivegrievanceplay(self):
        return self.getPlaybackParams(self.benReferencedGrievanceItem.detail.get_full_filename())

    def while_beneficiaryvalidactivegrievanceplay__sm_action_success(self, events, eventData):
        lastupdatebyhandict = self.last_update_on_available_grievances([self.benReferencedGrievance], self.otherPartyType)
        self.lastupdatebyhanitem = lastupdatebyhandict.get(self.benReferencedGrievance)
        if self.lastupdatebyhanitem:
            return 'lastupdatebyhan_exists'
        else:
            return self.eventAfterRecordingInputRef
        return self.eventAfterRecordingInputRef

    def pre_lastupdateonbeninputrefid(self):
        return self.getPlaybackParams(self.lastupdatebyhanitem.detail.get_full_filename())

    def while_lastupdateonbeninputrefid__sm_action_success(self, events, eventData):
        logger.info(str(self.eventAfterRecordingInputRef) + 'self.eventAfterRecordingInputRef')
        return self.eventAfterRecordingInputRef

    def pre_handlervalidactivegrievanceplay(self):
        return self.getPlaybackParams(self.hanReferencedGrievanceItem.detail.get_full_filename())

    def while_handlervalidactivegrievanceplay__sm_action_success(self, events, eventData):
        lastupdatebybendict = self.updates_on_grievances([self.hanReferencedGrievance])
        update_on_grievance = lastupdatebybendict.get(self.hanReferencedGrievance)
        self.lastupdatebybenitem = None
        if update_on_grievance and (len(update_on_grievance) > 1):
            self.lastupdatebybenitem = update_on_grievance[-1]
            return 'lastupdatebyben_exists'
        else:
            return self.eventAfterRecordingInputRef

    def pre_lastupdateonhaninputrefid(self):
        return self.getPlaybackParams(self.lastupdatebybenitem.detail.get_full_filename())

    def while_lastupdateonhaninputrefid__sm_action_success(self, events, eventData):
        return self.eventAfterRecordingInputRef

    def pre_handlervalidclosedgrievanceplay(self):
        return self.getPlaybackParams(self.hanReferencedGrievanceItem.detail.get_full_filename())

    def while_handlervalidclosedgrievanceplay__sm_action_success(self, events, eventData):
        return self.eventAfterRecordingInputRef

    def pre_handlerresolvedgrievanceprompt(self):
        return self.getPromptParams('case_manager_accessed_valid_closed_grievance')

    def while_handlerresolvedgrievanceprompt__sm_action_success(self, events, eventData):
        return self.eventAfterRecordingInputRef

    def post_handlerresolvedgrievanceprompt(self):
        self.eventAfterRecordingInputRef = None

    def pre_nonassignedtohandlergrievanceprompt(self):
        return self.getPromptParams('case_manager_accessed_unassigned_grievance_to_anyone')

    def while_nonassignedtohandlergrievanceprompt__sm_action_success(self, events, eventData):
        logger.info("nonassignedtoanyhandler :: " + str(self.eventAfterRecordingInputRef))
        return self.eventAfterRecordingInputRef

    def pre_nonassignedtocurrenthandlergrievanceprompt(self):
        return self.getPromptParams('case_manager_accessed_unassigned_grievance_to_you')

    def pre_beneficiaryvalidclosedgrievanceplay(self):
        return self.getPlaybackParams(self.benReferencedGrievanceItem.detail.get_full_filename())

    def pre_reopenedgrievanceprompt(self):
        return self.getPromptParams('case_manager_reopened_griev_prompt')

    def while_reopenedgrievanceprompt__sm_action_success(self, events, eventData):
        update_grievance_state_handler(self.benReferencedGrievance.id, Grievance_state.objects.get(state='REOPENED').id, self.callerType, News.Source.VOICE_INTERFACE)
        return self.eventAfterRecordingInputRef

    def post_reopenedgrievanceprompt(self):
        self.eventAfterRecordingInputRef = None

    def while_checkifresolvedgrievance__sm_action_success(self, events, eventData):
        if str(self.benReferencedGrievance.current_state) in ('CLOSED', 'CLOSED_UNRESOLVED'):
            return 'resolved_grievance'
        else:
            return 'active_grievance'

    def while_checkifhandlerresolvedgrievance__sm_action_success(self, events, eventData):
        if str(self.hanReferencedGrievance.current_state) in ('INPROGRESS', 'ASSIGNED', 'MARKED_RESOLVED'):
            return 'active_grievance'
        elif str(self.hanReferencedGrievance.current_state) in ('CLOSED', 'CLOSED_UNRESOLVED'):
            return 'resolved_grievance'

    def pre_noupdateassignedgrievsinstruction(self):
        return self.getPromptParams('case_manager_no_updateassigned_beneficiary')

    def pre_noupdatebyhandleronassigngrievsinstruction(self):
        return self.getPromptParams('case_manager_noupdateassignedgrievhan')

    def pre_nonoupdateassignedgrievsprompt(self):
        return self.getPromptParams('case_manager_no_ben_noupdate_assigned_grievs')

    def while_nonoupdateassignedgrievsprompt__sm_action_success(self, events, eventData):
        if len(self.ben_unassigned_grievs)>0:
            return 'unassigned_grievs_ben_exist'
        return 'thankyou'

    def while_noupdateassignedgrievscheck__sm_action_success(self, events, eventData):
        if len(self.no_update_assigned_grievs_ben)>0:
            return 'no_update_assigned_grievs_ben_exist'
        return 'no_update_assigned_grievs_ben_no_exist'

    def while_noupdatebyhandlerassignedgrievscheck__sm_action_success(self, events, eventData):
        if self.no_update_assigned_han_index < len(self.no_update_assigned_grievs_han):
            return 'no_update_assigned_grievs_han_exist'
        return 'no_update_assigned_grievs_han_no_exist'

    def pre_nonoupdatebyhandlerassignedgrievsprompt(self):
        return self.getPromptParams('case_manager_no_han_noupdate_assigned_grievs')

    def pre_noupdateassignedgrievs(self, events, eventData):
        self.currHanUpdatedGriev = self.no_update_assigned_grievs_ben[self.no_update_assigned_ben_index]
        self.currGrievItemHanUpdated = Grievance_item_mapping.objects.get(grievance=self.currHanUpdatedGriev, is_parent=True).item
        #return self.getPlaybackParams(self.currGrievItemHanUpdated.detail.get_full_filename())
        return self.getPromptPlayAndGetParams( promptAudio= self.currGrievItemHanUpdated.detail, timeout = 10000, tries = 1, minDigits = 1, maxDigits = 1 )

    def update_index_ben_noupdateassignedgrievs(self):
        self.no_update_assigned_ben_index += 1
        if self.no_update_assigned_ben_index>=len(self.no_update_assigned_grievs_ben):
            if len(self.ben_unassigned_grievs)>0:
                return 'no_han_update_griev_available'
            return 'thankyou'
        else:
            return 'han_update_griev_available'
    
    def while_noupdateassignedgrievs__sm_get_digits_no_digits(self, events, eventData):
        return self.update_index_ben_noupdateassignedgrievs()
 
    def while_noupdateassignedgrievs__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))

        if str(digit) == '4':
            if self.no_update_assigned_ben_index !=0:
                self.no_update_assigned_ben_index -= 1
            if self.no_update_assigned_ben_index>=len(self.no_update_assigned_grievs_ben):
                if len(self.ben_unassigned_grievs)>0:
                    return 'no_han_update_griev_available'
                return 'thankyou'
            else:
                return 'han_update_griev_available'
        else:
            if str(digit) == '1':
                return self.update_index_ben_noupdateassignedgrievs()
            elif str(digit) == '2':
                return 'give_update'
            elif str(digit) == '3':
                return 'record_grievance'
            elif str(digit) == '6':
                return 'random_access_grievance'
            elif str(digit) == '9':
                return 'call_handler'
            elif str(digit) == '*':
                return 'mark_greviance_resolved'
            elif str(digit) == '0':
                return 'skip_bucket'



    def while_noupdateassignedgrievs__dtmf_1(self, events, eventData):
        return self.update_index_ben_noupdateassignedgrievs()

    def while_noupdateassignedgrievs__dtmf_4(self, events, eventData):
        if self.no_update_assigned_ben_index !=0:
            self.no_update_assigned_ben_index -= 1

    def pre_noupdatebyhandleronassigngrievs(self):
        if self.no_update_assigned_han_index < len(self.no_update_assigned_grievs_han):
            logger.info("no_update_assigned_grievs_han :: " + str(self.no_update_assigned_grievs_han))
            logger.info("no_update_assigned_han_index :: " + str(self.no_update_assigned_han_index))
            self.currBenUpdatedGriev = self.no_update_assigned_grievs_han[self.no_update_assigned_han_index]
            logger.info("currBenUpdatedGriev :: " + str(self.currBenUpdatedGriev.__dict__))
            self.current_item = Grievance_item_mapping.objects.get(grievance=self.currBenUpdatedGriev, is_parent=True).item
            #return self.getPlaybackParams(self.current_item.detail.get_full_filename())
            return self.getPromptPlayAndGetParams( promptAudio= self.current_item.detail, timeout = 10000, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)

    def update_index_han_noupdatebyhandleronassignedgrievs(self):
        self.no_update_assigned_han_index += 1
        logger.info("no_update_assigned_han_index :: " + str(self.no_update_assigned_han_index))
        if self.no_update_assigned_han_index>=len(self.no_update_assigned_grievs_han):
            if len(self.han_unassigned_grievs)>0:
                return 'no_noupdatebyhandleronassignedgrievs'
            return 'thank_you'
        else:
            return 'noupdate_byhandleronassignedgrievs'

    def while_noupdatebyhandleronassigngrievs__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))
        if str(digit) == '4':
            if self.no_update_assigned_han_index !=0:
                self.no_update_assigned_han_index -= 1
            if self.no_update_assigned_han_index>=len(self.no_update_assigned_grievs_han):
                if len(self.han_unassigned_grievs)>0:
                    return 'no_noupdatebyhandleronassignedgrievs'
                return 'thank_you'
            else:
                return 'noupdate_byhandleronassignedgrievs'
        else:
            if str(digit) == '1':
                return self.update_index_han_noupdatebyhandleronassignedgrievs()
            elif str(digit) == '2':
                return 'give_update'
            elif str(digit) == '3':
                return 'quit_grievance'
            elif str(digit) == '5':
                return 'forward_current_grievance'
            elif str(digit) == '6':
                return 'random_access_grievance'
            elif str(digit) == '9':
                return 'call_beneficiary'
            elif str(digit) == '*':
                return 'mark_greviance_resolved'
            elif str(digit) == '0':
                return 'skip_bucket'

    def while_noupdatebyhandleronassigngrievs__sm_get_digits_no_digits(self, events, eventData):
        return self.update_index_han_noupdatebyhandleronassignedgrievs()
 
    def while_noupdatebyhandleronassigngrievs__dtmf_1(self, events, eventData):
        return self.update_index_han_noupdatebyhandleronassignedgrievs()

    def while_noupdatebyhandleronassigngrievs__dtmf_4(self, events, eventData):
        if self.no_update_assigned_han_index !=0:
            self.no_update_assigned_han_index -= 1

    def pre_unassignedgrievsinstruction(self):
        return self.getPromptParams('case_manager_unassigned_grievances_ben')

    def pre_unassignedgrievstohandlerinstruction(self):
        return self.getPromptParams('case_manager_unassigned_grievances_han')

    def while_unassignedgrievscheck__sm_action_success(self, events, eventData):
        if self.ben_unassigned_index < len(self.ben_unassigned_grievs):
            return 'unassigned_grievs_ben_exist'
        return 'unassigned_grievs_ben_no_exist'

    def while_unassignedgrievstohandlercheck__sm_action_success(self, events, eventData):
        if self.han_unassigned_index < len(self.han_unassigned_grievs):
            return 'unassigned_grievs_han_exist'
        return 'unassigned_grievs_han_no_exist'

    def pre_nounassignedgrievsprompt(self):
        return self.getPromptParams('case_manager_no_unassigned_grievances_exists')

    def pre_nounassignedgrievstohandlerprompt(self):
        return self.getPromptParams('case_manager_no_unassigned_grievances_exists')

    def pre_unassignedgrievs(self, events, eventData):
        logger.info("unassignedgrievs")
        self.currHanUpdatedGriev = self.ben_unassigned_grievs[self.ben_unassigned_index]
        self.currGrievItemHanUpdated = Grievance_item_mapping.objects.get(grievance=self.currHanUpdatedGriev, is_parent=True).item
        #return self.getPlaybackParams(self.currGrievItemHanUpdated.detail.get_full_filename())
        return self.getPromptPlayAndGetParams( promptAudio= self.currGrievItemHanUpdated.detail, timeout = 10000, tries = 1, minDigits = 1, maxDigits = 1 )

    def pre_unassignedgrievstohandler(self):
        if self.han_unassigned_index < len(self.han_unassigned_grievs):
            self.currBenUpdatedGriev = self.han_unassigned_grievs[self.han_unassigned_index]
            self.current_item = Grievance_item_mapping.objects.get(grievance=self.currBenUpdatedGriev, is_parent=True).item
            #return self.getPlaybackParams(self.current_item.detail.get_full_filename())
            return self.getPromptPlayAndGetParams( promptAudio= self.current_item.detail, timeout = 10000, tries = 1, minDigits = 0, maxDigits = 0, regularExp=True)

    def update_index_ben_unassignedgrievs(self):
        self.ben_unassigned_index += 1
        if self.ben_unassigned_index>=len(self.ben_unassigned_grievs):
            return 'ben_no_unassigned_griev_available'
        else:
            return 'ben_unassigned_griev_available'
    
    def while_unassignedgrievs__sm_get_digits_no_digits(self, events, eventData):
        return self.update_index_ben_unassignedgrievs()

    def while_unassignedgrievs__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))
        if str(digit) == '4':
            if self.ben_unassigned_index !=0:
                self.ben_unassigned_index -= 1
            if self.ben_unassigned_index>=len(self.ben_unassigned_grievs):
                return 'ben_no_unassigned_griev_available'
            else:
                return 'ben_unassigned_griev_available'
        else:
            if str(digit) == '1':
                return self.update_index_ben_unassignedgrievs()
            elif str(digit) == '2':
                return 'give_update'
            elif str(digit) == '3':
                return 'record_grievance'
            elif str(digit) == '6':
                return 'random_access_grievance'
            elif str(digit) == '0':
                return 'skip_bucket'

    def while_unassignedgrievs__dtmf_1(self, events, eventData):
        return self.update_index_ben_unassignedgrievs()

    def while_unassignedgrievs__dtmf_4(self, events, eventData):
        if self.ben_unassigned_index !=0:
            self.ben_unassigned_index -= 1

    def update_index_han_unassignedgrievs(self):
        self.han_unassigned_index += 1
        if self.han_unassigned_index>=len(self.han_unassigned_grievs):
            return 'han_no_griev_available'
        else:
            return 'han_griev_available'
    
    def while_unassignedgrievstohandler__sm_get_digits_no_digits(self, events, eventData):
        return self.update_index_han_unassignedgrievs()

    def while_unassignedgrievstohandler__sm_action_success(self, events, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info('digit' + str(digit))
        if str(digit) == '4':
            if self.han_unassigned_index !=0:
                self.han_unassigned_index -= 1
            if self.han_unassigned_index>=len(self.han_unassigned_grievs):
                return 'han_no_griev_available'
            else:
                return 'han_griev_available'
        else:
            if str(digit) == '1':
                return self.update_index_han_unassignedgrievs()
            elif str(digit) == '2':
                return 'self_assign'
            elif str(digit) == '6':
                return 'random_access_grievance'
            elif str(digit) == '0':
                return 'skip_bucket'

    def while_unassignedgrievstohandler__dtmf_1(self, events, eventData):
        return self.update_index_han_unassignedgrievs()

    def while_unassignedgrievstohandler__dtmf_4(self, events, eventData):
        if self.han_unassigned_index !=0:
            self.han_unassigned_index -= 1

    def pre_nogrievtoplayprompt(self):
        return self.getPromptParams('case_manager_nogrievtoplayprompt')

    def pre_selfassigngreviance(self, prevState):
        if prevState == 'nonassignedtohandlergrievanceprompt':
            self.postselfassignevent = self.eventAfterRecordingInputRef
        elif prevState == 'unassignedgrievstohandler':
            self.postselfassignevent = 'unassignedgrievstohandlerevent'
        return

    def while_selfassigngreviance__sm_action_success(self, events, eventData):
        self.hanunassignedgrievs = self.han_unassigned_grievs[self.han_unassigned_index]
        self.han_unassigned_index += 1
        update_grievance_state_handler(self.hanunassignedgrievs.id,Grievance_state.objects.get(state='ASSIGNED').id,ACTIONEE_CHOICES.HANDLER,Moderation_source.IVR,self.handler_id,self.handler_id)

    def pre_selfassigngrevianceprompt(self, prevState):
        return self.getPromptParams('case_manager_self_assign_grievance')

    def while_selfassigngrevianceprompt__sm_action_success(self, events, eventData):
        return self.postselfassignevent

    def handlerpermissioncheck(self, events, eventData, prevState):
        if prevState == "posthandlerupdate":
            if Action_setting.objects.filter(profile=self.ai.ai_profile.all()[0], prev_state='ASSIGNED', next_state='INPROGRESS', actionee=ACTIONEE_CHOICES.HANDLER):
                return "han_assgntoinprogress_enabled"
            else:
                return "han_assgntoinprogress_disabled"
        elif prevState == "quitgrievanceprompt":
            if Action_setting.objects.filter(profile=self.ai.ai_profile.all()[0], prev_state='INPROGRESS', next_state='HALTED_PROGRESS', actionee=ACTIONEE_CHOICES.HANDLER):
                return 'han_quit_permission_enabled'
            else:
                return 'han_quit_permission_disabled'
        elif prevState == "hanmarkresolution":
            if Action_setting.objects.filter(profile=self.ai.ai_profile.all()[0], prev_state='INPROGRESS', next_state='MARK_RESOLVED', actionee=ACTIONEE_CHOICES.HANDLER):
                return 'han_inprogresstomarkedresolved_enabled'
            else:
                return 'han_inprogresstomarkedresolved_disabled'

    def pre_thankyou(self):
        return self.getPromptParams('case_manager_thank_you')


CaseManagerStateDescriptionMap = [
    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url'],
        },
    },
    {   'name':'incomingstart',
        'action':'answer',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
        },
    },
    {   'name':'welcome',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkcallertype':['sm_action_success'],
        },
    },
    {   'name':'checkcallertype',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkschedule':['sm_action_success'],
        },
    },
    {   'name':'checkschedule',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'instructions':['schedule_on'],
            'scheduleoff':['schedule_off'],
        },
    },
    {   'name':'scheduleoff',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure', 'dont_record_msg'],
            'recordmessage':['record_msg'],
        },
    },
    {   'name':'recordmessage',
        'action':'record',
        'transitions': {
            'thankyou':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'instructions',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'benvalidateinstructions':['ben_validateinvalidate_instructions'],
            'hanvalidateinstructions':['han_validateinvalidate_instructions'], #not used
            'hanupdateinstructions':['handler_updates_instructions'],
            'handlermainbucketinstructions':['updates_instructions_handler_end'], #handler flow
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstruction'],
            'noupdategrievanceprompt':['noupdate_on_assigngrievsinstruction'], #handler flow
        },    
    },
    {   'name':'noupdategrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievstohandlerinstruction':['sm_action_success'],
        },
    },
    {   'name':'handlermainbucketinstructions',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermaincheck':['sm_action_success'],
            'handlerinputrefid':['random_access_grievance'],
            'unassignedgrievstohandlerinstruction':['skip_bucket'],
        },
        'dtmf': {
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'handlermaincheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermainbucket':['handler_main_next_grievs_exist'],
            'unassignedgrievstohandlerinstruction':['handler_main_no_next_grievs_exist']
        },
    },
    {   'name':'handlermainbucket',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            #'handlermainbucket':['play_next_grievance', 'play_previous_grievance', 'ben_update_comment', 'ben_update_griev_available', 'no_update_handler_flow'],
            'handlermaincheck':['play_next_grievance', 'play_previous_grievance'],
            #'updategrievplaypostprompt':['sm_action_success', 'play_comment'],
            'handlerupdateavaibleprompt':['play_comment'],
            'handlerrecordupdateprompt':['give_update'],
            'unassignedgrievstohandlerinstruction':['no_ben_update_griev_available', 'skip_bucket'],
            'handlerwaitprompt':['call_beneficiary'],
            'quitgrievanceprompt':['quit_grievance'],
            'handlerforwardgrievs':['forward_current_grievance'],
            'handlerinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'give_update',
            3:'quit_grievance',
            4:'play_previous_grievance',
            5:'forward_current_grievance',
            6:'random_access_grievance',
            9:'call_beneficiary',
            0:'skip_bucket',
        },
    },
    {  'name':'handlerupdateavaibleprompt',
        'action':'playback',
        'transitions':{
            'handlermainbucket':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'handlerrecordupdateprompt',
        'action':'playback',
        'transitions': {
            'handlerrecordupdate':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'handlerrecordupdate',
        'action':'record',
        'transitions': {
            'hanmarkresolution':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'hanmarkresolution',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            #'handlerpermissioncheck':['confirm'],
            'hanconfirmresolve':['confirm'],
            'hancancelresolve':['cancel'],
            'thankyou':['sm_get_digits_no_digits'],
        },
        'dtmf': {
            1:'confirm',
            2:'cancel'
        }
    },
    {   'name':'hanconfirmresolve',
        'action':'playback',
        'transitions': {
            'handlermaincheck':['handlermaincheckevent'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'hancancelresolve',
        'action':'playback',
        'transitions': {
            'handlermaincheck':['handlermaincheckevent'],
            'stop':['sm_action_failure'],
        },
    },

    {   'name':'handlerwaitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'callbeneficiary':['sm_action_success'],
        },
    },
    {   'name':'callbeneficiary',
        'action':'originate',
        'transitions': {
            'beneficiaryanswered':['sm_action_success'],
            'stop':['caller_hung_up'],
            'nobenavailable':['beneficiary_hung_up'],
        },
    },
    {   'name':'beneficiaryanswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'nobenavailable',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
        },
    },
    { 'name':'handlerforwardgrievs',
         'action':'play_and_get_digits',
         'transitions': {
            'stop':['sm_action_failure'],
            'handlerrecordpmprompt':['sm_action_success'],
            'thankyou':['sm_get_digits_no_digits'],
        }
    },
    {    'name':'handlerrecordpmprompt',
         'action':'playback',
         'transitions': {
            'stop':['sm_action_failure'],
            'handlerrecordpm':['sm_action_success'],
        }
    },
    {    'name':'handlerrecordpm',
         'action':'record',
         'transitions': {
            'stop':['sm_action_failure'],
            'forwardthankyou':['sm_action_success'],
        }
    },
    {    'name':'forwardthankyou',
         'action':'playback',
         'transitions': {
            'stop':['sm_action_failure'],
            'handlermaincheck':['sm_action_success'],
        }
    },
    {   'name':'nothandlerprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermainbucket':['handlermaincheckevent'],
            'noupdatebyhandleronassigngrievs':['noupdatebyhandleronassigngrievsevent'],
            'unresolvedgrievancesduringvalidation':['unresolvedgrievancesduringvalidationevent'],
        },
    },
    {   'name':'quitgrievanceprompt',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermaincheck':['sm_get_digits_no_digits', 'sm_action_success'],
            'thankyou':['not_a_valid_reason_choice']
            #'handlerpermissioncheck':['sm_action_success'],
        },
    },
    {   'name':'handlerinputrefid',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlervalidactivegrievanceplayinstruction':['valid_active_grievance'],
            'nonassignedtocurrenthandlergrievanceprompt':['non_assigned_grievance_to_you', 'non_assigned_grievance_to_anyone', 'valid_closed_grievance','invalid_grievance'],
            # 'handlerinvalidgrievance':['invalid_grievance'],
            # 'handlervalidactivegrievanceplayinstruction':['valid_active_grievance'],
            # 'handlervalidclosedgrievanceplayinstruction':['valid_closed_grievance'],
            # 'nonassignedtohandlergrievanceprompt':['non_assigned_grievance_to_anyone'],
            # 'nonassignedtocurrenthandlergrievanceprompt':['non_assigned_grievance_to_you'],
        },
    },
    {   'name':'nonassignedtocurrenthandlergrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermaincheck':['sm_action_success'],
        },
    },
    {   'name':'handlervalidactivegrievanceplayinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermaincheck':['sm_action_success'],
        },
    },
    {   'name':'handlervalidclosedgrievanceplayinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermaincheck':['sm_action_success'],
        },
    },
    {   'name':'handlerinvalidgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerinputrefid':['sm_action_success'],
        },
    },
    {   'name':'handlervalidactivegrievanceplay',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'lastupdateonhaninputrefid':['lastupdatebyben_exists'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
            'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
            'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
            'checkifhandlerresolvedgrievance':['give_update'],
            'handlerinputrefid':['random_access_grievance'],
            'handlerwaitprompt':['call_beneficiary'],
            'quitgrievanceprompt':['quit_grievance'],
            'handlerforwardgrievs':['forward_current_grievance'],
            },
        'dtmf': {
            2:'give_update',
            3:'quit_grievance',
            5:'forward_current_grievance',
            6:'random_access_grievance',
            9:'call_beneficiary',
            }
    },
    {   'name':'lastupdateonhaninputrefid',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
            'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
            'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
        },
    },
    {   'name':'handlervalidclosedgrievanceplay',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermaincheck':['handlermaincheckevent','handlermaincheckevent' ],
            'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
            'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
            'handlerinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            6:'random_access_grievance',
        }
    },
    {   'name':'checkifhandlerresolvedgrievance',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerrecordupdateprompt':['active_grievance'],
            'handlerresolvedgrievanceprompt':['resolved_grievance'],
        },
    },
    {   'name':'handlerresolvedgrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
            'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
            'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
        },
    },
        {   'name':'unassignedgrievstohandlerinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievstohandlercheck':['sm_action_success'],
            'handlerinputrefid':['random_access_grievance'],
            'thankyou':['skip_bucket'],
        },
        'dtmf': {
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'unassignedgrievstohandlercheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievstohandler':['unassigned_grievs_han_exist'],
            'nounassignedgrievstohandlerprompt':['unassigned_grievs_han_no_exist'],
        },
    },
    {   'name':'nounassignedgrievstohandlerprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
            'handlerinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            6:'random_access_grievance',
        }
    },
    {   'name':'unassignedgrievstohandler',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievstohandler':['play_next_grievance', 'play_previous_grievance','sm_action_success', 'han_griev_available'],
            'thankyou':['skip_bucket','han_no_griev_available'],
            'selfassigngreviance':['self_assign'],
            'handlerinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'self_assign',
            4:'play_previous_grievance',
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'nogrievtoplayprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
        },
    },
    {   'name':'selfassigngreviance',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'selfassigngrevianceprompt':['sm_action_success'],
        },
    },
    {   'name':'selfassigngrevianceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
            'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
            'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
        },
    },
    {   'name':'posthandlerupdate',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermainbucket':['ben_update_griev_available'],
            'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandleronassigngrievsevent', 'noupdatebyhandleronassignedgrievs', 'noupdate_byhandleronassignedgrievs'],
            'unresolvedgrievancesduringvalidation':['han_pending_validation_grievance'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermainbucketinstructions':['no_han_pending_validation_grievance'],
            #'noupdatebyhandleronassigngrievsinstruction':['no_ben_update_griev_available'],
            'unassignedgrievstohandlerinstruction':['no_noupdatebyhandleronassignedgrievs','no_ben_update_griev_available'],
            'thankyou':['thank_you'],
            ##'handlerpermissioncheck':['sm_action_success'],
        },
    },
    {   'name':'handlerpermissioncheck',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerassgntoinprogressenabled':['han_assgntoinprogress_enabled'],
            'handlerassgntoinprogressdisabled':['han_assgntoinprogress_disabled'],
            'hanconfirmresolve':['han_inprogresstomarkedresolved_enabled'],
            'hancancelresolve':['han_inprogresstomarkedresolved_disabled'],
        },
    },
    {   'name':'handlerassgntoinprogressenabled',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermainbucket':['ben_update_griev_available'],
            #'noupdatebyhandleronassigngrievsinstruction':['no_ben_update_griev_available'],
            'unassignedgrievstohandlerinstruction':['unassigned_grievs_tohan','no_ben_update_griev_available']
        },
    },
    {   'name':'handlerassgntoinprogressdisabled',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermainbucket':['ben_update_griev_available'],
            #'noupdatebyhandleronassigngrievsinstruction':['no_ben_update_griev_available'],
            'unassignedgrievstohandlerinstruction':['unassigned_grievs_tohan','no_ben_update_griev_available']
        },
    },
    
    {   'name':'nonassignedtohandlergrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
            'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
            'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
            'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
            'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
            'selfassigngreviance':['self_assign'],
            'handlerinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            2:'self_assign',
            6:'random_access_grievance',
        }
    },

    # {   'name':'nonassignedtocurrenthandlergrievanceprompt',
    #     'action':'playback',
    #     'transitions': {
    #         'stop':['sm_action_failure'],
    #         'unresolvedgrievancescheck':['unresolvedgrievancesduringvalidationevent'],
    #         'handlermaincheck':['handlermaincheckevent', 'handlermaincheckevent'],
    #         'noupdatebyhandleronassigngrievsinstruction':['noupdatebyhandleronassigngrievsinstructionevent'],
    #         'noupdatebyhandlerassignedgrievscheck':['noupdatebyhandlerassignedgrievscheckevent', 'noupdatebyhandleronassigngrievsevent'],
    #         'unassignedgrievstohandlerinstruction':['unassignedgrievstohandlerinstructionevent'],
    #         'thankyou':['thankyouevent'],
    #         'unassignedgrievstohandlercheck':['unassignedgrievstohandlerevent'],
    #     }
    # },
    {   'name':'noupdatebyhandleronassigngrievsinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdatebyhandlerassignedgrievscheck':['sm_action_success'],
            'handlerinputrefid':['random_access_grievance'],
            'unassignedgrievstohandlerinstruction':['skip_bucket'],
        },
        'dtmf': {
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'noupdatebyhandlerassignedgrievscheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdatebyhandleronassigngrievs':['no_update_assigned_grievs_han_exist'],
            'nonoupdatebyhandlerassignedgrievsprompt':['no_update_assigned_grievs_han_no_exist'],
        },
    },
    {   'name':'nonoupdatebyhandlerassignedgrievsprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievstohandlerinstruction':['sm_action_success'],
            'handlerinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            6:'random_access_grievance',
        }
    },
    {   'name':'noupdatebyhandleronassigngrievs',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdatebyhandleronassigngrievs':['play_next_grievance', 'play_previous_grievance', 'sm_action_success', 'noupdate_byhandleronassignedgrievs'],
            'handlerrecordupdateprompt':['give_update'],
            'unassignedgrievstohandlerinstruction':['no_noupdatebyhandleronassignedgrievs', 'skip_bucket'],
            'thankyou':['thank_you'],
            'handlerwaitprompt':['call_beneficiary'],
            'quitgrievanceprompt':['quit_grievance'],
            'handlerforwardgrievs':['forward_current_grievance'],
            'handlerinputrefid':['random_access_grievance'],
            'hanmarkresolution':['mark_greviance_resolved'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'give_update',
            3:'quit_grievance',
            4:'play_previous_grievance',
            5:'forward_current_grievance',
            6:'random_access_grievance',
            9:'call_beneficiary',
            '*':'mark_greviance_resolved',
            0:'skip_bucket',
        }
    },
    {   'name':'handlerupdatesavailablegrievs',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerupdatesavailablegrievs':['play_next_grievance', 'play_previous_grievance', 'han_update_comment', 'han_update_griev_available'],
            'updategrievplaypostprompt':['sm_action_success', 'play_comment'],
            'beneficiaryrecordupdateprompt':['give_update'],
            'noupdateassignedgrievsinstruction':['no_han_update_griev_available', 'skip_bucket'],
            'beneficiarywaitprompt':['call_handler'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'give_update',
            3:'record_grievance',
            4:'play_previous_grievance',
            6:'random_access_grievance',
            9:'call_handler',
            0:'skip_bucket',
        }
    },
    {   'name':'beneficiaryrecordupdateprompt',
        'action':'playback',
        'transitions': {
            'beneficiaryrecordupdate':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'beneficiaryrecordupdate',
        'action':'record',
        'transitions': {
            'postbeneficiaryupdate':['sm_action_success'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'postbeneficiaryupdate',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerupdatesavailablegrievs':['han_update_griev_available'],
            'noupdateassignedgrievscheck':['no_han_update_griev_available'],
            'unassignedgrievs':['unassigned_grievs', 'ben_unassigned_griev_available'],
            'thankyou':['thankyou', 'ben_no_unassigned_griev_available'],
        },
    },
    {   'name':'recordgrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordgrievance':['sm_action_success'],
        },
    },
    {   'name':'recordgrievance',
        'action':'record',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordgrievancethankyou':['sm_action_success'],
            'grievancerecorderror':['grievance_record_error'],
        },
    },
    {   'name':'grievancerecorderror',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordgrievanceprompt':['sm_action_success'],
        },
    },
    {   'name':'recordgrievancethankyou',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'hanupdategrievscheck':['hanupdategrievscheckevent'],
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstructionevent'],
            'handlerupdatesavailablegrievs':['handlerupdatesavailablegrievsevent'],
            'noupdateassignedgrievscheck':['noupdateassignedgrievscheckevent'],
            'unassignedgrievsinstruction':['unassignedgrievsinstructionevent'],
            'noupdateassignedgrievs':['noupdateassignedgrievsevent'],
            'unassignedgrievscheck':['unassignedgrievscheckevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievs':['unassignedgrievsevent'],
        },
    },
        {   'name':'hanvalidateinstructions',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancesduringvalidation':['sm_action_success'],
        },
    },
    #NOT USED
    {   'name':'unresolvedgrievancesduringvalidation', 
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'unresolvedgrievancesduringvalidation':['play_next_grievance', 'play_previous_grievance', 'han_pending_validation_grievance', 'sm_action_success'],
            'handlerrecordupdateprompt':['give_update'],
            'handlermainbucketinstructions':['no_han_pending_validation_grievance', 'skip_bucket'],
            'handlerwaitprompt':['call_beneficiary'],
            'quitgrievanceprompt':['quit_grievance'],
            'handlerforwardgrievs':['forward_grievance'],
            'handlerinputrefid':['random_access_grievance'],
            'hanmarkresolution':['mark_greviance_resolved'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'give_update',
            3:'quit_grievance',
            4:'play_previous_grievance',
            5:'forward_grievance',
            6:'random_access_grievance',
            9:'call_beneficiary',
            '*':'mark_greviance_resolved',
            0:'skip_bucket',
        }
    },
    {   'name':'unresolvedgrievancescheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'nounresolvedgrievancesprompt':['no_han_pending_validation_grievance'],
            'unresolvedgrievancesduringvalidation':['han_pending_validation_grievance'],
        },
    },
    {   'name':'nounresolvedgrievancesprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlermainbucketinstructions':['sm_action_success'],
        },
    },
    {   'name':'hanupdateinstructions',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'hanupdategrievscheck':['sm_action_success'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
            'noupdateassignedgrievsinstruction':['skip_bucket'],
        },
        'dtmf': {
            3:'record_grievance',
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'hanupdategrievscheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerupdatesavailablegrievs':['han_update_grievs_exist', 'han_update_comment'],
            'nohanupdategrievsprompt':['han_update_grievs_no_exist']
        },
    },
    {   'name':'nohanupdategrievsprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdateassignedgrievsinstruction':['sm_action_success'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            3:'record_grievance',
            6:'random_access_grievance',
        }
    },
    {   'name':'updategrievplaypostprompt',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'grievupdateplay':['sm_action_success'],
        },
    },
    {   'name':'updategrievplaypostprompt',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'grievupdateplay':['sm_action_success'],
        },
    },
    {   'name':'grievupdateplay',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'hanupdategrievscheck':['ben_update_grievs'],
            'handlermaincheck':['han_update_grievs']
        },
    },
    {   'name':'benvalidateinstructions',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'benvalidateinvalidate':['sm_action_success'],
        },
    },
    {   'name':'benvalidateinvalidate',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'lastupdatebyhandlerprompt':['sm_action_success'],
        },
    },
    {   'name':'lastupdatebyhandlerprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'lastupdatebyhandler':['sm_action_success'],
        },
    },
    {   'name':'lastupdatebyhandler',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'beninvalidateresolution':['invalidate_resolution'],
            'benvalidateresolution':['validate_resolution'],
            'benvalidateinvalidate':['next_ben_pending_validation_grievance'],
            'hanupdateinstructions':['no_ben_pending_validation_grievance'],
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstruction'],
        },
        'dtmf': {
            2:'invalidate_resolution',
            '*':'validate_resolution'
        }
    },
    {   'name':'beninvalidateresolution',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'benconfirminvalidate':['confirm'],
            'bencancelinvalidate':['cancel'],
            'thankyou':['sm_get_digits_no_digits'],
        },
        'dtmf': {
            1:'confirm',
            2:'cancel'
        }
    },
    {   'name':'benvalidateresolution',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'benconfirmvalidate':['confirm'],
            'bencancelvalidate':['cancel'],
            'thankyou':['sm_get_digits_no_digits'],
        },
        'dtmf': {
            1:'confirm',
            2:'cancel'
        }
    },
    {   'name':'benconfirminvalidate',
        'action':'playback',
        'transitions': {
            'benvalidateinvalidate':['nextpendingbenvalidationgrievs'],
            'hanupdateinstructions':['hanupdateinstructionsevent'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'benconfirmvalidate',
        'action':'playback',
        'transitions': {
            'benvalidateinvalidate':['nextpendingbenvalidationgrievs'],
            'hanupdateinstructions':['hanupdateinstructionsevent'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'bencancelinvalidate',
        'action':'none',
        'transitions': {
            'benvalidateinvalidate':['nextpendingbenvalidationgrievs'],
            'hanupdateinstructions':['hanupdateinstructionsevent'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'bencancelvalidate',
        'action':'none',
        'transitions': {
            'benvalidateinvalidate':['nextpendingbenvalidationgrievs'],
            'hanupdateinstructions':['hanupdateinstructionsevent'],
            'stop':['sm_action_failure'],
        },
    },
    {   'name':'beneficiaryinputrefid',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinvalidgrievance':['invalid_grievance'],
            'beneficiaryvalidactivegrievanceplayinstruction':['valid_active_grievance'],
            'beneficiaryvalidclosedgrievanceplayinstruction':['valid_closed_grievance'],
            'thankyou':['sm_get_digits_no_digits'],
        },
    },
    {   'name':'beneficiaryvalidactivegrievanceplayinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryvalidactivegrievanceplay':['sm_action_success'],
        },
    },
    {   'name':'beneficiaryvalidclosedgrievanceplayinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryvalidclosedgrievanceplay':['sm_action_success'],
        },
    },
    {   'name':'beneficiaryinvalidgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryinputrefid':['sm_action_success'],
        },
    },
    {   'name':'beneficiaryvalidactivegrievanceplay',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'lastupdateonbeninputrefid':['lastupdatebyhan_exists'],
            'hanupdategrievscheck':['hanupdategrievscheckevent'],
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstructionevent'],
            'handlerupdatesavailablegrievs':['handlerupdatesavailablegrievsevent'],
            'noupdateassignedgrievscheck':['noupdateassignedgrievscheckevent'],
            'unassignedgrievsinstruction':['unassignedgrievsinstructionevent'],
            'noupdateassignedgrievs':['noupdateassignedgrievsevent'],
            'unassignedgrievscheck':['unassignedgrievscheckevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievs':['unassignedgrievsevent'],
            'checkifresolvedgrievance':['give_update'],
            'beneficiaryinputrefid':['random_access_grievance'],
            'beneficiarywaitprompt':['call_handler'],
            'benvalidateresolution':['validate_resolution'],
        },
        'dtmf': {
            2:'give_update',
            6:'random_access_grievance',
            9:'call_handler',
            '*':'validate_resolution',
        }
    },
    {   'name':'lastupdateonbeninputrefid',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'hanupdategrievscheck':['hanupdategrievscheckevent'],
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstructionevent'],
            'handlerupdatesavailablegrievs':['handlerupdatesavailablegrievsevent'],
            'noupdateassignedgrievscheck':['noupdateassignedgrievscheckevent'],
            'unassignedgrievsinstruction':['unassignedgrievsinstructionevent'],
            'noupdateassignedgrievs':['noupdateassignedgrievsevent'],
            'unassignedgrievscheck':['unassignedgrievscheckevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievs':['unassignedgrievsevent'],
        },
    },
    {   'name':'beneficiaryvalidclosedgrievanceplay',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'hanupdategrievscheck':['hanupdategrievscheckevent'],
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstructionevent'],
            'handlerupdatesavailablegrievs':['handlerupdatesavailablegrievsevent'],
            'noupdateassignedgrievscheck':['noupdateassignedgrievscheckevent'],
            'unassignedgrievsinstruction':['unassignedgrievsinstructionevent'],
            'noupdateassignedgrievs':['noupdateassignedgrievsevent'],
            'unassignedgrievscheck':['unassignedgrievscheckevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievs':['unassignedgrievsevent'],
            'reopenedgrievanceprompt':['reopen_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            1:'reopen_grievance',
            6:'random_access_grievance',
        }
    },
    {   'name':'reopenedgrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'hanupdategrievscheck':['hanupdategrievscheckevent'],
            'noupdateassignedgrievsinstruction':['noupdateassignedgrievsinstructionevent'],
            'handlerupdatesavailablegrievs':['handlerupdatesavailablegrievsevent'],
            'noupdateassignedgrievscheck':['noupdateassignedgrievscheckevent'],
            'unassignedgrievsinstruction':['unassignedgrievsinstructionevent'],
            'noupdateassignedgrievs':['noupdateassignedgrievsevent'],
            'unassignedgrievscheck':['unassignedgrievscheckevent'],
            'thankyou':['thankyouevent'],
            'unassignedgrievs':['unassignedgrievsevent'],
        }
    },
    {   'name':'checkifresolvedgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'beneficiaryrecordupdateprompt':['active_grievance'],
            'resolvedgrievanceprompt':['resolved_grievance'],
        }
    },
    {   'name':'resolvedgrievanceprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'handlerupdatesavailablegrievs':['sm_action_success'],
        },
    },
    {   'name':'beneficiarywaitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'callhandler':['sm_action_success'],
        },
    },
    {   'name':'callhandler',
        'action':'originate',
        'transitions': {
            'handleranswered':['sm_action_success'],
            'stop':['caller_hung_up'],
            'thankyou':['handler_hung_up'],
        },
    },
    {   'name':'handleranswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'recordcall',
        'action':'record_call',
        'transitions': {
            'stop':['sm_action_failure'],
            'bridgecalls':['sm_action_success'],
        },
    },
    {   'name':'bridgecalls',
        'action':'bridge',
        'transitions': {
            'stop':['sm_action_failure'],
           # 'thankyou':['sm_action_success'],
        },
    },
    {   'name':'noupdateassignedgrievsinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdateassignedgrievscheck':['sm_action_success'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
            'unassignedgrievsinstruction':['skip_bucket'],
        },
        'dtmf': {
            3:'record_grievance',
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'noupdateassignedgrievscheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdateassignedgrievs':['no_update_assigned_grievs_ben_exist'],
            'nonoupdateassignedgrievsprompt':['no_update_assigned_grievs_ben_no_exist'],
        },
    },
    {   'name':'nonoupdateassignedgrievsprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievsinstruction':['unassigned_grievs_ben_exist'],
            'thankyou':['thankyou'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            3:'record_grievance',
            6:'random_access_grievance',
        }
    },
    {   'name':'noupdateassignedgrievs',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'noupdateassignedgrievs':['play_next_grievance', 'play_previous_grievance', 'sm_action_success', 'han_update_griev_available'],
            'beneficiaryrecordupdateprompt':['give_update'],
            'unassignedgrievsinstruction':['no_han_update_griev_available', 'skip_bucket'],
            'thankyou':['thankyou'],
            'beneficiarywaitprompt':['call_handler'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'give_update',
            3:'record_grievance',
            4:'play_previous_grievance',
            6:'random_access_grievance',
            9:'call_handler',
            0:'skip_bucket',
        }
    },
    {   'name':'unassignedgrievsinstruction',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievscheck':['sm_action_success'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
            'thankyou':['skip_bucket'],
        },
        'dtmf': {
            3:'record_grievance',
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'unassignedgrievscheck',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievs':['unassigned_grievs_ben_exist'],
            'nounassignedgrievsprompt':['unassigned_grievs_ben_no_exist'],
        },
    },
    {   'name':'nounassignedgrievsprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            3:'record_grievance',
            6:'random_access_grievance',
        }
    },
    {   'name':'unassignedgrievs',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'unassignedgrievs':['play_next_grievance', 'play_previous_grievance', 'sm_action_success','ben_unassigned_griev_available'],
            'beneficiaryrecordupdateprompt':['give_update'],
            'thankyou':['skip_bucket','ben_no_unassigned_griev_available'],
            'beneficiarywaitprompt':['call_handler'],
            'recordgrievanceprompt':['record_grievance'],
            'beneficiaryinputrefid':['random_access_grievance'],
        },
        'dtmf': {
            1:'play_next_grievance',
            2:'give_update',
            3:'record_grievance',
            4:'play_previous_grievance',
            6:'random_access_grievance',
            0:'skip_bucket',
        }
    },
    {   'name':'nogrievtoplayprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success'],
        },
    },
    {   'name':'thankyou',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {},
    },
]
