from app_manager.models import App_instance, Application
from case_manager.models import Grievance, Grievance_state, Grievance_item_mapping, Handler
from mnews.models import News, News_state
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db.models import Q
import urllib

from log import get_request_logger

logger = get_request_logger()
NUM_NEWS_PER_PAGE = 10


def show_news(request, ai_id=None, handler_id=None, state_id=None, tag=None, format_id=None, qualifier_id=None):
    query_set = Q()
    grievance_news=[]
    case_manager_id = Application.objects.get(name='case_manager').id
    handler = Handler.objects.filter(ai_id=ai_id)
    ai_list = App_instance.objects.filter(app_id=case_manager_id)
    state_list = Grievance_state.objects.all()


    #making query set with ai_id, handler_id, state_id
    if ai_id not in (None, 0, '0'):
        ai = App_instance.objects.get(id=int(ai_id))
        query_set.add(Q(ai_id=ai_id), Q.AND)
    else:
        ai = None
    if handler_id not in (None, 0, '0'):
        query_set.add(Q(handler_id=handler_id), Q.AND)
    else:
        handler_id=0
    if state_id not in (None, 0, '0'):
        query_set.add(Q(current_state_id=state_id), Q.AND)
    else:
        state_id=0

    grievances = Grievance.objects.filter(query_set).order_by('-creation_datetime')

    try:
        for grievance in grievances:
            item_list = Grievance_item_mapping.objects.filter(grievance_id=grievance.id, is_parent=True)
            news=item_list[0].item

            news.grievance=property(lambda  self: '')
            news.grievance= grievance

            if tag:
                tag = urllib.unquote_plus(str(tag))
                if tag in news.tags.split(','):
                    grievance_news.append(news)
            elif format_id:
                if news.format and news.format.id==int(format_id):
                    grievance_news.append(news)
            elif qualifier_id:
               if news.qualifier and news.qualifier.id == int(qualifier_id):
                   grievance_news.append(news)
            else:
                grievance_news.append(news)

    except Exception as e:
        print ("Rest error: {0}".format(e))

    paginator = Paginator(grievance_news, NUM_NEWS_PER_PAGE)
    page = request.GET.get('page')

    if page:
        try:
            news_page = paginator.page(page)

        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            news_page = paginator.page(1)

        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            news_page = paginator.page(paginator.num_pages)
    else:
        news_page = paginator.page(1)

    return render_to_response('case_manager/case_manager.html',
                              {'pub_news': news_page, 'ai': ai, 'ai_list': ai_list, 'handler_list': handler, 'state_list': state_list,
                               'handler_id': int(handler_id), 'state_id':int(state_id)
                              },
                              context_instance=RequestContext(request)
                              )