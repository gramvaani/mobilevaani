from case_manager.models import Grievance, Grievance_item_mapping, Grievance_state, Activity_log, Handler, \
    Action_setting, Profile, Properties, ACTIONEE_CHOICES, SmsEvents, Sms_log

from django.db.models import Q

from vapp.sms.tasks import SMSTask

from vapp.mnews.models import Event_sms_template

from vapp.log import get_request_logger

from datetime import datetime

import json

logger = get_request_logger()

DEFAULT_GRIEVANCE_ASSIGNED_STATE = 'ASSIGNED'


def smsOnAssignment(grievance_id):
    try:
        currgrievs = Grievance.objects.get(id=grievance_id)
        logger.info('SMS Trigger for grievance Id :: ' + str(grievance_id))
        properties = Properties.objects.get(ai=currgrievs.ai)
        params_ben = {}
        params_han = {}
        params_ben['reference_id'] = currgrievs.reference_id
        params_ben['handler_name'] = currgrievs.handler.name
        params_ben['handler_number'] = currgrievs.handler.number
        params_han['reference_id'] = currgrievs.reference_id
        params_han['number'] = currgrievs.beneficiary
        template_ben = Event_sms_template.objects.get(template__ai=properties.mnews_ai,
                                                      event=SmsEvents.GRIEVANCE_ASSIGNED_TO_BENEFICIARY).template
        template_han = Event_sms_template.objects.get(template__ai=properties.mnews_ai,
                                                      event=SmsEvents.GRIEVANCE_ASSIGNED_TO_HANDLER).template
        message_ben = template_ben.process_template(params_ben)
        message_han = template_han.process_template(params_han)
        sms_to_ben = SMSTask.create_send_msg(properties.mnews_ai.id, message_ben, currgrievs.beneficiary)
        sms_to_han = SMSTask.create_send_msg(properties.mnews_ai.id, message_han, currgrievs.handler.number)
        Sms_log.objects.create(grievance=currgrievs, sms_message=sms_to_ben,
                               recipient_type=ACTIONEE_CHOICES.BENEFICIARY)
        Sms_log.objects.create(grievance=currgrievs, sms_message=sms_to_han, recipient_type=ACTIONEE_CHOICES.HANDLER)
    except Exception, e:
        logger.exception("Error in smsOnAssignment %s" % str(e))


def update_grievance_log(grievance_id, source, actionee_type, actionee_id=None, grievance_state_id=None,
                         handler_id=None, reason_id=None):
    grievance = Grievance.objects.get(id=grievance_id)
    activity_log = Activity_log()
    activity_log.grievance = grievance
    activity_log.source = source
    logger.info('actionee_type ::' + str(actionee_type))
    activity_log.actionee_type = actionee_type
    activity_log.actionee_id = actionee_id
    state = Grievance_state.objects.get(id=grievance_state_id)
    if handler_id:
        activity_log.handler_id = handler_id
    else:
        activity_log.handler_id = grievance.handler_id

    logger.info('SMS Log 1')
    if handler_id and str(state.state) not in ('HALTED_PROGRESS'):
        logger.info('SMS Log 2')
        smsOnAssignment(grievance_id)
    activity_log.prev_state = grievance.current_state
    activity_log.next_state = state
    activity_log.grievance_quit_reason_id = reason_id
    logger.info("R3:"+str(reason_id))
    logger.info("Activity_log ;;; " + str(state) + "act:::" + str(grievance))
    activity_log.save()


def update_grievance_state_handler(grievance_id, state_id, actionee, source, user_id=None, assigned_handler_id=None, reason_id=None):
    if Grievance_state.objects.get(id=state_id).state == DEFAULT_GRIEVANCE_ASSIGNED_STATE:
        grievance_handler = Grievance.objects.filter(id=grievance_id).update(
            handler=Handler.objects.get(id=assigned_handler_id))
    grievance = Grievance.objects.get(id=grievance_id)
    profile = Profile.objects.filter(ai=grievance.ai)
    action_settings = Action_setting.objects.filter(profile=profile, prev_state=grievance.current_state)
    data_set = action_settings.filter(Q(actionee__isnull=True) | Q(actionee=actionee))
    auto_state_changed = False
    logger.info("action_settings =" + str(action_settings))
    logger.info("data_set =" + str(data_set))
    logger.info("profile =" + str(profile))

    if len(data_set) > 0:
        for data in data_set:
            logger.info("data.next_state.id ========" + str(data.next_state.id))
            if data.next_state.id == int(state_id):
                action_settings = Action_setting.objects.filter(profile=profile, prev_state__id=state_id)
                logger.info("data.next_state.id =" + str(data.next_state.id))
                logger.info("action_settings =" + str(action_settings))
                logger.info("R1:"+str(reason_id))
                handle_auto_state_change(grievance_id, state_id, source, actionee, user_id, action_settings,
                                         assigned_handler_id, reason_id)
                auto_state_changed = True
    if str(grievance.current_state) == 'ASSIGNED':
        updates = Grievance_item_mapping.objects.filter(grievance=grievance, is_parent=False,
                                                        contributor_type='BENEFICIARY').exists()
        if updates:
            grievance.current_state = Grievance_state.objects.get(state='INPROGRESS')
    logger.info("update_grievance_state_handler")

    return auto_state_changed


def handle_auto_state_change(grievance_id, state_id, source, actionee, user_id, action_settings,
                             assigned_handler_id=None, reason_id=None):
    data_set = filter(lambda a: a['actionee'] == ACTIONEE_CHOICES.AUTO, action_settings)
    grievance = Grievance.objects.get(id=grievance_id)
    logger.info("R2:"+str(reason_id))
    if len(data_set) == 1:
        update_grievance_log(grievance_id, source, actionee, user_id, data_set[0].next_state.id, assigned_handler_id, reason_id)
        grievance.current_state = data_set[0].next_state
    else:
        update_grievance_log(grievance_id, source, actionee, user_id, state_id, assigned_handler_id, reason_id)
        grievance.current_state = Grievance_state.objects.get(id=state_id)

    if str(grievance.current_state) == 'HALTED_PROGRESS' or str(grievance.current_state) == 'UNASSIGNED':
        grievance.handler_id = None
    logger.info(str(grievance.current_state) + "------")
    logger.info("handle_auto_state_change")
    grievance.last_modified_datetime = datetime.now()	
    grievance.save()

def populate_item_misc_field(grievance, is_update, is_conversation):
    news_misc = {}
    news_misc['gr_id'] = grievance.id
    news_misc['gr_update'] = int(is_update)
    news_misc['gr_conversation'] = is_conversation
    news_misc['gr_state'] = grievance.current_state.state
    news_misc['gr_handler_num'] = grievance.handler.number if grievance.handler else None
    return news_misc


def is_case_manager_mnews_instance(mnews_ai):
    return Properties.objects.filter(mnews_ai=mnews_ai).exists()


def create_grievance(news):
    logger.info("INTO CREATE GRIEVANCE:"+str(news.id))  
    if not news.cm_properties:
            case_manager_properties = Properties.objects.filter(mnews_ai=news.ai)
            if case_manager_properties.exists() and not Grievance_item_mapping.objects.filter(item=news).exists():
                grievance = Grievance(ai=case_manager_properties[0].ai, current_state = Grievance_state.objects.get(state='UNMODERATED'), beneficiary=news.callerid)
                grievance.save()
                #populate cm_properties for the news
                misc = populate_item_misc_field(grievance, False, False)
                news.cm_properties = json.dumps(misc)
                news.save()
                Grievance_item_mapping(grievance=grievance, item=news, is_parent=True, creation_time=grievance.creation_datetime, contributor=news.callerid, contributor_type='BENEFICIARY', is_conversation=False).save()
