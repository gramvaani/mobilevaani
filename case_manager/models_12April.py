from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import pre_save
from datetime import datetime
from vapp.log import get_request_logger
from app_manager.models import App_instance, CalleridField
from customer.models import Project
from location.models import Location
from vapp.mnews.models import News
from schedule.models import Schedule
from sms.models import SMS_message, SMS_template

from vapp.utils import Moderation_source

logger = get_request_logger()

CALLERID_LENGTH = 20

class Handler(models.Model):
    RECORDER = "__RECORDER__"

    ai = models.ForeignKey(App_instance, related_name='ai_handler')
    number = CalleridField(max_length=CALLERID_LENGTH)
    name = models.CharField(max_length=32)
    priority = models.IntegerField()
    location = models.ForeignKey(Location, null=True, blank=True, related_name='location_handler')

    def __unicode__(self):
        if self.location:
            return '{0}_{1}_{2}_{3}_{4}'.format(self.ai.id, self.number, self.name, self.priority, self.location.id)
        else:
            return '{0}_{1}_{2}_{3}'.format(self.ai.id, self.number, self.name, self.priority)


class Grievance_state(models.Model):
    state = models.CharField(max_length=20, null=True)

    def __unicode__(self):
        return self.state

class Grievance(models.Model):
    ai = models.ForeignKey(App_instance, related_name='app_instance_grievance')
    reference_id = models.PositiveIntegerField()
    current_state = models.ForeignKey(Grievance_state, null=False)
    beneficiary = CalleridField(max_length=CALLERID_LENGTH)
    handler = models.ForeignKey(Handler, null=True)
    creation_datetime = models.DateTimeField(auto_now_add=True)
    last_modified_datetime = models.DateTimeField(default=datetime.now)

def generate_unique_reference_num(ai):
    grievance_list = Grievance.objects.filter(ai = ai).values_list('reference_id', flat=True)
    grievance_id = 0
    if len(grievance_list) > 0:
        grievance_id = max(grievance_list)
    return (grievance_id + 1)

def grievance_pre_save_handler(sender, **kwargs):
    grievance = kwargs['instance']
    if not grievance:
        return
    if grievance.reference_id is None:
        reference_id = generate_unique_reference_num(grievance.ai)
        if reference_id:
            grievance.reference_id = reference_id
        else:
            logger.error('Unable to generate a unique reference id for the grievance: %s' % grievance.id)
            return

pre_save.connect(grievance_pre_save_handler, sender=Grievance, dispatch_uid='case_manager.grievance.pre_save')


class Profile(models.Model):
    name = models.CharField(max_length=20)
    ai = models.ManyToManyField(App_instance, null=True, blank=True, related_name='ai_profile')

    def __unicode__(self):
        return unicode(self.name)


# AUTO - auto state change without anyone's action, NULL - state change perm to all the handler ben mod.
class ACTIONEE_CHOICES(models.Model):
    HANDLER = 'HANDLER'
    BENEFICIARY = 'BENEFICIARY'
    MODERATOR = 'MODERATOR'
    HAN_MOD = 'HAN_MOD'
    AUTO = 'AUTO'


class Action_setting(models.Model):
    ACTIONEE_CHOICES = (
        (ACTIONEE_CHOICES.HANDLER, 'HANDLER'),
        (ACTIONEE_CHOICES.BENEFICIARY, 'BENEFICIARY'),
        (ACTIONEE_CHOICES.MODERATOR, 'MODERATOR'),
        (ACTIONEE_CHOICES.AUTO, 'AUTO'),
    )

    ACTION_CHOICES = (
        ('UPDATE', 'UPDATE'),
        ('CALL_HANDLER', 'CALL_HANDLER'),
        ('CALL_BENEFICIARY', 'CALL_BENEFICIARY'),
        ('REPLAY', 'REPLAY'),
        ('FLAG', 'FLAG'),
        ('INPUT', 'INPUT'),
        ('ASSIGN_HANDLER', 'ASSIGN_HANDLER'),
        ('RECORD_GRIEVANCE', 'RECORD_GRIEVANCE'),
    )

    profile = models.ForeignKey(Profile)
    prev_state = models.ForeignKey(Grievance_state, null=False, related_name='case_manager_prev_state')
    next_state = models.ForeignKey(Grievance_state, null=False, related_name='case_manager_next_state')
    actionee = models.CharField(max_length=20, choices=ACTIONEE_CHOICES, null=True, blank=True)
    action = models.CharField(max_length=20, choices=ACTION_CHOICES, null=True, blank=True)

    def __unicode__(self):
        return unicode(self.profile.name) + '_' + unicode(self.prev_state) + '_' + \
               unicode(self.next_state) + '_' + unicode(self.actionee)

    def clean(self):
        if self.prev_state == self.next_state:
            raise ValidationError("Previous state and Next state cannot be same")
        super(Action_setting, self).clean()

    def __getitem__(self, item):
        return getattr(self, item)


class Grievance_item_mapping(models.Model):
	grievance = models.ForeignKey(Grievance)
	item = models.ForeignKey(News)
	is_parent = models.BooleanField(default=False)
	creation_time = models.DateTimeField(default=datetime.now)
	contributor = CalleridField(max_length=CALLERID_LENGTH)
	contributor_type = models.CharField(max_length=20, choices=Action_setting.ACTIONEE_CHOICES)
	is_conversation = models.BooleanField(default=False)


class Validation_type(models.Model):
	AUTO_VALIDATE = 'AUTO_VALIDATE'
	BEN_VALIDATE = 'BEN_VALIDATE'
	MOD_VALIDATE = 'MOD_VALIDATE'
	BEN_MOD_VALIDATE = 'BEN_MOD_VALIDATE'
	MOD_BEN_VALIDATE = 'MOD_BEN_VALIDATE'


class Properties(models.Model):
    VALIDATION_CHOICES = (
        (Validation_type.AUTO_VALIDATE, 'AUTO_VALIDATE'),
        (Validation_type.BEN_VALIDATE, 'BEN_VALIDATE'),
        (Validation_type.MOD_VALIDATE, 'MOD_VALIDATE'),
        (Validation_type.BEN_MOD_VALIDATE, 'BEN_MOD_VALIDATE'),
        (Validation_type.MOD_BEN_VALIDATE, 'MOD_BEN_VALIDATE'),
    )
    profile = models.ForeignKey(Profile)
    ai = models.ForeignKey(App_instance, null=False, blank=False, related_name = 'case_manager_ai')
    mnews_ai = models.ForeignKey(App_instance, null=False, blank=False, related_name = 'mnews_ai')
    ben_schedule = models.ForeignKey(Schedule, null = True, blank = True, verbose_name = "Beneficiary - Schedule", related_name = "ben_schedule_properties_set")
    han_schedule = models.ForeignKey(Schedule, null = True, blank = True, verbose_name = "Handler - Schedule", related_name = "han_schedule_properties_set")
    han_access_ans_mc = models.BooleanField(default = False, verbose_name = "Handler - Go to answering machine when call received outside schedule")
    ben_access_ans_mc = models.BooleanField(default = False, verbose_name = "Beneficiary - Go to answering machine when call received outside schedule")
    allow_ben_update_autopub = models.BooleanField(default=False, verbose_name = "Allow beneficiary updates to get autopublished")
    allow_han_update_autopub = models.BooleanField(default=False, verbose_name = "Allow handler updates to get autopublished")
    call_handler_on_update = models.BooleanField(default=False, verbose_name = "Enable call to handler on grievance update by beneficiary")
    call_beneficiary_on_update = models.BooleanField(default=False, verbose_name = "Enable call to beneficiary on grievance update by handler")
    validation_type = models.CharField(max_length=20, choices=VALIDATION_CHOICES, default='BEN_VALIDATE')
    pending_ben_validation_limit = models.PositiveSmallIntegerField(default = 5)
    pending_han_validation_limit = models.PositiveSmallIntegerField(default = 5)
    ben_unassigned_grievs_limit = models.PositiveSmallIntegerField(default = 5)
    han_unassigned_grievs_limit = models.PositiveSmallIntegerField(default = 5)
    no_update_assigned_grievs_ben_limit = models.PositiveSmallIntegerField(default = 5)
    no_update_assigned_grievs_han_limit = models.PositiveSmallIntegerField(default = 5)
    ben_updates_limit = models.PositiveSmallIntegerField(default = 5)
    han_updates_limit = models.PositiveSmallIntegerField(default = 5)

    def __unicode__(self):
        return unicode(self.profile.name) + " Properties"

        
class Sms_log(models.Model):
    SMS_RECIPIENT_CHOICES = (
        (ACTIONEE_CHOICES.HANDLER, 'HANDLER'),
        (ACTIONEE_CHOICES.BENEFICIARY, 'BENEFICIARY'),
        (ACTIONEE_CHOICES.MODERATOR, 'MODERATOR'),
    )
    grievance = models.ForeignKey(Grievance)
    sms_message = models.ForeignKey(SMS_message)
    recipient_type = models.CharField(max_length=20, choices=SMS_RECIPIENT_CHOICES)


class Grievance_quit_reason(models.Model):
    REASONS_AVAILABLE_TO_CHOICES = (
		(ACTIONEE_CHOICES.HANDLER, 'HANDLER'),
        (ACTIONEE_CHOICES.MODERATOR, 'MODERATOR'),
        (ACTIONEE_CHOICES.HAN_MOD, 'HAN_MOD'),
    )
    reason = models.CharField(max_length=20, null=True)
    available_to = models.CharField(max_length=20, choices=REASONS_AVAILABLE_TO_CHOICES)
    numeric_key = models.PositiveSmallIntegerField(null=True, blank=True, help_text='Handler and Quit reason DTMF(1-9) mapping, NULL for moderators')

    def __unicode__(self):
		return str(self.reason) + '_' + str(self.available_to) + str(self.numeric_key)

class Grievance_quit_reason_group(models.Model):
    quit_reason = models.ManyToManyField(Grievance_quit_reason, blank=True)

    def __unicode__(self):
        return '{0}'.format(self.id)

class Grievance_ai_quit_reason_group(models.Model):
    ai = models.ForeignKey(App_instance)
    group = models.ForeignKey(Grievance_quit_reason_group)

    def __unicode__(self):
        return '{0}_{1}'.format(self.ai,self.group)

class Activity_log(models.Model):
	MODERATION_SOURCES = (
        (Moderation_source.IVR, 'IVR'),
        (Moderation_source.WEB, 'WEB'),
    )
	ACTIONEE_CHOICES = (
        (ACTIONEE_CHOICES.HANDLER, 'HANDLER'),
        (ACTIONEE_CHOICES.BENEFICIARY, 'BENEFICIARY'),
        (ACTIONEE_CHOICES.MODERATOR, 'MODERATOR'),
        (ACTIONEE_CHOICES.AUTO, 'AUTO'),
    )
	grievance = models.ForeignKey(Grievance)
	prev_state = models.ForeignKey(Grievance_state, related_name='activity_log_prev_state', null=True)
	next_state = models.ForeignKey(Grievance_state, related_name='activity_log_next_state', null=True)
	timestamp = models.DateTimeField(default=datetime.now)
	source = models.CharField(max_length=5, null=True, choices=MODERATION_SOURCES)
	actionee_type = models.CharField(max_length=20, choices=ACTIONEE_CHOICES)
	actionee_id = models.PositiveIntegerField(blank=True, null=True) # NULL for beneficiary
	handler = models.ForeignKey(Handler, blank=True, null=True, related_name='activity_log_handler') # NULL for beneficiary
	grievance_quit_reason = models.ForeignKey(Grievance_quit_reason, null=True)


class SmsEvents:
    GRIEVANCE_FORWARDED = 'GRIEVANCE_FORWARDED'
    GRIEVANCE_ASSIGNED_TO_HANDLER = 'GRIEVANCE_ASSIGNED_TO_HANDLER'
    GRIEVANCE_ASSIGNED_TO_BENEFICIARY = 'GRIEVANCE_ASSIGNED_TO_BENEFICIARY'
    GRIEVANCE_UPDATE_BY_BENEFICIARY = 'GRIEVANCE_UPDATE_BY_BENEFICIARY'
    GRIEVANCE_UPDATE_BY_HANDLER = 'GRIEVANCE_UPDATE_BY_HANDLER'
