from vapp.log import get_request_logger
from tastypie.resources import ModelResource, fields
from tastypie.utils import trailing_slash
from django.conf.urls import url
from case_manager.models import Profile, Action_setting, Grievance_state, Grievance, Handler, Grievance_item_mapping, \
    ACTIONEE_CHOICES, Moderation_source, Grievance_ai_quit_reason_group, Grievance_quit_reason, \
    Properties
from location.api import LocationResource
from utils import convert_model_to_dict
from util import update_grievance_state_handler
import traceback
from mnews.api import NewsResource
from app_manager.api import AppInstanceResource
import sys
from tastypie.authentication import ApiKeyAuthentication
from django.forms.models import model_to_dict
import json
from local_settings import SERVER_ID
from vapp.settings import MAX_CASES
logger = get_request_logger()


class CaseManagerGrievancePropertiesResource(ModelResource):
    mnews_ai = fields.ForeignKey(AppInstanceResource, 'mnews_ai', full=True, null=True)

    class Meta:
        queryset = Properties.objects.all()
        resource_name = "casemanager/properties"
        allowed_methods = ['get']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_by_casemanager_ai%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_properties_by_casemanager_ai'), name="api_get_handler_by_ai")
        ]

    def get_properties_by_casemanager_ai(self, request, **kwargs):

        properties_resource = CaseManagerGrievancePropertiesResource()

        self.method_check(request, allowed=['get'])
        ai_id = request.REQUEST['ai_id']
        property = Properties.objects.filter(ai=ai_id)[0]

        response_bundle = properties_resource.build_bundle(obj=property, request=request)
        response_bundle = properties_resource.full_dehydrate(response_bundle)

        return self.create_response(request, {'result': response_bundle})


class GrievanceStateResource(ModelResource):
    class Meta:
        queryset = Grievance_state.objects.all()
        resource_name = "casemanager/grievance_state"
        allowed_methods = ['get']


class GrievanceItemResource(ModelResource):
    class Meta:
        queryset = Grievance_state.objects.all()
        resource_name = "casemanager/grievance_item"
        allowed_methods = ['get']


class CaseManagerHandlerResource(ModelResource):
    location = fields.ForeignKey(LocationResource, 'location', full=True, null=True)

    class Meta:
        queryset = Handler.objects.all()
        resource_name = "casemanager/handler"
        allowed_methods = ['get']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/getHandler%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_handler_by_ai'), name="api_get_handler_by_ai")
        ]

    def get_handler_by_ai(self, request, **kwargs):
        handler_bundle = []

        handler_resource = CaseManagerHandlerResource()

        self.method_check(request, allowed=['get'])
        ai_id = request.REQUEST['ai_id']
        handlers = Handler.objects.filter(ai=ai_id)
        for handler in handlers:
            response_bundle = handler_resource.build_bundle(obj=handler, request=request)
            response_bundle = handler_resource.full_dehydrate(response_bundle)
            handler_bundle.append(response_bundle)

        return self.create_response(request, {'result': handler_bundle})


class ModeratorProfileResource(ModelResource):
    class Meta:
        resource_name = "casemanager/moderator"
        allowed_methods = ['get', 'post']
        queryset = Action_setting.objects.all().order_by('id')

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_profile%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_profile_by_ai'), name="api_get_profile")
        ]

    def get_profile_by_ai(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        moderator_profile_bundle = []
        moderator_profile_resource = ModeratorProfileResource()
        try:
            ai_id = request.REQUEST['ai_id']
            actionee = request.REQUEST['actionee']
            profile = Profile.objects.get(ai=ai_id)
            transition_setting = Action_setting.objects.filter(profile=profile, actionee=actionee)
            for ts in transition_setting:
                response_bundle = moderator_profile_resource.build_bundle(obj=ts, request=request)
                response_bundle = moderator_profile_resource.full_dehydrate(response_bundle)
                moderator_profile_bundle.append(response_bundle)
        except Exception as e:
            logger.error("Rest error: {0}".format(e))
        return self.create_response(request, {'result': moderator_profile_bundle})

    def dehydrate(self, bundle):
        bundle.data['prev_state'] = bundle.obj.prev_state.state
        bundle.data['next_state'] = bundle.obj.next_state.state
        return bundle


class CaseManagerGrievanceResource(ModelResource):
    handler = fields.ForeignKey(CaseManagerHandlerResource, 'handler', full=True, null=True)
    item = fields.ForeignKey(NewsResource, 'item', full=True)

    class Meta:
        resource_name = "casemanager/grievance"
        allowed_methods = ['get', 'post']
        queryset = Grievance.objects.all().order_by('id')
        #authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/getGrievance%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_grievance_by_ai'), name="api_get_grievance"),
            url(r"^(?P<resource_name>%s)/getGrievances%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_grievances'), name="api_get_grievance"),
            url(r"^(?P<resource_name>%s)/update%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_grievance'), name="api_update_grievance"),
            url(r"^(?P<resource_name>%s)/update_on_grievance%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_update_on_grievance'), name="add_update_on_grievance"),
            url(r"^(?P<resource_name>%s)/update_on_grievance_distmod%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_on_grievance_distmod'), name="update_on_grievance_distmod")
        ]


   
    def update_on_grievance_distmod(self, request, **kwargs ):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            from mnews.models import News, News_state
            from media.api import recording_upload_impl
            from datetime import datetime
            time = datetime.now()
            title = request.REQUEST.get('title') or 'Update from Handler, Created at %s' % time.strftime('%H:%M, %d %B %Y')
            uploaded_file = request.FILES[ 'audio_file' ]
            grievance_id = request.REQUEST.get('grievance_id')
            state = request.REQUEST['state']
            quitreason_id = request.REQUEST.get('quitreason_id')

            grievance = Grievance.objects.get(id=grievance_id)
            mnews_ai_id = Properties.objects.get(ai=grievance.ai).mnews_ai.id
            recording = recording_upload_impl(uploaded_file, mnews_ai_id)
            parent = Grievance_item_mapping.objects.get(grievance=grievance, is_parent=True).item
            comment = News(ai=parent.ai, channel=parent.channel,
                           callerid=grievance.handler.number,
                           state=News_state.UNM, is_comment=True,
                           source=News.Source.WEB_INTERFACE, title=title, detail=recording)
            logger.info(str(comment.__dict__))
            comment.modified_date = datetime.now()
            comment.parent_id_value = parent.id
            comment.save()
            logger.info(str(comment.id))
            parent.comments.add(comment)
            Grievance_item_mapping(grievance=grievance, item=comment,
                                   creation_time=time, contributor=grievance.handler.number,
                                   contributor_type=ACTIONEE_CHOICES.HANDLER).save()
            user = request.user.id
            grievance_state = Grievance_state.objects.get(state=state)
            handler_id = None
            if int(quitreason_id) == 0:
                quitreason_id = None
            update_grievance_state_handler(grievance_id, grievance_state.id, ACTIONEE_CHOICES.HANDLER,
                                           Moderation_source.WEB,
                                           user, handler_id, quitreason_id)
            self.log_throttled_access(request)
            return self.create_response(request, comment)
        except Exception as e:
            logger.exception("success: {0}".format(request))
            return self.create_response(request, {'error': "unable to update the grievance"}, e)
            print(e)  



    def get_grievance_by_ai(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        grievance_bundle = []
        grievance_resource = CaseManagerGrievanceResource()
        ai_id = request.REQUEST['ai_id']
        state_id = request.REQUEST['state_id']
        resultset = Grievance.objects.filter(ai=ai_id, current_state_id=state_id)
        try:
	    for rs in resultset:
                grievance_item_mapping = Grievance_item_mapping.objects.filter(grievance_id=rs.id, is_parent=True)
                if grievance_item_mapping.exists():
                    item = grievance_item_mapping[0].item
                    rs.item = item
                    response_bundle = grievance_resource.build_bundle(obj=rs, request=request)
                    response_bundle = grievance_resource.full_dehydrate(response_bundle)

                    grievance_bundle.append(response_bundle)    	
        except Exception as e:
            print ("Rest error: {0}".format(e))
            traceback.print_exc()
        return self.create_response(request, {'result': grievance_bundle})

    def get_grievances(self, request, **kwargs):
        logger.info('inside get gri')
        self.method_check(request, allowed=['get'])
        grievance_bundle = []
        grievance_resource = CaseManagerGrievanceResource()
        ai_id = request.REQUEST['ai_id']
        state_id = request.REQUEST['state_id']
        callerid = request.REQUEST.get('callerid', None)

        if callerid:
            if Grievance_state.objects.get(id=state_id).state=="UNASSIGNED":
                properties_qs = Properties.objects.filter(ai__id=ai_id)
                if properties_qs.exists():
                    profile = properties_qs[0].profile
                else:
                    profile = Profile.objects.get(id=1) # DEFAULT_PROFILE_ID = 1
                assigned_state = Grievance_state.objects.get(state='ASSIGNED')
                # For Unassigned tab, we are pulling grievances which can be assigned.
                state_ids = Action_setting.objects.filter(actionee__in=('AUTO', 'HANDLER'), next_state=assigned_state).values_list('prev_state__id', flat=True)
                resultset = Grievance.objects.filter(ai__id=ai_id, current_state_id__in=state_ids).order_by("-creation_datetime")[:MAX_CASES]
            elif Grievance_state.objects.get(id=state_id).state=="ASSIGNED":
                resultset = Grievance.objects.filter(ai=ai_id, current_state__state__in=['ASSIGNED', 'INPROGRESS'], handler__number=callerid).order_by("-creation_datetime")[:MAX_CASES]
            else:
                resultset = []
        else:
            resultset = Grievance.objects.filter(ai=ai_id, current_state_id=state_id).order_by("-creation_datetime")
        logger.info(str(resultset))
        
        grievance_bundle=[]
        try:
            for rs in resultset:
                logger.info('inside loop')
                response_item = {}
                primary_key = {}
                grievance_item_mapping = Grievance_item_mapping.objects.filter(grievance_id=rs.id, is_parent=True)
                logger.info(str(grievance_item_mapping))
                if grievance_item_mapping.exists():
                    item = grievance_item_mapping[0].item 
                    response_item = model_to_dict(rs)                  
                    response_item['item_id'] = item.id
                    response_item['mnews_ai'] = item.ai_id
                    primary_key['skey'] = item.id
                    primary_key['pkey'] = SERVER_ID+"-ai-"+str(item.ai_id)  
                    response_item['item_key'] = json.dumps(primary_key)                   
                    logger.info(str(response_item))
                    grievance_bundle.append(response_item)
            return self.create_response(request, {'result': grievance_bundle})

        except Exception as e:
            print ("Rest error: {0}".format(e))
            traceback.print_exc()
            return self.create_response(request, {'result': str(e)})

    def dehydrate(self, bundle):
        grienvance_item = Grievance_item_mapping.objects.filter(grievance_id=bundle.data['id'], is_parent=False)
        news_resource = NewsResource()
        comments = []
        for cm in grienvance_item:
            rs = news_resource.build_bundle(obj=cm.item, request=bundle.request)
            rs = news_resource.full_dehydrate(rs)
            comments.append(rs)
        bundle.data['comments'] = comments
        return bundle

    def update_grievance(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        state = request.REQUEST['state']
        handler_id = request.REQUEST['handler_id']
        grievance_id = request.REQUEST['grievance_id']
        quitreason_id = request.REQUEST.get('quitreason_id')
        user = request.user.id
        logger.info('user  ::' + str(user))
        grievance_state = Grievance_state.objects.get(state=state)
        if int(handler_id) == 0:
            handler_id = None
       	if int(quitreason_id) == 0:
            quitreason_id = None

        update_grievance_state_handler(grievance_id, grievance_state.id, ACTIONEE_CHOICES.MODERATOR,
                                       Moderation_source.WEB,
                                       user, handler_id, quitreason_id)

        return self.create_response(request, {'result': True})

    def add_update_on_grievance(self, request, **kwargs ):
        from datetime import datetime
        from mnews.models import News
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            time = datetime.now()
            user = request.user.id

            title = request.REQUEST.get('title') or 'Update from Handler, Created at %s' % time.strftime('%H:%M, %d %B %Y')
            parent = News.objects.get(pk=request.REQUEST['parent_id'])
            grievance_id = request.REQUEST.get('grievance_id')
            grievance = Grievance.objects.get(id=grievance_id)
            comment = News(ai=parent.ai, channel=parent.channel,
                           callerid=grievance.handler.number,
                           state='UNM', is_comment=True,
                           source=News.Source.WEB_INTERFACE, title=title)
            comment.parent_id_value = parent.id
            comment.save()
            parent.comments.add(comment)
            Grievance_item_mapping(grievance=grievance, item=comment,
                                   creation_time=time, contributor=grievance.handler.number,
                                   contributor_type=ACTIONEE_CHOICES.HANDLER).save()
            if grievance.current_state.state=='ASSIGNED':
                update_grievance_state_handler(grievance.id, Grievance_state.objects.get(state='INPROGRESS').id, ACTIONEE_CHOICES.MODERATOR, News.Source.WEB_INTERFACE, user, grievance.handler.id)

            self.log_throttled_access(request)
            return self.create_response(request, comment)
        except Exception as e:
            logger.exception("create_new_update: {0}".format(request))
            return self.create_response(request, {'error': "unable to create new update"}, e)
            print(e)


class CaseManagerGrievanceQuitReasonResource(ModelResource):
    class Meta:
        resource_name = "casemanager/grievance_quit_reason"
        allowed_methods = ['get', 'post']
        queryset = Grievance_quit_reason.objects.all()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/getGrievanceQuitReason%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_grievance_quit_reason_by_ai'), name="api_get_grievance_quit_reason_by_ai")
        ]

    def get_grievance_quit_reason_by_ai(self, request, **kwargs):
        grievance_quit_reason_group_bundle = []

        grievance_quit_reason_resource = CaseManagerGrievanceQuitReasonResource()

        self.method_check(request, allowed=['get'])
        ai_id = request.REQUEST['ai_id']
	if request.REQUEST.get('actionee'):
            actionee_choices = request.REQUEST['actionee']
            actionee_choices = eval(actionee_choices)
        else:
            actionee_choices = [ACTIONEE_CHOICES.MODERATOR, ACTIONEE_CHOICES.HAN_MOD]
        grievance_quit_reason_group = Grievance_ai_quit_reason_group.objects.filter(ai=ai_id)
        quit_reason_group = grievance_quit_reason_group[0].group
        quit_reasons = quit_reason_group.quit_reason
        #reasons = quit_reasons.filter(available_to__in=[ACTIONEE_CHOICES.MODERATOR, ACTIONEE_CHOICES.HAN_MOD])
	reasons = quit_reasons.filter(available_to__in= actionee_choices)
        for reason in reasons:
            response_bundle = grievance_quit_reason_resource.build_bundle(obj=reason, request=request)
            response_bundle = grievance_quit_reason_resource.full_dehydrate(response_bundle)
            grievance_quit_reason_group_bundle.append(response_bundle)

        return self.create_response(request, {'result': grievance_quit_reason_group_bundle})
