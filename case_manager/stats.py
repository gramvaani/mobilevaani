from datetime import *

from django.template.loader import render_to_string

from vapp.app_manager.models import App_instance
from vapp.case_manager.models import Grievance
from vapp.stats.models import Stats
from vapp.stats.stats import get_ai_call_stats, get_start_date_for_instance
from vapp.utils import is_start_of_week, LINE_BREAK


def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    data = []
    reports = []

    for ai in ais:
        stats = Stats()
        stats.ai_name = ai.name
        stats.call_stats = get_call_stats(ai.id, start_datetime, end_datetime)
        stats.grievance_stats = get_grievance_stats(ai.id, start_datetime, end_datetime)
        ai_start_date = get_start_date_for_instance(ai.id)
        stats.cumulative_call_stats = get_call_stats(ai.id, ai_start_date, end_datetime)
        data.append(stats)
    return (data, reports)


def get_call_stats(ai_id, start_datetime, end_datetime):
    call_stat = get_ai_call_stats(App_instance.objects.get(id = ai_id), start_datetime, end_datetime)
    return call_stat
    

def get_grievance_stats(ai_id, start_datetime, end_datetime):
	stats = Stats()
	grievance_qs = Grievance.objects.filter(ai=App_instance.objects.get(id=ai_id))
	stats.received_grievance = grievance_qs.filter(creation_datetime__range=(start_datetime, end_datetime)).count()
	stats.assigned_grievance = grievance_qs.filter(creation_datetime__range=(start_datetime, end_datetime), handler__isnull=False).count()
	stats.rejected_grievance = grievance_qs.filter(creation_datetime__range=(start_datetime, end_datetime), current_state__state='REJECTED').count()
	stats.total_received_grievance = grievance_qs.count()
	stats.total_assigned_grievance = grievance_qs.filter(handler__isnull=False).count()
	stats.total_unassigned_grievance = grievance_qs.filter(handler__isnull=True).count()
	stats.total_rejected_grievance = grievance_qs.filter(current_state__state='REJECTED').count()
	stats.total_opened_grievance = grievance_qs.filter(current_state__state='REOPENED').count()
	stats.total_resolved_grievance = grievance_qs.filter(current_state__state='CLOSED').count()
	stats.total_unresolvable_grievance = grievance_qs.filter(current_state__state='CLOSED_UNRESOLVED').count()
	return stats


def create_html_content(stats):
    return render_to_string('case_manager/stats.html', { 'data': stats }) + LINE_BREAK
