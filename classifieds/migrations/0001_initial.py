# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Classified'
        db.create_table(u'classifieds_classified', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('volunteer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['field_mis.Volunteer'], null=True, blank=True)),
            ('customer_no', self.gf('app_manager.models.CalleridField')(max_length=20, null=True, blank=True)),
            ('payment', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('classified_detail', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True, blank=True)),
            ('detail_transcription', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('addition_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('is_played', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('payment_recvd', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'classifieds', ['Classified'])

        # Adding model 'Classified_ai_properties'
        db.create_table(u'classifieds_classified_ai_properties', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('send_dialout_ack', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('send_sms_ack', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time_limit_seconds', self.gf('django.db.models.fields.PositiveIntegerField')(default=60)),
            ('max_retries', self.gf('django.db.models.fields.PositiveIntegerField')(default=3)),
            ('delay_between_tries_min', self.gf('django.db.models.fields.PositiveIntegerField')(default=60)),
            ('ack_schedule', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Transient_group_schedule'], null=True, blank=True)),
            ('sms_message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_template'], null=True, blank=True)),
        ))
        db.send_create_signal(u'classifieds', ['Classified_ai_properties'])

        # Adding model 'Classified_call_log'
        db.create_table(u'classifieds_classified_call_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('classified', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['classifieds.Classified'])),
            ('gcl', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Groups_call_log'])),
        ))
        db.send_create_signal(u'classifieds', ['Classified_call_log'])

        # Adding model 'Classified_sms_log'
        db.create_table(u'classifieds_classified_sms_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('classified', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['classifieds.Classified'])),
            ('sms_message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_message'])),
        ))
        db.send_create_signal(u'classifieds', ['Classified_sms_log'])


    def backwards(self, orm):
        # Deleting model 'Classified'
        db.delete_table(u'classifieds_classified')

        # Deleting model 'Classified_ai_properties'
        db.delete_table(u'classifieds_classified_ai_properties')

        # Deleting model 'Classified_call_log'
        db.delete_table(u'classifieds_classified_call_log')

        # Deleting model 'Classified_sms_log'
        db.delete_table(u'classifieds_classified_sms_log')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trigger': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'app_manager.vi_reference': {
            'Meta': {'object_name': 'VI_reference'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'target_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'target_vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'target_vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vi'", 'to': u"orm['app_manager.VI_conf']"}),
            'vi_data': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'})
        },
        u'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'location_fk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'classifieds.classified': {
            'Meta': {'object_name': 'Classified'},
            'addition_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'classified_detail': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            'customer_no': ('app_manager.models.CalleridField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'detail_transcription': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_played': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'payment': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'payment_recvd': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'volunteer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']", 'null': 'True', 'blank': 'True'})
        },
        u'classifieds.classified_ai_properties': {
            'Meta': {'object_name': 'Classified_ai_properties'},
            'ack_schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']", 'null': 'True', 'blank': 'True'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'delay_between_tries_min': ('django.db.models.fields.PositiveIntegerField', [], {'default': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_retries': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'send_dialout_ack': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'send_sms_ack': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']", 'null': 'True', 'blank': 'True'}),
            'time_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '60'})
        },
        u'classifieds.classified_call_log': {
            'Meta': {'object_name': 'Classified_call_log'},
            'classified': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['classifieds.Classified']"}),
            'gcl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Groups_call_log']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'classifieds.classified_sms_log': {
            'Meta': {'object_name': 'Classified_sms_log'},
            'classified': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['classifieds.Classified']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_message']"})
        },
        u'field_mis.field': {
            'Meta': {'object_name': 'Field'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'field_mnews_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'referral_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'field_referral_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"})
        },
        u'field_mis.volunteer': {
            'Meta': {'object_name': 'Volunteer'},
            'comments': ('django.db.models.fields.TextField', [], {}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Contact']"}),
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'volunteer_club': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer_club']", 'null': 'True', 'blank': 'True'}),
            'volunteer_since': ('django.db.models.fields.DateField', [], {})
        },
        u'field_mis.volunteer_club': {
            'Meta': {'object_name': 'Volunteer_club'},
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'local_volclub_mnews'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'local_referral_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'local_volclub_referrals'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'duration': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.ai_transient_group': {
            'Meta': {'object_name': 'Ai_transient_group'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vi_reference': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_reference']", 'null': 'True'})
        },
        u'mnews.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'mnews.groups_call_log': {
            'Meta': {'object_name': 'Groups_call_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'group_schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'vi_conf': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True', 'blank': 'True'})
        },
        u'mnews.transient_group': {
            'Meta': {'object_name': 'Transient_group'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'generating_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mnews.transient_group_schedule': {
            'Meta': {'object_name': 'Transient_group_schedule'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ai_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Ai_transient_group']"}),
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mnews.Days']", 'symmetrical': 'False'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'onpush_sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']", 'null': 'True', 'blank': 'True'}),
            'play_ai': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'prompt_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'repeat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['classifieds']