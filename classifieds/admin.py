from django.contrib import admin
from models import Classified, Classified_ai_properties, Classified_call_notification, Classified_sms_notification, Classified_status


class ClassifiedAdmin(admin.ModelAdmin):
    raw_id_fields = ("classified_detail",)

admin.site.register(Classified_ai_properties)
admin.site.register(Classified_status)
admin.site.register(Classified, ClassifiedAdmin)
admin.site.register(Classified_sms_notification)
admin.site.register(Classified_call_notification)
