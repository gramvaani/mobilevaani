from django.db import models
from field_mis.models import Volunteer
from callerinfo.models import Contact
from advert.models import Advertisement, Ad_impression_policy
from app_manager.models import App_instance, Cdr, CalleridField
from sms.models import SMS_message, SMS_template
from mnews.models import Transient_group_schedule
from telephony.utils import get_formatted_number
from mnews.models import Groups_call_log
from media.models import Recording
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.contrib.auth.models import User
from sms.tasks import SMSTask
# Create your models here.
from log import get_request_logger
logger = get_request_logger()
from django.forms.models import model_to_dict

FIELDS_CHANGED_LENGTH = 100

class Classified(models.Model):
    ai = models.ForeignKey( App_instance )
    volunteer = models.ForeignKey( Volunteer, null = True, blank = True )
    customer_no = CalleridField( null = True, blank = True )
    payment = models.PositiveIntegerField()
    voucher_no = models.PositiveIntegerField()
    classified_detail = models.ForeignKey( Recording, null = True, blank = True )
    detail_transcription = models.TextField( null = True, blank = True )
    addition_time    = models.DateTimeField(auto_now_add = True)    
    is_played   = models.BooleanField( default = False )
    payment_recvd  = models.BooleanField( default = False )

    def save(self, *args, **kwargs):            
        status_changes = []
        if self.pk:
            old = Classified.objects.get( id = self.id )
            if self.volunteer != old.volunteer or self.payment != old.payment or \
                self.voucher_no != old.voucher_no:
                status_changes.append( 'MOD' )
            if self.payment_recvd != old.payment_recvd and self.payment_recvd:
                status_changes.append( 'PMR' )
            if self.is_played != old.is_played and self.is_played:
                status_changes.append( 'CDP' )
        else:
            status_changes.append( 'NEW' )

        super( Classified, self).save( *args, **kwargs )

        sms_notify( self, status_changes )
        call_notify( self, status_changes )


class Classified_moderation_log(models.Model):
    user = models.ForeignKey(User, null = False)
    classified = models.ForeignKey(Classified)
    timestamp = models.DateTimeField(auto_now_add = True)
    fields_changed = models.CharField(max_length = FIELDS_CHANGED_LENGTH, null = True)


class Classified_ai_properties(models.Model):
    ai = models.ForeignKey( App_instance )
    time_limit_seconds = models.PositiveIntegerField(default = 60)
    sms_retries = models.PositiveIntegerField( default = 3 )
    call_retries = models.PositiveIntegerField( default = 3 )
               

class Classified_status(models.Model):
    STATUS_CHOICES = (
                    ('NEW', 'Newly added'),
                    ('MOD', 'Details Modified'),
                    ('DEL', 'Deleted'),
                    ('PMR', 'Payment Received'),
                    ('CDP', 'Classified Played'),
        )
    status_code = models.CharField( max_length = 3, choices = STATUS_CHOICES )
    
    def __unicode__(self):
        return self.status_code

class Classified_sms_notification(models.Model):
    NOTIFY_TO_OPTIONS = (
                        ('VOL', "Volunteer"),
                        ('CUS', "Customer"),
        )
    ai = models.ForeignKey( App_instance )
    ack_sms_message = models.ForeignKey( SMS_template )
    status = models.ForeignKey( Classified_status )
    notify_to = models.CharField( max_length = 3, choices = NOTIFY_TO_OPTIONS )

    def __unicode__( self ):
        return unicode( self.ai.id ) + u'_' + unicode( self.status ) + u'_' + unicode( self.notify_to )

class Classified_call_notification(models.Model):
    NOTIFY_TO_OPTIONS = (
                        ('VOL', "Volunteer"),
                        ('CUS', "Customer"),
        )
    ai = models.ForeignKey( App_instance )
    ack_call_message = models.ForeignKey( Transient_group_schedule )
    status = models.ForeignKey( Classified_status ) 
    notify_to = models.CharField( max_length = 3, choices = NOTIFY_TO_OPTIONS )

    def __unicode__( self ):
        return unicode( self.ai.id ) + u'_' + unicode( self.status ) + u'_' + unicode( self.notify_to )       

class Classified_call_log(models.Model):
    classified = models.ForeignKey( Classified )
    call_notification = models.ForeignKey( Classified_call_notification )
    gcl        = models.ForeignKey( Groups_call_log )


class Classified_sms_log(models.Model):
    classified = models.ForeignKey( Classified )
    sms_notification = models.ForeignKey( Classified_sms_notification )
    sms_message = models.ForeignKey( SMS_message )

def get_classified_dict( classified ):
    cl_dict = model_to_dict( classified, fields=[ field.name for field in classified._meta.fields ] )
    for key, val in cl_dict.items():
        if key == 'volunteer' and classified.volunteer:
            cl_dict[ key ] = classified.volunteer.contact.number
        elif key == 'detail_transcription' and classified.detail_transcription:
            cl_dict[ key ] = str( classified.detail_transcription.id )
        else:
            cl_dict[ key ] = str(  val )
    return cl_dict

def sms_notify( classified, status_changes ):
    cl_dict = get_classified_dict( classified )
    classified_properties = Classified_ai_properties.objects.get( ai = classified.ai )

    for status_change in status_changes:
        cl_sms_notificatons = Classified_sms_notification.objects.filter( ai = classified.ai, status__status_code = status_change )
        for cl_sms_notificaton in cl_sms_notificatons:
            message = cl_sms_notificaton.ack_sms_message.process_template( cl_dict )
            if cl_sms_notificaton.notify_to == 'CUS':
                number = classified.customer_no
            else:
                number = classified.volunteer.contact.number
            sms_message = SMSTask.create_send_msg( classified.ai.id, message, number, max_tries = classified_properties.sms_retries )
            Classified_sms_log.objects.create( classified = classified, sms_notification = cl_sms_notificaton, sms_message = sms_message )

def call_notify( classified, status_changes ):
    classified_properties = Classified_ai_properties.objects.get( ai = classified.ai )

    for status_change in status_changes:
        cl_call_notificatons = Classified_call_notification.objects.filter( ai = classified.ai, status__status_code = status_change )
        for cl_call_notificaton in cl_call_notificatons:
            if cl_call_notificaton.notify_to == 'CUS':
                number = classified.customer_no
            else:
                number = classified.volunteer.contact.number
            gcl = cl_call_notificaton.ack_call_message.callout_to_number( number, max_tries = classified_properties.call_retries )
            Classified_call_log.objects.create( classified = classified, call_notification = cl_call_notificaton, gcl = gcl )


@receiver( pre_delete, sender = Classified )
def notify_on_delete( sender, instance, **kwargs ):
    status_changes = [ 'DEL' ]
    sms_notify( instance, status_changes )
    call_notify( instance, status_changes )