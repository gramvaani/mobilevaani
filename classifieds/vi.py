from classifieds.models import Classified, Classified_ai_properties, Classified_call_log, Classified_sms_log
from field_mis.models import Volunteer
from telephony.statemachine import BaseVappController
from telephony.utils import *
from mnews.tasks import SMSTask
from log import get_logger
logger = get_logger()


class ClassifiedController( BaseVappController ): 
        
    def pre_welcome(self):
        self.callerid = self.sessionData.cdrs[0].callerid
        self.classified_properties = Classified_ai_properties.objects.get( ai = self.ai )
        return self.getPromptParams( 'classified_welcome' )
    
    def while_volunteeridentity__sm_action_success(self, events, eventsData):
        self.callerid = get_formatted_number( self.callerid )
        try:
            self.volunteer = Volunteer.objects.get( contact__number = get_formatted_number( self.callerid ) )
            return 'volunteer_identified'
        except Exception,e:
            logger.exception('exception while getting volunteer: %s' %(e))
        return 'volunteer_unidentified'
    
    def pre_volnotidentified(self):
        return self.getPromptParams( 'classified_vol_not_identified' )

    def pre_entercustomerno(self):
        self.classified = Classified( ai = self.ai, volunteer = self.volunteer )
        return self.getPromptPlayAndGetParams( promptName = 'classified_enter_customer_no',  minDigits = 10, maxDigits = 10, tries = 3 )

    def while_entercustomerno__sm_action_success(self, events, eventsData):
        cust_no = self.getPlayAndGetDigits(eventsData)
        self.classified.customer_no = str( cust_no )

    def pre_enterpayment(self):
        return self.getPromptPlayAndGetParams( promptName = 'classified_enter_payment',  minDigits = 2, maxDigits = 5, tries = 3 )

    def while_enterpayment__sm_action_success(self, events, eventsData):
        payment = self.getPlayAndGetDigits(eventsData)
        self.classified.payment = int( payment ) 

    def pre_entervoucherno(self):
        return self.getPromptPlayAndGetParams( promptName = 'classified_enter_voucher_no',  minDigits = 4, maxDigits = 4, tries = 3 )

    def while_entervoucherno__sm_action_success(self, events, eventsData):
        voucher_no = self.getPlayAndGetDigits(eventsData)
        self.classified.voucher_no = int( voucher_no ) 

    def pre_classifiedprompt(self):
        return self.getPromptParams( 'classified_record_classified' )

    def pre_recordclassified(self):        
        return self.getEmbeddedRecordingParams( self.classified_properties.time_limit_seconds, self.classified, Classified.classified_detail )
    
    def while_recordclassified__sm_action_success(self, events, eventsData):
        self.classified.save()            

    def pre_recordingthankyou(self):
        return self.getPromptParams( 'classified_recording_thank_you' )

    def pre_newclassified(self):
        return self.getPromptPlayAndGetParams( promptName = 'classified_new_classified',  minDigits = 1, maxDigits = 1 )

    def while_newclassified__sm_action_success(self, events, eventsData):
        choice = self.getPlayAndGetDigits(eventsData)
        if int( choice ) == 1:
            return 'new_classified'
        else:
            return 'no_new_classified'

    def pre_reqnotprovided(self):
        return self.getPromptParams( 'classified_required_no_input' )

    def pre_thankyou(self):
        return self.getPromptParams( 'classified_thank_you' )
    
    def while_appexit__sm_action_success(self, events, eventsData):
        transition_string = self.sessionData.popEmbedStack()
        if transition_string:
            return transition_string
    
    
ClassifiedStateDescriptionMap = [
    
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    
    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'] 
            } 
        },

    {   'name':'welcome', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'volunteeridentity':['sm_action_success'],
            } 
    },

    {   'name': 'volunteeridentity',
        'action': 'none',
        'transitions': {
            'stop': ['sm_action_failure'],            
            'entercustomerno': [ 'volunteer_identified', 'sm_action_success'],
            'volnotidentified': ['volunteer_unidentified'],
            },
        },

    {   'name': 'volnotidentified',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],            
            'appexit': ['sm_action_success'],
            },
        },
    
    {   'name': 'entercustomerno',
        'action': 'play_and_get_digits',
        'transitions' : {
            'stop': ['sm_action_failure'],
            'reqnotprovided': ['sm_get_digits_no_digits'],
            'enterpayment': ['sm_action_success'],                        
            },
    },
                                  
    {  'name':'enterpayment',
       'action':'play_and_get_digits',
       'transitions' :{       
        'stop': ['sm_action_failure'],
        'reqnotprovided': ['sm_get_digits_no_digits'],
        'entervoucherno': ['sm_action_success'],    
        },
    },
    {  'name':'entervoucherno',
       'action':'play_and_get_digits',
       'transitions' :{       
        'stop': ['sm_action_failure'],
        'reqnotprovided': ['sm_get_digits_no_digits'],
        'classifiedprompt': ['sm_action_success'],    
        },
    },
    
    {  'name':'classifiedprompt',
       'action':'playback',
       'transitions' :{    
        'stop': ['sm_action_failure'],
        'recordclassified': ['sm_action_success'],    
        
        },
    },

    {  'name':'recordclassified',
       'action':'record',
       'transitions' :{         
        'stop': [ 'sm_action_failure' ],  
        'recordingthankyou': ['sm_action_success'], 
        },
    },  

    {  'name':'recordingthankyou',
       'action':'playback',
       'transitions' :{         
        'stop': ['sm_action_failure'], 
        'newclassified': [ 'sm_action_success' ],
        },
    },

    {  'name':'newclassified',
       'action':'play_and_get_digits',
       'transitions' :{       
        'stop': ['sm_action_failure'],
        'entercustomerno': ['new_classified'],
        'thankyou': [ 'sm_action_success', 'no_new_classified', 'sm_get_digits_no_digits' ],    
        },
    },

    {  'name':'thankyou',
       'action':'playback',
       'transitions' :{         
        'stop': ['sm_action_failure'], 
        'appexit': [ 'sm_action_success' ],
        },
    },
    {  'name':'reqnotprovided',
       'action':'playback',
       'transitions' :{         
        'stop': ['sm_action_failure'], 
        'thankyou': [ 'sm_action_success' ],
        },
    },
    {  'name':'appexit',
       'action':'none',
       'transitions' :{         
        'stop': ['sm_action_failure', 'sm_action_success'],           
        },
    },      
        
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
    },   
    ]
