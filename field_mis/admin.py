from django.contrib import admin
from models import *

class FieldAdmin(admin.ModelAdmin):
    filter_horizontal = ('mnews_ais', 'referral_ais')
admin.site.register(Field, FieldAdmin)

class CommunityManagerAdmin(admin.ModelAdmin):
    filter_horizontal = ('managed_locations', )
admin.site.register(Community_manager, CommunityManagerAdmin)

class VolunteerClubAdmin(admin.ModelAdmin):
    filter_horizontal = ('local_referral_ais', )
admin.site.register(Volunteer_club, VolunteerClubAdmin)

class VolunteerAdmin(admin.ModelAdmin):
    search_fields = ['contact__name', 'contact__number']
admin.site.register(Volunteer, VolunteerAdmin)

class StatsSMSTemplateSettingsAdmin( admin.ModelAdmin ):
    filter_horizontal = ('clubs', )
admin.site.register(Stats_sms_template_settings, StatsSMSTemplateSettingsAdmin)

admin.site.register(Field_stats_user_map)
admin.site.register(Notification_group_settings)

