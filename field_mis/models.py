from django.db import models
from app_manager.models import App_instance, CalleridField, CALLERID_LENGTH
from campaign.models import Campaign
from location.models import Location
from callerinfo.models import Contact, Contact_list
from sms.models import SMS_message, SMS_template

from django.contrib.auth.models import User

NAME_MAX_LENGTH = 255
CLUB_STATS = 'CLUB_STATS'
MIN_REGULAR_REPORTING_COUNT = 5
MIN_COMMUNITY_MEETINGS_DONE = 1


class AddUpdate( object ):
    ADD = 0
    UPDATE = 1
    

class Field(models.Model):
    name = models.CharField(max_length = NAME_MAX_LENGTH)
    description = models.TextField()
    mnews_ais = models.ManyToManyField(App_instance, null = True, blank = True, related_name = 'field_mnews_ais')
    referral_ais = models.ManyToManyField(App_instance, null = True, blank = True, related_name = 'field_referral_ais')

    def __unicode__(self):
        return unicode(self.name)

    def get_all_vol_numbers(self):       
        numbers = []
        for vol in self.volunteer_set.all():
            numbers.append(vol.contact.number)
            
        return set(numbers)


class Community_manager(models.Model):
    name = models.CharField(max_length = NAME_MAX_LENGTH)
    field = models.ForeignKey(Field)
    managed_locations = models.ManyToManyField(Location)
    
    def __unicode__(self):
        return unicode(self.name)


class Volunteer_club(models.Model):
    name = models.CharField(max_length = NAME_MAX_LENGTH)
    field = models.ForeignKey(Field)
    location = models.ForeignKey(Location)
    local_mnews_ai = models.ForeignKey(App_instance, related_name = 'local_volclub_mnews', null = True, blank = True)
    local_referral_ais = models.ManyToManyField(App_instance, related_name = 'local_volclub_referrals', null = True, blank = True)

    def __unicode__(self):
        return unicode(self.name)

    def get_all_vol_numbers(self):       
        numbers = []
        for vol in self.volunteer_set.all():
            numbers.append(vol.contact.number)
            
        return set(numbers)
    
    def get_manager(self):
        managers = Community_manager.objects.filter(managed_locations__id = self.location.id)
        if managers.exists():
            return managers[0]


class Volunteer(models.Model):
    contact = models.ForeignKey(Contact)
    field = models.ForeignKey(Field)
    volunteer_club = models.ForeignKey(Volunteer_club, null = True, blank = True)
    volunteer_since = models.DateField()
    volunteer_till = models.DateField(null=True, blank=True)
    comments = models.TextField()
    
    def __unicode__(self):
        return unicode(self.contact.name)

    
class Volunteer_referral_stats(models.Model):
    vol = models.ForeignKey(Volunteer)
    start = models.DateTimeField()
    end = models.DateTimeField()
    ai = models.ForeignKey(App_instance)
    referrals = models.PositiveIntegerField()
    successful = models.PositiveIntegerField()


class Volunteer_cumulative_stats(models.Model):
    vol = models.ForeignKey(Volunteer)
    to_time = models.DateTimeField()
    calls = models.PositiveIntegerField()
    contribs = models.PositiveIntegerField()
    referrals = models.PositiveIntegerField()    
    successful = models.PositiveIntegerField()
    avg_monthly_calls = models.PositiveIntegerField()
    avg_monthly_contribs = models.PositiveIntegerField()
    avg_monthly_referrals = models.PositiveIntegerField()    
    avg_monthly_successful = models.PositiveIntegerField()


class Volunteer_club_stats(models.Model):
    start = models.DateTimeField()
    end = models.DateTimeField()
    club = models.ForeignKey(Volunteer_club)
    location_contribs = models.PositiveIntegerField(null = True)
    volunteer_contribs =  models.PositiveIntegerField(null = True)
    local_mnews_contribs = models.PositiveIntegerField(null = True)
    volunteer_count = models.PositiveIntegerField(null = True)
    avg_callback_calls = models.DecimalField(max_digits = 10, decimal_places = 2, null = True)
    avg_callout_calls = models.DecimalField(max_digits = 10, decimal_places = 2, null = True)


class Volunteer_club_weekly_stats(models.Model):
    club = models.ForeignKey(Volunteer_club)
    week_start_date = models.DateTimeField()
    new_volunteers = models.PositiveIntegerField()
    volunteers_trained = models.TextField()
    new_listener_clubs = models.PositiveIntegerField()
    trainings_by_company = models.PositiveIntegerField()
    trainings_by_club = models.PositiveIntegerField()
    meeting_conducters = models.ManyToManyField(Volunteer)
    content_quality = models.CharField(max_length = 3)
    new_campaign_orientation = models.BooleanField(default = False)
    grievance_followups_taken = models.PositiveIntegerField()
    grievance_followups_completed = models.PositiveIntegerField()
    impact_stories = models.PositiveIntegerField()


class Volunteer_incentive_stats(models.Model):
    volunteer = models.ForeignKey(Volunteer)
    month_start_date = models.DateTimeField()
    amount_for_comunity_meetings = models.PositiveIntegerField(default = 0, null = True)
    amount_for_good_contribs = models.PositiveIntegerField(default = 0, null = True)
    amount_for_club_calls = models.PositiveIntegerField(default = 0, null = True)


class Populate_volunteer_incentive_stats_status(models.Model):
    STATUS_CHOICES = (
        ('POPULATED', 'POPULATED'),
        ('INPROGRESS', 'INPROGRESS')
    )
    club = models.ForeignKey(Volunteer_club)
    status = models.CharField(max_length = 10, choices = STATUS_CHOICES)


class Volunteer_community_meetings_stats(models.Model):
    volunteer = models.ForeignKey(Volunteer)
    community_meetings = models.PositiveIntegerField(default = 0)
    start_date = models.DateTimeField()
    ever_applied = models.BooleanField(default = False)
    entered_by = models.ForeignKey(User)
    def __unicode__(self):
        return unicode(self.id) + " _ " + unicode(self.volunteer.contact.name) + " _ " + unicode(self.volunteer.contact.number) + " _ " + unicode(self.community_meetings)


class Volunteer_stats(models.Model):
    vol = models.ForeignKey(Volunteer)
    start = models.DateTimeField()
    end = models.DateTimeField()
    avg_calls = models.PositiveIntegerField(default = 0)
    contribs = models.PositiveIntegerField(default = 0)
    contribs_avg_listening_dur = models.DecimalField(max_digits = 10, decimal_places = 2, null = True)
    good_contribs = models.PositiveIntegerField(default = 0, null = True)
    community_meetings_stats = models.ForeignKey(Volunteer_community_meetings_stats, null = True)
    meeting_attended = models.BooleanField(default = True)
    status = models.BooleanField()
    entered_by = models.ForeignKey(User, null = True)


class Payment_slab(models.Model):
    club = models.ForeignKey(Volunteer_club)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null = True)
    ever_applied = models.BooleanField(default = False)

    def __unicode__(self):
        return unicode(self.id) + " _ " + unicode(self.club) + " _ " + unicode(self.start_date) + " _ " + unicode(self.end_date)


class Payment_slab_community_meetings(models.Model):
    payment_slab = models.ForeignKey(Payment_slab)
    lower_limit = models.PositiveIntegerField()
    upper_limit = models.PositiveIntegerField()
    avg_incentive = models.PositiveIntegerField()
    max_incentive = models.PositiveIntegerField()


class Payment_slab_good_contribs(models.Model):
    payment_slab = models.ForeignKey(Payment_slab)
    lower_limit = models.PositiveIntegerField()
    upper_limit = models.PositiveIntegerField()
    avg_incentive = models.PositiveIntegerField()
    max_incentive = models.PositiveIntegerField()


class Payment_slab_club_calls(models.Model):
    payment_slab = models.ForeignKey(Payment_slab)
    lower_limit = models.PositiveIntegerField()
    upper_limit = models.PositiveIntegerField()
    avg_incentive = models.PositiveIntegerField()
    max_incentive = models.PositiveIntegerField()


class Location_stats(models.Model):
    start = models.DateTimeField()
    end = models.DateTimeField()
    location = models.ForeignKey(Location)
    total_contribs = models.PositiveIntegerField(null = True)
    volunteer_contribs =  models.PositiveIntegerField(null = True)
    campaigns = models.ManyToManyField(Campaign, null=True)


class Campaign_stats(models.Model):
    start = models.DateTimeField()
    end = models.DateTimeField()
    location = models.ForeignKey(Location)
    campaign = models.ForeignKey(Campaign) 
    total_contribs = models.PositiveIntegerField(null = True)
    volunteer_contribs =  models.PositiveIntegerField(null = True)


class Field_stats_user_map(models.Model):
    field = models.ForeignKey(Field)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return '%s, %s' % (self.field, self.user)


class Notification_group_settings(models.Model):
    notification_group = models.ForeignKey(Contact_list)

    def __unicode__(self):
        return unicode(self.notification_group)

class Stats_sms_template_settings(models.Model):
    sms_template = models.ForeignKey(SMS_template)
    clubs = models.ManyToManyField(Volunteer_club)

    def __unicode__(self):
        return unicode(self.id) +'_'+ unicode(self.sms_template)

class Volunteer_sms_log(models.Model):
    volunteer = models.ForeignKey(Volunteer)
    sms_message = models.ForeignKey(SMS_message)