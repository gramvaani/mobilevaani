from models import *

from celery.task import Task

from field_mis.stats import populate_incentive_stats_for_club, populate_volunteer_incentive_stats, populate_volunteer_stats, populate_club_caller_stats_weekly
from vapp.utils import is_start_of_month
from datetime import datetime


class PopulateClubCallerStatsWeekly(Task):
	def run(self):
		populate_club_caller_stats_weekly()


class PopulateVolunteerStats(Task):
	def run(self):
		populate_volunteer_stats()


class PopulateVolunteerIncentiveStats(Task):
	def run(self, club = None):
		if club is not None:
			populate_incentive_stats_for_club(club)
		else:
			populate_volunteer_incentive_stats_status_objs = Populate_volunteer_incentive_stats_status.objects.filter(status = 'INPROGRESS')
			if populate_volunteer_incentive_stats_status_objs.exists():
				clubs = populate_volunteer_incentive_stats_status_objs.values_list('club', flat = True)
				for club in clubs:
					populate_incentive_stats_for_club(club)
			else:
				populate_volunteer_incentive_stats()