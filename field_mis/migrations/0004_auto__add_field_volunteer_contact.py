# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Volunteer.contact'
        db.add_column(u'field_mis_volunteer', 'contact',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['callerinfo.Contact']),
                      keep_default=False)

        # Removing M2M table for field contacts on 'Volunteer'
        db.delete_table(db.shorten_name(u'field_mis_volunteer_contacts'))


    def backwards(self, orm):
        # Deleting field 'Volunteer.contact'
        db.delete_column(u'field_mis_volunteer', 'contact_id')

        # Adding M2M table for field contacts on 'Volunteer'
        m2m_table_name = db.shorten_name(u'field_mis_volunteer_contacts')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('volunteer', models.ForeignKey(orm[u'field_mis.volunteer'], null=False)),
            ('contact', models.ForeignKey(orm[u'callerinfo.contact'], null=False))
        ))
        db.create_unique(m2m_table_name, ['volunteer_id', 'contact_id'])


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'location_fk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'field_mis.community_manager': {
            'Meta': {'object_name': 'Community_manager'},
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'managed_locations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['location.Location']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'field_mis.field': {
            'Meta': {'object_name': 'Field'},
            'campaigns': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['campaign.Campaign']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'field_mnews_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'referral_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'field_referral_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"})
        },
        u'field_mis.volunteer': {
            'Meta': {'object_name': 'Volunteer'},
            'comments': ('django.db.models.fields.TextField', [], {}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Contact']"}),
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'volunteer_club': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer_club']", 'null': 'True', 'blank': 'True'}),
            'volunteer_since': ('django.db.models.fields.DateField', [], {})
        },
        u'field_mis.volunteer_club': {
            'Meta': {'object_name': 'Volunteer_club'},
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'local_volclub_mnews'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'local_referral_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'local_volclub_referrals'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'field_mis.volunteer_cumulative_stats': {
            'Meta': {'object_name': 'Volunteer_cumulative_stats'},
            'avg_monthly_calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'avg_monthly_contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'avg_monthly_referrals': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'avg_monthly_successful': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referrals': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'successful': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'to_time': ('django.db.models.fields.DateTimeField', [], {}),
            'vol': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']"})
        },
        u'field_mis.volunteer_referral_stats': {
            'Meta': {'object_name': 'Volunteer_referral_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referrals': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'successful': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'vol': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']"})
        },
        u'field_mis.volunteer_stats': {
            'Meta': {'object_name': 'Volunteer_stats'},
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'vol': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']"})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'states': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'states'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.State']"})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            'blocks': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'blocks'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.Block']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            'districts': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'districts'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['field_mis']