# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Campaign_stats'
        db.create_table(u'field_mis_campaign_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['campaign.Campaign'])),
            ('total_contribs', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('volunteer_contribs', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Location'])),
        ))
        db.send_create_signal(u'field_mis', ['Campaign_stats'])

        # Adding model 'Location_stats'
        db.create_table(u'field_mis_location_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Location'])),
            ('total_contribs', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('volunteer_contribs', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
        ))
        db.send_create_signal(u'field_mis', ['Location_stats'])

        # Adding M2M table for field campaigns on 'Location_stats'
        m2m_table_name = db.shorten_name(u'field_mis_location_stats_campaigns')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('location_stats', models.ForeignKey(orm[u'field_mis.location_stats'], null=False)),
            ('campaign', models.ForeignKey(orm[u'campaign.campaign'], null=False))
        ))
        db.create_unique(m2m_table_name, ['location_stats_id', 'campaign_id'])


    def backwards(self, orm):
        # Deleting model 'Campaign_stats'
        db.delete_table(u'field_mis_campaign_stats')

        # Deleting model 'Location_stats'
        db.delete_table(u'field_mis_location_stats')

        # Removing M2M table for field campaigns on 'Location_stats'
        db.delete_table(db.shorten_name(u'field_mis_location_stats_campaigns'))


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'callerinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'b_day': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'b_year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'location_fk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'number': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'abstract': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'cover_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cover_image'", 'null': 'True', 'to': u"orm['media.Image_caption_map']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'images'", 'symmetrical': 'False', 'to': u"orm['media.Image_caption_map']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'show_in_ui': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'field_mis.campaign_stats': {
            'Meta': {'object_name': 'Campaign_stats'},
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']"}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'total_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'volunteer_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        u'field_mis.community_manager': {
            'Meta': {'object_name': 'Community_manager'},
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'managed_locations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['location.Location']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'field_mis.field': {
            'Meta': {'object_name': 'Field'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mnews_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'field_mnews_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'referral_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'field_referral_ais'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"})
        },
        u'field_mis.field_stats_user_map': {
            'Meta': {'object_name': 'Field_stats_user_map'},
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'field_mis.location_stats': {
            'Meta': {'object_name': 'Location_stats'},
            'campaigns': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['campaign.Campaign']", 'null': 'True', 'symmetrical': 'False'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']"}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'total_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'volunteer_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        u'field_mis.volunteer': {
            'Meta': {'object_name': 'Volunteer'},
            'comments': ('django.db.models.fields.TextField', [], {}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['callerinfo.Contact']"}),
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'volunteer_club': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer_club']", 'null': 'True', 'blank': 'True'}),
            'volunteer_since': ('django.db.models.fields.DateField', [], {})
        },
        u'field_mis.volunteer_club': {
            'Meta': {'object_name': 'Volunteer_club'},
            'field': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_mnews_ai': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'local_volclub_mnews'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'local_referral_ais': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'local_volclub_referrals'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']"}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'field_mis.volunteer_club_stats': {
            'Meta': {'object_name': 'Volunteer_club_stats'},
            'avg_callback_calls': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'avg_callout_calls': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'club': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer_club']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_mnews_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'location_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'volunteer_contribs': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'volunteer_count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        u'field_mis.volunteer_cumulative_stats': {
            'Meta': {'object_name': 'Volunteer_cumulative_stats'},
            'avg_monthly_calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'avg_monthly_contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'avg_monthly_referrals': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'avg_monthly_successful': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referrals': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'successful': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'to_time': ('django.db.models.fields.DateTimeField', [], {}),
            'vol': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']"})
        },
        u'field_mis.volunteer_referral_stats': {
            'Meta': {'object_name': 'Volunteer_referral_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referrals': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'successful': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'vol': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']"})
        },
        u'field_mis.volunteer_stats': {
            'Meta': {'object_name': 'Volunteer_stats'},
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'vol': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['field_mis.Volunteer']"})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.image': {
            'Meta': {'object_name': 'Image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'media.image_caption_map': {
            'Meta': {'object_name': 'Image_caption_map'},
            'caption': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']"})
        }
    }

    complete_apps = ['field_mis']