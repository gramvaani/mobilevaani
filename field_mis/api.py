from django.conf.urls import url

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import Authorization

from models import Populate_volunteer_incentive_stats_status, Volunteer_stats, AddUpdate, Volunteer_community_meetings_stats, Payment_slab, Payment_slab_community_meetings, Payment_slab_good_contribs, Payment_slab_club_calls, Volunteer, Volunteer_incentive_stats, Volunteer_club_stats, Volunteer_club, Community_manager, Location_stats, Campaign_stats
from field_mis.stats import populate_volunteer_stats, populate_volunteer_stats_for_club, update_volunteer_stat, del_community_meetings, get_community_meetings_slabs, get_good_contribs_slabs, get_club_calls_slabs, del_payment_slab, get_payment_slabs, save_payment_slab, populate_volunteer_community_meetings_stats
from field_mis.tasks import PopulateVolunteerIncentiveStats
from mnews.stats import get_location_contribs
from vapp.stats.stats import TimeFrame
from mnews.models import Item_heard_stats, News, Bookmark
from callerinfo.api import ContactResource
from tastypie.utils import trailing_slash
from tastypie import fields
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from campaign.api import CampaignResource
from location.api import LocationResource

from dateutil.relativedelta import relativedelta
from datetime import *

from vapp.log import get_request_logger
logger = get_request_logger()


class VolunteerClubResource(ModelResource):
    manager = fields.CharField()
    desc = fields.CharField()
    class Meta:
        queryset = Volunteer_club.objects.all()
        resource_name = "clubs"
        ordering = [ 'name' ]

    def dehydrate_manager( self, bundle ):
        return bundle.obj.get_manager()

    def dehydrate_desc(self, bundle):
        club = bundle.obj
        loc_arr = []
        if club.location.state.name != 'Not Known':
            loc_arr.append(club.location.state.name)
        if club.location.district.name !='Not Known':
            loc_arr.append(club.location.district.name)
        if club.location.block.name != 'Not Known':
            loc_arr.append(club.location.block.name)
        if club.location.panchayat.name != 'Not Known':
            loc_arr.append(club.location.panchayat.name)
        if club.location.village != 'Not Known':
            loc_arr.append(club.location.village)
        if club.location.state.name == 'Not Known' and club.location.district.name == 'Not Known' and club.location.block.name == 'Not Known' and club.location.panchayat.name == 'Not Known' and club.location.village == 'Not Known':
            loc_arr = ['Not Known']
        return club.name + ', ' + ', '.join(loc_arr)


class VolunteerResource(ModelResource):
    club = fields.ForeignKey(VolunteerClubResource, 'volunteer_club', full = True)
    contact = fields.ForeignKey(ContactResource, 'contact', full = True)
    desc = fields.CharField()
    class Meta:
        max_limit = 0
        queryset = Volunteer.objects.all()
        resource_name = "volunteer"
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_methods = ['get']
        filtering = {
            'club': ALL_WITH_RELATIONS,
            'contact': ALL_WITH_RELATIONS
        }

    def dehydrate_desc(self, bundle):
        vol = bundle.obj
        return ( vol.contact.name + "_" + vol.contact.number )

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}
        orm_filters = super(VolunteerResource, self).build_filters( filters )
        if "club_id" in filters:
            orm_filters[ "club_id" ] = filters[ "club_id" ]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        club_id_filter = applicable_filters.pop('club_id', None)
        semi_filtered = super(VolunteerResource, self).apply_filters( request, applicable_filters )
        if club_id_filter:
            semi_filtered = [ vol for vol in semi_filtered if vol.volunteer_club.id == int(club_id_filter) ]
        return semi_filtered


class VolunteerIncentiveStatsResource(ModelResource):
    volunteer = fields.ForeignKey(VolunteerResource, 'volunteer', full = True)
    class Meta:
        queryset = Volunteer_incentive_stats.objects.all()
        resource_name = "field_mis/volunteer_incentive_stats"
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_methods = ['get']
        filtering = {
            'volunteer': ALL_WITH_RELATIONS
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_volunteer_incentive_stats' ), name = "api_get_volunteer_incentive_stats" ),
        ]

    def get_volunteer_incentive_stats(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            club_id = request.REQUEST.get('club_id')
            start_date = datetime.strptime(request.REQUEST[ 'start_date' ], '%Y-%m-%dT%H:%M:%S.%fZ')
            start_date = datetime.combine(start_date.replace(day = 1), datetime.min.time())
            page_no = int(request.REQUEST[ 'selectedPage' ])
            limit = int(request.REQUEST[ 'limit' ])
            from_index = ( page_no - 1 ) * limit
            to_index = page_no * limit
            volunteer_incentive_stats_objs = Volunteer_incentive_stats.objects.filter(month_start_date = start_date)
            if club_id is not None:
                volunteer_incentive_stats_objs = volunteer_incentive_stats_objs.filter(volunteer__volunteer_club__id = club_id)
            count = volunteer_incentive_stats_objs.count()
            volunteer_incentive_stats_objs_limited = volunteer_incentive_stats_objs [ from_index : to_index ]
            objects = []
            for volunteer_incentive_stats_obj in volunteer_incentive_stats_objs_limited:
                volunteer_incentive_stats_resource = VolunteerIncentiveStatsResource()
                volunteer_incentive_stats_bundle = volunteer_incentive_stats_resource.build_bundle( obj = volunteer_incentive_stats_obj, request = request )
                volunteer_incentive_stats_bundle = volunteer_incentive_stats_resource.full_dehydrate( volunteer_incentive_stats_bundle )
                objects.append(volunteer_incentive_stats_bundle)
            return self.create_response( request, { 'objects': objects, 'count':count } )
        except:
            logger.exception("Exception in get_volunteer_incentive_stats: %s" % request)
        return self.create_response(request, { 'error' : "unable to get volunteer incentive stats" }) 


class VolunteerCommunityMeetingsResource(ModelResource):
    volunteer = fields.ForeignKey(VolunteerResource, 'volunteer', full = True)
    class Meta:
        queryset = Volunteer_community_meetings_stats.objects.all()
        resource_name = "field_mis/comm_meetings"
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_methods = ['get', 'post']
        filtering = {
            'volunteer': ALL_WITH_RELATIONS
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_vol_community_meetings' ), name = "api_vol_comm_meetings" ),
            url(r"^(?P<resource_name>%s)/add%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'add_vol_community_meetings' ), name = "api_add_comm_meetings" ),
            url(r"^(?P<resource_name>%s)/del%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'del_vol_community_meetings' ), name = "api_del_comm_meetings" ),
        ]

    def get_vol_community_meetings(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            club_id = request.REQUEST.get('club_id')
            page_no = int(request.REQUEST.get('selectedPage'))
            limit = int(request.REQUEST.get('limit'))
            from_index = ( page_no - 1 ) * limit
            to_index = page_no * limit
            comm_meetings_objs = Volunteer_community_meetings_stats.objects.filter(volunteer__volunteer_club__id = club_id).order_by('volunteer__contact__name', 'volunteer__contact__number', '-start_date')
            count = comm_meetings_objs.count()
            comm_meetings_objs_limited = comm_meetings_objs [ from_index : to_index ]
            objects = []
            for comm_meetings_obj in comm_meetings_objs_limited:
                comm_meetings_resource = VolunteerCommunityMeetingsResource()
                comm_meetings_bundle = comm_meetings_resource.build_bundle( obj = comm_meetings_obj, request = request )
                comm_meetings_bundle = comm_meetings_resource.full_dehydrate( comm_meetings_bundle )
                objects.append(comm_meetings_bundle)
            return self.create_response( request, { 'objects': objects, 'count':count } )
        except:
            logger.exception("Exception in get_vol_community_meetings: %s" % request)
        return self.create_response(request, { 'error' : "unable to get vol community meetings list" }) 

    def add_vol_community_meetings(self, request, **kwargs):
        self.method_check(request, allowed = ['post'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            vol_id = request.REQUEST.get('vol_id')
            community_meetings = request.REQUEST.get('community_meetings')
            add_or_update = int( request.REQUEST.get('add_or_update') )
            if add_or_update == AddUpdate.ADD:
                start_date = datetime.strptime(request.REQUEST['start_date'], '%Y-%m-%dT%H:%M:%S.%fZ')
                start_date = datetime.combine(start_date.replace(day = 1), datetime.min.time())
            if add_or_update == AddUpdate.UPDATE:
                start_date = datetime.strptime(request.REQUEST['start_date'], '%Y-%m-%dT%H:%M:%S')
            user = request.user
            populate_volunteer_community_meetings_stats(vol_id, community_meetings, start_date, user)
            update_populate_volunteer_incentive_stats( Volunteer.objects.get(id = vol_id).volunteer_club )
            return self.create_response( request, { 'message': "comm meetings entry added" } )
        except:
            logger.exception("Exception in add_vol_community_meetings: %s" % request)
        return self.create_response(request, { 'error' : "unable to add community meetings entry" }) 

    def del_vol_community_meetings(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            comm_meetings_id = request.REQUEST.get('comm_meetings_id', None)
            club = Volunteer_community_meetings_stats.objects.get(id = comm_meetings_id).volunteer.volunteer_club
            del_community_meetings(comm_meetings_id)
            update_populate_volunteer_incentive_stats(club)
            return self.create_response( request, { 'message': "comm meetings entry deleted" } )
        except:
            logger.exception("Exception in del_vol_community_meetings: %s" % request)
        return self.create_response(request, { 'error' : "unable to delete community meetings entry" })


class VolunteerStatsResource(ModelResource):
    volunteer = fields.ForeignKey(VolunteerResource, 'vol', full = True)
    community_meetings_stats = fields.ForeignKey(VolunteerCommunityMeetingsResource, 'community_meetings_stats', full = True, null = True)
    class Meta:
        queryset = Volunteer_stats.objects.all()
        resource_name = "volunteer_stats"
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_methods = ['get', 'post']
        filtering = {
            'volunteer': ALL_WITH_RELATIONS
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_vol_stats' ), name = "api_get_vol_stats" ),
            url(r"^(?P<resource_name>%s)/update%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'update_vol_stat' ), name = "api_update_vol_stat" ),
        ]

    def get_vol_stats(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            club_id = request.REQUEST.get('club_id')
            vol_id = request.REQUEST.get('vol_id')
            page_no = int( request.REQUEST [ 'selectedPage' ] )
            limit = int( request.REQUEST [ 'limit' ] )
            from_index = ( page_no - 1 ) * limit
            to_index = page_no * limit
            current_month_start = TimeFrame(TimeFrame.START_OF_THE_MONTH, datetime.now()).get_timeframe()
            vol_stats_objs = Volunteer_stats.objects.exclude(start = current_month_start).order_by('vol__volunteer_club__name', 'vol__contact__name', 'vol__contact__number', '-start')
            if club_id is not None:
                vol_stats_objs = vol_stats_objs.filter(vol__volunteer_club__id = club_id)
            if vol_id is not None:
                vol_stats_objs = vol_stats_objs.filter(vol__id = vol_id)
            count = vol_stats_objs.count()
            vol_stats_objs_limited = vol_stats_objs [ from_index : to_index ]
            objects = []
            for vol_stats_obj in vol_stats_objs_limited:
                volunteer_stats_resource = VolunteerStatsResource()
                volunteer_stats_bundle = volunteer_stats_resource.build_bundle( obj = vol_stats_obj, request = request )
                volunteer_stats_bundle = volunteer_stats_resource.full_dehydrate( volunteer_stats_bundle )
                objects.append(volunteer_stats_bundle)
            return self.create_response( request, { 'objects': objects, 'count' : count } )
        except:
            logger.exception("Exception in get_vol_stats: %s" % request)
        return self.create_response(request, { 'error' : "unable to get vol stats" })

    def update_vol_stat(self, request, **kwargs):
        self.method_check(request, allowed = ['post'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            entry_id = request.REQUEST [ 'id' ]
            meeting_attended = request.REQUEST.get( 'meeting_attended' )
            if meeting_attended is not None:
                meeting_attended = bool( int( meeting_attended ) )
            volunteer_status = request.REQUEST.get( 'status' )
            if volunteer_status is not None:
                volunteer_status = bool( int( volunteer_status ) )
            user = request.user
            update_volunteer_stat(entry_id, user, meeting_attended = meeting_attended, volunteer_status = volunteer_status)
            update_populate_volunteer_incentive_stats( Volunteer_stats.objects.get(id = entry_id).vol.volunteer_club )
            return self.create_response( request, { 'message': "vol stat updated" } )
        except:
            logger.exception("Exception in update_vol_stat: %s" % request)
        return self.create_response(request, { 'error' : "unable to update vol stat" })


class PaymentSlabResource(ModelResource):
    club = fields.ForeignKey(VolunteerClubResource, 'club', full = True)
    class Meta:
        queryset = Payment_slab.objects.all()
        resource_name = "field_mis_slabs"
        filtering = {
            'club': ALL_WITH_RELATIONS
        }


class PaymentSlabCommunityMeetingsResource(ModelResource):
    payment_slab = fields.ForeignKey(PaymentSlabResource, 'payment_slab', full = True)
    class Meta:
        queryset = Payment_slab_community_meetings.objects.all()
        resource_name = "field_mis/get_comm_slabs"
        allowed_methods = ['get']
        filtering = {
            'club': ALL_WITH_RELATIONS,
            'contact': ALL_WITH_RELATIONS
        }

    def dehydrate_desc(self, bundle):
        vol = bundle.obj
        return ( vol.contact.name + "_" + vol.contact.number )

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}
        orm_filters = super(VolunteerResource, self).build_filters( filters )
        if "club_id" in filters:
            orm_filters[ "club_id" ] = filters[ "club_id" ]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        club_id_filter = applicable_filters.pop('club_id', None)
        semi_filtered = super(VolunteerResource, self).apply_filters( request, applicable_filters )
        if club_id_filter:
            semi_filtered = [ vol for vol in semi_filtered if vol.volunteer_club.id == int(club_id_filter) ]
        return semi_filtered


class PaymentSlabResource(ModelResource):
    club = fields.ForeignKey(VolunteerClubResource, 'club', full = True)
    class Meta:
        queryset = Payment_slab.objects.all()
        resource_name = "field_mis/slabs"
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_methods = ['post', 'get']
        filtering = {
            'club': ALL_WITH_RELATIONS,
            'volunteer': ALL_WITH_RELATIONS
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_club_slabs' ), name = "api_club_slabs" ),
            url(r"^(?P<resource_name>%s)/add%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'add_new_slab' ), name = "api_add_slab" ),
            url(r"^(?P<resource_name>%s)/del%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'delete_slab' ), name = "api_del_slab" ),
        ]

    def get_club_slabs(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            if 'pageNo' in request.REQUEST and 'limit' in request.REQUEST:
                page_no = int(request.REQUEST.get('pageNo'))
                limit = int(request.REQUEST.get('limit'))
                slab_from = (page_no - 1)*limit
                slab_to = page_no*limit
                if 'club_id' in request.REQUEST:
                    club_id = request.REQUEST.get('club_id')
                    slabs = get_payment_slabs(club_id)
                else:
                    slabs = Payment_slab.objects.all().order_by('club', '-id')
                count = slabs.count()
                slabsLimited = slabs[slab_from : slab_to]
            else:
                club_id = request.REQUEST.get('club_id')
                slabsLimited = get_payment_slabs(club_id)
                count = slabsLimited.count()
            objects = []
            for slab in slabsLimited:
                payment_slab_resource = PaymentSlabResource()
                payment_slab_bundle = payment_slab_resource.build_bundle( obj = slab, request = request )
                payment_slab_bundle = payment_slab_resource.full_dehydrate( payment_slab_bundle )
                objects.append(payment_slab_bundle)
            return self.create_response( request, { 'objects': objects, 'count':count } )
        except:
            logger.exception("Exception in get_club_slabs: %s" % request)
        return self.create_response(request, { 'error' : "unable to get club slabs" })

    def add_new_slab(self, request, **kwargs):
        self.method_check(request, allowed = ['post'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            club_id = request.REQUEST.get('club_id', None)
            start_date = datetime.strptime(request.REQUEST['start_date'], "%Y-%m-%dT%H:%M:%S.%fZ")
            start_date = datetime.combine(start_date.replace(day = 1), datetime.min.time())
            community_meetings_dict_list =  request.REQUEST.get('community_meetings_dict_list', None)
            good_contribs_dict_list = request.REQUEST.get('good_contribs_dict_list', None)
            club_calls_dict_list = request.REQUEST.get('club_calls_dict_list', None)
            save_payment_slab(club_id, start_date, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list)
            update_populate_volunteer_incentive_stats( Volunteer_club.objects.get(id = club_id) )
            return self.create_response( request, { 'message': "new slab added" } )
        except:
            logger.exception("Exception in add_new_slab: %s" % request)
        return self.create_response(request, { 'error' : "unable to add new slab" })

    def delete_slab(self, request, **kwargs):
        self.method_check(request, allowed = ['post'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            slab_id = request.REQUEST.get('slab_id', None)
            del_payment_slab(slab_id)
            return self.create_response( request, { 'message': "slab deleted" } )
        except:
            logger.exception("Exception in delete_slab: %s" % request)
        return self.create_response(request, { 'error' : "unable to delete" }) 


class PaymentSlabCommunityMeetingsResource(ModelResource):
    payment_slab = fields.ForeignKey(PaymentSlabResource, 'payment_slab', full = True)
    class Meta:
        queryset = Payment_slab_community_meetings.objects.all()
        resource_name = "field_mis/get_comm_slabs"
        allowed_methods = ['get']
        filtering = {
            'payment_slab': ALL_WITH_RELATIONS
        }

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}
        orm_filters = super(PaymentSlabCommunityMeetingsResource, self).build_filters( filters )
        
        if "slab_id" in filters:
            orm_filters[ "slab_id" ] = filters["slab_id"]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        slab_filter = applicable_filters.pop('slab_id', None)
        semi_filtered = super(PaymentSlabCommunityMeetingsResource, self).apply_filters( request, applicable_filters )
        if slab_filter:
            semi_filtered = [ slab for slab in semi_filtered if slab.payment_slab.id == int(slab_filter) ]
        return semi_filtered


class PaymentSlabGoodContribsResource(ModelResource):
    payment_slab = fields.ForeignKey(PaymentSlabResource, 'payment_slab', full = True)
    class Meta:
        queryset = Payment_slab_good_contribs.objects.all()
        resource_name = "field_mis/get_contrib_slabs"
        filtering = {
            'payment_slab': ALL_WITH_RELATIONS
        }

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}
        orm_filters = super(PaymentSlabGoodContribsResource, self).build_filters( filters )
        
        if "slab_id" in filters:
            orm_filters[ "slab_id" ] = filters["slab_id"]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        slab_filter = applicable_filters.pop('slab_id', None)
        semi_filtered = super(PaymentSlabGoodContribsResource, self).apply_filters( request, applicable_filters )
        if slab_filter:
            semi_filtered = [ slab for slab in semi_filtered if slab.payment_slab.id == int(slab_filter) ]
        return semi_filtered


class PaymentSlabClubCallsResource(ModelResource):
    payment_slab = fields.ForeignKey(PaymentSlabResource, 'payment_slab', full = True)
    class Meta:
        queryset = Payment_slab_club_calls.objects.all()
        resource_name = "field_mis/get_call_slabs"
        filtering = {
            'payment_slab': ALL_WITH_RELATIONS
        }

    def build_filters( self, filters = None ):
        if filters is None:
            filters = {}
        orm_filters = super(PaymentSlabClubCallsResource, self).build_filters( filters )
        
        if "slab_id" in filters:
            orm_filters[ "slab_id" ] = filters["slab_id"]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        slab_filter = applicable_filters.pop('slab_id', None)
        semi_filtered = super(PaymentSlabClubCallsResource, self).apply_filters( request, applicable_filters )
        if slab_filter:
            semi_filtered = [ slab for slab in semi_filtered if slab.payment_slab.id == int(slab_filter) ]
        return semi_filtered


class VolunteerClubStatsResource(ModelResource):
    club = fields.ForeignKey(VolunteerClubResource, 'club')
    class Meta:
        queryset = Volunteer_club_stats.objects.all()
        resource_name = "club_stats"
        filtering = {
            'club': ALL_WITH_RELATIONS,
        }

class CommunityManagerResource(ModelResource):
    locations = fields.ManyToManyField(LocationResource, attribute='managed_locations', full=True)
    class Meta:
        queryset = Community_manager.objects.all()
        resource_name = "community_manager"

class FieldLocationStatsResource(ModelResource):
    campaigns = fields.ManyToManyField(CampaignResource, attribute='campaigns', full=True)
    location = fields.ForeignKey(LocationResource, attribute='location')
    class Meta:
        queryset = Location_stats.objects.all()
        resource_name = "field_location_stats"
        filtering = {
            'location': ALL_WITH_RELATIONS,
            'start': ALL_WITH_RELATIONS,
            'end': ALL_WITH_RELATIONS,
        }

class FieldCampaignStatsResource(ModelResource):
    campaign = fields.ForeignKey(CampaignResource, attribute='campaign')
    class Meta:
        queryset = Campaign_stats.objects.all()
        resource_name = "field_campaign_stats"
        filtering = {
            'campaign': ALL_WITH_RELATIONS,
            'start': ALL_WITH_RELATIONS,
            'end': ALL_WITH_RELATIONS,
        }

def update_populate_volunteer_incentive_stats(club):
    populate_volunteer_incentive_stats_status = Populate_volunteer_incentive_stats_status.objects.filter(club = club)
    if populate_volunteer_incentive_stats_status.exists():
        populate_volunteer_incentive_stats_status_obj = populate_volunteer_incentive_stats_status[0]
        populate_volunteer_incentive_stats_status_obj.status = 'INPROGRESS'
        populate_volunteer_incentive_stats_status_obj.save()
        populate_volunteer_incentive_stats_task = PopulateVolunteerIncentiveStats()
        populate_volunteer_incentive_stats_task.run(club = club)