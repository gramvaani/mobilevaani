from models import *
from mnews.models import News, Call_stats, Bookmark, Groups_call_log, Transition_event, Item_stats
from decimal import Decimal
from campaign.models import Campaign
from vapp.stats.models import Stats_type
from app_manager.models import Cdr_trigger, Cdr, Ai_vi_start_event, Transition_event, Ai_callers_stats_weekly, Ai_callers_call_stats
from field_mis.models import Populate_volunteer_incentive_stats_status, Volunteer_incentive_stats, Volunteer_stats, Payment_slab, Volunteer_club, Payment_slab_community_meetings, Payment_slab_good_contribs, Payment_slab_club_calls, MIN_REGULAR_REPORTING_COUNT, MIN_COMMUNITY_MEETINGS_DONE
from campaign.stats import get_campaign_items, get_callers_campaign_contribs
from mnews.stats import get_contribs_avg_listening_dur, get_contribs, get_good_contribs_count, get_items_heard_count, get_location_contribs
from vapp.stats.stats import get_start_date_for_instance, get_club_calls_for_min_dur, get_daily_avg_calls, get_calls, get_call_duration, get_callers_for_ai, get_callers_for_ai_for_timeframe, TimeFrame
from sms.tasks import SMSTask
from callerinfo.models import Contact

from vapp.utils import get_total_seconds, is_start_of_week, generate_workbook, send_email, get_ratio, daterange

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

import ast

from log import get_logger
logger = get_logger()

MIN_CALL_DURATION = 150

try:
    from vapp.settings import CLUB_AI_SCHEDULE_MAP
except ImportError:
    pass

def populate_volunteer_stats():
    end_date = datetime.now()
    for club in Volunteer_club.objects.all():
        populate_volunteer_stats_for_club(club, end_date)


def populate_volunteer_stats_for_club(club, end_date = None):
    if end_date is None:
        end_date = datetime.now()
    volunteer_stats_objs = Volunteer_stats.objects.filter(vol__volunteer_club = club)
    if volunteer_stats_objs.count() == 0:
        start_date = TimeFrame(TimeFrame.START_OF_THE_MONTH, end_date).get_timeframe() - relativedelta(months = 1)
    else:
        start_date = min( volunteer_stats_objs.values_list('start', flat = True) )
    temp_date = start_date + relativedelta(months = 1)
    while temp_date < end_date:
        populate_volunteer_stats_for_club_monthly(club, start_date, temp_date)
        start_date = temp_date
        temp_date = start_date + relativedelta(months = 1)
    if temp_date >= end_date:
        populate_volunteer_stats_for_club_monthly(club, start_date, end_date)


def populate_volunteer_stats_for_club_monthly(club, start_date, end_date):
    vols = Volunteer.objects.filter(volunteer_club=club).exclude(volunteer_till__lt=start_date)
    vol_count = vols.count()
    for vol in vols:
        volunteer_stats_objs = Volunteer_stats.objects.filter(vol = vol, start = start_date)
        if volunteer_stats_objs.exists():
            volunteer_stats_obj = volunteer_stats_objs[0]
            temp_date = datetime.combine(end_date, datetime.min.time())
            if temp_date != volunteer_stats_obj.end:
                avg_calls, contribs, good_contribs = get_calls_contribs_good_contribs_count(vol, start_date, end_date, club, vols, vol_count)
                status = volunteer_stats_obj.status
                if not volunteer_stats_obj.entered_by:
                    meetings = 0
                    if volunteer_stats_obj.community_meetings_stats is not None:
                        meetings = volunteer_stats_obj.community_meetings_stats.community_meetings
                    status = get_volunteer_status(contribs.count(), meetings, volunteer_stats_obj.meeting_attended)
                volunteer_stats_obj.end = end_date
                volunteer_stats_obj.avg_calls = avg_calls
                volunteer_stats_obj.contribs = contribs.count()
                volunteer_stats_obj.contribs_avg_listening_dur = Decimal(str(get_contribs_avg_listening_dur(contribs, start_date, end_date)))
                volunteer_stats_obj.good_contribs = good_contribs
                volunteer_stats_obj.status = status
                volunteer_stats_obj.save()
        else:
            avg_calls, contribs, good_contribs = get_calls_contribs_good_contribs_count(vol, start_date, end_date, club, vols, vol_count) 
            contribs_avg_listening_dur = Decimal(str(get_contribs_avg_listening_dur(contribs, start_date, end_date)))
            vol_comm_meetings_objs = Volunteer_community_meetings_stats.objects.filter(volunteer = vol, start_date = start_date)
            if vol_comm_meetings_objs.exists():
                status = get_volunteer_status(contribs.count(), vol_comm_meetings_objs[0].community_meetings)
                volunteer_stats_obj = Volunteer_stats(vol = vol, start = start_date, end = end_date, avg_calls = avg_calls, contribs = contribs.count(), contribs_avg_listening_dur = contribs_avg_listening_dur, good_contribs = good_contribs, community_meetings_stats = vol_comm_meetings_objs[0], status = status)
            else:
                status = get_volunteer_status(contribs.count(), meetings = 0)
                volunteer_stats_obj = Volunteer_stats(vol = vol, start = start_date, end = end_date, avg_calls = avg_calls, contribs = contribs.count(), contribs_avg_listening_dur = contribs_avg_listening_dur, good_contribs = good_contribs, status = status)
            volunteer_stats_obj.save()


def get_calls_contribs_good_contribs_count(vol, start_date, end_date, club = None, vols = None, vol_count = None):
    if club is None:
        club = vol.volunteer_club
    if vols is None:
        vols = Volunteer.objects.filter(volunteer_club = club).exclude(volunteer_till__lt=start_date)
    if vol_count is None:
        vol_count = vols.count()
    club_calls = get_club_calls_for_min_dur(club.local_mnews_ai, start_date, end_date)
    club_calls_per_vol = int( round( float(club_calls) / vol_count ) )
    contribs = get_contribs(club, vol, start_date, end_date)
    good_contribs = get_good_contribs_count(club, vol, start_date, end_date, contribs)
    return club_calls_per_vol, contribs, good_contribs


def get_volunteer_status(contribs, meetings, meeting_attended = True):
    return ( contribs >= MIN_REGULAR_REPORTING_COUNT or meetings >= MIN_COMMUNITY_MEETINGS_DONE or meeting_attended )


def update_volunteer_stat(volunteer_stats_object_id, user, meeting_attended = None, volunteer_status = None):
    volunteer_stats_obj = Volunteer_stats.objects.get(id = volunteer_stats_object_id)
    if meeting_attended is not None:
        volunteer_stats_obj.meeting_attended = meeting_attended
        community_meetings = 0
        if volunteer_stats_obj.community_meetings_stats is not None:
            community_meetings = volunteer_stats_obj.community_meetings_stats.community_meetings
        volunteer_stats_obj.status = get_volunteer_status(volunteer_stats_obj.contribs, community_meetings, meeting_attended)
        volunteer_stats_obj.entered_by = user
    if volunteer_status is not None:
        volunteer_stats_obj.status = volunteer_status
        volunteer_stats_obj.entered_by = user
    volunteer_stats_obj.save()


def get_total_callers(start_date = None, end_date = None):
    club_ais = get_all_club_ais()
    all_caller_ids = []
    for club_ai in club_ais:
        all_caller_ids += get_callers_for_ai(club_ai, start_date, end_date)
    return len(set(all_caller_ids))


def set_club_callers_count():
    club_ais = get_all_club_ais()
    for club_ai in club_ais:
        all_caller_ids = get_callers_for_ai(club_ai)

        for caller_id in all_caller_ids:
            direct_calls_start_dates = list(Cdr.objects.filter(ai = club_ai, is_incoming = True, callerid = caller_id).values_list('start_time', flat = True))
            trans_calls_start_dates = list(Ai_vi_start_event.objects.exclude(prev_ai = models.F('next_ai')).filter(next_ai = club_ai, trans_event__cdr__is_incoming = True, trans_event__cdr__callerid = caller_id).values_list('trans_event__cdr__start_time', flat = True))
            caller_all_calls_start_dates = direct_calls_start_dates + trans_calls_start_dates
            total_calls_count = len(caller_all_calls_start_dates)
            caller_first_date = min(caller_all_calls_start_dates)
                  
            try:
                ai_callers_call_stats = Ai_callers_call_stats.objects.get(ai = club_ai, caller_id = caller_id, first_call_date = caller_first_date)
                if total_calls_count != Ai_callers_call_stats.call_count:
                    ai_callers_call_stats.call_count = total_calls_count
                    ai_callers_call_stats.save()
            except ObjectDoesNotExist:
                ai_callers_call_stats = Ai_callers_call_stats(ai = club_ai, caller_id = caller_id, call_count = total_calls_count, first_call_date = caller_first_date)
                ai_callers_call_stats.save()
            

def populate_club_caller_stats_weekly():
    ais = get_all_club_ais()
    end_date = datetime.now()
    for ai in ais:
        entries = Ai_callers_stats_weekly.objects.filter(ai = ai)
        if entries.exists():
            latest_end_date = max(entries.values('end_date'))['end_date']
            latest_start_date = entries.filter(end_date = latest_end_date).values('start_date')[0]['start_date']
            count_prev_table = entries.filter(end_date = latest_end_date).values('total_callers')[0]['total_callers']
            entries_exists = 0
        else:
            latest_end_date = get_start_date_for_instance(ai.id)
            entries_exists = 1
        club_specific_dict_list = get_callers_for_ai_for_timeframe(ai.id, TimeFrame.WEEKLY, latest_end_date, end_date)
        for i in range (len(club_specific_dict_list)):
            club_specific_each_entry = club_specific_dict_list[i]
            if entries_exists == 0:
                if (club_specific_each_entry['end_date'] <= (datetime.combine(latest_start_date, datetime.min.time()) + timedelta(days = 7))):
                    ai_callers_stats_weekly = Ai_callers_stats_weekly.objects.get(ai = ai, start_date = latest_start_date)
                    ai_callers_stats_weekly.end_date = club_specific_each_entry['end_date']
                    ai_callers_stats_weekly.total_callers = club_specific_each_entry['callers_count'] + count_prev_table
                    ai_callers_stats_weekly.save()
                else:
                    ai_callers_stats_weekly = Ai_callers_stats_weekly(ai = ai, start_date = club_specific_each_entry['start_date'], end_date = club_specific_each_entry['end_date'], total_callers = club_specific_each_entry['callers_count'])
                    ai_callers_stats_weekly.save()
            else:
                ai_callers_stats_weekly = Ai_callers_stats_weekly(ai = ai, start_date = club_specific_each_entry['start_date'], end_date = club_specific_each_entry['end_date'], total_callers = club_specific_each_entry['callers_count'])
                ai_callers_stats_weekly.save()


def get_all_club_ais():
    return [ club.local_mnews_ai for club in Volunteer_club.objects.all() if club.local_mnews_ai ]


def get_payment_slabs(club_id):
    return Payment_slab.objects.filter(club__id = club_id).order_by('-id')


def del_payment_slab(slab_id):
    payment_slab = Payment_slab.objects.get(id = slab_id)
    start_date_deleted_slab = payment_slab.start_date
    if payment_slab.end_date == None:
        if Payment_slab.objects.filter(club = payment_slab.club, end_date = start_date_deleted_slab).exists():
            current_slab = Payment_slab.objects.filter(club = payment_slab.club, end_date = start_date_deleted_slab)[0]
            current_slab.end_date = None
            current_slab.save()
            del_slab_for_models( payment_slab )
        else:
            del_slab_for_models( payment_slab )
    else:
        payment_slab_objects = Payment_slab.objects.filter(club = payment_slab.club, end_date = start_date_deleted_slab)
        if payment_slab_objects.count() > 0:
            payment_slab_object = payment_slab_objects[0]
            payment_slab_object.end_date = payment_slab.end_date
            payment_slab_object.save()
            del_slab_for_models( payment_slab )
        else:
            del_slab_for_models( payment_slab )
    payment_slab.delete()


def del_slab_for_models( payment_slab ):
    Payment_slab_community_meetings.objects.filter(payment_slab = payment_slab).delete()
    Payment_slab_good_contribs.objects.filter(payment_slab = payment_slab).delete()
    Payment_slab_club_calls.objects.filter(payment_slab = payment_slab).delete()


def save_payment_slab(club_id, start_date, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list):
    club = Volunteer_club.objects.get(id = club_id)
    payment_slab_objects_for_club = Payment_slab.objects.filter(club = club_id)
    if payment_slab_objects_for_club.count() > 0:
        objs_for_start_date = payment_slab_objects_for_club.filter(start_date = start_date)
        if objs_for_start_date.count() > 0:
            latest_payment_slab = objs_for_start_date[0]
            del_slab_for_models(latest_payment_slab)
            update_models_for_slab(latest_payment_slab, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list)
        else:
            latest_payment_slab = payment_slab_objects_for_club.get(end_date__isnull = True)
            latest_payment_slab.end_date = start_date
            latest_payment_slab.save()
            save_new_payment_slab(club, start_date, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list)
    else:
        save_new_payment_slab(club, start_date, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list)


def update_models_for_slab(payment_slab, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list):
    save_new_payment_slab_for_model(payment_slab, Payment_slab_community_meetings, community_meetings_dict_list)
    save_new_payment_slab_for_model(payment_slab, Payment_slab_good_contribs, good_contribs_dict_list)
    save_new_payment_slab_for_model(payment_slab, Payment_slab_club_calls, club_calls_dict_list)

            
def save_new_payment_slab(club, start_date, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list):
    new_payment_slab = Payment_slab(club = club, start_date = start_date, end_date = None)
    new_payment_slab.save()
    update_models_for_slab(new_payment_slab, community_meetings_dict_list, good_contribs_dict_list, club_calls_dict_list)


def save_new_payment_slab_for_model(slab, model_name, dict_list):
    dict_list = ast.literal_eval(dict_list)
    for i in range (len(dict_list)):
        slab_part = dict_list[i]
        model_object = model_name(payment_slab = slab, lower_limit = slab_part['lower_limit'], upper_limit = slab_part['upper_limit'], avg_incentive = slab_part['avg_incentive'], max_incentive = slab_part['max_incentive'])
        model_object.save()


def populate_volunteer_community_meetings_stats(vol_id, community_meetings, start_date, user):
    volunteer = Volunteer.objects.get( id = vol_id )
    try:
        vol_comm_meetings_stats_obj = Volunteer_community_meetings_stats.objects.get(volunteer = volunteer, start_date = start_date)
        vol_comm_meetings_stats_obj.entered_by = user
        vol_comm_meetings_stats_obj.community_meetings = community_meetings
        vol_comm_meetings_stats_obj.save()
    except ObjectDoesNotExist:
        vol_comm_meetings_stats_obj = Volunteer_community_meetings_stats(volunteer = volunteer, community_meetings = community_meetings, start_date = start_date, entered_by = user)
        vol_comm_meetings_stats_obj.save()
    update_vol_stats_for_comm_meeting_add(vol_comm_meetings_stats_obj)


def update_vol_stats_for_comm_meeting_add(comm_meetings_obj):
    try:
        volunteer_stats_obj = Volunteer_stats.objects.get(start = comm_meetings_obj.start_date, vol = comm_meetings_obj.volunteer)
        if volunteer_stats_obj.community_meetings_stats is None:
            volunteer_stats_obj.community_meetings_stats = comm_meetings_obj
            volunteer_stats_obj.status = get_volunteer_status(volunteer_stats_obj.contribs, comm_meetings_obj.community_meetings, volunteer_stats_obj.meeting_attended)
            volunteer_stats_obj.save()
    except ObjectDoesNotExist:
        pass


def del_community_meetings(comm_meetings_id):
    comm_meetings_obj = Volunteer_community_meetings_stats.objects.get(id = comm_meetings_id)
    update_vol_stats_for_comm_meeting_del(comm_meetings_obj)
    comm_meetings_obj.delete()


def update_vol_stats_for_comm_meeting_del(comm_meetings_obj):
    try:
        vol_stats_obj = Volunteer_stats.objects.get(community_meetings_stats = comm_meetings_obj)
        vol_stats_obj.community_meetings_stats = None
        community_meetings = 0
        vol_stats_obj.status = get_volunteer_status(vol_stats_obj.contribs, community_meetings, vol_stats_obj.meeting_attended)
        vol_stats_obj.save()
    except ObjectDoesNotExist:
        pass

      
def populate_volunteer_incentive_stats():
    clubs = Volunteer_club.objects.all()
    for club in clubs:
        populate_incentive_stats_for_club(club)


def populate_incentive_stats_for_club(club):
    start_date_obj = Volunteer_stats.objects.filter(vol__volunteer_club = club).values_list('start', flat = True)
    if start_date_obj:
        start_date = min(start_date_obj)
        end_date = datetime.combine(datetime.now().replace(day = 1), datetime.min.time())
        vol_objs = Volunteer.objects.filter(volunteer_club=club)
        while start_date < end_date:
            vols = vol_objs.exclude(volunteer_till__lt=start_date)
            temp_date = start_date + relativedelta(months = 1)
            slab = get_slab_for_daterange(club, start_date, temp_date)
            if slab is None:
                for vol in vols:
                    populate_incentive_stats_for_vol(vol, start_date)
            else:
                community_meetings_payment_dict_for_vols = get_final_payment_dict_for_vols_for_param(vols, slab, start_date, Payment_slab_community_meetings)
                good_contribs_payment_dict_for_vols = get_final_payment_dict_for_vols_for_param(vols, slab, start_date, Payment_slab_good_contribs)
                club_calls_payment_dict_for_vols = get_final_payment_dict_for_vols_for_param(vols, slab, start_date, Payment_slab_club_calls)
                for vol in vols:
                    populate_incentive_stats_for_vol(vol, start_date, community_meetings_payment_dict_for_vols[vol], good_contribs_payment_dict_for_vols[vol], club_calls_payment_dict_for_vols[vol])
                slab.ever_applied = True
                slab.save()
            start_date = temp_date
        populate_volunteer_incentive_stats_status_objs = Populate_volunteer_incentive_stats_status.objects.filter(club = club)
        if populate_volunteer_incentive_stats_status_objs.exists():
            populate_volunteer_incentive_stats_status_obj = populate_volunteer_incentive_stats_status_objs[0]
            populate_volunteer_incentive_stats_status_obj.status = 'POPULATED'
        else:
            populate_volunteer_incentive_stats_status_obj = Populate_volunteer_incentive_stats_status(club = club, status = 'POPULATED')
        populate_volunteer_incentive_stats_status_obj.save()
    

def populate_incentive_stats_for_vol(volunteer, start_date, amount_for_comunity_meetings = 0, amount_for_good_contribs = 0, amount_for_club_calls = 0):
    volunteer_incentive_stats_objs = Volunteer_incentive_stats.objects.filter(volunteer = volunteer, month_start_date = start_date)
    if volunteer_incentive_stats_objs.exists():
        volunteer_incentive_stats_obj = volunteer_incentive_stats_objs[0]
        volunteer_incentive_stats_obj.amount_for_comunity_meetings = amount_for_comunity_meetings
        volunteer_incentive_stats_obj.amount_for_good_contribs = amount_for_good_contribs
        volunteer_incentive_stats_obj.amount_for_club_calls = amount_for_club_calls
    else:
        volunteer_incentive_stats_obj = Volunteer_incentive_stats(volunteer = volunteer, month_start_date = start_date, amount_for_comunity_meetings = amount_for_comunity_meetings, amount_for_good_contribs = amount_for_good_contribs, amount_for_club_calls = amount_for_club_calls)
    volunteer_incentive_stats_obj.save()


def get_final_payment_dict_for_vols_for_param(vols, slab, start_date, model_for_param):
    max_amount_for_club_for_param = model_for_param.objects.filter(payment_slab = slab)[0].max_incentive
    payment_dict_for_vols_for_param = get_payment_dict_for_vols_for_param(vols, slab, start_date, model_for_param)
    payment_for_club_for_param = sum(payment_dict_for_vols_for_param.values()) 
    if (payment_for_club_for_param > max_amount_for_club_for_param):
        if model_for_param != Payment_slab_club_calls:
            for vol in vols:
                payment_dict_for_vols_for_param[ vol ] = payment_dict_for_vols_for_param[ vol ] * max_amount_for_club_for_param / payment_for_club_for_param
    return payment_dict_for_vols_for_param


def get_payment_dict_for_vols_for_param(vols, slab, start_date, model_for_param):
    payment_dict_for_param = {}
    for vol in vols:
        volunteer_stats_obj = Volunteer_stats.objects.get(vol = vol, start = start_date)
        payment_for_vol_for_param = 0
        if volunteer_stats_obj.status: #checking volunteer active status
            payment_for_vol_for_param = get_payment_for_vol(slab, volunteer_stats_obj, model_for_param, vols)
        payment_dict_for_param[ vol ] = payment_for_vol_for_param
    return payment_dict_for_param


def get_payment_for_vol(slab, volunteer_stats_obj, model_for_param, vols):
    param_count = 0
    if model_for_param == Payment_slab_community_meetings:
        model_for_param_objects = model_for_param.objects.filter(payment_slab = slab)
        if volunteer_stats_obj.community_meetings_stats:
            param_count = volunteer_stats_obj.community_meetings_stats.community_meetings
            if param_count == 0: return 0
            return (param_count*model_for_param_objects[0].avg_incentive)
        else:
            return 0
    elif model_for_param == Payment_slab_good_contribs:
        param_count = volunteer_stats_obj.good_contribs
        if param_count == 0: return 0
        model_for_param_objects = model_for_param.objects.filter(payment_slab = slab)
        for param_slab_range in model_for_param_objects:
            if param_count >= param_slab_range.lower_limit and param_count <= param_slab_range.upper_limit:
                return param_slab_range.avg_incentive
        return max(model_for_param_objects.values_list('avg_incentive', flat=True))
    elif model_for_param == Payment_slab_club_calls:
        days_in_month = (volunteer_stats_obj.end-volunteer_stats_obj.start).days
        vol_count=len(vols)
        param_count = int(round(float(volunteer_stats_obj.avg_calls)*vol_count/days_in_month))
        if param_count == 0: return 0
        model_for_param_objects = model_for_param.objects.filter(payment_slab = slab)
        for param_slab_range in model_for_param_objects:
            if param_count >= param_slab_range.lower_limit and param_count <= param_slab_range.upper_limit:
                 return int(round(float(param_slab_range.avg_incentive)/vol_count))
        max_avg_incentive = max( model_for_param_objects.values_list('avg_incentive', flat = True) )
        return int(round(float(max_avg_incentive)/vol_count))
    else: return 0  

def get_slab_for_daterange(club, start_date, end_date):
    payment_slabs_for_club = Payment_slab.objects.filter(club = club)
    if payment_slabs_for_club:
        earlier_payment_slabs_for_club = payment_slabs_for_club.filter(end_date__isnull = False)
        current_payment_slab_for_club = payment_slabs_for_club.filter(end_date__isnull = True)[0]
        for payment_slab_for_club in earlier_payment_slabs_for_club:
            if ( (payment_slab_for_club.start_date == start_date) | (payment_slab_for_club.end_date == end_date) | ( (start_date > payment_slab_for_club.start_date) & (end_date < payment_slab_for_club.end_date) ) ):
                return payment_slab_for_club
        if start_date >= current_payment_slab_for_club.start_date:
            return current_payment_slab_for_club

def get_community_meetings_slabs( slab_id ):
    slab = Payment_slab.objects.get(id = slab_id)
    return Payment_slab_community_meetings.objects.filter(payment_slab = slab)


def get_good_contribs_slabs( slab_id ):
    slab = Payment_slab.objects.get(id = slab_id)
    return Payment_slab_good_contribs.objects.filter(payment_slab = slab)


def get_club_calls_slabs( slab_id ):
    slab = Payment_slab.objects.get(id = slab_id)
    return Payment_slab_club_calls.objects.filter(payment_slab = slab)


def get_club_stats_start_date(club):
    club_stats = Volunteer_club_stats.objects.filter(club = club)
    if not club_stats.exists():
        trans_stats = Transition_event.objects.filter(ai_id = club.local_mnews_ai)
        if trans_stats.exists():
            return trans_stats.reverse().latest('id').time.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
        else:
            return None
    return club_stats.latest('id').end.replace(hour = 0, minute = 0, second = 0, microsecond = 0)


def get_location_stats_start_date(location):
    stats = Location_stats.objects.filter(location = location)
    if stats.exists():
        return stats.latest('id').end.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    else:
        return None


def populate_stats(start_date = None, end_date = None):
    fields = Field.objects.all()
    clubs = []
    volunteer_club_stats = []

    for field in fields:
        clubs += field.volunteer_club_set.all()
        for ai in field.mnews_ais.all():
            populate_field_location_stats(field, ai, start_date, end_date)

    for club in set(clubs):
        if club.local_mnews_ai:
            start_date = start_date or get_club_stats_start_date(club)
            if not start_date:
                logger.info("Start date for populating stats for club = " + str(club.name) + " not found.")
                continue

            end_date = datetime.now().replace(hour = 0, minute = 0, second = 0, microsecond = 0)

            logger.info("Populating stats for club = " + str(club.name) + " start date = " + str(start_date) + " and end date = " + str(end_date))

            for date in daterange(start_date, end_date):
                start_datetime = datetime.combine(date, datetime.min.time())
                end_datetime = datetime.combine(date + timedelta(days = 1), datetime.min.time())
                volunteer_club_stats.append(populate_voluteer_club_stats( club, start_date =  start_datetime, end_date = end_datetime ))

    return volunteer_club_stats


def populate_voluteer_club_stats( club, start_date, end_date ):
    if club.local_mnews_ai:
        volunteer_club_stats, created = Volunteer_club_stats.objects.get_or_create( club = club, start = start_date, end = end_date  )

        if created:
            local_mnews_ai = club.local_mnews_ai
            volunteer_club_stats.local_mnews_contribs = News.get_pub_arc_news().filter(ai = local_mnews_ai, time__range = (start_date, end_date)).count()
            try:
                volunteer_club_stats.avg_callback_calls = str(get_daily_avg_calls(local_mnews_ai.id, start_date, end_date, Cdr_trigger.CALLBACK))
                volunteer_club_stats.avg_callout_calls = str(get_daily_avg_calls(local_mnews_ai.id, start_date, end_date, Cdr_trigger.CALLANDPLAY))
            except Exception,e:
                volunteer_club_stats.avg_callback_calls = None
                volunteer_club_stats.avg_callout_calls = None

            vol_numbers = club.get_all_vol_numbers()
            volunteer_club_stats.volunteer_count = len(vol_numbers)

            location_contribs = get_location_contribs(club.location, start_date, end_date)
            volunteer_contribs = location_contribs.filter(callerid__in = vol_numbers)
            volunteer_club_stats.location_contribs = location_contribs.count()
            volunteer_club_stats.volunteer_contribs = volunteer_contribs.count()
            volunteer_club_stats.save()
        
        return volunteer_club_stats


def populate_field_location_stats(field, ai, start_datetime = None, end_datetime = None):
    vol_numbers = field.get_all_vol_numbers()
    districts = Location.objects.filter(ai = ai, block__name = 'Not Known').exclude(district__name = 'Not Known')

    for location in districts:
        if not (start_datetime and end_datetime):
            start_datetime = get_location_stats_start_date(location)
            if not start_datetime:
                logger.info("Start date for populating stats for location = " + str(location.id) + " not found.")
                continue
            end_datetime = datetime.now().replace(hour = 0, minute = 0, second = 0, microsecond = 0)

        for date in daterange(start_datetime, end_datetime):
            populate_daily_field_location_stats(location, vol_numbers, date)


def populate_daily_field_location_stats(location, vol_numbers, date):
    start = datetime.combine(date, datetime.min.time())
    end = datetime.combine(date + timedelta(days = 1), datetime.min.time())

    location_stats, created = Location_stats.objects.get_or_create(start = start, end = end, location = location)
    location_stats.campaigns = Campaign.objects.filter(ais__id = location.ai_id, end__gte = start, start__lte = end)
    loc_tot_contribs = get_location_contribs(location, start, end)
    location_stats.total_contribs = loc_tot_contribs.count()
    loc_vol_contribs = loc_tot_contribs.filter(callerid__in = vol_numbers)
    location_stats.volunteer_contribs = loc_vol_contribs.count()
    location_stats.save()

    for campaign in location_stats.campaigns.iterator():
        camp_item_ids = campaign.get_item_ids()
        campaign_stats, created = Campaign_stats.objects.get_or_create(start = start, end = end, campaign = campaign, location = location)
        campaign_stats.total_contribs = loc_tot_contribs.filter(id__in = camp_item_ids).count()
        campaign_stats.volunteer_contribs = loc_vol_contribs.filter(id__in = camp_item_ids).count()
        campaign_stats.save()


def email_stats(user):
    fields = Field_stats_user_map.objects.filter(user = user)

    today = datetime.combine(datetime.now().date(), datetime.min.time())

    if is_start_of_week(today):
        start = today - timedelta(days = 7)
        for field in fields:
            prepare_and_email_stats(field.id, start, today, Stats_type.WEEKLY, user.email)


def prepare_and_email_stats(field_id, start, end, stat_type, email_id):
    stats, attachment = get_field_stats(start, end, field_id)
    subject, formatted_stats = create_mail_content(stats, field_id, start, end, stat_type)
    send_email(subject, formatted_stats, [email_id], attachments = attachment)


def create_mail_content(stats, field_id, start, end, stats_type):
    subject = "Field MIS statistics for %s for period %s -> %s" % (Field.objects.get(pk = field_id).name, start.date(), end.date())
    return subject, ''


def get_field_stats(start, end, field_id):
    content_activity = get_content_activity(start, end, field_id)

    content_sheet = {
        'name': 'Content Activity',
        'data': content_activity.get_data_for_spreadsheet(),
        }

    club_activity = get_club_activity(start, end, field_id)
    club_sheet = {
        'name': 'Club Activity',
        'data': club_activity.get_data_for_spreadsheet(),
        }

    volunteer_effectiveness = get_volunteer_effectiveness(start, end, field_id)
    volunteer_sheet = {
        'name': 'Volunteer Effectiveness',
        'data': volunteer_effectiveness.get_data_for_spreadsheet(),
        }

    attachment = '/tmp/%s_field_stats_%s_%s.xlsx' % (field_id, start.date(), end.date())
    generate_workbook([content_sheet, club_sheet, volunteer_sheet], attachment)
    return None, attachment


def get_content_activity(start, end, field_id):
    field = Field.objects.get(pk = field_id)

    cont_act = Content_activity(start, end)
    for ai in field.mnews_ais.all():
        cont_act.extend(get_content_activity_for_ai(start, end, ai, field))

    return cont_act

    
class Content_activity():
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.loc_acts = []

    def extend(self, content_activity):
        self.loc_acts.extend(content_activity.loc_acts)

    def get_data_for_spreadsheet(self):
        header = self.get_header_for_spreadsheet()
        data = [header]
        for loc_act in self.loc_acts:
            data.append(loc_act.get_data_for_spreadsheet(dates = True))
        return data

    def get_header_for_spreadsheet(self):
        if self.loc_acts:
            header = ['Start', 'End']
            header.extend(self.loc_acts[0].get_header_for_spreadsheet(False))
            return header
        else:
            return []


class Location_content_activity():

    def __init__(self, location, start, end):
        self.location = location
        self.start = start
        self.end = end
        self.campaign_activities = []
        self.manager = None
        self.total_contribs = 0
        self.vol_contribs = 0

    def get_data_for_spreadsheet(self, dates = False):
        row = [self.start.date(), self.end.date()] if dates else []

        manager_name = self.manager.name if self.manager else 'N/A'
        manager_id = self.manager.id if self.manager else 'N/A'
        row.extend([self.location.id, manager_id, str(self.location) + " | " + manager_name, self.total_contribs, self.vol_contribs])

        for camp_act in self.campaign_activities:
            row.extend(camp_act.get_data_for_spreadsheet(dates = False))
        return row

    def get_header_for_spreadsheet(self, dates = False):
        header = ['Start', 'End'] if dates else []
        header.extend(['Location ID', 'Manager ID', 'Location | Manager', 'Total Contribs', 'Vol Contribs'])

        for camp_act in self.campaign_activities:
            header.extend(camp_act.get_header_for_spreadsheet(False))
        return header


class Campaign_activity():

    def __init__(self, campaign, start, end):
        self.campaign = campaign
        self.start = start
        self.end = end
        self.total_contribs = 0
        self.vol_contribs = 0


    def get_data_for_spreadsheet(self, dates = False):
        row = [self.start, self.end] if dates else []
        row.extend([self.campaign.name, self.vol_contribs, self.total_contribs])
        return row

    def get_header_for_spreadsheet(self, dates = False):
        header = ['Start', 'End'] if dates else []
        header.extend(['Campaign Name', '%s Vol Contribs' % self.campaign.name, '%s Contribs' % self.campaign.name])
        return header


def get_location_content_activity(location, field, start, end):
    activity = Location_content_activity(location, start, end)
    campaigns = Campaign.objects.filter(ais__id = location.ai_id, end__gte = start, start__lte = end)

    managers = Community_manager.objects.filter(field = field, managed_locations__id = location.id)
    if managers.exists():
        activity.manager = managers[0]

    loc_tot_contribs = get_location_contribs(location, start, end)
    activity.total_contribs = loc_tot_contribs.count()

    vol_numbers = field.get_all_vol_numbers()
    loc_vol_contribs = loc_tot_contribs.filter(callerid__in = vol_numbers)
    activity.vol_contribs = loc_vol_contribs.count()

    for campaign in campaigns:
        camp_item_ids = campaign.get_item_ids()
        camp_act = Campaign_activity(campaign, start, end)
        camp_act.total_contribs = loc_tot_contribs.filter(id__in = camp_item_ids).count()
        camp_act.vol_contribs = loc_vol_contribs.filter(id__in = camp_item_ids).count()
        activity.campaign_activities.append(camp_act)

    return activity


def get_content_activity_for_ai(start, end, ai, field):
    districts = Location.objects.filter(ai = ai, block__name = 'Not Known').exclude(district__name = 'Not Known')
    loc_acts = []
    for location in districts:
        activity = get_location_content_activity(location, field, start, end)
        loc_acts.append(activity)

    cont_act = Content_activity(start, end)
    cont_act.loc_acts = loc_acts
    return cont_act


class Field_club_activity():
    def __init__(self, start, end, field):
        self.field = field
        self.start = start
        self.end = end
        self.club_acts = []

    def get_data_for_spreadsheet(self):
        header = self.get_header_for_spreadsheet()
        data = [header]
        for club_act in self.club_acts:
            data.append(club_act.get_data_for_spreadsheet())
        return data

    def get_header_for_spreadsheet(self):
        if self.club_acts:
            return self.club_acts[0].get_header_for_spreadsheet()
        else:
            return []


class Club_activity():
    def __init__(self, start, end, club, populate = False):
        self.start = start
        self.end = end
        self.club = club
        self.vol_count = 0
        self.active_vol_count = 0
        self.mnews_contribs = {}
        self.club_avg_callback_calls = 0
        self.club_avg_callout_calls = 0
        self.dialout_calls_with_min_call_dur = 0
        self.local_mnews_contribs = 0
        if populate:
            self.populate_activity()

    def get_header_for_spreadsheet(self):
        header = ['Start', 'End', 'Location ID', 'Manager ID', 'Club ID', 'Location | Manager | Club',
                  'MV Location Contribs', 'MV Vol Contribs', 'Avg Daily Callbacks (Loc Inst)',
                  'Avg Daily Callouts (Loc Inst)', 'Daily Callouts Minimum %s seconds' % MIN_CALL_DURATION,
                  'Local Instance Contribs', 'Vol Count', 'Active Vol Count', 'Community Mobilization Index']
        return header

    def get_data_for_spreadsheet(self):
        manager = self.club.get_manager().name if self.club.get_manager() else 'N/A'
        manager_id = self.club.get_manager().id if self.club.get_manager() else 'N/A'

        data = [self.start.date(), self.end.date(), self.club.location.id, manager_id, self.club.id,
                str(self.club.location) + " | " + manager + " | " + self.club.name, self.mnews_contribs['location_contribs'],
                self.mnews_contribs['vol_contribs'], self.club_avg_callback_calls, self.club_avg_callout_calls,
                self.dialout_calls_with_min_call_dur, self.local_mnews_contribs, self.vol_count,
                self.active_vol_count, '' ]

        return data

    def get_active_vols(self):
        return []

    def populate_activity(self):
        local_mnews_ai = self.club.local_mnews_ai
        self.local_mnews_contribs = News.get_pub_arc_news().filter(ai = local_mnews_ai, time__range = (self.start, self.end)).count()
        try:
            self.club_avg_callback_calls = get_daily_avg_calls(local_mnews_ai.id, self.start, self.end, Cdr_trigger.CALLBACK)
            self.club_avg_callout_calls = get_daily_avg_calls(local_mnews_ai.id, self.start, self.end, Cdr_trigger.CALLANDPLAY)

            schedule_id = CLUB_AI_SCHEDULE_MAP.get(local_mnews_ai.id, None)
            gcl = Groups_call_log.objects.filter(group_schedule_id = schedule_id, ai_id = local_mnews_ai.id,
                    last_cdr__start_time__range = (self.start, self.end), last_cdr__answered_time__isnull = False)
            for entry in gcl:
                if get_call_duration(entry.last_cdr) >= MIN_CALL_DURATION:
                    self.dialout_calls_with_min_call_dur += 1
        except:
            self.club_avg_callback_calls = 'Not Available'
            self.club_avg_callout_calls = 'Not Available'

        vol_numbers = self.club.get_all_vol_numbers()

        self.vol_count = len(vol_numbers)
        self.active_vol_count = len(self.get_active_vols())

        location_contribs = get_location_contribs(self.club.location, self.start, self.end)
        vol_contribs = location_contribs.filter(callerid__in = vol_numbers)
        self.mnews_contribs = {
            'location_contribs': location_contribs.count(),
            'vol_contribs': vol_contribs.count(),
            }


def get_club_activity(start, end, field_id):
    field = Field.objects.get(pk = field_id)
    field_club_act = Field_club_activity(start, end, field)

    clubs = field.volunteer_club_set.all()
    for club in clubs:
        club_act = Club_activity(start, end, club, populate = True)
        field_club_act.club_acts.append(club_act)

    return field_club_act


class Field_vol_effectiveness():
    def __init__(self, start, end, field):
        self.start = start
        self.end = end
        self.field = field
        self.vol_effs = []

    def get_header_for_spreadsheet(self):
        if self.vol_effs:
            return self.vol_effs[0].get_header_for_spreadsheet()
        else:
            return []

    def get_data_for_spreadsheet(self):
        header = self.get_header_for_spreadsheet()
        data = [header]

        for vol_eff in self.vol_effs:
            data.append(vol_eff.get_data_for_spreadsheet())
        return data


class Vol_effectiveness():
    def __init__(self, start, end, vol, populate = True):
        self.start = start
        self.end = end
        self.vol = vol

        self.contrib_count = 0
        self.total_tr_contrib = 0
        self.tr_contrib_count = 0
        self.campaign_contribs = 0

        self.contrib_heard_count = 0
        self.contrib_count_min_fifteen_secs_heard = 0

        self.contrib_count_min_two_comments = 0
        self.contrib_count_atleast_one_like = 0

        self.calls_count_min_listening_time = 0

        if populate:
            self.populate_effectiveness()


    def populate_effectiveness(self):
        try:
            number = self.vol.contact.number
            mnews_ai_ids = list(self.vol.field.mnews_ais.all().values_list('id', flat = True))
            local_mnews_ai_id = []
            if self.vol.volunteer_club and self.vol.volunteer_club.local_mnews_ai:
                club_ai_id = self.vol.volunteer_club.local_mnews_ai.id
                local_mnews_ai_id.append(club_ai_id)
                calls = get_calls(club_ai_id, self.start, self.end, [number])
                for call in calls:
                    if get_call_duration(call) >= MIN_CALL_DURATION:
                        self.calls_count_min_listening_time += 1

            ai_ids = mnews_ai_ids + local_mnews_ai_id
            news = News.get_pub_arc_news().filter(ai_id__in = ai_ids, callerid = number)

            self.contrib_count = news.filter(time__range = (self.start, self.end)).count()
            self.total_tr_contrib = news.filter(tags__contains = 'TR').count()
            self.tr_contrib_count = news.filter(time__range = (self.start, self.end), tags__contains = 'TR').count()

            self.campaign_contribs = get_callers_campaign_contribs(ai_ids, number, self.start, self.end)

            self.contrib_heard_count = get_items_heard_count(news, self.start, self.end)
            self.contrib_count_min_fifteen_secs_heard = get_items_heard_count(news, self.start, self.end, 15)

            for each in news.filter(time__range = (self.start, self.end)):
                if each.num_pub_arc_comments() >= 2:
                    self.contrib_count_min_two_comments += 1
                if Bookmark.objects.filter(news = each).exclude(callerid = number).count() >= 1:
                    self.contrib_count_atleast_one_like += 1
        except Exception, e:
            logger.exception("Unable to populate data for volunteer effectiveness. " + str(e))


    def get_header_for_spreadsheet(self):
        return ['Name', 'Number', 'Volunteer Since', 'Location', 'Club', 'Start', 'End', 'Total Contribs',
                'TR Contribs', 'Cumulative TR Contribs', 'Campaign Contribs', 'Contribs Heard',
                'Contribs Heard (Minimum 15 secs)', 'Contribs (Minimum 2 comments)', 'Contribs (Minimum 1 Like)',
                'Calls (Minimum 2.5 minutes duration)']


    def get_data_for_spreadsheet(self):
        contact = self.vol.contact
        club_name = self.vol.volunteer_club.name if self.vol.volunteer_club else '-'
        return [contact.name, contact.number, self.vol.volunteer_since, str(contact.location), club_name,
                self.start.date(), self.end.date(), self.contrib_count, self.tr_contrib_count, self.total_tr_contrib,
                self.campaign_contribs, self.contrib_heard_count, self.contrib_count_min_fifteen_secs_heard,
                self.contrib_count_min_two_comments, self.contrib_count_atleast_one_like, self.calls_count_min_listening_time]


def get_volunteer_effectiveness(start, end, field_id):
    field = Field.objects.get(pk = field_id)
    field_vol_eff = Field_vol_effectiveness(start, end, field)

    for vol in field.volunteer_set.all():
        vol_eff = Vol_effectiveness(start, end, vol, populate = True)
        field_vol_eff.vol_effs.append(vol_eff)

    return field_vol_eff

def get_formatted_stats_message( volunteer, sms_template ):
    today = date.today()
    yesterday = today - timedelta( days = 1 )
    club = volunteer.volunteer_club
    club_stats = Volunteer_club_stats.objects.get( club = club, start = yesterday, end = today )
    item_stats = Item_stats.objects.get( ai = club.local_mnews_ai, start = yesterday, end = today )
    message_args = { 'date': yesterday.strftime('%d-%m-%Y'), 'club': club.name, 'club_contribs': item_stats.pub_items, 'callback_calls': str(club_stats.avg_callback_calls) }
    message = sms_template.process_template(message_args)
    return message

def send_notification_stats_message( contact ):
    try:
        volunteer = contact.volunteer_set.all()[0]
        if volunteer.volunteer_club and volunteer.volunteer_club.local_mnews_ai.status:
            stats_setting = Stats_sms_template_settings.objects.get( clubs = volunteer.volunteer_club, sms_template__name = CLUB_STATS )
            message = get_formatted_stats_message( volunteer, stats_setting.sms_template  )
            sms_message = SMSTask.create_send_msg( contact.location_fk.ai.id, message, contact.number )
            vol_group_sms_log = Volunteer_sms_log(volunteer = volunteer, sms_message = sms_message)
            vol_group_sms_log.save()
    except Exception, e:
        logger.exception("Exception encountered in send_notification_stats_message - %s." % str(e))

def send_notification_group_sms():
    for notification_group_setting in Notification_group_settings.objects.all():
        contacts = Contact.objects.filter( contact_list = notification_group_setting.notification_group ).distinct()
        for contact in contacts:
            send_notification_stats_message( contact )