from models import *
from sms.tasks import SMSTask
from sms.utils import *
from telephony.utils import get_formatted_number

import re
from django.core.exceptions import *

from log import get_request_logger
logger = get_request_logger()


# just extract the number, leave everything else. Last resort.
def salvage_number(message):
    number = None
    if message:
        matches = re.findall(r"\d{10,12}", message)
        if matches:
            number = matches[0]
    return number 

def get_welcome_msg(ai):
    try:
        return Referral_sms_response.objects.get(ai = ai, response_code = 'WL')
    except:
        return None


def get_number_name_location(message, app_name):
    modified_message = remove_app_name(message, app_name)
    message_list = split_message(modified_message, separators=r'[,.;]+')
    message_list = [ m.strip().replace(",.", "") for m in message_list ]

    if len(message_list) < 3:
        number = get_formatted_number(salvage_number(message))
        return number, None, None
    else:
        number = get_formatted_number(message_list[0])
        if len(number) > 10:
            return number, message_list[1], message_list[2]
        else:
            return (None,) * 3
    

def in_handler(ai, app_name, referring_number, message):
    response = None    
    
    referred_number, name, location = get_number_name_location(message, app_name)
    if not referred_number or referred_number == referring_number:
        response = Referral_sms_response.objects.get(ai = ai, response_code = 'FL')
        Referral_failure_log.add_failure_log(ai, Referral_failure_log.MFM, message = message, referrer_number = referring_number )
        return response.message

    properties = Refer_ai_properties.objects.get(ai = ai)
    referral, failure_code = Referral.create(ai, referring_number, referred_number)

    if ( not failure_code ) and (name or location):
        profile = Referred_contact_profile(referral = referral, name = name, location = location)
        profile.save()     

    if failure_code and not properties.allow_duplicate:
        logger.info('duplicate as properties.allow_duplicate is false')
        response = Referral_sms_response.objects.get(ai = ai, response_code = 'DP')
        Referral_failure_log.add_failure_log(ai, Referral_failure_log.DRN, message = message, referrer_number = referring_number )
   
    else:
        welcome_msg = get_welcome_msg(ai) 
        if welcome_msg:
            SMSTask.create_send_msg(ai.id, welcome_msg.message, referral.referred_contact)            
        referral.schedule_referral_call()
        response = Referral_sms_response.objects.get(ai = ai, response_code = 'SC')
            
    return response.message
    
    
    
