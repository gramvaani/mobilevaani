from app_manager.common import get_queue_for_task
from app_manager.models import App_instance, Cdr, CalleridField
from mnews.models import Transient_group_schedule, Groups_call_log

from mnews.tasks import CallAndPlayTask

from django.db.models.signals import post_save
from django.core.exceptions import *
from django.db import models

from datetime import datetime

REFER_MEDIA_DIR = 'refer'

class Referral_status(object):
    SC = 'SC'
    PR = 'PR'
    FL = 'FL'        

class Referral(models.Model):

    STATUS_CHOICES = (
            (Referral_status.PR, 'Processing'),
            (Referral_status.SC, 'Successful'),
            (Referral_status.FL, 'Failure'),
        )

    ai = models.ForeignKey(App_instance)
    referring_contact = CalleridField(max_length = 20)
    referred_contact = CalleridField(max_length = 20)
    time_added = models.DateTimeField(auto_now_add = True)
    source = models.CharField(max_length = 32, default='sms')
    cdr = models.ForeignKey(Cdr, null = True)
    status = models.CharField(max_length = 2, choices = STATUS_CHOICES)

    def save(self, *args, **kwargs):
        if not kwargs.pop('skip_lastupdatetime', False):
            self.time_added = datetime.now()
        super(Referral, self).save(*args, **kwargs)
    
    def schedule_referral_call( self, eta = None ):
        eta = (eta if eta else datetime.now())
        properties = Refer_ai_properties.objects.get(ai = self.ai)
        if self.referred_contact and properties and properties.schedule and properties.push_ai:
            ai = properties.push_ai
            group_call_log = Groups_call_log(number = self.referred_contact, ai = ai, group_schedule = properties.schedule)
            group_call_log.save()
            refer_group_call_log = Referral_group_call_log( referral = self, group_call_log = group_call_log)
            refer_group_call_log.save()
            queue = get_queue_for_task(ai, default_queue='push')
            CallAndPlayTask.apply_async([group_call_log.id, properties.max_refer_tries], 
                                        eta=eta, queue=queue)
            
    @classmethod
    def create(cls, ai, referrer, referred, cdr = None, source = 'sms'):

        failure_code = False
        properties = Refer_ai_properties.objects.get(ai = ai)  

        referral = Referral.objects.create(ai = ai, referring_contact = referrer, referred_contact = referred, source = source, cdr = cdr)
        prev_referral_num = Referral.objects.filter(ai = ai, referred_contact = referred).count()

        if (( prev_referral_num == 1 ) or ( prev_referral_num > 1 and properties.allow_duplicate )):
            referral.status = Referral_status.PR
        else:
            referral.status = Referral_status.FL
            failure_code = True

        referral.save()                  

        return ( referral, failure_code )

    
class Refer_ai_properties(models.Model):
    
    # more properties maybe added to the same Ai
    ai = models.ForeignKey(App_instance, related_name = 'refer_ai')
    allow_duplicate = models.BooleanField()
    amount_per_referral = models.DecimalField(max_digits = 10, decimal_places = 2)
    pay_for_dups = models.BooleanField()   
    push_ai = models.ForeignKey(App_instance, null = True, blank = True, related_name = 'push_ai') 
    schedule = models.ForeignKey(Transient_group_schedule, null = True, blank = True) 
    max_refer_tries = models.PositiveSmallIntegerField(default = 3)   
    
    def __unicode__(self):
        return str(self.ai) + '_' + str(self.allow_duplicate)
    

class Referral_group_call_log(models.Model):    
    
    referral = models.ForeignKey(Referral)
    group_call_log = models.ForeignKey(Groups_call_log)
    creation_time = models.DateTimeField(auto_now_add = True)
    
class Referral_sms_response(models.Model):
    
    RESPONSE_CODES = (
    ("SC","Successful"),
    ("FL","Failed"),
    ("DP","Duplicate"),
    ("RT","Retry"),
    ("WL","Welcome"),
    )
    
    ai = models.ForeignKey(App_instance)
    message = models.TextField()
    response_code = models.CharField(max_length = 2, choices = RESPONSE_CODES)
    
    def __unicode__(self):
        return str(self.ai) + '_' + str(self.response_code)
    
    
class Referred_contact_profile(models.Model):
    
    referral = models.ForeignKey(Referral)
    name = models.CharField(max_length = 50, null = True, blank = True)
    location = models.CharField(max_length = 50, null = True, blank = True)
    age = models.PositiveSmallIntegerField(null = True, blank = True)
    occupation = models.CharField(max_length = 50, null = True, blank = True)
    
    
class Referred_call_log(models.Model):
     
    referral = models.ForeignKey(Referral)
    last_cdr = models.ForeignKey(Cdr, null = True)
    success = models.BooleanField(default = False)
    ai_played = models.ManyToManyField(App_instance, null = True)
    tries = models.PositiveSmallIntegerField(default = 0)
  

class Referral_failure_log(models.Model):
    
    MFM = "MFM"
    RCF = "RCF"
    CIF = "CIF"
    RNI = "RNI"
    DRN = "DRN"
    SRM = "SRM"
    ITN = "ITN"
    
    
    REFERRAL_FAILURE_CODES = (
                              (MFM, "Malformed Message"),
                              (RCF, "Referral Call Failed"),
                              (CIF, "Calling Infra Failed"),
                              (RNI, "Referred Number Invalid"),
                              (DRN, "Duplicate Referred Number"),
                              (SRM, "Self Referral Made"), 
                              (ITN, "Invalid Typed Number"),                              
                              )
    
    refer_ai = models.ForeignKey(App_instance)
    referring_contact = CalleridField(max_length = 20, null = True)
    referral = models.ForeignKey(Referral, null = True, blank = True)
    message = models.TextField(null = True, blank = True)
    time_added = models.DateTimeField(default = datetime.now)
    refer_failure_code = models.CharField(max_length = 3, choices = REFERRAL_FAILURE_CODES)
    
    @classmethod
    def is_valid_failure_code(cls, code_passed ):
        return (code_passed in set([code[0] for code in cls.REFERRAL_FAILURE_CODES]))
        
    @classmethod
    def add_failure_log(cls, ai, refer_failure_code, message = None, referral = None, referrer_number = None ):        
        
        if (isinstance(ai, App_instance) and ((referrer_number and referrer_number.isdigit()) or (referral and isinstance(referral, Referral))) and cls.is_valid_failure_code(refer_failure_code)):           
            rf_log = Referral_failure_log(refer_ai = ai, referring_contact = referrer_number, referral = referral, message = message, refer_failure_code = refer_failure_code )
            if referral and not referrer_number:
                rf_log.referring_contact = referral.referring_contact           
            rf_log.save()
            return rf_log
        else:            
            raise Exception("invalid parameters for add_failure_log. expected: ai, refer_failure_code, message, referral, referrer_number")


def refer_gcl_post_save_handler(sender, **kwargs):
    
    gcl = kwargs['instance']    
    if not kwargs['created']:
        try:
            rgcl = Referral_group_call_log.objects.get(group_call_log = gcl)
            properties = Refer_ai_properties.objects.get(ai = rgcl.referral.ai)
    
            if  gcl.tries >= properties.max_refer_tries and gcl.success == False: 
                rgcl.referral.status = Referral_status.FL 
                rgcl.referral.save()          
                Referral_failure_log.add_failure_log(properties.ai, Referral_failure_log.RCF, referral = rgcl.referral)            
            elif gcl.success == True:
                rgcl.referral.status = Referral_status.SC
                rgcl.referral.save()
        except Exception,e:
            pass

post_save.connect(refer_gcl_post_save_handler, sender = Groups_call_log, dispatch_uid = 'refer.groups_call_log.save')
        
