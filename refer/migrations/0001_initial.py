# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Referral'
        db.create_table(u'refer_referral', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('referring_contact', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('referred_contact', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('time_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('source', self.gf('django.db.models.fields.CharField')(default='sms', max_length=32)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'], null=True)),
        ))
        db.send_create_signal(u'refer', ['Referral'])

        # Adding model 'Refer_ai_properties'
        db.create_table(u'refer_refer_ai_properties', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('allow_duplicate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('amount_per_referral', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('pay_for_dups', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'refer', ['Refer_ai_properties'])

        # Adding model 'Referral_sms_response'
        db.create_table(u'refer_referral_sms_response', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('response_code', self.gf('django.db.models.fields.CharField')(max_length=2)),
        ))
        db.send_create_signal(u'refer', ['Referral_sms_response'])

        # Adding model 'Ai_ordered_chain'
        db.create_table(u'refer_ai_ordered_chain', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('referral_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='referral_ai', to=orm['app_manager.App_instance'])),
            ('linked_ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='linked_ai', to=orm['app_manager.App_instance'])),
            ('order', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('introduction_prompt', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'refer', ['Ai_ordered_chain'])

        # Adding model 'Referred_contact_profile'
        db.create_table(u'refer_referred_contact_profile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('referral', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['refer.Referral'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('age', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('occupation', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'refer', ['Referred_contact_profile'])

        # Adding model 'Referred_call_log'
        db.create_table(u'refer_referred_call_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('referral', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['refer.Referral'])),
            ('last_cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'], null=True)),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tries', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'refer', ['Referred_call_log'])

        # Adding M2M table for field ai_played on 'Referred_call_log'
        m2m_table_name = db.shorten_name(u'refer_referred_call_log_ai_played')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('referred_call_log', models.ForeignKey(orm[u'refer.referred_call_log'], null=False)),
            ('app_instance', models.ForeignKey(orm[u'app_manager.app_instance'], null=False))
        ))
        db.create_unique(m2m_table_name, ['referred_call_log_id', 'app_instance_id'])

        # Adding model 'Mob_credit_api_credential'
        db.create_table(u'refer_mob_credit_api_credential', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'refer', ['Mob_credit_api_credential'])

        # Adding model 'Mob_credit_creds_ai_map'
        db.create_table(u'refer_mob_credit_creds_ai_map', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('creds', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['refer.Mob_credit_api_credential'])),
        ))
        db.send_create_signal(u'refer', ['Mob_credit_creds_ai_map'])

        # Adding model 'Mob_cred_referrer_log'
        db.create_table(u'refer_mob_cred_referrer_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True, blank=True)),
            ('mbl_number', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('refs_from_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('refs_to_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('cred_amount', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'refer', ['Mob_cred_referrer_log'])

        # Adding model 'Mob_cred_recharge_log'
        db.create_table(u'refer_mob_cred_recharge_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True, blank=True)),
            ('mbl_number', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('recharge_amount', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2)),
            ('pending_amount', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=10, decimal_places=2)),
            ('payment_done', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('refs_from_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('refs_to_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('last_recharge_attempt_date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('recharge_tries', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'refer', ['Mob_cred_recharge_log'])

        # Adding model 'Mob_cred_req_resp_log'
        db.create_table(u'refer_mob_cred_req_resp_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('recharge_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['refer.Mob_cred_recharge_log'], null=True, blank=True)),
            ('recharge_amount_done', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('req_url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('resp_code', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('transaction_id', self.gf('django.db.models.fields.CharField')(max_length=36, null=True, blank=True)),
        ))
        db.send_create_signal(u'refer', ['Mob_cred_req_resp_log'])

        # Adding model 'Mob_credit_operator_info'
        db.create_table(u'refer_mob_credit_operator_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mbl_number', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('operator_name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('operator_code', self.gf('django.db.models.fields.CharField')(max_length=3)),
        ))
        db.send_create_signal(u'refer', ['Mob_credit_operator_info'])


    def backwards(self, orm):
        # Deleting model 'Referral'
        db.delete_table(u'refer_referral')

        # Deleting model 'Refer_ai_properties'
        db.delete_table(u'refer_refer_ai_properties')

        # Deleting model 'Referral_sms_response'
        db.delete_table(u'refer_referral_sms_response')

        # Deleting model 'Ai_ordered_chain'
        db.delete_table(u'refer_ai_ordered_chain')

        # Deleting model 'Referred_contact_profile'
        db.delete_table(u'refer_referred_contact_profile')

        # Deleting model 'Referred_call_log'
        db.delete_table(u'refer_referred_call_log')

        # Removing M2M table for field ai_played on 'Referred_call_log'
        db.delete_table(db.shorten_name(u'refer_referred_call_log_ai_played'))

        # Deleting model 'Mob_credit_api_credential'
        db.delete_table(u'refer_mob_credit_api_credential')

        # Deleting model 'Mob_credit_creds_ai_map'
        db.delete_table(u'refer_mob_credit_creds_ai_map')

        # Deleting model 'Mob_cred_referrer_log'
        db.delete_table(u'refer_mob_cred_referrer_log')

        # Deleting model 'Mob_cred_recharge_log'
        db.delete_table(u'refer_mob_cred_recharge_log')

        # Deleting model 'Mob_cred_req_resp_log'
        db.delete_table(u'refer_mob_cred_req_resp_log')

        # Deleting model 'Mob_credit_operator_info'
        db.delete_table(u'refer_mob_credit_operator_info')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'refer.ai_ordered_chain': {
            'Meta': {'object_name': 'Ai_ordered_chain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'introduction_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'linked_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'linked_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'referral_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'referral_ai'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'refer.mob_cred_recharge_log': {
            'Meta': {'object_name': 'Mob_cred_recharge_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_recharge_attempt_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'mbl_number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'payment_done': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pending_amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'recharge_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'recharge_tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'refs_from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'refs_to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'refer.mob_cred_referrer_log': {
            'Meta': {'object_name': 'Mob_cred_referrer_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'cred_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mbl_number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'refs_from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'refs_to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'refer.mob_cred_req_resp_log': {
            'Meta': {'object_name': 'Mob_cred_req_resp_log'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'recharge_amount_done': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'recharge_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Mob_cred_recharge_log']", 'null': 'True', 'blank': 'True'}),
            'req_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'resp_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '36', 'null': 'True', 'blank': 'True'})
        },
        u'refer.mob_credit_api_credential': {
            'Meta': {'object_name': 'Mob_credit_api_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'refer.mob_credit_creds_ai_map': {
            'Meta': {'object_name': 'Mob_credit_creds_ai_map'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'creds': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Mob_credit_api_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'refer.mob_credit_operator_info': {
            'Meta': {'object_name': 'Mob_credit_operator_info'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mbl_number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'operator_code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'operator_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'refer.refer_ai_properties': {
            'Meta': {'object_name': 'Refer_ai_properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'allow_duplicate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'amount_per_referral': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pay_for_dups': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'refer.referral': {
            'Meta': {'object_name': 'Referral'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referred_contact': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'referring_contact': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'source': ('django.db.models.fields.CharField', [], {'default': "'sms'", 'max_length': '32'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'refer.referral_sms_response': {
            'Meta': {'object_name': 'Referral_sms_response'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'response_code': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'refer.referred_call_log': {
            'Meta': {'object_name': 'Referred_call_log'},
            'ai_played': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'referral': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Referral']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'refer.referred_contact_profile': {
            'Meta': {'object_name': 'Referred_contact_profile'},
            'age': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'occupation': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'referral': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Referral']"})
        }
    }

    complete_apps = ['refer']