# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Referral_group_call_log.ai'
        db.delete_column(u'refer_referral_group_call_log', 'ai_id')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Referral_group_call_log.ai'
        raise RuntimeError("Cannot reverse this migration. 'Referral_group_call_log.ai' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Referral_group_call_log.ai'
        db.add_column(u'refer_referral_group_call_log', 'ai',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance']),
                      keep_default=False)


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'mnews.ai_transient_group': {
            'Meta': {'object_name': 'Ai_transient_group'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mnews.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'mnews.groups_call_log': {
            'Meta': {'object_name': 'Groups_call_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'group_schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'mnews.transient_group': {
            'Meta': {'object_name': 'Transient_group'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'generating_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mnews.transient_group_schedule': {
            'Meta': {'object_name': 'Transient_group_schedule'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ai_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Ai_transient_group']"}),
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mnews.Days']", 'symmetrical': 'False'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'onpush_sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']", 'null': 'True', 'blank': 'True'}),
            'play_ai': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prompt_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'repeat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'refer.ai_ordered_chain': {
            'Meta': {'object_name': 'Ai_ordered_chain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'introduction_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'linked_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'linked_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'referral_ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'referral_ai'", 'to': u"orm['app_manager.App_instance']"})
        },
        u'refer.mob_cred_recharge_log': {
            'Meta': {'object_name': 'Mob_cred_recharge_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_recharge_attempt_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'mbl_number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'payment_done': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pending_amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'recharge_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'recharge_tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'refs_from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'refs_to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'refer.mob_cred_referrer_log': {
            'Meta': {'object_name': 'Mob_cred_referrer_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'cred_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mbl_number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'refs_from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'refs_to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'refer.mob_cred_req_resp_log': {
            'Meta': {'object_name': 'Mob_cred_req_resp_log'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'recharge_amount_done': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'recharge_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Mob_cred_recharge_log']", 'null': 'True', 'blank': 'True'}),
            'req_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'resp_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '36', 'null': 'True', 'blank': 'True'})
        },
        u'refer.mob_credit_api_credential': {
            'Meta': {'object_name': 'Mob_credit_api_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'refer.mob_credit_creds_ai_map': {
            'Meta': {'object_name': 'Mob_credit_creds_ai_map'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'creds': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Mob_credit_api_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'refer.mob_credit_operator_info': {
            'Meta': {'object_name': 'Mob_credit_operator_info'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mbl_number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'operator_code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'operator_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'refer.refer_ai_properties': {
            'Meta': {'object_name': 'Refer_ai_properties'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'refer_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'allow_duplicate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'amount_per_referral': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pay_for_dups': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'push_ai': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'push_ai'", 'null': 'True', 'to': u"orm['app_manager.App_instance']"}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']", 'null': 'True', 'blank': 'True'})
        },
        u'refer.referral': {
            'Meta': {'object_name': 'Referral'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referred_contact': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'referring_contact': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'source': ('django.db.models.fields.CharField', [], {'default': "'sms'", 'max_length': '32'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'refer.referral_group_call_log': {
            'Meta': {'object_name': 'Referral_group_call_log'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'group_call_log': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Groups_call_log']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referral': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Referral']"})
        },
        u'refer.referral_sms_response': {
            'Meta': {'object_name': 'Referral_sms_response'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'response_code': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'refer.referred_call_log': {
            'Meta': {'object_name': 'Referred_call_log'},
            'ai_played': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'referral': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Referral']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'refer.referred_contact_profile': {
            'Meta': {'object_name': 'Referred_contact_profile'},
            'age': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'occupation': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'referral': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['refer.Referral']"})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['refer']