from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication

from refer.models import Referral

from vapp.app_manager.api import AppInstanceResource
from vapp.api import AppInstanceAuthorization

from vapp.log import get_request_logger
logger = get_request_logger()


class ReferralResource(ModelResource):
    ai = fields.ForeignKey(AppInstanceResource, 'ai')

    class Meta:
        queryset = Referral.objects.all()
        resource_name = "referral"
        allowed_methods = ['get']
        fields = ['referring_contact', 'referred_contact', 'time_added', 'source']
        filtering = {
            'ai':         ALL_WITH_RELATIONS,
            'time_added': ['gte', 'lte', 'range'],
            'id':         ['exact'],
        }
        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()