from celery.task import Task

from app_manager.models import App_instance
from mnews.models import Event_sms_template
from refer.stats import get_referrers_success_stats
from sms.tasks import SMSTask

from vapp.utils import get_start_of_week, get_start_of_month

from decimal import Decimal
from datetime import datetime, timedelta

from log import get_request_logger
logger = get_request_logger()

MIN_AMOUNT_FOR_RECHARGE = 100.00
MIN_AMOUNT_FOR_RECHARGE = Decimal(str(MIN_AMOUNT_FOR_RECHARGE))

zero_decimal_val = Decimal(str(0.00))


class SendReferralSuccessReportTask(Task):  
    
    referral_score_event = 'REFERRAL_SCORE_EVENT'
    
    def run(self, ai_ids, stats_type, message_template = None):
        
        if stats_type == 'DAILY':
            end = datetime.combine(datetime.now().date(), datetime.min.time())
            start = end - timedelta(days = 1)
        elif stats_type == 'WEEKLY':
            today = datetime.combine(datetime.now().date(), datetime.min.time())
            end = get_start_of_week(today)
            start = end - timedelta(days = 7)
        elif stats_type == 'MONTHLY':
            today = datetime.combine(datetime.now().date(), datetime.min.time())
            end = get_start_of_month(today)
            start = get_start_of_month(end - timedelta(days = 1))

        self.send_referral_success_messages(ai_ids, start, end, message_template)


    def send_referral_success_messages(self, ai_ids, start, end, message_template):
        for ai_id in ai_ids:
            ai = App_instance.objects.get(pk = ai_id)
            message_template = message_template or Event_sms_template.objects.get(template__ai = ai, event = self.referral_score_event).template.message
            
            referral_stats = get_referrers_success_stats(ai, start, end)
            
            for contact in referral_stats:
                stat = {
                    'referring_contact': contact,
                    'success_count': referral_stats[contact]['success_count'],
                    'failure_count': referral_stats[contact]['failure_count'],
                }
                message = message_template.format( **stat )
                SMSTask.create_send_msg(ai_id, message, contact)

