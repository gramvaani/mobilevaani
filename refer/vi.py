from vapp.telephony.statemachine import BaseVappController
from vapp.log import get_logger
from vapp.app_manager.models import CalleridField
from vapp.telephony.utils import get_formatted_number

from sms.tasks import SMSTask
from datetime import datetime

from sms_handler import get_welcome_msg
from models import Referral, Refer_ai_properties, Referral_failure_log

logger = get_logger()

class ReferController( BaseVappController ):
    def __init__(self, ai, sessionData, vi_data):
        super( ReferController, self ).__init__( ai, sessionData )
        self.sessionData = sessionData

    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid )

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def pre_welcome(self):
        return self.getPromptParams( 'refer_welcome' )

    def pre_continue(self):
        return self.getPromptPlayAndGetParams( 'refer_continue', timeout = 10000, tries = 3, errorPromptName = 'refer_invalid_choice' )

    def pre_thank_you(self):
        return self.getPromptParams( 'refer_thank_you' )

    def pre_capture_number(self):
        return self.getPromptPlayAndGetParams( 'refer_input_number', timeout = 10000, tries = 3, minDigits = 10, maxDigits = 10, errorPromptName = 'refer_invalid_value' )

    def while_capture_number__sm_action_success(self, events, eventData):
        digits = str( self.getPlayAndGetDigits( eventData ) )
        if CalleridField.is_valid_callerid(digits):
            properties = Refer_ai_properties.objects.get(ai = self.ai)
            referred = get_formatted_number(digits)
            referral, failure_code = Referral.create(self.ai, self.sessionData.callerid, referred, cdr = self.sessionData.cdrs[0], source = 'call')
         
            if failure_code and not properties.allow_duplicate:
                Referral_failure_log.add_failure_log(self.ai, Referral_failure_log.DRN, referrer_number = self.sessionData.callerid, referral = referral)
                return 'ref_duplicate_number'
            else:
                referral.schedule_referral_call()
                welcome_msg = get_welcome_msg(self.ai) 
                if welcome_msg:
                    SMSTask.create_send_msg(self.ai.id, welcome_msg.message, referral.referred_contact)
                return 'sm_action_success'
        else:            
            Referral_failure_log.add_failure_log( self.ai, Referral_failure_log.ITN, referrer_number = self.sessionData.callerid, message = str(digits) )
            return 'ref_invalid_number'

    def pre_success_notify(self):
        return self.getPromptParams( 'refer_success_notify' )
     
    def pre_invalid_number(self):
        return self.getPromptParams( 'refer_invalid_value' )

    def pre_duplicate_number(self):
        return self.getPromptParams( 'refer_duplicate_number' )

    def while_app_exit__sm_action_success(self, events, eventData):
        return self.sessionData.popEmbedStack()


ReferStatesDescriptionMap = [
{
    'name':     'outgoingstart',
    'action':   'originate',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'welcome':  ['sm_action_success'],
        'outgoingstart':    ['sm_next_originate_url'],
    },
},
{
    'name':     'incomingstart',
    'action':   'answer',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'welcome':  ['sm_action_success'],
    },
},
{
    'name':     'welcome',
    'action':   'playback',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'continue': ['sm_action_success'],
    },
},
{
    'name':     'continue',
    'action':   'play_and_get_digits',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'capture_number':   ['ref_capture_number'],
        'thank_you':    ['ref_skip', 'sm_get_digits_no_digits'],
    },
    'dtmf': {
        1:      'ref_capture_number',
        'any':  'ref_skip'
    },
},
{
    'name':     'thank_you',
    'action':   'playback',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'app_exit': ['sm_action_success'],
    },
},
{
    'name':     'capture_number',
    'action':   'play_and_get_digits',
    'transitions':  {
        'stop':     ['sm_action_failure'],
        'success_notify': ['sm_action_success'],
        'invalid_number': ['ref_invalid_number'],
        'duplicate_number': ['ref_duplicate_number'],
    },
},
{
    'name':     'invalid_number',
    'action':   'playback',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'capture_number': ['sm_action_success'],
        },
},
{
    'name':     'duplicate_number',
    'action':   'playback',
    'transitions': {
        'stop':     ['sm_action_failure'],
        'capture_number': ['sm_action_success'],
        },
},
{
    'name':     'success_notify',
    'action':   'playback',
    'transitions':  {
        'stop':     ['sm_action_failure'],
        'continue': ['sm_action_success'],
    },
},
{
    'name':     'app_exit',
    'action':   'none',
    'transitions':  {
        'stop':     ['sm_action_failure','sm_action_success'],
    },
},
{
    'name':     'stop',
    'action':   'none',
    'transitions':  {},
},

                             
]

