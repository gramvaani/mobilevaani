import vapp.settings

from refer.models import Referral, Referral_status, Referral_group_call_log, Referred_contact_profile, Refer_ai_properties, Referral_failure_log
from callerinfo.models import get_contact_name 
from app_manager.models import Cdr
from mnews.models import Groups_call_log

from vapp.utils import is_start_of_week, send_email, generate_attachment, get_ratio, get_sorted_data, is_thursday, get_start_of_week
from datetime import *
from dateutil.relativedelta import relativedelta
from django.template.loader import render_to_string

from vapp.stats.stats import get_calls_count_by_caller
from mnews.stats import get_contrib_count_by_caller
from django.db.models import Count
from vapp.app_manager import perms as app_manager_perms

from log import get_request_logger
logger = get_request_logger()


# Required for showing week-wise trends for stats
def get_header_for_n_weeks( start, end, n ):
    header = []    
    for i in range( n ):
        header.append( 'Week:%s-%s' % ( ( start-timedelta(days=7*i) ).strftime("%b %d"), ( end-timedelta(days=7*i) ).strftime("%b %d") ) )
    
    return header


def get_hdr_for_grouping_trend( start, end, col_names, col_for_trends ):
    weeks_for_trend = 4
    header = []
    col_name_row = list( col_names )
    week_list_row = [ '' for col in col_name_row ]
    
    for column in col_for_trends:
        col_name_row.append( column )
        col_name_row += [ '' ] * ( weeks_for_trend - 1 )
        week_list_row += get_header_for_n_weeks( start, end, weeks_for_trend )
        
    header.append( col_name_row )
    header.append( week_list_row )    
    return header


def get_referals_last_ndays(ais, referer, current_date, n=1, total=False):
    if total:
        referrals_count = Referral.objects.filter(ai__in=ais, referring_contact=referer, time_added__lt=current_date).count()
        callback_success = Referral_group_call_log.objects.filter(referral__ai__in=ais, referral__referring_contact=referer, creation_time__lt=current_date, group_call_log__success=1).count()
    else:
        start_date = current_date - timedelta(days=n)
        referrals_count = Referral.objects.filter(ai__in=ais, referring_contact=referer, time_added__range=(start_date, current_date)).count()
        callback_success = Referral_group_call_log.objects.filter(referral__ai__in=ais, referral__referring_contact=referer, creation_time__range=(start_date, current_date), group_call_log__success=1).count()
        
    return (referrals_count, callback_success)


def get_pre_ref_status( referral, pre_months, ai ):
    is_old = False
    past_calls = past_contribs = 0
    cdrs = Cdr.objects.filter( ai=ai, callerid=referral.referred_contact, start_time__lt=referral.time_added )
    cdr_ids = cdrs.values_list( 'id', flat=True ).distinct()
    outbound_cdrs = Groups_call_log.objects.filter( ai=ai, last_cdr__id__in=cdr_ids, success=True, last_cdr__isnull=False ).values_list( 'last_cdr_id', flat=True ).distinct()
    calls = cdrs.exclude( id__in=outbound_cdrs ) if outbound_cdrs else cdrs
    if calls.exists():
        is_old = True
        past_calls = get_calls_count_by_caller( referral.referred_contact, ai, referral.time_added - relativedelta( months=pre_months ), referral.time_added )
        past_contribs = get_contrib_count_by_caller( referral.referred_contact, ai, referral.time_added - relativedelta( months=pre_months ), referral.time_added )                            

    return is_old, past_calls, past_contribs


def get_post_ref_status( referral, ai ):
    start = referral.time_added
    oneweek = start + relativedelta( days=7 )
    twoweeks = start + relativedelta( days=14 )
    
    oneweek_calls = get_calls_count_by_caller( referral.referred_contact, ai, start, oneweek )
    twoweek_calls = get_calls_count_by_caller( referral.referred_contact, ai, start, twoweeks )
    oneweek_contribs = get_contrib_count_by_caller( referral.referred_contact, ai, start, oneweek )
    twoweek_contribs = get_contrib_count_by_caller( referral.referred_contact, ai, start, twoweeks )    

    return ( oneweek_calls, twoweek_calls, oneweek_contribs, twoweek_contribs )


# referral_queryset = Referral.objects.filter(ai=refer_ai)
def get_effectiveness(refer_ai, instance_ai, start_date, end_date, referral_queryset=None):
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai=refer_ai)
    
    referrals = referral_queryset.filter(time_added__range=(start_date, end_date))    
    referred_referral = {}
    referred_ref_count = {}
    referred_data = {}
    for referral in referrals:
        contact = referral.referred_contact
        referrals_for_contact = referral_queryset.filter(referred_contact=contact).order_by('time_added')
        date_first_referral = str(referrals_for_contact[0].time_added)
        contact_location = "Not available"
        contact_profile = Referred_contact_profile.objects.filter(referral=referral)
        if contact_profile.exists():
            contact_location = str(contact_profile[0].location)
        #Choosing on the first referral of the referred contact
        if contact in referred_referral:
            referred_ref_count[contact] += 1
        else:
            referred_referral[contact] = referral
            referred_ref_count[contact] = 1
            is_old, calls_pre_3months, contribs_pre_3months = get_pre_ref_status(referral, 3, instance_ai)
            calls_post_week, calls_post_2weeks, contribs_post_week, contribs_post_2weeks = get_post_ref_status(referral, instance_ai)
            
            referred_data[contact] = { 'referred_contact': contact, 'referring_contact': referral.referring_contact, 
                        'referred_contact_loc': contact_location, 'date': date_first_referral, 'referral_count': 0,
                        'is_old_caller': is_old, 'calls_pre_referral': calls_pre_3months, 'contribs_pre_referral': contribs_pre_3months,
                        'calls_oneweek_post_ref': calls_post_week, 'calls_twoweeks_post_ref': calls_post_2weeks, 
                        'contribs_oneweek_post_ref': contribs_post_week, 'contribs_twoweeks_post_ref': contribs_post_2weeks }
            
    total_data = [ [ 'Referred Contact', 'Referring Contact', 'Location', 'Date', 'Referral Count', 'Is Old', 'Calls(Pre)', 'Contribs(Pre)',
                     'Calls(1 Week)', 'Calls(2 Weeks)', 'Contribs(1 Week)', 'Contribs(2 Weeks)' ] ]
    
    col_for_report = [ 'referred_contact', 'referring_contact', 'referred_contact_loc', 'date', 'referral_count', 'is_old_caller', 
                       'calls_pre_referral', 'contribs_pre_referral', 'calls_oneweek_post_ref', 'calls_twoweeks_post_ref',
                       'contribs_oneweek_post_ref', 'contribs_twoweeks_post_ref' ]
    
    data_body = []
    for contact in referred_data:
        data = referred_data[contact]
        data['referral_count'] = referred_ref_count[contact]
        data_body.append( [data[col] for col in col_for_report] )
    
    total_data += get_sorted_data(data_body, 4)
    return total_data


def format_location( data ):
    data = data.strip()
    return data or "Not available"


def get_formatted_locations( locations ):
    return sorted( [ format_location( location ) for location in locations ] )


def is_referred_before(refer_ai, contact, time_of_referral, referral_queryset = None):
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai = refer_ai)
    
    referral = referral_queryset.filter(referred_contact = contact, time_added__lt = time_of_referral).exclude(referring_contact = contact)
    return referral.exists()


def get_outbound_calls_rcvd_by_referees( refer_ai, instance_ai, start, end, referees ):
    return Referral_group_call_log.objects.filter( referral__ai=refer_ai, creation_time__range=( start, end ), group_call_log__number__in=referees, group_call_log__success=1, group_call_log__ai=instance_ai ).count()


# Need to update Location names in ref_contact_profile after finding a map of similar location names 
def get_eff_for_loc(location, refer_ai, instance_ai, start_date, end_date, contact_profiles_queryset=None, referral_queryset=None):
    
    if not contact_profiles_queryset:
        contact_profiles_queryset = Referred_contact_profile.objects.filter(referral__ai=refer_ai, location__isnull=False)
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai=refer_ai)    
    
    profiles = contact_profiles_queryset.filter(referral__time_added__range=(start_date, end_date))
    locs_for_duration = get_formatted_locations(profiles.values_list('location', flat=True))
    locations_count = locs_for_duration.count(location)
    outbound_calls = total_oneweek_calls = total_twoweek_calls = total_contribs_oneweek = total_contribs_twoweek = min_three_calls_twoweeks = 0
    ratio = 0.0
    referees = []
    
    for contact in profiles.filter(location__contains=location):
        refs = Referral.objects.filter(id=contact.referral.id).order_by('time_added')
        if not refs.exists() or is_referred_before(refer_ai, refs[0].referred_contact, refs[0].time_added, referral_queryset):
            continue

        (oneweek_calls, twoweek_calls, oneweek_contribs, twoweek_contribs) = get_post_ref_status(refs[0], instance_ai)
        
        if get_calls_count_by_caller(refs[0].referred_contact, instance_ai, start_date, start_date + timedelta(days=14)) >= 3:
            min_three_calls_twoweeks += 1
        
        referees.append(refs[0].referred_contact)
        total_oneweek_calls += oneweek_calls
        total_twoweek_calls += twoweek_calls
        total_contribs_oneweek += oneweek_contribs
        total_contribs_twoweek += twoweek_contribs

    ratio = get_ratio(total_twoweek_calls, locations_count)
    outbound_calls = get_outbound_calls_rcvd_by_referees(refer_ai, instance_ai, start_date, end_date, referees)
    
    return { 'locations_count': locations_count, 'ratio': ratio, 'outbound_calls': outbound_calls, 'min_three_calls_twoweeks': min_three_calls_twoweeks,
             'total_oneweek_calls': total_oneweek_calls, 'total_twoweek_calls': total_twoweek_calls, 'total_contribs_oneweek': total_contribs_oneweek,
             'total_contribs_twoweek': total_contribs_twoweek }
    

def get_eff_grp_by_loc(refer_ai, instance_ai, start_date, end_date, contact_profiles_queryset=None, referral_queryset=None):
    
    if not contact_profiles_queryset:
        contact_profiles_queryset = Referred_contact_profile.objects.filter(referral__ai=refer_ai, location__isnull=False)
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai=refer_ai)
    
    data = [ [ 'Location', 'Total Referrals', 'Ratio', 'Out-bound calls', 'No. of referred contacts', 'Calls(1 week)', 'Calls(2 weeks)',
               'Contribs(1 week)', 'Contribs(2 weeks)' ] ]
    body = []
    
    locations = contact_profiles_queryset.filter(referral__time_added__range=(start_date, end_date)).values_list('location', flat=True)
    for location in set(get_formatted_locations(locations)):
        data_body = [ location ]
        eff_data = get_eff_for_loc(location, refer_ai, instance_ai, start_date, end_date, contact_profiles_queryset, referral_queryset)
        data_body += eff_data['locations_count'], eff_data['ratio'], eff_data['outbound_calls'], eff_data['min_three_calls_twoweeks']
        data_body += eff_data['total_oneweek_calls'], eff_data['total_twoweek_calls'], eff_data['total_contribs_oneweek'], eff_data['total_contribs_twoweek']
        body.append(data_body)
    
    data += get_sorted_data(body, 2)  
    return data



#XXX Need to implement a structure that would be to make a call and get app.stats.effectiveness stats 
def get_eff_for_referrer(refer_ai, instance_ai, referrer, start, end, referral_queryset = None):
        
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai = refer_ai)
        
    total_refs = success_refs = min_three_calls_twoweeks = calls_oneweek_postref = calls_twoweeks_postref = contribs_oneweek_postref = contribs_twoweeks_postref = 0
    valid_referees = []
    
    refs = referral_queryset.filter(time_added__range = (start, end), referring_contact = referrer).exclude(referred_contact = referrer)
    for referee in refs.values_list('referred_contact', flat = True).distinct():
        curr_referral = refs.filter(referred_contact = referee).order_by( 'time_added')
        
        #if is_referred_before(refer_ai, referee, curr_referral[0].time_added, referral_queryset):
        #    continue
        #if get_calls_count_by_caller(referee, instance_ai, start, start+timedelta(days=14)) >= 3:
        #    min_three_calls_twoweeks += 1
            
        total_refs += 1
        if curr_referral[0].status == Referral_status.SC and min_calls_post_ref(curr_referral[0], 1):
            success_refs += 1
        #valid_referees.append(referee)
        #(oneweek_calls, twoweek_calls, oneweek_contribs, twoweek_contribs) = get_post_ref_status(curr_referral[0], instance_ai)
        #calls_oneweek_postref += oneweek_calls
        #calls_twoweeks_postref += twoweek_calls
        #contribs_oneweek_postref += oneweek_contribs
        #contribs_twoweeks_postref += twoweek_contribs

    #outbound_calls = get_outbound_calls_rcvd_by_referees(refer_ai, instance_ai, start, end, valid_referees)
    #ratio = get_ratio(calls_twoweeks_postref, total_refs)
    return { 'total_refs': total_refs, 'success_refs': success_refs }
    #return { 'total_refs': total_refs, 'ratio': ratio, 'min_three_calls_twoweeks': min_three_calls_twoweeks, 'outbound_calls': outbound_calls,
     #        'calls_oneweek_postref': calls_oneweek_postref, 'calls_twoweeks_postref': calls_twoweeks_postref, 'contribs_oneweek_postref': contribs_oneweek_postref,
     #        'contribs_twoweeks_postref': contribs_twoweeks_postref }


def get_eff_grp_by_referrer(refer_ai, instance_ai, start_date, end_date, referral_queryset=None):
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai=refer_ai)
    
    data = [ [ 'Referrer name', 'Referrer number', 'Total Referrals', 'Ratio', 'No. of referred contacts', 'Out-bound calls', 
              'Calls(1 week)', 'Calls(2 weeks)', 'Contribs(1 week)', 'Contribs(2 weeks)' ] ]
    body = []
    
    referrers = referral_queryset.filter(time_added__range=(start_date, end_date)).values_list('referring_contact', flat=True).distinct()
    for referrer in referrers:
        data_body = [ get_contact_name(referrer), referrer ]
        eff_data = get_eff_for_referrer(refer_ai, instance_ai, referrer, start_date, end_date, referral_queryset)
        data_body += eff_data['total_refs'], eff_data['ratio'], eff_data['min_three_calls_twoweeks'], eff_data['outbound_calls']
        data_body += eff_data['calls_oneweek_postref'], eff_data['calls_twoweeks_postref'], eff_data['contribs_oneweek_postref'], eff_data['contribs_twoweeks_postref'] 
        body.append( data_body )
        
    data += get_sorted_data( body, 3 )    
    return data


def get_location_grouping_trends(refer_ai, instance_ai, start, end, contact_profiles_queryset=None, referral_queryset=None):
    
    if not contact_profiles_queryset:
        contact_profiles_queryset = Referred_contact_profile.objects.filter(referral__ai=refer_ai, location__isnull=False)
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai=refer_ai)
    
    col_names = [ 'Location' ]
    col_for_trends = [ 'Total referrals', 'Ratio', 'Out-bound calls', 'No. of referred contacts', 'Calls(1 week)', 'Calls(2 weeks)', 'Contribs(1 week)', 'Contribs(2 weeks)' ]
    data = get_hdr_for_grouping_trend(start, end - timedelta(days=1), col_names, col_for_trends)
    
    locations = contact_profiles_queryset.values_list('location', flat=True)
    for location in set(get_formatted_locations(locations)):
        curr_week = get_eff_for_loc(location, refer_ai, instance_ai, start, end, contact_profiles_queryset, referral_queryset)
        curr_week_minus_one = get_eff_for_loc(location, refer_ai, instance_ai, start-timedelta(days=7), start, contact_profiles_queryset, referral_queryset)
        curr_week_minus_two = get_eff_for_loc(location, refer_ai, instance_ai, start-timedelta(days=14), start-timedelta(days=7), contact_profiles_queryset, referral_queryset)
        curr_week_minus_three = get_eff_for_loc(location, refer_ai, instance_ai, start-timedelta(days=21), start-timedelta(days=14), contact_profiles_queryset, referral_queryset)

        eff_report_columns = ['locations_count', 'ratio', 'outbound_calls', 'min_three_calls_twoweeks', 'total_oneweek_calls', 'total_twoweek_calls', 'total_contribs_oneweek', 'total_contribs_twoweek']
        
        effectiveness_stats = []
        for column in eff_report_columns:
            effectiveness_stats.append(curr_week[column])
            effectiveness_stats.append(curr_week_minus_one[column])
            effectiveness_stats.append(curr_week_minus_two[column])
            effectiveness_stats.append(curr_week_minus_three[column])
            
        data.append([location] + effectiveness_stats)

    return data


def get_referrer_grouping_trends(refer_ai, instance_ai, start, end, referral_queryset=None):
    
    if not referral_queryset:
        referral_queryset = Referral.objects.filter(ai=refer_ai)
    
    col_names = [ 'Referrer name', 'Referrer number' ]
    col_for_trends = [ 'Total referrals', 'Ratio', 'No. of referred contacts', 'Out-bound calls', 'Calls(1 week)', 'Calls(2 weeks)', 'Contribs(1 week)', 'Contribs(2 weeks)' ]
    eff_report_columns = ['total_refs', 'ratio', 'min_three_calls_twoweeks', 'outbound_calls', 'calls_oneweek_postref', 'calls_twoweeks_postref', 'contribs_oneweek_postref', 'contribs_twoweeks_postref']
    
    data = get_hdr_for_grouping_trend(start, end - timedelta(days=1), col_names, col_for_trends)
    
    referrers = referral_queryset.values_list('referring_contact', flat=True).distinct()
    for referrer in referrers:
        curr_week = get_eff_for_referrer(refer_ai, instance_ai, referrer, start, end, referral_queryset)
        curr_week_minus_one = get_eff_for_referrer(refer_ai, instance_ai, referrer, start-timedelta(days=7), start, referral_queryset)
        curr_week_minus_two = get_eff_for_referrer(refer_ai, instance_ai, referrer, start-timedelta(days=14), start-timedelta(days=7), referral_queryset)
        curr_week_minus_three = get_eff_for_referrer(refer_ai, instance_ai, referrer, start-timedelta(days=21), start-timedelta(days=14), referral_queryset)
        
        effectiveness_stats = []
        for column in eff_report_columns:
            effectiveness_stats.append(curr_week[column])
            effectiveness_stats.append(curr_week_minus_one[column])
            effectiveness_stats.append(curr_week_minus_two[column])
            effectiveness_stats.append(curr_week_minus_three[column])            
            
        data.append( [get_contact_name(referrer), referrer] + effectiveness_stats)

    return data


def create_legend_sheet_eff():
    data = [ [ '---Referral effectiveness---' ] ]
    data.append( [ 'Referred Contact' ] )
    data.append( [ 'Referring Contact' ] )
    data.append( [ 'Location:', 'Referred contact location' ] )
    data.append( [ 'Date:', 'Date of 1st referral' ] )
    data.append( [ 'Referral Count' ] )
    data.append( [ 'Is Old' ] )
    data.append( [ 'Calls(Pre):', 'Calls 3 Months pre-Referral' ] )
    data.append( [ 'Contribs(Pre):', 'Contribs 3 Months pre-Referral' ] )
    data.append( [ 'Calls(1 week):', 'Calls 1 Week post-Referral' ] )
    data.append( [ 'Calls(2 weeks):', 'Calls 2 Weeks post-Referral' ] )
    data.append( [ 'Contribs(1 week):', 'Contribs 1 Week post-Referral' ] )
    data.append( [ 'Contribs(2 weeks):', 'Contribs 2 Weeks post-Referral' ] )
    data.append( [ '---Location grouping---' ] )
    data.append( [ 'Location' ] )
    data.append( [ 'Total referrals' ] )
    data.append( [ 'Ratio:', 'Calls 2 weeks post referral/Total referrals' ] )
    data.append( [ 'Out-bound calls:', 'Out-bound calls received by referees' ] )
    data.append( [ 'No. of referred contacts:', 'No. of referred contacts (with at-least 3 calls in next 2 weeks)' ] )
    data.append( [ 'Calls(1 week):', 'Calls from referees 1 week post-referral' ] )
    data.append( [ 'Calls(2 weeks):', 'Calls from referees 2 weeks post-referral' ] )
    data.append( [ 'Contribs(1 week):', 'Contribs from referees 1 week post-referral' ] )
    data.append( [ 'Contribs(2 weeks):', 'Contribs from referees 2 week post-referral' ] )
    data.append( [ '---Referrer grouping---' ] )
    data.append( [ 'Referrer name' ] )
    data.append( [ 'Referrer number' ] )
    data.append( [ 'Total referrals' ] )
    data.append( [ 'Ratio:' , 'Calls 2 weeks post referral/Total referrals' ] )
    data.append( [ 'No. of referred contacts:', 'No. of referred contacts with at-least 3 calls in last 2 weeks' ] )
    data.append( [ 'Out-bound calls:', 'Out-bound calls received by referees' ] )
    data.append( [ 'Calls(1 week):', 'Calls from referees 1 week post-referral' ] )
    data.append( [ 'Calls(2 weeks):', 'Calls from referees 2 weeks post-referral' ] )
    data.append( [ 'Contribs(1 week):', 'Contribs from referees 1 week post-referral' ] )
    data.append( [ 'Contribs(2 weeks):', 'Contribs from referees 2 week post-referral' ] )
    data.append( [ '---Trends-Location grouping---', 'Contains per location data calculated week wise.' ] )
    data.append( [ '---Trends-Referrer grouping---', 'Contains per referrer data calculated week wise.' ] )

    return data

def min_calls_post_ref(ref, min_calls, days = 3):
    referred_ai = Refer_ai_properties.objects.get(ai = ref.ai).push_ai
    start = ref.time_added
    end = start + timedelta(days = days)
    calls = Cdr.objects.filter(ai = referred_ai, callerid = ref.referred_contact, is_incoming = True, start_time__range = (start, end))[:min_calls]
    return calls.count() == min_calls

def refs_with_min_calls_post_ref(referrals, min_calls):
    ret_refs = []
    for ref in referrals:
        if min_calls_post_ref(ref, min_calls):
            ret_refs.append(ref)
    return ret_refs

def get_successful_referrals(referrals):
    referrals = referrals.filter(status = Referral_status.SC)
    return refs_with_min_calls_post_ref(referrals, 1)


def get_ref_stats(ais, start_date, end_date, referrers = None, get_success_count = False):
    stats = {}
    referrals = Referral.objects.filter(ai__in = ais, time_added__lt = end_date)
    if referrers:
        referrals = referrals.filter(referring_contact__in = referrers)

    stats['total_referrals'] = referrals.count()

    referrals = referrals.filter(time_added__gte = start_date)
    stats['referrals'] = referrals.count()
    stats['referrals_call_received'] = referrals.filter(status = Referral_status.SC).count()
    if get_success_count:
        successful_referrals = get_successful_referrals(referrals)
        stats['successful_referrals'] = len(successful_referrals)
  
    return stats


def get_ref_eff_data_for_ndays_before_date(refer_ai, n, end_date):
    start_date = end_date - timedelta(days=n)
    instance_ai = Refer_ai_properties.objects.get(ai=refer_ai).push_ai
    referral_queryset = Referral.objects.filter(ai=refer_ai)
    contact_profiles_queryset = Referred_contact_profile.objects.filter(referral__ai=refer_ai, location__isnull=False)
    
    data = [ get_effectiveness(refer_ai, instance_ai, start_date, end_date, referral_queryset) ]
    data.append( get_eff_grp_by_referrer(refer_ai, instance_ai, start_date, end_date, referral_queryset) )
    data.append( get_referrer_grouping_trends(refer_ai, instance_ai, start_date, end_date, referral_queryset) )
    
    sheet_names = [ 'Referral effectiveness', 'Referrer grouping', 'Trends-Referrer grouping' ]
    sheet_freeze_order = [ [1,1], [2,1], [2,2] ]
    
    if location_info_present(refer_ai):
        data.append( get_eff_grp_by_loc(refer_ai, instance_ai, start_date, end_date, contact_profiles_queryset, referral_queryset) )
        data.append( get_location_grouping_trends(refer_ai, instance_ai, start_date, end_date, contact_profiles_queryset, referral_queryset) )
        sheet_names += [ 'Location grouping', 'Trends-Location grouping' ]
        sheet_freeze_order += [ [1, 2], [1, 2] ]
        
    data.append( create_legend_sheet_eff() )
    sheet_names += [ 'Legend Sheet' ]
    sheet_freeze_order += [ [1, 1] ]
    
    return data, sheet_names, sheet_freeze_order
    

def stats_recv_perms_present( perms ):
    return app_manager_perms.recv_refer_stats in perms


def check_and_get_eff_attachment( ai, end_date, report_path=None ):
    if not is_start_of_week( end_date ):
        return None
    data, sheet_names, sheet_freeze_order = get_ref_eff_data_for_ndays_before_date( ai, 7, end_date )
    report_path = report_path or '/tmp/%s_%s_effectiveness.xlsx' % ( ai.id, ai.name )
    
    if not generate_attachment( data, report_path, True, sheet_names, True, sheet_freeze_order ):
        logger.error( 'Error:Attachment could not be generated for ai: %s_%s.' % ( ai.id, ai.name ) )
        return None
    return report_path


def create_mail_content( stats, ai_name, start_date, end_date = None ):
    if end_date:
        subject = '%s: Statistics from %s to %s' % ( ai_name, start_date, end_date )
    else:
        subject = '%s: Statistics for %s' % ( ai_name, start_date )
    return subject, render_to_string( 'refer/stats.html', stats )


def email_stats( ai_perms_dict, to_email ):
    today = datetime.now().date()
    
    for ai, perms in ai_perms_dict.iteritems():
        if not stats_recv_perms_present( perms ):
            continue

        create_and_email_stats(ai, today - timedelta(days = 1), today, to_email, get_success_count = False)

        if is_thursday(today):
            end_date = get_start_of_week(today)
            start_date = end_date - timedelta(days = 7)
            create_and_email_stats(ai, start_date, end_date, to_email, get_success_count = True)


def create_and_email_stats(ai, start, end, to_email, get_success_count):
    stats = get_ref_stats([ai], start, end, get_success_count = get_success_count)
    subject, content = create_mail_content( stats, ai.name, start, end )
    #send_email( subject , content, [ to_email ], attachments = check_and_get_eff_attachment( ai, today ) )
    send_email(subject, content, [to_email])


def location_info_present( refer_ai ):
    return Referred_contact_profile.objects.filter( referral__ai=refer_ai ).exists()


def get_referrers_success_stats( ai, start_time, end_time = None, referrers = None ):
    if not end_time:
         end_time = datetime.now()
    
    referrals = Referral.objects.filter(ai = ai, time_added__range = (start_time, end_time))
    if referrers:
        referrals = referrals.filter(referring_contact__in = referrers)

    stats = {}
    for ref in referrals:
        if not stats.get(ref.referring_contact):
            stats[ref.referring_contact] = {'success_count':0, 'failure_count':0}

        if ref.status == Referral_status.SC and min_calls_post_ref(ref, 1):
            stats[ref.referring_contact]['success_count'] += 1
        else:
            stats[ref.referring_contact]['failure_count'] += 1
                
    return stats