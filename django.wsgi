import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'vapp.settings'

import_paths = [ '/usr/local/voicesite/vapp', '/usr/local/voicesite' ]
for path in import_paths:
    if path not in sys.path:
       sys.path.append(path)
import os
print '===== sys.path / PYTHONPATH ====='
for k in sorted(os.environ.keys()):
    v = os.environ[k]
    print ('%-30s %s' % (k,v[:70]))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

os.environ['CELERY_LOADER'] = 'django'
