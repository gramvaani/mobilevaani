from vapp.settings import *
from caching_service.models import *
from django.db.utils import IntegrityError
#from vapp.mnews.models import *
#from vapp.app_manager.models import *


from vapp.utils import update_to_dynamo, delete_obj_in_dynamo
from log import get_request_logger
import json
from django.core.serializers.json import DjangoJSONEncoder
from local_settings import SERVER_ID

logger = get_request_logger()
MAX_ITEMS = 100



def sync_object_data(model_name, object_id, payload_type = '', payload = None, status = 'CREATED', action_by = APP_NAME):
    logger.info("LOG FOR DATA SYNC RECORD"+str(model_name)+str(object_id)+str(action_by))	
    try: 
        data_sync_info = Data_sync_info(model_name=model_name, object_id=object_id, status=status, updated_by=action_by, payload_type = payload_type, payload = payload)
        data_sync_info.save()
    except Exception as e:
        logger.info("Exception in sync_object_data"+str(e))

def sync_ai_pub_list(**kwargs):
	try:
		ai_dict = {}
		from mnews.models import News
		channel_id = kwargs.get("channel_id")
		ai_id = kwargs.get("ai_id")
		logger.info("In sync_ai_pub_list for "+str(ai_id))
		items_list = News.objects.filter(ai__id = ai_id, channel__id = channel_id, state = 'PUB', is_comment = False).order_by('pub_order')[:MAX_ITEMS].values_list('id', flat=True)
		items_list = map(int,items_list)
		pub_list = 'publistCh'+str(channel_id)
		ai_dict[pub_list] = items_list
		ai_dict['server'] = SERVER_ID
		ai_dict['ai'] = ai_id
		pkey = SERVER_ID+"-ai-"+str(ai_id)
		skey = 'General'
		update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))
	except Exception as e:
		logger.info("Exception in sync_ai_pub_list"+str(e))

def update_ai_for_channel(ai_id):
	from app_manager.models import User_permission
	users = User_permission.objects.filter(ai_id =ai_id)
	user_list = []
	for user in users:
		if user.user_id not in user_list:
			user.SyncDynamo()

def delete_from_dynamo(model_name, pkey_to_delete, skey):
	delete_obj_in_dynamo(model_name, pkey_to_delete, skey)
