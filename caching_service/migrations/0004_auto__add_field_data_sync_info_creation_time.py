# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Data_sync_info.creation_time'
        db.add_column(u'caching_service_data_sync_info', 'creation_time',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Data_sync_info.creation_time'
        db.delete_column(u'caching_service_data_sync_info', 'creation_time')


    models = {
        u'caching_service.data_sync_info': {
            'Meta': {'object_name': 'Data_sync_info'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'payload': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True'}),
            'payload_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30', 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CREATED'", 'max_length': '10'}),
            'synced_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'updated_by': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['caching_service']