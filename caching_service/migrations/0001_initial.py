# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Data_sync_info'
        db.create_table(u'caching_service_data_sync_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('model_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('synced_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('updated_by', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'caching_service', ['Data_sync_info'])


    def backwards(self, orm):
        # Deleting model 'Data_sync_info'
        db.delete_table(u'caching_service_data_sync_info')


    models = {
        u'caching_service.data_sync_info': {
            'Meta': {'object_name': 'Data_sync_info'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'synced_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'updated_by': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['caching_service']