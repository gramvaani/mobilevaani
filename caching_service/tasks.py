from celery.task import Task
from caching_service.models import Data_sync_info
from caching_service.utils import sync_object_data
from datetime import datetime, timedelta
from django.db.models import get_model, Q
import json
from log import get_request_logger

logger = get_request_logger()
SYNC_MAX_TRIES_LIMIT=3

app_model_dict = {'News':'mnews', 'Channel':'mnews', 'Application':'app_manager', 'Event_rejection_reason_group':'mnews', 'Location': 'location', 'Campaign_category':'campaign', 'User_permission':'app_manager', 'Contact':'callerinfo'}


class SyncFailedTask(Task):

	def run(self, *args, **kwargs):
		try:
			items_to_sync = Data_sync_info.objects.filter(Q(tries__isnull=True) | Q(tries__lt=SYNC_MAX_TRIES_LIMIT), status__in=('CREATED', 'FAILED'))
			if items_to_sync:
				for item in items_to_sync:
					item.status = 'INPROGRESS'
					item.save()
				# items_to_sync_list = items_to_sync.values('model_name', 'object_id', 'payload_type', 'payload').distinct()
				for item in items_to_sync:
					model_name = item.model_name
					object_id = item.object_id
					payload_type = item.payload_type
					payload = item.payload
					if payload:
						payload_dict = json.loads(payload)
					try:
						model_class = get_model(app_model_dict.get(model_name), model_name)
						model_object = model_class.objects.get(pk = object_id)
						if payload_type == 'news_update':
							model_object.SyncDynamo(**payload_dict)
						elif payload_type == "pub_order_list":
							sync_ai_pub_list(**payload_dict)
						else:
							model_object.SyncDynamo()
					except Exception as e:
						for item in items_to_sync:
							item.status = 'FAILED'
							item.tries += 1
							item.save()
						logger.info("Exception while syncing to Dynamo in SyncFailedTask for object_id : " + str(object_id) + str(e))
				for item in items_to_sync:
					item.status = 'SYNCED'
					item.synced_time = datetime.now()
					item.save()
		except Exception as e:
			logger.info("Exception in SyncFailedTask for object_id : " + str(object_id) + str(e))


class SyncDynamoTask(Task):

	def run(self, model_object, payload_type, payload):
		try:
			logger.info("Inside SyncDynamoTask")
			model_name = model_object.__class__.__name__
			if payload:
				payload_dict = json.loads(payload)
			if payload_type == 'news_update':
				model_object.SyncDynamo(**payload_dict)
			else:
				model_object.SyncDynamo()
		except Exception as e:
			sync_object_data(model_name, model_object.id, 'exception', str(e), status ='FAILED')
			logger.info("Exception in SyncDynamoTask for object_id : " + str(model_object) + str(e))
