from django.db import models

from datetime import datetime


class Data_sync_info(models.Model):
    SYNC_STATUS_CHOICES = (
        ('CREATED', 'CREATED'),
        ('INPROGRESS', 'INPROGRESS'),
        ('SYNCED', 'SYNCED'),
        ('FAILED', 'FAILED'),
        ('SUCCESS','SUCCESS')
    )
    model_name = models.CharField(max_length=100)
    object_id = models.PositiveIntegerField(null=True)
    status = models.CharField(max_length=10, choices=SYNC_STATUS_CHOICES, default='CREATED')
    tries = models.PositiveSmallIntegerField(null=True)
    creation_time  = models.DateTimeField(default = datetime.now)
    synced_time = models.DateTimeField(null=True)
    updated_by = models.CharField(max_length = 10)
    payload_type = models.CharField(max_length=30, default = '', null=True)
    payload = models.TextField(default='', null=True)

