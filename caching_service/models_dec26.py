from django.db import models

from datetime import datetime

class Data_sync_info(models.Model):

    model_name = models.CharField(max_length=100)
    object_id = models.PositiveIntegerField(null=True)
    status = models.BooleanField(default=False)
    synced_time = models.DateTimeField(null=True)
    updated_by = models.CharField(max_length = 10)

