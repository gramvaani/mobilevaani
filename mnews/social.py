from models import *
from vapp.social.models import *
from log import get_request_logger
logger = get_request_logger()

def pre_sm_publish( ai, smedia, title, message, app_kwargs ):
    news_id = app_kwargs[ 'news_id' ]
    news = News.objects.get( id = news_id )
    background_image = news.sm_image
    detail_url = ( news.detail.get_full_filename() if smedia.sm_name == 'facebook' else news.detail.get_url() )
    social_log, created = Social_publish_log.get_publish_log( ai, news, smedia, detail_url ) 
    return ( detail_url, background_image ) if created else ( None, None )

def post_sm_publish( ai, smedia, url, status, response = None ):
    return Social_publish_log.update_log_status( ai, smedia, url, status, response )
    

def update_url( ai, smedia, old_url, new_url):
    return Social_publish_log.update_url( ai, smedia, old_url, new_url )
