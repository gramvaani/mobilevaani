from django.http import HttpResponse, HttpResponseNotFound
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.db.models import Q
from django.db import connection
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth.models import User, auth
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

from mnews.tasks import *
from mnews.models import *

from app_manager.decorators import instance_perm, instance_perm_per_obj
from app_manager.models import App_instance, Cdr
from campaign.models import Campaign
from log import get_request_logger
from media.views import recording_upload_impl
from callerinfo.models import DEFAULT_CALLERID
from vapp.utils import daterange
from local_settings import SERVER_ID

from datetime import datetime, timedelta, time
import RQMC
import json, urllib
import datetime


logger = get_request_logger()

ERR_UPLOAD_FILE = 'ERR_UPLOAD_FILE'

@login_required
@instance_perm
def load_channels(request, ai_id):
    logger.debug( request )
    js = serializers.get_serializer("json")()

    channels = Channel.objects.filter( ai = ai_id )
    data = js.serialize( channels )
    return HttpResponse( data )

@login_required
@instance_perm
def load_news(request, ai_id, channel_id, state, start_row, num_rows):
    logger.debug(request)
    js = serializers.get_serializer("json")()
    
    if None in [start_row, num_rows, state]:
        return HttpResponseNotFound()
    
    start_row = int(start_row)
    num_rows  = int(num_rows)
    end_row   = start_row + num_rows
    
    query = News.objects.filter(ai = ai_id, is_comment = False, state = state)
    if int( channel_id ) >= 0:
        query = query.filter( channel = channel_id )
    if state == 'PUB':
        news = query.order_by('pub_order')
    else:
        news = query.order_by('-time')
    data = js.serialize(news[start_row:end_row], extras=('num_unm_comments','num_comments'), relations=('detail','summary',), excludes=('comments',))
    
    return HttpResponse(data)
    

@login_required
@instance_perm
def load_comments(request, ai_id, parent_id, start_row, num_rows):
    logger.debug(request)
    js = serializers.get_serializer("json")()
    
    if parent_id is None or start_row is None or num_rows is None:
        return HttpResponseNotFound()
    
    parent_id = int(parent_id)
    start_row = int(start_row)
    num_rows  = int(num_rows)
    end_row   = start_row + num_rows
    
    try:
        comments = News.objects.get(ai = ai_id, pk = parent_id).comments.filter(state__in=['PUB','UNM']).order_by('-time')
        data = js.serialize(comments[start_row:end_row], extras=('num_comments','num_unm_comments', 'parent_id'), relations=('detail', 'summary',))
        return HttpResponse(data)
    except:
        logger.exception("Unable to load comments for ai: %s, parent: %s" % (ai_id, parent_id))
        return HttpResponseNotFound()

SEARCH_FIELDS = [ 'tags', 'title', 'category__name', 'transcript', 'callerid' ]

def get_q_term_for_field( keywords, field, lookup_type = 'icontains' ):
    if keywords == '_':
        return Q()
    keywords = [ i.strip() for i in keywords.split(',') ]
    keyword_q = []
    for keyword in keywords:
        if lookup_type == 'icontains':
            keyword_q.append( Q( **{'%s__icontains' % field: keyword } ) | Q( **{'comments__%s__icontains' % field: keyword } ) )
        elif lookup_type == 'equals':
            keyword_q.append( Q( **{ field: keyword } ) | Q( **{'comments__%s' % field: keyword } ) )
        else:
            logger.error( "Unknown lookup type" )
    q = reduce( lambda q1, q2: q1 & q2, keyword_q )
    return q

def get_q_term_for_generic( keywords ):
    if keywords == '_':
        return Q()
    keywords = [ i.strip() for i in keywords.split(',') ]
    keyword_q = []
    for keyword in keywords:
        q = Q()
        for field in SEARCH_FIELDS:
            q = q | Q( **{'%s__icontains' % field: keyword } ) | Q( **{'comments__%s__icontains' % field: keyword } )
        keyword_q.append( q )
    return reduce( lambda q1, q2: q1 & q2, keyword_q )
    
def get_search_query( ai_id, generic, tags, title, category, transcript, ids, channel, callerid ):
    q_term = get_q_term_for_generic( generic )
    q_term &= get_q_term_for_field( tags, 'tags' )
    q_term &= get_q_term_for_field( title, 'title' )
    q_term &= get_q_term_for_field( category, 'category__name' )
    q_term &= get_q_term_for_field( transcript, 'transcript' )
    q_term &= get_q_term_for_field( ids, 'id', 'equals' )
    q_term &= get_q_term_for_field( channel, 'channel__name' )
    q_term &= get_q_term_for_field( callerid, 'callerid' )
    return News.objects.filter( ai__id = ai_id, is_comment = False ).filter( q_term ).distinct().order_by( '-time' )

@login_required
@instance_perm
def search_news(request, ai_id, generic, tags, title, category, transcript, ids, channel, callerid, start_row, num_rows):
    logger.debug(request)
    js = serializers.get_serializer("json")()
    
    if None in [ ai_id, start_row, num_rows ]:
        return HttpResponseNotFound()

    start_row = int(start_row)
    num_rows  = int(num_rows)
    end_row   = start_row + num_rows
    
    try:
        logger.info('search_news - The callerid is:' + str(callerid))
        news = get_search_query( ai_id, generic, tags, title, category, transcript, ids, channel, callerid )
        data = js.serialize(news[start_row:end_row], extras=('num_comments', 'num_unm_comments', 'parent_id'), relations=('detail', 'summary',), excludes=('comments',))
        return HttpResponse(data)
    except:
        logger.exception("Unable to search for ai: %s, tags: %s" % (ai_id, tags))
        return HttpResponseNotFound()

@login_required
@instance_perm
def search_news_count(request, ai_id, generic, tags, title, category, transcript, ids, channel, callerid ):
    logger.debug(request)
    js = serializers.get_serializer("json")()
    
    if ai_id is None:
        return HttpResponseNotFound()
    
    try:
        logger.info('search_news - The callerid is:' + str(callerid))
        news = get_search_query( ai_id, generic, tags, title, category, transcript, ids, channel, callerid )
        data = json.dumps( {'count': news.count()} )
        return HttpResponse( data )
    except:
        logger.exception("Unable to count search for ai: %s, tags: %s" % (ai_id, tags))
        return HttpResponseNotFound()
    
    
@login_required
@csrf_exempt
@require_POST
@instance_perm
def upload_file(request, ai_id, role, news_id):
    logger.info(request)
    recording = recording_upload_impl(request, ai_id)
    
    if type(recording) != Recording:
        return HttpResponse(recording)
    
    try:
        news = News.objects.get(pk = int(news_id))
        if role == 'detail':
            news.detail = recording
        elif role == 'summary':
            news.summary = recording
        else:
            return HttpResponse(ERR_UPLOAD_FILE)
        #if news.detail.file_exists():
        #    news.is_empty = False
        news.modified_date = datetime.now()
        news.save()
        js = serializers.get_serializer("json")()
        return HttpResponse(js.serialize([recording]))
    except:
        logger.exception('Could not upload file to ai: %s, role: %s, news_id: %s' % (ai_id, role, news_id))
        return HttpResponse(ERR_UPLOAD_FILE)



@login_required
@csrf_exempt
@require_POST
@instance_perm_per_obj('ai')   
def save_recording(request):
    
    logger.debug(request)
    for news_wrapper in serializers.deserialize('json', request.POST['json']):
        news = news_wrapper.object
        news.moderator = request.user
        news.save()                
    return HttpResponse("ok")

@login_required
@csrf_exempt
@require_POST
@instance_perm
def create_new_item(request, ai_id, channel_id):

    logger.info(request)
    ai_id = int(ai_id)
    time = datetime.now()
    title = 'News, Created at %s' % time.strftime('%H:%M, %d %b\'%y')
    #recording = Recording(ai_id = ai_id, time = time)
    #recording.save()
    news = News(ai_id=ai_id, channel_id=channel_id,
                state='UNM', is_comment=False,
                source=News.Source.WEB_INTERFACE,
                title=title, time=time,
                callerid=DEFAULT_CALLERID)
    news.modified_date = datetime.now()
    news.save()
    return HttpResponse("ok")

def clone_news(news, auto_save=True, parent=None, copy_state='UNM',
               copy_channel_id=None, as_item=False):
    news_copy = News.objects.get( pk = news.id )
    news_copy.id = None
    tag = "cpf" + str(news_copy.ai_id)

    #if not news_copy.is_comment:
    news_copy.state = copy_state
    if as_item:
        news_copy.is_comment = False
    if parent:
        news_copy.parent_id_value = parent.id
    if copy_channel_id:
        news_copy.channel_id = copy_channel_id
        new_channel = Channel.objects.get(pk = copy_channel_id)
        news_copy.ai_id = new_channel.ai_id
    if news.detail:
        news_copy.detail = news.detail.clone(news_copy.ai_id)
    if news.summary:
        news_copy.summary = news.summary.clone(news_copy.ai_id)
    if news_copy.tags:
        tag_list = news_copy.tags.split(",")
        tag_list.append(tag)
        news_copy.tags= ','.join(tag_list)
    else:
        news_copy.tags = tag
    news_copy.cm_properties = ''	
    if auto_save:
        news_copy.save()
        if parent:
            parent.comments.add( news_copy )

    return news_copy
    
def clone_campaign(news, copy):
    """Clone campaign categories.
    Directly copy categories if source item's AI and destination items AI are same.
    Otherwise, assign an intersection of source item's campaign categories and destination AI's campaign categories to the copy news item. """
    source_ai = news.ai
    dest_ai = copy.ai
    source_camp_cat = news.campaign_categories.all()
    if source_ai == dest_ai:
        copy.campaign_categories = source_camp_cat
    else:
        copy.campaign_categories = Campaign_category.objects.filter(
                                    campaign__ais=dest_ai,
                                    campaign__show_in_ui=True,
                                    id__in=source_camp_cat)
    copy.save()
@login_required
@csrf_exempt
@require_POST
@instance_perm
def copy_to_channel(request, ai_id, news_id, channel_id):
    logger.debug( request )
    try:
        news = News.objects.get( ai = ai_id, pk = news_id )
        copy = clone_news( news, copy_channel_id = channel_id )
        news.record_copy(copy)

        for comment in news.comments.exclude(state=News_state.REJ):
            comment_copy = clone_news(comment, parent=copy,
                                      copy_channel_id=channel_id)
            comment.record_copy(comment_copy)

        return HttpResponse(str(copy))
    except:
        logger.exception( "Unable to copy to channel ai: %s, news: %s, channel: %s" % (ai_id, news_id, channel_id) )
        return HttpResponse( "err" )


@login_required
@instance_perm
def get_stats_incoming_calls(request, ai_id):
    logger.debug(request)

    end = datetime.now().date() - timedelta(days = 1)
    date_labels = [(end - i *timedelta(days = 1)).strftime("%d %b") for i in range(7)]

    start = end - timedelta(days = 7)

    counts = []
    for d in daterange(start, end + timedelta(days = 1)):
        for hour in range(24):
            start_hour = datetime.combine(d, time(hour = hour))
            end_hour = start_hour + timedelta(hours = 1)
            try:
                stat = Hourly_call_stats.objects.get(ai_id = ai_id, start = start_hour, end = end_hour)
                counts.append(stat.calls)
            except Exception, e:
                counts.append(0)

    data = json.dumps({'date_labels': date_labels, 'counts': counts})
    return HttpResponse(data)


NUM_NEWS_PER_PAGE = 10


def show_news(request, ai_id=None, location=None, tag=None, location_id=None,
              format_id=None, qualifier_id=None):

    logger.info(request)

    mnews_id = Application.objects.get(name='mnews').id
    ai_list = App_instance.objects.filter(app_id=mnews_id)
    news = News.objects.filter(Q(state=News_state.ARC) | Q(state=News_state.PUB), is_comment=False)

    if ai_id not in (None, 0, '0'):
        ai = App_instance.objects.get(id=int(ai_id))
        news = news.filter(ai=ai)
    else:
        ai = None

    if location:
        location = urllib.unquote_plus(str(location))
        news = news.filter(location_text=location)

    if tag:
        tag = urllib.unquote_plus(str(tag))
        news = news.filter(tags__regex=r'(^|[,])' + tag + r'($|[,])')

    if location_id:
        location_id = int(location_id)
        news = news.filter(location__pk=location_id)

    if format_id:
        news = news.filter(format_id=format_id)

    if qualifier_id:
        news = news.filter(qualifier_id=qualifier_id)


    news = news.order_by('-time')
    paginator = Paginator(news, NUM_NEWS_PER_PAGE)
    page = request.GET.get('page')

    if page:
        try:
            news_page = paginator.page(page)

        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            news_page = paginator.page(1)

        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            news_page = paginator.page(paginator.num_pages)
    else:
        news_page = paginator.page(1)

    return render_to_response('mnews/news.html',
                              {'pub_news': news_page, 'ai': ai, 'ai_list': ai_list},
                              context_instance=RequestContext(request)
                              )


def show_news_oursay(request):
    logger.info(request)
    ais = [161]
    jmr_ai=10
    news = News.objects.filter(Q(ai__in=ais) | Q(tags__icontains='oursay-1'), state__in=['PUB','ARC'], is_comment = False).order_by('-time')    
    paginator = Paginator(news, NUM_NEWS_PER_PAGE)
    page = request.GET.get('page')
    if page:
        try:
            news_page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            news_page = paginator.page(1)
        except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
            news_page = paginator.page(paginator.num_pages)
    else:
        news_page = paginator.page(1)
    return render_to_response('mnews/oursay.html',{'pub_news': news_page}, context_instance = RequestContext(request))


def get_embedded_news(request, ai_id, news_id):
    
    try:
        ai_id = App_instance.objects.get(id=int(ai_id))
        news_id = int(news_id)
        news_item = News.objects.get(Q(state='ARC') | Q(state='PUB'), ai=ai_id, id=news_id)
    except Exception as e:
        logger.exception('news id passed on is not correct' + str(news_id) + ', exception:' + str(e))
        return HttpResponse("News Not Found")
    
    embed_title = request.GET.get('title', "1")
    embed_transcript = request.GET.get('transcript', "0")
    embed_audio = request.GET.get('audio', "1")

    return render_to_response('mnews/embed_news.html',{'news': news_item, 'ai': ai_id, 'embed_title': embed_title, 'embed_transcript': embed_transcript, 'embed_audio': embed_audio})


def show_detail(request, ai_id, news_id):
    
    try:
        ai_id = App_instance.objects.get(id=int(ai_id))
        news_id = int(news_id)
        news_item = News.objects.filter(Q(state='ARC') | Q(state='PUB'),ai=ai_id, id=news_id, is_comment = False)        
    except Exception as e:
        logger.exception('news id passed on is not correct' + str(news_id) + ', exception:' + str(e))
        return HttpResponse("News Not Found")
    
    return render_to_response('mnews/news_detail.html',{'pub_news': news_item, 'ai': ai_id}, context_instance = RequestContext(request))    
        

#@login_required
def get_news_json(request,ai_id,year,month,day,hour,min):
    
    year = int(year)
    month = int(month)
    day = int(day)
    hour = int(hour)
    min = int(min)
    ai_id = int(ai_id)
    query_time = None
    try:
        query_time = datetime(year,month,day,hour,min)
        news = News.objects.filter(ai__id=ai_id, time__gte=query_time, state__in=['PUB','ARC'])
        js = serializers.get_serializer("json")()
        data = js.serialize(news, extras=('num_unm_comments',), relations=('detail','summary',))
        return HttpResponse(data)
    except Exception,e:
        logger.info('get_news_json exception:' + str(e)) 
        return HttpResponse('get_news_json exception:' + str(e))
    return HttpResponse('unreachable code')

@login_required
def get_locations(request, ai_id):
   
   locations = list(Location.objects.filter(ai=ai_id))
   locations.sort(key=str)
   #default_location = Location.objects.get(district__name = 'Not Known', block__name = 'Not Known', village = 'Not Known') 
   js = serializers.get_serializer("json")()
   data = js.serialize(locations, extras=('__unicode__',), excludes=('country','state','district','block'))
   return HttpResponse(data)
    

@login_required
def get_categories(request, ai_id):
   
   categories =list(Category.objects.filter(ai__exact=ai_id))
   categories.sort(key=str)    
   js = serializers.get_serializer("json")()
   data = js.serialize(categories, extras=('__unicode__',), excludes =('name','parent','subcategories','is_subcategory'))
   return HttpResponse(data)


@login_required
def get_qualifiers(request):
   
   qualifiers = Qualifier.objects.all()    
   js = serializers.get_serializer("json")()
   data = js.serialize(qualifiers)
   return HttpResponse(data)

@login_required
def get_formats(request):
   
   formats = Format.objects.all()    
   js = serializers.get_serializer("json")()
   data = js.serialize(formats)
   return HttpResponse(data)
#@login_required
def get_news_metadata(request, format, news_id):
    
    try:
        logger.info('the format is:' + str(format))
        news = News.objects.get(pk = int(news_id))
        data = None
        if format == 'json':
            js = serializers.get_serializer("json")()
            data = js.serialize([news])
        else:
            raise Http404
        return HttpResponse(data)
    except Exception, e:
        logger.info('Exception while getting news data:' + str(e))
        return HttpResponse(str(e))

def upload_all_categories_for_ai(ai_id):
    ai_dict= {}
    ai = App_instance.objects.filter(pk = ai_id)
    categories = Category.objects.filter(ai__in = ai)
    cat_parent = categories.filter(parent__isnull = True)
    cat_list = []
    for cat in cat_parent:
        p_dict = {}
        p_dict["name"] = cat.name
        p_dict["id"] = cat.id
        child_list = []
        childrens = categories.filter(parent = cat)
        for child in childrens:
            c_dict ={}
            c_dict["id"] = child.id
            c_dict["name"] = child.name
            child_list.append(c_dict)
        p_dict["subCategories"] = child_list
        cat_list.append(p_dict)

    ai_dict["category"] = json.dumps(cat_list)
    ai_dict["ai"] = ai_id
    ai_dict["server"] = SERVER_ID
    # print ai_dict
    if cat_list:
        pkey = SERVER_ID+"-ai-"+str(ai_id)
        skey = 'General'
        update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))
    else:
        logger.info('Not sending to server')
 
def get_reject_reasons(news_obj):
    if news_obj.is_comment:
        event_ai_properties_qs = Event_ai_properties.objects.filter(event_type=ModerationEvents.COMMENT_REJECTED, ai_id=news_obj.ai_id)
    else:
        event_ai_properties_qs = Event_ai_properties.objects.filter(event_type=ModerationEvents.ITEM_REJECTED, ai_id=news_obj.ai_id)

    rejection_reasons = None
    if event_ai_properties_qs.exists():
        event_rejection_reason_group = Event_rejection_reason_group.objects.filter(event=event_ai_properties_qs[0])
        rejection_reason_group = event_rejection_reason_group[0].group
        rejection_reasons = rejection_reason_group.rejection_reason

    return rejection_reasons

def is_mnews_instance(ai_id):
    return True if App_instance.objects.get(pk = ai_id).app_id == 6 else False

def get_moderation_event_history(request, user_id=None, start_date=None, end_date=None, app_instance=None):
    #define how many items to be returned
    history_items_capacity = 20

    page = request.GET.get('page')
    if page:
        page=int(page)
    else:
        page=0
    if (user_id):
        user_id=int(user_id)
    if app_instance:
        app_instance = int(app_instance)
    try:
        # fetch all moderators and app instance list to display on template
        moderator_list = User.objects.filter(groups__name='GV Moderators') #auth_group table, GV Moderators group = 6
        app_instance_list = App_instance.objects.filter(app__name='mnews', status=1)# mnews active instances app_id = 6

        # create a query set to fetch data on provided parameterss
        query_set = Q()

        query_set.add(Q(user_id=user_id), Q.AND)
        if app_instance:
            query_set.add(Q(item__ai=app_instance), Q.AND)

        if start_date and end_date:
            s_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
            e_date = datetime.datetime.strptime(end_date, '%Y-%m-%d')
            query_set.add(Q(timestamp__range=(s_date, e_date)), Q.AND)
        history_list = ModerationEventRecorder.objects.filter(query_set).order_by('-id')[page*history_items_capacity:history_items_capacity*(page+1)]
        total_count = ModerationEventRecorder.objects.filter(query_set).order_by('-id').count()

        pre_filled= {}
        pagination={}
        #pagination object for displaying paginator
        pagination['total_pages'] = (total_count + history_items_capacity - 1)/20
        pagination['current_page'] = page + 1
        pagination['previous_page'] = page - 1
        pagination['next_page'] = page + 1
        pre_filled['pagination']=pagination
        # Object for prefilled input data fields
        pre_filled['moderator_list'] = moderator_list
        pre_filled['app_instance_list'] = app_instance_list
        pre_filled['selected_user_id'] = user_id
        pre_filled['selected_ai'] = app_instance
        selected_dates = {'start_date': start_date, 'end_date': end_date}
        pre_filled['selected_dates'] = selected_dates

        return render_to_response('mnews/history.html',
                                  {'history_list': history_list,
                                   'preFilled': pre_filled,
                                  })
    except exception as e:
        logger.info('Exception while getting eventRecorder data:' + str(e))
        print(e)

def login(request):
    if request.method=="POST":
        loginusername=request.POST['username']
        loginpassword=request.POST['password']
        user=authenticate(username= loginusername, password= loginpassword)
        if user is not None:
            auth.login(request, user)
            return redirect("/vapp/mnews/0/show")
        else:
            return render(request, "mnews/login.html")
    else:
        pass
    return render(request, "mnews/login.html")

def logout(request):
    auth.logout(request)
    return redirect('/vapp/mnews/0/show')
