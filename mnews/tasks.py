'''
Created on 22-Apr-2011

@author: bchandra
'''

from celery.task import Task

from telephony.utils import *
from telephony.inbound import FreeswitchSession
from telephony.statemachine import run_state_machine

from app_manager.models import App_instance, VI_conf
from app_manager.common import is_state_machine_impl, get_queue_for_task
from sms.tasks import SMSTask

from log import get_request_logger
from mnews.models import *
from app_manager.common import *
from utils import *
from vapp.stats.tasks import PopulateSessionDataTask

from django.db.models import Q
from datetime import timedelta, datetime, date
from operator import and_, or_
from local_settings import SERVER_ID, NO_DIAL_OUT_LIST, BUCKET_NAME

from mnews.stats import get_advanced_stats

logger = get_request_logger()


class PushTransGroupCallsTask(Task):
    
    def run(self, tasks = 1000, max_tries = 2):
        
        logger.info('We are going to schedule at most '+ str(tasks) + \
                    ' number of tasks today.')
        try:
            today = str(datetime.today().weekday())
            calls = Groups_call_log.objects.filter(success = False, \
                                                   tries__lt=max_tries, \
                                                   group_schedule__active = True, \
                                                   group_schedule__day_of_week__name__in=today) \
                                                   [0:tasks]                                                                                                     
            for call in calls:
                eta = datetime.combine(datetime.now().today(), call.group_schedule.start_time)
                queue = get_queue_for_task(call.ai, default_queue='push')
                CallAndPlayTask.apply_async([call.id, max_tries], eta=eta, queue=queue)
              
                if call.group_schedule.onpush_sms_message and max_tries <= 1:
                        message_details = call.group_schedule.onpush_sms_message
                        ai = call.group_schedule.get_group_ai()
                        message = message_details.message
                        SMSTask.create_send_msg(ai.id, message, call.number, eta = eta)
        
        except Exception,e:
            logger.info('exception thrown while running PushTransGroupCallsTask:' + str(e))


class CallAndPlayTask(Task):

    def run(self, call_log_id, max_tries = 2,  vi_data = {}, is_trunk = False):
        logger.debug('CallAndPlayTask: gcl_id=%s, max_tries = %s, vi_data = %s' %(call_log_id, max_tries, vi_data))
        max_tries = int(max_tries)
        call_log = Groups_call_log.objects.get(pk = call_log_id)
        vi_ref = call_log.group_schedule.ai_group.vi_reference
        
        vi_data_dict = {'vi_ref':vi_ref} if vi_ref else {}
        vi_data = dict( vi_data_dict.items() + RestrictedDict.convert_rdict_to_dict(vi_data).items() )

        app_instance = call_log.ai
        play_ai = call_log.group_schedule.play_ai
        file = call_log.group_schedule.get_prompt_file()
        vi_conf = ( VI_conf.objects.get( id = vi_data[ 'vi_conf_id' ] ) if ( vi_data and vi_data.has_key( 'vi_conf_id' )) else None )
            
        # at execution time, if the time has passed or this task is already
        # successful then we return and don't call him. If its already time
        # for today, then it needs to be rescheduled again.
        if not call_log.should_dial(max_tries):
            
            return
            
        logger.info('Calling ' + call_log.number + ' via for schedule: ' + str(call_log.group_schedule) + '. Play ai: ' + str(play_ai))
            
        vi_data[ 'group_call_log' ] = call_log
        vi_data[ 'max_tries' ] =  int(max_tries)
        vi_data['is_trunk'] = is_trunk
        vi_data['action'] = 'obd'
                
        if not vi_conf:                  
            vis = VI_conf.objects.filter(controller = 'app_manager.vi.CallAndPlayController', 
                                         description = 'app_manager.vi.CallAndPlayStateDescriptionMap')
            if vis.exists():
                vi_conf = vis[0]                
            else:
                logger.error("Could not find Call and Play VI Conf. Dial out has not happened.")
                                   
        if vi_conf and call_log.vi_conf != vi_conf:
            call_log.vi_conf = vi_conf
            call_log.save()
        if call_log.group_schedule.onpush_sms_message:
            message = call_log.group_schedule.onpush_sms_message.message
            print "Scheduling sms for %s" % (call_log.number,)
            SMSTask.create_send_msg(call_log.group_schedule.ai_group.ai.id, message, call_log.number, eta = datetime.now())
                
        session = run_state_machine(app_instance, False, call_log.number, viConf = vi_conf, vi_data = vi_data)
        if session:
           if not if_dict_values_empty(session.data.app_data):
              PopulateSessionDataTask.delay(session.data)

        
class DetailedStatsTask(Task):

    def run(self, ai_id, to_emails, output = None, start_date = None, end_date = None):
        from vapp.utils import send_email
        ai = App_instance.objects.get(pk = ai_id)
        if not (start_date and end_date):
            start_date, end_date = self.get_start_end_date()
        output = output or '/tmp/%s_%s_detailed_statistics.xlsx' % (ai_id, ai.name)
        get_advanced_stats(ai, start_date, end_date, output, True)
        subject = 'Monthly Detailed Statisics for %s_%s' % (ai_id, ai.name)
        body = 'Please find attached monthly statistics report.'
        send_email(subject, body, to_emails, attachments = output)

    def get_start_end_date(self):
        today = date.today()
        end_date = today.replace(day = 1)
        start_date = (end_date - timedelta(days = 1)).replace(day = 1)
        return start_date, end_date


class CollectionSaveTask(Task):

    def run(self, collection):
        try:
            args = []
            kwargs = {}
            tags = collection.tags.split(",")
            categories = collection.categories.all()
            locations = collection.locations.all()
            ais = collection.ais.all()
            operations = { "and": and_ , "or": or_ }

            if tags:
                tags_filter = reduce( operations[ collection.tags_relation ] , [ Q( tags__contains = tag ) for tag in tags ] )
                args.append( tags_filter )
            if categories:
                cat_filter = reduce( operations[ collection.category_relation ] , [ Q( category = cat ) for cat in categories ] )
                args.append( cat_filter )
            if locations:
                loc_filter = reduce( or_ , [ Q( location = loc ) for loc in locations ] )
                args.append( loc_filter )
            if ais: 
                kwargs['ai__in'] = collection.ais.all()
            if collection.start:
                kwargs['time__gt'] = collection.start
            if collection.end:
                kwargs['time__lt'] = collection.end
            if collection.max_items:
                max_items = collection.max_items
            kwargs['state__in'] = [ News_state.PUB, News_state.ARC ]

            news = News.objects.filter( *args, **kwargs ).exclude(transcript__exact='').order_by('-modified_date')[:max_items]        
            news_collection, news_collection_created = News_collection.objects.get_or_create( collection = collection )
            if news_collection_created:       
                news_collection.save()
            news_collection.news_items = news

        except Exception,e:
            logger.exception("Exception in fetching News in Collection" + str(e))

class AutoRejectTask(Task):
    def run(self, news_id):
        news = News.objects.filter(id=news_id)
        logger.info("For Item_id : "+str(news[0].id))
        news_detail = news[0].detail
        if news_detail:
            news_detail_path = news_detail.get_full_filename()
        rej_state = get_news_state(news_detail_path)
        logger.info("checksum for AutoRejectTask : " + str(news[0].checksum))
        if rej_state:
            tag_name = "autorej"
            if news[0].tags:
                tags_list = [tag.strip() for tag in news[0].tags.split(',')]
                if tag_name not in tags_list:
                    tags_list.append(tag_name)
                tags = ','.join(tags_list)
            else:
                tags = tag_name
            news.update(tags=tags)

class PubOrderSyncTask(Task):
    def run(self, channel_id):
        pub_list_channel = []
        channel = Channel.objects.get(id = channel_id)
        if channel.pub_list:
            pub_list_channel = eval(channel.pub_list)
        logger.info("pub_list"+str(pub_list_channel))
        #old_pub_list_channel =  pub_list_channel
        news = News.objects.filter(channel__id = channel_id, state = 'PUB', is_comment = False)
        pub_dict_news = dict((int(x.pk), x.pub_order) for x in news.order_by('pub_order'))
        logger.info("dict::::"+str(pub_dict_news))
        #remove items from pub_list_channel which are not in dict
        new_pub_list_channel = [x for x in pub_list_channel if x in pub_dict_news.keys()]
        logger.info("Before update of pub_list"+str(new_pub_list_channel))
        for item_id, pub_order in pub_dict_news.iteritems():
            if item_id not in new_pub_list_channel:
                if pub_order < 3:
                    new_pub_list_channel.insert(0, item_id)
                else:
                    new_pub_list_channel.append(item_id)
        logger.info("After update of pub_list" + str(new_pub_list_channel))
        for counter, item_id in enumerate(new_pub_list_channel):
            if counter < 40:
                if counter != pub_dict_news.get(item_id):
                    news.filter(id=item_id).update(pub_order=counter)
            else:
                if 9999 != pub_dict_news.get(item_id):
                    news.filter(id=item_id).update(pub_order=9999)
        if new_pub_list_channel != pub_list_channel:
            channel.pub_list = str(new_pub_list_channel)
            channel.save()

class UploadtoS3Task(Task):
      def run(self, news_id):
          news_query_set = News.objects.filter(id = news_id)
          news = news_query_set[0]
          pkey = SERVER_ID+"-ai-"+str(news.ai.id)
          skey = str(news_id)
          from mnews.models import S3_sync_log
          if news.should_upload_audio_to_s3() :
             if news.state != 'REJ': #todos - remove this. Not required
                logger.info('Audio qualifies to upload. Uploading') #todos - better logging
           	filename = 'audios/'+str(news.detail.id)+'.mp3'
                modified_file_path = '/tmp/'+str(news.detail.id)+'.mp3'
             	modified_file_path = ffmpeg_modify_mp3(news.detail.get_full_filename(), modified_file_path)
             	status, s3_url, exception = push_to_s3(modified_file_path, BUCKET_NAME, filename, 'audio/mp3')
             
            
             	if status: 
                   sync_properties = {}
                   try:
                      sync_properties = json.loads(news.sync_properties)
                   except Exception as e:
                      logger.exception(str(e))
                   sync_properties['s3_audio_url'] = s3_url
                   sync_properties['checksum'] = str(news.checksum)
		   news_query_set.update(sync_properties = json.dumps(sync_properties))
                   s3_log = S3_sync_log(sync_type='Audio', news = news, success=True, exception = exception)
                   s3_log.save()

                else:
                   s3_log = S3_sync_log(sync_type='Audio', news=news, success=False, exception=exception)
                   s3_log.save()
          if news.sm_image and news.should_upload_image_to_s3():
                news_query_set = News.objects.filter(pk = news_id)
                news = news_query_set[0]
                logger.info('Image qualifies to upload. Uploading')
                item_mod_time = ""
                image_name=str(news.detail.id)
                item_image_path = news.sm_image.get_location()
                if os.path.isfile(item_image_path):
                   image_mod_time = datetime.fromtimestamp(os.path.getmtime(item_image_path))
                   image_modified_date = str(image_mod_time).replace(' ','_') 
                   image_name=image_name+'_'+image_modified_date    
                from PIL import Image
                img = Image.open(item_image_path)
                img_format = img.format
                cType = 'image/' + img_format.strip().lower()
                ext = item_image_path.rsplit(".", 1)[-1]
                filename = 'images/'+str(image_name)+'.'+ext
                from mvapp.sync import convert_image
                hdpi_image_filepath, hdpi_image_filename = convert_image(item_image_path, image_name, ext)
                status_hdpi, s3_hdpi_url, hdpi_exception = push_to_s3(hdpi_image_filepath[0], BUCKET_NAME, "images/"+hdpi_image_filename[0], cType=cType)
                status, s3_url, exception = push_to_s3(item_image_path, BUCKET_NAME, filename, cType=cType)
                if status:
                   sync_properties = {}
                   try:
                      sync_properties = json.loads(news_query_set[0].sync_properties)
                   except Exception as e:
                      logger.excetpion(str(e))
                   sync_properties['s3_image_url'] = s3_url
                   sync_properties['s3_image_modified_time'] = str(image_modified_date)
                   news_query_set.update(sync_properties=json.dumps(sync_properties))
                   s3_log = S3_sync_log(sync_type='Image', news = news, success=True, exception = exception)
                   s3_log.save()
                else:
                   s3_log = S3_sync_log(sync_type='Image', news= news, success=False, exception=exception)
                   s3_log.save()
         
          news_dict = model_to_dict(News.objects.get(pk = news_id))
          update_to_dynamo('Items', pkey, skey, json.dumps(news_dict, cls = DjangoJSONEncoder))
                 
                
                    

               
              
                 

           
          
  
