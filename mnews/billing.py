from django.db.models.query import QuerySet
from vapp.billing.utils import get_common_billing_infos, seconds_to_units, remove_exclusion_duplicates
from vapp.billing.utils import Billing_info, Billing_summary, Billing_detail, Call_data
from models import Item_heard_stats

def get_billing_info(startdate, enddate, ai, composite = True, unit_size = 60, excludes = []):
    infos = get_common_billing_infos(startdate, enddate, ai, composite, unit_size, excludes)
    
    ex_tags = [ exclude for exclude in excludes if exclude.discount_type == exclude.TO ]
    for ex_tag in ex_tags:
        tags_info = get_tags_or_exclusion_info(ex_tag.get_formatted_values(), startdate, enddate, ai, unit_size)
        tags_info.discount_type = ex_tag.TO
        tags_info.discount_values = ex_tag.discount_values
        infos.append(tags_info)

    ex_channels = [ exclude for exclude in excludes if exclude.discount_type == exclude.CH ]
    for ex_channel in ex_channels:
        channels_info = get_channels_exclusion_info(ex_channel.get_formatted_values(), startdate, enddate, ai, unit_size)
        channels_info.discount_type = ex_channel.CH
        channels_info.discount_values = ex_channel.discount_values
        infos.append(channels_info)

    remove_exclusion_duplicates(infos)
    return infos

def add_item_heard(info, heard):
    info.summary.count += 1
    info.summary.duration += heard.duration
    info.summary.units += seconds_to_units(heard.duration, info.unit_size)

    call_data = info.detail.data.get(heard.cdr.id)
    if call_data:
        #slightly in accurate since when_heard info is not tracked, but is easier to present
        call_data.duration += heard.duration 
    else:
        info.detail.data[heard.cdr.id] = Call_data(heard.cdr.id, heard.cdr.callerid, heard.when_heard, heard.duration)

    return info

def get_channels_exclusion_info(ex_channels, startdate, enddate, ai, unit_size):
    info = Billing_info()
    info.name = 'Channel Items Discount'
    info.unit_size = unit_size

    chan_item_heards = [ Item_heard_stats.objects.filter(ai = ai, when_heard__gte = startdate, when_heard__lt = enddate, item__channel_id = int(channel_id))
                         for channel_id in ex_channels ]
    item_heards = reduce(QuerySet.__or__, chan_item_heards)
    return reduce(add_item_heard, item_heards, info)

def get_tags_or_exclusion_info(ex_tags, startdate, enddate, ai, unit_size):
    info = Billing_info()
    info.name = 'Tagged Items Discount'
    info.unit_size = unit_size

    tag_heards = [ Item_heard_stats.objects.filter(ai = ai, when_heard__gte = startdate, when_heard__lt = enddate, item__tags__contains = tag)
                   for tag in ex_tags ]
    item_heards = reduce(QuerySet.__or__, tag_heards)

    return reduce(add_item_heard, item_heards, info)

