from vapp.app_manager.common import get_queue_for_task
from vapp.app_manager.models import App_instance, VI_conf
from vapp.mnews.models import *
from vapp.mnews.models import ModerationEventRecorder as OtherEventsRecorder
from vapp.mnews.models import ModerationEvents as MnewsOtherEvents
from mnews.tasks import *
from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.utils import *
from sms.tasks import SMSTask
from advert.utils import get_ads_for_pos, log_impression, get_target_reference_for_ad, log_ad_lead, send_ad_message
 
from datetime import datetime, timedelta
import stat

from vapp.log import get_logger
logger = get_logger()


class MnewsNoContribAnnounceController( BaseVappController ):
    MIN_RECORDING_DUR = 25*1000

    currentChannel = None

    @classmethod
    def shouldSaveItem(cls, news, min_duration):
        if not news:
            return False
 
        if ( (not news.summary and not news.detail) or
             ((not news.summary) and news.detail and mp3_duration(news.detail.get_full_filename()) < min_duration) or
             ((not news.detail) and news.summary and mp3_duration(news.summary.get_full_filename()) < min_duration)):
            logger.info('News ought to be deleted:' + str(news))
            return False
        else:
            return True
    
    @classmethod
    def validateAndSaveItem(cls, item, parent = None, min_duration = MIN_RECORDING_DUR):
        if cls.shouldSaveItem(item, min_duration):
            item.modified_date = datetime.now()
            item.save()
            if item.is_comment:
                parent.comments.add( item )
        else:
            item.delete()
    
    @classmethod
    def scheduleForwardingCall(cls, forwarding_log):      
        
        vi_conf = VI_conf.objects.get(controller = 'mnews.forwardingvi.ForwardingController', 
                                            description = 'mnews.forwardingvi.ForwardingStateDescriptionMap')    
        
        properties = Forwarding_ai_properties.objects.get( ai = forwarding_log.ai )        
        vi_data = RestrictedDict.convert_dict_to_rdict({ 'forwarding_log_id': forwarding_log.id, 'forwarding_properties_id': properties.id, 'vi_conf_id': vi_conf.id })
        ai = properties.ai_to_jump
        group_call_log = Groups_call_log(number = forwarding_log.forwarded_to, group_schedule = properties.forwarding_schedule, ai = ai)
        group_call_log.save()
        
        forwarding_gc_log = Forwarding_group_call_log(forwarding_log = forwarding_log, group_call_log = group_call_log )
        forwarding_gc_log.save()
        queue = get_queue_for_task(ai, default_queue='push')
        CallAndPlayTask.apply_async(args=[group_call_log.id, properties.max_tries], 
                                    kwargs={'vi_data': vi_data}, eta=datetime.now(), queue=queue)
                                          

    @classmethod
    def getInlineAdPosition(cls, last_news_pos):
        return "MNEWS_INLINE_%s_%s" % (last_news_pos, last_news_pos + 1)

    def initNewsAndComments(self):
        self.news = News.objects.filter(ai = self.ai.id, channel = self.currentChannel, state = 'PUB', is_comment = False).order_by('pub_order')[:self.currentChannel.max_news]
        self.comments = {}
        for news in self.news:
            self.comments[ news ] = news.comments.filter( state = 'PUB' )
        self.currentNews = 0
        self.newNews = None
        self.newComment = None        

    def initChannelAds(self):
        self.currentChannelAd = -1
        try:
            #count = Position_ad_count.objects.get(position = 'MNEWS_CHANNEL', ai = self.ai)
            self.channelAds = get_ads_for_pos(self.ai, 'MNEWS_CHANNEL', self.sessionData.callerid)
            adIds = [ ad.id for ad in self.channelAds ]
            self.channelAds = Advertisement.objects.filter(ad_channel_map_set__channel = self.currentChannel, 
                                                           id__in = adIds)
        except:
            pass

    def logViTransitionData(self, transition_event):
        if self.currentChannel:
            data = Vi_transition_data(transition_event = transition_event, channel = self.currentChannel)
            data.save()

    def __init__(self, ai, sessionData, vi_data = {}):
        super(MnewsNoContribAnnounceController, self).__init__( ai, sessionData, vi_data )  
        if self.vi_data.has_key('channel') and isinstance(self.vi_data['channel'], Channel):                    
            self.currentChannel = self.vi_data['channel']
        elif self.vi_data.has_key('channel_id'):
            self.currentChannel = Channel.objects.get(pk = self.vi_data['channel_id'])
        else:
            self.currentChannel = Channel.get_default_channel(ai.id)        
        self.vi_data = {}   
        
        self.networkAds = self.channelAds = self.inlineAds = None

        #temporary hack for sending SMS message on call
        message = OtherEventsRecorder.get_message_for_event(self.ai, MnewsOtherEvents.PLAY_MNEWS_WELCOME_START)
        if message:
            logger.info('we got a message:' + str(message))
            sms = SMSTask.create_send_msg(self.ai.id, message, self.sessionData.cdrs[0].callerid, max_tries = 3)
    
    def getPromptForCurrentChannel(self, prompt):
        return prompt + '__' + self.currentChannel.name
    
    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid )
    
    def pre_incomingstart(self):
        return self.getIncomingCallParams()
        
    def pre_welcome(self):       
        if self.vi_data.has_key('channel') and isinstance(self.vi_data['channel'], Channel):        
            self.currentChannel = self.vi_data['channel']               
        elif self.vi_data.has_key('channel_id'):                    
            self.currentChannel = Channel.objects.get(pk = self.vi_data['channel_id'])

        self.vi_data = {}                    
        self.initNewsAndComments()

        max_dur = self.ai.app_instance_settings_set.all()[0].max_duration
        self.min_recording_duration = ( self.ai.app_instance_settings_set.all()[0].min_recording_duration*1000 )
        
        self.ai_timeout = datetime.now() + timedelta(seconds = max_dur) if max_dur else None

        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_welcome') )


    def pre_thankyou(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_thank_you') )
    
    def pre_instructions(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_instructions') )
    
    def pre_listnews(self, prevState, event):
        news = self.news[ self.currentNews ]
        return self.getPlaybackParams( news.detail.get_full_filename() )
    

    def while_nextnews__sm_action_success(self, event, eventsData):
        return self.nextNewsEvent()

    def nextNewsEvent(self):
        if self.ai_timeout and self.ai_timeout <= datetime.now():
            return 'sm_ai_time_out'

        if self.currentNews < self.news.count() - 1:
            self.currentNews += 1
            return 'mnews_next_news'
        else:
            return 'mnews_news_done'

    def nextCommentOrAdEvent(self):
        if self.ai_timeout and self.ai_timeout <= datetime.now():
            return 'sm_ai_time_out'

        if self.currentComment < self.comments[ self.news[ self.currentNews ] ].count() - 1:
            self.currentComment += 1
            return 'mnews_next_comment'
        else:
            return 'advert_play_inline_ad'
            #return self.nextNewsEvent()

    def prevNewsEvent(self):
        if self.currentNews > 0:
            self.currentNews -= 1
        else:
            self.currentNews = 0
        return 'mnews_prev_news'
    
    def while_listnews__dtmf_4(self, events, eventData):
        return self.prevNewsEvent()

    def while_listcomments__dtmf_4(self, events, eventData):
        return self.prevNewsEvent()

    def while_listnews__sm_action_success(self, events, eventData):
        
        current_news = self.news[ self.currentNews ]
                 
        if self.comments[ current_news ]:
            self.currentComment = 0
            return 'mnews_start_comments'
        else:
            return 'advert_play_inline_ads'


    def while_listnews__dtmf_9(self, events, eventData):
        try:
            reference = News_vi_reference.objects.get(news = self.news[ self.currentNews ]).vi_reference.reference
            return '@' + reference
        except:
            pass
 

    def pre_listcomments(self):
        comment = self.comments[ self.news[ self.currentNews ] ][ self.currentComment ]
        return self.getPlaybackParams( comment.detail.get_full_filename() )
    
    def while_listcomments__sm_action_success(self, events, eventData):
        return self.nextCommentOrNewsEvent()
        
    def pre_recordcommentprompt(self, prevState):
        self.prevRecordCommentState = prevState
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_leave_comment') )
    
    def pre_recordcommentrecord(self):
        title = 'Comment, %s' % datetime.now().strftime('%H:%M, %d %B %Y')
        self.newComment = News(ai_id=self.ai.id, channel=self.currentChannel,
                               callerid=self.sessionData.callerid,
                               state='UNM', is_comment=True,
                               source=News.Source.VOICE_INTERFACE, title=title, creation_cdr=self.sessionData.cdrs[0])
        parent = self.news[ self.currentNews ]
        self.newComment.parent_id_value = parent.id
        return self.getEmbeddedRecordingParams( self.ai.record_duration_limit_seconds, self.newComment, News.detail )
    
    def while_recordcommentrecord__sm_action_success(self, events, eventData):
        MnewsNoContribAnnounceController.validateAndSaveItem(self.newComment, self.news[ self.currentNews ], self.min_recording_duration)

    def while_recordcommentrecord__sm_action_failure(self, events, eventData):
        MnewsNoContribAnnounceController.validateAndSaveItem(self.newComment, self.news[ self.currentNews ], self.min_recording_duration)

    def post_recordcommentrecord(self):
        self.newComment = None
        
    def while_recordcommentthankyou__sm_action_success(self, events, eventData):
        if self.prevRecordCommentState == 'listnews':
            return 'mnews_continue_news'
        else:
            return 'mnews_continue_comments'
     
    def pre_recordcommentthankyou(self):
        return self.getPromptParams(self.getPromptForCurrentChannel('mnews_ack_comment'))

    def pre_contributeprompt(self, prevState):
        self.prevRecordNewsState = prevState
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_contrib_record') )
            
    def pre_contributerecord(self):
        title = 'News, %s' % datetime.now().strftime('%H:%M, %d %B %Y')
        self.newNews = News(ai_id=self.ai.id, channel=self.currentChannel,
                            callerid=self.sessionData.callerid,
                            state='UNM', is_comment=False,
                            source=News.Source.VOICE_INTERFACE, title=title, creation_cdr=self.sessionData.cdrs[0])
        return self.getEmbeddedRecordingParams( self.ai.record_duration_limit_seconds, self.newNews, News.detail )

    def while_contributerecord__sm_action_success(self, events, eventData):
        MnewsNoContribAnnounceController.validateAndSaveItem(self.newNews, min_duration = self.min_recording_duration)

    def while_contributerecord__sm_action_failure(self, events, eventData):
        MnewsNoContribAnnounceController.validateAndSaveItem(self.newNews, min_duration = self.min_recording_duration)

    def post_contributerecord(self):
        self.newNews = None

    def pre_contributethankyou(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_contrib_thank_you') )
    
    def while_contributethankyou__sm_action_success(self, events, eventData):
        if self.prevRecordNewsState in ('listnews', 'listcomments', 'instructions', 'welcome'):
            return 'mnews_continue_news'
        else:
            return 'mnews_action_success'

    def while_nextchannel__sm_action_success(self, events, eventData):
        
        next_channel = Channel_order.get_next_channel( self.currentChannel, self.sessionData.initial_ai )        
        if next_channel:            
            self.currentChannel = next_channel
            if next_channel.ai == self.ai:
                return 'mnews_next_channel'
            else:
                self.vi_data['channel'] = next_channel
                return '@ai_' + str(self.currentChannel.ai.id) + '_welcome'
        else:
            self.currentChannel = Channel.get_default_channel(self.ai.id)
        return '@nextai'

    def while_prevchannel__sm_action_success(self, events, eventData):
        
        prev_channel = Channel_order.get_prev_channel( self.currentChannel, self.sessionData.initial_ai )        
        if prev_channel:            
            self.currentChannel = prev_channel
            if prev_channel.ai == self.ai:
                return 'mnews_prev_channel'   
            else:
                self.vi_data['channel'] = prev_channel  
                return '@ai_' + str(self.currentChannel.ai.id) + '_welcome'       
        else:
            self.currentChannel = Channel.get_default_channel(self.ai.id)
        return '@prevai'

    def while_checknewsavailable__sm_action_success(self, events, eventData):
        if self.news:
            return 'mnews_continue_news'
        else:
            return 'mnews_no_news'
    
        
    def pre_postbookmarkprompt(self, prevState):
        self.prevBookmarkState = prevState
        if self.news[ self.currentNews ]:            
            news = self.news[ self.currentNews ]                       
            news.bookmark_news_and_send_sms(self.sessionData.cdrs[0], 'ITB')
        return self.getPromptParams( 'mnews_bookmark_notify' )
    
    
    def pre_playbookmarkitem(self, prevState):
        self.prevBookmarkState = prevState
        return self.getPromptPlayAndGetParams( promptName = 'mnews_enter_bookmark', maxDigits = 9, tries = 3 )
    
    def while_playbookmarkitem__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if Bookmark.objects.filter(bookmark_id = digits, ai_id = self.ai.id).exists():
            self.news_bookmark_id = int(digits)
            return 'mnews_valid_bookmark'
        else:
            return 'mnews_invalid_bookmark'
        
    def pre_playmarkeditem(self):
        bookmark = Bookmark.objects.get(bookmark_id = self.news_bookmark_id, ai_id = self.ai.id)        
        news = bookmark.news
        blog = Bookmark_log(bookmark = bookmark, cdr = self.sessionData.cdrs[0], accessed = True, bookmarked = False, source = 'BIA' )
        blog.save()        
        return self.getPlaybackParams( news.detail.get_full_filename())
    
    def while_playmarkeditem__sm_action_success(self, events, eventData):
        if self.prevBookmarkState == 'listnews':
            return 'mnews_continue_news'
        elif self.prevBookmarkState == 'listcomments':
            return 'mnews_continue_comments'
        return 'sm_action_success'        
        
    def while_playmarkeditem__dtmf_9(self, events, eventData):
        try:
            news = Bookmark.objects.get(bookmark_id = self.news_bookmark_id, ai_id = self.ai.id).news
            reference = News_vi_reference.objects.get(news = news).vi_reference.reference
            return '@' + reference
        except:
            pass
    
    def pre_invalidbookmark(self):
        return self.getPromptParams( 'mnews_invalid_bookmark' )
    
    def pre_forwarditemprompt(self, prevState):
        self.prevForwardState = prevState
        self.forwarding_properties = Forwarding_ai_properties.objects.get( ai = self.ai )
        return self.getPromptPlayAndGetParams( 'mnews_forward_prompt', timeout = 5000, tries = 1, minDigits = 1, maxDigits = 1 )
    
    def while_forwarditemprompt__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if int(digits) == 1:
            return 'mnews_forward_input'
        else:            
            return 'mnews_no_forward_num'   
            
    
    def pre_forwardcurrentitem(self, prevState):
        return self.getPromptPlayAndGetParams( 'mnews_forward_input_number', timeout = 10000, tries = 3, minDigits = 10, maxDigits = 10, errorPromptName = 'mnews_forward_invalid_number' )
    
    
    def while_forwardcurrentitem__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)        
        if self.news[ self.currentNews ]:            
            news = self.news[ self.currentNews ]
            forwarding_log = Forwarding_log(ai = self.ai, forwarding_cdr = self.sessionData.cdrs[0], forwarded_item = news, forwarded_to = str(digits), forwarding_event = "FFC")
            forwarding_log.save()            
            self.forwarding_log = forwarding_log                  
    
    def pre_recordpmprompt(self, prevState):
        return self.getPromptParams( 'mnews_pm_prompt' )
    
    def pre_recordpm(self, prevState):
        return self.getEmbeddedRecordingParams(self.forwarding_properties.personal_msg_time_secs, self.forwarding_log, Forwarding_log.forwarder_personal_recording )
    
    
    def pre_forwardthankyou(self, prevState):        
        self.scheduleForwardingCall(self.forwarding_log)
        return self.getPromptParams( 'mnews_forward_thank_you' )
    
    def while_forwardthankyou__sm_action_success(self, events, eventData):
        if self.prevForwardState == 'listnews':
            return 'mnews_listnews_next'
        elif self.prevForwardState == 'playmarkeditem':
            return 'mnews_welcome_next'
        elif self.prevForwardState == 'listcomments':
            return 'mnews_listcomments_next'
        return 'sm_action_success'


    def while_checknetworkads__sm_action_success(self, events, eventData):
        if self.networkAds is None:
            self.currentNetworkAd = -1
            try:
                #count = Position_ad_count.objects.get(position = 'MNEWS_NETWORK', ai = self.ai)
                self.networkAds = get_ads_for_pos(self.ai, 'MNEWS_NETWORK', self.sessionData.callerid)
            except Exception,e:
                logger.error("Got Exception:" + str(e))

        self.currentNetworkAd += 1
        if self.networkAds and self.currentNetworkAd < len(self.networkAds):
            return 'advert_play_ad'
        else:
            return 'advert_ads_done'

    def pre_playnetworkads(self):
        self.networkAdStartTime = datetime.now()
        return self.getPlaybackParams( self.networkAds[self.currentNetworkAd].ad_recording.get_full_filename() )
        
    def while_playnetworkads__sm_action_success(self, events, eventData):
        log_impression(self.networkAds[self.currentNetworkAd], 'MNEWS_NETWORK', self.sessionData.cdrs[0], 
                       self.networkAdStartTime, datetime.now(), self.ai, 
                       self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
        send_ad_message(self.networkAds[self.currentNetworkAd], self.ai, self.sessionData.callerid)
        return 'sm_action_success'
        
    def while_playnetworkads__dtmf_9(self, events, eventData):
        ref = get_target_reference_for_ad(self.networkAds[self.currentNetworkAd], self.ai)
        if ref:
            vi_ref, embeddable = ref
            log = log_impression(self.networkAds[self.currentNetworkAd], 'MNEWS_NETWORK', self.sessionData.cdrs[0], 
                                 self.networkAdStartTime, datetime.now(), self.ai, 
                                 self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
            send_ad_message(self.networkAds[self.currentNetworkAd], self.ai, self.sessionData.callerid)
            log_ad_lead(log)
            if embeddable:
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'listnews') )
            return vi_ref
        return ref

    def while_checkchannelads__sm_action_success(self, events, eventData):
        if self.channelAds is None:
            self.initChannelAds()

        self.currentChannelAd += 1
        if self.channelAds and self.currentChannelAd < len(self.channelAds):
            return 'advert_play_ad'
        else:
            self.channelAds = None
            return 'advert_ads_done'

    def pre_playchannelads(self):
        self.channelAdStartTime = datetime.now()
        return self.getPlaybackParams( self.channelAds[self.currentChannelAd].ad_recording.get_full_filename() )

    def while_playchannelads__sm_action_success(self, events, eventData):
        log_impression(self.channelAds[self.currentChannelAd], 'MNEWS_CHANNEL', self.sessionData.cdrs[0], 
                       self.channelAdStartTime, datetime.now(), self.ai, 
                       self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
        send_ad_message(self.channelAds[self.currentChannelAd], self.ai, self.sessionData.callerid)
        return 'sm_action_success'


    def while_playchannelads__dtmf_9(self, events, eventData):
        ref = get_target_reference_for_ad(self.channelAds[self.currentChannelAd], self.ai)
        if ref:
            vi_ref, embeddable = ref
            log = log_impression(self.channelAds[self.currentChannelAd], 'MNEWS_CHANNEL', self.sessionData.cdrs[0], 
                                 self.channelAdStartTime, datetime.now(), self.ai, 
                                 self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
            send_ad_message(self.channelAds[self.currentChannelAd], self.ai, self.sessionData.callerid)
            log_ad_lead(log)
            if embeddable:
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'listnews') )
            return vi_ref
        return ref

            
    def while_checkinlineads__sm_action_success(self, events, eventData):
        if self.inlineAds is None:
            self.currentInlineAd = -1
            try:
                position = self.getInlineAdPosition(self.currentNews)
                #count = Position_ad_count.objects.get(position = position, ai = self.ai)
                self.inlineAds = get_ads_for_pos(self.ai, position, self.sessionData.callerid)
            except Exception,e:
                logger.error("Got Exception:" + str(e))

        self.currentInlineAd += 1
        if self.inlineAds and self.currentInlineAd < len(self.inlineAds):
            return 'advert_play_ad'
        else:
            self.inlineAds = None
            return 'advert_ads_done'
        
    def pre_playinlineads(self):
        self.inlineAdStartTime = datetime.now()
        return self.getPlaybackParams( self.inlineAds[self.currentInlineAd].ad_recording.get_full_filename() )

    def while_playinlineads__sm_action_success(self, events, eventData):
        position = self.getInlineAdPosition(self.currentNews)
        log_impression(self.inlineAds[self.currentInlineAd], position, self.sessionData.cdrs[0], 
                       self.inlineAdStartTime, datetime.now(), self.ai, 
                       self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
        send_ad_message(self.inlineAds[self.currentInlineAd], self.ai, self.sessionData.callerid)
        return 'sm_action_success'


    def while_playinlineads__dtmf_9(self, events, eventData):
        position = self.getInlineAdPosition(self.currentNews)
        ref = get_target_reference_for_ad(self.inlineAds[self.currentInlineAd], self.ai)
        if ref:
            vi_ref, embeddable = ref
            log = log_impression(self.inlineAds[self.currentInlineAd], position, self.sessionData.cdrs[0], 
                                 self.inlineAdStartTime, datetime.now(), self.ai, 
                                 self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
            send_ad_message(self.inlineAds[self.currentInlineAd], self.ai, self.sessionData.callerid)
            log_ad_lead(log)
            if embeddable:
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'listnews') )
            return vi_ref
        return ref


MnewsNoContribAnnounceStatesMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'] 
            } 
        },
    {   'name':'welcome', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checknetworkads':['sm_action_success', 'mnews_continue_news'],
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],            
            'playbookmarkitem': ['mnews_play_bookmarked_item']
            }, 
        'dtmf': {
            1:'mnews_continue_news',
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',            
            }
        },
    {   'name':'checknetworkads',
        'action':'none',
        'transitions': {
            'checkchannelads':['advert_ads_done'],
            'playnetworkads':['advert_play_ad'],
            'stop':['sm_action_failure'],
            },
        },
    {
        'name':'playnetworkads',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checknetworkads': ['sm_action_success'],
            },
        },
    {
        'name':'checkchannelads',
        'action': 'none',
        'transitions': {
            'stop': ['sm_action_failure'],
            'instructions': ['advert_ads_done'],
            'playchannelads': ['advert_play_ad'],
            },
        },
    {
        'name':'playchannelads',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checkchannelads': ['sm_action_success'],
            },
        },
    {   'name':'instructions', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checknewsavailable':['sm_action_success', 'mnews_continue_news'],
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],            
            'playbookmarkitem': ['mnews_play_bookmarked_item']
            }, 
        'dtmf': {
            1:'mnews_continue_news',
            3:'mnews_record_news',
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',            
            6:'mnews_play_bookmarked_item'
            }
        },
    {
        'name':'checkinlineads',
        'action':'none',
        'transitions': {
            'stop': ['sm_action_failure'],
            'playinlineads': ['advert_play_ad'],
            'nextnews': ['advert_ads_done'],
            },
        },
    {
        'name':'playinlineads',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkinlineads':['sm_action_success'],
            },
        },
    {
        'name': 'nextnews',
        'action': 'none',
        'transitions': {
            'stop':['sm_action_failure', 'sm_ai_time_out'],
            'thankyou':['mnews_news_done'], 
            'listnews':['mnews_next_news'],
            },
        },
    {   'name': 'checknewsavailable',
        'action': 'none',
        'transitions': {
            'listnews':['mnews_continue_news'],
            'thankyou': ['mnews_no_news'],
            'stop':['sm_action_failure']
            },            
        },
    {   'name':'listnews', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure', 'sm_ai_time_out'], 
            'listnews':['mnews_prev_news'], 
            'checkinlineads':['advert_play_inline_ads'],
            'listcomments':['mnews_start_comments'], 
            'recordcommentprompt':['mnews_record_comment'], 
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],
            'postbookmarkprompt':['mnews_news_bookmarked'],
            'playbookmarkitem': ['mnews_play_bookmarked_item']
            }, 
        'dtmf': { 
            1:'advert_play_inline_ads',
            2:'mnews_record_comment', 
            3:'mnews_record_news',
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',
            5:'mnews_news_bookmarked',
            6:'mnews_play_bookmarked_item'
            }     
        },
    {   'name':'listcomments', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure', 'sm_ai_time_out'], 
            'listcomments':['mnews_next_comment'], 
            'listnews':['mnews_prev_news'], 
            'checkinlineads':['advert_play_inline_ads'],
            'recordcommentprompt':['mnews_record_comment'], 
            'thankyou':['mnews_news_done'], 
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],
            'postbookmarkprompt':['mnews_news_bookmarked'],
            'playbookmarkitem': ['mnews_play_bookmarked_item']
            }, 
        'dtmf': { 
            1:'advert_play_inline_ads',
            2:'mnews_record_comment', 
            3:'mnews_record_news', 
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',
            5:'mnews_bookmark_item',
            6:'mnews_play_bookmarked_item'
            } 
        },
    {    'name':'recordcommentprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'recordcommentrecord':['sm_action_success'] 
            } 
         },
    {    'name':'recordcommentrecord', 
         'action':'record', 
         'transitions': { 
            'stop':['sm_action_failure'],
            'recordcommentthankyou':['sm_action_success']
            }
         },
    {    'name': 'recordcommentthankyou',
         'action': 'playback',
         'transitions': {
            'stop':['sm_action_failure'],
            'listnews':['mnews_continue_news'], 
            'listcomments':['mnews_continue_comments'] 
            }
         },
    {    'name':'contributeprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'contributerecord':['sm_action_success'] 
            } 
         },
    {    'name':'contributerecord', 
         'action':'record', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'contributethankyou':['sm_action_success'] 
            } 
         },
    {    'name':'contributethankyou', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure', 'sm_action_success'], 
            'checknewsavailable':['mnews_continue_news'] 
            } 
         },
    {    'name':'thankyou', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['sm_action_success', 'mnews_next_channel']
            },
         'dtmf': {
            '*':'mnews_prev_channel',
            0:'mnews_next_channel'
            }
         },
    {    'name':'stop', 
         'action':'hangup', 
         'transitions': {} 
         },
    {    'name': 'prevchannel',
         'action':'none',
         'transitions': {
            'welcome':['mnews_prev_channel'],
            'stop':['sm_action_failure']
            }
         },
    {    'name': 'nextchannel',
         'action':'none',
         'transitions': {
            'welcome':['mnews_next_channel'],
            'stop':['sm_action_failure']
            }
         },
    {    'name':'postbookmarkprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'listnews':['mnews_continue_news'], 
            'listcomments':['mnews_continue_comments'],
            'forwarditemprompt':['sm_action_success'],  
            } 
         },                             
    {    'name':'forwarditemprompt', 
         'action':'play_and_get_digits', 
         'transitions': {
            'stop':['sm_action_failure'],            
            'listnews':['sm_get_digits_no_digits', 'mnews_no_forward_num'],            
            'forwardcurrentitem':['mnews_forward_input'],  
            },       
         },
    
    {    'name':'forwardcurrentitem', 
         'action':'play_and_get_digits', 
         'transitions': { 
             'stop':['sm_action_failure'],
            'listnews':['sm_get_digits_no_digits'],            
            'recordpmprompt':['sm_action_success'],  
            }       
         },
    {    'name':'recordpmprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'],            
            'recordpm':['sm_action_success'],  
            }       
         },
    {    'name':'recordpm', 
         'action':'record', 
         'transitions': { 
            'stop':['sm_action_failure'],            
            'forwardthankyou':['sm_action_success'],  
            }       
         },
                             
    {    'name':'forwardthankyou', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'],           
            'listnews':['mnews_listnews_next', 'sm_action_success'],
            'listcomments':['mnews_listcomments_next'], 
            'welcome': ['mnews_welcome_next'],
        }       
    },
       
    {    'name':'playbookmarkitem', 
         'action':'play_and_get_digits', 
         'transitions': { 
            'stop':['sm_action_failure'],            
            'playmarkeditem':['mnews_valid_bookmark'],
            'invalidbookmark':['mnews_invalid_bookmark'],
            'listnews':['sm_action_success', 'sm_get_digits_no_digits']  
            },
         'dtmf': {
            6: 'sm_action_success',
            } 
         },
    {    'name':'invalidbookmark', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'listnews':['mnews_continue_news'], 
            'listcomments':['mnews_continue_comments'],
            'playbookmarkitem':['sm_action_success']  
            },
          'dtmf': { 
            6:'mnews_continue_news',                      
            }           
         },
    {    'name':'playmarkeditem', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'checknewsavailable':['mnews_continue_news', 'sm_action_success'], 
            'listcomments':['mnews_continue_comments'],  
            'postbookmarkprompt':['mnews_bookmark_item'],           
            },
         'dtmf': { 
            5:'mnews_bookmark_item',
            6:'mnews_continue_news',
            }           
         },
    ]

