from django.db import models
from django.core import serializers
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete, pre_delete, pre_save, m2m_changed
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, ForeignKey
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.core.files import File
from decimal import Decimal
from django.db import transaction
from pathlib import Path
import base64
import requests
from local_settings import NO_DIAL_OUT_LIST
import json
import RQMC
import json, urllib, uuid, os
import audiodiff

from django.contrib.syndication.views import FeedDoesNotExist
from django.shortcuts import get_object_or_404
from django.utils.feedgenerator import Rss201rev2Feed
from django.conf import settings
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder

from datetime import datetime, timedelta

from app_manager.common import get_queue_for_task
from app_manager.models import App_instance, CalleridField, Ai_field_properties, \
VI_conf, VI_reference, App_instance_settings, Cdr, Events, Transition_event, Data_sync

from media.models import Recording, Image_caption_map, Image, get_ai_prompt_set_path
from media.paths import RECORDINGS_DIR

from events import Event
from prompts import add_prompts_for_channel

from sms.models import SMS_template, SMS_message
from sms.tasks import SMSTask
from callerinfo.models import Contact_list, Contact
from advert.models import Advertisement
from campaign.models import Campaign_category
from location.models import Location

from vapp.caching_service.utils import sync_object_data

from mnews import feedgenerators
from callerinfo.models import DEFAULT_CALLERID
from vapp.social.models import SocialMedia 
from vapp.utils import get_dnd_status, ffmpeg_modify_mp3, push_to_s3, convert_image
from local_settings import SERVER_ID
from vapp.utils import update_to_dynamo,delete_obj_in_dynamo, get_file_creationtime
from django.db.models import F
from vapp.dynamo_utils import sync_news, sync_reordered_channel

import re
from log import get_request_logger
logger = get_request_logger()


CALLERID_LENGTH = 20
TITLE_LENGTH    = 255
LOCATION_LENGTH = 64
TAGS_LENGTH     = 64
MAX_BOOKMARK_NUMBER = 1000000
MAX_BOOKMARK_ID_GENERATION_TRIES = 10
PUB_ORDER_MAX = 9999
IMAGES_DIR = 'images/mnews'

DEFAULT_CHANNEL = "Main"

MAX_PUB_NEWS_ON_CHANNEL = 10

tags_for_call = ["grievance"]

class Tag(models.Model):
    name = models.CharField(max_length=TAGS_LENGTH)

    def __unicode__(self):
        return unicode(self.name)

class Category(models.Model):

    name = models.CharField(max_length = TAGS_LENGTH)
    parent = models.ForeignKey('Category', null = True, blank = True, related_name = 'Parent')
    subcategories = models.ManyToManyField('Category', null = True, blank = True, related_name = 'subcategory')
    is_subcategory = models.BooleanField()
    ai = models.ManyToManyField(App_instance)

    def __unicode__(self):
        if self.is_subcategory and self.parent:
            return (self.parent.__unicode__() + '->' + self.name)
        else:
            return self.name

class Format(models.Model):

    name = models.CharField(max_length = TAGS_LENGTH)

    def __unicode__(self):
        return self.name

class Qualifier(models.Model):

    name = models.CharField(max_length = TAGS_LENGTH)

    def __unicode__(self):
        return self.name

class Age_group(models.Model):

    name = models.CharField(max_length = TAGS_LENGTH)
    min_age = models.PositiveIntegerField( default = 0 )
    max_age = models.PositiveIntegerField()

    def __unicode__(self):
        return self.name

class Occupation(models.Model):
    name = models.CharField(max_length = TAGS_LENGTH)

    def __unicode__(self):
        return self.name

class Gender(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('T', 'Third Gender'),
        ('G', 'Group')

    )
    gender = models.CharField( max_length = 2, choices = GENDER_CHOICES )

    def __unicode__(self):
        return self.gender


class News(models.Model):

    STATE_CHOICES = (
        ('UNM', 'Unmoderated'),
        ('PUB', 'Published'),
        ('REJ', 'Rejected'),
        ('ARC', 'Archived'),
    )

    ai      = models.ForeignKey(App_instance)
    channel = models.ForeignKey('Channel')
    state   = models.CharField(max_length=3, choices=STATE_CHOICES)
    callerid= CalleridField(max_length=CALLERID_LENGTH)
    detail  = models.ForeignKey(Recording, related_name='detail', null=True)
    summary = models.ForeignKey(Recording, related_name='summary', null=True)
    comments= models.ManyToManyField('News', related_name='parent')
    rating  = models.IntegerField(default=0)
    is_comment   = models.BooleanField()
    source  = models.PositiveSmallIntegerField(null=True)
    time    = models.DateTimeField(auto_now_add=True)
    title   = models.CharField(max_length=TITLE_LENGTH)
    location= models.ForeignKey(Location, null=True, blank=True)
    tags    = models.CharField(max_length=512, null=True, blank=True)
    transcript = models.TextField()
    pub_order = models.SmallIntegerField(default=PUB_ORDER_MAX)
    modified_date = models.DateTimeField(default=datetime.now)
    category = models.ForeignKey(Category, null=True)
    format = models.ForeignKey(Format, null=True)
    qualifier = models.ForeignKey(Qualifier, null=True)
    location_text = models.CharField(max_length=LOCATION_LENGTH, null=True)
    is_mod_flagged = models.BooleanField(default=False)
    is_advertisement = models.BooleanField(default=False)
    campaign_categories = models.ManyToManyField(Campaign_category)
    sm_image = models.ForeignKey(Image, null=True, blank=True)
    age_group = models.ForeignKey(Age_group, null=True, blank=True)
    occupation = models.ForeignKey(Occupation, null=True, blank=True)
    gender = models.ForeignKey(Gender, null=True, blank=True)
    autopublished = models.BooleanField(default=False)
    original_item = models.ForeignKey('News', related_name='originalitem', null = True)
    creation_cdr = models.ForeignKey(Cdr, null = True)
    checksum = models.CharField(max_length=64, null = True)
    misc = models.TextField()
    dm_propeties = models.TextField()
    cm_properties = models.TextField()
    ml_properties = models.TextField()
    app_properties = models.TextField()
    sync_properties = models.TextField()

    moderator = None
    fields_changed = None
    event_type = None
    reason_id = None

    class Source:
        WEB_INTERFACE   = 0
        VOICE_INTERFACE = 1
        APP_INTERFACE   = 2

    class ContentType:
        UGC = 'User generated content'
        IHC = 'In-house content'

    class Action:
        STATUS_UPDATE = 'STATUS_UPDATE'
        LIKED         = 'LIKED'
        BMARK_ACCD    = 'BMARK_ACCD'
        FORWARD       = 'FWD'
        HEARD         = 'HEARD'

    def should_upload_audio_to_s3(self):
        if self.state == 'REJ' or not self.detail:
           return False
        if not self.sync_properties:
           return True
        try:
           sync_properties = json.loads(self.sync_properties)
           logger.info(str(sync_properties))
        except Exception as e:
           logger.info(str(e))
           return True
        try:
           if sync_properties.get('checksum') != self.checksum:
              return True
        except Exception as e:
           logger.info(str(e))
        return False




    def get_changed_fields(self, news):
        attribs = ['state', 'title', 'location', 'tags', 'transcript', 'category', 'format', 'qualifier', \
                   'is_mod_flagged', 'pub_order', 'rating', 'campaign_categories', 'age_group', 'occupation', 'gender', 'dm_propeties']
        changed_fields = []
        if not news:
            return attribs

        for attrib in attribs:
            previous = getattr(news, attrib)
            if getattr(self, attrib) != previous:
                if attrib == 'campaign_categories':
                    changed_fields.append(str(attrib)+" : "+str(map(str, previous.values_list('name', flat=True))))
                else:
                    changed_fields.append(str(attrib)+" : "+str(previous))

        reject_reason = getattr(self, 'reason_id')
        if reject_reason:
            changed_fields.append("reason_id : "+str(reject_reason))

        return changed_fields
        
    def should_upload_image_to_s3(self):
            if not (self.detail and  self.sm_image):
               return False
            if not  self.sync_properties:
               return True

            try:
               sync_properties = json.loads(self.sync_properties)
            except Exception as e:
               return True
            if not sync_properties.has_key('s3_image_url'):
               return True
            else:
               image_m_time =  datetime.strptime(sync_properties['s3_image_modified_time'],  '%Y-%m-%d_%H:%M:%S.%f')
               current_image_file = self.sm_image.get_location()
               current_image_file_m_time = datetime.fromtimestamp(os.path.getmtime(current_image_file))
               if current_image_file_m_time > image_m_time:
                  return True
               else:
                  return False
              
           


          

    def generate_lskey(self, **kwargs):
        lskey = "ch-"+str(self.channel.id)+"-"+str(int(self.is_comment))+"-"+str(self.state)+"-"+str(self.time.date())+"-ap-"+str(int(self.autopublished))
        return lskey


    def SyncDynamo(self, **kwargs):
        sync_news(self,**kwargs)
        sync_object_data(self.__class__.__name__, self.id, status='SUCCESS')

    def tags_dict(self):
        if self.tags:
            return dict([(tag, urllib.quote_plus(tag)) for tag in self.tags.split(',')])
        else:
            return None

    def get_transcript(self):
        if self.transcript:
            return self.transcript
        else:
            return 'Transcript Unavailable'

    def encoded_location(self):

        if self.location_text:
            return urllib.quote_plus(self.location_text)
        return None

    def num_comments(self):
        return self.comments.filter(state__in = [News_state.UNM, News_state.PUB]).count()

    def num_unm_comments(self):
        return self.comments.filter(state = News_state.UNM).count()

    def published_comments(self):
        return self.comments.filter(state = News_state.PUB)

    def num_pub_arc_comments(self):
        return self.comments.filter(state__in = [News_state.ARC, News_state.PUB]).count()

    def parent_id(self):
        if hasattr(self, 'parent_id_value'):
            return self.parent_id_value
        try:
            return News.objects.filter(comments = self.id)[0].id
        except:
            return None

    def __unicode__(self):
        if self.is_comment:
            comm = 'comm'
        else:
            comm = 'news'
        detail_id = unicode( self.detail.id ) if self.detail else 'no_recording'
        return unicode(self.id) + '_' + detail_id + '_' + self.state + '_' + comm

    def bookmark_news_and_send_sms(self, cdr, source):
        bookmark, already_exists = Bookmark.get_or_create_bookmark(self, cdr.callerid)
        if bookmark:
            blog = Bookmark_log(bookmark = bookmark, cdr = cdr, accessed = False, bookmarked = True, source = source)
            blog.save()
            args = {'bookmark_id': bookmark.bookmark_id, 'destination': cdr.callerid}
            ModerationEventRecorder.send_event_message(self, ModerationEvents.ITEM_BOOKMARKED, **args)

    def process_tags(self):
        logger.info("Into process_tags")
        self.tags = self.tags or ''
        tags_list = [tag.strip() for tag in self.tags.split(',')]

        if self.reason_id:
            if self.reason_id == -1:
                tags_list.append('mlautorej')
            from mnews.views import get_reject_reasons
            rejection_reasons = get_reject_reasons(self)
            if rejection_reasons:
                for reason in rejection_reasons.all():
                    if str(reason.rej_tag) in tags_list:
                        tags_list.remove(str(reason.rej_tag))
                rej_reason = rejection_reasons.filter(is_ml_tag = True)
                if self.reason_id == -1 and rej_reason.exists():
                    tags_list.append(str(rej_reason[0].rej_tag))
                elif self.reason_id != -1:
                    tags_list.append(str(rejection_reasons.get(id= self.reason_id).rej_tag))

        filtered_tags= []
        for tag in tags_list:
            if bool(re.compile(r'[A-Za-z]').search(tag)):
                filtered_tags.append(tag)
        self.tags = ','.join(filtered_tags)
        

    def set_default_values(self):
        default_field_values = Ai_field_properties.objects.filter( ai = self.ai )
        default_field_map = {}

        for default_field in default_field_values:
            default_field_map[ default_field.field_name ] = default_field

        all_fields = News._meta.get_all_field_names()
        for field in all_fields:
            field_object, model, direct, m2m = News._meta.get_field_by_name( field )
            # Currently not handling m2m keys
            if ( m2m ) :
                continue
            if ( hasattr( self, field ) ) and ( not ( getattr( self, field ) ) and default_field_map.has_key( field ) ) :
                default_value = default_field_map[ field ].get_default_field_value()
                setattr( self, field, default_value )

    def record_copy(self, copy):
        root_id = News_copy.find_copy_root(News.objects.get(id=self.id))
        News.objects.filter(pk=copy.id).update(original_item=root_id)        
        News_copy(news = self, copy = copy).save()

    def get_all_copies(self):
        return News_copy.get_all_copies(self)

    @classmethod
    def get_pub_arc_news(cls, **kwargs):
        return News.objects.filter(state__in = (News_state.PUB, News_state.ARC), **kwargs)

    def get_news_duration(self):
        from vapp.utils import get_ratio, mp3_duration
        if self.detail:
            duration = mp3_duration(self.detail.get_full_filename())
        elif self.summary:
            duration = mp3_duration(self.summary.get_full_filename())
        else:
            duration = 0.0

        return get_ratio(duration, 1000)

    def sm_image_url(self):
        if self.sm_image:
            return self.sm_image.get_image_url()

    def get_news_location(self):
        if not self.location:
            return None
        return self.location

    def campaign_category_assigned(self, subcategory_id):
        return self.campaign_categories.filter(id = subcategory_id).exists()
    
    def get_published_datetime(self):
        me = ModerationEventRecorder.objects.filter(item = self, event_type = 'ITEM_PUBLISHED')
        if me:
           return me[0].timestamp
        else:
           return None  


class News_copy(models.Model):
    news = models.ForeignKey(News, related_name = 'news_set')
    copy = models.ForeignKey(News, related_name = 'copy_set')

    def __unicode__(self):
        return unicode(self.news) + '<->' + unicode(self.copy)


    @classmethod
    def get_all_copies(cls, news):
        root = cls.find_copy_root(news)
        return cls.traverse(root)

    @classmethod
    def find_copy_root(cls, news):
        parents = News.objects.filter(news_set__copy = news)
        if parents.exists():
            return cls.find_copy_root(parents[0])
        else:
            return news

    @classmethod
    def traverse(cls, news):
        traversed = set()
        cls.dfs(news, traversed)
        return traversed

    @classmethod
    def dfs(cls, news, copy_set):
        copy_set.add(news)
        copies = News.objects.filter(copy_set__news = news)
        for copy in copies:
            cls.dfs(copy, copy_set)
        return copy_set

class Event_callout_blocking_policy(models.Model):

    ai = models.ForeignKey(App_instance)
    event_type = models.CharField(max_length = 20)
    enabled = models.BooleanField(default = True)
    contact_list_specific = models.BooleanField(default = False)
    contact_list = models.ForeignKey(Contact_list, null = True, blank = True)

    def __unicode__(self):
        return str(self.ai) + '_' + str(self.event_type) + '_' + str(self.enabled)

    @classmethod
    def block_callout(cls, ai, event, callerid):

        block = False
        try:
            blocking_policy = Event_callout_blocking_policy.objects.get(ai = ai, event_type = event, enabled = True)
            block = True
            if blocking_policy.contact_list_specific and blocking_policy.contact_list:
                block = ( callerid in blocking_policy.contact_list.get_numbers() )
        except Exception,e:
            pass

        return block


def is_published_items_reorder_needed(news):

    '''
    return True if news is not comment, has been published from unpublished or from published to
    unpublished or remains published but the publishing order has changed
    '''
    return (( not news.is_comment ) and (( news.prev_state == 'PUB' and news.state != 'PUB' )
                         or (news.prev_state != 'PUB' and news.state == 'PUB')
                         or (news.prev_state == 'PUB' and news.state == 'PUB' and news.prev_order != news.pub_order)
                         or (news.prev_channel_id != news.channel_id and news.state == 'PUB') ))

def get_items_to_be_reordered(ai, channel, min_pub_order, max_pub_order, excluded_ids = []):
    return News.objects.filter(ai = ai, channel = channel, state = 'PUB', is_comment = False,
                               pub_order__gte = min_pub_order, pub_order__lte = max_pub_order).exclude(id__in = excluded_ids).order_by('pub_order')


def reorder_published_items(news, prv_channel_id, prv_state, prv_order):
    from mnews.tasks import PubOrderSyncTask
    logger.info('start of reorder ')
    channel = news.channel
    logger.info(str(news.channel))
    logger.info(str(prv_channel_id))
    news_id = int(news.id)
    prev_channel = Channel.objects.get(pk=prv_channel_id)
    if channel.id != prv_channel_id:
        prev_ch_pub_list = eval(prev_channel.pub_list)
        try:
            prev_ch_pub_list.remove(news_id)          
            prev_channel.pub_list = str(prev_ch_pub_list)
            prev_channel.save()
        except Exception as e: 
            logger.info("Exception in prev_channel_pub_list : " + str(e))   
        #running the task to maintain consistency
        PubOrderSyncTask.delay(prv_channel_id)

    new_pub_list = eval(news.channel.pub_list)
    try: 
        new_pub_list.remove(news.id)
    except Exception as e: 
        logger.info("Exception in current_channel_pub_list : " + str(e))
                
    if news.state =='PUB':
        new_pub_list.insert(news.pub_order, news_id)
    news.channel.pub_list = str(new_pub_list)
    news.channel.save()
    PubOrderSyncTask.delay(news.channel.id)
          

def news_pre_save_handler(sender, **kwargs):
    news = kwargs['instance']
    logger.info("News in pre save handler"+str(news))
    if not news:
        return
    news.modified_date = datetime.now()
    news.process_tags()
    news_detail = news.detail
    if news_detail:
        news_detail_path = news_detail.get_full_filename()
        news_file = Path(news_detail_path)
        logger.info("checksum :: " + str(news.checksum))
        try:
            if news_file.is_file():
                news.checksum = audiodiff.checksum(news_detail_path)           
	except Exception as e:
	    logger.info("Exception in checksum"+str(e))
    if not news.channel_id:
        news.channel = Channel.get_default_channel(news.ai.id)
    news.set_default_values()
    prev_news = None
    news.prev_state  = news.prev_channel_id = news.prev_checksum = None
    news.prev_order = 9999
    if news.pk:
        prev_news = News.objects.get(pk = news.pk)
        news.prev_state = prev_news.state
        news.prev_order = prev_news.pub_order
        news.prev_channel_id = prev_news.channel.id
        news.prev_checksum = prev_news.checksum

    news.fields_changed = news.get_changed_fields(prev_news)
    news.event_type = ModerationEventRecorder.get_moderation_event(news, prev_news, news.fields_changed)

    '''
    Set pub_order=0 when moderating from any other state to PUB state and
    whenever moving from one channel to other
    '''
    if (news.state == 'PUB' and news.prev_state != 'PUB') or (news.prev_channel_id and (news.prev_channel_id != news.channel.id)):
        news.pub_order = 0

    if news.state != 'PUB':
        news.pub_order = PUB_ORDER_MAX


    logger.info("END of pre_save_handler"+str(news.__dict__))

def call_contributor(ai, event, action_settings, **event_data):
    logger.info("Into call_contributor")
    news = event_data.get('news')
    if news.reason_id:
        try:
            from mnews.views import get_reject_reasons
            rejection_reasons = get_reject_reasons(news)
            ml_reject = news.reason_id
            if rejection_reasons:
                rej_reason = rejection_reasons.filter(is_ml_tag = True)
                if ml_reject == -1 and rej_reason.exists():
                    reject_obj = rej_reason[0]
                elif ml_reject != -1:
                    reject_obj = rejection_reasons.get(id= news.reason_id)
                if not reject_obj.call_on_rejection:
                    return
            else:
                return
        except:
            logger.exception( "No Reason object with this " + str(news.reason_id) + "id")

    guidance_call_attempted = None
    if news.misc and news.state != 'PUB':
       misc_dict = json.loads(news.misc)
       dist_mod_dict = misc_dict.get('dist_mod')
       if dist_mod_dict:
           guidance_call_attempted = dist_mod_dict.get('guidance_call_attempted')

    if ( Event_callout_blocking_policy.block_callout(news.ai, event, news.callerid)
        or (hasattr(news,'autopublished') and news.autopublished and event!='ITEM_REJECTED') or guidance_call_attempted ):
        return
   
    vi = VI_conf.objects.get(controller = 'mnews.publishvi.OnPubCallController', description = 'mnews.publishvi.OnPubCallStateMap')
    vi_data = { 'news_id': news.id,
                'vi_conf_id': vi.id,
                'reason_id' : news.reason_id,
                'event_properties_id': action_settings.id}
    call_log = action_settings.schedule.callout_to_number(news.callerid, max_tries = action_settings.max_tries, vi_data = vi_data)
    event_notification_log = Event_notification_call_log(news_item = news, event_type = event, group_call_log = call_log)
    event_notification_log.save()

def call_on_publish_list(ai, event, action_settings, **event_data):
    schedule = action_settings.schedule
    max_tries = action_settings.max_tries
    news = event_data.get('news')
    vi = VI_conf.objects.get(controller = 'mnews.publishvi.OnPubCallController', description = 'mnews.publishvi.OnPubCallStateMap')
    vi_data = { 'news_id': news.id,
            'vi_conf_id': vi.id,
            'event_properties_id': action_settings.id}

    # action_settings.on_pub_contrbutior_dialout_list
    # {fo_number: cl_id, fo2: cl_id, ....., ..... ,.... }
    # if fo is present in dict then fetch cl mapped against it
    # call the same fun() .... schedule.callout_to_contact_list(cl, max_tries = max_tries, vi_data = vi_data)
    if action_settings.on_pub_contrbutior_dialout_list:
        on_pub_contrbutior_dialout_list = json.loads(on_pub_contrbutior_dialout_list)
        if news.callerid in on_pub_contrbutior_dialout_list :
            cl_id_for_dailout = d[news.callerid]
            schedule.callout_to_contact_list(cl_id_for_dailout max_tries = max_tries, vi_data = vi_data)

    for cl in action_settings.on_publish_dialout_lists.all():
        schedule.callout_to_contact_list(cl, max_tries = max_tries, vi_data = vi_data)

def call_contrib_comment_pub(ai, event, action_settings, **event_data):
    news = event_data.get('news')
    parent = News.objects.get(pk = news.parent_id())
    if ( Event_callout_blocking_policy.block_callout(news.ai, event, parent.callerid)
         or (hasattr(news,'autopublished') and news.autopublished) ):
        return

    vi = VI_conf.objects.get(controller = 'mnews.publishvi.OnCommentPubCallContribController', description = 'mnews.publishvi.OnCommentPubCallContribStateMap')
    vi_data = { 'parent_id': parent.id,
            'news_id': news.id,
            'vi_conf_id': vi.id,
            'event_properties_id': action_settings.id}

    call_log = action_settings.schedule.callout_to_number(parent.callerid, max_tries = action_settings.max_tries, vi_data = vi_data)
    event_notification_log = Event_notification_call_log(news_item = news, event_type = event, group_call_log = call_log)
    event_notification_log.save()


def process_event(ai, event, **event_data):
    logger.info("Into process_event")
    action_settings = Event_ai_properties.objects.filter(ai = ai, event_type = event).order_by('order')
    logger.info("ai = %s and event = %s" % (ai, event))
    logger.info(str(action_settings))
    for action_setting in action_settings:
        if action_setting.action == Event_ai_properties.Action.CALL_CONTRIBUTOR:
            call_contributor(ai, event, action_setting, **event_data)
        elif action_setting.action == Event_ai_properties.Action.CALL_ON_PUBLISH_LIST:
            call_on_publish_list(ai, event, action_setting, **event_data)
        elif action_setting.action == Event_ai_properties.Action.CALL_CONTRIB_COMMENT_PUB:
            call_contrib_comment_pub(ai, event, action_setting, **event_data)

def news_save_handler(sender, **kwargs):
    news = kwargs['instance']
    model_name = news.__class__.__name__    

    logger.info("Into Post save for "+str(news.__dict__))

    from vapp.customization.models import Custom_handler_type, custom_news_save
    (custom_news, customization_applied) = custom_news_save(news, Custom_handler_type.NEWS_POST_SAVE, **kwargs)
    if customization_applied:
        news = custom_news
        return

    try:
        from vapp.case_manager.util import is_case_manager_mnews_instance, create_grievance
        if is_case_manager_mnews_instance(news.ai) and not news.is_comment:
            create_grievance(news)
    except Exception as e:
        logger.info(str(e))

    try:
        logger.info(str(news.moderator)+"news moderator")
    	if news.moderator:
        	ModerationEventRecorder.record_event(news.moderator, news, news.event_type, news.fields_changed)
    except Exception as e:
        logger.info(str(e))

    logger.info('Pushing item to caching db' +str(news.id))

    try:
        prev_channel_id = None

        if news.prev_channel_id and (news.prev_channel_id != news.channel.id):
            prev_channel_id = news.prev_channel_id

        prev_checksum = news.prev_checksum

        payload_type = "news_update"
        payload = {"prev_state": news.prev_state, "cur_state": news.state, "prev_channel": prev_channel_id, "prev_checksum":prev_checksum}
        from caching_service.tasks import SyncDynamoTask
        SyncDynamoTask.delay(model_object = news, payload_type = payload_type, payload = json.dumps(payload))
    except Exception as e:
        sync_object_data(model_name, news.pk, 'exception', str(e), status = 'FAILED')
        logger.info("Exception in news_save_handler for News : "+str(e))

    try:
    	process_event(news.ai, news.event_type, **{'news':news})
    	news_args = {'news_id':str(news.pk)}
    	ModerationEventRecorder.send_event_message(news, news.event_type, **news_args)
    except Exception as e:
	   logger.info(str(e))

    if kwargs['created']:
        event = Event.ADD
    else:
        event = Event.UPDATE

    logger.info("before reorder in news_save_handler"+str(news.prev_state) + str(news.prev_order)+str(news.id))
    if is_published_items_reorder_needed(news):
        reorder_published_items(news, news.prev_channel_id, news.prev_state, news.prev_order)
    
    queue = App_instance.message_queue_from_id( news.ai_id )
    RQMC.push_instance(queue, news, event, extras=('num_comments', 'num_unm_comments', 'parent_id'), relations=('detail', 'summary',))
    if news.is_comment:
        RQMC.push_instance(queue, News.objects.get(pk = news.parent_id()), Event.UPDATE, extras=('num_unm_comments', 'parent_id'), relations=('detail', 'summary',))
    logger.info("End of Post save for "+str(news.id))

def get_news_state(recording):
    threshold = 0.75
    url = "http://voice2.gramvaani.org:8004/score"
    logger.info("RECORDING PATH"+str(recording))
    with open(recording,"rb") as f:
        # Read the file
        read = f.read()

        # Encode as a base64 string
        base64_encoded_bytes = base64.b64encode(read)
        base64_encoded_str = base64_encoded_bytes.decode()

        # Form and send the request
        request = {"data": base64_encoded_str, "threshold": threshold}
        upload = json.dumps(request)

        # Predict using model
        headers = {'Content-type': 'application/json'}
        try:
            result = requests.post(url, data = upload, headers = headers)
        except Exception as e:
            logger.exception("Connection error to server : "+str(e))
            return None

        # Print results to screen
        state = None
        if result.ok:
            J = json.loads(result.json())
            print(J['Reject'])
            state = J['Reject']
        logger.info("state : "+str(state))
        logger.info(str(result.text))
        return state

def news_delete_handler(sender, **kwargs):
    news = kwargs['instance']
    if news.detail:
        news.detail.delete()
    if news.summary:
        news.summary.delete()
    for comment in news.comments.all():
        comment.delete()

    queue = App_instance.message_queue_from_id( news.ai_id )
    RQMC.push_instance(queue, news, Event.DELETE, extras=('num_comments', 'num_unm_comments', 'parent_id'))
    if news.is_comment:
        RQMC.push_instance(queue, News.objects.get(pk = news.parent_id()), Event.UPDATE, extras=('num_unm_comments', 'parent_id'), relations=('detail', 'summary',))

def news_pre_delete_handler(sender, **kwargs):
    news = kwargs['instance']
    news.parent_id_value = news.parent_id()

def mnews_news_m2m_changed_handler(sender, **kwargs):
    news = kwargs['instance']
    logger.info("kwargs in item_comment: "+str(kwargs))
    try:
        if kwargs.get('action') not in ['post_add', 'post_remove']:
            return
        payload_type = "news_update"
        payload = {"prev_state": news.state, "cur_state": news.state, "prev_channel": news.channel_id, "prev_checksum":news.checksum}
        from caching_service.tasks import SyncDynamoTask
        SyncDynamoTask.delay(model_object = news, payload_type = payload_type, payload = json.dumps(payload))
        logger.info("for m2m comment sync : " + str(news.id))

    except Exception as e:
        logger.info("Exception in sending comments on m2m_changed : "+str(e)+str(news.id))

post_save.connect(news_save_handler, sender = News, dispatch_uid = 'mnews.news.save')
pre_save.connect(news_pre_save_handler, sender = News, dispatch_uid = 'mnews.news.pre_save')
post_delete.connect(news_delete_handler, sender = News, dispatch_uid = 'mnews.news.delete')
pre_delete.connect(news_pre_delete_handler, sender = News, dispatch_uid = 'mnews.news.pre_delete')
m2m_changed.connect(mnews_news_m2m_changed_handler, sender = News.comments.through, dispatch_uid = 'mnews.news.m2m_changed')


class Mod_comment(models.Model):
    news = models.ForeignKey( News )
    user = models.ForeignKey( User )
    time = models.DateTimeField( default = datetime.now )
    text = models.TextField()

    def __unicode__(self):
        return unicode( self.news.id ) + '_' + unicode( self.user.id ) + '_' + unicode( self.text )

class Channel(models.Model):
    ai = models.ForeignKey(App_instance)
    name = models.CharField(max_length=64)
    default = models.BooleanField()
    sticky = models.BooleanField(default=False)
    pub_list = models.CharField(max_length='60000', null=False, default='[]')
    max_news = models.SmallIntegerField(default=10)


    #ToDo: Use custom django admin form to ensure only one channel
    #is chosen as default and channels are assigned only to mnews
    #instances.

    @classmethod
    def create_default_channel(cls, ai_id):
        try:
            channel, created = Channel.objects.get_or_create( ai_id = ai_id, name = DEFAULT_CHANNEL, default = True )
            if created:
                add_prompts_for_channel( ai_id, channel )
        except:
            logger.exception( "Could not create channel for app instance: " + str(ai_id)  )

    @classmethod
    def get_sticky_channel(cls, ai_id):
        try:
            channel = Channel.objects.filter(ai_id=ai_id, sticky=True)
            if not channel.exists():
                return None
            logger.info("dibyendu: mnews/models sticky channel is there")
            channel = channel[0]
            if News.objects.filter(ai_id=ai_id, channel_id=channel.id,
                                   state=News_state.PUB).exists():
                logger.info("dibyendu: item exists in sticky channel")
                return channel
            else:
                return None
        except:
            logger.exception("Could not find sticky channel for app instance: " + str(ai_id))

    @classmethod
    def get_default_channel(cls, ai_id):
        try:
            return Channel.objects.get(ai__id = ai_id, default = True)
        except:
            logger.exception("Could not find default channel for app instance: " + str(ai_id) + ".")
            return None

    def SyncDynamo(self):
	try:
            sync_reordered_channel(self)
        except Exception as e:
            logger.info("Exception in SyncDynamo for Channel : "+str(e))

    def __unicode__(self):
        return unicode( self.ai ) + '_' + unicode( self.name )

def channel_save_handler(sender, **kwargs):
    channel = kwargs['instance']
    object_id = channel.id
    model_name = channel.__class__.__name__
    from caching_service.tasks import SyncDynamoTask
    SyncDynamoTask.delay(model_object = channel, payload_type = None, payload = None)

post_save.connect(channel_save_handler, sender=Channel, dispatch_uid = 'mnews.channel.save')


class Channel_order(models.Model):
    #Can add more fields like dialed num to decide on next channel later
    #Not adding unique constraint to cur_channel for above reason
    initial_ai = models.ForeignKey( App_instance, null = True, blank = True )
    cur_channel = models.ForeignKey(Channel, related_name = 'cur_channel_set')
    next_channel = models.ForeignKey('Channel', null = True, blank = True, related_name = 'next_channel_set')

    @classmethod
    def get_next_channel(cls, channel, init_ai = None ):
        try:
            order = Channel_order.objects.get( cur_channel = channel, initial_ai = init_ai )
            return order.next_channel
        except Exception,e:
            return None

    @classmethod
    def get_prev_channel(cls, channel, init_ai = None ):
        try:
            order = Channel_order.objects.get(next_channel = channel, initial_ai = init_ai )
            return order.cur_channel
        except Exception,e:
            return None

    def __unicode__(self):
        return unicode(self.cur_channel) +  ' -> ' + unicode(self.next_channel)

class Vi_transition_data(models.Model):
    transition_event = models.ForeignKey(Transition_event)
    channel = models.ForeignKey(Channel)
    selection_algo = models.CharField(max_length = 128, default = '')

class Event_sms_template(models.Model):

    event = models.CharField(max_length = 64)
    template = models.ForeignKey(SMS_template)

    def __unicode__(self):
            return unicode(self.event) + "->" + unicode(self.template)

class Bookmark(models.Model):

    ai = models.ForeignKey(App_instance)
    bookmark_id = models.PositiveIntegerField()
    news = models.ForeignKey(News)
    callerid = CalleridField()
    time = models.DateTimeField(auto_now_add = True)

    @classmethod
    def get_or_create_bookmark(cls, news, callerid):
        mapping_exists = Bookmark.objects.filter(news = news, callerid = callerid)
        if mapping_exists.exists():
            return (mapping_exists[0], True)

        candidate_id = None
        tries = MAX_BOOKMARK_ID_GENERATION_TRIES

        while(tries > 0):
                # one way to generate a 5 digit number (which may increase to 6 digits or more)
            candidate_id = (hash(str(uuid.uuid1()))%MAX_BOOKMARK_NUMBER)

            if (not (Bookmark.objects.filter(ai = news.ai, bookmark_id = candidate_id).exists())) and candidate_id > 0:
                candidate = Bookmark(ai = news.ai, news = news, callerid = callerid, bookmark_id = candidate_id)
                candidate.save()
                return (candidate, False)
            tries -= 1
        logger.info('unable to generate a unique bookmark id for the item:' + str(news))
        Bookmark_generation_error(news = news, callerid = callerid, last_id_generated = candidate_id)

        return (None, False)

class Bookmark_generation_error(models.Model):

    news = models.ForeignKey(News)
    callerid = CalleridField()
    last_id_generated = models.PositiveIntegerField()
    time = models.DateTimeField(auto_now_add = True)


class Bookmark_log(models.Model):

    BOOKMARK_SOURCES = (
                    ("ITP", "Item Published"),
                    ("ITB", "Item Bookmarked"),
                    ("BIA", "Bookmarked Item Accessed"),
                    )

    bookmark = models.ForeignKey(Bookmark)
    cdr = models.ForeignKey(Cdr, null = True)
    accessed = models.BooleanField()
    bookmarked = models.BooleanField()
    time = models.DateTimeField(auto_now_add = True)
    source = models.CharField(max_length = 3, null = True, choices = BOOKMARK_SOURCES)

class News_sms_log(models.Model):

    news = models.ForeignKey(News)
    sms = models.ForeignKey(SMS_message)

    @classmethod
    def is_message_sent(cls, news, message):
        sms = News_sms_log.objects.filter(news = news, sms__message = message, sms__sent_success = True)
        return sms.exists()

class Ai_feed_generator_mapping(models.Model):
    ai = models.ForeignKey( App_instance )
    feed_generator = models.CharField( max_length = 50 )
    limit = models.PositiveIntegerField( default = 30 )
    feed_ais = models.ManyToManyField( App_instance, null = True, blank = True, related_name = 'feed_ais' )

    def __unicode__(self):
        return self.feed_generator + " for " + str(self.ai)

class JMVCustomFeed(Rss201rev2Feed):

    def add_item_elements(self, handler, item):

        handler.addQuickElement(u"audio_url", item['audio_url'])
        handler.addQuickElement(u"time_of_call", item['time_of_call'])
        handler.addQuickElement(u"category", item['category'])
        handler.addQuickElement(u"tags", item['tags'])
        super(JMVCustomFeed, self).add_item_elements(handler, item)

class JMRItemsFeed(Feed):

    def get_object(self, request, ai_id):
        return get_object_or_404(App_instance, pk=ai_id)

    feed_type = JMVCustomFeed

    def title(self, obj):
        return "Mobile Vaani - Community based social media network over mobile"

    def link(self, obj):
        return settings.JMR_SITE

    def description(self, obj):
        return "Mobile Vaani is our answer to building a social media platform for rural India. It is a space for communities to share information and represent themselves, in their own voice."

    def item_link(self,obj):
        return settings.JMR_SITE + reverse('mnews.views.show_detail', kwargs={'ai_id': str(obj.ai.id),'news_id': str(obj.id)})

    def item_title(self,obj):
        if not obj.title:
            return "no title"
        return obj.title

    def item_description(self,obj):

        if not obj.transcript:
            return "no description"
        return obj.transcript

    def items(self, obj):
        return self.get_feed_items_for_ai(obj)

    def get_feed_items_for_ai( self, ai ):
        if ai and isinstance( ai, App_instance ):
            feed_generator_map = Ai_feed_generator_mapping.objects.get( ai = ai )
            feed_generator = feed_generator_map.feed_generator

            if hasattr( feedgenerators, feed_generator ):
                feed_generator_function = getattr( feedgenerators, feed_generator )
                return feed_generator_function( ai, feed_generator_map.feed_ais.all, limit = feed_generator_map.limit )

    def item_extra_kwargs(self, obj):

        return { 'audio_url': unicode(obj.detail.get_url()) if obj.detail else u"no recording",
                 'time_of_call' : unicode(obj.time) if obj.time else u"no timestamp",
                 'category' : unicode(obj.category) if obj.category else u"no category",
                 'tags' : unicode(obj.tags) if obj.tags else u"no tags",}


class Transient_group(models.Model):

    group_name = models.CharField(max_length = 50)
    description = models.TextField(null = True)
    generating_code = models.CharField(max_length = 100, null = True)

    def __unicode__(self):
        return self.group_name

class Ai_transient_group(models.Model):

    ai = models.ForeignKey(App_instance)
    group = models.ForeignKey(Transient_group)
    vi_reference = models.ForeignKey(VI_reference, null = True, blank = True)

    def __unicode__(self):
        return str(self.ai) + '_' + str(self.group)

    def clean(self):
        if self.vi_reference:
            if self.vi_reference.ai.id != self.vi_reference.target_ai.id:
                raise ValidationError('Source and targe ai for group call out VI reference should be the same.')
            if self.vi_reference.vi.controller != 'app_manager.vi.CallAndPlayController' or self.vi_reference.vi.description != 'app_manager.vi.CallAndPlayStateDescriptionMap':
                raise ValidationError('Source vi group call out must be CallAndPlay Vi')

        super(Ai_transient_group, self).clean()


class Days(models.Model):

    DAYS_OF_WEEK = (
    ('0', 'Monday'),
    ('1', 'Tuesday'),
    ('2', 'Wednesday'),
    ('3','Thursday'),
    ('4', 'Friday'),
    ('5', 'Saturday'),
    ('6', 'Sunday'),
    )
    name = models.CharField(max_length = 1, choices = DAYS_OF_WEEK)

    def __unicode__(self):
        return self.DAYS_OF_WEEK[int(self.name)][1]

class Transient_group_schedule(models.Model):

    name = models.CharField(max_length = 50)
    ai_group = models.ForeignKey(Ai_transient_group)
    prompt_file = models.FileField(null = True, blank = True, upload_to='groups')
    play_ai = models.BooleanField(default = True)
    start_time = models.TimeField()
    end_time = models.TimeField()
    day_of_week = models.ManyToManyField(Days)
    repeat = models.BooleanField()
    active = models.BooleanField(default = False)
    dnd_check = models.BooleanField(default = False)
    onpush_sms_message = models.ForeignKey(SMS_template, null = True, blank = True)

    def __unicode__(self):
        return self.name

    def get_prompt_file(self):
        if self.prompt_file:
            return self.prompt_file.path
        return None

    def is_call_valid(self):
        if self.active:
            today = str(datetime.now().today().weekday())
            all_days = list(self.day_of_week.get_query_set().values_list('name', flat = True))
            if today in all_days:
                time = datetime.now().time()
                if time >= self.start_time and time < self.end_time:
                    return True
        return False

    def get_group_ai(self):
        if self.ai_group:
            return self.ai_group.ai
        else:
            return None

    def callout_to_contact_list(self, contact_list, max_tries = None, vi_data = {}, eta = None):
        for number in contact_list.get_numbers():
            self.callout_to_number(number, max_tries, vi_data, eta = None)

    def callout_to_number(self, number = None, max_tries = None, vi_data = {}, eta = None, numbers = []):
        from mnews.tasks import CallAndPlayTask
        ai = self.ai_group.ai
        queue = get_queue_for_task(ai, default_queue='push')
        if numbers:
            call_log_list = []
            gcl_id = Groups_call_log.objects.order_by('-id')[0].id
            for number in numbers:
                call_log_list.append( Groups_call_log(number = number, group_schedule = self, ai = ai) )
            Groups_call_log.objects.bulk_create( call_log_list )
            new_gcl_ids = Groups_call_log.objects.filter(id__gt = gcl_id).values_list('id', flat = True)
            for new_gcl_id in new_gcl_ids:
                args = [new_gcl_id, max_tries] if max_tries else [new_gcl_id]
                CallAndPlayTask.apply_async(args=args, kwargs={'vi_data': vi_data},
                                            eta=eta or datetime.now(), queue=queue)
            return call_log_list
        else:
            if number not in NO_DIAL_OUT_LIST:
            	call_log = Groups_call_log(number = number, group_schedule = self, ai = ai)
            	call_log.save()
            	args = [call_log.id, max_tries] if max_tries else [call_log.id]
            	CallAndPlayTask.apply_async(args=args, kwargs={'vi_data': vi_data},
                                        eta=eta or datetime.now(), queue=queue)
            	return call_log
            else:
                logger.info('SGC found skipping dialout')

class Groups_call_log(models.Model):

    number = CalleridField(max_length = 20)
    group_schedule = models.ForeignKey(Transient_group_schedule)
    ai = models.ForeignKey(App_instance, null = True)
    last_cdr = models.ForeignKey(Cdr, null = True)
    success = models.BooleanField(default = False)
    tries = models.PositiveSmallIntegerField(default = 0)
    vi_conf  = models.ForeignKey(VI_conf, null = True, blank = True)
    no_dial_cause = models.CharField(max_length = 50, null = True)

    def __unicode__(self):
        return str(self.group_schedule) + '_' + self.number

    def should_dial(self, max_tries):
        if self.success:
            logger.info("Call to %s has already succeeded." % (self.number,))
            return False

        if not self.group_schedule.is_call_valid():
            logger.info("Schedule %s not valid." % (self.group_schedule,))
            return False

        if self.tries >= max_tries:
            logger.info("Max tries for call log %s reached." % (self,))
            return False

        if self.number  in Contact_list.objects.get(pk = 519).get_numbers():
            logger.info("%s is in DND List. Cannot Dial" %(self.number,))
            self.no_dial_cause = 'DND_NUMBER'
            self.save()
            return False
        
        if self.group_schedule.dnd_check:
            if get_dnd_status(self):
               logger.info("%s is in DND List. Cannot Dial" %(self.number,))
               self.no_dial_cause = 'DND_NUMBER'
               self.save()
               return False
        
        if not (self.group_schedule.play_ai or self.group_schedule.prompt_file):
            logger.info("Neither play_ai nor prompt_file provided in schedule %s." % (self.group_schedule,))
            return False

        return True


class CallEvents:

    ITEM_FORWARDED = 'ITEM_FORWARDED'


class ModerationEvents:

    NEW_ITEM            = 'NEW_ITEM'
    NEW_COMMENT         = 'NEW_COMMENT'
    ITEM_PUBLISHED      = 'ITEM_PUBLISHED'
    COMMENT_PUBLISHED   = 'COMMENT_PUBLISHED'
    ITEM_ARCHIVED       = 'ITEM_ARCHIVED'
    COMMENT_ARCHIVED    = 'COMMENT_ARCHIVED'
    ITEM_UNPUBLISHED    = 'ITEM_UNPUBLISHED'
    COMMENT_UNPUBLISHED = 'COMMENT_UNPUBLISHED'
    ITEM_REJECTED       = 'ITEM_REJECTED'
    COMMENT_REJECTED    = 'COMMENT_REJECTED'
    ITEM_SAVED          = 'ITEM_SAVED'
    COMMENT_SAVED       = 'COMMENT_SAVED'
    ITEM_BOOKMARKED     = 'ITEM_BOOKMARKED'
    PLAY_MNEWS_WELCOME_START = 'PLAY_MNEWS_WELCOME_START'
    ITEM_META_UPDATED   = 'ITEM_META_UPDATED'

EVENT_LENGTH = 50
FIELDS_CHANGED_LENGTH = 150

class ModerationEventRecorder(models.Model):
    user = models.ForeignKey(User, null = False)
    item = models.ForeignKey(News, null = True)
    event_type = models.CharField(max_length = EVENT_LENGTH, null = False)
    timestamp = models.DateTimeField(auto_now_add = True)
    fields_changed = models.CharField(max_length = FIELDS_CHANGED_LENGTH, null = True)

    @classmethod
    def get_message_for_event(cls, ai, event, **message_args):
        try:
            template =  Event_sms_template.objects.get(template__ai = ai, event = event)
            message = template.template.message
            if message_args:
                message = template.template.process_template(message_args)
            return message
        except ObjectDoesNotExist:
            logger.info("get_message_for_event: No message found for ai:" + str(ai)
                                 + " event:" + str(event))
        except Exception,e:
            logger.exception("get_message_for_event: got exception getting message for ai:" + str(ai)
                                 + " event:" + str(event) + ". Exception:" + str(e))
        return None

    @classmethod
    def send_event_message(cls, news, event_type, max_tries = 3, **message_args):
        message_args = cls.process_args_for_event_type(news, event_type, **message_args)
        message = cls.get_message_for_event(news.ai, event_type, **message_args)
        destination = message_args.get('destination', None)

        if message and not News_sms_log.is_message_sent(news, message):
            if not destination:
                logger.error("Destination is None in send_event_message for news {0} and event {1}".format(news, event_type))
                return

            logger.info('Sending SMS:' + message + " to:" + destination)
            sms = SMSTask.create_send_msg(news.ai.id, message, destination, max_tries)
            sms_log =  News_sms_log(news = news, sms = sms)
            sms_log.save()


    @classmethod
    def record_event(cls, user, item, event_type, fields_changed):
        event = ModerationEventRecorder(user = user, item = item, event_type = event_type, fields_changed = ", ".join(fields_changed))
        event.save()

    # A generic way to process the message args currently putting a specific condition check
    # on item publish, can modify to do more processing
    @classmethod
    def process_args_for_event_type(cls, news, event_type, **message_args):
        if event_type == ModerationEvents.ITEM_PUBLISHED:
            bookmark, already_exists = Bookmark.get_or_create_bookmark(news, news.callerid)
            if bookmark:
                blog = Bookmark_log(bookmark = bookmark, accessed = False, bookmarked = True, source = 'ITP')
                blog.save()
                message_args['bookmark_id'] = bookmark.bookmark_id
                message_args['destination'] = news.callerid

        elif event_type == ModerationEvents.COMMENT_PUBLISHED:
            bookmark, already_exists = Bookmark.get_or_create_bookmark(news, news.callerid)
            if bookmark:
                blog = Bookmark_log(bookmark = bookmark, accessed = False, bookmarked = True, source = 'ITP')
                blog.save()
                message_args['bookmark_id'] = bookmark.bookmark_id

                parent = News.objects.get(pk = news.parent_id())
                message_args['destination'] = parent.callerid

                parent_bookmark, exists = Bookmark.get_or_create_bookmark(parent, parent.callerid)
                message_args['parent_bookmark_id'] = parent_bookmark.bookmark_id

        return message_args

    @classmethod
    def get_moderation_event(cls, news, prev_news, changed_fields):
        prev_tags_list = []
        curr_tags_list = []
        if prev_news:
            prev_tags_list = [tag for tag in prev_news.tags.split(',')]
        if news:
            curr_tags_list = [tag for tag in news.tags.split(',')]
        if(Event_ai_properties.objects.filter(ai = news.ai, event_type = "ITEM_META_UPDATED").exists()):
            for tag in tags_for_call:
                if (tag not in prev_tags_list) and (tag in curr_tags_list):
                    return ModerationEvents.ITEM_META_UPDATED
        if not prev_news:
            if news.is_comment:
                return ModerationEvents.NEW_COMMENT
            else:
                return ModerationEvents.NEW_ITEM
#       elif 'state' in changed_fields: change_fields is to be used for only dumping in database.
        elif news.state != prev_news.state:
            if news.state == 'PUB':
                if news.is_comment:
                    return ModerationEvents.COMMENT_PUBLISHED
                else:
                    return ModerationEvents.ITEM_PUBLISHED
            elif news.state == 'ARC':
                if news.is_comment:
                    return ModerationEvents.COMMENT_ARCHIVED
                else:
                    return ModerationEvents.ITEM_ARCHIVED
            elif news.state == 'UNM':
                if news.is_comment:
                    return ModerationEvents.COMMENT_UNPUBLISHED
                else:
                    return ModerationEvents.ITEM_UNPUBLISHED
            elif news.state == 'REJ':
                if news.is_comment:
                    return ModerationEvents.COMMENT_REJECTED
                else:
                    return ModerationEvents.ITEM_REJECTED
        else:
            if news.is_comment:
                return ModerationEvents.COMMENT_SAVED
            else:
                return ModerationEvents.ITEM_SAVED

MOD_EVENTS = [ ModerationEvents.ITEM_PUBLISHED, ModerationEvents.COMMENT_PUBLISHED, ModerationEvents.ITEM_ARCHIVED,
               ModerationEvents.COMMENT_ARCHIVED, ModerationEvents.ITEM_REJECTED, ModerationEvents.COMMENT_REJECTED ]


def get_moderation_events(user_ids, start_date, end_date, ai_ids = None):
    events = ModerationEventRecorder.objects.filter(user_id__in = user_ids, timestamp__range = (start_date, end_date))
    if ai_ids:
        events = events.filter(item__ai_id__in = ai_ids)
    return events

def is_item_moderated(item_id, moderation_events):
    return moderation_events.filter(item_id = item_id, event_type__in = MOD_EVENTS).exists()

def get_moderated_event(item_id, moderation_events):
    return moderation_events.filter(item_id = item_id, event_type__in = MOD_EVENTS).order_by('id')[0]

def is_item_transcribed(item_id, moderation_events):
    return moderation_events.filter(item_id = item_id, fields_changed__contains = 'transcript').exists()

def is_item_rated(item_id, moderation_events):
    return moderation_events.filter(item_id = item_id, fields_changed__contains = 'rating').exists()

def item_has_tag(item_id, tag, moderation_events):
    return moderation_events.filter(item_id = item_id, item__tags__contains = tag).exists()

class Call_stats(models.Model):
    ai = models.ForeignKey(App_instance)
    line = models.IntegerField(null = True)
    start = models.DateTimeField()
    end = models.DateTimeField()
    calls = models.PositiveIntegerField()
    callers = models.PositiveIntegerField()
    avg_dur = models.DecimalField(max_digits = 10, decimal_places = 2)

class Item_stats(models.Model):
    ai = models.ForeignKey(App_instance)
    start = models.DateTimeField()
    end = models.DateTimeField()
    new_items = models.PositiveIntegerField()
    """
    pub_items: items published during moderation for start-end duration
    total_pub_items: items in published state as on end
    """
    pub_items = models.PositiveIntegerField()
    total_pub_items = models.PositiveIntegerField()
    repub_items = models.PositiveIntegerField()
    new_comments = models.PositiveIntegerField()
    pub_comments = models.PositiveIntegerField()
    repub_comments = models.PositiveIntegerField()
    items_heard_count = models.PositiveIntegerField()
    items_skipped_count = models.PositiveIntegerField()
    comments_heard_count = models.PositiveIntegerField()
    comments_skipped_count = models.PositiveIntegerField()

class Forwarding_stats(models.Model):
    ai = models.ForeignKey(App_instance)
    items_forwarded = models.PositiveIntegerField()
    unique_items_forwarded = models.PositiveIntegerField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    top_forwarded_item = models.ForeignKey(News, related_name='news.forward_stats_top_item_set', null = True)
    top_forwarded_item_count = models.PositiveIntegerField()
    second_top_forwarded_item = models.ForeignKey(News, related_name='news.forward_stats_second_item_set', null = True)
    second_top_forwarded_item_count = models.PositiveIntegerField()

class Hourly_call_stats(models.Model):
    ai = models.ForeignKey(App_instance)
    start = models.DateTimeField()
    end = models.DateTimeField()
    calls = models.PositiveIntegerField()
    callers = models.PositiveIntegerField()
    avg_dur = models.DecimalField(max_digits = 10, decimal_places = 2)

class Item_heard_stats(models.Model):
    cdr = models.ForeignKey(Cdr)
    ai = models.ForeignKey(App_instance)
    item = models.ForeignKey(News)
    when_heard = models.DateTimeField()
    duration = models.DecimalField(max_digits=10, decimal_places=2)
    position = models.IntegerField()
    event = models.CharField(max_length=64, null=True)


class Forwarding_ai_properties(models.Model):
    ai = models.ForeignKey(App_instance, related_name = 'forwarding_ai')
    personal_msg_time_secs = models.PositiveSmallIntegerField(default = 5)
    max_tries = models.PositiveSmallIntegerField(default = 3)
    forwarding_schedule = models.ForeignKey(Transient_group_schedule, null = True, blank = True)
    forwarding_prompt_start = models.FileField(null = True, blank = True, upload_to='groups')
    forwarding_prompt_end = models.FileField(null = True, blank = True, upload_to='groups')
    ai_to_jump = models.ForeignKey(App_instance, null = True, blank = True, related_name = 'jump_ai')
    is_enabled = models.BooleanField( default = True )
    default_prompt = models.ForeignKey(Recording, null =True, blank = True)

    def __unicode__(self):
        return unicode( self.ai )

    def get_start_prompt_file(self):
        if self.forwarding_prompt_start:
            return self.forwarding_prompt_start.path
        return None

    def get_end_prompt_file(self):
        if self.forwarding_prompt_end:
            return self.forwarding_prompt_end.path
        return None

class Forwarding_log(models.Model):
    FORWARDING_SOURCES = (
                    ("FFC", "Forwarding from Call"),
                    )
    ai = models.ForeignKey(App_instance)
    forwarding_cdr = models.ForeignKey(Cdr, null = True)
    forwarded_item = models.ForeignKey(News)
    forwarded_to   = CalleridField()
    forwarding_event = models.CharField(max_length = 3, choices = FORWARDING_SOURCES)
    forwarding_time = models.DateTimeField(auto_now_add = True)
    forwarder_personal_recording = models.ForeignKey(Recording, null = True, blank = True)
    forwarded_sms = models.ForeignKey(SMS_message, null = True)

    def __unicode__(self):
        return str( self.forwarded_item.id ) + '_' + str( self.forwarded_to ) + '_' + str( self.forwarding_event )

    def get_personal_message_file(self):
        if self.forwarder_personal_recording:
            return self.forwarder_personal_recording.get_full_filename()
        return None

    def get_forwarded_item_file(self):
        if self.forwarded_item:
            return self.forwarded_item.detail.get_full_filename()
        return None

class Forwarding_group_call_log(models.Model):
    forwarding_log = models.ForeignKey(Forwarding_log)
    group_call_log = models.ForeignKey(Groups_call_log)
    creation_time = models.DateTimeField(auto_now_add = True)

class Event_ai_properties(models.Model):
    class Action():
        CALL_CONTRIBUTOR            = 'call_contributor'
        CALL_ON_PUBLISH_LIST        = 'call_on_publish_list'
        CALL_CONTRIB_COMMENT_PUB    = 'call_contrib_comment_pub'

    action_choices = (
        (Action.CALL_CONTRIBUTOR,           Action.CALL_CONTRIBUTOR),
        (Action.CALL_ON_PUBLISH_LIST,       Action.CALL_ON_PUBLISH_LIST),
        (Action.CALL_CONTRIB_COMMENT_PUB,   Action.CALL_CONTRIB_COMMENT_PUB),
        )

    ai = models.ForeignKey(App_instance)
    event_type = models.CharField(max_length = EVENT_LENGTH)
    order = models.PositiveSmallIntegerField(help_text = "Order of the action in the list of actions to be exec'ed")
    action = models.CharField(max_length = 512, choices = action_choices)

    on_publish_dialout_lists = models.ManyToManyField(Contact_list, null = True, blank = True)
    event_start_prompt = models.FileField(null = True, blank = True, upload_to = 'groups')
    event_end_prompt = models.FileField(null = True, blank = True, upload_to = 'groups')
    item_preview_secs = models.PositiveSmallIntegerField(default = 10, help_text = "Set to 0 to play the full item.")
    max_tries = models.PositiveSmallIntegerField(default = 3)
    schedule  = models.ForeignKey(Transient_group_schedule)
    on_pub_contrbutior_dialout_list = models.TextField()

    def get_fo_cl_mapping(self):  
        if self.on_pub_contrbutior_dialout_list:
            return self.on_pub_contrbutior_dialout_list
        return None

    def __unicode__(self):
        return str(self.ai) + "_" + str(self.order) + "_" + str(self.event_type)

    def get_start_prompt_filename(self):
        if self.event_start_prompt:
            return self.event_start_prompt.path
        return None

    def get_end_prompt_filename(self):
        if self.event_end_prompt:
            return self.event_end_prompt.path
        return None
    
    def clean(self):
        if self.event_type == "ITEM_REJECTED" and self.action != "call_contributor":
            raise ValidationError('Action cannot be call_on_publish_list/call_contrib_comment_pub for Event Type:ITEM_REJECTED')
        super(Event_ai_properties, self).clean()

class Event_notification_call_log(models.Model):
    news_item = models.ForeignKey(News)
    event_type = models.CharField(max_length = EVENT_LENGTH, null = False)
    group_call_log = models.ForeignKey(Groups_call_log, null = True)
    call_time = models.DateTimeField(auto_now_add = True)

class News_state():
    UNM = 'UNM'
    PUB = 'PUB'
    REJ = 'REJ'
    ARC = 'ARC'
    class NAMES:
        UNM = 'Unmoderated'
        PUB = 'Published'
        REJ = 'Rejected'
        ARC = 'Archived'


class News_vi_reference(models.Model):
    news = models.OneToOneField(News)
    vi_reference = models.ForeignKey(VI_reference)
    keypress_required = models.BooleanField(default = True)

    def __unicode__(self):
        return str(self.news) + "-" + str(self.vi_reference)

class Ad_channel_map(models.Model):
    ad = models.ForeignKey(Advertisement)
    channel = models.ForeignKey(Channel)

    def __unicode__(self):
        return str(self.ad) + "-" + str(self.channel)

class News_settings(models.Model):
    contrib_elsewhere_policy_choices = (
        ('ANCIENT', 'ANCIENT'),
        ('LATEST', 'LATEST'),
        ('INSTANCE_ID', 'INSTANCE_ID')
        )
    ai = models.OneToOneField(App_instance)
    allow_public_access = models.BooleanField(default = False)
    play_contrib_announce = models.BooleanField(default=True, help_text='mnews_contrib_announce prompt to be played or not after all the items get played.')
    validate_contrib = models.BooleanField(default = False, help_text = 'Ask contributor to listen to the recording and approve before submission.')
    autopublish = models.BooleanField(default = False, help_text = 'Autopublish all contributions for this ai. Validation will also be done.')
    autopublish_lists = models.ManyToManyField(Contact_list, blank = True, null = True, help_text = 'Autopublish contributions from those in these lists. Validation will also be done.')
    best_items_to_new_callers = models.BooleanField(default = False)
    is_contrib_elsewhere = models.BooleanField(default = False, help_text = 'Flag to check if contributions are to be saved in different mnews instance.')
    contrib_elsewhere_policy = models.CharField(null= True, blank = True,  max_length=512, choices = contrib_elsewhere_policy_choices)
    contrib_elsewhere_instance = models.ForeignKey(App_instance, related_name= 'mnews_app_instance', null = True, blank = True)
    contrib_elsewhere_tags = models.CharField(max_length = 255, null = True, blank = True)

    def __unicode__(self):
        return str(self.id) + "-" + str(self.ai)

signal_function_choices = (
    ('getDefaultMainChannelNews_v1', 'getDefaultMainChannelNews_v1'),
    ('getBestRatingNews_v1', 'getBestRatingNews_v1'),
    )

class News_selection_signal(models.Model):
    name = models.CharField(max_length = 255)
    function = models.CharField(max_length = 255, choices = signal_function_choices)

    def __unicode__(self):
        return self.name

class News_selection_signal_weights(models.Model):
    signal = models.ForeignKey(News_selection_signal)
    weight = models.DecimalField(max_digits = 3, decimal_places = 2)

    def __unicode__(self):
        return "%s - %s" % (unicode(self.signal), self.weight)

class News_selection_algo(models.Model):
    name = models.CharField(max_length = 255)
    signal_weights = models.ManyToManyField(News_selection_signal_weights)

    def __unicode__(self):
        return self.name

    def get_long_str(self):
        algo = self.name + ": "
        algo += " | ".join( ["%s * %s" % (s_w.signal.function, str(s_w.weight)) for s_w in self.signal_weights.all()] )
        return algo

class AB_testing_config(models.Model):
    ai = models.ForeignKey(App_instance)
    algos = models.ManyToManyField(News_selection_algo)
    active = models.BooleanField(default = False)

    def __unicode__(self):
        return unicode(self.ai) + ": " + ", ".join([unicode(algo) for algo in self.algos.all()])

class Selection_algo_callerid(models.Model):
    config = models.ForeignKey(AB_testing_config)
    callerid = CalleridField(max_length = CALLERID_LENGTH)
    algo = models.ForeignKey(News_selection_algo)

class Social_publish_log(models.Model):
    PUB_CHOICES = (
                ('PR', 'processing'),
                ('PB', 'published'),
                ('FL', 'failed'),
            )
    ai = models.ForeignKey(App_instance)
    news = models.ForeignKey(News)
    smedia = models.ForeignKey(SocialMedia)
    url    = models.CharField( max_length = 255, null = True, blank = True)
    status = models.CharField( max_length = 2, choices = PUB_CHOICES, null = True, blank = True )
    last_updated = models.DateTimeField( default = datetime.now )
    permalink = models.URLField( null = True, blank = True )

    @classmethod
    def get_publish_log( cls, ai, news, smedia, url ):
        try:
            social_log, created = Social_publish_log.objects.get_or_create( ai = ai, news = news, smedia = smedia, url = url )
            if created:
                social_log.status = 'PR'
                social_log.save()
            return ( social_log, created )
        except Exception,e:
            logger.exception( 'exception while creating social sharing log for news_id: %s.' %(str(news)) )

    @classmethod
    def update_url(cls, ai, smedia, old_url, new_url):
        try:
            social_log = Social_publish_log.objects.get( ai = ai, smedia = smedia, url = old_url )
            social_log.url = new_url
            social_log.save()
            return True
        except Exception,e:
            logger.exception( 'exception while updating publish url for url: %s.' %(str(new_url)) )
        return False

    @classmethod
    def update_log_status( cls, ai, smedia, url, status, permalink = None ):
        try:
            social_log = Social_publish_log.objects.get( ai = ai, smedia = smedia, url = url )
            social_log.status = status
            social_log.permalink = permalink
            social_log.save()
            return True
        except Exception,e:
            logger.exception( 'exception while updating social sharing log for url: %s.' %(str(url)) )
        return False

class Collection( models.Model ):
    LOGICAL_CHOICES = (
        ('and', 'And'),
        ('or', 'Or'),
    )
    name = models.CharField( max_length = 32, unique = True )
    title = models.CharField( max_length = 255, unique = True )
    tags = models.CharField( max_length = 255, null = True, blank = True, help_text="comma separated" )
    tags_relation = models.CharField( max_length=5, choices=LOGICAL_CHOICES, default='or' )
    locations = models.ManyToManyField( Location, null = True, blank = True )
    categories = models.ManyToManyField( Category, null = True, blank = True )
    category_relation = models.CharField( max_length=4, choices=LOGICAL_CHOICES, default='or' )
    ais = models.ManyToManyField( App_instance )
    start = models.DateTimeField( null = True, blank = True )
    end = models.DateTimeField( null = True, blank = True )
    max_items = models.PositiveIntegerField( default = 25 )
    cover_image = models.ForeignKey(Image_caption_map, null = True, blank = True)

    def __init__(self, *args, **kwargs):
        super( Collection, self ).__init__( *args, **kwargs )
        self.__initial = self._dict()

    def diff( self ):
        d1 = self.__initial
        d2 = self._dict()
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    def has_changed( self ):
        return bool( self.diff() )

    def _dict( self ):
        return model_to_dict( self, fields = [ field.name for field in  self._meta.fields ] )

    def __unicode__( self ):
        return unicode( self.name )

class News_collection( models.Model ):
    collection = models.ForeignKey( Collection )
    news_items = models.ManyToManyField( News, null = True, blank = True )

def collection_save_handler( sender, **kwargs ):
    collection = kwargs['instance']
    if collection.has_changed():
        from mnews.tasks import CollectionSaveTask
        CollectionSaveTask.apply_async( [ collection ] )

post_save.connect(collection_save_handler , sender=Collection, dispatch_uid="collection.news.save")

def app_specific_creation_tasks( ai_id, user_id = None ):
    Channel.create_default_channel( ai_id )

class Playlist(models.Model):
    SHARING_CHOICES = (
        ('PUB', 'Public'),
        ('PRI', 'Private'),
    )

    title = models.CharField(max_length=100)
    playlist_id = models.PositiveIntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    permalink_url = models.URLField(null=True, blank=True)
    sharing = models.CharField(null=True, max_length=3, choices=SHARING_CHOICES)
    remote_create = models.BooleanField(default=False)
    image_jpg = models.ImageField(null=True, upload_to=IMAGES_DIR)
    artwork_url = models.URLField(null=True, blank=True)
    data_sync = models.ForeignKey(Data_sync, null=True)
    order = models.PositiveIntegerField()

    def __unicode__(self):
        return self.title

    def playlist_ai(self):
        return Playlist_channel_map.objects.filter(playlist= \
                            self).latest('id').channel.ai.id

    @classmethod
    def update_playlist(cls, playlist, sc_playlist):
        playlist.title = sc_playlist.title
        playlist.playlist_id = sc_playlist.id
        playlist.created_at = datetime.strptime(sc_playlist.created_at, \
                                                "%Y/%m/%d %H:%M:%S +0000")
        playlist.sharing = 'PUB' if sc_playlist.sharing == 'public' else 'PRI'
        playlist.permalink_url = sc_playlist.permalink_url
        playlist.artwork_url = sc_playlist.artwork_url
        playlist.save()
        return playlist

    @classmethod
    def add_image(cls, playlist, image_file):
        if os.path.exists(image_file):
            playlist.image_jpg.save(os.path.basename(image_file), File(open(image_file)))
            return True
        return False

    def save(self, *args, **kwargs):
        if not self.order:
            self.order = (Playlist.objects.filter(data_sync=self.data_sync).order_by('-order')[0].order + 1) if \
                            Playlist.objects.filter(data_sync=self.data_sync).exists() else 1
        super(Playlist, self).save(*args, **kwargs)

class Playlist_channel_map(models.Model):
    playlist = models.ForeignKey(Playlist)
    channel  = models.ForeignKey(Channel)

    def __unicode__(self):
        return unicode(self.playlist) + '_' + unicode(self.channel)

class Playlist_obd_setting(models.Model):
    playlist = models.ForeignKey(Playlist)
    schedule = models.ForeignKey(Transient_group_schedule)

    def __unicode__(self):
        return unicode(self.playlist) + '_' + unicode(self.schedule)

class Playlist_sync_log(models.Model):
    pc_map = models.ForeignKey(Playlist_channel_map)
    sync_start_time = models.DateTimeField(auto_now_add=True)
    sync_end_time = models.DateTimeField(null=True)
    syncd_by = models.ForeignKey(User, null=True)
    success = models.BooleanField(default=False)
    items_syncd = models.ManyToManyField(News, null=True)

class Playlist_item(models.Model):
    playlist = models.ForeignKey(Playlist)
    item = models.ForeignKey(News)
    title = models.CharField(max_length=TITLE_LENGTH, null=True)
    transcript = models.TextField(null=True)
    item_location = models.CharField(max_length=TITLE_LENGTH, null=True)
    is_current = models.BooleanField()

    @classmethod
    @transaction.commit_manually
    def clear_playlist_items(cls, playlist_id, already_synced_ids):
        try:
            Playlist_item.objects.filter(playlist_id=playlist_id, \
                is_current=True).exclude(item_id__in= \
                already_synced_ids).update(is_current=False)
        except:
            transaction.rollback()
            return False
        else:
            transaction.commit()
        return True

    @classmethod
    @transaction.commit_manually
    def add_playlist_items(cls, playlist_id, news_items):
        try:
            playlist_items = []
            for news, location in news_items.items():
                playlist_item = Playlist_item(playlist_id=playlist_id,
                                              item=news,
                                              is_current=True,
                                              title=news.title,
                                              transcript=news.transcript,
                                              item_location=location)
                playlist_item.save()
                playlist_items.append(playlist_item)
        except:
            transaction.rollback()
        else:
            transaction.commit()
            playlist_items.sort(key=lambda x: x.item.pub_order)
            return playlist_items

    def item_url(self):
        url = reverse('api_dispatch_list', kwargs={ \
                'resource_name': 'mnews_playlist_item', 'api_name': 'v1'})
        return "%s%s%s/?item_id=%s"%(settings.JMR_SITE, url, \
                                    'get_item', self.id)

    def num_comments(self):
        return Playlist_item_comment.objects.filter(playlist_item_id=self.id).count()

    def contributor_name(self):
        contacts = Contact.objects.filter(number=self.item.callerid)
        if contacts:
            return contacts.latest('id').name
        else:
            return '-'

class Playlist_item_comment(models.Model):
    playlist_item = models.ForeignKey(Playlist_item)
    user = models.ForeignKey(User, null=True)
    comment = models.TextField()
    add_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return unicode(self.playlist_item) + '_' + unicode(self.user)

    def comment_user(self):
        if MV_app_user.objects.filter(app_user=self.user).exists():
            mv_app_user = MV_app_user.objects.get(app_user=self.user)
            return mv_app_user.contact.name or "mobile app user"
        else:
            return self.user.username

def generate_node_data( ai, ai_node_list ):
    ai_settings = App_instance_settings.objects.get( ai = ai )
    prompt_path = get_ai_prompt_set_path( ai )
    channel = Channel.get_default_channel( ai.id )
    news = list( News.objects.filter( ai = ai, channel = channel, state = News_state.PUB, is_comment = False ).order_by( 'pub_order' )[:MAX_PUB_NEWS_ON_CHANNEL] )
    recordings = [ '{0}{1}/{2}'.format( RECORDINGS_DIR, ai.id, each.detail.id ) for each in news if each.detail ]
    try:
        next_ai = VI_reference.objects.get( ai = ai, vi = ai_settings.vi_conf, reference = 'nextai' ).target_ai
        next_node_id = ( each[ 'node' ] for each in ai_node_list if each[ 'ai' ] == next_ai ).next()
    except:
        next_node_id = -1

    node_data = {
        "id": str( ( each[ 'node' ] for each in ai_node_list if each[ 'ai' ] == ai ).next() ),
        "ai": str( ai.id ),
        "vi": str( ai_settings.vi_conf.id ),
        "type": "mnews",
        "welcomePrompt": '{0}/mnews_welcome__{1}'.format( prompt_path, channel.name ),
        "instructionPrompt": '{0}/mnews_instructions__{1}'.format( prompt_path, channel.name ),
        "thankyouPrompt": '{0}/mnews_thank_you__{1}'.format( prompt_path, channel.name ),
        "contribPrompt": '{0}/mnews_contrib_record__{1}'.format( prompt_path, channel.name ),
        "contribThankYouPrompt": '{0}/mnews_contrib_thank_you__{1}'.format( prompt_path, channel.name ),
        "contribAnnouncePrompt": '{0}/mnews_contrib_announce__{1}'.format( prompt_path, channel.name ),
        "recordings": recordings,
        "bargein": "true",
        "nextNodeId": str( next_node_id ),
        "recordingLimit": '{0}s'.format( ai_settings.min_recording_duration )
    }
    return node_data

class MV_app_user( models.Model ):
    app_user = models.ForeignKey( User )
    contact = models.ForeignKey( Contact )
    is_registered = models.BooleanField( default = False )

class MV_SMS_detail(models.Model):
    name = models.CharField( max_length = 50 )
    sms_ai = models.ForeignKey( App_instance )
    sms_template = models.ForeignKey( SMS_template )
    max_tries = models.PositiveSmallIntegerField( default = 3)

class MV_user_registration_detail(models.Model):
    mv_user = models.ForeignKey(MV_app_user)
    sms_message = models.ForeignKey(SMS_message, null=True)
    reg_code_expected = models.CharField(max_length=5)
    add_time = models.DateTimeField(auto_now_add=True)
    expiry_time = models.DateTimeField()
    query_type = models.PositiveSmallIntegerField(null=True)

    class QueryType:
        NEW_USER_REGISTRATION      = 0
        EXISTING_USER_REGISTRATION = 1
        UPDATE_NAME                = 2
        UPDATE_NUMBER              = 3
        UPDATE_NAME_AND_NUMBER     = 4

    @classmethod
    def send_registration_message(cls, mv_user, number, query_type):
        from vapp.utils import random_with_N_digits
        sms_details = MV_SMS_detail.objects.all()[0]
        sms_ai = sms_details.sms_ai
        code_expected = random_with_N_digits(5)
        template_args = {'number': number, 'code_expected': code_expected}
        message = sms_details.sms_template.process_template(template_args)
        logger.debug('message generated: %s' %(message))
        sms = SMSTask.create_send_msg(sms_ai.id,
                                      message,
                                      number,
                                      sms_details.max_tries)
        reg_log =  MV_user_registration_detail(mv_user=mv_user,
                                               sms_message=sms,
                                               reg_code_expected=code_expected,
                                               query_type=query_type)
        reg_log.expiry_time = datetime.now() + timedelta(days=2)
        reg_log.save()
        return reg_log

class MV_registration_log( models.Model ):
    reg_detail = models.ForeignKey(MV_user_registration_detail)
    reg_code_entered = models.CharField( max_length = 5, null = True )
    time = models.DateTimeField( auto_now_add = True )

    @classmethod
    def check_and_log_attempt(cls, reg_details, reg_code_entered ):
        success = False
        for reg_detail in reg_details:
            rlog = MV_registration_log( reg_detail = reg_detail, reg_code_entered = reg_code_entered )
            rlog.save()
            success = ( reg_detail.reg_code_expected == reg_code_entered )
            if success:
                break
        return success

class App_access_log(models.Model):
    user        = models.ForeignKey(User, null=True)
    access_time = models.DateTimeField(auto_now_add=True)
    device_id   = models.CharField(max_length = 50, null=True)
    app_version = models.CharField(max_length=10, null=True)
    class Meta:
        abstract = True

class Playlist_access_log(App_access_log):
    playlists = models.ManyToManyField(Playlist, null=True)

    def type(self):
        return 'pl'

    @classmethod
    def add_access_log(cls, playlists, user=None, \
                        device_id=None, app_version=None):
        access_log = Playlist_access_log.objects.create( \
                                            user=user,
                                            device_id=device_id,
                                            app_version=app_version)
        access_log.playlists.add(*[playlist for playlist in playlists])
        access_log.save()
        return access_log

class Playlist_item_access_log(App_access_log):
    news = models.ManyToManyField(News, null=True)

    def type(self):
        return 'pi'

    @classmethod
    def add_access_log(cls, playlist_items, user=None, \
                        device_id=None, app_version=None):
        access_log = Playlist_item_access_log.objects.create( \
                                                user=user,
                                                device_id=device_id,
                                                app_version=app_version)
        access_log.news.add(*[each.item for each in playlist_items])
        access_log.save()
        return access_log

class Item_file_access_log(App_access_log):
    recording_id = models.PositiveIntegerField()

    def type(self):
        return 'ifa'

    @classmethod
    def add_access_log(cls, recording_id, user=None, device_id=None, app_version=None):
        access_log = Item_file_access_log.objects.create(recording_id=recording_id,
                                                         user=user,
                                                         device_id=device_id,
                                                         app_version=app_version)
        access_log.save()
        return access_log


class Playlist_obd_log(models.Model):
    playlist      = models.ForeignKey(Playlist)
    app_user      = models.ForeignKey(MV_app_user)
    gcl           = models.ForeignKey(Groups_call_log)
    creation_time = models.DateTimeField(auto_now_add=True)
    device_id     = models.CharField(max_length=50, null=True)
    app_version   = models.CharField(max_length=10, null=True)


class Rejection_reason(models.Model):
    reason = models.CharField(max_length=EVENT_LENGTH)
    rej_tag = models.CharField(max_length=EVENT_LENGTH, null=True, blank=True)
    rej_reason_prompt = models.FileField(upload_to = 'groups')
    play_item_preview = models.BooleanField(default=True)
    call_on_rejection = models.BooleanField(default=True)
    is_ml_tag = models.BooleanField(default=False)

    def __unicode__(self):
        return '{0}'.format(self.reason)

    def get_rejectreason_prompt_filename(self):
        if self.rej_reason_prompt:
            return self.rej_reason_prompt.path
        return None

    def clean(self):
        if not(self.rej_tag):
            raise ValidationError('rej_tag field is a mandatory field')
        super(Rejection_reason, self).clean()

class Rejection_reason_group(models.Model):
    rejection_reason = models.ManyToManyField(Rejection_reason, blank=True)
    
    def __unicode__(self):
        return '{0}'.format(self.id)
        
class Event_rejection_reason_group(models.Model):
    event = models.ForeignKey(Event_ai_properties)
    group = models.ForeignKey(Rejection_reason_group)

    def __unicode__(self):
        return '{0}_{1}'.format(self.event.ai,self.group)

class S3_sync_log(models.Model):
    sync_type = models.CharField(max_length=20)
    news = models.ForeignKey(News)
    success = models.BooleanField(default=True)
    exception = models.CharField(max_length=200, null=True)
