import inspect
import uuid

from datetime import datetime, timedelta
from threading import Thread, Condition
from django.db.models.fields.related import ReverseSingleRelatedObjectDescriptor
from twisted.internet import defer, protocol
from django.conf import settings

from eventsocket import *
from app_manager.models import Cdr, Transition_event, App_instance, App_instance_settings, VI_conf, VI_reference, \
STATUS_ACTIVE, Ai_vi_start_event, get_ai_from_short_code

from media.models import Recording, Prompt_audio
from media.paths import MEDIA_DIR

from mvas.models import Recording_map
import time

from vapp.log import get_logger
from vapp.telephony.utils import get_session_url, get_formatted_number
from vapp.app_manager.common import filters_allow_ai_caller
from vapp.utils import csv_to_dict

logger = get_logger() 


class VappProtocol(EventProtocol):
    def __init__(self):
        EventProtocol.__init__(self)
                
    @defer.inlineCallbacks
    def authRequest(self, event):
        try:
            yield self.auth( self.factory.password )
            self.factory.__class__.is_logged_in = True
            self.startStateMachine()
        except AuthError, e:
            logger.exception("Unable to login to freeswitch eventsocket")
        
    def startStateMachine(self):
        f = self.factory
        self.stateMachine = VappStateMachine( self, f.stoppedDeferred, f.session )
        self.stateMachine.start()
        
    def reportFsEvent(self, name, ctx):
        if self.stateMachine:
            self.stateMachine.reportFsEvent( name, ctx.data )
        
class VappFactory(protocol.ReconnectingClientFactory):
    maxDelay = 30
    is_logged_in = False
    
    def __init__(self, session ):
        self.password = 'ClueCon'
        
        VappFactory.stoppedDeferred     = defer.Deferred()        
        VappFactory.session             = session
        
    def buildProtocol(self, addr):
        protocol = VappProtocol()
        protocol.factory = self
        VappFactory.vappProtocol = protocol
        return protocol

class VappSessionData:
    def __init__(self, ai, callerid, cdrId = None, uniqueId = None, embedStack = None):
        self.ai = ai
        self.callerid = callerid
        self.initial_ai = ai
        self.embed_stack = embedStack or []
        self.app_data = {'customization': {}, 'app_selector': {}, 'helpline': {}, 'campaign': {},
                        'mnews': {'items_heard_info': []}, 'survey': {}, 'vmiss': {}, 'advert': {}, 'registration': {}
        }
        self.group_id = str(uuid.uuid1())
        self.initUuids(uniqueId)
        self.initCdrs(cdrId)
        
    def initUuids(self, uuid):
        self.uuids = []
        if uuid:
            self.uuids.append(uuid)

    def initCdrs(self, cdrId):
        self.cdrs = []
        self.session_cdrs = []
        if cdrId:
            cdr = Cdr.objects.get( pk = cdrId )
            self.cdrs.append(cdr)
            self.session_cdrs.append(cdr)

    def addUuid(self, uniqueID):
        if uniqueID not in self.uuids:
            self.uuids.append(uniqueID)

    def removeUuid(self, uniqueID):
        if uniqueID in self.uuids:
            self.uuids.remove(uniqueID)

    def addCdr(self, cdr):
        if cdr not in self.cdrs:
            self.cdrs.append(cdr)
            self.session_cdrs.append(cdr)
            
    def removeCdr(self, cdr):
        if cdr in self.cdrs:
            self.cdrs.remove(cdr)

    def pushEmbedStack(self, transition_string):
        if transition_string:
            self.embed_stack.append(transition_string)
            return True
        return False

    def popEmbedStack(self):
        if len( self.embed_stack ) > 0:
            return self.embed_stack.pop()
        return None


class VappSession:
    def __init__(self, ai, isIncoming, callerid, cdrId = None, uuid = None, isAnswered = False, viConf = None, vi_data = {} ):
        self.isIncoming = isIncoming
        self.isAnswered = isAnswered
        self.data       = VappSessionData(ai, callerid, cdrId, uuid)
        self.controllers  = {}
        self.descriptions = {}

        self.aiSettings = App_instance_settings.objects.get( ai = ai.id )

        if viConf:
            self.initialInterface = self.getVi( ai, viConf, vi_data )
        else:
            self.initialInterface = self.getVi( ai, self.aiSettings.vi_conf, vi_data )

        if isIncoming:
            startState = "incomingstart"
        else:
            startState = "outgoingstart"

        self.initialInterface[ 2 ].setStartState( startState )


    def getInitialInterface(self):
        return self.initialInterface

    def getVi(self, ai, viConf, vi_data = {}):
        return ( viConf, self.getViController( ai, viConf, vi_data ), self.getViDescription( viConf ) )

    def getViController(self, ai, viConf, vi_data = {}):
        if not self.controllers.has_key( viConf.id ):
            self.controllers[ viConf.id ] = self.loadController( ai, viConf, vi_data )
        self.controllers[ viConf.id ].vi_data = vi_data
        return self.controllers[ viConf.id ]

    def getViDescription(self, viConf):
        if not self.descriptions.has_key( viConf.id ):
            self.descriptions[ viConf.id ] = self.loadDescription( viConf )
        return self.descriptions[ viConf.id ]

    def loadController(self, ai, viConf, vi_data):
        controllerClass = self.loadObject( viConf.controller )
        return controllerClass( ai, self.data, vi_data )

    def loadDescription(self, viConf):
        description = self.loadObject( viConf.description )
        return StatesDescription( description )

    def loadObject(self, name):
        tokens = name.split('.')
        loadedObject = __import__( '.'.join( tokens[:-1] ) )
        for subModule in tokens[1:]:
            loadedObject = getattr( loadedObject, subModule )
        return loadedObject

class SMA:
    answer      = "answer"
    originate   = "originate"
    play_and_get_digits = "play_and_get_digits"
    playback    = "playback"
    record      = "record"
    record_call = "record_call"
    bridge      = "bridge"
    hangup      = "hangup"
    none        = "none"
    break_media = "break_media"
    play_and_return = "play_and_return"
    timed_sleep = "timed_sleep"

class SME:
    action_success         = 'sm_action_success'
    action_failure         = 'sm_action_failure'
    action_update          = 'sm_action_update'
    action_interrupted     = 'sm_action_interrupted'
    app_start              = 'sm_app_start'
    app_stop               = 'sm_app_stop'
    next_originate_url     = 'sm_next_originate_url'
    get_digits_no_digits   = 'sm_get_digits_no_digits'
    no_reference           = 'sm_no_reference'
    ai_time_out            = 'sm_ai_time_out'
    ai_inactive            = 'sm_ai_inactive'
    ref_filtered_out       = 'sm_ref_filtered_out'


class HUPCAUSE:
    CALL_REJECTED              = 'CALL_REJECTED'
    DESTINATION_OUT_OF_ORDER   = 'DESTINATION_OUT_OF_ORDER'
    NO_ANSWER                  = 'NO_ANSWER'
    NO_USER_RESPONSE           = 'NO_USER_RESPONSE'
    NORMAL_CIRCUIT_CONGESTION  = 'NORMAL_CIRCUIT_CONGESTION'
    NORMAL_CLEARING            = 'NORMAL_CLEARING'
    NORMAL_TEMPORARY_FAILURE   = 'NORMAL_TEMPORARY_FAILURE'
    NORMAL_UNSPECIFIED         = 'NORMAL_UNSPECIFIED'
    REQUESTED_CHAN_UNAVAIL     = 'REQUESTED_CHAN_UNAVAIL'
    USER_BUSY                  = 'USER_BUSY'
    
NO_REDIAL_CAUSES = [ HUPCAUSE.CALL_REJECTED,
                     HUPCAUSE.NO_ANSWER,
                     ]


def actionHandler( action ):
    def wrapped( func ):
        func.actionName = action
        return func
    return wrapped

def containsActionHandlers( klass ):
    for name, obj in inspect.getmembers( klass ):
        if hasattr(obj, 'actionName'):
            if inspect.ismethod( obj ):
                klass.actionHandlers[ obj.actionName ] = obj
            else:
                logger.error("Non-method decorated with actionHandler")
    return klass

@containsActionHandlers
class VappStateMachine:
    STATE_START = "sm_start"

    actionHandlers = {}
    smEventMap = {
                  SMA.answer: {'fs_channel_hangup': SME.action_failure },
                  SMA.originate: {'fs_channel_answer': SME.action_success, 'fs_channel_hangup': SME.action_failure },
                  SMA.play_and_get_digits: {'fs_channel_hangup': SME.action_failure },
                  SMA.playback: {'fs_channel_hangup': SME.action_failure },
                  SMA.record: {'fs_channel_hangup': SME.action_failure },
                  SMA.none: {'fs_channel_execute': SME.action_success, 'fs_channel_execute_complete':SME.action_success, 'fs_channel_hangup': SME.action_failure },
                  SMA.record_call: {'fs_channel_hangup': SME.action_failure, 'fs_record_start': SME.action_success },
                  SMA.bridge: {'fs_channel_hangup': SME.action_failure},
                  SMA.break_media: {'fs_channel_hangup': SME.action_failure, 'fs_playback_stop': SME.action_success, 'fs_channel_execute_complete': SME.action_success},
                  SMA.play_and_return: {'fs_channel_hangup': SME.action_failure, 'fs_playback_start':SME.action_success},
                  SMA.hangup: {'fs_channel_execute_complete':SME.action_success, 'fs_channel_hangup':SME.action_success},
                  SMA.timed_sleep: {'fs_channel_execute': SME.action_success, 'fs_channel_hangup': SME.action_failure },
    }
    ignoredFsEvents = [ 
                       'CALL_UPDATE',
                       'CHANNEL_CALLSTATE',
                       'CHANNEL_STATE',
                       'CHANNEL_DESTROY',
                       'CHANNEL_HANGUP_COMPLETE',
                       'CHANNEL_OUTGOING',
                       'CHANNEL_PARK', 
                       'CHANNEL_UNPARK',
                       'CHANNEL_UUID', 
                       'CODEC', 
                       'PRESENCE_IN',
                       'PLAYBACK_STOP',
                       'API',
                       'HEARTBEAT',
                       'RE_SCHEDULE',
                       'PRIVATE_COMMAND',
                       'RECV_INFO',
    ]
    ignoredFsEvents = set( ignoredFsEvents )

    def __init__(self, protocol, stoppedDeferred, session):
        self.fs                 = protocol
        self.stoppedDeferred    = stoppedDeferred
        self.session            = session
        self.stateCounter       = 0
        self.urlIndices         = {}

        self.dtmfBubbledEvent   = None
        self.dtmfEvent          = None

        self.curState           = VappStateMachine.STATE_START

        self.viConf, self.controller, self.statesDescription = session.getInitialInterface()


    def isDtmfEvent(self, data):
        return data.get( 'Event_Name' ) == 'DTMF'

    def isPlayAndGetCompleteEvent(self, data):
        return data.get( 'Event_Name' ) == 'CHANNEL_EXECUTE_COMPLETE' and data.get( 'Application' ) == 'play_and_get_digits'

    def getEventForDtmf(self, state, data):
        if self.isDtmfEvent( data ):
            digit = data['DTMF_Digit']
        elif self.isPlayAndGetCompleteEvent( data ):
            digit = data.get( 'variable_dtmfdigits' )
        else:
            return
        event = self.statesDescription.getTranslatedDtmf( state, digit ) or self.statesDescription.getTranslatedDtmf( state, 'any' )
        return event

    def canonicalEventName(self, fragment):
        return fragment.strip('_').lower()

    def logEventData(self, data):
        print "-"*20, "Begin: ", data['Event_Name'], "-"*20
        list = []
        for k, v in data.items():
            list.append( (k, v) )
        list.sort()
        for k, v in list:
            print k, ":", v
        print "-"*20, "End: ", data['Event_Name'], "-"*20

    def logEventDataToFile(self, data, filename = "/tmp/events.txt"):
        f = file( filename, "a" )
        text = [ "-"*20 + " Begin: " + data['Event_Name'] + " " + "-"*20 + "\n"]
        list = []
        for k, v in data.items():
            list.append( k + "\t" +  v + "\n" )
        list.sort()
        text.extend( list )
        text.append( "-"*20 + " End: " +  data['Event_Name'] + " " + "-"*20 + "\n" )
        f.writelines( text )
        f.close()

    def logChannelExecuteComplete(self, data):
        logger.debug("%s data: %s response: %s" % (data.get('Application'), data.get('Application_Data'), data.get('Application_Response')))

    def isAiViStartEvent(self, event, transitionEvent):
        return event == SME.app_start or self.isReferenceEvent(transitionEvent)

    def logTransition(self, prevAi, prevVi, curAi, curVi, stateCounter, prevState, event, transitionEvent, state, action, params, cdrId):
        filename = params.get("filename", "")
        uniqueName = filename.split( MEDIA_DIR )[1] if filename else ""
        transEvent = Transition_event( cdr_id = int(cdrId), ai = curAi, vi = curVi, state_counter = stateCounter, prev_state = prevState, event = event, trans_event = transitionEvent, state = state, action = action, filename = uniqueName)
        transEvent.save()

        if self.isAiViStartEvent( event, transitionEvent ):
            event = Ai_vi_start_event(prev_ai = prevAi, prev_vi = prevVi, next_ai = curAi, next_vi = curVi, trans_event = transEvent)
            event.save()

        if hasattr(self.controller, 'logViTransitionData'):
            self.controller.logViTransitionData(transEvent)
    
    def fsErrorHandlerWrapper(self, action):
        def fsErrorHandler(error):
            logger.debug("FS error for action: %s. Error: %s" % (action, error))
            self.transitionForEvents( self.curState, [ SME.action_failure ], { 'Event_Name': 'fs_error' } )
        return fsErrorHandler

    def dtmfBreakMediaErrorHandler(self, error):
        logger.error("Error in breaking media when dtmf was received. Error: %s", error)
        #self.transitionForEvents( self.curState, [ SME.action_failure ], { 'Event_Name': 'fs_error' } )

    @actionHandler("answer")
    def actionAnswer(self, params):
        uuid = params.get( "uuid" )
        cdrId = params.get( "cdr_id" )
        self.fs.set( "group_id=%s" % (self.session.data.group_id), uuid = uuid )
        self.fs.set( "cdr_id=%s" % (cdrId), uuid = uuid )
        d = self.fs.answer( uuid = uuid )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.answer ) )
    
    @actionHandler("hangup")
    def actionHangup(self, params):
        uuid = self.controller.getUuidParam(params.get( "uuid" ))
        d = self.fs.hangup( uuid = uuid )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.hangup ) )
        return d
        
    @actionHandler("none")
    def actionNone(self, params):
        d = self.fs.api( "global_setvar dummyvar = 1" )
        cdr_id = self.controller.getCdrIdParam()
        group_id = self.session.data.group_id
        d.addCallback( self.noneActionCompleteWrapper( cdr_id, group_id ) )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.none ) )


    def noneActionCompleteWrapper(self, cdr_id, group_id):
        def noneActionComplete(response):
            event = 'channel_execute'
            eventData = {
                'Event_Name': 'fs_' + event,
                'variable_cdr_id': cdr_id,
                'variable_group_id': group_id,
                }
            self.reportFsEvent(event, eventData)
        return noneActionComplete

    def originateErrorHandler(self, error):
        logger.info("OriginateError: " + str(error))
        eventData = { 
            'Event_Name': 'fs_channel_hangup',
            'variable_cdr_id': self.origCdrId,
            'variable_group_id': self.session.data.group_id,
            'Unique_ID': self.origUuid,
            'Answer_State': 'unanswered',
            'Hangup_Cause': error.value.args[0]['data']['rawresponse'].strip().split(' ')[1],
            }
        events = self.getSmEventHangupOriginate(eventData)
        events.insert( 0, 'fs_channel_hangup' )

        self.updateSessionState( "CHANNEL_HANGUP", eventData )
        #logger.debug("before transition for events: " + str(events) + ", curstate: " + str(self.curState))
        self.transitionForEvents( self.curState, events, eventData )
    
    @actionHandler("originate")
    def actionOriginate(self, params):
        urlIndex = self.getUrlIndex( self.viConf, self.curState )
        self.origCdrId = params['cdr_id']
        self.origUuid = params['uuid']
        apiCommand = "originate {group_id=%s,cdr_id=%s,origination_uuid=%s}%s &park" % (self.session.data.group_id, params['cdr_id'], params['uuid'], params['urls'][urlIndex])
        d = self.fs.api( apiCommand )
        d.addErrback( self.originateErrorHandler )
        return d
    
    @actionHandler("record_call")
    def actionRecordCall(self, params):
        apiCommand = "uuid_record %s start %s" % (params['uuid'], params['filename'])
        d = self.fs.api( apiCommand )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.record_call ) )

    @actionHandler("bridge")
    def actionBridge(self, params):
        apiCommand = "uuid_bridge %s %s" % (params['uuid1'], params['uuid2'])
        d = self.fs.api( apiCommand )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.bridge ) )

    @actionHandler("play_and_get_digits")
    def actionPlayAndGet(self, params):
        uniqueID = params.get( "uuid" )
        if not params.get('invalidFilename'):
            params['invalidFilename'] = "silence_stream://250"
        args = "%(minDigits)s %(maxDigits)s %(tries)s %(timeout)s %(terminators)s %(filename)s %(invalidFilename)s dtmfdigits \d+" % params
        d = self.fs.play_and_get_digits( args, uuid = uniqueID )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.play_and_get_digits ) )

    @actionHandler('break_media')
    def actionBreakMedia(self, params):
        uniqueID = self.controller.getUuidParam(params.get("uuid"))
        d = self.breakMedia(uniqueID)
        d.addErrback( self.fsErrorHandlerWrapper( SMA.break_media ) )

    @actionHandler("play_and_return")
    def actionPlayAndReturn(self, params):
        uniqueID = params.get("uuid")
        d = self.fs.playback( params['filename'], uuid = uniqueID, lock = False )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.play_and_return ) )

    @actionHandler("playback")
    def actionPlayback(self, params):
        uniqueID = params.get( "uuid" )
        d = self.fs.playback( params['filename'], uuid = uniqueID )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.playback ) )
        
    @actionHandler("record")
    @defer.inlineCallbacks
    def actionRecord(self, params):
        uniqueID = params.get( "uuid" )
        yield self.fs.set( "playback_terminators=*#1234567890", uniqueID )
        args = "%(filename)s %(timeLimit)s %(silenceThreshold)s %(silenceHits)s" % params
        d = self.fs.playback('tone_stream://%(500,0,620)', uuid = uniqueID)
        d.addErrback( self.fsErrorHandlerWrapper( SMA.record ) )
        d = self.fs.record( args, uuid = uniqueID )
        d.addErrback( self.fsErrorHandlerWrapper( SMA.record ) )
        
    @actionHandler("timed_sleep")
    def actionTimedSleep(self, params):
        time_in_secs = params.get("time")
        time.sleep( int(time_in_secs) )
        uuid = self.controller.getUuidParam(params.get( "uuid" ))
        d = self.fs.set( "dummyvar=0", uuid = uuid ) #To trigger an fs event
        d.addErrback( self.fsErrorHandlerWrapper( SMA.timed_sleep ) )
        
    @defer.inlineCallbacks
    def start(self):
        yield self.fs.filter_delete( 'all' )
        yield self.fs.eventplain( 'all' )
        for event in self.ignoredFsEvents:
            self.fs.nixevent("%s" % (event,))
        self.transitionForEvents( VappStateMachine.STATE_START, [ SME.app_start ], {} )
        
    def stopped(self):
        if not self.stoppedDeferred.called:
            self.stoppedDeferred.callback( "done" )
        
    def executeCallbackWithArgs(self, callback, args):
        numargs = len(inspect.getargspec( callback )[0]) - 1
        return callback( *args[:numargs] )
        
    def executePreState(self, state, event): 
        actionParams = {}
        callback = self.controller.getPreStateHandler( state )        
        if inspect.ismethod( callback ):
            try:                
                actionParams = self.executeCallbackWithArgs(callback, [self.curState, event] )
            except:
                logger.exception( "Exception in pre_%s" % state )                  
                self.transitionForEvents( state, [ SME.action_failure ], { 'Event_Name': 'pre_error' } )
                return
        if not type( actionParams ) == dict:
            logger.error("Return type from pre_%s is %s" % (state, type(actionParams)) )
        return actionParams
    
    def executePostState(self, state, event):
        callback = self.controller.getPostStateHandler( state )
        if inspect.ismethod( callback ):
            try:
                self.executeCallbackWithArgs(callback, [ event ])
            except:
                logger.exception( "Exception in post_%s" % state)
                self.transitionForEvents( state, [ SME.action_failure ], { 'Event_Name': 'post_error' } )
                return

    def executeAction(self, state, actionParams ):
        action = self.statesDescription.getAction( state )
        if not action:
            logger.error("Action not set for state %s" % state )
            return
        handler = self.actionHandlers[ action ]
        logger.info( " State: %s, action: %s, params: %s" % (state, action, actionParams) )
        try:
            handler( self, actionParams )
        except:
            logger.exception( "Exception in executing action %s for state %s" %(action, state) )
            self.transitionForEvents( state, [ SME.action_failure ], { 'Event_Name': 'action_error' } )
            return
            
    def executeWhileState(self, state, events, eventData ):
        whileCallbacks = []
        for event in events:
            whileCallbacks.append( self.controller.getWhileStateHandler( state, event ) )                
        whileCallbacks.append( self.controller.getWhileStateHandler( state, 'ANY' ) )
        whileCallbacks = [ c for c in whileCallbacks if c ]
        transitionEvents = []
        for callback in whileCallbacks:
            try:
                transitionEvents.append( callback( events, eventData ) )
            except:
                logger.exception("Exception in state: %s, while callback: %s" % (state, callback.func_name) )
                self.transitionForEvents( state, [ SME.action_failure ], { 'Event_Name': 'while_error__%s' % (callback.func_name) } )
                return

        if transitionEvents:
            return transitionEvents[0]        
    
    def stop(self):
        self.curState = 'stop'
        self.stateCounter += 1
        self.executePostState('stop', SME.app_stop)
        if self.session.isAnswered:
            self.actionHangup( {} )
        else:
            self.stopped()
        
    def setState(self, state, event = None):
        logger.info("Setting state: %s, prev: %s, event: %s"  % (state, self.curState, event) )
        if state == 'stop':
            self.stop()
            return {}
        
        actionParams = self.executePreState(state, event)
        self.curState = state
        self.stateCounter += 1
        self.curParams = actionParams
        self.executeAction( state, actionParams )

        return actionParams
    

    def acceptFsEvent(self, action, event, data):
        #logger.info("----------------- %s -----------------" % (event,))
        if action == SMA.play_and_get_digits and event == "DTMF":
            return False

        groupID = str(data.get('variable_group_id'))
        uniqueID = str(data.get('Unique_ID'))
        #Need to check group_id for originate response and 
        #uuids for dtmf events
        if (self.session.data.group_id != groupID) and (uniqueID not in self.session.data.uuids):
            return False
        if event == "CHANNEL_CREATE":
            logger.info("channel name:%s"%(data.get('variable_channel_name')))

        if event == "CHANNEL_ANSWER":
            self.fs.filter("Unique-ID %s" % (uniqueID,))

            #when dealing with multiple channels for e.g in a helpline, we need to track answer
            #events for the other channels that we may originate or receive
            self.fs.filter("Event-Name CHANNEL_ANSWER") 

        #The check below is only required if we do not have uuids of originated channels
        #in the uuid list. Right now this is not true, but still leaving this here till
        #final cleanup is done.
        if event == "CHANNEL_HANGUP":
            foundCdr = False
            cdrID = data.get('variable_cdr_id')
            for cdr in self.session.data.cdrs:
                if str(cdr.id) == cdrID:
                    foundCdr = True
                    break
            if not foundCdr:
                return False
        
        if event == "CHANNEL_EXECUTE_COMPLETE" and data.get("Application") == "park":
            return False


        if event == "PLAYBACK_START" and action == SMA.play_and_return:
            return True
        
        if event == "PLAYBACK_STOP" and action == SMA.break_media:
            return True


        return not event in self.ignoredFsEvents
    
    def breakMedia(self, uniqueID):
        return self.fs.api( "uuid_break %s" % uniqueID )

    def isReferenceEvent(self, event):
        return event[ 0 ] == '@'
    
    def isAiReference(self, reference):
        return reference[0:3] == '@ai'

    def getTargetFromAiReference(self, reference):
        arr = reference.split('_')
        ai_id = arr[1]
        state = arr[2]
        try:
            ai = App_instance.objects.get(pk = ai_id)
            vi = ai.app_instance_settings_set.all()[0].vi_conf
            if not vi:
                raise Exception("No vi found for ai_id: " + str(ai_id))
            return ai, vi, state, None
        except:
            logger.exception("Unable to look up vi for ai id: " + ai_id)
            return (None,)*4

    def getTargetForReference(self, viConf, reference):
        if self.isAiReference(reference):
            return self.getTargetFromAiReference(reference)
        else:
            try:
                refEntry = VI_reference.objects.get( vi = viConf, reference = reference[1:] )
                return refEntry.target_ai, refEntry.target_vi, refEntry.target_state, csv_to_dict(refEntry.vi_data)
            except:
                logger.info( "Unable to lookup target for reference vi: %s, ref: %s" % (viConf, reference) )
                return (None,)*4
    
    def loadNextInterface(self, ai, viConf, vi_data = {} ):
        self.viConf, self.controller, self.statesDescription = self.session.getVi( ai, viConf, vi_data )
        
    def getUrlIndex(self, viConf, state):
        if not self.urlIndices.has_key( viConf ):
            self.urlIndices[ viConf ] = {}
        if not self.urlIndices[ viConf ].has_key( state ):
            self.urlIndices[ viConf ][ state ] = 0
        return self.urlIndices[ viConf ][ state ]
    
    def advanceUrlIndex(self, viConf, state):
        numUrls = len( self.curParams['urls'] )
        self.urlIndices[ viConf ][ state ] = ( self.urlIndices[ viConf ][ state ] + 1 ) % numUrls
        return self.urlIndices[ viConf ][ state ]
    
    def getSmEventHangupOriginate(self, eventData):
        if self.redialOnOrigFailed(eventData):
            return [ SME.next_originate_url ] 
        else:
            return [ SME.action_failure ]

    def redialOnOrigFailed(self, eventData):
        urlIndex = self.advanceUrlIndex( self.viConf, self.curState )
        if urlIndex == 0:
            return False

        hangup_cause = eventData.get('Hangup_Cause')
        if hangup_cause and hangup_cause in NO_REDIAL_CAUSES:
            return False

        return True
    
    def transitionForEvents(self, state, events, data):
        transitionEvent = self.dtmfBubbledEvent or self.executeWhileState( state, events, data ) or self.getEventForDtmf(state, data) or events[-1]
        prevAi = self.controller.ai
        prevVi = self.viConf
        if self.isReferenceEvent( transitionEvent ):
            nextAi, nextViConf, nextState, viData = self.getTargetForReference( self.viConf, transitionEvent )
            if nextViConf is None:
                nextState = 'stop'
                transitionEvent = SME.no_reference
            elif not filters_allow_ai_caller(nextAi.id, self.controller.sessionData.cdrs[0].callerid, self.controller.sessionData.cdrs[0].id):
                nextState = 'stop'
                transitionEvent = SME.ref_filtered_out
            elif nextAi.status != STATUS_ACTIVE:
                nextState = 'stop'
                transitionEvent = SME.ai_inactive
        else:
            nextState = self.statesDescription.getNextState( state, transitionEvent )

        if self.dtmfBubbledEvent:
            self.dtmfBubbledEvent = None
        if nextState:
            if self.isDtmfEvent(data):
                self.dtmfBubbledEvent = transitionEvent
                self.dtmfEvent = events[1]
                d = self.breakMedia(data['Unique_ID'])
                d.addErrback( self.dtmfBreakMediaErrorHandler )
            else:
                if data.get('Event_Name', None) != 'post_error':
                    self.executePostState( state, transitionEvent )
                if self.isReferenceEvent( transitionEvent ):
                    if viData:
                        viData.update(self.controller.vi_data)
                    else:
                        viData = self.controller.vi_data
                    self.loadNextInterface( nextAi, nextViConf, viData )

                params = self.setState( nextState, transitionEvent )
                if self.isPlayAndGetCompleteEvent( data ):
                    self.dtmfEvent = 'dtmf_%s' % data['variable_dtmfdigits'] if data.get('variable_dtmfdigits', None) else None
                action = self.statesDescription.getAction( nextState )
                self.logTransition(prevAi, prevVi, self.controller.ai, self.viConf, self.stateCounter, state, self.dtmfEvent or events[0], transitionEvent, nextState, action, params, self.controller.getCdrIdParam(data.get('variable_cdr_id', None)))
                self.dtmfEvent = None

            
    def getSmEventsForFsEvent(self, action, eventName, data):
        if self.smEventMap.get( action ):
            sme = self.smEventMap[ action ].get( eventName )
            if sme:
                return [ sme ]

        if eventName == 'fs_channel_execute_complete':
            self.logChannelExecuteComplete(data)
            if action == SMA.play_and_get_digits and data.get('Application') == SMA.play_and_get_digits:
                if data.get('variable_dtmfdigits'):
                    return [ SME.action_success ]
                else:
                    return [ SME.get_digits_no_digits ]
            elif action == SMA.playback and data.get('Application') == SMA.playback:
                if data['Application_Response'] == 'FILE PLAYED':
                    return [ SME.action_success ]
                else:
                    return [ SME.action_failure ]
            elif action == SMA.record and data.get('Application') == SMA.record:
                return [ SME.action_success ]
        elif eventName == 'fs_dtmf':
            return ['dtmf_%s' % data['DTMF_Digit'], 'dtmf']
        elif eventName == 'fs_channel_answer' and action == SMA.answer:
            return [ SME.action_success ]
        elif eventName == 'fs_channel_hangup' and action == SMA.originate:
            return self.getSmEventHangupOriginate(data)
        return []
        
    def updateSessionState(self, name, data):
        if name in ['CHANNEL_ANSWER', 'CHANNEL_HANGUP']:
            self.session.isAnswered = data['Answer_State'] == 'answered'
            time = datetime.now()
            cdr = Cdr.objects.get(pk = data['variable_cdr_id'])
            uniqueID = data['Unique_ID']
            if name == 'CHANNEL_ANSWER':
                cdr.answered_time = time
                self.session.data.addUuid(uniqueID)
                self.session.data.addCdr(cdr)
            else:
                cdr.end_time = time
                cdr.hangup_cause = data['Hangup_Cause']
                self.session.data.removeUuid(uniqueID)
                self.session.data.removeCdr(cdr)
            if not cdr.uuid:
                cdr.uuid = uniqueID
            cdr.save()


    def isLastEvent(self, name, data):
        return self.curState == 'stop' and name == 'CHANNEL_HANGUP'
    
    def reportFsEvent(self, name, data):
        action = self.statesDescription.getAction( self.curState )
        if not self.acceptFsEvent( action, name, data ):
            return
        
        self.updateSessionState( name, data )
        if self.isLastEvent( name, data ):
            self.stopped()
            return
        eventName = 'fs_' + self.canonicalEventName( name )
        #logger.debug("Received event: " + str(name))
        events = self.getSmEventsForFsEvent(action, eventName, data)
        events.insert( 0, eventName )
        #logger.debug("before transition for events: " + str(name) + ", events: " + str(events) + ", curstate: " + str(self.curState))
        self.transitionForEvents(self.curState, events, data)


class StateMachineRunner(Thread):
    def __init__(self, session ):
        Thread.__init__(self)
        self.name       = 'ReactorThread'
        self.session    = session
        self.stateMachineRunningLock = Condition()
    
    def run(self):
        from twisted.internet import reactor
        reactor.connectTCP(settings.FREESWITCH_IP, 8021, self.factory)
        reactor.run(installSignalHandlers = 0)
            
    def stateMachineStopped(self, args):
        self.stateMachineRunningLock.acquire()
        self.stateMachineRunningLock.notify()
        self.stateMachineRunningLock.release()
        logger.info("Releasing lock-------------------------")
        
    def execute(self):
        self.stateMachineRunningLock.acquire()
        self.factory = VappFactory( self.session )
        self.factory.stoppedDeferred.addCallback( self.stateMachineStopped )
        if self.factory.is_logged_in:
            self.factory.vappProtocol.startStateMachine()
        else:
            self.start()
        self.stateMachineRunningLock.wait()
        self.stateMachineRunningLock.release()

def graphStateMachine( statesDescription, filename = '/tmp/graph.png', skipTransitions = None, suppressSuccess = True ):
    if skipTransitions is None:
        skipTransitions = [ ('sm_action_failure','stop') ]
    import pydot
    graph = pydot.Dot( graph_type = 'digraph' )
    actionColors = {'originate':'#CDFFCD', 'answer':'#CDFFCD','hangup':'#FFCDCD', 'playback':'#CDCDFF', 'record':'#FFFFCD', 'play_and_get_digits':'#FFCDFF'}
    edgeColors = { 'sm_action_success':'#69ff73' ,'sm_action_failure':'#ff6969'}
    edgeColorPalette = ['#6980ff', '#ffff69', '#69fffa', '#fd69ff', '#ffaa69', '#69ffa8', '#af69ff']
    paletteIndex = 0
    graph.set_node_defaults( **{'fontcolor':'#333333', 'style':'filled', 'shape':'rectangle' } )
    graph.set_bgcolor("black")
    annotatedNames = {}
    nodeColor = {}
    nodes = {}
    for state in statesDescription.getStates():
        action = statesDescription.getAction( state )
        name = "%s - %s" % ( state.upper(), action )
        annotatedNames[ state ] = name
        node = pydot.Node( name )
        node.set_fillcolor( actionColors.get( action, '#f0f0f0' ) )
        node.set_shape( 'record' )
        node.set_label( '{ %s | %s }' % ( state.upper(), action ) )
        graph.add_node( node )
        nodes[ name ] = node
    for state in statesDescription.getStates():
        source = annotatedNames[ state ]
        for event, destStateName in statesDescription.getTransitionsFrom( state ):
            dest = annotatedNames[ destStateName ]
            if not event in edgeColors:
                edgeColors[ event ] = edgeColorPalette[ paletteIndex ]
                paletteIndex = (paletteIndex + 1) % len( edgeColorPalette )
            color = edgeColors[ event ]
            if ( event, destStateName ) not in skipTransitions:
                label = "" if suppressSuccess and event == 'sm_action_success' else event
                graph.add_edge( pydot.Edge( nodes[ source ], nodes[ dest ], label = label, color = color, fontcolor = color, style = 'bold' ) )
    graph.write_png( filename )
    
class StatesDescription( object ):
    def __init__(self, stateMap ):
        self.action      = {}
        self.transitions = {}
        self.dtmf        = {}
        self.states      = []
        
        for state in stateMap:
            name = state['name']
            self.states.append( name )
            self.action[ name ] = state['action']
            transitions = state['transitions']
            self.transitions[ name ] = {}
            for nextState, transitionEvents in transitions.items():
                for transitionEvent in transitionEvents:
                    self.transitions[ name ][ transitionEvent ] = nextState
            dtmf = state.get('dtmf', {})
            self.dtmf[ name ] = {}
            for digit, event in dtmf.items():
                self.dtmf[ name ][ str(digit) ] = event
        
    def getStates(self):
        return list( self.states )
    
    def getAction(self, state):
        if state == 'stop':
            return SMA.hangup
        return self.action.get( state )
    
    def getNextState(self, state, event):
        if state == 'stop':
            return None
        
        if self.transitions.has_key( state ):
            return self.transitions[ state ].get( event )
        else:
            logger.error("Error in accessing transition for unknown state: %s event: %s" % (state, event) )
            return None
    
    def getTranslatedDtmf(self, state, digit):
        if self.dtmf.has_key( state ):
            return self.dtmf[ state ].get( str( digit ) )
        else:
            return None
        
    def getTransitionsFrom(self, state):
        result = []
        transitions = self.transitions.get( state, {} )
        for event, nextState in transitions.items():
            result.append( ( event, nextState ) )
        return result
        
    def setStartState(self, startState):
        self.transitions[ VappStateMachine.STATE_START ] = { SME.app_start: startState }
        self.action[ VappStateMachine.STATE_START ] = SMA.none

    
class BaseVappController( object ):
    def __init__(self, ai, sessionData, vi_data = {} ):
        self.ai = ai
        self.sessionData = sessionData
        self.vi_data = vi_data

        self.lang = None
 
        self.preStateHandlers   = {}
        self.postStateHandlers  = {}
        self.whileStateHandlers = {}
        
        self.initHandlers()


    def initHandlers(self):
        for name, method in inspect.getmembers( self ):
            if not inspect.ismethod( method ):
                continue
            handlerType = name.split( '_' )[0]
            handlerFor = name[ name.find( '_' ) + 1:]
            if handlerType == 'pre':
                self.preStateHandlers[ handlerFor ] = method
            elif handlerType == 'post':
                self.postStateHandlers[ handlerFor ] = method
            elif handlerType == 'while':
                tokens = handlerFor.split( '__' )
                stateName = tokens[0]
                if not self.whileStateHandlers.has_key( stateName ):
                    self.whileStateHandlers[ stateName ] = {}
                if len( tokens ) == 2:
                    self.whileStateHandlers[ stateName ][ tokens[1] ] = method
                elif len( tokens ) == 1:
                    self.whileStateHandlers[ stateName ][ 'ANY' ] = method
                else:
                    logger.error( "Error in while handler name format: %s" % name )

    def getUuidParam(self, uniqueID = None):
        return uniqueID if uniqueID else self.sessionData.uuids[0]

    def getCdrIdParam(self, cdrId = None):
        return cdrId if cdrId else self.sessionData.cdrs[0].id
    
    def addCdr(self, cdr):
        self.sessionData.addCdr(cdr)

    def getPreStateHandler(self, state):
        return self.preStateHandlers.get( state )
    
    def getPostStateHandler(self, state):
        return self.postStateHandlers.get( state )
    
    def getWhileStateHandler(self, state, event):
        whileHandlers = self.whileStateHandlers.get( state )
        if whileHandlers:
            return whileHandlers.get( event )
        else:
            return None
            
    def logEventData(self, data):
        print "-"*20, "Begin: ", data['Event_Name'], "-"*20
        list = []
        for k, v in data.items():
            list.append( (k, v) )
        list.sort()
        for k, v in list:
            print k, ":", v
        print "-"*20, "End: ", data['Event_Name'], "-"*20
        
    def getDtmfDigit(self, eventData):
        if eventData['Event_Name'] == 'DTMF':
            return eventData[ 'DTMF_Digit' ]
        elif eventData['Event_Name'] == 'CHANNEL_EXECUTE_COMPLETE':
            return eventData[ 'variable_dtmfdigits' ]

    def getOriginateParams(self, urls, cdr_id, uniqueID):
        return {'urls': urls, 'cdr_id': cdr_id, 'uuid': uniqueID}

    def getAnswerParams(self, uniqueID, cdrId):
        return {'uuid': uniqueID, 'cdr_id': cdrId}
    
    def getIncomingCallParams(self, uniqueID = None, cdrId = None):
        return self.getAnswerParams( self.getUuidParam(uniqueID), self.getCdrIdParam(cdrId) )
    
    def getStartCallParams(self, callerid, trigger = None):
        #TODO: Use create_uuid api call to get uuid from freeswitch
        uniqueID = str(uuid.uuid1())
        cdr = Cdr(is_incoming = False, ai_id = self.ai.id, start_time = datetime.now(), callerid = callerid, uuid = uniqueID)
        if trigger is not None:
            cdr.trigger = trigger
        cdr.save()
        self.addCdr(cdr)
        
        return self.getOriginateParams( get_session_url( callerid, self.ai ), cdr.id, uniqueID )

    def getBridgeCallsParams(self, uuid1, uuid2):
        return {'uuid1': uuid1, 'uuid2': uuid2}

    def getRecordCallParams(self, wrapper, recordField, autoSave = True, uniqueID = None):
        recording = self.createRecording(wrapper, recordField, autoSave)
        return {'uuid': self.getUuidParam(uniqueID), 'filename':recording.get_full_filename()}

    def createRecording(self, wrapper, recordField, autoSave):
        if type(recordField) == ReverseSingleRelatedObjectDescriptor:
            recordField = recordField.field.name
        recording = Recording(ai_id = self.ai.id)    
        recording.save()
        setattr(wrapper, recordField, recording)
        if autoSave:
            wrapper.save()
        return recording


    def getPromptFilename(self, promptName, lang = None):
        try:
            if lang:
                prompt = Prompt_audio.objects.get(prompt_set__app_instance_prompt_set__ai = self.ai.id,
                                                  prompt_set__lang = lang,
                                                  info__name = promptName)
            else:
                prompt = Prompt_audio.objects.get(prompt_set__app_instance_prompt_set__ai = self.ai.id, 
                                                  prompt_set__app_instance_prompt_set__is_current = True, 
                                                  info__name = promptName)
        except:
            logger.exception("Unable to lookup prompt: %s for ai: %s" % (promptName, self.ai) )
            return ''
        return str( prompt.get_full_filename() )
    
    def getPlayAndGetParams(self, filename, minDigits = 1, maxDigits = 1, tries = 1, timeout = 5000, terminators = '#', invalidFilename = '', uniqueID = None ):
        return { 'filename': filename, 'minDigits': minDigits, 'maxDigits': maxDigits, 'tries': tries, 'timeout': timeout, 'terminators': terminators, 'invalidFilename': invalidFilename, 'uuid': self.getUuidParam(uniqueID) }
    
    def getPromptPlayAndGetParams(self, promptName = None, lang = None, promptAudio = None, minDigits = 1, maxDigits = 1, tries = 1, timeout = 5000, terminators = '*#', invalidFilename = '', uniqueID = None, errorPromptName = None ):
        if not lang:
            lang = self.lang
        if promptAudio:
            filename = str( promptAudio.get_full_filename() )
        else:
            filename = self.getPromptFilename(promptName, lang)
        
        if errorPromptName:
            invalidFilename = self.getPromptFilename(errorPromptName, lang)
            
        return self.getPlayAndGetParams(filename, minDigits, maxDigits, tries, timeout, terminators, invalidFilename, uniqueID)
    
    def getPlaybackParams(self, filename, uniqueID = None):
        return { 'filename': filename, 'uuid': self.getUuidParam(uniqueID) }
    
    def getTimedSleepParams(self, time_in_secs, uniqueID = None):
        return { 'time': int(time_in_secs), 'uuid': self.getUuidParam(uniqueID) }
    
    def getPromptParams(self, promptName = None, promptAudio = None, lang = None, uniqueID = None):
        if promptAudio:
            filename = str( promptAudio.get_full_filename() )
        else:
            filename = self.getPromptFilename(promptName, lang)
        return self.getPlaybackParams(filename, uniqueID)
    
    def getRecordParams(self, filename, timeLimit, silenceThreshold = 5, silenceHits = 10, uniqueID = None):
        return { 'filename': filename, 'timeLimit': timeLimit, 'silenceThreshold': silenceThreshold, 'silenceHits': silenceHits, 'uuid':self.getUuidParam(uniqueID) }
    
    def getEmbeddedRecordingParams(self, timeLimit, wrapper, recordField, autoSave = True, uniqueID = None):
        recording = self.createRecording(wrapper, recordField, autoSave)
        return self.getRecordParams( recording.get_full_filename(), timeLimit, uniqueID = uniqueID )        

    def getPlayAndGetDigits(self, data):
        try:
            digits = int(data['variable_dtmfdigits'])
            return digits
        except:
            return None
           

def run_state_machine(appInstance, isIncoming, callerid, cdr_id = None, uniqueID = None, viConf = None, vi_data = {} ):
    try:
        session = VappSession( appInstance, isIncoming, callerid, cdr_id, uniqueID, viConf = viConf, vi_data = vi_data )
        runner = StateMachineRunner( session )
        runner.execute()
        return session
    except:
        logger.exception("Exception in running state machine for ai: %s, callerid: %s, uuid: %s" % (appInstance.id, callerid, uniqueID ) )

            
def run_state_machine_for_ai( appInstance, isIncoming, callerid, cdr_id = None, uniqueID = None ):
    try:
        session = VappSession( appInstance, isIncoming, callerid, cdr_id, uniqueID )
        runner = StateMachineRunner( session )
        runner.execute()
    except:
        logger.exception("Exception in running state machine for ai: %s, callerid: %s, uuid: %s" % ( appInstance.id, callerid, uniqueID ) )
        

__all__ = ['SMA', 'SME', 'VappStateMachine', 'BaseVappController', 'run_state_machine_for_ai']


class MvasVappStateMachine( VappStateMachine ):

    mvasStateEventsMap = {
        'NEWCALL': [ SME.app_start, SME.action_success ],
        'CALLEND': [ SME.action_failure ]
    }

    def __init__( self, session, callTrace ):
        self.session          = session
        self.stateCounter     = 0
        self.dtmfBubbledEvent = None
        self.dtmfEvent        = None
        self.curState         = VappStateMachine.STATE_START
        self.callTrace        = callTrace
        self.traceData        = {}
        self.callId           = None

        self.viConf, self.controller, self.statesDescription = session.getInitialInterface()

    def transitionForEvents( self, state, events, data ):
        transitionEvent = self.dtmfBubbledEvent or self.executeWhileState( state, events, data ) or self.getEventForDtmf( state, data ) or events[ -1 ]
        prevAi = self.controller.ai
        prevVi = self.viConf
        if self.isReferenceEvent( transitionEvent ):
            nextAi, nextViConf, nextState, viData = self.getTargetForReference( self.viConf, transitionEvent )
            if nextViConf is None:
                nextState = 'stop'
                transitionEvent = SME.no_reference
            elif not filters_allow_ai_caller( nextAi.id, self.controller.sessionData.cdrs[ 0 ].callerid, self.controller.sessionData.cdrs[ 0 ].id ):
                nextState = 'stop'
                transitionEvent = SME.ref_filtered_out
            elif nextAi.status != STATUS_ACTIVE:
                nextState = 'stop'
                transitionEvent = SME.ai_inactive
        else:
            nextState = self.statesDescription.getNextState( state, transitionEvent )

        if self.dtmfBubbledEvent:
            self.dtmfBubbledEvent = None
        if nextState:
            if self.isDtmfEvent( data ):
                self.dtmfBubbledEvent = transitionEvent
                self.dtmfEvent = events[ 1 ]
            else:
                if data.get( 'Event_Name', None ) != 'post_error':
                    self.executePostState( state, transitionEvent )
                if self.isReferenceEvent( transitionEvent ):
                    if viData:
                        viData.update( self.controller.vi_data )
                    else:
                        viData = self.controller.vi_data
                    self.loadNextInterface( nextAi, nextViConf, viData )

                params = self.setState( nextState, transitionEvent )
                if self.isPlayAndGetCompleteEvent( data ):
                    self.dtmfEvent = 'dtmf_%s' % data[ 'variable_dtmfdigits' ] if data.get( 'variable_dtmfdigits' ) else None
                action = self.statesDescription.getAction( nextState )
                self.logTransition( prevAi, prevVi, self.controller.ai, self.viConf, self.stateCounter, state, self.dtmfEvent or events[0], transitionEvent, nextState, action, params, self.controller.getCdrIdParam( data.get( 'variable_cdr_id' ) ) )
                self.dtmfEvent = None

    def setState( self, state, event = None ):
        logger.info( "Setting state: %s, prev: %s, event: %s"  % ( state, self.curState, event ) )
        if state == 'stop':
            self.stop()
            return { 'traceData': self.traceData }

        actionParams = self.executePreState( state, event )
        self.curState = state
        self.stateCounter += 1
        self.curParams = actionParams
        actionParams[ 'traceData' ] = self.traceData
        return actionParams

    def logTransition( self, prevAi, prevVi, curAi, curVi, stateCounter, prevState, event, transitionEvent, state, action, params, cdrId ):
        filename = params.get( "filename", "" )
        time = params[ 'traceData' ][ 'time' ]
        uniqueName = filename.split( MEDIA_DIR )[ 1 ] if filename else ""
        transEvent = Transition_event( cdr_id = int( cdrId ), ai = curAi, vi = curVi, state_counter = stateCounter, prev_state = prevState, event = event, trans_event = transitionEvent, state = state, action = action, filename = uniqueName )
        transEvent.save()
        transEvent.time = time
        transEvent.save()

        if self.isAiViStartEvent( event, transitionEvent ):
            event = Ai_vi_start_event( prev_ai = prevAi, prev_vi = prevVi, next_ai = curAi, next_vi = curVi, trans_event = transEvent )
            event.save()

        if hasattr( self.controller, 'logViTransitionData' ):
            self.controller.logViTransitionData( transEvent )

        mVasRecordingId = params[ 'traceData' ].get( 'mVasRecordingId' )
        if mVasRecordingId and uniqueName:
            self.controller.newNews.title = 'News, %s' % time.strftime( '%H:%M, %d %b\'%y' )
            self.controller.newNews.time = time
            self.controller.newNews.save()
            recordingId = uniqueName.split( '/' )[ 2 ].split( '.' )[ 0 ]
            recording = Recording.objects.get( pk = recordingId )
            recording.time = time
            recording.duration = params[ 'traceData' ][ 'detailDuration' ]
            recording.save()
            recording_map = Recording_map( recording = recording, mvas_recording_id = mVasRecordingId, call_id = self.callId )
            recording_map.save()

    def start( self ):
        self.callId = self.callTrace[ 0 ][ 2 ][ 'callId' ]
        for each in self.callTrace:
            self.traceData = {}
            self.traceData[ 'time' ] = datetime.strptime( each[ 0 ], '%Y-%m-%d %H:%M:%S' )
            self.traceData[ 'state' ] = each[ 1 ]

            for paramDict in each[ 2: ]:
                key = paramDict.keys()[ 0 ]
                self.traceData[ key ] = paramDict[ key ]

            events = self.mvasStateEventsMap.get( self.traceData[ 'state' ] )
            if events:
                for event in events:
                    self.transitionForEvents( self.curState, [ event ], self.traceData )
            else:
                self.logApplicationTransitions()

    def logApplicationTransitions( self ):
        if self.controller.ai.app.name == 'app_selector':
            self.logAppSelectorTransitions()
        elif self.controller.ai.app.name == 'mnews':
            self.logMnewsTransitions()

    def logAppSelectorTransitions( self ):
        if self.traceData[ 'state' ] == 'INSTRUCTION':
            for event in [ SME.action_success ] * 2:
                self.transitionForEvents( self.curState, [ event ], self.traceData )
        elif self.traceData[ 'state' ] == 'DTMF':
            self.traceData[ 'Event_Name' ] = 'CHANNEL_EXECUTE_COMPLETE'
            self.traceData[ 'Application' ] = 'play_and_get_digits'
            self.traceData[ 'variable_dtmfdigits' ] = self.traceData[ 'value' ]
            self.transitionForEvents( self.curState, [ SME.action_success ], self.traceData )

    def logMnewsTransitions( self ):
        if self.traceData[ 'state' ] == 'INSTRUCTION':
            for event in [ SME.action_success ] * 3:
                self.transitionForEvents( self.curState, [ event ], self.traceData )
        elif self.traceData[ 'state' ] == 'NEWS':
            if self.dtmfEvent:
                self.transitionForEvents( self.curState, [ SME.action_success ], self.traceData )
            elif self.curState == 'listnews':
                for event in [ SME.action_success ] * 4:
                    self.transitionForEvents( self.curState, [ event ], self.traceData )
            else:
                for event in [ SME.action_success ] * 2:
                    self.transitionForEvents( self.curState, [ event ], self.traceData )
        elif self.traceData[ 'state' ] == 'DTMF':
            if self.curState == 'contributeannounce':
                self.traceData[ 'Event_Name' ] = 'CHANNEL_EXECUTE_COMPLETE'
                self.traceData[ 'Application' ] = 'play_and_get_digits'
                self.traceData[ 'variable_dtmfdigits' ] = self.traceData[ 'value' ]
                self.transitionForEvents( self.curState, [ SME.action_success ], self.traceData )
            else:
                self.traceData[ 'Event_Name' ] = 'DTMF'
                self.traceData[ 'DTMF_Digit' ] = self.traceData[ 'value' ]
                self.transitionForEvents( self.curState, [ 'dtmf_%s' % self.traceData[ 'DTMF_Digit' ] ] * 2, self.traceData )
        elif self.traceData[ 'state' ] in [ 'CONTRIB', 'CONTRIBTHANKYOU', 'CONTRIBANNOUNCE' ]:
            self.transitionForEvents( self.curState, [ SME.action_success ], self.traceData )
        elif self.traceData[ 'state' ] == 'RECORDINGCOMPLETE':
            self.controller.vi_data[ 'detailDuration' ] = float( self.traceData[ 'duration' ] )
            self.controller.vi_data[ 'modifyRecordingAttribs' ] = False
            self.traceData[ 'time' ] -= timedelta( milliseconds = self.controller.vi_data[ 'detailDuration' ] )
            self.traceData[ 'mVasRecordingId' ] = self.traceData[ 'id' ]
            self.traceData[ 'detailDuration' ] = self.controller.vi_data[ 'detailDuration' ]
            self.transitionForEvents( self.curState, [ SME.action_success ], self.traceData )
        elif self.traceData[ 'state' ] == 'NEWSCOMPLETE':
            for event in [ SME.action_success ] * 3:
                self.transitionForEvents( self.curState, [ event ], self.traceData )
        elif self.traceData[ 'state' ] == 'MNEWSTHANKYOU':
            for event in [ SME.get_digits_no_digits, SME.action_success ]:
                self.transitionForEvents( self.curState, [ event ], self.traceData )

    def stop( self ):
        self.curState = 'stop'
        self.stateCounter += 1
        self.executePostState( 'stop', SME.app_stop )


def generate_call_info( call_start_trace, call_end_trace ):
    call_params = get_call_start_end_params( call_start_trace, call_end_trace )
    cdr = generate_cdr( call_params )
    info = {}
    info[ 'called_number' ] = call_params[ 'calledNumber' ]
    info[ 'cdr' ] = cdr
    info[ 'call_id' ] = call_params[ 'callId' ]
    info[ 'operator' ] = call_params[ 'operator' ]
    info[ 'circle' ] = call_params[ 'circle' ]
    return info

def get_call_start_end_params( call_start_params, call_end_params ):
    info = {}
    info[ 'start_time' ] = datetime.strptime( call_start_params[ 0 ], '%Y-%m-%d %H:%M:%S' )
    info[ 'answered_time' ] = info[ 'start_time' ]
    info[ 'end_time' ] = datetime.strptime( call_end_params[ 0 ], '%Y-%m-%d %H:%M:%S' )
    for params in [ call_start_params, call_end_params ]:
        for each in params:
            if type( each ) == dict:
                key = each.keys()[ 0 ]
                info[ key ] = each[ key ]

    return info

def generate_cdr( call_start_end_params ):
    number = get_formatted_number( call_start_end_params[ 'callerNumber' ] )
    if not number:
        raise Exception( "caller number not in correct format." )

    params = {}
    params[ 'answered_time' ] = call_start_end_params[ 'answered_time' ]
    params[ 'end_time' ] = call_start_end_params[ 'end_time' ]
    params[ 'is_incoming' ] = call_start_end_params.get( 'is_incoming', True )
    params[ 'ai' ] = get_ai_from_short_code( call_start_end_params[ 'calledNumber' ] )
    params[ 'callerid' ] = number
    params[ 'uuid' ] = str( uuid.uuid1() )
    params[ 'hangup_cause' ] = call_start_end_params[ 'reason' ]

    cdr = Cdr( **params )
    cdr.save()
    cdr.start_time = call_start_end_params[ 'start_time' ]
    cdr.save()
    return cdr

def log_call_data( cdr, call_trace ):
    try:
        session = VappSession( cdr.ai, cdr.is_incoming, cdr.callerid, cdr.id, cdr.uuid, True )
        sm = MvasVappStateMachine( session, call_trace )
        sm.start()
    except Exception, e:
        logger.error( "mvas data logging - error in log_call_data: %s" % str( e ) )