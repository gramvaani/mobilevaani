import sys, os, mad, traceback, gc
from decimal import Decimal
from vapp.app_manager.models import Transition_event

reload(sys)
sys.setdefaultencoding('utf8')

from django.contrib.contenttypes.models import ContentType

from app_manager.models import *
from mnews.models import *
from callerinfo.models import *
from location.models import *
from vapp.stats.models import Cumulative_call_stats, Stats, Stats_type

from vapp.utils import *
from vapp.app_manager import perms as app_manager_perms
from mnews.statsutils import *

from django.db.models import Q, Sum
from celery.task import Task

from datetime import *
from dateutil.relativedelta import relativedelta

from django.db.models import Count
from django.template.loader import render_to_string

import time
import shutil

import vapp.settings

from vapp.stats.stats import get_calls, get_ai_call_stats, populate_daily_cumulative_stats
import operator
from operator import itemgetter
from xlsxwriter import Workbook

from log import get_logger
logger = get_logger()
MIN_LISTENING_DURATION = 30
MIN_LISTENER = 5
MIN_LIKE = 1
MIN_COMMENT = 1

NEWS_STATES = [ News_state.PUB, News_state.ARC, News_state.REJ ]


def get_start_date(ai_id):
    stats = Item_stats.objects.filter(ai_id = ai_id)
    if stats.exists():
        return stats.order_by('-id')[0].end.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    else:
        stats = Transition_event.objects.filter(ai_id = ai_id)
        if stats.exists():
            return stats.order_by('id')[0].time.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
        else:
            return None


def get_ai_ids_for_package():
    ai_ids = []
    for app in Application.objects.filter(pkg_name = 'mnews'):
        ais = App_instance.objects.filter(app = app)
        ai_ids.extend([ai.id for ai in ais])
    return ai_ids


def populate_stats(ai, stats_start_date = None, stats_end_date = None, force_update = False):
    if not stats_start_date:
        stats_start_date = get_start_date(ai.id)
        if not stats_start_date:
            logger.info("Start date for populating stats for ai = " + str(ai.name) + " not found.")
            return
    if not stats_end_date:
        stats_end_date = datetime.now().replace(hour = 0, minute = 0, second = 0, microsecond = 0)

    logger.info("Populating stats for ai = " + str(ai.name) + " start date = " + str(stats_start_date) + " and end date = " + str(stats_end_date))
    for date in daterange(stats_start_date, stats_end_date):
        start_datetime = datetime.combine(date, datetime.min.time())
        end_datetime = datetime.combine(date + timedelta(days = 1), datetime.min.time())
    
        populate_call_stats(ai, start_datetime, end_datetime, force_update)
        populate_item_stats(ai, start_datetime, end_datetime, force_update)
        populate_forwarding_stats(ai, start_datetime, end_datetime, force_update)

        #If start of week then calculate last week's weekly stats also
        if is_start_of_week(end_datetime.date()):
            start = end_datetime - timedelta(days = 7)
            populate_call_stats(ai, start, end_datetime, force_update)
            populate_item_stats(ai, start, end_datetime, force_update)
            populate_forwarding_stats(ai, start, end_datetime, force_update)

        #If start of the month then calculate last month's monthly stats also
        if is_start_of_month(end_datetime.date()):
            start = (end_datetime - timedelta(days = 1)).replace(day = 1)
            #populate_call_stats(ai, start, end_datetime, force_update)
            populate_item_stats(ai, start, end_datetime, force_update)
            populate_forwarding_stats(ai, start, end_datetime, force_update)

    populate_daily_cumulative_stats(ai, force_update = force_update)


class Mnews_call_trace_entry():
    start_time = None
    end_time = None
    news = None
    end_cause = None

    def __init__(self, news, start_time):
        self.start_time = start_time
        self.news = news

    def set_ended(self, end_time, cause):
        self.end_cause = cause
        self.end_time = end_time

    def get_duration(self):
        if self.start_time and self.end_time:
            return get_total_seconds(self.end_time - self.start_time)
        else:
            return -1

    def dump(self, single_line = False):
        max_title_length = 15
        title = self.news.title[:max_title_length] if self.news and isinstance(self.news, News) else None
        if single_line:
            logger.info(title.encode('utf-8') + "\t" + str(self.get_duration()) + "\t")


class Mnews_call_trace:
    def __init__(self, cdr, ai):
        self.cdr = cdr
        self.entries = []
        self.build_call_trace(ai)

    def add_entry(self, entry):
        self.entries.append(entry)

    def get_total_items(self):
        total_items = 0
        for entry in self.entries:
            if isinstance(entry.news, News):
                total_items += 1
        return total_items

    def get_items(self):
        for entry in self.entries:
            if isinstance(entry.news, News):
                yield entry.news

    def get_nth_item_timestamp_duration(self, n):
        if n > len(self.entries):
            return (None, None, -1)
        item_num = 0
        i = 0
        while i < len(self.entries):
            entry = self.entries[i]
            if isinstance(entry.news, News):
                item_num += 1
                if item_num == n:
                    return (entry.news, entry.start_time, entry.get_duration())
            i += 1

        return (None, None, -1)

    def dump(self, single_line = False, news_only = False):
        if len(self.entries) == 0:
            return

        if single_line:
            logger.info(str(self.cdr.callerid) + "\t" + str(self.cdr.start_time) + "\t")
        else:
            logger.info("Title\t\tStart Time\t\tDuration\n")

        for entry in self.entries:
            if (news_only and entry.news) or not news_only:
                entry.dump(single_line)

        logger.info("\n")

    def get_call_events(self):
        events = Transition_event.objects.filter(cdr = self.cdr).order_by('state_counter')
        return events


    def create_listnewscomment_entry(self, news, time):
        trace_entry = Mnews_call_trace_entry(news, time)
        self.add_entry(trace_entry)
        return trace_entry


    def build_call_trace(self, ai):
        prev_entry = None
        events = self.get_call_events()
        for event in events:
            if prev_entry and event.prev_state in ['listnews', 'listcomments']:
                prev_entry.end_time = event.time
                prev_entry = None
            if event.ai == ai and event.state in ['listnews', 'listcomments']:
                news = get_news_from_trans_event(event)
                prev_entry = self.create_listnewscomment_entry(news, event.time)


def get_all_lines(ai):
    return Cdr.objects.filter(ai = ai).values_list('line', flat = True).distinct()


#Uses item heard stats table
#It is assumed that the table is populated before this call is made
def get_heard_skipped_stats(ai_id, start_datetime, end_datetime, skip_duration = 15):
    item_query = "select H.id from mnews_item_heard_stats H, mnews_news N where H.ai_id = %s and N.id = H.item_id and N.is_comment = 0 and H.when_heard >= %s and H.when_heard < %s and duration > %s"
    item_heard_count = len(list(Item_heard_stats.objects.raw(item_query, (ai_id, start_datetime, end_datetime, skip_duration))))

    comment_query = "select H.id from mnews_item_heard_stats H, mnews_news N where H.ai_id = %s and N.id = H.item_id and N.is_comment = 1 and H.when_heard >= %s and H.when_heard < %s and duration > %s"
    comment_heard_count = len(list(Item_heard_stats.objects.raw(comment_query, (ai_id, start_datetime, end_datetime, skip_duration))))

    item_query = "select H.id from mnews_item_heard_stats H, mnews_news N where H.ai_id = %s and N.id = H.item_id and N.is_comment = 0 and H.when_heard >= %s and H.when_heard < %s and duration <= %s"
    item_skip_count = len(list(Item_heard_stats.objects.raw(item_query, (ai_id, start_datetime, end_datetime, skip_duration))))

    comment_query = "select H.id from mnews_item_heard_stats H, mnews_news N where H.ai_id = %s and N.id = H.item_id and N.is_comment = 1 and H.when_heard >= %s and H.when_heard < %s and duration <= %s"
    comment_skip_count = len(list(Item_heard_stats.objects.raw(comment_query, (ai_id, start_datetime, end_datetime, skip_duration))))

    return (comment_heard_count, item_heard_count, comment_skip_count, item_skip_count)

def get_new_item_stats(ai_id, start_datetime, end_datetime):
    new_items_query = "select id from mnews_news where ai_id = %s and time >= %s and time < %s and is_comment = 0"
    new_items = len(list(News.objects.raw(new_items_query, (ai_id, start_datetime, end_datetime))))

    new_comments_query = "select id from mnews_news where ai_id = %s and time >= %s and time < %s and is_comment = 1"
    new_comments = len(list(News.objects.raw(new_comments_query, (ai_id, start_datetime, end_datetime))))
    return (new_comments, new_items)

def get_pub_repub_stats(ai_id, start_datetime, end_datetime):
    pub_comments = pub_items = repub_comments = repub_items = 0

    pub_items_query = "select E.id, E.item_id from mnews_moderationeventrecorder E, mnews_news N where E.timestamp >= %s and E.timestamp < %s and E.event_type = %s and N.id = E.item_id and N.ai_id = %s and not exists (select 1 from mnews_moderationeventrecorder E2 where E2.timestamp < E.timestamp and E.item_id = E2.item_id and E2.event_type = %s)"
    pub_items = len(list(ModerationEventRecorder.objects.raw(pub_items_query, (start_datetime, end_datetime, ModerationEvents.ITEM_PUBLISHED, ai_id, ModerationEvents.ITEM_PUBLISHED))))
    total_pub_items = News.objects.filter(ai_id=ai_id,
                                          time__lte=end_datetime,
                                          is_comment=False,
                                          state=News_state.PUB).count()

    repub_items_query = "select E.id, E.item_id from mnews_moderationeventrecorder E, mnews_news N where E.timestamp >= %s and E.timestamp < %s and E.event_type = %s and N.id = E.item_id and N.ai_id = %s and exists (select 1 from mnews_moderationeventrecorder E2 where E2.timestamp < E.timestamp and E.item_id = E2.item_id and E2.event_type = %s)"
    repub_items = len(list(ModerationEventRecorder.objects.raw(repub_items_query, (start_datetime, end_datetime, ModerationEvents.ITEM_PUBLISHED, ai_id, ModerationEvents.ITEM_PUBLISHED))))

    pub_comments_query = "select E.id, E.item_id from mnews_moderationeventrecorder E, mnews_news N where E.timestamp >= %s and E.timestamp < %s and E.event_type = %s and N.id = E.item_id and N.ai_id = %s and not exists (select 1 from mnews_moderationeventrecorder E2 where E2.timestamp < E.timestamp and E.item_id = E2.item_id and E2.event_type = %s)"
    pub_comments = len(list(ModerationEventRecorder.objects.raw(pub_comments_query, (start_datetime, end_datetime, ModerationEvents.COMMENT_PUBLISHED, ai_id, ModerationEvents.COMMENT_PUBLISHED))))

    repub_comments_query = "select E.id, E.item_id from mnews_moderationeventrecorder E, mnews_news N where E.timestamp >= %s and E.timestamp < %s and E.event_type = %s and N.id = E.item_id and N.ai_id = %s and exists (select 1 from mnews_moderationeventrecorder E2 where E2.timestamp < E.timestamp and E.item_id = E2.item_id and E2.event_type = %s)"
    repub_comments = len(list(ModerationEventRecorder.objects.raw(repub_comments_query, (start_datetime, end_datetime, ModerationEvents.COMMENT_PUBLISHED, ai_id, ModerationEvents.COMMENT_PUBLISHED))))

    return pub_comments, pub_items, repub_comments, repub_items, total_pub_items

def get_forwarding_counts(ai_id, start_datetime, end_datetime):                                                                        
    items_forwarded_list = Forwarding_log.objects.filter(ai=ai_id, forwarding_time__gt=start_datetime, forwarding_time__lt=end_datetime).values('forwarded_item')
    items_forwarded = items_forwarded_list.count()
    unique_items_forwarded = items_forwarded_list.distinct().count()
    
    top_two_items = list(Forwarding_log.objects.filter(ai=ai_id, forwarding_time__gt=start_datetime,
                         forwarding_time__lt=end_datetime).values('forwarded_item').annotate(forward_count=Count('forwarded_item'))\
                         .order_by('-forward_count')[:2].values_list('forwarded_item','forward_count'))

    # if only one item was forwarded in the selected time range
    if len(top_two_items) == 1:
        top_two_items.append((None, 0))
    # if no items were forwarded in selected time range
    elif len(top_two_items) == 0:
        top_two_items.extend([(None, 0)]*2)

    (top_forwarded_item, top_forwarded_item_count), (second_top_forwarded_item, second_top_forwarded_item_count) = top_two_items

    return items_forwarded, unique_items_forwarded, top_forwarded_item, top_forwarded_item_count, second_top_forwarded_item, second_top_forwarded_item_count


def populate_item_heard_stats(ai, start_datetime, end_datetime, force_update = False):
    logger.info("populate_item_heard_stats: " + str(start_datetime) + " -> " + str(end_datetime))

    calls = get_calls(ai.id, start_datetime, end_datetime)
    for cdr in calls:
        prev_stat = Item_heard_stats.objects.filter(cdr = cdr, ai = ai)
        prev_stat_exists = prev_stat.count() > 0
        if prev_stat_exists and not force_update:
            logger.info("Skipping repopulation of item_heard_stats for cdr_id: {0} and ai: {1}".format(cdr.id, ai))
            continue

        trace = Mnews_call_trace(cdr, ai)
        for i in range(1, trace.get_total_items() + 1):
            (item, timestamp, dur) = trace.get_nth_item_timestamp_duration(i)
            if item and timestamp:
                prev_stat_for_item = prev_stat.filter(item = item)
                if prev_stat_exists and prev_stat_for_item.exists():
                    prev_stat_for_item.update(when_heard = timestamp, duration = str(dur), position = i)
                else:
                    Item_heard_stats(cdr = cdr, ai = ai, item = item, when_heard = timestamp, duration = str(dur), position = i).save()


def populate_call_stats(ai, start_datetime, end_datetime, force_update = False):
    logger.info("populate_call_stats: ai = " + str(ai.name) + ", " + str(start_datetime) + " -> " + str(end_datetime))
    prev_stat = Call_stats.objects.filter(ai = ai, start = start_datetime, end = end_datetime)

    if prev_stat.exists():
        if force_update:
            call_stats = get_ai_call_stats(ai, start_datetime, end_datetime)
            prev_stat.update(calls = call_stats.calls, callers = call_stats.callers, avg_dur = str(call_stats.avg_call_dur))
        else:
            logger.info("populate_call_stats: stats for ai " + ai.name + " already present. skipping recalculation.")
    else:
        call_stats = get_ai_call_stats(ai, start_datetime, end_datetime)
        Call_stats(ai = ai, start = start_datetime, end = end_datetime, calls = call_stats.calls, callers = call_stats.callers, avg_dur = str(call_stats.avg_call_dur)).save()
        

def populate_forwarding_stats(ai, start_datetime, end_datetime, force_update = False):
    logger.info("populate_forwarding_stats: ai = " + str(ai.name) + ", " + str(start_datetime) + " -> " + str(end_datetime))
    prev_stat = Forwarding_stats.objects.filter(ai = ai, start = start_datetime, end = end_datetime)

    if prev_stat.exists():
        if force_update:
            items_forwarded, unique_items_forwarded, top_forwarded_item, top_forwarded_item_count, second_top_forwarded_item, second_top_forwarded_item_count = get_forwarding_counts(ai.id, start_datetime, end_datetime)
            prev_stat.update(items_forwarded = items_forwarded,
                            unique_items_forwarded = unique_items_forwarded,
                            top_forwarded_item_id = top_forwarded_item or None,
                            top_forwarded_item_count = top_forwarded_item_count,
                            second_top_forwarded_item_id = second_top_forwarded_item or None,
                            second_top_forwarded_item_count = second_top_forwarded_item_count)
        else:
            logger.info("populate_forwarding_stats: stats already present. skipping recalculation.")
    else:
        items_forwarded, unique_items_forwarded, top_forwarded_item, top_forwarded_item_count, second_top_forwarded_item, second_top_forwarded_item_count = get_forwarding_counts(ai.id, start_datetime, end_datetime)
        Forwarding_stats(ai = ai, start = start_datetime, end = end_datetime,
                            items_forwarded = items_forwarded,
                            unique_items_forwarded = unique_items_forwarded,
                            top_forwarded_item_id = top_forwarded_item or None,
                            top_forwarded_item_count = top_forwarded_item_count,
                            second_top_forwarded_item_id = second_top_forwarded_item or None,
                            second_top_forwarded_item_count = second_top_forwarded_item_count).save()


def populate_item_stats(ai, start_datetime, end_datetime, force_update=False):
    logger.info("populate_item_stats: ai = " + str(ai.name) + ", " + str(start_datetime) + " -> " + str(end_datetime))
    prev_stat = Item_stats.objects.filter(ai=ai, start=start_datetime, end=end_datetime)

    if prev_stat.exists():
        if force_update:
            new_comments, new_items = get_new_item_stats(ai.id, start_datetime, end_datetime)
            pub_comments, pub_items, repub_comments, repub_items, total_pub_items = get_pub_repub_stats(ai.id, start_datetime, end_datetime)
            comments_heard_count, items_heard_count, comments_skipped_count, items_skipped_count = get_heard_skipped_stats(ai.id, start_datetime, end_datetime)
            prev_stat.update(new_items=new_items,
                             new_comments=new_comments,
                             pub_items=pub_items,
                             total_pub_items=total_pub_items,
                             repub_items=repub_items,
                             pub_comments=pub_comments,
                             repub_comments=repub_comments,
                             items_heard_count=items_heard_count,
                             items_skipped_count=items_skipped_count,
                             comments_heard_count=comments_heard_count,
                             comments_skipped_count=comments_skipped_count)
        else:
            logger.info("populate_item_stats: stats for ai %s already present. skipping recalculation." % ai.name)
    else:
        new_comments, new_items = get_new_item_stats(ai.id, start_datetime, end_datetime)
        pub_comments, pub_items, repub_comments, repub_items, total_pub_items = get_pub_repub_stats(ai.id, start_datetime, end_datetime)
        comments_heard_count, items_heard_count, comments_skipped_count, items_skipped_count = get_heard_skipped_stats(ai.id, start_datetime, end_datetime)
        Item_stats(ai=ai,
                   start=start_datetime,
                   end=end_datetime,
                   new_items=new_items,
                   new_comments=new_comments,
                   pub_items=pub_items,
                   total_pub_items=total_pub_items,
                   repub_items=repub_items,
                   pub_comments=pub_comments,
                   repub_comments=repub_comments,
                   items_heard_count=items_heard_count,
                   items_skipped_count=items_skipped_count,
                   comments_heard_count=comments_heard_count,
                   comments_skipped_count=comments_skipped_count).save()

def populate_hourly_call_stats(ai, start_date, end_date, force_update = False):
    logger.info("populate_hourly_call_stats: ai = " + str(ai.name) + ", " + str(start_date) + " -> " + str(end_date))
    for date in daterange(start_date, end_date):
        for i in range(0, 24, 1):
            start_time = datetime.combine(date, datetime.min.time()) + timedelta(hours = i)
            end_time = start_time + timedelta(hours = 1)
            
            prev_stat = Hourly_call_stats.objects.filter(ai = ai, start = start_time, end = end_time)
            if prev_stat.exists():
                if force_update:
                    call_stats = get_ai_call_stats(ai, start_time, end_time)
                    prev_stat.update(calls = call_stats.calls, callers = call_stats.callers, avg_dur = str(call_stats.avg_call_dur))
                else:
                    logger.info('populate_hourly_call_stats: an entry for ' + str(start_time) + ' -> ' + str(end_time) + ' exists. Skipping recalculation.')
            else:
                call_stats = get_ai_call_stats(ai, start_time, end_time)
                Hourly_call_stats(ai = ai, start = start_time, end = end_time, calls = call_stats.calls, callers = call_stats.callers, avg_dur = str(call_stats.avg_call_dur)).save()


def generate_and_email_stats(ais, start_datetime, end_datetime, recipient_email, stats_type, adv_stats_ais = None):
    cumulative = (stats_type == Stats_type.DAILY)
    stats, attachments = get_stats(ais, start_datetime, end_datetime, cumulative, adv_stats_ais)
    subject, body = create_mail_content(stats, start_datetime.date(), (end_datetime - timedelta(days = 1)).date(), stats_type)
    attachments.append(generate_legends_attachment())
    send_email(subject, body, [recipient_email], attachments)


def get_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    data = []
    reports = []

    for ai in ais:
        stats = Stats()
        stats.ai_name = ai.name
        stats.bmark_items = get_total_bookmarks(ai, start_datetime, end_datetime)
        stats.bmark_accessed = get_total_bookmarks_accessed(ai, start_datetime, end_datetime)
        stats.call_stats = get_call_stats(ai.id, start_datetime, end_datetime)
        stats.item_stats = get_item_stats(ai.id, start_datetime, end_datetime)
        stats.forwarding_stats = get_forwarding_stats(ai.id, start_datetime, end_datetime)

        if stat_type == Stats_type.DAILY:
            stats.is_cumulative = True
            try:
                stats.cumulative_stats = Cumulative_call_stats.objects.get(ai = ai, to_time = end_datetime)
            except:
                pass

        data.append(stats)

    if ai_stats_settings:
        for setting in ai_stats_settings:
            output = '/tmp/%s_%s_content_stats.xlsx' % (setting.ai.id, setting.ai.name)
            reports.append(get_advanced_stats(setting.ai, start_datetime, end_datetime, output = output))

    return (data, reports)


def create_html_content(stats):
    return render_to_string('mnews/stats.html', { 'data': stats }) + LINE_BREAK

def generate_legends_attachment():
    message = """
    <html>
        <body>
            <div>Calls, Callers, Avg Call Duration, Cumulative Calls carry same meaning as implied by the name.</div>
            <div>New Items, New Comments are the user contributions that came into the system.</div>
            <div>Items Published/Republished, Comments Published/Republished are subset of the new items/comments above, that were selected for publication.</div>
            <div>Users can press a button to skip items/comments. Items Heard/Skipped, Comments Heard/Skipped indicates how many times an item or comment was heard or skipped.</div>
            <div>Users can press another button to like items and forward them to their friends. Bookmarked Items indicates how many times an item was actually liked by caller and possibly even forwarded to their friends.</div>
            <div>When friends get an SMS message recommendation from above, they can call Mobile Vaani and access the item for which they got a recommendation. This is the bookmarks accessed statistic.</div>
        </body>
    </html>
    """
    output_file_loc = '/tmp/MV_legends.html'
    output_file = open(output_file_loc, 'w')
    output_file.write(message)
    output_file.close()
    return output_file_loc


def get_call_stats(ai_id, start_datetime, end_datetime):
    try:
        call_stats = Call_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime, line = None)
    except:
        call_stat = get_ai_call_stats(App_instance.objects.get(id = ai_id), start_datetime, end_datetime)
        call_stats = Call_stats(calls = call_stat.calls, callers = call_stat.callers, avg_dur = call_stat.avg_call_dur)
    return call_stats


def get_item_stats(ai_id, start_datetime, end_datetime):
    try:
        return Item_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime)
    except:
        populate_item_stats(App_instance.objects.get(pk=ai_id), start_datetime, end_datetime)
        return Item_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime)

def create_mail_content(stats, start_date, end_date, stats_type):
    if stats_type == Stats_type.DAILY:
        subject = 'Mnews: Statistics for %s' % end_date
    else:
        subject = 'Mnews: Statistics for %s - %s' % (start_date, end_date)

    return subject, render_to_string('mnews/stats.html', stats)

def get_forwarding_stats(ai_id, start_datetime, end_datetime):
    try:
        return Forwarding_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime)
    except:
        populate_forwarding_stats(App_instance.objects.get(pk=ai_id), start_datetime, end_datetime)
        return Forwarding_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime)

def get_mnews_attrs():
    return [ 'id', 'title', 'callerid', 'time', 'location', 'tags', 'transcript', 'category', 'format', 'qualifier' ]

def copy_item_to_folder( folder_name, recording ):
    if not os.path.exists( folder_name + recording.get_basefilename() ) and \
       os.path.exists( recording.get_full_filename() ):
       shutil.copyfile( recording.get_full_filename(), folder_name + recording.get_basefilename() )

def get_campaign_categories_str( news ):
    if news.campaign_categories:
        return ','.join( set([ str(category.campaign.name) for category in news.campaign_categories.get_query_set() ]) )
    return ''

def copy_detailed_data(ai_id, tmp_dir = None):
    news_items = News.objects.filter( ai__id = ai_id )
    dump_data(news_items, tmp_dir)

def dump_data(news_items, tmp_dir = None):
    timestamp = datetime.now().date()
    tmp_dir = ( '/tmp/mnews_dump_{0}'.format(timestamp)) if not tmp_dir else tmp_dir
    recordings_dir = tmp_dir + "/recordings/"
    workbook_path = tmp_dir + "/" + str( timestamp ) + '.xlsx'
    
    if not os.path.exists( tmp_dir ):
        os.makedirs( tmp_dir )
        os.makedirs( recordings_dir )
    
    if os.path.exists( workbook_path ):
        os.remove( workbook_path )
    
    workbook = Workbook( workbook_path )
    sheet = workbook.add_worksheet()
    sheet.title = str(timestamp)

    
    data = []
    attrs = get_mnews_attrs()
    title_headers = attrs + [ 'Campaign Name', 'Audio Name']
    data.append( title_headers )
    for news in news_items:
        news_data = [ unicode(getattr(news, attr)) for attr in attrs ]
        news_data.append( get_campaign_categories_str(news) )        
        filename = news.detail.get_basefilename() if news.detail else 'None'
        news_data.append( filename )
        data.append( news_data )
        if news.detail:
            copy_item_to_folder( recordings_dir, news.detail )
    
    sheet = populate_spreadsheet( sheet, data )
    workbook.close()


def get_advanced_stats(ai, start_date, end_date, output = '/tmp/Content_stats.xlsx', monthly_adv_stats_req = False):
    stats_generators = [ get_location_stats, get_category_stats, get_tag_stats, get_channel_stats,
                         get_listening_stats, get_qualifier_stats, get_news_format_stats ]

    if monthly_adv_stats_req:
        stats_generators.extend( [get_stickiness_stats, get_call_stats_report] )

    data = [ each(ai, start_date, end_date) for each in stats_generators]
    output_file = output or '/tmp/%s_%s_content_stats.xlsx' % (ai.id, ai.name)
    generate_workbook(data, output_file)
    return output_file


def new_callers_callback_results(ai, start):
    callback_results = []
    month_count = 4
    for i in range(3,-1,-1):
        month_results = []
        start_month = start + relativedelta(months = -i)
        new_callers = get_new_callers(ai, start_month)
        callers_never_called_back = get_no_callback_results(new_callers, ai, start_month)
        (callback_sec_month, callback_third_month, callback_fourth_month) = get_no_callback_results(new_callers, ai, start_month, True)
        month_results.append(len(new_callers))
        month_results.append(len(callers_never_called_back))
        month_results.append(len(callback_sec_month))
        month_results.append(len(callback_third_month))
        month_results.append(len(callback_fourth_month))

        callback_results.append(month_results)

    return callback_results

    
def append_data(start, callers, group_1, group_2, group_3, avg_call_duration, call_dur_grp_one, call_dur_grp_two, call_dur_grp_three):
    data = []
    data.append(['Stickiness statistics', str(start.strftime("%B, %Y")), '', '', ''])

    data.append(['', '', '', '', ''])
    data.append(['User set 1(those who called more than once that month)', '', '', '', ''])
    data.append(['DAU', str(group_1[0]), '', '', ''])
    data.append(['MAU', str(group_1[1]), '', '', ''])
    data.append(['DAU/MAU', str(group_1[2]), '', '', ''])

    data.append(['', '', '', '', ''])
    data.append(['User set 2 (those who called that month and the last month)', '', '', '', ''])
    data.append(['DAU', str(group_2[0]), '', '', ''])
    data.append(['MAU', str(group_2[1]), '', '', ''])
    data.append(['DAU/MAU', str(group_2[2]), '', '', ''])

    data.append(['', '', '', '', ''])
    data.append(['User set 3 (those who called that month and in the last two months)', '', '', '', ''])
    data.append(['DAU', str(group_3[0]), '', '', ''])
    data.append(['MAU', str(group_3[1]), '', '', ''])
    data.append(['DAU/MAU', str(group_3[2]), '', '', ''])

    data.append(['', '', '', '', ''])
    data.append(['Average call duration', '', '', '', ''])
    data.append(['New users', str(avg_call_duration), '', '', ''])
    data.append(['User set 1(those who called more than once that month)', call_dur_grp_one, '', '', ''])
    data.append(['User set 2 (those who called that month and the last month)', call_dur_grp_two, '', '', ''])
    data.append(['User set 3 (those who called that month and in the last two months)', call_dur_grp_three, '', '', ''])

    data.append(['', '', '', '', ''])
    row = ['-', '-', '-', '-', '-']
    row[0] = 'Month'
    month_count = 4
    for i in range(3,-1,-1):
        end = start + relativedelta(months = -i)
        row[month_count - i] =  end.strftime("%B, %Y")
    data.append(row)

    data.append(['', '', '', '', ''])
    data.append(['Number of new users added', str(callers[0][0]), str(callers[1][0]), str(callers[2][0]), str(callers[3][0])])

    data.append(['', '', '', '', ''])
    data.append(['New users who never called again', str(callers[0][1]), str(callers[1][1]), str(callers[2][1]), str(callers[3][1])])

    data.append(['', '', '', '', ''])
    data.append(['New users who called in their 1st month but not in their 2nd month', str(callers[0][2]), str(callers[1][2]), str(callers[2][2]), str(callers[3][2])])

    data.append(['', '', '', '', ''])
    data.append(['New users who called in their 1st, 2nd month but not in their 3rd month', str(callers[0][3]), str(callers[1][3]), str(callers[2][3]), str(callers[3][3])])

    data.append(['', '', '', '', ''])
    data.append(['New users who called in their 1st, 2nd, 3rd month but not in their 4th month', str(callers[0][4]), str(callers[1][4]), str(callers[2][4]), str(callers[3][4])])
    return data


def get_new_callers(ai, start, cdrs = None):
    curr_month_callers = get_callers(ai, start, cdrs = cdrs)
    total_past_callers = get_callers(ai, start, 0, False, True, cdrs = cdrs)
    new_callers = list(set(curr_month_callers) - set(total_past_callers))
    return new_callers

        
def get_callers(ai, start, months_duration = 1, duration_till_today = False, duration_since_start = False, distinct = True, cdrs = None):
    cdrs = cdrs or Cdr.objects.filter(ai = ai, is_incoming = False, answered_time__isnull = False)
    if duration_till_today:
        cdrs = cdrs.filter(start_time__range = (start, date.today()))
    elif duration_since_start:
        cdrs = cdrs.filter(start_time__lt = start)
    else:
        end = start + relativedelta(months = months_duration)
        cdrs = cdrs.filter(start_time__range = (start, end))

    cdr_ids = cdrs.values_list('id', flat = True)
    outbound_cdrs = Groups_call_log.objects.filter(ai_id = ai.id, last_cdr_id__in = cdr_ids, success = True, last_cdr__isnull = False).values_list('last_cdr_id', flat = True)

    if distinct:
        return cdrs.exclude(id__in = outbound_cdrs).values_list('callerid', flat = True).distinct()

    return cdrs.exclude(id__in = outbound_cdrs).values_list('callerid', flat = True)

     
def get_no_callback_results(new_callers, ai, start, future_month_results = False):
    if future_month_results:
        curr_month_callers = get_callers(ai, start, 1, False, False, False)
        second_month_callers = get_callers(ai, start + relativedelta(months = 1))
        third_month_callers = get_callers(ai, start + relativedelta(months = 2))
        fourth_month_callers = get_callers(ai, start + relativedelta(months = 3))

        callers = [caller for caller in new_callers if curr_month_callers.count(caller) >= 1]
        calls_first_not_second = list(set(callers) - set(second_month_callers))
        calls_first_second_not_third = list(set(callers).intersection(second_month_callers) - set(third_month_callers))
        calls_first_sec_third_not_fourth = list(set(callers).intersection(second_month_callers).intersection(third_month_callers) - set(fourth_month_callers))

        return (calls_first_not_second, calls_first_second_not_third, calls_first_sec_third_not_fourth)
    else:
        callers_curr_month_till_today = get_callers(ai, start, 0, True, False, False)
        callers = [caller for caller in new_callers if callers_curr_month_till_today.count(caller) == 1]

        return callers


# Group 1: those who called atleast twice in this month
def get_dau_mau_group_one(ai, start, days, curr_month_callers):
    callers = get_callers_group_one(curr_month_callers)
    dau = get_dau(ai, start, days, callers)
    mau = len(callers)
    if mau: return (dau, mau, get_ratio(dau, mau))
    else: return (dau, None, None)


# Group 2: those who called this month and previous month
# Group 3: those who called this month and previous two months
def get_dau_mau_group_two_three(ai, start, days, month_count, curr_month_callers, prev_month_callers, prev_to_prev_month_callers = None):

    if month_count == 1:
        callers_for_grp = get_callers_group_two(curr_month_callers, prev_month_callers)
    elif month_count == 2:
        callers_for_grp = get_callers_group_three(curr_month_callers, prev_month_callers, prev_to_prev_month_callers)

    dau = get_dau(ai, start, days, callers_for_grp)
    mau = len(callers_for_grp)
    if mau:
        return (dau, mau, get_ratio(dau, mau))
    else:
        return (dau, None, None)


def get_dau(ai, start, days, mau_callers):
    usr_count = 0
    cdrs = Cdr.objects.filter(ai = ai, is_incoming = False, answered_time__isnull = False, callerid__in = mau_callers)

    if not cdrs:
        return get_ratio(usr_count, days)

    for day in range(days):
        start_date = start + relativedelta(days = day)
        end_date = start + relativedelta(days = day + 1)

        cdr_time_filtered = cdrs.filter(start_time__gte = start_date, start_time__lt = end_date)
        cdr_ids = list(cdr_time_filtered.values_list('id', flat = True).distinct())
        outbound_cdrs = list(Groups_call_log.objects.filter(ai__id = ai.id, last_cdr__id__in = cdr_ids, success = True, last_cdr__isnull = False).values_list('last_cdr_id', flat = True).distinct())

        if (not cdr_ids) or (not outbound_cdrs):
            usr_count += cdr_time_filtered.values_list('callerid', flat = True).count()
            continue
        usr_count += cdr_time_filtered.exclude(id__in = outbound_cdrs).values_list('callerid', flat = True).count()

    return get_ratio(usr_count, days)

     
def get_tags_dict(items_list):
    dict_items = {}
    for items in items_list:
        items = items.strip()
        if items:
            for item in items.split(','):
                item = item.strip()
                if item:
                    item = item.lower()
                    dict_items[item] = dict_items.get(item, 0) + 1

    return dict_items


def get_tag_set(news):
    tag_set = set()
    for tags in news.filter(tags__isnull = False).exclude(tags = '').values_list('tags', flat = True).distinct():
        tags = tags.strip()
        if tags:
            for tag in tags.split(','):
                tag = tag.strip()
                if tag:
                    tag_set.add(tag.lower())

    return tag_set


def populate_group_contributions_stats(ai, start_time, end_time = datetime.now(), groups_name_ids = []):

    if (not (groups_name_ids and ai and start_time)) or (start_time > end_time):
        logger.debug('invalid values passed as params in populate_group_contributions_stats')
        return None

    news_contributed = News.objects.filter(ai = ai, time__gte = start_time, time__lte = end_time)
    contact_news_grouping = news_contributed.values('callerid').annotate(news_count = Count('callerid'))

    logger.info('extracting stats for group contributions for groups: ' + str(groups_name_ids))
    contacts = []
    for contact_list_id in groups_name_ids:
        contact_list_contacts = Contact_list.objects.get(id = contact_list_id).contacts.get_query_set()
        for contact in contact_list_contacts:
            if contact not in contacts:
                contacts.append(contact)

    contacts_news_output = {}
    for contact in contacts:
        numbers = map((lambda str: str.strip()), contact.number.split(','))
        for contact_news in contact_news_grouping:
            if contact_news['callerid'] in numbers:
                contacts_news_output[contact] = int(contact_news['news_count'])

    return contacts_news_output


def get_calls_dist_stats(ai, histogram_ranges, start = None,  end = None, month = None, year = None, output = '/tmp/calls_dist_stats.xlsx'):
    if month and year:
        start_date, end_date = get_start_end_from_month_year(month, year)
    elif start and end:
        start_date = get_date_from_str(start)
        end_date   = get_date_from_str(end)
    else:
        logger.error('Date parameters given are of incorrect format.')
        return

    data=[]
    data.append(['For ' + str(start_date.date()) + ' to ' + str(end_date.date()), ''])
    data.append(['No calls', 'callers'])
    calls=Cdr.objects.filter(ai=ai, start_time__gte=start_date, start_time__lte=end_date, answered_time__isnull=False).values_list('callerid', flat=True)
    caller_new="(%s)" % ",".join(calls)
    count_list=[]
    for caller in calls:
        call_count=caller_new.count(caller)
        count_list.append(call_count)
        count_list.sort()

    for k,v in histogram_ranges.items():
        counter=0
        row = ['', '']
        row[0]=k
        min=v[0]
        max=v[1]
        row[1]=get_count_range(count_list, max, min)
        data.append(row)
    generate_attachment(data, output)
    return output

def get_freq_dist_stats(ai, histogram_ranges, start = None,  end = None, month = None, year = None, output = '/tmp/calls_freq_stats.xlsx'):
    freq_time=[]
    if month and year:
        start_date, end_date = get_start_end_from_month_year(month, year)
    elif start and end:
        start_date = get_date_from_str(start)
        end_date   = get_date_from_str(end)
    else:
        logger.error('Date parameters given are of incorrect format.')
        return
    data=[]
    data.append(['For ' + str(start_date.date()) + ' to ' + str(end_date.date()), ''])
    data.append(['Duration', 'callers'])
    listening_time=Cdr.objects.filter(ai=ai, start_time__gte=start, start_time__lte=end, is_incoming=False, end_time__isnull=False, answered_time__isnull=False).values_list('answered_time', 'end_time')
    for i in listening_time:
        t=i[1]-i[0]
        t_sec=t.total_seconds()
        freq_time.append(t_sec)
    for k,v in histogram_ranges.items():
        counter=0
        row=['_', '_']
        row[0]=k
        min=v[0]
        max=v[1]
        row[1]=get_count_range(freq_time, max, min)
        data.append(row)
    generate_attachment(data, output)
    return output

def get_n_top_bookmarked_items(ai, start_time, end_time = None, n = 5):
    top_bookmarks = None
    if n > 0:
        end_time = end_time or datetime.now()
        bookmarks = Bookmark.objects.filter(ai = ai, time__range = (start_time, end_time))
        top_bookmarks = bookmarks.values('news').annotate(news_count = Count('news')).order_by('-news_count')[:n]

    return top_bookmarks


def get_n_top_accessed_bookmarks(ai, start_time, end_time = None, n = 5):
    if n > 0:
        end_time = end_time or datetime.now()
        accessed_bookmarks = Bookmark_log.objects.filter(bookmark__ai = ai, time__range = (start_time, end_time), accessed = True)
        top_accessed_bookmarks = accessed_bookmarks.values('bookmark').annotate(bookmark_count = Count('bookmark')).order_by('-bookmark_count')[:n]
        return top_accessed_bookmarks


def get_total_bookmarks(ai, start_time, end_time = None):
    end_time = end_time or datetime.now()
    bookmark_count = Bookmark.objects.filter(ai = ai, time__range = (start_time, end_time)).count()
    return bookmark_count


def get_total_bookmarks_accessed(ai, start_time, end_time = None):
    end_time = end_time or datetime.now()
    accessed_bookmark_logs = Bookmark_log.objects.filter(bookmark__ai = ai, time__range = (start_time, end_time), accessed = True)
    return accessed_bookmark_logs.count()


def get_callers_atleast_ncallbacks(callers, n):
    callers_atleast_ncallbacks = [caller for caller in callers if callers.count(caller) >= n]
    return list(set(callers_atleast_ncallbacks))

    
# Group 1: those who called atleast twice in this month
def get_callers_group_one(callers):
    return get_callers_atleast_ncallbacks(callers, 2)


# Group 2: those who called this month and previous month
def get_callers_group_two(curr_month_callers, prev_month_callers):
    callers_atleast_one_callback = get_callers_atleast_ncallbacks(curr_month_callers, 1)
    return list(set(callers_atleast_one_callback).intersection(prev_month_callers))


# Group 3: those who called this month and previous two months
def get_callers_group_three(curr_month_callers, prev_month_callers, prev_to_prev_month_callers):
    callers_atleast_one_callback = get_callers_atleast_ncallbacks(curr_month_callers, 1)
    return list(set(callers_atleast_one_callback).intersection(prev_month_callers).intersection(prev_to_prev_month_callers))


def get_category_stats(ai, start_date, end_date):
    stats = {
             'name': 'Category Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append( [ 'Category', 'Sub-category', 'Published Items', 'Archived Items', 'Unmoderated Items', \
                            'Rejected Items', 'Published Comments', 'Archived Comments', 'Unmoderated Comments', \
                            'Rejected Comments','Sum(items + comments)', 'Total stats' ] )
    cat_data = []

    for cat in Category.objects.filter(parent__isnull = True, is_subcategory = False).distinct():
        cat_items = get_news_for_category(ai, cat, start_date, end_date, is_comment = False)
        cat_comments = get_news_for_category(ai, cat, start_date, end_date, is_comment = True)
        total_subcat_items = get_news_for_subcategory(ai, cat, start_date, end_date, is_comment = False)
        total_subcat_comments = get_news_for_subcategory(ai, cat, start_date, end_date, is_comment = True)
        row = populate_row_for_category(cat, cat_items, cat_comments, total_subcat_items, total_subcat_comments)
        cat_data.append(row)

    sorted_cat_data = sorted(cat_data, key = itemgetter(11), reverse = True)

    for row in sorted_cat_data:
        stats['data'].append(row)
        cat = Category.objects.filter(name = row[0], parent__isnull = True, is_subcategory = False)[0]
        for subcat in Category.objects.filter(parent__id = cat.id).distinct():
            subcat_items = get_news_for_category(ai, subcat, start_date, end_date, is_comment = False)
            subcat_comments = get_news_for_category(ai, subcat, start_date, end_date, is_comment = True)
            stats['data'].append( populate_row_for_subcategory(subcat, subcat_items, subcat_comments) )

    return stats


def get_location_contribs(location, start, end):
    if str(location.district.name).lower() == 'Not Known'.lower():
        return News.get_pub_arc_news().filter(ai = location.ai, location = location, time__range = (start, end))
    else:
        return News.get_pub_arc_news().filter(ai = location.ai, location__district = location.district, time__range = (start, end))


def get_location_stats(ai, start_date, end_date):
    stats = {
             'name': 'Location Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append( [ 'Village', 'Block', 'District', 'State', 'Published Items', 'Archived Items', \
                            'Unmoderated Items', 'Rejected Items', 'Published Comments', 'Archived Comments', \
                            'Unmoderated Comments', 'Rejected Comments', 'Total' ] )

    loc_stats = []
    for loc in Location.objects.filter(ai = ai, state__id__isnull = False).exclude(state__name = 'Not Known'):
        items = get_news_for_location(ai, loc, start_date, end_date, is_comment = False)
        comments = get_news_for_location(ai, loc, start_date, end_date, is_comment = True)
        loc_stats.append( populate_row_for_location(loc, items, comments) )

    loc_stats = sorted(loc_stats, key = itemgetter(2, 1))
    stats['data'].extend(loc_stats)
    return stats


def get_tag_stats(ai, start_date, end_date):
    stats = {
             'name': 'Tag Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append( ['Tag', 'Published', 'Archived', 'Unmoderated', 'Rejected'] )

    news = News.objects.filter(ai = ai, time__range = (start_date, end_date))
    for tag in get_tag_set(news):
        row = [tag]
        for state in [News_state.PUB, News_state.ARC, News_state.UNM, News_state.REJ]:
            news_count = news.filter(tags__icontains = tag, state = state).count()
            row.append(news_count)
        stats['data'].append(row)

    return stats


def get_channel_stats(ai, start_date, end_date):
    stats = {
             'name': 'Channel Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append(['Channel', 'Calls', 'Callers', 'Heard Duration (s)', 'Total Contribs', 'Published Contribs'])

    news = News.objects.filter(ai = ai, time__range = (start_date, end_date))

    for channel in Channel.objects.filter(ai = ai):
        contribs = news.filter(channel_id = channel.id)
        state_wise_contribs = get_news_state_distrib(contribs)
        item_heards = Item_heard_stats.objects.filter(item_id__channel = channel, when_heard__range = (start_date, end_date))
        heard_calls = set()
        heard_duration = 0
        heard_callers = set()
        for heard in item_heards:
            heard_duration += heard.duration
            heard_calls.add(heard.cdr.id)
            heard_callers.add(heard.cdr.callerid)

        row = [ channel.name, len(heard_calls), len(heard_callers), heard_duration ]
        row.extend([ state_wise_contribs['Total'], state_wise_contribs[News_state.PUB] + state_wise_contribs[News_state.ARC] ])
        stats['data'].append(row)

    return stats


def get_listening_stats(ai, start_date, end_date):
    stats = {
             'name': 'Listening Stats',
             'header_row_count': 1,
             'data': []
            }

    bookmarks = get_total_bookmarks(ai, start_date, end_date)
    bookmarks_accessed = get_total_bookmarks_accessed(ai, start_date, end_date)
    stats['data'].append( ['Total items bookmarked', bookmarks] )
    stats['data'].append( ['Total bookmarks accessed', bookmarks_accessed ] )
    stats['data'].append(['Item ID', 'Title', 'Item State', 'Contributor Number', 'Contributor Name', 'Date', 'Tags',\
        'Category', 'Channel', 'Transcript', 'Age Group', 'Gender', 'Moderator', 'Times Bookmarked',\
        'Listeners count', 'Avg Listening Time (s)', 'Median Listening Time (s)', 'Item Duration',\
        'Village', 'Panchayat', 'Block', 'District'] )

    kwargs = {}
    kwargs['ai_id'] = ai.id
    kwargs['time__gte'] = start_date
    kwargs['time__lt'] = end_date
    locations = { None:['Not Known']*4 }

    for news in News.get_pub_arc_news(**kwargs):
        try:
            bmark_count = Bookmark.objects.filter(ai = ai, news = news, time__range = (start_date, end_date)).count()
            listeners_count = get_unique_listeners_count(news.id, start_date, end_date)
            mean, median = get_mean_median_listening_time(news.id, start_date, end_date)
            category = news.category.name if (news.category) else ''
            age_group = news.age_group.name if (news.age_group) else ''
            gender = news.gender.gender if (news.gender) else ''
            if not locations.has_key(news.location_id):
                location = Location.objects.get(id=news.location_id)
                locations[news.location_id] = [location.village, location.panchayat.name,
                                               location.block.name, location.district.name]
            location = locations[news.location_id]
            moderator = (',').join(ModerationEventRecorder.objects.filter(item = news).values_list('user__username', flat = True).distinct())
            news_state = getattr(News_state.NAMES, news.state) if hasattr(News_state.NAMES, news.state) else  'Unknown'
            row = [news.id, news.title, news_state, news.callerid,
                   get_contact_name(news.callerid), news.time, news.tags,
                   category, news.channel.name, news.transcript, age_group,
                   gender, moderator, bmark_count, listeners_count, mean,
                   median, news.get_news_duration()]
            row.extend(location)
            stats['data'].append(row)
        except:
            pass

    return stats


def get_qualifier_stats(ai, start_date, end_date):
    stats = {
             'name': 'Qualifier Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append(['Qualifier', 'Published Items', 'Published Comments', 'Archived Items', 'Archived Comments', 'Total'])

    news = News.objects.filter(ai = ai, time__range = (start_date, end_date))

    for qualifier in Qualifier.objects.all():
        row = []
        for state in [News_state.PUB, News_state.ARC]:
            item_count = news.filter(qualifier = qualifier, state = state, is_comment = False).count()
            comment_count = news.filter(qualifier = qualifier, state = state, is_comment = True).count()
            row.append(item_count)
            row.append(comment_count)

        stats['data'].append([qualifier.name] + row + [sum(row)])

    return stats


def get_news_format_stats(ai, start_date, end_date):
    stats = {
             'name': 'News Format Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append(['Format', 'Published Items', 'Published Comments', 'Archived Items', 'Archived Comments', 'Total'])

    news = News.objects.filter(ai = ai, time__range = (start_date, end_date))

    for format in Format.objects.all():
        row = []
        for state in [News_state.PUB, News_state.ARC]:
            item_count = news.filter(format = format, state = state, is_comment = False).count()
            comment_count = news.filter(format = format, state = state, is_comment = True).count()
            row.append(item_count)
            row.append(comment_count)

        stats['data'].append([format.name] + row + [sum(row)])

    return stats


def get_call_stats_report(ai, start_date, end_date):
    stats = {
             'name': 'Call Stats',
             'header_row_count': 1,
             'data': []
            }

    stats['data'].append(['Date', 'Calls', 'Callers', 'Avg Call Duration'])

    data = []
    daily_stats = { 'calls': 0, 'callers': 0, 'avg_dur': 0 }

    start_datetime = datetime.combine(start_date, datetime.min.time())
    end_datetime = datetime.combine(end_date, datetime.min.time())
    days = (end_datetime - start_datetime).days

    for dt in daterange(start_datetime, end_datetime):
        to_datetime = dt + timedelta(days = 1)
        stats = get_call_stats(ai.id, dt, to_datetime)
        stats['data'].append([ dt.date(), stats.calls, stats.callers, stats.avg_dur ])
        daily_stats['calls'] += stats.calls
        daily_stats['callers'] += stats.callers
        daily_stats['avg_dur'] += stats.avg_dur

    stats_for_month = get_call_stats(ai.id, start_datetime, end_datetime)
    daily_avg_stats = {}
    for key, val in daily_stats.items():
        daily_avg_stats[key] = get_ratio(val, days)

    stats['data'].extend([ [''], [''] ])
    stats['data'].append([ 'Daily Average', daily_avg_stats['calls'], daily_avg_stats['callers'], daily_avg_stats['avg_dur'] ])
    stats['data'].append([ 'Total', stats_for_month.calls, stats_for_month.callers, stats_for_month.avg_dur ])
    return stats


def get_stickiness_stats(ai, start_date, end_date):
    stats = {
             'name': 'Stickiness Stats',
             'header_row_count': 1,
             'data': []
            }

    start_datetime = datetime.combine(start_date, datetime.min.time())
    end_datetime = datetime.combine(end_date, datetime.min.time())
    days = (end_datetime - start_datetime).days
    cdrs = Cdr.objects.filter(ai = ai, is_incoming = False, answered_time__isnull = False)

    new_callers = get_new_callers(ai, start_datetime, cdrs)
    curr_month_callers = get_callers(ai, start_datetime, 1, False, False, False)
    prev_month_callers = get_callers(ai, start_datetime + relativedelta(months = -1))
    prev_to_prev_month_callers = get_callers(ai, start_datetime + relativedelta(months = -2))

    group_1 = group_2 = group_3 = ['', '', '']

    group_1 = (get_dau_mau_group_one(ai, start_datetime, days, curr_month_callers))
    group_2 = (get_dau_mau_group_two_three(ai, start_datetime, days, 1, curr_month_callers, prev_month_callers))
    group_3 = (get_dau_mau_group_two_three(ai, start_datetime, days, 2, curr_month_callers, prev_month_callers, prev_to_prev_month_callers))

    (callers, calls, avg_duration) = get_callscount_callerscount_avgdur(ai.id, start_datetime, end_datetime, new_callers)
    call_stats_grp_one = get_callscount_callerscount_avgdur(ai.id, start_datetime, end_datetime, get_callers_group_one(curr_month_callers))
    call_stats_grp_two = get_callscount_callerscount_avgdur(ai.id, start_datetime, end_datetime, get_callers_group_two(curr_month_callers, prev_month_callers))
    call_stats_grp_three = get_callscount_callerscount_avgdur(ai.id, start_datetime, end_datetime, get_callers_group_three(curr_month_callers, prev_month_callers, prev_to_prev_month_callers))

    stats['data'] = append_data(start_datetime, new_callers_callback_results(ai, start_datetime), group_1, group_2, group_3, avg_duration, call_stats_grp_one[2], call_stats_grp_two[2], call_stats_grp_three[2] )
    return stats


def get_news_from_trans_event(event):
    if event.filename and event.state in ['listnews', 'listcomments']:
        news_detail_id = int(event.filename.split('/')[2].split('.')[0]) #Splitting string: recordings/ai_id/detail_id.mp3
        # objects.get not used since there's conflict in entries between media_recording and mnews_news.detail.id
        news = News.objects.filter(detail_id = news_detail_id)
        if news.exists():
            return news.order_by('id')[0]
    else:
        return None


def get_news_for_location(ai, location, start_date, end_date, is_comment):
    news = News.objects.filter(ai = ai, location__isnull = False, location_id = location.id, time__range = (start_date, end_date), is_comment = is_comment)
    data = get_news_state_distrib(news)
    return data


def get_news_state_distrib(news):
    data = { 'Total': 0 }
    for state in [ News_state.PUB, News_state.ARC, News_state.UNM, News_state.REJ ]:
        data[state] = news.filter(state = state).count()
        data['Total'] += data[state]

    return data


def populate_row_for_location(location, items, comments):
    row = []
    row.append(str(location.village))
    row.append(str(location.block.name))
    row.append(str(location.district.name))
    row.append(str(location.state.name))
    for each in [items, comments]:
        for state in [ News_state.PUB, News_state.ARC, News_state.UNM, News_state.REJ ]:
            row.append(each[state])

    row.append(items['Total'] + comments['Total'])
    return row


def get_news_for_category(ai, category, start_date, end_date, is_comment):
    news = News.objects.filter(ai = ai, category = category, time__range = (start_date, end_date), is_comment = is_comment)
    data = get_news_state_distrib(news)
    return data


def get_news_for_subcategory(ai, category, start_date, end_date, is_comment):
    sub_cats = Category.objects.filter(parent = category).values_list('id', flat = True)
    news = News.objects.filter(ai = ai, category_id__in = sub_cats, time__range = (start_date, end_date), is_comment = is_comment)
    data = get_news_state_distrib(news)
    return data


def populate_row_for_category(category, category_items, category_comments, total_subcat_items, total_subcat_comments):
    row = []
    row.append(str(category.name))
    row.append('-')
    for each in [category_items, category_comments]:
        for state in [ News_state.PUB, News_state.ARC, News_state.UNM, News_state.REJ ]:
            row.append(each[state])

    total_cat_news = category_items['Total'] + category_comments['Total']
    row.append(total_cat_news)
    row.append(total_cat_news + total_subcat_items['Total'] + total_subcat_comments['Total'])
    return row


def populate_row_for_subcategory(subcategory, subcategory_items, subcategory_comments):
    row = []
    row.append('-')
    row.append(str(subcategory.name))
    for each in [subcategory_items, subcategory_comments]:
        for state in [ News_state.PUB, News_state.ARC, News_state.UNM, News_state.REJ ]:
            row.append(each[state])

    row.append(subcategory_items['Total'] + subcategory_comments['Total'])
    return row


def get_unique_listeners_count(item_id, start, end):
    return Item_heard_stats.objects.filter(item_id = item_id, cdr__start_time__range = (start, end)).values('cdr__callerid').distinct().count()


def get_contribs_avg_listening_dur(contribs, start_date, end_date):
    if contribs:
        listeners_count = duration_sum = 0
        for contrib in contribs:
            stats = Item_heard_stats.objects.filter(item_id = contrib, when_heard__range = (start_date, end_date))
            if stats:
                duration_sum += stats.aggregate(Sum('duration'))['duration__sum']
                listeners_count += stats.count()
        if listeners_count != 0 and duration_sum != 0:
            return get_ratio(duration_sum, listeners_count)
    return 0.00


def get_mean_median_listening_time(item_id, start_date, end_date):
    stats = Item_heard_stats.objects.filter(item_id = item_id, cdr__start_time__range = (start_date, end_date))
    if not stats:
        return None, None
    mean = get_ratio(stats.aggregate(Sum('duration'))['duration__sum'], stats.count())
    median = get_median(stats.values_list('duration', flat = True))
    return mean, median


def get_contrib_count_by_caller(callerid, ai, start, end):
    return News.objects.filter(ai = ai, callerid = callerid, state__in = [News_state.PUB, News_state.ARC], time__range = (start, end)).count()


def get_mod_activity_stats(user_ids, start_date, end_date, ai_ids = None):
    data = [ ['User ID', 'User'] + MOD_EVENTS + ['Total', 'Transcribed', 'Rated'] ]
    stats = {}
    for user_id in user_ids:
        stats[user_id] = {}

    mod_events = get_moderation_events(user_ids, start_date, end_date, ai_ids)
    moderated_items = mod_events.values('item_id').distinct()

    for item in moderated_items:
        if not is_item_moderated(item['item_id'], mod_events):
            continue
        moderated_event = get_moderated_event(item['item_id'], mod_events)
        user_id = moderated_event.user.id
        event_type = moderated_event.event_type
        stats[user_id][event_type] = stats[user_id].get(event_type, 0) + 1
        if is_item_transcribed(item['item_id'], mod_events):
            stats[user_id]['Transcribed'] = stats[user_id].get('Transcribed', 0) + 1
        if is_item_rated(item['item_id'], mod_events):
            stats[user_id]['Rated'] = stats[user_id].get('Rated', 0) + 1

    for user_id in stats:
        row = [stats[user_id].get(event, 0) for event in MOD_EVENTS]
        data.append( [user_id, User.objects.get(id = user_id).username] + row + [sum(row), stats[user_id].get('Transcribed', 0), stats[user_id].get('Rated',0)] )

    return data


def get_total_transcriptions(user_ids, start_date, end_date):
    data = [ ['User ID', 'User', 'Total Transcriptions'] ]
    stats = {}

    mod_events = get_moderation_events(user_ids, start_date, end_date)
    moderated_items = mod_events.values('item_id').distinct()

    for item in moderated_items:
        if not is_item_moderated(item['item_id'], mod_events):
            continue
        if not is_item_transcribed(item['item_id'], mod_events):
            continue
        moderated_event = get_moderated_event(item['item_id'], mod_events)
        user_id = moderated_event.user.id
        stats[user_id] = stats.get(user_id, 0) + 1

    for user_id in user_ids:
        data.append( [user_id, User.objects.get(id = user_id).username, stats.get(user_id, 0)] )

    return data


def get_yesterdays_unm_items(ai_ids, end_date = None):
    if not end_date:
        end_date = datetime.combine(date.today(), datetime.min.time())
    start_date = end_date - timedelta(days = 1)
    return News.objects.filter(ai_id__in = ai_ids, time__range = (start_date, end_date), state = News_state.UNM)


def get_moderated_items_with_manual_tags(tags, user_ids, start_date, end_date):
    data = [ ['User ID', 'User'] + tags ]
    stats = {}
    for user_id in user_ids:
        stats[user_id] = {}

    mod_events = get_moderation_events(user_ids, start_date, end_date)
    moderated_items = mod_events.values('item_id').distinct()

    for item in moderated_items:
        if not is_item_moderated(item['item_id'], mod_events):
            continue
        for tag in tags:
            if not item_has_tag(item['item_id'], tag, mod_events):
                continue
            moderated_event = get_moderated_event(item['item_id'], mod_events)
            user_id = moderated_event.user.id
            stats[user_id][tag] = stats[user_id].get(tag, 0) + 1

    for user_id in stats:
        row = [stats[user_id].get(tag, 0) for tag in tags]
        data.append( [user_id, User.objects.get(id = user_id).username] + row )

    return data


def get_moderation_data_for_campaign(user_ids, campaign_tag, start_date, end_date, ai_ids = None):
    data = [ [campaign_tag], ['User ID', 'User'] + NEWS_STATES ]
    stats = {}
    for user_id in user_ids:
        stats[user_id] = {}

    mod_events = get_moderation_events(user_ids, start_date, end_date, ai_ids)
    moderated_items = mod_events.values('item_id').distinct()

    for item in moderated_items:
        if not is_item_moderated(item['item_id'], mod_events):
            continue
        if not item_has_tag(item['item_id'], campaign_tag, mod_events):
            continue
        moderated_event = get_moderated_event(item['item_id'], mod_events)
        user_id = moderated_event.user.id
        news_state = moderated_event.item.state
        stats[user_id][news_state] = stats[user_id].get(news_state, 0) + 1

    for user_id in stats:
        row = [stats[user_id].get(state, 0) for state in NEWS_STATES]
        data.append( [user_id, User.objects.get(id = user_id).username] + row )

    return data


def get_total_news_and_transcribed_items(ai_ids, tags, user_ids, start, end):
    news_count = News.objects.filter(ai_id__in = ai_ids, time__range = (start, end)).exclude(callerid = '91' + DEFAULT_CALLERID).count()
    data = [ ['Incoming News Items:', news_count], ['Tag', 'Transcribed News Items'] ]

    mod_events = get_moderation_events(user_ids, start, end)
    moderated_items = mod_events.values('item_id').distinct()
    transcribed_items = {}
    for item in moderated_items:
        if not is_item_moderated(item['item_id'], mod_events):
            continue
        for tag in tags:
            if not item_has_tag(item['item_id'], tag, mod_events):
                continue
            if not is_item_transcribed(item['item_id'], mod_events):
                continue
            transcribed_items[tag] = transcribed_items.get(tag, 0) + 1

    for tag in transcribed_items:
        data.append([tag, transcribed_items[tag]])

    return data


def track_campaign_category_moderation(tag):
    data = [ ['Item ID', 'Instance', 'Item State', 'Title', 'Tags', 'Campaign Category Marked'] ]

    for news in News.objects.filter(tags__contains = tag):
        if not news.campaign_categories.exists():
            data.append([news.id, news.ai.name, news.state, news.title, news.tags, 'No'])

    return data


def get_news_label_distribution(ai_ids, start_date, end_date, labels = None):
    labels = labels or ['AP', 'E', 'G', 'GG', 'I', 'PM', 'RI', 'U', 'V', 'D', 'SP', 'G1', 'PL', 'Q', 'TP']
    data = [[ 'Label', 'News Items']]
    for label in labels:
        news_count = News.objects.filter(ai_id__in = ai_ids, title = label, time__range = (start_date, end_date)).count()
        data.append([label, news_count])

    return data


def get_news_location_distrib_using_tags(ai, tag, start_date, end_date):
    items = News.objects.filter(ai = ai, time__range = (start_date, end_date), tags__contains = tag)
    data = [ ['Date', 'Item id', 'Item State', 'Instance', 'Channel', 'Tag', 'Village', 'District', 'Block', 'State' ] ]

    for item in items:
        row = []
        if item.location:
            row.append(item.location.village)
            row.append(item.location.district.name)
            row.append(item.location.block.name)
            row.append(item.location.state.name)

        data.append([ item.time, item.id, item.state, item.ai.name, item.channel.name, item.tags ] + row )

    return data


def get_item_listening_stats(item, start_date = None, end_date = None):
    stats = Item_heard_stats.objects.filter(item_id = item.id)
    if start_date and end_date:
        stats = stats.filter(when_heard__range = (start_date, end_date))
    item_heard_count = stats.count()
    total_duration_heard = stats.aggregate(Sum('duration')).get('duration__sum', 0)
    return item_heard_count, total_duration_heard


def get_items_heard_stats(items, start_date = None, end_date = None, min_duration = None):
    item_ids = [item.id for item in items]
    stats = Item_heard_stats.objects.filter(item_id__in = item_ids)
    if start_date and end_date:
        stats = stats.filter(when_heard__range = (start_date, end_date))
    if min_duration:
        stats = stats.filter(duration__gte = min_duration)

    return stats

def get_items_heard_count(items, start_date = None, end_date = None, min_duration = None):
    return get_items_heard_stats(items, start_date, end_date, min_duration).count()


def get_items_listeners_count(items, start_date = None, end_date = None):
    stats = get_items_heard_stats(items, start_date, end_date)
    return len(stats.values_list('cdr__callerid', flat = True).distinct())


def get_avg_listening_duration(items, start_date = None, end_date = None):
    item_ids = [item.id for item in items]
    stats = Item_heard_stats.objects.filter(item_id__in = item_ids)
    if start_date and end_date:
        stats = stats.filter(when_heard__range = (start_date, end_date))

    total_listening_dur = stats.aggregate(Sum('duration')).get('duration__sum', 0)
    duration_count = stats.count()
    if duration_count:
        return get_ratio(total_listening_dur, duration_count)


def get_contrib_dur_distrib(ai_ids, start_date = None, end_date = None):
    data = [ ['News ID', 'News Duration'] ]
    excluded_callerid = '91' + DEFAULT_CALLERID
    news = News.objects.filter(ai_id__in = ai_ids, state__in = [News_state.PUB, News_state.ARC]).exclude(callerid = excluded_callerid)
    if start_date and end_date:
        news = news.filter(time__range = (start_date, end_date))
    else:
        news = news.filter(time__lte = datetime.now())

    for n in news:
        data.append( [n.id, n.get_news_duration()] )

    return data


def get_moderation_stats(ai_id, start_date = None, end_date = None):
    stats = {}
    excluded_callerid = '91' + DEFAULT_CALLERID
    news = News.objects.filter(ai_id = ai_id).exclude(callerid = excluded_callerid)
    if start_date and end_date:
        news = news.filter(time__range = (start_date, end_date))
    total_news = news.count()
    rej_news = news.filter(state = News_state.REJ).count()
    pub_news = news.filter(state = News_state.PUB).count()
    archived_news = news.filter(state = News_state.ARC).count()
    transc_news = news.filter(state__in = [News_state.PUB, News_state.ARC]).exclude(transcript__exact = '').count()
    stats['rejected'] = (rej_news/float(total_news)) * 100
    stats['published'] = (pub_news/float(total_news)) * 100
    stats['archived'] = (archived_news/float(total_news)) * 100
    stats['transcribed'] = (transc_news/float(total_news)) * 100
    return stats

def get_contribs(club, vol, start_date, end_date):
    return News.get_pub_arc_news().filter(ai = club.local_mnews_ai, callerid = vol.contact.number, time__range = (start_date, end_date))

def get_good_contribs_count(club, vol, start_date, end_date, contribs = None):
    if contribs is None:
        contribs = get_contribs(club, vol, start_date, end_date)
    good_contrib_count = 0
    for contrib in contribs:
        if is_good_contrib(contrib):
            good_contrib_count += 1
    return good_contrib_count

def is_good_contrib(item):
    return having_min_like_comments(item) or having_min_listeners(item)

def having_min_like_comments(item):
    return having_min_likes(item) or having_min_comments(item)

def having_min_likes(item):
    likes_count = Bookmark.objects.all().filter(news = item.id).count()
    return likes_count >= MIN_LIKE

def having_min_comments(item):
    if item.is_comment:
        return False
    else:
        return item.num_comments() >= MIN_COMMENT

def having_min_listeners(item):
    min_listening_dur_decim = Decimal(str(MIN_LISTENING_DURATION))
    item_listener_count = Item_heard_stats.objects.all().filter(item = item, duration__gte = min_listening_dur_decim).count()
    return item_listener_count >= MIN_LISTENER

def get_top_listner_tag_ai(tag, ai_id, recipient_email, start_date = None, end_date = None):
    if start_date and end_date:
        start_datetime = datetime.strptime( start_date, '%Y-%m-%d' )
        end_datetime = datetime.strptime( end_date, '%Y-%m-%d' )
        item_ids = News.objects.filter(ai_id = ai_id, tags__icontains = tag, time__gte = start_datetime, time__lt = end_datetime).values_list('id', flat = True)
    else:
        item_ids = News.objects.filter(ai_id = ai_id, tags__icontains = tag).values_list('id', flat = True)
    subject = 'Top listner under tag ' + str(tag) 
    body = 'Please find attached report for top listner under ' + str(tag)
    data = [['Item id', 'Total listner Count', 'Duration']]
    attachment = '/tmp/' +str(tag)+'listner_data'+str(ai_id)+'.xlsx'

    print len(item_ids)
    
    for item_id in item_ids:
        heard_data = []
        heard_stats = Item_heard_stats.objects.filter(item_id = item_id).values_list('duration', flat = True)        
        heard_data.append(item_id)
        heard_data.append(heard_stats.count())
        heard_data.append(sum(heard_stats))
        data.append(heard_data)

    data.sort(key=lambda x: x[1], reverse = True)   
    generate_attachment(data, attachment)
    send_email(subject, body, [recipient_email], attachment)


def populate_session_data(data, cdr):
    items_heard_info = data.get('items_heard_info', [])
    position = 0
    event = None
    for item_heard_info in items_heard_info:
        item = News.objects.get(id = item_heard_info['item'].id)
        ai = item.ai
        item_heard_stats_qs = Item_heard_stats.objects.filter(cdr=cdr, item=item, ai=ai)
        if item_heard_stats_qs.exists():
            position = item_heard_stats_qs[0].position
        else:
            if event in ['dtmf_*', 'dtmf_0', 'dtmf_9']:
                position = 1
            else:
                position += 1
        when_heard = item_heard_info['start']
        duration = get_total_seconds(item_heard_info['end'] - when_heard)
        event = item_heard_info['event']
        Item_heard_stats(cdr=cdr, item=item, ai=ai, when_heard=when_heard, \
                        duration=duration, position=position, event=event).save()
                        

def get_item_bookmark_forward_count(news, start, end):
    bookmark_ids =  Bookmark.objects.filter(time__gte = start, time__lt = end, news = news)
    bookmark_count = Bookmark_log.objects.filter(bookmark_id__in = bookmark_ids, source='ITB').count()
    forwarding_count = Forwarding_log.objects.filter(forwarded_item = news, forwarding_time__gte = start, forwarding_time__lt = end).count()
    return bookmark_count, forwarding_count
