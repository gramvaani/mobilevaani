from app_manager.models import Data_sync_base
from mnews.models import Channel, News, Playlist_channel_map, Playlist, \
Playlist_sync_log, Playlist_item, News_state
from vapp.social.utils import *
from vapp.social.models import SocialMedia
from vapp.utils import ffmpeg_modify_mp3, create_dirs_path
from django.conf import settings
import os
from django.core.urlresolvers import reverse

from vapp.log import get_request_logger
logger = get_request_logger() 

class Soundcloud_sync( Data_sync_base ):

    def sync(self, data_sync_obj, uploaded_files_dict = {}, **kwargs ):
        if 'channel_id' in kwargs:
            user = kwargs.get( 'user', None )
            channel = Channel.objects.get( id = kwargs[ 'channel_id' ] )
            creds_map = SocialMedia.get_sm_creds( channel.ai )
            if not creds_map.has_key('soundcloud'):
                return False

            title = None
            sc_creds = creds_map[ 'soundcloud' ]

            if 'title' in kwargs and kwargs['title'] != channel.name:
                title = kwargs['title']
                logger.debug('title: %s' %(title))

            channel_news = list( News.objects.filter( channel = channel, state = 'PUB', is_comment = False ) )

            if not Playlist_channel_map.objects.filter( channel = channel, playlist__data_sync = data_sync_obj ).exists():
                playlist_title = title if title else unicode(channel.name)
                playlist = Playlist( title = playlist_title, sharing = 'PUB', data_sync = data_sync_obj )
                playlist.save()
                if uploaded_files_dict.has_key('playlist_image'):
                    Playlist.add_image( playlist, uploaded_files_dict['playlist_image'] )
                pc_map = Playlist_channel_map( playlist = playlist, channel = channel )
                pc_map.save()
            else:
                pc_map = Playlist_channel_map.objects.filter( channel = channel, playlist__data_sync = data_sync_obj )[0]
                playlist = pc_map.playlist
                playlist.save()

            sync_log = Playlist_sync_log( pc_map = pc_map, syncd_by = user )
            sync_log.save()

            Playlist_item.clear_playlist_items( playlist )

            tracks = []
            news_url_map = {}

            for news in channel_news:
                track = publish_track_to_soundcloud(sc_creds, news.title, news.detail.get_full_filename() )
                if track:
                    sync_log.items_syncd.add( news )
                    sync_log.save()
                    news_url_map[ news ] = track.stream_url
                    tracks.append( track )

            sc_playlist = publish_playlist_to_soundcloud( sc_creds, playlist.title, playlist_id = playlist.playlist_id, tracks_list = tracks )
            if sc_playlist:
                Playlist.update_playlist( playlist, sc_playlist )
            Playlist_item.add_playlist_items( playlist, news_url_map )
            sync_log.sync_end_time = datetime.now()
            sync_log.save()
            return True
        else:
            return False

class Download_playlist_sync(Data_sync_base):

    def generate_output_mp3_details(self, playlist_id, input_mp3):
        path = None
        if input_mp3:
            name = os.path.basename(input_mp3)
            path = ('%s%s/%s') %(settings.PLAYLIST_ROOT, playlist_id, name)
            create_dirs_path(path)
        return path

    def get_item_sync_info(self, playlist_id, channel_id):
        channel_news = News.objects.filter(channel_id=channel_id,
                                           state=News_state.PUB,
                                           is_comment=False)
        synced_news = channel_news.filter(playlist_item__playlist_id=\
                            playlist_id, playlist_item__is_current=\
                            True).values_list('id', flat=True)
        items_to_sync = channel_news.exclude(id__in=synced_news)
        return {'synced_news': synced_news, 'items_to_sync': items_to_sync}

    def sync(self, data_sync_obj, playlist_id, **kwargs):
        pc_sync_info = []
        pl_synced_items = []

        for pc_map in Playlist_channel_map.objects.filter(playlist_id=playlist_id):
            sync_log = Playlist_sync_log(pc_map_id=pc_map.id)
            sync_log.save()

            sync_info = self.get_item_sync_info(playlist_id, pc_map.channel.id)
            pc_sync_info.append({
                'sync_log': sync_log,
                'sync_info': sync_info
            })
            pl_synced_items.extend(sync_info['synced_news'])

        Playlist_item.clear_playlist_items(playlist_id, pl_synced_items)

        for each in pc_sync_info:
            news_loc_map = {}

            for news in each['sync_info']['items_to_sync']:
                mp3_file = news.detail.get_full_filename()
                output_path = self.generate_output_mp3_details(playlist_id, mp3_file)
                output_file = ffmpeg_modify_mp3(mp3_file, output_mp3=output_path)

                if output_file and os.path.exists(output_file):
                    each['sync_log'].items_syncd.add(news)
                    each['sync_log'].save()
                    news_loc_map[news] = output_file

            Playlist_item.add_playlist_items(playlist_id, news_loc_map)
            each['sync_log'].sync_end_time = datetime.now()
            each['sync_log'].success = True
            each['sync_log'].save()
