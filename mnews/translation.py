from microsofttranslator import Translator
from field_mis.models import Volunteer

def translate_title_for_auto_pub_items( cred, item, *args, **kwargs ):
    translator = Translator( cred.client_id, cred.client_secret_key )
    volunteers = Volunteer.objects.filter( contact__number = item.callerid )
    if volunteers.exists():
        translation_success = True
        volunteer = volunteers[ 0 ]
        contact_name = volunteer.contact.name.strip( " " )
        names = contact_name.split( " " )
        while "" in names:
            names.remove( "" )

        if names:
            translated_names = []
            for each in names:
                translated_word = translator.translate( each, "hi" )
                if translated_word == each:
                    translation_success = False
                    break
                translated_names.append( translated_word )

            if translation_success:
                contact_name = " ".join( translated_names )
            else:
                contact_name = translator.translate( "Reporter", "hi" )
        else:
            contact_name = translator.translate( "Reporter", "hi" )

        news_str = translator.translate( "News", "hi" )
        time = translator.translate( item.time.strftime( '%H:%M, %d %B\'%y' ), "hi" )
        title = "%s, %s, %s" % ( contact_name, news_str, time )
        return title
