from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.utils import *

from mnews.models import *
from mnews.tasks import *
from mnews.views import get_reject_reasons

from app_manager.common import get_queue_for_task
from app_manager.models import Cdr_trigger, VI_reference, App_instance_settings, VI_conf

from datetime import datetime, timedelta
import time
from vapp.log import get_logger
logger = get_logger()


class OnPubCallController(BaseVappController):

    def __init__(self, ai, sessionData, vi_data ):

        super(OnPubCallController, self).__init__( ai, sessionData )

        self.item = News.objects.get(id = vi_data['news_id'])
        self.event_ai_properties = Event_ai_properties.objects.get(id = vi_data['event_properties_id'])
        self.reason_id = vi_data.get('reason_id', None)
        self.play_item_preview = None
        self.reject_reason = None

        if self.reason_id:
            try:
                rejection_reasons = get_reject_reasons(self.item)
                ml_reject = self.reason_id
                if rejection_reasons:
                    rej_reason = rejection_reasons.filter(is_ml_tag = True)
                    if ml_reject == -1 and rej_reason.exists():
                        self.reject_reason = rej_reason[0]
                    elif self.reason_id != -1:
                        self.reject_reason = rejection_reasons.get(id= self.reason_id)
                    self.play_item_preview = self.reject_reason.play_item_preview
            except:
                logger.exception("No Reason object with this " + str(self.reason_id) + "id")
        logger.info("Into __init__ of OnPubCallController")

        self.call_log = vi_data['group_call_log']
        self.call_log.tries += 1
        self.call_log.save()
        self.max_tries = vi_data['max_tries']


    def pre_outgoingstart(self):
        call_params_dict = self.getStartCallParams( self.sessionData.callerid, trigger = Cdr_trigger.CALLANDPLAY )
        self.call_log.last_cdr = Cdr.objects.get(pk = call_params_dict['cdr_id'])
        self.call_log.save()
        return call_params_dict


    def while_outgoingstart__sm_action_failure(self, events, eventData):
        if self.call_log.tries < self.max_tries:
            time = datetime.now() + timedelta(minutes = 30)
            queue = get_queue_for_task(self.ai, 'obd', default_queue='push')
            kwargs = {'vi_data': RestrictedDict.convert_dict_to_rdict(self.vi_data, filter=True)}
            CallAndPlayTask.apply_async(args=[self.call_log.id, self.max_tries], 
                                        kwargs=kwargs, queue=queue, eta=time)
        return 'sm_action_failure'

    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.call_log.success = True
        self.call_log.last_cdr = self.sessionData.cdrs[0]
        self.call_log.save()

    def pre_welcome(self, prevState):
        return self.getPlaybackParams( self.event_ai_properties.get_start_prompt_filename() )

    def while_checkpreviewtime__sm_action_success(self, events, eventData):
        if self.reason_id:
            if self.play_item_preview:
                if int(self.event_ai_properties.item_preview_secs) > 0:
                    return 'mnews_rej_preview_time_set'
                else:
                    return 'mnews_rej_preview_time_not_set'
            else:
                return 'mnews_rej_no_preview'
        else:
            if int(self.event_ai_properties.item_preview_secs) > 0:
                return 'mnews_pub_preview_time_set'
            else:
                return 'mnews_pub_preview_time_not_set'

    def pre_playlimiteditem(self, prevState):
        return self.getPlaybackParams( self.item.detail.get_full_filename() )

    def pre_playfullitem(self, prevState):
        return self.getPlaybackParams( self.item.detail.get_full_filename() )
    
    def pre_playlimiteditem_rejection(self, prevState):
        return self.getPlaybackParams( self.item.detail.get_full_filename() )

    def pre_playfullitem_rejection(self, prevState):
        return self.getPlaybackParams( self.item.detail.get_full_filename() )

    def pre_fullitem_rejectionreason(self, prevState):
        return self.getPlaybackParams( self.reject_reason.get_rejectreason_prompt_filename() )

    def pre_limiteditem_rejectionreason(self, prevState):
        return self.getPlaybackParams( self.reject_reason.get_rejectreason_prompt_filename() )

    def pre_timedsleep(self, prevState):
        self.preview_duration = int(self.event_ai_properties.item_preview_secs)
        return self.getTimedSleepParams(self.preview_duration)

    def pre_rej_timedsleep(self, prevState):
        self.preview_duration = int(self.event_ai_properties.item_preview_secs)
        return self.getTimedSleepParams(self.preview_duration)

    def pre_playendprompt(self, prevState):
        return self.getPlaybackParams( self.event_ai_properties.get_end_prompt_filename() )


OnPubCallStateMap = [

    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url']
            }
        },
    {   'name': 'welcome',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checkpreviewtime': ['sm_action_success'],
            },
        },
    {   'name': 'checkpreviewtime',
        'action': 'none',
        'transitions': {
            'playlimiteditem': ['mnews_pub_preview_time_set'],
            'playfullitem': ['mnews_pub_preview_time_not_set'],
            'playlimiteditem_rejection': ['mnews_rej_preview_time_set'],
            'playfullitem_rejection': ['mnews_rej_preview_time_not_set'],
            'rejection_no_preview': ['mnews_rej_no_preview'],
            'stop': ['sm_action_failure'],
            },
        },
    {   'name': 'playfullitem',
        'action': 'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'playendprompt':['sm_action_success'],
            },
        },
    {   'name': 'playlimiteditem',
        'action': 'play_and_return',
        'transitions': {
            'stop': ['sm_action_failure'],
            'timedsleep': ['sm_action_success'],
            },
        },
     {   'name': 'playfullitem_rejection',
         'action': 'playback',
         'transitions': {
             'stop':['sm_action_failure'],
             'fullitem_rejectionreason':['sm_action_success'],
             },
        },
    {   'name'    : 'fullitem_rejectionreason',
        'action': 'playback',
        'transitions': {
             'stop':['sm_action_failure'],
             'playendprompt':['sm_action_success'],
             },
        },
     {   'name': 'playlimiteditem_rejection',
         'action': 'play_and_return',
         'transitions': {
             'stop': ['sm_action_failure'],
             'rej_timedsleep': ['sm_action_success'],
             },
        },
    {   'name': 'limiteditem_rejectionreason',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'playendprompt': ['sm_action_success'],
            },
        },
     {   'name': 'rejection_no_preview',
         'action': 'none',
         'transitions': {
             'stop':['sm_action_failure'],
             'limiteditem_rejectionreason': ['sm_action_success'],
             },
        },                   
    {   'name':'timedsleep',
        'action':'timed_sleep',
        'transitions': {
            'stop':['sm_action_failure'],
            'timedbreak':['sm_action_success'],
            },
    },

    {   'name':'timedbreak',
        'action':'break_media',
        'transitions': {
            'stop':['sm_action_failure'],
            'playendprompt':['sm_action_success'],
            },
        },
    
    {   'name':'rej_timedsleep',
        'action':'timed_sleep',
        'transitions': {
            'stop':['sm_action_failure'],
            'limiteditem_rejectionreason':['sm_action_success'],
            },
    },

    {   'name': 'playendprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure', 'sm_action_success'],
            },
        },

    {   'name':'stop',
        'action':'hangup',
        'transitions': {}
        },
    ]



class OnCommentPubCallContribController(BaseVappController):

    @classmethod
    def checkViRefEntry(self,vi_conf_id, ai, reference):
        try:
            return VI_reference.objects.get(vi= VI_conf.objects.get(pk = vi_conf_id), ai = ai, reference=reference)
        except Exception as e:
            logger.info(e)
            return None

    def __init__(self, ai, sessionData, vi_data ):

        super(OnCommentPubCallContribController, self).__init__( ai, sessionData )

        self.comment = News.objects.get(id = vi_data['news_id'])
        self.parent = News.objects.get(id = vi_data['parent_id'])
        self.event_ai_properties = Event_ai_properties.objects.get(id = vi_data['event_properties_id'])
        self.call_log = vi_data['group_call_log']
        self.call_log.tries += 1
        self.call_log.save()
        self.max_tries = vi_data['max_tries']
	self.tries = 0
        self.maxlooptries = 1
        self.vi_conf_id =  vi_data['vi_conf_id']


    def pre_outgoingstart(self):
        call_params_dict = self.getStartCallParams( self.sessionData.callerid, trigger = Cdr_trigger.CALLANDPLAY )
        self.call_log.last_cdr = Cdr.objects.get(pk = call_params_dict['cdr_id'])
        self.call_log.save()
        return call_params_dict

    def while_outgoingstart__sm_action_failure(self, events, eventData):
        if self.call_log.tries < self.max_tries:
            time = datetime.now() + timedelta(minutes = 30)
            queue = get_queue_for_task(self.ai, 'obd', default_queue='push')
            kwargs = {'vi_data': RestrictedDict.convert_dict_to_rdict(self.vi_data, filter=True)}
            CallAndPlayTask.apply_async(args=[self.call_log.id, self.max_tries], 
                                        kwargs=kwargs, queue=queue, eta=time)
        return 'sm_action_failure'

    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.call_log.success = True
        self.call_log.last_cdr = self.sessionData.cdrs[0]
        self.call_log.save()

    def pre_welcome(self, prevState):
        return self.getPlaybackParams( self.event_ai_properties.get_start_prompt_filename() )

    def while_checkpreviewtime__sm_action_success(self, events, eventData):
        if int(self.event_ai_properties.item_preview_secs) > 0:
            return 'mnews_pub_preview_time_set'
        else:
            return 'mnews_pub_preview_time_not_set'

    def pre_playfullparent(self, prevState):
        return self.getPlaybackParams( self.parent.detail.get_full_filename() )

    def pre_playlimitedparent(self, prevState):
        return self.getPlaybackParams( self.parent.detail.get_full_filename() )

    def pre_timedsleep(self, prevState):
        self.preview_duration = int(self.event_ai_properties.item_preview_secs)
        return self.getTimedSleepParams(self.preview_duration)

    def pre_playendprompt(self, prevState):
        return self.getPlaybackParams( self.event_ai_properties.get_end_prompt_filename() )

    def pre_playcomment(self, prevState):
        return self.getPlaybackParams( self.comment.detail.get_full_filename() )

    def while_checkrepeatcomment__sm_action_success(self, event, eventData):
        self.tries += 1
        if self.tries <= self.maxlooptries:
            return 'in_tries_repeat_comment'
        else:
            return 'tries_complete_jumpai'

    def pre_repeatcommentprompt(self, prevState):
        return self.getPromptPlayAndGetParams(promptName='mnews_pub_repeat_comment', maxDigits=9, tries=2)

    def while_repeatcommentprompt__sm_get_digits_no_digits(self, event, eventData):
        return 'no_input_jumpai'

    def while_jumptoai__sm_action_success(self, event, eventData):
        viref = self.checkViRefEntry(self.vi_conf_id, self.ai, 'call_on_comment_nextai')
        if viref:
            return '@ai_'+str(viref.target_ai.id)+'_welcome'
        elif App_instance.objects.filter(id = self.ai.id, app_id = 30).exists():
            return 'cm_instance_stop'
        else:
            return "@ai_{0}_welcome".format(self.ai.id)

OnCommentPubCallContribStateMap = [

    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url']
            }
        },
    {   'name': 'welcome',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checkpreviewtime': ['sm_action_success'],
            },
        },
    {   'name': 'checkpreviewtime',
        'action': 'none',
        'transitions': {
            'playlimitedparent': ['mnews_pub_preview_time_set'],
            'playfullparent': ['mnews_pub_preview_time_not_set'],
            'stop': ['sm_action_failure'],
            },
        },
    {   'name': 'playfullparent',
        'action': 'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'playendprompt':['sm_action_success'],
            },
        },
    {   'name': 'playlimitedparent',
        'action': 'play_and_return',
        'transitions': {
            'stop': ['sm_action_failure'],
            'timedsleep': ['sm_action_success'],
            },
        },
    {   'name':'timedsleep',
        'action':'timed_sleep',
        'transitions': {
            'stop':['sm_action_failure'],
            'timedbreak':['sm_action_success'],
            },
        },
    {   'name':'timedbreak',
        'action':'break_media',
        'transitions': {
            #'stop':['sm_action_failure'],
            'playendprompt':['sm_action_success', 'sm_action_failure'],
            },
        },
    {   'name': 'playendprompt',
        'action': 'playback',
        'transitions': {
            #'stop': ['sm_action_failure'],
            'playcomment': ['sm_action_success', 'sm_action_failure'],
            },
        },
    {   'name': 'playcomment',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'jumptoai': ['jump_to_ai'],
            'checkrepeatcomment': ['sm_action_success'],
            },
        'dtmf': {
            1: 'jump_to_ai',
            },
        },
    {   'name':'checkrepeatcomment',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'repeatcommentprompt':['in_tries_repeat_comment'],
            'jumptoai': ['tries_complete_jumpai']
        },
    },
    {   'name':'repeatcommentprompt',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'playcomment':['confirm'],
            'jumptoai': ['no_input_jumpai', 'jump_to_ai']
        },
        'dtmf': {
            1:'confirm',
            'any':'jump_to_ai',
        },
    },
    {   'name': 'jumptoai',
        'action': 'none',
        'transitions': {
            'stop': ['sm_action_failure', 'cm_instance_stop'],
            },
        },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {}
        },
    ]
