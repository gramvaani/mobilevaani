from django.db.models import Q

DEFAULT_ITEMS_LIMIT = 30

def ai_published_items( ai, feed_ais, limit = DEFAULT_ITEMS_LIMIT ):
    
    from models import News    
    return News.objects.filter( ai = ai, state__in=[ 'PUB','ARC' ], is_comment = False ).order_by( '-time' )[ :limit ]

def oxfam_items( ai, feed_ais, limit = DEFAULT_ITEMS_LIMIT ):
    
    #Loaded when the function is called, otherwise circular dependency occurs.
    from models import News
    
    
    jmr_ai = ai    
    return News.objects.filter( Q( ai__in=feed_ais ) | Q( ai=jmr_ai, tags__contains='Oxfam' ), \
                              state__in=[ 'PUB','ARC' ], is_comment = False ).order_by( '-time' )[ :limit ]


def pfi_items( ai, feed_ais, limit = DEFAULT_ITEMS_LIMIT ): 
    from models import News, News_state
    tags = ['RSS', 'PFI']
    

    feed_items = News.objects.filter(ai__in = feed_ais, state__in = [ News_state.PUB, News_state.ARC ])
    for tag in tags:
        feed_items = feed_items.filter(tags__contains = tag)

    return feed_items.order_by('-time')[ :limit ]

def pfi_items_s2( ai, feed_ais, limit = DEFAULT_ITEMS_LIMIT ): 
    from models import News, News_state
    feed_items = News.objects.filter(ai__in = feed_ais, state__in = [ News_state.PUB, News_state.ARC ])
    tags = ['QG']
    for tag in tags:
        feed_items = feed_items.filter(tags__contains = tag)

    return feed_items.order_by('-time')[ :limit ]


def igf_items( ai, feed_ais, limit = DEFAULT_ITEMS_LIMIT ):
    from models import News, News_state
    tags = ['IGF']
    feed_items = News.objects.filter(state__in = [ News_state.PUB, News_state.ARC ])

    for tag in tags:
        feed_items = feed_items.filter(tags__contains = tag)
    
    return feed_items.order_by('-time') [ :limit ]
    
