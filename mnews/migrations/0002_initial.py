# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'mnews_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='Parent', null=True, to=orm['mnews.Category'])),
            ('is_subcategory', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'mnews', ['Category'])

        # Adding M2M table for field subcategories on 'Category'
        m2m_table_name = db.shorten_name(u'mnews_category_subcategories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_category', models.ForeignKey(orm[u'mnews.category'], null=False)),
            ('to_category', models.ForeignKey(orm[u'mnews.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_category_id', 'to_category_id'])

        # Adding M2M table for field ai on 'Category'
        m2m_table_name = db.shorten_name(u'mnews_category_ai')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('category', models.ForeignKey(orm[u'mnews.category'], null=False)),
            ('app_instance', models.ForeignKey(orm[u'app_manager.app_instance'], null=False))
        ))
        db.create_unique(m2m_table_name, ['category_id', 'app_instance_id'])

        # Adding model 'Format'
        db.create_table(u'mnews_format', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'mnews', ['Format'])

        # Adding model 'Qualifier'
        db.create_table(u'mnews_qualifier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'mnews', ['Qualifier'])

        # Adding model 'News'
        db.create_table(u'mnews_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('channel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Channel'])),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('callerid', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('detail', self.gf('django.db.models.fields.related.ForeignKey')(related_name='detail', null=True, to=orm['media.Recording'])),
            ('summary', self.gf('django.db.models.fields.related.ForeignKey')(related_name='summary', null=True, to=orm['media.Recording'])),
            ('rating', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('is_comment', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_src_caller', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Location'], null=True, blank=True)),
            ('tags', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('transcript', self.gf('django.db.models.fields.TextField')()),
            ('pub_order', self.gf('django.db.models.fields.SmallIntegerField')(default=-1)),
            ('modified_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Category'], null=True)),
            ('format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Format'], null=True)),
            ('qualifier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Qualifier'], null=True)),
            ('location_text', self.gf('django.db.models.fields.CharField')(max_length=64, null=True)),
        ))
        db.send_create_signal(u'mnews', ['News'])

        # Adding M2M table for field comments on 'News'
        m2m_table_name = db.shorten_name(u'mnews_news_comments')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_news', models.ForeignKey(orm[u'mnews.news'], null=False)),
            ('to_news', models.ForeignKey(orm[u'mnews.news'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_news_id', 'to_news_id'])

        # Adding model 'Channel'
        db.create_table(u'mnews_channel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'mnews', ['Channel'])

        # Adding model 'Channel_order'
        db.create_table(u'mnews_channel_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('initialAi', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True, blank=True)),
            ('cur_channel', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cur_channel_set', to=orm['mnews.Channel'])),
            ('next_channel', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='next_channel_set', null=True, to=orm['mnews.Channel'])),
        ))
        db.send_create_signal(u'mnews', ['Channel_order'])

        # Adding model 'Vi_transition_data'
        db.create_table(u'mnews_vi_transition_data', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transition_event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Transition_event'])),
            ('channel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Channel'])),
        ))
        db.send_create_signal(u'mnews', ['Vi_transition_data'])

        # Adding model 'Event_sms_template'
        db.create_table(u'mnews_event_sms_template', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_template'])),
        ))
        db.send_create_signal(u'mnews', ['Event_sms_template'])

        # Adding model 'Bookmark'
        db.create_table(u'mnews_bookmark', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('bookmark_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('news', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.News'])),
            ('callerid', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'mnews', ['Bookmark'])

        # Adding model 'Bookmark_generation_error'
        db.create_table(u'mnews_bookmark_generation_error', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('news', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.News'])),
            ('callerid', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('last_id_generated', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'mnews', ['Bookmark_generation_error'])

        # Adding model 'Bookmark_log'
        db.create_table(u'mnews_bookmark_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bookmark', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Bookmark'])),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('accessed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('bookmarked', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'mnews', ['Bookmark_log'])

        # Adding model 'News_sms_log'
        db.create_table(u'mnews_news_sms_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('news', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.News'])),
            ('sms', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_message'])),
        ))
        db.send_create_signal(u'mnews', ['News_sms_log'])

        # Adding model 'Transient_group'
        db.create_table(u'mnews_transient_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True)),
            ('generating_code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'mnews', ['Transient_group'])

        # Adding model 'Ai_transient_group'
        db.create_table(u'mnews_ai_transient_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Transient_group'])),
        ))
        db.send_create_signal(u'mnews', ['Ai_transient_group'])

        # Adding model 'Days'
        db.create_table(u'mnews_days', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'mnews', ['Days'])

        # Adding model 'Transient_group_schedule'
        db.create_table(u'mnews_transient_group_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ai_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Ai_transient_group'])),
            ('prompt_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('play_ai', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('start_time', self.gf('django.db.models.fields.TimeField')()),
            ('end_time', self.gf('django.db.models.fields.TimeField')()),
            ('repeat', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('onpush_sms_message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_template'], null=True, blank=True)),
        ))
        db.send_create_signal(u'mnews', ['Transient_group_schedule'])

        # Adding M2M table for field day_of_week on 'Transient_group_schedule'
        m2m_table_name = db.shorten_name(u'mnews_transient_group_schedule_day_of_week')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transient_group_schedule', models.ForeignKey(orm[u'mnews.transient_group_schedule'], null=False)),
            ('days', models.ForeignKey(orm[u'mnews.days'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transient_group_schedule_id', 'days_id'])

        # Adding model 'Groups_call_log'
        db.create_table(u'mnews_groups_call_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('group_schedule', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.Transient_group_schedule'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('last_cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'], null=True)),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tries', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'mnews', ['Groups_call_log'])

        # Adding model 'ModerationEventRecorder'
        db.create_table(u'mnews_moderationeventrecorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.News'], null=True)),
            ('event_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('fields_changed', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
        ))
        db.send_create_signal(u'mnews', ['ModerationEventRecorder'])

        # Adding model 'Call_stats'
        db.create_table(u'mnews_call_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('line', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('calls', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('callers', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('avg_dur', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'mnews', ['Call_stats'])

        # Adding model 'Item_stats'
        db.create_table(u'mnews_item_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('new_items', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('pub_items', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('repub_items', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('new_comments', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('pub_comments', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('repub_comments', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('items_heard_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('items_skipped_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('comments_heard_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('comments_skipped_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'mnews', ['Item_stats'])

        # Adding model 'Hourly_call_stats'
        db.create_table(u'mnews_hourly_call_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('calls', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('callers', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('avg_dur', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'mnews', ['Hourly_call_stats'])

        # Adding model 'Item_heard_stats'
        db.create_table(u'mnews_item_heard_stats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cdr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.Cdr'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mnews.News'])),
            ('when_heard', self.gf('django.db.models.fields.DateTimeField')()),
            ('duration', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'mnews', ['Item_heard_stats'])

        # Adding model 'OtherEventsRecorder'
        db.create_table(u'mnews_othereventsrecorder', (
            (u'moderationeventrecorder_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['mnews.ModerationEventRecorder'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'mnews', ['OtherEventsRecorder'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table(u'mnews_category')

        # Removing M2M table for field subcategories on 'Category'
        db.delete_table(db.shorten_name(u'mnews_category_subcategories'))

        # Removing M2M table for field ai on 'Category'
        db.delete_table(db.shorten_name(u'mnews_category_ai'))

        # Deleting model 'Format'
        db.delete_table(u'mnews_format')

        # Deleting model 'Qualifier'
        db.delete_table(u'mnews_qualifier')

        # Deleting model 'News'
        db.delete_table(u'mnews_news')

        # Removing M2M table for field comments on 'News'
        db.delete_table(db.shorten_name(u'mnews_news_comments'))

        # Deleting model 'Channel'
        db.delete_table(u'mnews_channel')

        # Deleting model 'Channel_order'
        db.delete_table(u'mnews_channel_order')

        # Deleting model 'Vi_transition_data'
        db.delete_table(u'mnews_vi_transition_data')

        # Deleting model 'Event_sms_template'
        db.delete_table(u'mnews_event_sms_template')

        # Deleting model 'Bookmark'
        db.delete_table(u'mnews_bookmark')

        # Deleting model 'Bookmark_generation_error'
        db.delete_table(u'mnews_bookmark_generation_error')

        # Deleting model 'Bookmark_log'
        db.delete_table(u'mnews_bookmark_log')

        # Deleting model 'News_sms_log'
        db.delete_table(u'mnews_news_sms_log')

        # Deleting model 'Transient_group'
        db.delete_table(u'mnews_transient_group')

        # Deleting model 'Ai_transient_group'
        db.delete_table(u'mnews_ai_transient_group')

        # Deleting model 'Days'
        db.delete_table(u'mnews_days')

        # Deleting model 'Transient_group_schedule'
        db.delete_table(u'mnews_transient_group_schedule')

        # Removing M2M table for field day_of_week on 'Transient_group_schedule'
        db.delete_table(db.shorten_name(u'mnews_transient_group_schedule_day_of_week'))

        # Deleting model 'Groups_call_log'
        db.delete_table(u'mnews_groups_call_log')

        # Deleting model 'ModerationEventRecorder'
        db.delete_table(u'mnews_moderationeventrecorder')

        # Deleting model 'Call_stats'
        db.delete_table(u'mnews_call_stats')

        # Deleting model 'Item_stats'
        db.delete_table(u'mnews_item_stats')

        # Deleting model 'Hourly_call_stats'
        db.delete_table(u'mnews_hourly_call_stats')

        # Deleting model 'Item_heard_stats'
        db.delete_table(u'mnews_item_heard_stats')

        # Deleting model 'OtherEventsRecorder'
        db.delete_table(u'mnews_othereventsrecorder')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'app_manager.cdr': {
            'Meta': {'object_name': 'Cdr'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'answered_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'callerid': ('vapp.app_manager.models.CalleridField', [], {'max_length': '20'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'hangup_cause': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        u'app_manager.transition_event': {
            'Meta': {'object_name': 'Transition_event'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prev_state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'state_counter': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trans_event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'vi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.VI_conf']", 'null': 'True'})
        },
        u'app_manager.vi_conf': {
            'Meta': {'object_name': 'VI_conf'},
            'controller': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'states': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'states'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.State']"})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            'blocks': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'blocks'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.Block']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            'districts': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'districts'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.ai_transient_group': {
            'Meta': {'object_name': 'Ai_transient_group'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mnews.bookmark': {
            'Meta': {'object_name': 'Bookmark'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'bookmark_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'news': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.bookmark_generation_error': {
            'Meta': {'object_name': 'Bookmark_generation_error'},
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_id_generated': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'news': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.bookmark_log': {
            'Meta': {'object_name': 'Bookmark_log'},
            'accessed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bookmark': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Bookmark']"}),
            'bookmarked': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.call_stats': {
            'Meta': {'object_name': 'Call_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'avg_dur': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'callers': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'mnews.category': {
            'Meta': {'object_name': 'Category'},
            'ai': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_subcategory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'Parent'", 'null': 'True', 'to': u"orm['mnews.Category']"}),
            'subcategories': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'subcategory'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['mnews.Category']"})
        },
        u'mnews.channel': {
            'Meta': {'object_name': 'Channel'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.channel_order': {
            'Meta': {'object_name': 'Channel_order'},
            'cur_channel': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cur_channel_set'", 'to': u"orm['mnews.Channel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initialAi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'next_channel': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'next_channel_set'", 'null': 'True', 'to': u"orm['mnews.Channel']"})
        },
        u'mnews.days': {
            'Meta': {'object_name': 'Days'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'mnews.event_sms_template': {
            'Meta': {'object_name': 'Event_sms_template'},
            'event': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'template': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']"})
        },
        u'mnews.format': {
            'Meta': {'object_name': 'Format'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.groups_call_log': {
            'Meta': {'object_name': 'Groups_call_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            'group_schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Transient_group_schedule']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']", 'null': 'True'}),
            'number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'mnews.hourly_call_stats': {
            'Meta': {'object_name': 'Hourly_call_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'avg_dur': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'callers': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'calls': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'mnews.item_heard_stats': {
            'Meta': {'object_name': 'Item_heard_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cdr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Cdr']"}),
            'duration': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']"}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'when_heard': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'mnews.item_stats': {
            'Meta': {'object_name': 'Item_stats'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'comments_heard_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comments_skipped_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'items_heard_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'items_skipped_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'new_comments': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'new_items': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'pub_comments': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'pub_items': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'repub_comments': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'repub_items': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'mnews.moderationeventrecorder': {
            'Meta': {'object_name': 'ModerationEventRecorder'},
            'event_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fields_changed': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']", 'null': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'mnews.news': {
            'Meta': {'object_name': 'News'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Category']", 'null': 'True'}),
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Channel']"}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mnews.News']", 'symmetrical': 'False'}),
            'detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'detail'", 'null': 'True', 'to': u"orm['media.Recording']"}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Format']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_comment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_src_caller': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True', 'blank': 'True'}),
            'location_text': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'pub_order': ('django.db.models.fields.SmallIntegerField', [], {'default': '-1'}),
            'qualifier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Qualifier']", 'null': 'True'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'summary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'summary'", 'null': 'True', 'to': u"orm['media.Recording']"}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'transcript': ('django.db.models.fields.TextField', [], {})
        },
        u'mnews.news_sms_log': {
            'Meta': {'object_name': 'News_sms_log'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'news': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']"}),
            'sms': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_message']"})
        },
        u'mnews.othereventsrecorder': {
            'Meta': {'object_name': 'OtherEventsRecorder', '_ormbases': [u'mnews.ModerationEventRecorder']},
            u'moderationeventrecorder_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['mnews.ModerationEventRecorder']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'mnews.qualifier': {
            'Meta': {'object_name': 'Qualifier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.transient_group': {
            'Meta': {'object_name': 'Transient_group'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'generating_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mnews.transient_group_schedule': {
            'Meta': {'object_name': 'Transient_group_schedule'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ai_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Ai_transient_group']"}),
            'day_of_week': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mnews.Days']", 'symmetrical': 'False'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'onpush_sms_message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_template']", 'null': 'True', 'blank': 'True'}),
            'play_ai': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prompt_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'repeat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'mnews.vi_transition_data': {
            'Meta': {'object_name': 'Vi_transition_data'},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Channel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'transition_event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Transition_event']"})
        },
        u'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['mnews']