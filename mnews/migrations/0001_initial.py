# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'News'
        db.create_table('mnews_news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('callerid', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('detail', self.gf('django.db.models.fields.related.ForeignKey')(related_name='detail', null=True, to=orm['media.Recording'])),
            ('summary', self.gf('django.db.models.fields.related.ForeignKey')(related_name='summary', null=True, to=orm['media.Recording'])),
            ('rating', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('is_comment', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_src_caller', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('tags', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('transcript', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('mnews', ['News'])

        # Adding M2M table for field comments on 'News'
        db.create_table('mnews_news_comments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_news', models.ForeignKey(orm['mnews.news'], null=False)),
            ('to_news', models.ForeignKey(orm['mnews.news'], null=False))
        ))
        db.create_unique('mnews_news_comments', ['from_news_id', 'to_news_id'])


    def backwards(self, orm):
        
        # Deleting model 'News'
        db.delete_table('mnews_news')

        # Removing M2M table for field comments on 'News'
        db.delete_table('mnews_news_comments')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'mnews.news': {
            'Meta': {'object_name': 'News'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'callerid': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['mnews.News']", 'symmetrical': 'False'}),
            'detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'detail'", 'null': 'True', 'to': "orm['media.Recording']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_comment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_src_caller': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'summary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'summary'", 'null': 'True', 'to': "orm['media.Recording']"}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'transcript': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['mnews']
