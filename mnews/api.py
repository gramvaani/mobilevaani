import json

from django.conf.urls import url
from django.db.models import Q

from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication, MultiAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from tastypie.http import HttpUnauthorized, HttpForbidden, HttpApplicationError, HttpCreated
from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields
from tastypie.models import ApiKey
import os
from datetime import datetime, timedelta
from operator import and_
from pydub import AudioSegment
import shutil
import traceback
from campaign.models import  Campaign_category

from models import News, Channel, Channel_order, Category, ModerationEventRecorder, Mod_comment,\
Format, Qualifier, Call_stats, Item_stats, Social_publish_log, Collection, News_collection,\
News_state, VI_reference, News_vi_reference, Playlist_item, Playlist_sync_log,\
Playlist_channel_map, Playlist, MV_app_user, MV_user_registration_detail, MV_registration_log,\
Playlist_item_comment, Occupation, Age_group, Gender, Playlist_access_log, Playlist_item_access_log,\
ModerationEvents, Item_file_access_log, Playlist_obd_setting, Playlist_obd_log, Groups_call_log, Event_ai_properties,\
Rejection_reason, Forwarding_log, Event_sms_template, CallEvents, Forwarding_ai_properties, Forwarding_group_call_log,\
Rejection_reason_group, Event_rejection_reason_group

from prompts import PROMPTS_AI, PROMPTS_CHANNEL
from vapp.mnews.stats import generate_and_email_stats, get_contrib_dur_distrib, get_moderation_stats, get_call_stats, copy_detailed_data
from views import clone_news, clone_campaign
from tasks import CallAndPlayTask

from vapp.app_manager.common import get_queue_for_task
from vapp.api import AppInstanceAuthorization, AppManageAuthorization, apply_default_checks
from vapp.app_manager.api import AppInstanceResource, UserResource, AnonymousReadAuthentication, ViReferenceResource
from vapp.app_manager.models import App_instance, VI_conf, Cdr

from vapp.media.api import RecordingResource, PromptAudioResource, recording_upload_impl, ImageCaptionMapResource, ImageResource
from vapp.media.models import Prompt_audio, Image, Recording
from vapp.callerinfo.api import ContactResource
from vapp.callerinfo.models import Contact
from vapp.callerinfo.common import lookup_contact_for_user, DEFAULT_CALLERID
from vapp.campaign.api import CampaignCategoryResource
from vapp.location.api import LocationResource
from vapp.stats.models import Stats_type
from vapp.caching_service.utils import delete_from_dynamo
from vapp.sms.tasks import SMSTask
import vapp.app_manager.perms as app_manager_perms
from vapp.perms import user_has_perms, user_perms_ai_ids
from xlwt import *
from django.http import HttpResponse
from django.db.models.fields import FieldDoesNotExist

from vapp.utils import populate_spreadsheet, delete_obj_in_dynamo
from vapp.telephony.utils import get_formatted_number, RestrictedDict

from vapp.utils import get_start_of_month, daterange, get_start_of_week, datetimefield_to_seconds, generate_random_user
from vapp.log import get_request_logger
from local_settings import SERVER_ID


logger = get_request_logger()

class ChannelResource(ModelResource):
    ai = fields.ForeignKey(AppInstanceResource, 'ai')
    next = fields.CharField()

    class Meta:
        queryset = Channel.objects.all()
        resource_name = "mnews_channel"
        filtering = {
            'ai': ALL_WITH_RELATIONS,
            'name': ALL_WITH_RELATIONS,
            'default': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_default_channel%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_default_channel' ), name = "api_get_default_channel" ),
        ]

    def get_default_channel(self, request, **kwargs):
        self.method_check( request, allowed = ['get','post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            channel = Channel.get_default_channel( request.REQUEST[ 'ai_id' ] )
            channel_resource = ChannelResource()
            channel_bundle = channel_resource.build_bundle( obj = channel, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, channel_resource.full_dehydrate( channel_bundle ) )
        except:
            logger.exception( "get_default_channel: %s" % request )
        return self.create_response( request, { 'error': "unable to lookup default channel" } )

    def dehydrate_next(self, bundle):
        try:
            channel = bundle.obj
            channel_order = Channel_order.objects.get( initial_ai = channel.ai, cur_channel = channel )
            return ChannelResource().get_resource_uri( channel_order.next_channel )
        except:
            pass

class CategoryResource(ModelResource):
    #ai      = fields.ToManyField(AppInstanceResource, 'ai' )
    parent  = fields.ForeignKey('self', 'parent', null = True )
    #fullname= fields.CharField()

    class Meta:
        queryset = Category.objects.all()
        resource_name = "mnews_category"
        
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def dehydrate_fullname(self, bundle):
        category = bundle.obj
        if not category:
            return ""
        id = category.id
        names = [ category.name ]
        while category.parent:
            names.append( category.parent.name )
            category = category.parent
        names.reverse()
        return str( id ) + ". " + "/".join( names )

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(CategoryResource, self).build_filters( filters )
        if "ai" in filters:
            orm_filters[ "ai" ] = filters[ "ai" ]
        return orm_filters

class NewsFormatResource(ModelResource):
    class Meta:
        queryset = Format.objects.all()
        resource_name = "mnews_format"
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class NewsQualifierResource(ModelResource):
    class Meta:
        queryset = Qualifier.objects.all()
        resource_name = "mnews_qualifier"
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class GenderResource(ModelResource):

    class Meta:
        queryset = Gender.objects.all()
        resource_name = "mnews_gender"
        allowed_methods = ['get']
        
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class OccupationResource(ModelResource):

    class Meta:
        queryset = Occupation.objects.all()
        resource_name = "mnews_occupation"
        allowed_methods = ['get']

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class AgeGroupResource(ModelResource):

    class Meta:
        queryset = Age_group.objects.all()
        resource_name = "mnews_age_group"
        allowed_methods = ['get']

        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class NewsResource(ModelResource):
    ai      = fields.ForeignKey(AppInstanceResource, 'ai')
    channel = fields.ForeignKey(ChannelResource, 'channel', null = True )
    detail  = fields.ForeignKey(RecordingResource, 'detail', full = True, null = True )
    contact = fields.CharField()
    category= fields.ForeignKey(CategoryResource, 'category', null = True )
    location= fields.ForeignKey(LocationResource, 'location', null = True )
    nformat = fields.ForeignKey(NewsFormatResource, 'format', null = True )
    qualifier= fields.ForeignKey(NewsQualifierResource, 'qualifier', null = True )
    campaign_categories = fields.ToManyField(CampaignCategoryResource, 'campaign_categories', null = True )
    sm_image = fields.ForeignKey( ImageResource, 'sm_image', null = True, full = True )
    age_group = fields.ForeignKey( AgeGroupResource, 'age_group', null = True )
    occupation = fields.ForeignKey( OccupationResource, 'occupation',  null = True )
    gender = fields.ForeignKey( GenderResource, 'gender', null = True )

    class Meta:
        logger.info('Inside meta')
        queryset = News.objects.all().order_by('-time')
        logger.info(str(queryset))
        resource_name = "mnews_news"
        filtering = {
            'ai':             ALL_WITH_RELATIONS,
            'channel':        ALL_WITH_RELATIONS,
            'state':          ALL_WITH_RELATIONS,
            'is_comment':     ALL_WITH_RELATIONS,
            'is_mod_flagged': ALL_WITH_RELATIONS,
            'time':           ALL_WITH_RELATIONS,
            'title' :         ['exact', 'startswith', 'endswith', 'icontains'],
            'category' :      ALL_WITH_RELATIONS,
            'id':             ['exact', 'in'],
            'tags' :          ['icontains'],
            'callerid' :      ['startswith', 'contains'],
            'transcript' :    ['icontains', 'regex'],
            'rating' :        ['gte', 'lte'],
        }
        ordering = [ 'time', 'pub_order', 'rating' ]
        authentication = MultiAuthentication( ApiKeyAuthentication(), AnonymousReadAuthentication() )
        authorization = AppInstanceAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/reparent_comment%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'reparent_comment' ), name = "api_reparent_comment" ),
            url(r"^(?P<resource_name>%s)/create_new_item%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_new_item' ), name = "api_create_new_item" ),
            url(r"^(?P<resource_name>%s)/create_new_comment%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_new_comment' ), name = "api_create_new_comment" ),
            url(r"^(?P<resource_name>%s)/upload_audio%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'upload_audio' ), name = "api_upload_audio" ),
            url(r"^(?P<resource_name>%s)/update_pub_order%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'update_pub_order' ), name = "api_update_pub_order" ),
            url(r"^(?P<resource_name>%s)/get_all_tags%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_all_tags' ), name = "api_get_all_tags" ),
            url(r"^(?P<resource_name>%s)/copy_item_to_channel%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'copy_item_to_channel' ), name = "api_copy_item_to_channel" ),
            url(r"^(?P<resource_name>%s)/move_item_to_channel%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'move_item_to_channel' ), name = "api_move_item_to_channel" ),
            url(r"^(?P<resource_name>%s)/upload_sm_image%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'upload_sm_image' ), name = "api_upload_sm_image" ),
            url(r"^(?P<resource_name>%s)/create_dump%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_dump' ), name = "api_create_dump" ),
            url(r"^(?P<resource_name>%s)/forward_item%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'forward_item' ), name = "api_forward_item" ),
            url(r"^(?P<resource_name>%s)/news_save_item%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('news_save_item'), name="api_news_save_item"),
            url(r"^(?P<resource_name>%s)/push_item%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('push_item'), name="api_push_item"),

        ]


    def news_save_item(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        # self.throttle_check( request )

        try:
            logger.info(str(request.body))
            news_str = json.loads(str(request.body))
            item_id = news_str['id']
            try:
                news = News.objects.get(pk = int(item_id))
                for attrib, value in news_str.iteritems():
                    try:
                        attrib_type = News._meta.get_field(attrib).get_internal_type()
                        if attrib_type == 'ForeignKey':
                            logger.info(str(attrib)+'-->'+str(value))
                            setattr(news, attrib+'_id', value)
                        elif attrib_type == 'ManyToManyField':
                            refered_model = getattr(news, attrib).model
                            old_values = list(getattr(news, attrib).all().values_list('id', flat=True))
                            missing = set(old_values).difference(value)
                            additional = set(value).difference(old_values)
                            getattr(news, attrib).remove(*refered_model.objects.filter(id__in=missing))
                            getattr(news, attrib).add(*refered_model.objects.filter(id__in=additional))
                        elif attrib_type == 'DateTimeField':
                            setattr(news, attrib, datetime.strptime(value.split('.')[0], '%Y-%m-%dT%H:%M:%S'))
                        else:
                            logger.info('Inside else only')
                            setattr(news, attrib, value)
                    except FieldDoesNotExist:
                        setattr(news, attrib, value)
                    except Exception as ex:
                        logger.info(str(ex))
                news.moderator = request.user
                news.save()
                return self.create_response(request, {'success': 'True', 'message': 'Item Updated'})
            except Exception as e:
                logger.info('First try')
                logger.info(str(e))
                track = traceback.format_exc()
                logger.info(track)
                return self.create_response(request, {'success': 'False', 'message': 'Item Not Found'})
        except Exception as ee:
            logger.info('Second try')
            logger.info(str(ee))
            return self.create_response(request, {'success': 'False', 'message': 'Not a proper JSON'})


    def push_item(self, request, **kwargs):
        logger.info('push item')
        if request.body:
           news_str = json.loads(str(request.body))
           logger.info(str(news_str))
           item_id = news_str['id']
           logger.info(str(item_id)) 
        else:
           logger.info('prefligh request sending ok')
           return self.create_response(request, {'success': 'sucess'}) 
        try:
            news = News.objects.get(pk = int(item_id))
            news.misc = news_str['misc']
            if news_str['title']:
               news.title = news_str['title']
            if news_str['tags']:
               news.tags = news.tags + str(news_str['tags'])
            if news_str['transcript']:
               news.transcript = news_str['transcript']

            news.save()

        except Exception as e:
            logger.error(str(e))
            logger.error('Item not found. New Item. Creating item')

        return self.create_response(request, {'success': 'success'})

    def forward_item(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        success = None
	logger.info('forward item')
        try:
            news_id = request.REQUEST['item_id']
            number = request.REQUEST['forwarded_to']
            audio = request.FILES.get('audio_file')
            news = News.objects.get(pk=news_id)
            forwarding_ai_properties = Forwarding_ai_properties.objects.filter(ai_id = news.ai_id)
	    logger.info("Audio ::"+str(audio))
            if audio:
                forwarder_personal_recording = recording_upload_impl(audio, news.channel.ai.id)
                rec_full_path = forwarder_personal_recording.get_full_filename()
                rec_export_path = "/tmp/"+rec_full_path.split('/')[-1]
                AudioSegment.from_file(rec_full_path).export(rec_export_path, format="mp3", bitrate="16k")
                shutil.move(rec_export_path, rec_full_path)
            elif forwarding_ai_properties.exists() and forwarding_ai_properties[0].default_prompt:
                forwarder_personal_recording = forwarding_ai_properties[0].default_prompt
            else:
                forwarder_personal_recording = Recording.objects.get(id=3207115)
            logger.info('forwarder_personal_recording : ' + str(forwarder_personal_recording))
            forwarded_sms = None
            event_sms_template_qs = Event_sms_template.objects.filter(template__ai=news.ai, event=CallEvents.ITEM_FORWARDED)
            if event_sms_template_qs.exists():
                template = event_sms_template_qs[0].template
                message = template.process_template({})
                forwarded_sms = SMSTask.create_send_msg(news.ai.id, message, number)            
            forwarding_log = Forwarding_log(ai=news.ai, forwarded_item=news, forwarded_to=str(number), \
                                            forwarding_event="FFA", forwarded_sms=forwarded_sms, \
                                            forwarder_personal_recording=forwarder_personal_recording)
            forwarding_log.save()
            vi_conf = VI_conf.objects.get(controller = 'mnews.forwardingvi.ForwardingController',
                                            description = 'mnews.forwardingvi.ForwardingStateDescriptionMap')

            properties = Forwarding_ai_properties.objects.get(ai=forwarding_log.ai)
            vi_data = RestrictedDict.convert_dict_to_rdict({'forwarding_log_id':forwarding_log.id, 'forwarding_properties_id':properties.id, 'vi_conf_id':vi_conf.id})
            ai = properties.ai_to_jump
            group_call_log = Groups_call_log(number=forwarding_log.forwarded_to, group_schedule=properties.forwarding_schedule, ai=ai)
            group_call_log.save()
            logger.info(str(group_call_log))

            forwarding_gc_log = Forwarding_group_call_log(forwarding_log=forwarding_log, group_call_log=group_call_log)
            forwarding_gc_log.save()
            queue = get_queue_for_task(ai, default_queue='forwarding')
            CallAndPlayTask.apply_async(args=[group_call_log.id, properties.max_tries], 
                                    kwargs={'vi_data': vi_data}, eta=datetime.now(), queue=queue)
            success = True
        except Exception as e:
            logger.exception("Unable to initiate call")
            logger.exception(str(e))
            success = False
        return self.create_response(request, {'success': success})
    
    def create_dump(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_id = request.REQUEST[ 'ai_id' ]
            copy_detailed_data( ai_id )

            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, "reason": "creation of dump was successful." } )
        except Exception,e:
            logger.exception('exception in create_dump: {0}'.format(e))
        return self.create_response( request, { 'success': False, "reason": "creation of dump failed." } )
 
    def reparent_comment(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            comment = News.objects.get( pk = request.REQUEST[ 'comment_id' ] )
            old_parent = News.objects.get( pk = comment.parent_id() )
            new_parent = News.objects.get( pk = request.REQUEST[ 'parent_id' ] )
            old_parent.comments.remove( comment )
            new_parent.comments.add( comment )
            
            self.log_throttled_access( request )
            return self.create_response( request, { 'comment_id': comment.id, 'old_parent_id': old_parent.id, 'new_parent_id': new_parent.id } )
        except:
            logger.exception( "reparent_comment" )
        return self.create_response( request, { "error": "reparenting failed." } )

    def create_new_item(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            time = datetime.now()
            ai_id = request.REQUEST[ 'ai_id' ]
            channel_id = request.REQUEST[ 'channel_id' ]
            title = request.REQUEST.get( 'title' ) or 'News, Created at %s' % time.strftime('%H:%M, %d %B %Y')

            news = News(ai_id=ai_id, channel_id=channel_id,
                        state='UNM', is_comment=False,
                        source=News.Source.WEB_INTERFACE, title=title,
                        time=time, callerid=DEFAULT_CALLERID,
                        modified_date=time)
            news.save()
            news_resource = NewsResource()
            news_bundle = news_resource.build_bundle( obj = news, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, news_resource.full_dehydrate( news_bundle ) )
        except:
            logger.exception( "create_new_item: %s" % request )
        return self.create_response( request, {'error': "unable to create new item"} )

    def create_new_comment(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            time = datetime.now()
            title = request.REQUEST.get( 'title' ) or 'Comment, Created at %s' % time.strftime('%H:%M, %d %B %Y')
            parent = News.objects.get( pk = request.REQUEST[ 'parent_id' ] )
            
            comment = News(ai_id=parent.ai_id, channel_id=parent.channel_id,
                           state=News_state.UNM, is_comment=True,
                           source=News.Source.WEB_INTERFACE, title=title,
                           time=time, callerid=DEFAULT_CALLERID,
                           modified_date=time)
            comment.parent_id_value = parent.id
            comment.save()
            parent.comments.add( comment )
            news_resource = NewsResource()
            news_bundle = news_resource.build_bundle( obj = comment, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, news_resource.full_dehydrate( news_bundle ) )
        except:
            logger.exception( "create_new_comment: {0}".format( request ) )
        return self.create_response( request, {'error': "unable to create new comment"} )


    def copy_item_to_channel(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            news_id = request.REQUEST[ 'news_id' ]
            channel_id = request.REQUEST[ 'channel_id' ]
            news = News.objects.get(pk = news_id)
            copy = clone_news(news, copy_channel_id=channel_id, as_item=True)
            clone_campaign(news, copy)
            news.record_copy(copy)

            for comment in news.comments.exclude(state=News_state.REJ):
                comment_copy = clone_news( comment, parent = copy, copy_channel_id = channel_id )
                comment.record_copy(comment_copy)
            news_resource = NewsResource()
            news_bundle = news_resource.build_bundle( obj = copy, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, news_resource.full_dehydrate( news_bundle ) )

        except Exception, e:
            logger.exception("copy_item_to_channel: %s" % str(e))
        return self.create_response( request, {'error': "unable to copy news item"} )

    def move_item_to_channel(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            news_id = request.REQUEST['news_id']
            channel_id = request.REQUEST['channel_id']
            news = News.objects.get(id=news_id)
            news.channel_id = channel_id
            news.save()

            for comment in news.comments.all():
               comment.channel_id = channel_id
           

            news_resource = NewsResource()
            news_bundle = news_resource.build_bundle(obj=news, request=request)
            self.log_throttled_access(request)
            return self.create_response(request, news_resource.full_dehydrate(news_bundle))
        except Exception as e:
            logger.exception("Unable to move item to channel: %s", str(e))
        return self.create_response( request, {'error': "unable to move news item"} )

    def upload_audio(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            uploaded_file = request.FILES[ 'audio_file' ]
            ai_id = request.REQUEST[ 'ai_id' ]
            news_id = request.REQUEST[ 'news_id' ]
            recording_id = request.REQUEST.get( 'recording_id' )
            logger.debug( "upload_audio: user: %s, ai_id: %s, news_id: %s, rec_id: %s" % ( request.user.id, ai_id, news_id, recording_id ) )

            recording = recording_upload_impl( uploaded_file, ai_id, recording_id )
            news = News.objects.get( pk = news_id )
            news.detail = recording
            news.modified_date = datetime.now()
            news.save()
            recording_resource = RecordingResource()
            recording_bundle = recording_resource.build_bundle( obj = recording, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, recording_resource.full_dehydrate( recording_bundle ) )
        except:
            logger.exception( "upload_audio: %s" % request )
        return self.create_response( request, {'error': "unable to upload audio"} )

    def update_pub_order(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            news = News.objects.get( pk = request.REQUEST[ 'news_id' ] )
            news.pub_order = int( request.REQUEST[ 'target_pub_order' ] )
            news.moderator = request.user
            news.save()  
            return self.create_response( request, { 'news_pub_order': news.pub_order } )
        except:
            logger.exception( "update_pub_order" )
        return self.create_response( request, { "error": "re-ordering failed." } )

    def get_all_tags(self, request, **kwargs):
        self.method_check( request, allowed = [ 'get' ] )
        self.is_authenticated( request )
        self.throttle_check( request )
 
        try:
            spl_words = (' ', '-', '`', '.')
            ai_id = request.REQUEST[ 'ai_id' ]
            news_tags = News.objects.filter( ai_id = ai_id ).values_list('tags',flat = True).distinct().exclude(tags__isnull = True)
            formatted_news_tags = ",".join( news_tags)
            sorted_news_tags =  sorted(set(formatted_news_tags.split(",")))
            news_tags = [ tag for tag in sorted_news_tags if ( tag.islower() or tag.isupper() ) and not tag.startswith( spl_words ) and len( tag ) > 2 ]
            return self.create_response( request, { 'news_tags': news_tags } )
        except:
            logger.exception( "get_all_tags" )
        return self.create_response( request, { "error": "Fetching all Tags failed." } )

    def upload_sm_image( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'post' ] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:            
            uploaded_img_file = request.FILES[ 'image_file' ]
            img = Image( name = os.path.basename ( uploaded_img_file.name ))
            img.image = uploaded_img_file
            img.save()
            
            news = News.objects.get( id = request.REQUEST[ 'news_id' ] )
            news.sm_image = img
            news.save()
            image_resource = ImageResource()
            image_bundle = image_resource.build_bundle( obj = img, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, image_resource.full_dehydrate( image_bundle ) )
        except:
            logger.exception( "upload_sm_image: %s" % request )
        return self.create_response( request, {'error': "unable to upload sm image"} )

    def dehydrate_contact(self, bundle):
        request = bundle.request
        contact = lookup_contact_for_user( request.user, bundle.obj.callerid )
        contact_resource = ContactResource()
        contact_bundle = contact_resource.build_bundle( obj = contact, request = request )
        return contact_resource.full_dehydrate( contact_bundle )

    def dehydrate( self,bundle ):
        show_full_loc = bundle.request.GET.get('full_location', "false").lower() == "true"
        if show_full_loc:
            old_location_full = self.location.full
            self.location.full = True
            bundle.data['location'] = self.location.dehydrate(bundle)
            self.location.full = old_location_full
        return bundle

    def hydrate(self, bundle):
        if bundle.data.has_key( 'detail' ) and bundle.data['detail']:
            bundle.obj.detail_id = bundle.data['detail']['id']
            del bundle.data['detail']
        if bundle.data.has_key( 'sm_image' ) and bundle.data['sm_image']:
            bundle.obj.sm_image_id = bundle.data['sm_image']['id']
            del bundle.data['sm_image']
        if bundle.data.has_key( 'reason_id' ) and bundle.data['reason_id']:
            bundle.obj.reason_id = bundle.data['reason_id']

        bundle.obj.moderator = bundle.request.user
        return bundle

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(NewsResource, self).build_filters( filters )
        if "ids" in filters:
            orm_filters["id__in" ] = filters["ids"].split(',')
        if "from_date" in filters:
            orm_filters["time__gte"] = filters["from_date"]
        if "to_date" in filters:
            orm_filters["time__lte"] = filters["to_date"]
        if "moderator" in filters:
            orm_filters[ "moderationeventrecorder__user__id" ] = filters[ "moderator" ]
        if "parent" in filters:
            orm_filters[ "parent" ] = filters[ "parent" ]
        if "search_category" in filters:
            orm_filters[ "category__name__icontains" ] = filters[ "search_category" ]
        if "search_id" in filters:
            ids = filters['search_id'].split('&')
            orm_filters[ "id__in" ] = ids
        if "search_caller" in filters:
            orm_filters[ "callerid__startswith" ] = filters[ "search_caller" ]
        if "news_type" in filters:
            news_type = filters['news_type']
            if news_type == "recommended":
                orm_filters[ "rating__gte" ] = 4
            elif news_type == "trending":
                orm_filters[ "rating__lte" ] = 3
        if "campaign_id" in filters:
            orm_filters[ "campaign_categories__campaign" ] = filters["campaign_id"]   
        if "search_tags" in filters:
            orm_filters['q_filters'] = orm_filters.get('q_filters', []) + [ Q(tags__icontains = tag) for tag in filters["search_tags"].split(',') ]
        if "search_title" in filters:
            orm_filters['q_filters'] = orm_filters.get('q_filters', []) + [ Q(title__icontains = title_kw) for title_kw in filters["search_title"].split(',') ]
        if "search_transcript" in filters:
            orm_filters['q_filters'] = orm_filters.get('q_filters', []) + [ Q(transcript__icontains = trans_kw) for trans_kw in filters["search_transcript"].split(',') ]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        q_filters = applicable_filters.pop('q_filters', None)

        semi_filtered = super(NewsResource, self).apply_filters( request, applicable_filters )

        if q_filters:
            semi_filtered = semi_filtered.filter( reduce(and_, q_filters) )

        if "moderationeventrecorder__user__id" in applicable_filters:
            return semi_filtered.distinct()
        else:
            return semi_filtered


class ModerationEventResource(ModelResource):
    user = fields.CharField()
    item = fields.ForeignKey(NewsResource, 'item')

    class Meta:
        queryset = ModerationEventRecorder.objects.all().order_by('-timestamp')
        resource_name = "mnews_moderation_event"
        filtering = {
            'item': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_news_ids%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_news_ids'), name = "api_get_news_ids"),
        ] 

    def get_news_ids(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            mod_events_filters = {}
            mod_events_filters['item__ai__id'] = request.REQUEST['ai']
            if request.REQUEST.get('state'):
                mod_events_filters['item__state'] = request.REQUEST['state']
            if request.REQUEST.get('moderator'):
                mod_events_filters['user__id'] = request.REQUEST['moderator']
            if request.REQUEST.get('is_mod_flagged'):
                mod_events_filters['item__is_mod_flagged'] = request.REQUEST['is_mod_flagged']
            if request.REQUEST.get('event_type'):
                mod_events_filters['event_type'] = request.REQUEST['event_type']
            if request.REQUEST.get('channel'):
                mod_events_filters['item__channel__id'] = request.REQUEST['channel']
            if request.REQUEST.get('from_date'):
                mod_events_filters['timestamp__gte'] = request.REQUEST['from_date']
            if request.REQUEST.get('to_date'):
                mod_events_filters['timestamp__lte'] = request.REQUEST['to_date']
            if request.REQUEST.get('search_category'):
                mod_events_filters['item__category__name__icontains'] = request.REQUEST['search_category']
            if request.REQUEST.get('search_id'):
                mod_events_filters['item__id__exact'] = request.REQUEST['search_id']
            if request.REQUEST.get('search_caller'):
                mod_events_filters['item__callerid__startswith'] = request.REQUEST['search_caller']
            if request.REQUEST.get('search_tags'):
                mod_events_filters['item__tags__icontains'] = request.REQUEST['search_tags']
            if request.REQUEST.get('search_title'):
                mod_events_filters['item__title__icontains'] = request.REQUEST['search_title']
            if request.REQUEST.get('search_transcript'):
                mod_events_filters['item__transcript__icontains'] = request.REQUEST['search_transcript']

            item_ids = list(ModerationEventRecorder.objects.filter(**mod_events_filters).order_by('-item__id').values_list('item__id', flat = True).distinct())
            self.log_throttled_access(request)
            return self.create_response(request, {'news_ids': item_ids})
        except:
            logger.exception("not able to find news_ids from moderation events")
        return self.create_response(request, {"error": "news search failed."})

    def dehydrate_user(self, bundle):
        return bundle.obj.user.username

class ModCommentResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    news = fields.ForeignKey(NewsResource, 'news')

    class Meta:
        queryset = Mod_comment.objects.all()

        resource_name = "mnews_mod_comment"
        filtering = {
            'news': ALL_WITH_RELATIONS,
            'user': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def dehydrate_user(self, bundle):
        return bundle.obj.user.username

    def hydrate(self, bundle):
        bundle.obj.user_id = bundle.request.user.id
        bundle.obj.news_id = bundle.data['news_id']
        bundle.obj.time = datetime.now()
        return bundle

class MnewsPromptDescWrapper(object):
    def __init__(self, initial = None):
        self.__dict__[ '_data' ] = {}

        if hasattr( initial, 'items' ):
            self.__dict__[ '_data' ] = initial

    def __getattr__(self, name):
        return self._data.get( name, None )

    def __setattr__(self, name, value):
        self.__dict__[ '_data'][ name ] = value

    def to_dict(self):
        return self._data


class MnewsPromptDescResource(Resource):
    pk = fields.IntegerField( "pk" )
    ai_prompts = fields.ListField( "ai_prompts" )
    channel_prompts = fields.DictField( "channel_prompts" )

    class Meta:
        resource_name = "mnews_prompt_desc"
        object_class = MnewsPromptDescWrapper
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

    def get_prompt_audio( self, ai_id, prompt_name, request ):
        try:
            prompt_audio = Prompt_audio.objects.get(prompt_set__app_instance_prompt_set__ai = ai_id, 
                                          prompt_set__app_instance_prompt_set__is_current = True, 
                                          info__name = prompt_name )
            pa_resource = PromptAudioResource()
            pa_bundle = pa_resource.build_bundle( obj = prompt_audio, request = request )
            return pa_resource.full_dehydrate( pa_bundle )
        except:
            logger.exception( "get_prompt_audio ai: %s, prompt %s" % ( ai_id, prompt_name ) )

    def get_ai_prompts( self, ai_id, request ):
        res = []
        for prompt_name in PROMPTS_AI:
            prompt_audio = self.get_prompt_audio( ai_id, prompt_name, request )
            res.append( { "name": prompt_name, "prompt_audio": prompt_audio } )
        return res

    def get_channel_prompts( self, ai_id, channel_name, request ):
        res = []
        for prompt_name in PROMPTS_CHANNEL:
            prompt_audio = self.get_prompt_audio( ai_id, "%s__%s" % ( prompt_name, channel_name ), request )
            res.append( { "name": prompt_name, "prompt_audio": prompt_audio } )
        return res

    def get_wrapper_for_ai_id( self, ai_id, request ):
        obj = MnewsPromptDescWrapper()
        obj.pk = ai_id
        obj.ai_prompts = self.get_ai_prompts( ai_id, request )
        obj.channel_prompts = {}
        channels = Channel.objects.filter( ai_id = ai_id )
        for channel in channels:
            obj.channel_prompts[ channel.id ] = self.get_channel_prompts( ai_id, channel.name, request )
        return obj

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        if isinstance( bundle_or_obj, Bundle ):
            pk = bundle_or_obj.obj.pk
        else:
            pk = bundle_or_obj.pk
        kwargs[ 'pk' ] = pk
        return kwargs

    def get_object_list(self, request):
        ai_ids = user_perms_ai_ids( request.user, app_manager_perms.app_use )
        # ais = [ App_instance.objects.get( pk = ai_id ) for ai_id in ai_ids ]
        # ais = [ ai for ai in ais if ai.app.name == "mnews" ]
        results = []
        for ai_id in ai_ids:
            results.append( self.get_wrapper_for_ai_id( ai_id, request ) )
        return results

    def obj_get_list(self, bundle, **kwargs):
        return self.get_object_list( bundle.request )

    def obj_get(self, bundle, **kwargs):
        return self.get_wrapper_for_ai_id( kwargs[ 'pk' ], bundle.request )

    def obj_create(self, bundle, **kwargs):
        bundle.obj = None
        return bundle

    def obj_udpate(self, bundle, **kwargs):
        return bundle

    def obj_delete_list(self, bundle, **kwargs):
        pass

    def obj_delete(self, bundle, **kwargs):
        pass

    def rollback(self, bundles):
        pass
    

class MnewsStatsResource(Resource):
    class Meta:
        resource_name = 'mnews_stats'
        authentication = ApiKeyAuthentication()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/call%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_call_stats' ), name = "api_call_stats" ),
            url(r"^(?P<resource_name>%s)/item%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_item_stats' ), name = "api_item_stats" ),
            url(r"^(?P<resource_name>%s)/email%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'email_stats' ), name = "api_email_stats" ),
            url(r"^(?P<resource_name>%s)/contrib_dur_distrib%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_contrib_duration_distrib' ), name = "api_contrib_dur_distrib" ),
            url(r"^(?P<resource_name>%s)/moderation%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_moderation_stats' ), name = "api_moderation_stats" ),
        ]
        
    def get_call_stats(self, request, **kwargs):
        self.method_check( request, allowed = [ 'get' ] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST[ 'ai_id' ]
            stat_type = request.REQUEST[ 'stat_type' ]
            data = []
            start_datetime, end_datetime = request.REQUEST.get('start_date', None), request.REQUEST.get('end_date', None)
            if (start_datetime and end_datetime):
                stats = get_call_stats(ai_id, start_datetime, end_datetime)
                time_in_sec = datetimefield_to_seconds(start_datetime)
                data.append({ 'calls': stats.calls, 'callers': stats.callers, 'avg_dur': stats.avg_dur, 'time': time_in_sec })
            else:
                end_datetime = datetime.combine(datetime.today(), datetime.min.time()) + timedelta(days = 1)
                if stat_type == Stats_type.DAILY:
                    start_datetime = end_datetime - timedelta(days = 30)
                    date_diff = 1
                elif stat_type == Stats_type.WEEKLY:
                    weeks_count = 15
                    end_datetime = get_start_of_week(end_datetime)
                    start_datetime = end_datetime - timedelta(days = 7*weeks_count)
                    date_diff = 7
                for dt in daterange(start_datetime, end_datetime, timedelta(days = date_diff)):
                    to_datetime = dt + timedelta(days = date_diff)
                    stats = get_call_stats( ai_id, dt, to_datetime )
                    time_in_sec = datetimefield_to_seconds( dt )
                    data.append({ 'calls': stats.calls, 'callers': stats.callers, 'avg_dur': stats.avg_dur, 'time': time_in_sec })

            self.log_throttled_access( request )
            return self.create_response( request, { "data": data } )
        except:
            logger.exception( "Exception while running: get_call_stats: %s" % request )
        return self.create_response( request, { 'error': "unable to get call stats" } )
    
    def get_item_stats(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST['ai_id']
            start_datetime = datetime.strptime(request.REQUEST['start_date'], '%Y-%m-%d')
            end_datetime = datetime.strptime(request.REQUEST['end_date'], '%Y-%m-%d')
            stats = Item_stats.objects.get(ai_id = ai_id, start = start_datetime, end = end_datetime)
            item_stats = {}
            for field_name in stats._meta.get_all_field_names():
                if field_name not in [ 'id', 'ai', 'start', 'end' ]:
                    item_stats[field_name] = getattr(stats, field_name)
            
            self.log_throttled_access( request )
            return self.create_response( request, item_stats )
        except:
            logger.exception( "Exception while running: get_item_stats: %s" % request )
        return self.create_response( request, { 'error': "unable to get item stats" } )
    
    def email_stats(self, request, **kwargs):
        self.method_check( request, allowed = [ 'get' ] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_ids = request.REQUEST['ai_ids'].split(",")
            ais = App_instance.objects.filter(pk__in = ai_ids)
            start_datetime = datetime.strptime(request.REQUEST['start_date'], '%Y-%m-%d')
            end_datetime = datetime.strptime(request.REQUEST['end_date'], '%Y-%m-%d')
            recipient_email = request.REQUEST['email']
            stats_type = request.REQUEST['stats_type']
            adv_ai_ids = request.REQUEST.get('adv_ai_ids', None)
            adv_ais = None
            if adv_ai_ids:
                adv_ai_ids = adv_ai_ids.split(",")
                adv_ais = App_instance.objects.filter(pk__in = adv_ai_ids)
            generate_and_email_stats(ais, start_datetime, end_datetime, recipient_email, stats_type, adv_ais)

            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True } )
        except:
            logger.exception( "Exception while running: email_stats: %s" % request )
        return self.create_response( request, { 'error': "unable to send email stats" } )
    
    def get_contrib_duration_distrib(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_ids = request.REQUEST['ai_ids'].split(",")
            start_datetime = request.REQUEST.get('start_date', None)
            end_datetime = request.REQUEST.get('end_date', None)
            if start_datetime and end_datetime:
                start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
                end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d')

            stats = get_contrib_dur_distrib(ai_ids, start_datetime, end_datetime)

            self.log_throttled_access( request )
            return self.create_response( request, { 'stats': stats } )
        except:
            logger.exception( "Exception while running: get_contrib_duration_distribution: %s" % request )
        return self.create_response( request, { 'error': "unable to get contrib duration distribution" } )
    
    def get_moderation_stats(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_id = request.REQUEST['ai_id']
            start_datetime = request.REQUEST.get('start_date', None)
            end_datetime = request.REQUEST.get('end_date', None)
            if start_datetime and end_datetime:
                start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
                end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d')

            stats = get_moderation_stats(ai_id, start_datetime, end_datetime)

            self.log_throttled_access( request )
            return self.create_response( request, { 'stats': stats } )
        except:
            logger.exception( "Exception while running: get_moderation_stats: %s" % request )
        return self.create_response( request, { 'error': "unable to get moderation stats" } )


class SocialLogResource(ModelResource):
    news_id = fields.IntegerField( attribute = 'news__id' )
    smedia  = fields.CharField( attribute = 'smedia__sm_name' )
    permalink = fields.CharField( attribute = 'permalink', null = True )
    class Meta:
        queryset = Social_publish_log.objects.all()
        resource_name = 'mnews_social_log'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class CollectionResource( ModelResource ):
    ais = fields.ToManyField(AppInstanceResource, attribute='ais',full=True)
    categories= fields.ToManyField(CategoryResource, attribute = 'categories')
    locations = fields.ToManyField(LocationResource, attribute = 'locations' )
    cover_image = fields.ForeignKey(ImageCaptionMapResource, attribute='cover_image', full=True, null= True)    

    class Meta:
        queryset = Collection.objects.all()
        resource_name = "mnews_collection"
        authentication = MultiAuthentication( ApiKeyAuthentication(), AnonymousReadAuthentication() )
        authorization = ReadOnlyAuthorization() 

class NewsCollectionResource(ModelResource):
    collection = fields.ForeignKey(CollectionResource, attribute='collection')
    news = fields.ToManyField(NewsResource, attribute = 'news_items', full = True )

    class Meta:
        queryset = News_collection.objects.all()
        resource_name = "mnews_news_collection"
        filtering = {
            'collection': ALL_WITH_RELATIONS,
            }
        authentication = MultiAuthentication(ApiKeyAuthentication(), AnonymousReadAuthentication())
        authorization = ReadOnlyAuthorization() 

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(NewsCollectionResource, self).build_filters(filters)
        if "collection_id" in filters:
            orm_filters[ "collection" ] = filters[ "collection_id" ]
        return orm_filters

class NewsViReferenceResource( ModelResource ):
    news = fields.ToOneField( NewsResource, "news" )
    vi_reference = fields.ForeignKey( ViReferenceResource, "vi_reference", full= True )

    class Meta:
        queryset = News_vi_reference.objects.all()
        resource_name = "mnews_news_vi_reference"
        filtering = {
             'news' : ALL,
        }
        authentication = ApiKeyAuthentication()
        authorization = AppManageAuthorization()

    def hydrate(self, bundle):
        news_id = bundle.obj.news_id = bundle.data['news_id']
        source_ai = bundle.data['ai']
        target_ai =  bundle.data['target_ai']
        news_vi_reference = News_vi_reference.objects.filter( news_id = news_id )
        params = { 'vi_reference': news_vi_reference[0].vi_reference } if news_vi_reference else {}
        vi_reference = VI_reference.add_or_update(source_ai, target_ai, news_id, **params )        
        bundle.obj.vi_reference = vi_reference
        return bundle

class PlaylistResource(ModelResource):
    ai = fields.IntegerField(attribute="playlist_ai")

    class Meta:
        queryset = Playlist.objects.all().order_by('order')
        resource_name = "mnews_playlist"
        filtering = {
            'id' : ALL,
            'ai': 'exact',
        }
        allowed_methods = ['get']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/obd%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('schedule_obd'), name="api_schedule_obd"),
        ]

    def schedule_obd(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mnews_playlist/schedule_obd with request: {}'.format(request))
            success = False
            error = ''
            obd_limit_exceeded = False
            user_id = request.REQUEST['mv_app_user_id']
            playlist_id = request.REQUEST['playlist_id']
            device_id = request.REQUEST.get('device_id')
            app_version = request.REQUEST.get('app_version')

            schedule = Playlist_obd_setting.objects.get(playlist_id=playlist_id).schedule
            number = MV_app_user.objects.get(pk=user_id).contact.number
            number = get_formatted_number(number)

            if Playlist_obd_log.objects.filter(playlist_id=playlist_id,
                        app_user_id=user_id,
                        creation_time__gte=datetime.now() - timedelta(seconds=300)).count() >= 1:
                logger.debug('Already received 1 or more OBD requests.')
                obd_limit_exceeded = True
            else:
                ai = schedule.ai_group.ai
                gcl = Groups_call_log(number=number,
                                      ai=ai,
                                      group_schedule=schedule)
                gcl.save()
                Playlist_obd_log(playlist_id=playlist_id,
                                 app_user_id=user_id,
                                 gcl_id=gcl.id,
                                 device_id=device_id,
                                 app_version=app_version).save()
                queue = get_queue_for_task(ai, default_queue='push')
                CallAndPlayTask.apply_async([gcl.id], eta=datetime.now(), queue=queue)
                success = True
            response_class = HttpCreated
        except Exception as e:
            response_class = HttpApplicationError
            error = 'exception while scheduling obd: {}'.format(e)
            logger.exception(error)

        response = {'success': success, 'error': error, 'obd_limit_exceeded': obd_limit_exceeded}
        self.log_throttled_access(request)
        return self.create_response(request, response, response_class=response_class)

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(PlaylistResource, self).build_filters(filters)

        if "ai_id" in filters:
            orm_filters["ai_id"] = filters["ai_id"]
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        ai_filter = applicable_filters.pop('ai_id', None)
        semi_filtered = super(PlaylistResource, self).apply_filters(request, applicable_filters)
        if ai_filter:
            semi_filtered = [playlist for playlist in semi_filtered if \
                                playlist.playlist_ai() == int(ai_filter)]
        return semi_filtered

    def authorized_read_list(self, object_list, bundle):
        api_key = bundle.request.REQUEST.get('api_key')
        device_id = bundle.request.REQUEST.get('device_id')
        app_version = bundle.request.REQUEST.get('app_version')
        user = None
        if api_key and ApiKey.objects.filter(key=api_key).exists():
            apikey = ApiKey.objects.get(key=api_key)
            user = apikey.user
        Playlist_access_log.add_access_log(object_list,
                                           user=user,
                                           device_id=device_id,
                                           app_version=app_version)
        return super(PlaylistResource, self).authorized_read_list(object_list, bundle)

class PlaylistItemResource(ModelResource):
    playlist_id  = fields.IntegerField(attribute='playlist__id')
    num_comments = fields.IntegerField(attribute='num_comments')
    duration     = fields.DecimalField(attribute='item__detail__get_duration')
    contributor  = fields.CharField(attribute='contributor_name', null=True)
    item_url     = fields.CharField(attribute='item_url')

    class Meta:
        queryset = Playlist_item.objects.filter(is_current=True)
        resource_name = "mnews_playlist_item"
        filtering = {
            'playlist' : ALL_WITH_RELATIONS,
            'playlist_id': 'exact',
        }
        allowed_methods = ['get']
        exclude = ('item', 'is_current')

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_item%s$" % (self._meta.resource_name, \
                trailing_slash()), self.wrap_view('get_item'), name="api_get_item"),
        ]

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(PlaylistItemResource, self).build_filters(filters)

        if not "is_current" in filters:
            orm_filters["is_current"] = True

        return orm_filters

    def authorized_read_list(self, object_list, bundle):
        api_key = bundle.request.REQUEST.get('api_key')
        device_id = bundle.request.REQUEST.get('device_id')
        app_version = bundle.request.REQUEST.get('app_version')
        user = None
        if api_key and ApiKey.objects.filter(key=api_key).exists():
            apikey = ApiKey.objects.get(key=api_key)
            user = apikey.user
        Playlist_item_access_log.add_access_log(object_list,
                                                user=user,
                                                device_id=device_id,
                                                app_version=app_version)
        object_list = list(super(PlaylistItemResource, self).authorized_read_list( \
                                    object_list, bundle))
        object_list.sort(key=lambda x: x.item.pub_order)
        return object_list

    def get_item( self, request, **kwargs ):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            api_key = request.REQUEST.get('api_key')
            device_id = request.REQUEST.get('device_id')
            app_version = request.REQUEST.get('app_version')
            item_id = request.GET.get('item_id')
            user = None
            if api_key and ApiKey.objects.filter( key = api_key ).exists():
                apikey = ApiKey.objects.get( key = api_key )
                user = apikey.user
            item = Playlist_item.objects.get( id = item_id )
            access_log = Item_file_access_log.add_access_log( item_id, user = user, device_id = device_id, app_version = app_version )
            f = open( item.item_location, "rb" )
            response = HttpResponse()
            response.write( f.read() )
            response['Content-Type'] ='audio/mp3'
            response['Content-Length'] = os.path.getsize( item.item_location )
            return response
        except Exception,e:
            error = ('exception while getting item: %s'%(e))
            logger.exception( error )
        return self.create_response( request, { 'error': error }, response_class = HttpApplicationError )


class MVAppResource(ModelResource):

    class Meta:
        queryset = MV_app_user.objects.all()
        resource_name = "mv_app_user"
        filtering = {
            'contact' : ALL_WITH_RELATIONS,
        }
        allowed_methods = ['get']
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/register%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('register'),
                name="api_register"),
            url(r"^(?P<resource_name>%s)/update_profile%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('update_profile'),
                name="api_update_profile"),
            url(r"^(?P<resource_name>%s)/verify_registration%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('verify_registration'),
                name="api_verify_registration"),
            url(r"^(?P<resource_name>%s)/verify_update%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('verify_update'),
                name="api_verify_update"),
        ]

    def register(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/register with request: {}'.format(request))
            mv_app_user_id = None
            success = False

            number = get_formatted_number(request.REQUEST['number'])
            if not number:
                raise Exception("number not in correct format.")
            name = request.REQUEST['name']

            mv_app_user_set = MV_app_user.objects.filter(contact__number=number)
            if mv_app_user_set.filter(contact__name=name).exists():
                logger.debug('User already exists for the credentials provided.')
                mv_app_user = mv_app_user_set.filter(contact__name=name).latest('id')
                mv_app_user.is_registered = False
                mv_app_user.save()
                query_type = MV_user_registration_detail.QueryType.EXISTING_USER_REGISTRATION
            else:
                logger.debug('Creating new mv_app_user.')
                contact = Contact.objects.create(name=name,
                                                 number=number)
                mv_app_user = MV_app_user(contact=contact)
                mv_app_user.app_user = generate_random_user()
                mv_app_user.save()
                query_type = MV_user_registration_detail.QueryType.NEW_USER_REGISTRATION

            mv_app_user_id = mv_app_user.id
            success = True
            MV_user_registration_detail.send_registration_message(mv_app_user,
                                                                  number,
                                                                  query_type)
        except Exception as e:
            error = 'exception while registering user: {}'.format(e)
            logger.exception(error)

        response = {'mv_app_user_id': mv_app_user_id, 'success': success}
        self.log_throttled_access(request)
        return self.create_response(request, response)

    def update_profile(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/update_profile with request: {}'.format(request))
            success = False

            mv_app_user = MV_app_user.objects.get(pk=request.REQUEST['userId'])
            number = get_formatted_number(request.REQUEST['number'])
            if not number:
                raise Exception("number not in correct format.")
            name = request.REQUEST['name']

            contact = mv_app_user.contact
            if contact.number != number:
                if contact.name != name:
                    query_type = MV_user_registration_detail.QueryType.UPDATE_NAME_AND_NUMBER
                else:
                    query_type = MV_user_registration_detail.QueryType.UPDATE_NUMBER

                MV_user_registration_detail.send_registration_message(mv_app_user,
                                                                      number,
                                                                      query_type)
                success = True
            elif contact.name != name:
                mv_app_user.contact.name = name
                mv_app_user.contact.save()
                query_type = MV_user_registration_detail.QueryType.UPDATE_NAME
                MV_user_registration_detail(mv_user=mv_app_user,
                                            query_type=query_type,
                                            expiry_time=datetime.now()).save()
                success = True
        except Exception as e:
            logger.exception('exception while updating profile: {}'.format(e))

        response = {'success': success}
        self.log_throttled_access(request)
        return self.create_response(request, response)

    def verify_registration(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/verify_registration with request: {}'.format(request))
            api_key = None
            success = False

            mv_user_id = request.REQUEST['userId']
            input_code = request.REQUEST['code']

            mv_user = MV_app_user.objects.get(pk=mv_user_id)
            user_details = MV_user_registration_detail.objects.filter(mv_user=mv_user,
                                                         expiry_time__gte=datetime.now())
            success = MV_registration_log.check_and_log_attempt(user_details, input_code)
            if success:
                mv_user.is_registered = True
                mv_user.save()
                api_key = ApiKey.objects.get(user=mv_user.app_user).key
        except Exception as e:
            logger.exception('exception while verify_registration: {}'.format(e))

        response = {'api_key': api_key, 'success': success}
        self.log_throttled_access(request)
        return self.create_response(request, response)

    def verify_update(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            logger.debug('Inside mv_app_user/verify_update with request: {}'.format(request))
            api_key = None
            success = False

            mv_user_id = request.REQUEST['userId']
            input_code = request.REQUEST['code']
            number = get_formatted_number(request.REQUEST['number'])
            if not number:
                raise Exception("number not in correct format.")
            name = request.REQUEST['name']

            mv_user = MV_app_user.objects.get(pk=mv_user_id)
            user_details = MV_user_registration_detail.objects.filter(mv_user=mv_user,
                                                         expiry_time__gte=datetime.now())
            success = MV_registration_log.check_and_log_attempt(user_details, input_code)
            if success:
                mv_user.contact.name = name
                mv_user.contact.number = number
                mv_user.contact.save()
                api_key = ApiKey.objects.get(user=mv_user.app_user).key
        except Exception as e:
            message = 'exception while verify_update: {}'.format(e)
            logger.exception(message)

        response = {'api_key': api_key, 'success': success, 'name':name, 'number': number[2:]}
        self.log_throttled_access(request)
        return self.create_response(request, response)


class PlaylistItemCommentResource(ModelResource):
    playlist_item_id = fields.IntegerField(attribute='playlist_item__id')
    comment_user = fields.CharField(attribute="comment_user", null=True)
    user_id = fields.IntegerField(attribute="user__id", null=True)

    class Meta:
        queryset = Playlist_item_comment.objects.all()
        resource_name = "mnews_item_comment"
        filtering = {
            'playlist_item_id': 'exact',
        }
        allowed_methods = ['get', 'post']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/comment%s$" % (self._meta.resource_name, \
                trailing_slash()), self.wrap_view('comment'), name="api_comment"),
        ]

    def comment(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        try:
            success = False
            api_key = request.REQUEST['api_key']
            comment = request.REQUEST['comment']
            item_id = request.REQUEST['playlist_item_id']

            if (api_key and not ApiKey.objects.filter(key=api_key).exists()):
                raise Exception('api key doesnt exist.')

            user = ApiKey.objects.get(key=api_key).user
            item = Playlist_item.objects.get(id=item_id)

            comment = Playlist_item_comment.objects.create( \
                                            user=user,
                                            comment=comment,
                                            playlist_item=item)
            success = True
            comment_resource = PlaylistItemCommentResource()
            comment_bundle = comment_resource.build_bundle(obj=comment, request=request)

            self.log_throttled_access(request)
            return self.create_response(request, {'success': success, \
                    'comment': comment_resource.full_dehydrate(comment_bundle)})
        except Exception as e:
                success = False
                exception_str = ("exception while creating comment: %s"%(e))
                logger.exception(exception_str)
        return self.create_response(request, {'success': success, 'error': exception_str})

class RejectionReasonResource(ModelResource):

    class Meta:
        queryset = Rejection_reason_group.objects.all()
        resource_name = "mnews/reject_reason"
        allowed_methods = ['get']
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get%s$" % (self._meta.resource_name, \
                trailing_slash()), self.wrap_view('get_reasons'), name="api_get_reasons"),
        ]

    def get_reasons(self,request,**kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:

            ai_id = request.REQUEST['ai_id']
            comment = request.REQUEST['comment']=='true'
              
            if comment:
                event_ai_properties_qs = Event_ai_properties.objects.filter(event_type=ModerationEvents.COMMENT_REJECTED, ai_id=ai_id)
            else:
                event_ai_properties_qs = Event_ai_properties.objects.filter(event_type=ModerationEvents.ITEM_REJECTED, ai_id=ai_id)
            
            rejection_reasons = None
            if event_ai_properties_qs:
                event_rejection_reason_group = Event_rejection_reason_group.objects.filter(event=event_ai_properties_qs[0])
                rejection_reason_group = event_rejection_reason_group[0].group
                rejection_reasons = rejection_reason_group.rejection_reason
            count = 0
            reason_objects = []
            if rejection_reasons:
                count = rejection_reasons.count()
                for reason in rejection_reasons.all():
                    reason_dict = {}
                    reason_dict['id'] = str(reason.id)
                    reason_dict['name'] = str(reason.reason)
                    reason_objects.append(reason_dict)
            return self.create_response(request, {'objects': reason_objects, 'count': count})
        except:
            logger.exception("Exception in get_reasons: %s" % request)
        return self.create_response(request, {'error': "unable to get reasons"})
