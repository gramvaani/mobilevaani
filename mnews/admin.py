from django.contrib import admin
from django import forms

from models import *

from media.models import Recording
from app_manager.models import App_instance
from media.api import recording_upload_impl
from log import get_request_logger
logger = get_request_logger()

class TransientGroupAdminForm(forms.ModelForm):
    class Meta:
        model = Transient_group_schedule
        widgets = {
          'day_of_week': forms.CheckboxSelectMultiple,
        }
    
class TransientGroupAdmin(admin.ModelAdmin):
    form = TransientGroupAdminForm
    list_filter = ('active',)
    search_fields = ['name']

class PlaylistAdminForm(forms.ModelForm):
    class Meta:
        model = Playlist
        exclude = ('created_at',)
        

class PlaylistAdmin(admin.ModelAdmin):
    form = PlaylistAdminForm

class ForwardingAiPropertiesAdminForm(forms.ModelForm):
    default_forward_file = forms.FileField(required = False)
    
    class Meta:
        model = Forwarding_ai_properties
	widgets = {
            'default_prompt': forms.TextInput(attrs={'readonly':'True'})
        }

class ForwardingAiPropertiesAdmin(admin.ModelAdmin):
    form = ForwardingAiPropertiesAdminForm

    def save_model(self, request, obj, form, change):
        if request.method == 'POST' and request.FILES.get('default_forward_file'):
            form = ForwardingAiPropertiesAdminForm(request.POST, request.FILES)
            if form.is_valid():
                recording_obj = Recording(ai = App_instance.objects.get(pk = 10))
                recording_obj.save()
                obj.default_prompt = recording_obj
                super(ForwardingAiPropertiesAdmin, self).save_model(request, obj, form, change)
                recording_upload_impl(request.FILES['default_forward_file'], 10, recording_obj.id)
        else:
            form = ForwardingAiPropertiesAdminForm()
	    super(ForwardingAiPropertiesAdmin, self).save_model(request, obj, form, change)


admin.site.register(Transient_group)
admin.site.register(Days)
admin.site.register(Ai_transient_group)
admin.site.register(Transient_group_schedule,TransientGroupAdmin)
admin.site.register(Event_sms_template)
admin.site.register(Channel)
admin.site.register(Channel_order) 
admin.site.register(Ai_feed_generator_mapping)
admin.site.register(Forwarding_ai_properties, ForwardingAiPropertiesAdmin)
admin.site.register(Event_callout_blocking_policy)
admin.site.register(Playlist, PlaylistAdmin)
admin.site.register(Playlist_channel_map)
admin.site.register(Playlist_obd_setting)
admin.site.register(Rejection_reason)
admin.site.register(Rejection_reason_group)
admin.site.register(Event_rejection_reason_group)

class Event_ai_properties_admin(admin.ModelAdmin):
    filter_horizontal = ('on_publish_dialout_lists',)
admin.site.register(Event_ai_properties, Event_ai_properties_admin)


#Choosing a text field to enter news id on purpose as the new table is very
#big and admin takes very long to load the page
class News_vi_reference_admin(admin.ModelAdmin):
    formfield_overrides = {
        models.OneToOneField: {'widget': forms.TextInput},
    }
admin.site.register(News_vi_reference, News_vi_reference_admin)

admin.site.register(Category)
admin.site.register(Ad_channel_map)

class News_settings_admin(admin.ModelAdmin):
    filter_horizontal = ('autopublish_lists',)
admin.site.register(News_settings, News_settings_admin)

admin.site.register(News_selection_signal)
admin.site.register(News_selection_signal_weights)
admin.site.register(News_selection_algo)
admin.site.register(AB_testing_config)

class CollectionAdmin(admin.ModelAdmin):
    filter_horizontal = ('ais', 'categories','locations')
admin.site.register(Collection, CollectionAdmin)
admin.site.register(MV_SMS_detail)
admin.site.register(Age_group)
admin.site.register(Occupation)
admin.site.register(Gender)
admin.site.register(Tag)
admin.site.register(Format)
