from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.utils import *

from mnews.models import *
from mnews.tasks import CallAndPlayTask

from app_manager.common import get_queue_for_task
from app_manager.models import Cdr_trigger

from datetime import datetime


class ForwardingController(BaseVappController):
    
    def __init__(self, ai, sessionData, vi_data ):
        
        super(ForwardingController, self).__init__( ai, sessionData, vi_data )        
        self.max_tries = vi_data['max_tries']
        self.forwarding_log = Forwarding_log.objects.get(id = vi_data['forwarding_log_id'])
        self.forwarding_ai_properties = Forwarding_ai_properties.objects.get(id = vi_data['forwarding_properties_id'])
        self.call_log = vi_data['group_call_log']       
        self.call_log.tries += 1
        self.call_log.save()        
        self.vi_data = vi_data  


    def pre_outgoingstart(self):
        call_params_dict = self.getStartCallParams( self.sessionData.callerid, trigger = Cdr_trigger.CALLANDPLAY )
        self.call_log.last_cdr = Cdr.objects.get(pk = call_params_dict['cdr_id'])
        self.call_log.save()
        return call_params_dict

    def while_outgoingstart__sm_action_failure(self, events, eventData):
        if self.call_log.tries < self.max_tries:
            time = datetime.now() + timedelta(minutes = 30)
            queue = get_queue_for_task(self.ai, default_queue='push')
            kwargs = {'vi_data': RestrictedDict.convert_dict_to_rdict(self.vi_data, filter=True)}
            CallAndPlayTask.apply_async(args=[self.call_log.id, self.forwarding_ai_properties.max_tries],
                                        kwargs=kwargs, queue=queue, eta=time)
        return 'sm_action_failure'
    
    def while_outgoingstart__sm_action_success(self, events, eventData):
        self.call_log.success = True
        self.call_log.last_cdr = self.sessionData.cdrs[0]
        self.call_log.save()  
        if self.forwarding_ai_properties.forwarding_prompt_start:      
            return 'forward_play_start_prompt'        
    
    def while_outgoingstart__sm_next_originate_url(self, events, eventData):
        logger.debug('outgoing sm_next_originate_url, gcl_id: %s, tries:%s, max_tries:%s'%(self.call_log.id, self.call_log
.tries, self.max_tries))
        logger.info('Inside while outgoing sm next originate url')
        if not self.call_log.should_dial( int( self.max_tries ) ):
            logger.debug('CallAndPlayController sm_next_originate_url should_dial failed for gcl_id %s'%(self.call_log.id)
)
            return 'sm_action_failure'

        hangup_cause = eventData.get('Hangup_Cause', '')
            

       
        logger.debug('hangup_cause not in normal uns nor user busy')
        self.call_log.tries += 1
        self.call_log.save()

        return 'sm_next_originate_url'
       

    def pre_playstartprompt(self, prevState):        
        return self.getPlaybackParams( self.forwarding_ai_properties.get_start_prompt_file() )

    def pre_playsmessage(self, prevState):
        return self.getPlaybackParams( self.forwarding_log.get_personal_message_file() )
        
    def pre_playendprompt(self, prevState):
        return self.getPlaybackParams( self.forwarding_ai_properties.get_end_prompt_file() )
    
    def pre_playforwardeditem(self, prevState):
        return self.getPlaybackParams( self.forwarding_log.get_forwarded_item_file() )
    
    def while_playforwardeditem__sm_action_success(self, events, eventData):
        if self.forwarding_ai_properties.forwarding_schedule.prompt_file:
            return 'forward_pre_jump_prompt'
        else:
            return 'forward_jump_ai'
    
    def pre_prejumpprompt(self, prevState):
        return self.getPlaybackParams( self.forwarding_ai_properties.forwarding_schedule.get_prompt_file() )

    def while_jump__sm_action_success(self, events, eventData):
        return '@ai_' + str(self.forwarding_ai_properties.ai_to_jump.id) + '_welcome'

ForwardingStateDescriptionMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'playstartprompt':['forward_play_start_prompt', 'sm_action_success'],            
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name': 'playstartprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'playsmessage': ['sm_action_success'],
            },
        },
    {   'name': 'playsmessage',
        'action': 'playback',
        'transitions': {
            
            'playendprompt': ['sm_action_success', 'sm_action_failure'],
            },
        },
    {   'name': 'playendprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'playforwardeditem': ['sm_action_success'],
            },
        },
    
        {   'name': 'playforwardeditem',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'prejumpprompt': ['forward_pre_jump_prompt', 'sm_action_success'],
            'jump': ['forward_jump_ai'],
            },
        },
    {   'name': 'prejumpprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'jump': ['sm_action_success'],
            },
        },
    
        {   'name': 'jump',
        'action': 'none',
        'transitions' : {}
        },
    
    {   'name':'stop', 
        'action':'hangup', 
        'transitions': {}
        },
    ]
