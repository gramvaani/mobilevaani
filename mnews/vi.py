from vapp.app_manager.models import App_instance, VI_conf, Embedded_ai_map, Ai_translation_method, Ai_translator_api_cred, Call_response_type
from vapp.app_manager.common import load_module, get_queue_for_task
from vapp.mnews.models import *
from mnews.tasks import *
from vapp.telephony.statemachine import BaseVappController
from vapp.telephony.utils import *
from vapp.utils import *
from sms.tasks import SMSTask
from advert.utils import get_ads_for_pos, log_impression, get_target_reference_for_ad, log_ad_lead
from advert.utils import send_ad_message, is_ad_skippable, log_partial_impression

from django.core.exceptions import ObjectDoesNotExist
 
from datetime import datetime, timedelta
import stat

from vapp.log import get_logger
logger = get_logger()


class MnewsController( BaseVappController ):
    MIN_RECORDING_DUR = 25*1000

    @classmethod
    def shouldSaveItem(cls, news, min_duration, detail_duration = None, summary_duration = None):
        if not news:
            return False

        if news.detail and not detail_duration:
            detail_duration = mp3_duration(news.detail.get_full_filename())
        elif news.summary and not summary_duration:
            summary_duration = mp3_duration(news.summary.get_full_filename())

        if  ((not news.summary and not news.detail) or
            ((not news.summary) and news.detail and detail_duration < min_duration) or
            ((not news.detail) and news.summary and summary_duration < min_duration)):
            logger.info('News ought to be deleted:' + str(news))
            return False
        else:
            return True

    @classmethod
    def verifyAndSaveItem(cls, item, min_duration = MIN_RECORDING_DUR, detail_duration = None, summary_duration = None, modify_recording_attribs = True):
        if cls.shouldSaveItem(item, min_duration, detail_duration, summary_duration):
            item.modified_date = datetime.now()
            item.save()
            if item.is_comment:
                parent = News.objects.get(pk = item.parent_id_value)
                parent.comments.add( item )
            return True
        else:
            item.state = 'REJ'
            item.tags = item.tags or ''

            if item.tags:
                tags_list = [tag.strip() for tag in item.tags.split(',')]
                tags_list.append('autorej')
                item.tags = ','.join(tags_list)
            else:
                 item.tags = 'autorej'
            item.save()
            return False

    @classmethod
    def scheduleForwardingCall(cls, forwarding_log):

        vi_conf = VI_conf.objects.get(controller = 'mnews.forwardingvi.ForwardingController',
                                            description = 'mnews.forwardingvi.ForwardingStateDescriptionMap')

        properties = Forwarding_ai_properties.objects.get( ai = forwarding_log.ai )
        vi_data = RestrictedDict.convert_dict_to_rdict({ 'forwarding_log_id': forwarding_log.id, 'forwarding_properties_id': properties.id, 'vi_conf_id': vi_conf.id })
        ai = properties.ai_to_jump
        group_call_log = Groups_call_log(number = forwarding_log.forwarded_to, group_schedule = properties.forwarding_schedule, ai = ai)
        group_call_log.save()

        forwarding_gc_log = Forwarding_group_call_log(forwarding_log = forwarding_log, group_call_log = group_call_log )
        forwarding_gc_log.save()
        queue = get_queue_for_task(ai, default_queue='forwarding')
        CallAndPlayTask.apply_async(args=[group_call_log.id, properties.max_tries], 
                                    kwargs={'vi_data': vi_data}, eta=datetime.now(), queue=queue)


    @classmethod
    def getInlineAdPosition(cls, last_news_pos, channel):
        return "%s__MNEWS_INLINE_%s_%s" % (channel.name, last_news_pos, last_news_pos + 1)

    @classmethod
    def autoPublish(cls, ai, callerid):
        try:
            if ai.news_settings.autopublish:
                return True

            for cl in ai.news_settings.autopublish_lists.all():
                if callerid in cl.get_numbers():
                    return True
            return False
        except:
            return False

    @classmethod
    def validateRecording(cls, ai, callerid):
        try:
            return (ai.news_settings and ai.news_settings.validate_contrib) or cls.autoPublish(ai, callerid)
        except:
            return False

    @classmethod
    def playContribAnnounce(cls, ai):
        try:
            if ai.news_settings.play_contrib_announce:
                return True
            return False
        except:
            return False

    def populateEmbeddedAiMap( self ):
        embedded_maps = Embedded_ai_map.objects.filter(vi_ref__ai = self.ai, active = True).order_by('priority')

        for embedded_map in embedded_maps:
            if embedded_map.position not in self.embeddedStateMap:
                self.embeddedStateMap[ embedded_map.position ] = []
            self.embeddedStateMap[embedded_map.position].append( embedded_map )

    def getEmbedRefForPosition(self, position):
        try:
            embedded_map = self.embeddedStateMap[ position ].pop()
            if embedded_map.should_embed_func:
                should_embed = getattr(self, embedded_map.should_embed_func)
                if should_embed(embedded_map):
                    return '@' + embedded_map.vi_ref.reference
                else:
                    return

            return '@' + embedded_map.vi_ref.reference

        except KeyError:
            pass
        except TypeError:
            pass
        except IndexError:
            pass

    @classmethod
    def getEmbedRefForNews(cls, news, ref_key_pressed, dtmf = None):
        if dtmf == '1':
            return
        else:
            try:
                if not ref_key_pressed:
                    reference = News_vi_reference.objects.get(news = news, keypress_required = False).vi_reference.reference
                else:
                    reference = News_vi_reference.objects.get(news = news).vi_reference.reference
                return '@' + reference
            except:
                pass

    @classmethod
    def getSelectionAlgo(cls, ai, callerid):
        try:
            config = AB_testing_config.objects.get(ai = ai, active = True)
            algo_logs = Selection_algo_callerid.objects.filter(config = config, callerid = callerid)
            if algo_logs.exists():
                return algo_logs[0].algo
            else:
                algos = list(config.algos.all())
                selected_algo = random.choice(algos)
                Selection_algo_callerid(config = config, callerid = callerid, algo = selected_algo).save()
                return selected_algo
        except:
            return None

    @classmethod
    def getDefaultMainChannelNews_v1(cls, callerid, ai, channel, count):
        return list(News.objects.filter(ai = ai, channel = channel, state = News_state.PUB, is_comment = False).order_by('pub_order')[:count])

    @classmethod
    def getBestRatingNews_v1(cls, callerid, ai, channel, count):
        news = list(News.objects.filter(ai = ai, state = News_state.PUB, rating__gte = 4, is_comment = False).order_by('-time')[:2*count])
        if len(news) > count:
            return random.sample(news, count)
        else:
            return news

    @classmethod
    def calledInLastMonth(cls, ai, callerid):
        start = datetime.now() - timedelta(days = 30)
        calls = Cdr.objects.filter(ai = ai, callerid = callerid, start_time__gte = start, answered_time__isnull = False)[0:2]
        return (len(calls) == 2)

    @classmethod
    def checkViRefEntry(self, viRef, reference):
        try:
            return VI_reference.objects.get(vi=viRef, reference=reference)
        except Exception as e:
            logger.info(e)
            return None

    @classmethod
    def checkViRef(self, viRef, reference):
        try:
            return VI_reference.objects.filter(vi=viRef, reference__startswith= reference)
        except Exception as e:
            logger.info(e)
            return None   

    @classmethod
    def getViRefDefaultEntry(self, viRef, reference):
        try:
            return VI_reference.objects.filter(vi=viRef, reference=reference)
        except Exception as e:
            logger.info(e)
            return None            

    def getTranslatedTitle( self, ai, item, translation_map, creds ):
        title = None
        translation_module = load_module( ai, 'translation' )
        signature = translation_map.translation_method.signature
        if hasattr( translation_module, signature ):
            try:
                translation_method = getattr( translation_module, signature )
                title = translation_method( creds, item )
            except Exception,e:
                logger.exception( "exception while calling translation method: %s. error: %s" % ( signature, e ) )
        else:
            logger.debug( "translation method %s not found for map: %s." % ( signature, translation_map ) )

        return title

    def minTwoCallsLastWeek(self, embedded_map):
        start = datetime.now() - timedelta(days = 7)
        cdrs = Cdr.objects.filter(ai = self.ai, callerid = self.sessionData.callerid, start_time__gte = start, answered_time__isnull = False)[:2]
        return (len(cdrs) >= 2)

    def getNewsForSignal(self, signal, count):
        func = getattr(MnewsController, signal.function)
        if func:
            return func(self.sessionData.callerid, self.ai, self.currentChannel, count)
        else:
            return []

    def getWeightedNews(self, signal_weight):
        news = self.getNewsForSignal(signal_weight.signal, self.currentChannel.max_news)
        return news[:int(self.currentChannel.max_news * signal_weight.weight)]

    def getNewsForSelectionAlgo(self, selectionAlgo):
        return reduce(list.__add__, map(self.getWeightedNews, list(selectionAlgo.signal_weights.all())), [])

    def smsOnItemForwarding(self, callerid):
        try:
            params = {}
            params['destination'] = callerid
            template = Event_sms_template.objects.get(template__ai = self.ai, event = CallEvents.ITEM_FORWARDED).template
            message = template.process_template(params)
            sms = SMSTask.create_send_msg(self.ai.id, message, callerid)
            return sms
        except Exception, e:
            logger.exception("Error in smsOnItemForwarding %s" % str(e))

    def initNews(self):
        if self.currentChannel.name == 'Main':
            self.selectionAlgo = MnewsController.getSelectionAlgo(self.ai, self.sessionData.callerid)
            if self.selectionAlgo:
                self.selectionAlgoStr = self.selectionAlgo.get_long_str()
                return self.getNewsForSelectionAlgo(self.selectionAlgo)

            try:
                if self.ai.news_settings.best_items_to_new_callers and not MnewsController.calledInLastMonth(self.ai, self.sessionData.callerid):
                    return MnewsController.getBestRatingNews_v1(self.sessionData.callerid, self.ai, self.currentChannel, self.currentChannel.max_news)
            except ObjectDoesNotExist:
                pass

	self.newsList = self.sessionData.app_data['virtual_mnews'].get(self.ai.id)

        if self.newsList:
	    news_objects = News.objects.filter(id__in = self.newsList)[:self.currentChannel.max_news]
	    news_objects = dict([(obj.id, obj) for obj in news_objects])
	    sorted_news_objects = [news_objects[id] for id in self.newsList]
            return sorted_news_objects
        else:
            return News.objects.filter(ai = self.ai.id, channel = self.currentChannel, state = News_state.PUB, is_comment = False, detail__isnull = False).order_by('pub_order')[:self.currentChannel.max_news]

    def initNewsAndComments(self):
        self.news = self.initNews()
        self.comments = {}
        for news in self.news:
            self.comments[ news ] = news.comments.filter( state = News_state.PUB, detail__isnull = False )
        self.currentNews = 0
        self.newNews = None
        self.newComment = None        
        self.itemToValidate = None


    def initChannelAds(self):
        self.currentChannelAd = -1
        try:
            self.channelAds = get_ads_for_pos(self.ai, '%s__MNEWS_CHANNEL' % self.currentChannel.name, self.sessionData.callerid)
        except:
            pass

    def logViTransitionData(self, transition_event):
        if self.currentChannel:
            data = Vi_transition_data(transition_event = transition_event, channel = self.currentChannel, selection_algo = self.selectionAlgoStr)
            data.save()

    def __init__(self, ai, sessionData, vi_data = {}):
        super(MnewsController, self).__init__( ai, sessionData, vi_data )
        logger.info("dibyendu: mnews vi init start")
        if 'channel' in self.vi_data and isinstance(self.vi_data['channel'], Channel):
            self.currentChannel = self.vi_data['channel']
        elif 'channel_id' in self.vi_data:
            self.currentChannel = Channel.objects.get(pk = self.vi_data['channel_id'])
        else:
            logger.info("dibyendu: mnews vi init else")
            # if ai has a sticky channel with non zero number of news items:
            # set current channel to be the sticky channel
            sticky_channel = Channel.get_sticky_channel(ai.id)
            logger.info("dibyendu: sticky channel found")
            if sticky_channel:
                logger.info("dibyendu: sticky channel set as default channel")
                self.currentChannel = sticky_channel
            else:
                self.currentChannel = Channel.get_default_channel(ai.id)
        logger.info("dibyendu: mnews vi init currentChannel set"+str(self.sessionData.app_data))
	
        self.newsList = []

        self.vi_data = {}

        self.networkAds = self.channelAds = self.inlineAds = None
        self.embeddedStateMap = {}
        self.populateEmbeddedAiMap()
        logger.info("dibyendu: mnews vi init populateEmbeddedAiMap done")

        self.selectionAlgoStr = ''

        self.tm_map = None
        self.translation_api_cred = None
        self.skip_enabled = False

        self.item_heard = {}
        self.currentPlayingBookmarkedItem = None

        self.tm_maps = Ai_translation_method.objects.filter( ai = self.ai )
        if self.tm_maps.exists():
            self.tm_map = self.tm_maps[ 0 ]
        logger.info("dibyendu: mnews vi init tm_map done")

        self.translation_api_creds = Ai_translator_api_cred.objects.filter( ai = self.ai )
        if self.translation_api_creds.exists():
            self.translation_api_cred = self.translation_api_creds[ 0 ]
        logger.info("dibyendu: mnews vi init translation_api_cred done")

        #temporary hack for sending SMS message on call
        message = ModerationEventRecorder.get_message_for_event(self.ai, ModerationEvents.PLAY_MNEWS_WELCOME_START)
        logger.info("dibyendu: mnews vi init message done")
        if message:
            SMSTask.create_send_msg(self.ai.id, message, self.sessionData.cdrs[0].callerid, max_tries = 3)
        logger.info("dibyendu: mnews vi init end")

    def getPromptForCurrentChannel(self, prompt):
        return prompt + '__' + self.currentChannel.name

    def pre_outgoingstart(self):
        return self.getStartCallParams( self.sessionData.callerid )

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def pre_welcome(self):
        if self.vi_data.has_key('channel') and isinstance(self.vi_data['channel'], Channel):
            self.currentChannel = self.vi_data['channel']
        elif self.vi_data.has_key('channel_id'):
            self.currentChannel = Channel.objects.get(pk = self.vi_data['channel_id'])

        self.vi_data = {}
        self.initNewsAndComments()

        max_dur = self.ai.app_instance_settings_set.all()[0].max_duration
        self.min_recording_duration = ( self.ai.app_instance_settings_set.all()[0].min_recording_duration*1000 )

        self.ai_timeout = datetime.now() + timedelta(seconds = max_dur) if max_dur else None

        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_welcome') )

    def while_welcome__sm_action_success(self, events, eventData):
        transition_string = self.getEmbedRefForPosition('MNEWS_NETWORK')
        if transition_string:
            self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'checknetworkads') )
            return transition_string

    def while_welcome__dtmf_1(self, events, eventData):
        return self.while_welcome__sm_action_success(events, eventData)

    def pre_thankyou(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_thank_you') )

    def while_thankyou__sm_action_success(self, events, eventData):
        #for qna purpose specifically
        if self.newsList and len(self.sessionData.embed_stack) > 0:
            transition_string = self.sessionData.popEmbedStack()
            return transition_string    

    def pre_instructions(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_instructions') )

    def set_item_heard_start(self, news):
        self.item_heard['item'] = news
        self.item_heard['start'] = datetime.now()
        self.item_heard['cdr'] = self.sessionData.cdrs[0]

    # Item heard end gets set while sm_action_success, sm_action_failure, sm_ai_time_out,
    # pressing DTMF while one item getting played. It does not set end for DMTF pressing
    # in welcome/instructions playing. It skips appending of one item with same start time.
    def set_item_heard_end(self, event=None):
        if self.item_heard:
            items_heard_info = self.sessionData.app_data['mnews']['items_heard_info']
            item_heard_info_exists = False
            for item_heard_info in items_heard_info:
                if item_heard_info['item'] == self.item_heard['item'] and item_heard_info['start'] == self.item_heard['start']:
                    item_heard_info_exists = True
                    break
            if not item_heard_info_exists:
                self.item_heard['end'] = datetime.now()
                self.item_heard['event'] = event
                self.sessionData.app_data['mnews']['items_heard_info'].append(self.item_heard)
                self.item_heard = {}

    def pre_listnews(self, prevState, event):
        self.currentPlayingNews = self.news[self.currentNews]
        return self.getPlaybackParams(self.currentPlayingNews.detail.get_full_filename())

    def while_listnews__fs_playback_start(self, events, eventData):
        self.set_item_heard_start(self.currentPlayingNews)
        return None

    def while_listcomments__fs_playback_start(self, events, eventData):
        self.set_item_heard_start(self.comment)
        return None

    def while_nextnews__sm_action_success(self, events, eventData):
        return self.nextNewsEvent()

    def nextNewsEvent(self):
        if self.ai_timeout and self.ai_timeout <= datetime.now():
            return 'sm_ai_time_out'

        if self.currentNews < len(self.news) - 1:
            self.currentNews += 1
            return 'mnews_next_news'
        elif MnewsController.playContribAnnounce(self.ai):
            return 'mnews_news_done'
        else:
            return 'mnews_news_done_no_contrib_announce'

    def getInlineEmbedOrAdEvent(self):
        position = self.getInlineAdPosition(self.currentNews, self.currentChannel)
        transition_string = self.getEmbedRefForPosition(position)
        if transition_string:
            self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'checkinlineads') )
            return transition_string
        else:
            return 'advert_play_inline_ads'

    def nextCommentOrAdEvent(self):
        if self.ai_timeout and self.ai_timeout <= datetime.now():
            return 'sm_ai_time_out'

        if self.currentComment < self.comments[ self.news[ self.currentNews ] ].count() - 1:
            self.currentComment += 1
            return 'mnews_next_comment'

        return self.getInlineEmbedOrAdEvent()

    def prevNewsEvent(self):
        if self.currentNews > 0:
            self.currentNews -= 1
        else:
            self.currentNews = 0
        return 'mnews_prev_news'

    def is_forwarding_enabled( self ):
        return Forwarding_ai_properties.objects.filter( ai = self.ai ).exists() and \
            Forwarding_ai_properties.objects.get( ai = self.ai ).is_enabled

    def while_listnews__dtmf_4(self, events, eventData):
        self.set_item_heard_end(event='dtmf_4')
        return self.prevNewsEvent()

    def while_listcomments__dtmf_4(self, events, eventData):
        self.set_item_heard_end(event='dtmf_4')
        return self.prevNewsEvent()

    def while_listcomments__dtmf_5(self, events, eventData):
        self.set_item_heard_end(event='dtmf_5')
        if self.is_forwarding_enabled():
            return 'mnews_bookmark_item'
        return 'mnews_next_comment'

    def while_listnews__dtmf_5(self, events, eventData):
        self.set_item_heard_end(event='dtmf_5')
        if self.is_forwarding_enabled():
            return 'mnews_news_bookmarked'
        return 'mnews_prev_news'

    def while_listcomments__dtmf_6(self, events, eventData):
        self.set_item_heard_end(event='dtmf_6')
        if self.is_forwarding_enabled():
            return 'mnews_play_bookmarked_item'
        return 'mnews_next_comment'

    def while_listnews__dtmf_6(self, events, eventData):
        self.set_item_heard_end(event='dtmf_6')
        if self.is_forwarding_enabled():
            return 'mnews_play_bookmarked_item'
        return 'mnews_prev_news'

    def while_listnews__sm_action_success(self, events, eventData, event='completed'):
        self.set_item_heard_end(event=event)
        if self.skip_enabled:
            transition_string = MnewsController.getEmbedRefForNews(self.news[ self.currentNews ], False, dtmf = '1')
            self.skip_enabled = False
        else:
            transition_string = MnewsController.getEmbedRefForNews(self.news[ self.currentNews ], False)
        if transition_string:
            self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'checkcomments') )
            return transition_string

    def while_listnews__dtmf_1(self, events, eventData):
        self.skip_enabled = True
        return self.while_listnews__sm_action_success(events, eventData, event='dtmf_1')

    def while_listcomments__dtmf_8(self, events, eventData):
        self.vi_ref_next_ai = self.checkViRef(App_instance_settings.objects.get(ai_id=self.ai.id).vi_conf_id,'jump_8_dtmf_')
        if self.vi_ref_next_ai.exists():
            self.set_item_heard_end(event='dtmf_8')
            return 'mnews_jump_ai'


    def while_listcomments__dtmf_1(self, events, eventData):
        self.set_item_heard_end(event='dtmf_1')
        return None
    
    def while_listnews__dtmf_9(self, events, eventData):
        if News_vi_reference.objects.filter(news=self.news[self.currentNews]).exists():
            self.set_item_heard_end(event='dtmf_9')
        elif (self.checkViRefEntry(App_instance_settings.objects.get(ai_id=self.ai.id).vi_conf_id, 'global_ai_jump')):
            self.set_item_heard_end(event='dtmf_9')
            self.currentChannel = Channel.get_default_channel(self.ai.id)
            return '@global_ai_jump'
        transition_string = MnewsController.getEmbedRefForNews(self.news[ self.currentNews ], True, eventData['DTMF_Digit'])
        if transition_string:
            self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'checkcomments') )
            return transition_string

    def while_listnews__dtmf_8(self, events, eventData):
        logger.info('Inside while of dtmf 8')
        self.vi_ref_next_ai = self.checkViRef(App_instance_settings.objects.get(ai_id=self.ai.id).vi_conf_id, 'jump_8_dtmf_')
        if self.vi_ref_next_ai.exists():
            self.set_item_heard_end(event='dtmf_8')
            return 'mnews_jump_ai'

    def pre_selectnextai(self):
        return self.getPromptPlayAndGetParams(promptName='mnews_jump_8_blank_audio', timeout=3000, tries=1, minDigits=0,  maxDigits=0, regularExp=True)


    def while_selectnextai__sm_get_digits_no_digits(self, events, eventData):
        default_vi_ref = self.getViRefDefaultEntry(App_instance_settings.objects.get(ai_id=self.ai.id).vi_conf_id,'jump_8_default')
        if default_vi_ref.exists():
           transition_string = default_vi_ref[0].reference
           if transition_string:
                logger.info('Inside while select nextai no digit')
                logger.info(transition_string)
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'nextnews') )
                return '@'+transition_string
        return 'mnews_invalid_choice'

    def while_selectnextai__sm_action_success(self, event, eventData):
        digit = self.getPlayAndGetDigits(eventData)
        logger.info(str(digit))
        transition_string = 'jump_8_dtmf_'+str(digit)

        vi_ref = self.vi_ref_next_ai.filter(reference = transition_string)
        if vi_ref.exists():
            logger.info('Inside viref')
            self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'nextnews') )
            logger.info(transition_string)
            return '@'+transition_string

        return 'mnews_invalid_choice'

    def pre_playinvalidprompt(self):
        return self.getPromptParams(promptName='mnews_dtmf_8_invalid_choice')


    def while_listcomments__dtmf_9(self, events, eventData):
    	if (self.checkViRefEntry(App_instance_settings.objects.get(ai_id=self.ai.id).vi_conf_id, 'global_ai_jump')):
            self.set_item_heard_end(event='dtmf_9')
            self.currentChannel = Channel.get_default_channel(self.ai.id)
            return '@global_ai_jump'

    def while_listnews__sm_action_failure(self, events, eventData):
        self.set_item_heard_end(event='hangup')

    def while_listcomments__sm_action_failure(self, events, eventData):
        self.set_item_heard_end(event='hangup')

    def while_playmarkeditem__sm_action_failure(self, events, eventData):
        self.set_item_heard_end(event='hangup')

    def while_listnews__sm_ai_time_out(self, events, eventData):
        self.set_item_heard_end(event='hangup')

    def while_listcomments__sm_ai_time_out(self, events, eventData):
        self.set_item_heard_end(event='hangup')

    def while_playmarkeditem__sm_ai_time_out(self, events, eventData):
        self.set_item_heard_end(event='hangup')

    def while_checkcomments__sm_action_success(self, events, eventData):
        current_news = self.news[ self.currentNews ]

        if self.comments[ current_news ]:
            self.currentComment = 0
            return 'mnews_start_comments'

        return self.getInlineEmbedOrAdEvent()

    def pre_listcomments(self):
        self.comment = self.comments[ self.news[ self.currentNews ] ][ self.currentComment ]
        return self.getPlaybackParams(self.comment.detail.get_full_filename())
    
    def while_listcomments__sm_action_success(self, events, eventData):
        self.set_item_heard_end(event='completed')
        return self.nextCommentOrAdEvent()
        
    def pre_recordcommentprompt(self, prevState):
        self.set_item_heard_end(event='dtmf_2')
        if prevState not in ['recordinginvalidated']:
            self.prevRecordCommentState = prevState 
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_leave_comment') )

    def getAiForContribSave(self):
        if self.save_to_news_ai.news_settings.contrib_elsewhere_policy == 'ANCIENT':
            ai = self.sessionData.getAncientAiEmbedStack()
            return ai
        return None

    def pre_recordcommentrecord(self):
        parent = self.news[ self.currentNews ]
        item_channel = parent.channel
        self.save_to_news_ai = self.ai
        title = 'Comment, %s' % datetime.now().strftime('%H:%M, %d %B %Y')
        is_comment = True
        tags = None
        news_settings = News_settings.objects.filter(ai = self.ai)

        if news_settings.exists() and news_settings[0].is_contrib_elsewhere:
            news_ai = self.getAiForContribSave()
            if news_ai:
                self.save_to_news_ai = news_ai
                item_channel = Channel.get_default_channel(self.save_to_news_ai.id)
                title = 'News, %s' % datetime.now().strftime('%H:%M, %d %B %Y')
                is_comment = False
                tags = news_settings[0].contrib_elsewhere_tags

        self.newComment = News(ai_id=self.save_to_news_ai.id, channel=item_channel,
                               callerid=self.sessionData.callerid,
                               state=News_state.UNM, is_comment=is_comment,
                               source=News.Source.VOICE_INTERFACE, title=title, creation_cdr=self.sessionData.cdrs[0])
        if is_comment:
            self.newComment.parent_id_value = parent.id

        if tags:
            self.newComment.tags = tags


        return self.getEmbeddedRecordingParams( self.save_to_news_ai.record_duration_limit_seconds, self.newComment, News.detail, ai = self.save_to_news_ai.id )


    def while_recordcommentrecord__sm_action_success(self, events, eventData):
        if MnewsController.validateRecording(self.save_to_news_ai, self.sessionData.callerid):
            self.itemToValidate = self.newComment
            return 'mnews_validate_recording'
        else:
            MnewsController.verifyAndSaveItem(self.newComment, self.min_recording_duration)

    def while_recordcommentrecord__sm_action_failure(self, events, eventData):
        MnewsController.verifyAndSaveItem(self.newComment, self.min_recording_duration)

    def post_recordcommentrecord(self):
        self.newComment = None

    def while_recordcommentthankyou__sm_action_success(self, events, eventData):
        if self.prevRecordCommentState == 'listcomments':
            return 'mnews_continue_comments'
        else:
            return 'mnews_continue_news'

    def pre_recordcommentthankyou(self):
        return self.getPromptParams( self.getPromptForCurrentChannel( 'mnews_ack_comment' ) )

    def pre_contributeannounce(self):
        return self.getPromptPlayAndGetParams( self.getPromptForCurrentChannel('mnews_contrib_announce') )

    def pre_contributeprompt(self, prevState):
        self.set_item_heard_end(event='dtmf_3')
        if prevState not in ['recordinginvalidated']:
            self.prevRecordNewsState = prevState
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_contrib_record') )

    def pre_contributerecord(self):
        title = 'News, %s' % datetime.now().strftime('%H:%M, %d %B %Y')
        self.save_to_news_ai = self.ai

        if self.currentChannel.sticky:
            item_channel = Channel.get_default_channel(self.save_to_news_ai.id)
        else:
            item_channel = self.currentChannel

        tags = None
        news_settings = News_settings.objects.filter(ai = self.ai)


        #check for mnews-mnews jump configuration in order to save the item in ancient mnews instance
        if news_settings.exists() and news_settings[0].is_contrib_elsewhere:
            news_ai = self.getAiForContribSave()
            if news_ai:
                self.save_to_news_ai = news_ai
                item_channel = Channel.get_default_channel(self.save_to_news_ai.id)
                tags = news_settings[0].contrib_elsewhere_tags

        self.newNews = News(ai_id=self.save_to_news_ai.id, channel=item_channel,
                            callerid=self.sessionData.callerid,
                            state=News_state.UNM, is_comment=False,
                            source=News.Source.VOICE_INTERFACE, title=title, creation_cdr=self.sessionData.cdrs[0])
        if tags:
            self.newNews.tags = tags

        return self.getEmbeddedRecordingParams( self.save_to_news_ai.record_duration_limit_seconds, self.newNews, News.detail, ai = self.save_to_news_ai.id )

    def pre_validateannounce(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_validate_announce') )

    def while_validateannounce__sm_action_failure(self, events, eventData):
        MnewsController.verifyAndSaveItem(self.itemToValidate, min_duration = self.min_recording_duration)

    def pre_playrecording(self):
        return self.getPlayAndGetParams( self.itemToValidate.detail.get_full_filename() )

    def while_playrecording__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if digits == 1:
            MnewsController.verifyAndSaveItem(self.itemToValidate, min_duration = self.min_recording_duration)
            return 'mnews_recording_good'
        elif digits == 2:
            return 'mnews_recording_bad'
        elif digits == 3:
            return 'mnews_recording_cancelled'
        else:
            return 'mnews_invalid_choice'
    
    def while_playrecording__sm_get_digits_no_digits(self, events, eventData):
        return 'mnews_move_validatechoice'

    def while_playrecording__sm_action_failure(self, events, eventData):
        MnewsController.verifyAndSaveItem(self.itemToValidate, min_duration = self.min_recording_duration)

    def pre_validatechoice(self):
        return self.getPromptPlayAndGetParams( self.getPromptForCurrentChannel('mnews_validate_choice'), tries = 3 )

    def while_validatechoice__sm_get_digits_no_digits(self, events, eventData):
        return 'mnews_no_choice'

    def while_validatechoice__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if digits == 1:
            return 'mnews_recording_good'
        elif digits == 2:
            return 'mnews_recording_bad'
        elif digits == 3:
            return 'mnews_recording_cancelled'
        else:
            return 'mnews_invalid_choice'

    def while_validatechoice__sm_action_failure(self, events, eventData):
        MnewsController.verifyAndSaveItem(self.itemToValidate, min_duration = self.min_recording_duration)

    def while_contributerecord__sm_action_success(self, events, eventData):
        if MnewsController.validateRecording(self.save_to_news_ai, self.sessionData.callerid):
            self.itemToValidate = self.newNews
            return 'mnews_validate_recording'
        else:
            detail_duration = self.vi_data.get('detailDuration')
            modify_recording_attribs = self.vi_data.get('modifyRecordingAttribs')
            MnewsController.verifyAndSaveItem(self.newNews, min_duration = self.min_recording_duration, detail_duration = detail_duration, modify_recording_attribs = modify_recording_attribs)

    def while_contributerecord__sm_action_failure(self, events, eventData):
        detail_duration = self.vi_data.get('detailDuration')
        modify_recording_attribs = self.vi_data.get('modifyRecordingAttribs')
        MnewsController.verifyAndSaveItem(self.newNews, min_duration = self.min_recording_duration, detail_duration = detail_duration, modify_recording_attribs = modify_recording_attribs)

    def while_recordinginvalidated__sm_action_success(self, events, eventData):
        event = 'mnews_recording_is_comment' if self.itemToValidate.is_comment else 'mnews_recording_is_news'
        #item is not getting deleted but getting saved with autorej tag and state- REJ
        self.itemToValidate.state = 'REJ'
        self.itemToValidate.tags = self.itemToValidate.tags or ''

        if self.itemToValidate.tags:
            tags_list = [tag.strip() for tag in self.itemToValidate.tags.split(',')]
            tags_list.append('autorej')
            self.itemToValidate.tags = ','.join(tags_list)
        else:
             self.itemToValidate.tags = 'autorej'
        self.itemToValidate.save()
        #self.itemToValidate.delete()
        return event

    def while_recordingcancelled__sm_action_success(self, events, eventData):
        self.itemToValidate.state = 'REJ'
        self.itemToValidate.tags = self.itemToValidate.tags or ''

        if self.itemToValidate.tags:
            tags_list = [tag.strip() for tag in self.itemToValidate.tags.split(',')]
            tags_list.append('autorej')
            self.itemToValidate.tags = ','.join(tags_list)
        else:
             self.itemToValidate.tags = 'autorej'
        self.itemToValidate.save()
        #self.itemToValidate.delete()
        return 'sm_action_success'

    def while_recordingvalidated__sm_action_success(self, events, eventData):
        saved = MnewsController.verifyAndSaveItem(self.itemToValidate, min_duration = self.min_recording_duration)

        if saved and MnewsController.autoPublish(self.save_to_news_ai, self.sessionData.callerid):
            self.itemToValidate.state = News_state.PUB

            translated_title = None
            if self.tm_map and self.translation_api_cred:
                translated_title = self.getTranslatedTitle(self.save_to_news_ai, self.itemToValidate,
                                                           self.tm_map, self.translation_api_cred.cred )

            if translated_title:
                self.itemToValidate.title = translated_title
            else:
                self.itemToValidate.title += ' - Auto Published'

            self.itemToValidate.pub_order = 0
            self.itemToValidate.autopublished = True
            self.itemToValidate.tags = self.itemToValidate.tags or ''

            if self.itemToValidate.tags:
                tags_list = [tag.strip() for tag in self.itemToValidate.tags.split(',')]
                tags_list.append('autopub')
                self.itemToValidate.tags = ','.join(tags_list)
            else:
                self.itemToValidate.tags = 'autopub'

            self.itemToValidate.save()
            return 'mnews_autopublish'

        elif self.itemToValidate.is_comment:
            return 'mnews_comment_no_autopublish'
        else:
            return 'mnews_contrib_no_autopublish'

    def pre_contributethankyou(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_contrib_thank_you') )


    def while_contributethankyou__sm_action_success(self, events, eventData):
        if self.prevRecordNewsState in ('listnews', 'listcomments', 'instructions', 'welcome'):
            return 'mnews_continue_news'
        elif MnewsController.playContribAnnounce(self.ai):
            return 'sm_action_success'
        else:
            return 'sm_action_success_no_contrib_announce'


    def pre_recordingautopublished(self):
        return self.getPromptParams( self.getPromptForCurrentChannel('mnews_recording_autopublished') )


    def while_recordingautopublished__sm_action_success(self, events, eventData):
        if not self.itemToValidate.is_comment and self.prevRecordNewsState in ('listnews', \
                                                                   'listcomments', 'instructions', 'welcome'):
            return 'mnews_continue_news'
        elif self.itemToValidate.is_comment:
            if self.prevRecordCommentState == 'listcomments':
                return 'mnews_continue_comments'
            else:
                return 'mnews_continue_news'
        else:
            return 'sm_action_success'


    def while_nextchannel__sm_action_success(self, events, eventData):
        self.set_item_heard_end(event='dtmf_0')
        if self.currentChannel.sticky:
            next_channel = Channel.get_default_channel(self.ai.id)
        else:
            next_channel = Channel_order.get_next_channel(self.currentChannel, self.ai.id)
        if next_channel and next_channel.sticky:
            next_channel = Channel_order.get_next_channel(next_channel, self.ai.id)
        if next_channel:
            if next_channel.ai == self.ai:
                self.currentChannel = next_channel
                return 'mnews_next_channel'
            elif not filters_allow_ai_caller(next_channel.ai.id, self.sessionData.cdrs[0].callerid, self.sessionData.cdrs[0].id, Call_response_type.CALLBACK ):
                return 'mnews_no_channel_change'
            else:
                self.currentChannel = next_channel
                self.vi_data['channel'] = next_channel
                return '@ai_' + str(self.currentChannel.ai.id) + '_welcome'
        else:
            self.currentChannel = Channel.get_default_channel(self.ai.id)
        return '@nextai'


    def while_prevchannel__sm_action_success(self, events, eventData):
        self.set_item_heard_end(event='dtmf_*')
        if len(self.sessionData.embed_stack) > 0:
            transition_string = self.sessionData.popEmbedStack()
            return transition_string
        prev_channel = Channel_order.get_prev_channel(self.currentChannel, self.ai.id)
        if prev_channel and prev_channel.sticky:
            prev_channel = Channel_order.get_prev_channel(prev_channel, self.ai.id)
        if prev_channel:
            if prev_channel.ai == self.ai:
                self.currentChannel = prev_channel
                return 'mnews_prev_channel'
            elif not filters_allow_ai_caller(prev_channel.ai.id, self.sessionData.cdrs[0].callerid, self.sessionData.cdrs[0].id, Call_response_type.CALLBACK ):
                return 'mnews_no_channel_change'
            else:
                self.currentChannel = prev_channel
                self.vi_data['channel'] = prev_channel
                return '@ai_' + str(self.currentChannel.ai.id) + '_welcome'
        else:
            self.currentChannel = Channel.get_default_channel(self.ai.id)
        return '@prevai'

    def while_checknewsavailable__sm_action_success(self, events, eventData):
        if self.news:
            return 'mnews_continue_news'
        elif MnewsController.playContribAnnounce(self.ai):
            return 'mnews_no_news'
        else:
            return 'mnews_no_news_no_contrib_announce'

    def pre_postbookmarkprompt(self, prevState):
        self.prevBookmarkState = prevState
        if self.news[ self.currentNews ]:
            news = self.news[ self.currentNews ]
            news.bookmark_news_and_send_sms(self.sessionData.cdrs[0], 'ITB')
        return self.getPromptParams( 'mnews_bookmark_notify' )

    def pre_playbookmarkitem(self, prevState):
        self.prevBookmarkState = prevState
        return self.getPromptPlayAndGetParams( promptName = 'mnews_enter_bookmark', maxDigits = 9, tries = 3 )

    def while_playbookmarkitem__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if Bookmark.objects.filter(bookmark_id = digits, ai_id = self.ai.id).exists():
            self.news_bookmark_id = int(digits)
            return 'mnews_valid_bookmark'
        else:
            return 'mnews_invalid_bookmark'

    def pre_playmarkeditem(self):
        bookmark = Bookmark.objects.get(bookmark_id = self.news_bookmark_id, ai_id = self.ai.id)        
        self.currentPlayingBookmarkedItem = bookmark.news
        blog = Bookmark_log(bookmark = bookmark, cdr = self.sessionData.cdrs[0], accessed = True, bookmarked = False, source = 'BIA' )
        blog.save()
        return self.getPlaybackParams(self.currentPlayingBookmarkedItem.detail.get_full_filename())

    def while_playmarkeditem__fs_playback_start(self, events, eventData):
        self.set_item_heard_start(self.currentPlayingBookmarkedItem)
        return None

    def while_playmarkeditem__sm_action_success(self, events, eventData):
        self.set_item_heard_end(event='completed')
        if self.prevBookmarkState == 'listnews':
            return 'mnews_continue_news'
        elif self.prevBookmarkState == 'listcomments':
            return 'mnews_continue_comments'
        return 'sm_action_success'

    def while_playmarkeditem__dtmf_5( self, events, eventData ):
        self.set_item_heard_end(event='dtmf_5')
        if self.is_forwarding_enabled():
            return 'mnews_bookmark_item'
        return 'sm_action_success'

    def while_playmarkeditem__dtmf_6( self, events, eventData ):
        self.set_item_heard_end(event='dtmf_6')
        if self.is_forwarding_enabled():
            return 'mnews_continue_news'
        return 'sm_action_success'

    def while_playmarkeditem__dtmf_9(self, events, eventData):
        try:
            news = Bookmark.objects.get(bookmark_id = self.news_bookmark_id, ai_id = self.ai.id).news
            news_vi_ref_objs = News_vi_reference.objects.filter(news=news)
            if news_vi_ref_objs.exists():
                self.set_item_heard_end(event='dtmf_9')
                reference = news_vi_ref_objs[0].vi_reference.reference
                return '@' + reference
        except:
            pass

    def pre_invalidbookmark(self):
        return self.getPromptParams( 'mnews_invalid_bookmark' )
    
    def pre_forwarditemprompt(self, prevState):
        self.prevForwardState = prevState
        self.forwarding_properties = Forwarding_ai_properties.objects.get( ai = self.ai )
        return self.getPromptPlayAndGetParams( 'mnews_forward_prompt', timeout = 5000, tries = 1, minDigits = 1, maxDigits = 1 )

    def while_forwarditemprompt__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if int(digits) == 1:
            return 'mnews_forward_input'
        else:
            return 'mnews_no_forward_num'

    def pre_forwardcurrentitem(self, prevState):
        return self.getPromptPlayAndGetParams( 'mnews_forward_input_number', timeout = 10000, tries = 3, minDigits = 10, maxDigits = 10, errorPromptName = 'mnews_forward_invalid_number' )

    def while_forwardcurrentitem__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        if self.news[ self.currentNews ]:
            news = self.news[ self.currentNews ]
            forwarded_sms = self.smsOnItemForwarding(str(digits))
            if self.currentPlayingBookmarkedItem:
                news = self.currentPlayingBookmarkedItem
            forwarding_log = Forwarding_log(ai = self.ai, forwarding_cdr = self.sessionData.cdrs[0], forwarded_item = news, forwarded_to = str(digits), forwarding_event = "FFC", forwarded_sms = forwarded_sms)
            forwarding_log.save()
            self.currentPlayingBookmarkedItem = None
            self.forwarding_log = forwarding_log

    def pre_recordpmprompt(self, prevState):
        return self.getPromptParams( 'mnews_pm_prompt' )

    def pre_recordpm(self, prevState):
        return self.getEmbeddedRecordingParams(self.forwarding_properties.personal_msg_time_secs, self.forwarding_log, Forwarding_log.forwarder_personal_recording )
    
    def pre_forwardthankyou(self, prevState):
        self.scheduleForwardingCall(self.forwarding_log)
        return self.getPromptParams( 'mnews_forward_thank_you' )
    
    def while_forwardthankyou__sm_action_success(self, events, eventData):
        if self.prevForwardState == 'listnews':
            return 'mnews_listnews_next'
        elif self.prevForwardState == 'playmarkeditem':
            return 'mnews_welcome_next'
        elif self.prevForwardState == 'listcomments':
            return 'mnews_listcomments_next'
        return 'sm_action_success'

    def while_checknetworkads__sm_action_success(self, events, eventData):
        if self.networkAds is None:
            self.currentNetworkAd = -1
            try:
                #count = Position_ad_count.objects.get(position = 'MNEWS_NETWORK', ai = self.ai)
                self.networkAds = get_ads_for_pos(self.ai, 'MNEWS_NETWORK', self.sessionData.callerid)
            except Exception,e:
                logger.error("Got Exception:" + str(e))

        self.currentNetworkAd += 1
        if self.networkAds and self.currentNetworkAd < len(self.networkAds):
            return 'advert_play_ad'
        else:
            return 'advert_ads_done'

    def pre_playnetworkads(self):
        self.networkAdStartTime = datetime.now()
        return self.getPlaybackParams( self.networkAds[self.currentNetworkAd].ad_recording.get_full_filename() )

    def while_playnetworkads__sm_action_success(self, events, eventData):
        log_impression(self.networkAds[self.currentNetworkAd], 'MNEWS_NETWORK', self.sessionData.cdrs[0],
                       self.networkAdStartTime, datetime.now(), self.ai, 
                       self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
        send_ad_message(self.networkAds[self.currentNetworkAd], self.ai, self.sessionData.callerid)
        return 'sm_action_success'

    def while_playnetworkads__dtmf_1(self, events, eventData):
        ad = self.networkAds[self.currentNetworkAd]
        if is_ad_skippable(ad, self.ai, 'MNEWS_NETWORK'):
            ad_end_time = datetime.now()
            if log_partial_impression(ad, self.ai, 'MNEWS_NETWORK', self.networkAdStartTime, ad_end_time):
                log = log_impression(self.networkAds[self.currentNetworkAd], 'MNEWS_NETWORK', self.sessionData.cdrs[0],
                        self.networkAdStartTime, ad_end_time, self.ai, self.ai.app_instance_settings_set.all()[0].vi_conf)
            return 'skip_ad'

    def while_playnetworkads__dtmf_9(self, events, eventData):
        ref = get_target_reference_for_ad(self.networkAds[self.currentNetworkAd], self.ai)
        if ref:
            vi_ref, embeddable = ref
            log = log_impression(self.networkAds[self.currentNetworkAd], 'MNEWS_NETWORK', self.sessionData.cdrs[0], 
                                 self.networkAdStartTime, datetime.now(), self.ai, 
                                 self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
            send_ad_message(self.networkAds[self.currentNetworkAd], self.ai, self.sessionData.callerid)
            log_ad_lead(log)
            if embeddable:
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'listnews') )
            return vi_ref
        return ref

    def while_checkchannelads__sm_action_success(self, events, eventData):
        if self.channelAds is None:
            self.initChannelAds()

        self.currentChannelAd += 1
        if self.channelAds and self.currentChannelAd < len(self.channelAds):
            return 'advert_play_ad'
        else:
            self.channelAds = None
            return 'advert_ads_done'

    def pre_playchannelads(self):
        self.channelAdStartTime = datetime.now()
        return self.getPlaybackParams( self.channelAds[self.currentChannelAd].ad_recording.get_full_filename() )

    def while_playchannelads__sm_action_success(self, events, eventData):
        log_impression(self.channelAds[self.currentChannelAd], '%s__MNEWS_CHANNEL' % self.currentChannel.name, self.sessionData.cdrs[0],
                       self.channelAdStartTime, datetime.now(), self.ai, 
                       self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
        send_ad_message(self.channelAds[self.currentChannelAd], self.ai, self.sessionData.callerid)
        return 'sm_action_success'

    def while_playchannelads__dtmf_1(self, events, eventData):
        ad = self.channelAds[self.currentChannelAd]
        position = '%s__MNEWS_CHANNEL' % self.currentChannel.name
        if is_ad_skippable(ad, self.ai, position):
            ad_end_time = datetime.now()
            if log_partial_impression(ad, self.ai, position, self.channelAdStartTime, ad_end_time):
                log = log_impression(self.channelAds[self.currentChannelAd], position, self.sessionData.cdrs[0],
                        self.channelAdStartTime, ad_end_time, self.ai, self.ai.app_instance_settings_set.all()[0].vi_conf)
            return 'skip_ad'

    def while_playchannelads__dtmf_9(self, events, eventData):
        ref = get_target_reference_for_ad(self.channelAds[self.currentChannelAd], self.ai)
        if ref:
            vi_ref, embeddable = ref
            log = log_impression(self.channelAds[self.currentChannelAd], '%s__MNEWS_CHANNEL' % self.currentChannel.name,
                                self.sessionData.cdrs[0], self.channelAdStartTime, datetime.now(), self.ai,
                                self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
            send_ad_message(self.channelAds[self.currentChannelAd], self.ai, self.sessionData.callerid)
            log_ad_lead(log)
            if embeddable:
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'listnews') )
            return vi_ref
        return ref
            
    def while_checkinlineads__sm_action_success(self, events, eventData):
        if self.inlineAds is None:
            self.currentInlineAd = -1
            try:
                position = self.getInlineAdPosition(self.currentNews, self.currentChannel)
                self.inlineAds = get_ads_for_pos(self.ai, position, self.sessionData.callerid)
            except Exception,e:
                logger.error("Got Exception:" + str(e))

        self.currentInlineAd += 1
        if self.inlineAds and self.currentInlineAd < len(self.inlineAds):
            return 'advert_play_ad'
        else:
            self.inlineAds = None
            return 'advert_ads_done'
        
    def pre_playinlineads(self):
        self.inlineAdStartTime = datetime.now()
        return self.getPlaybackParams( self.inlineAds[self.currentInlineAd].ad_recording.get_full_filename() )

    def while_playinlineads__sm_action_success(self, events, eventData):
        position = self.getInlineAdPosition(self.currentNews, self.currentChannel)
        log_impression(self.inlineAds[self.currentInlineAd], position, self.sessionData.cdrs[0], 
                       self.inlineAdStartTime, datetime.now(), self.ai, 
                       self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
        send_ad_message(self.inlineAds[self.currentInlineAd], self.ai, self.sessionData.callerid)
        return 'sm_action_success'

    def while_playinlineads__dtmf_1(self, events, eventData):
        position = self.getInlineAdPosition(self.currentNews, self.currentChannel)
        ad = self.inlineAds[self.currentInlineAd]
        if is_ad_skippable(ad, self.ai, position):
            ad_end_time = datetime.now()
            if log_partial_impression(ad, self.ai, position, self.inlineAdStartTime, ad_end_time):
                log = log_impression(self.inlineAds[self.currentInlineAd], position, self.sessionData.cdrs[0],
                        self.inlineAdStartTime, datetime.now(), self.ai, self.ai.app_instance_settings_set.all()[0].vi_conf)
            return 'skip_ad'

    def while_playinlineads__dtmf_8(self, events, eventData):
        self.vi_ref_next_ai = self.checkViRef(App_instance_settings.objects.get(ai_id=self.ai.id).vi_conf_id,'jump_8_dtmf_')
        if self.vi_ref_next_ai.exists():
            return 'mnews_jump_ai'

    def while_playinlineads__dtmf_9(self, events, eventData):
        position = self.getInlineAdPosition(self.currentNews, self.currentChannel)
        ref = get_target_reference_for_ad(self.inlineAds[self.currentInlineAd], self.ai)
        if ref:
            vi_ref, embeddable = ref
            log = log_impression(self.inlineAds[self.currentInlineAd], position, self.sessionData.cdrs[0], 
                                 self.inlineAdStartTime, datetime.now(), self.ai, 
                                 self.ai.app_instance_settings_set.all()[0].vi_conf) #vi obtained from ai. not the best thing to do.
            send_ad_message(self.inlineAds[self.currentInlineAd], self.ai, self.sessionData.callerid)
            log_ad_lead(log)
            if embeddable:
                self.sessionData.pushEmbedStack( get_ai_state_transition_string(self.ai, 'listnews') )
            return vi_ref
        return ref


MnewsStatesDescriptionMap = [
    {   'name':'outgoingstart', 
        'action':'originate', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'], 
            'outgoingstart':['sm_next_originate_url'] 
            } 
        },
    {   'name':'incomingstart', 
        'action':'answer', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'welcome':['sm_action_success'] 
            } 
        },
    {   'name':'welcome', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checknetworkads':['sm_action_success', 'mnews_continue_news'],
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],            
            'playbookmarkitem': ['mnews_play_bookmarked_item']
            }, 
        'dtmf': {
            1:'mnews_continue_news',
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',            
            }
        },

    {   'name':'checknetworkads',
        'action':'none',
        'transitions': {
            'checkchannelads':['advert_ads_done'],
            'playnetworkads':['advert_play_ad'],
            'stop':['sm_action_failure'],
            },
        },
    {
        'name':'playnetworkads',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checknetworkads': ['sm_action_success', 'skip_ad'],
            },
        },
    {
        'name':'checkchannelads',
        'action': 'none',
        'transitions': {
            'stop': ['sm_action_failure'],
            'instructions': ['advert_ads_done'],
            'playchannelads': ['advert_play_ad'],
            },
        },
    {
        'name':'playchannelads',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'checkchannelads': ['sm_action_success', 'skip_ad'],
            },
        },
    {   'name':'instructions', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure'], 
            'checknewsavailable':['sm_action_success', 'mnews_continue_news'],
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],            
            'playbookmarkitem': ['mnews_play_bookmarked_item']
            }, 
        'dtmf': {
            1:'mnews_continue_news',
            3:'mnews_record_news',
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',            
            6:'mnews_play_bookmarked_item'
            }
        },
    {
        'name':'checkinlineads',
        'action':'none',
        'transitions': {
            'stop': ['sm_action_failure'],
            'playinlineads': ['advert_play_ad'],
            'nextnews': ['advert_ads_done'],
            },
        },
    {
        'name':'playinlineads',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'checkinlineads':['sm_action_success', 'skip_ad'],
            'selectnextai': ['mnews_jump_ai'],
            },
        },
    {
        'name': 'nextnews',
        'action': 'none',
        'transitions': {
            'stop':['sm_action_failure', 'sm_ai_time_out'],
            'contributeannounce':['mnews_news_done'],
            'thankyou':['mnews_news_done_no_contrib_announce'],
            'listnews':['mnews_next_news'],
            },
        },
    {   'name': 'checknewsavailable',
        'action': 'none',
        'transitions': {
            'listnews':['mnews_continue_news'],
            'contributeannounce':['mnews_no_news'],
            'thankyou':['mnews_no_news_no_contrib_announce'],
            'stop':['sm_action_failure']
            },            
        },
    {   'name':'listnews', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure', 'sm_ai_time_out'], 
            'listnews':['mnews_prev_news'], 
            'checkinlineads':['advert_play_inline_ads','mnews_next_news'],
            'recordcommentprompt':['mnews_record_comment'], 
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],
            'postbookmarkprompt':['mnews_news_bookmarked'],
            'playbookmarkitem': ['mnews_play_bookmarked_item'],
            'selectnextai':['mnews_jump_ai'],
            'checkcomments':['sm_action_success'],
            }, 
        'dtmf': { 
            1:'mnews_next_news',
            2:'mnews_record_comment', 
            3:'mnews_record_news',
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',
            5:'mnews_news_bookmarked',
            6:'mnews_play_bookmarked_item',


            }     
        },
    {   'name':'selectnextai',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'playinvalidprompt' : ['mnews_invalid_choice'],
            #'checkcomments':['sm_action_success', 'check_comments'],
            },
        },   
    {   'name':'playinvalidprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'nextnews':['sm_action_success'],
            },
        },
    {   'name':'checkcomments',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'listcomments':['mnews_start_comments'], 
            'checkinlineads':['advert_play_inline_ads'],
            },
        },
    {   'name':'listcomments', 
        'action':'playback', 
        'transitions': { 
            'stop':['sm_action_failure', 'sm_ai_time_out'], 
            'listcomments':['mnews_next_comment'], 
            'listnews':['mnews_prev_news'], 
            'checkinlineads':['advert_play_inline_ads'],
            'recordcommentprompt':['mnews_record_comment'], 
            'contributeannounce':['mnews_news_done'], 
            'contributeprompt':['mnews_record_news'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['mnews_next_channel'],
            'postbookmarkprompt':['mnews_bookmark_item'],
            'playbookmarkitem': ['mnews_play_bookmarked_item'],
            'selectnextai': ['mnews_jump_ai'],
            }, 
        'dtmf': { 
            1:'advert_play_inline_ads',
            2:'mnews_record_comment', 
            3:'mnews_record_news', 
            '*':'mnews_prev_channel',
            0:'mnews_next_channel',
            5:'mnews_bookmark_item',
            6:'mnews_play_bookmarked_item'
            } 
        },
    {    'name':'recordcommentprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'recordcommentrecord':['sm_action_success'] 
            } 
         },
    {    'name':'recordcommentrecord', 
         'action':'record', 
         'transitions': { 
            'stop':['sm_action_failure'],
            'recordcommentthankyou':['sm_action_success'],
            'validateannounce':['mnews_validate_recording'],
            }
         },
    {    'name': 'recordcommentthankyou',
         'action': 'playback',
         'transitions': {
            'stop':['sm_action_failure'],
            'listnews':['mnews_continue_news'], 
            'listcomments':['mnews_continue_comments'] 
            }
         },
    {    'name':'contributeannounce', 
         'action':'play_and_get_digits', 
         'transitions': { 
            'stop':['sm_action_failure'],
            'thankyou':['sm_get_digits_no_digits', 'mnews_no_contribution'], 
            'contributeprompt':['mnews_record_contribution'] 
            }, 
         'dtmf': { 
            3:'mnews_record_contribution', 
            'any':'mnews_no_contribution' 
            } 
         },
    {    'name':'contributeprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'contributerecord':['sm_action_success'] 
            } 
         },
    {    'name':'contributerecord', 
         'action':'record', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'contributethankyou':['sm_action_success'],
            'validateannounce':['mnews_validate_recording']
            } 
         },
    {    'name':'validateannounce',
         'action':'playback',
         'transitions': {
            'stop':['sm_action_failure'],
            'playrecording':['sm_action_success'],
            }
         },
    {    'name':'playrecording',
         'action':'play_and_get_digits',
         'transitions': {
            'stop':['sm_action_failure'],
            'validatechoice':['sm_action_success', 'mnews_invalid_choice', 'mnews_move_validatechoice'],
            'recordingvalidated':['mnews_recording_good'],
            'recordinginvalidated':['mnews_recording_bad'],
            'recordingcancelled':['mnews_recording_cancelled'],
            },
         # 'dtmf': {
         #    1:'mnews_recording_good',
         #    2:'mnews_recording_bad',
         #    3:'mnews_recording_cancelled',
         #    }
         },
    {    'name':'validatechoice',
         'action':'play_and_get_digits',
         'transitions': {
            'stop': ['sm_action_failure'],
            'recordingvalidated':['mnews_recording_good'],
            'recordinginvalidated':['mnews_recording_bad', 'mnews_no_choice'],
            'recordingcancelled':['mnews_recording_cancelled'],
            'validatechoice':['mnews_invalid_choice'],
            },
         },
    {    'name':'recordinginvalidated',
         'action':'none',
         'transitions': {
            'stop':['sm_action_failure'],
            'recordcommentprompt':['mnews_recording_is_comment'],
            'contributeprompt':['mnews_recording_is_news'],
            }
         },
    {    'name':'recordingcancelled',
         'action':'none',
         'transitions': {
            'stop':['sm_action_failure'],
            'checknewsavailable':['sm_action_success'],
            }
         },
    {    'name':'recordingvalidated',
         'action':'none',
         'transitions': {
            'stop':['sm_action_failure'],
            'recordingautopublished':['mnews_autopublish'],
            'contributethankyou':['mnews_contrib_no_autopublish'],
            'recordcommentthankyou':['mnews_comment_no_autopublish'],
            },
         },
    {    'name':'recordingautopublished',
         'action':'playback',
         'transitions': {
            'stop':['sm_action_failure', 'sm_action_success'],
            'checknewsavailable':['mnews_continue_news'],
            'listcomments':['mnews_continue_comments'],
            } 
         },
    {    'name':'contributethankyou', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure', 'sm_action_success_no_contrib_announce'],
            'thankyou':['sm_action_success'],
            'checknewsavailable':['mnews_continue_news'] 
            } 
         },
    {    'name':'thankyou', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'],
            'prevchannel':['mnews_prev_channel'],
            'nextchannel':['sm_action_success', 'mnews_next_channel']
            },
         'dtmf': {
            '*':'mnews_prev_channel',
            0:'mnews_next_channel'
            }
         },
    {    'name':'stop', 
         'action':'hangup', 
         'transitions': {} 
         },
    {    'name': 'prevchannel',
         'action':'none',
         'transitions': {
        'listnews': ['mnews_no_channel_change'],
            'welcome':['mnews_prev_channel'],
            'stop':['sm_action_failure']
            }
         },
    {    'name': 'nextchannel',
         'action':'none',
         'transitions': {
        'listnews': ['mnews_no_channel_change'],
            'welcome':['mnews_next_channel'],
            'stop':['sm_action_failure']
            }
         },
    {    'name':'postbookmarkprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'listnews':['mnews_continue_news'], 
            'listcomments':['mnews_continue_comments'],
            'forwarditemprompt':['sm_action_success'],  
            } 
         },                             
    {    'name':'forwarditemprompt', 
         'action':'play_and_get_digits', 
         'transitions': {
            'stop':['sm_action_failure'],            
            'listnews':['sm_get_digits_no_digits', 'mnews_no_forward_num'],            
            'forwardcurrentitem':['mnews_forward_input'],  
            },       
         },
    
    {    'name':'forwardcurrentitem', 
         'action':'play_and_get_digits', 
         'transitions': { 
             'stop':['sm_action_failure'],
            'listnews':['sm_get_digits_no_digits'],            
            'recordpmprompt':['sm_action_success'],  
            }       
         },
    {    'name':'recordpmprompt', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'],            
            'recordpm':['sm_action_success'],  
            }       
         },
    {    'name':'recordpm', 
         'action':'record', 
         'transitions': { 
            'stop':['sm_action_failure'],            
            'forwardthankyou':['sm_action_success'],  
            }       
         },
                             
    {    'name':'forwardthankyou', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'],           
            'listnews':['mnews_listnews_next', 'sm_action_success'],
            'listcomments':['mnews_listcomments_next'], 
            'welcome': ['mnews_welcome_next'],
        }       
    },
       
    {    'name':'playbookmarkitem', 
         'action':'play_and_get_digits', 
         'transitions': { 
            'stop':['sm_action_failure'],            
            'playmarkeditem':['mnews_valid_bookmark'],
            'invalidbookmark':['mnews_invalid_bookmark'],
            'listnews':['sm_action_success', 'sm_get_digits_no_digits']  
            },
         'dtmf': {
            6: 'sm_action_success',
            } 
         },
    {    'name':'invalidbookmark', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure'], 
            'listnews':['mnews_continue_news'], 
            'listcomments':['mnews_continue_comments'],
            'playbookmarkitem':['sm_action_success']  
            },
          'dtmf': { 
            6:'mnews_continue_news',                      
            }           
         },
    {    'name':'playmarkeditem', 
         'action':'playback', 
         'transitions': { 
            'stop':['sm_action_failure', 'sm_ai_time_out'], 
            'checknewsavailable':['mnews_continue_news', 'sm_action_success'], 
            'listcomments':['mnews_continue_comments'],  
            'postbookmarkprompt':['mnews_bookmark_item'],           
            },
         'dtmf': { 
            5:'mnews_bookmark_item',
            6:'mnews_continue_news',
            }           
         },
    ]
