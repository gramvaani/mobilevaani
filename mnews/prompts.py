from media.models import Prompt_audio

PROMPTS_AI = [ 'mnews_bookmark_notify', 'mnews_enter_bookmark', 'mnews_invalid_bookmark', 'mnews_forward_prompt', 'mnews_forward_input_number', 'mnews_forward_invalid_number', 'mnews_pm_prompt', 'mnews_forward_thank_you' ]
PROMPTS_CHANNEL = [ 'mnews_welcome', 'mnews_thank_you', 'mnews_instructions', 'mnews_leave_comment', 'mnews_ack_comment', 'mnews_contrib_announce', 'mnews_contrib_record', 'mnews_contrib_thank_you' ]

def get_prompt_info_names():
    return PROMPTS_AI 

def add_prompts_for_channel( ai_id, channel ):
    prompts_channel = [ prompt_channel + "__" + str( channel.name ) for prompt_channel in PROMPTS_CHANNEL ]
    Prompt_audio.add_media( prompts_channel, ai_id = ai_id )
