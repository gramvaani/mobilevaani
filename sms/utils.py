# Utility functions needed by many SMS handlers
import re, json
import urllib, urllib2, socket, httplib, base64, re, hashlib
from log import get_request_logger
import settings
logger = get_request_logger()

def remove_app_name(message, app_name):
    
    modified_message = message.strip()   
    if app_name and message.lower().startswith(app_name.lower()):
         modified_message = message[len(app_name):]
         # strip of modified_message is required as it has been changed
    return modified_message.strip()
    

def split_message(string, separators=r'[ ,.;]+'):
        
    result = []
    if string:
        result = re.split(separators, string)
    
#     for current in strings:
#         cleaned = re.sub(separators, '', current)        
#         result.append(cleaned)                        
    return result


#Todo: Pick up the service to use based on creds or something else
def send_sms(creds, receiver_num, sender_id, message, dlr_url = None):
    logger.info(str(dlr_url))
    return send_valuefirst_sms(creds, receiver_num, sender_id, message, dlr_url)


def get_guid_from_result(result):
    return result.split("&")[0].split("=")[1]


def send_valuefirst_sms(creds, receiver_num, sender_id, message, dlr_url=None):
    from vapp.utils import is_english
    try:
        params = {}
        params['username']      = str(creds.user_id)
        params['password']       = str(creds.password)
        params['to']  = str(receiver_num)
        params['from']  = str(sender_id)
        params['text']   = str(message)
        if dlr_url:
            params['dlr-url'] = dlr_url
            params['category'] = 'bulk'
        if not is_english(message):
            params['coding'] = '3'
       
        logger.info('params: '+str(params))
        url = creds.url + '?' + urllib.urlencode(params)
        logger.info('ValueFirstSMSTask url:' + url)
        res = urllib2.urlopen(url)
        res_text = res.readlines()
        result = res_text[0]
        logger.info("result ::" + str(result))
        logger.info("res ::" + str(res))
        logger.info("res.getcode() ::" + str(res.getcode()))
        logger.info(json.dumps(res, default=lambda o: getattr(o, '__dict__', str(o))))
        if res.getcode() != httplib.OK:
            raise Exception(result)
        is_success = True
        guid = get_guid_from_result(str(result))
    except Exception,e:
        logger.exception('Exception in sending sms through valuefirst:' + str(e))
        is_success = False
        guid = None
            
    return is_success, guid



def generate_dlr_url(msg_obj):
    dlr_url = settings.DLR_URL +"?unique_id=" + str(msg_obj.id) + "&status=%d&circle=%8&operator=%9&msg_status=%16&message_id=%5&status_error=%2&delivered_date=%3&split_count=%17"
    return dlr_url
