'''
Created on 23-Mar-2011

@author: bchandra
'''

from django.db import IntegrityError
from celery.task import Task

from sms.models import *
from sms.utils import *
from vapp.log import get_logger
from config import config

from django.db.models.signals import post_save
from mward.views import sms_handler

import urllib, urllib2, socket, httplib, base64, re, hashlib
from datetime import datetime, timedelta

logger = get_logger()

class SMSTask(Task):
    
    @classmethod
    def create_send_msg(cls, ai_id, message, receiver_id, max_tries = 1, sender_id = 'GRMVNI', eta = datetime.now()):
        sms             = SMS_message()
        sms.ai_id       = ai_id
        sms.cred_id     = SMS_credential.objects.get(ai_sms_credential__ai__id = ai_id, ai_sms_credential__role = 'default').id
        sms.message     = message
        sms.sender_id   = sender_id
        sms.receiver_id = receiver_id
        sms.save()
        try:
            SMSTask.apply_async([sms.pk, max_tries], eta = eta)
        except Exception,e:
            logger.exception("Encountered Exception:" + str(e))
        return sms
    
    def run(self, msg_pk, max_tries):
        msg = SMS_message.objects.get(pk = msg_pk)
        if msg.sent_success:
            return

        msg.tries += 1
        msg.save()
        dlr_url=generate_dlr_url(msg)
        msg.sent_success, msg.message_id = send_sms(msg.cred, msg.receiver_id, msg.sender_id, msg.message, dlr_url)
        if msg.sent_success:
            msg.sent_time = datetime.now()
            logger.info("Message sent to: " + str(msg.receiver_id))
        elif msg.tries <= max_tries:
            delay = 10
            logger.info('Unable to send the SMS .. retrying in ' + str(delay) + ' minutes')
            eta = datetime.now() + timedelta(minutes = delay)
            SMSTask.apply_async([msg.id, max_tries], eta = eta)
        msg.save()

def store_sms(sms, source_info):
    routes = [p.route for p in Priority_sms_route.objects.order_by('priority').order_by('id')]
    ai_id = None
    while routes:
        route = routes.pop(0)
        ai_id = route.route(sms, source_info)
        if ai_id:
            sms.ai_id = ai_id
            break
    try:
        sms.save()
        return True
    except IntegrityError:
        return True
    except:
        logger.exception("Unable to save sms %s", sms.message)
    return False


class TopexSMSTask(Task):
    
    send_url_template = 'https://{ip}/admin/forms/sms-send.html?number={number}&smsencode={message}&send={send}'
    cred = base64.encodestring('%s:%s' % ('admin', '99admin11')).rstrip()
    send_timeout = 10
        
        
    def run(self,ai_id, message, number, send = 'Send'):
        
            topex_ips = Topex_SMS_Credential.objects.filter(ai__id = ai_id)
            logger.debug('inside run of TopexSMSTask, topex ips ' + str(len(topex_ips)))
            
            sms = Topex_SMS_Message()
            sms.ai = App_instance.objects.get(pk = ai_id)
            sms.receiver = number
            sms.message = message
            sms.save()
            
            for topex in topex_ips:
                
                if topex.user and topex.pwd:
                     self.cred = base64.encodestring('%s:%s' % (topex.user, topex.pwd)).rstrip()
                     logger.debug('cred: ' + str(self.cred))
                     
                send_url = self.send_url_template.format(ip=topex.ip,number=str(number),message=urllib.quote(message),send=send)
                logger.debug('send_url: ' + send_url)       
                request = urllib2.Request(send_url)
                request.add_header('Authorization', 'Basic %s' % self.cred)
                try:                
                    socket.setdefaulttimeout(self.send_timeout) 
                    urllib2.urlopen(request).readlines()
                    logger.debug('sms has been sent successfully')
                    sms = Topex_SMS_Message.objects.get(pk = sms.id)
                    sms.sent_from = topex
                    sms.success = True
                    sms.save()
                    
                    break
                except IOError, e:
                    logger.exception('Exception in sending sms to topex: ' + str(topex.ip))
    
class PollGSM(Task):
    read_url_template = 'https://{ip}/admin/forms/sms-read.html'
    delete_url_template =  'https://{ip}/admin/forms/sms-read.html?id={msg_id}&send=del'
    sms_pattern = re.compile('//out/sms/.*[.]sms')
    cred = base64.encodestring('%s:%s' % ('admin', '99admin11')).rstrip()
    read_timeout = 2
    delete_timeout = 2

    def line_contains_msg(self, line):
        return self.sms_pattern.search(line)
    
    def msg_to_tuple(self, line):
        tokens = line.split('"')
        time = datetime.strptime(tokens[1][:17], "%m.%d.%y %H:%M:%S")
        sender = tokens[3]
        msg = urllib2.unquote(tokens[5])
        msg_id = tokens[7]
        return (time, sender, msg, msg_id)
    
    def delete_sms(self, source_info):
        delete_url = self.delete_url_template.format(ip = source_info['ip'], msg_id = source_info['msg_id'])
        request = urllib2.Request(delete_url)
        request.add_header('Authorization', 'Basic %s' % self.cred)
        try:
            socket.setdefaulttimeout(self.delete_timeout) 
            # readlines() important to ensure deleting of message
            urllib2.urlopen(request).readlines()
        except IOError, e:
            logger.exception('Exception in deleting sms')
                
    def run(self):
        for ip in config.options('gsmgw-modules'):
            self.poll_from_ip(ip)
        
    def poll_from_ip(self, ip):
        read_url = self.read_url_template.format(ip = ip)
        request = urllib2.Request(read_url)
        request.add_header('Authorization', 'Basic %s' % self.cred)
        try:
            socket.setdefaulttimeout(self.read_timeout)
            lines = urllib2.urlopen(request).readlines()
            #lines = open('/tmp/smsdata1').readlines()
            for line in lines:
                if self.line_contains_msg(line):
                    time, sender, msg, msg_id = self.msg_to_tuple(line)
                    #hash_md5 = hashlib.md5("".join([str(time), sender, msg])).hexdigest()
                    sms = SMS_message_received(sender = sender, time = time, message = msg)
                    source_info = {'ip': ip, 'type': 'gsm_gw', 'msg_id': msg_id}
                    if store_sms(sms, source_info):
                        self.delete_sms(source_info)
        except:
            logger.exception("Unable to poll from gateway")


post_save.connect(sms_handler, sender=SMS_message_received)

