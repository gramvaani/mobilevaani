import ast
from tastypie.authentication import ApiKeyAuthentication
from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields

from django.conf.urls import url

from models import SMS_message, SMS_template, SMS_credential
from tasks import SMSTask
from utils import send_sms
from vapp.api import AppInstanceAuthorization, apply_default_checks
from vapp.app_manager.api import AppInstanceResource

from datetime import datetime

from vapp.log import get_request_logger
logger = get_request_logger()


class SMSMessageResource(ModelResource):
    ai = fields.ForeignKey(AppInstanceResource, 'ai')

    class Meta:
        queryset = SMS_message.objects.all()
        allowed_methods = [ 'get' ]
        resource_name = 'sms_message'
        filtering = {
            'ai': ALL_WITH_RELATIONS,
            'receiver_id':  ['exact'],
        }
        authentication = ApiKeyAuthentication()
        authorization = AppInstanceAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/send%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('send'), name='api_send'),
            url(r"^(?P<resource_name>%s)/send_formatted_msg%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('send_formatted_msg'), name='api_send_formatted_msg'),
            url(r"^(?P<resource_name>%s)/status%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_sms_status'), name='api_get_sms_status'),
        ]

    @apply_default_checks( allowed_methods = [ 'post' ] )
    def send(self, request, **kwargs):
        ai_id = kwargs[ 'ai' ]
        message_id = kwargs[ 'message_id' ]
        max_tries = int(kwargs.get( 'max_tries', 1 ))

        try:
            response = {}
            response_objects = []
            sms_message = SMS_template.objects.get( pk = message_id )

            message_args = kwargs.get( 'message_args' )
            callerids = kwargs.get( 'caller_ids' )
            number_sms_dict = kwargs.get( 'number_sms_dict' )

            if message_args and callerids:
                message_args = ast.literal_eval( message_args )
                callerids = callerids.split(",")
                sms_message = sms_message.process_template( message_args )
                for callerid in callerids:
                    sms_obj = SMSTask.create_send_msg( ai_id, sms_message, callerid, max_tries = max_tries )
                    response_objects.append( self.create_sms_resp_obj( request, sms_obj ) )
            elif number_sms_dict:
                number_sms_dict = ast.literal_eval( number_sms_dict )
                for callerid in number_sms_dict:
                    message_args = number_sms_dict[ callerid ]
                    message = sms_message.process_template( message_args )
                    sms_obj = SMSTask.create_send_msg( ai_id, message, callerid, max_tries = max_tries )
                    response_objects.append( self.create_sms_resp_obj( request, sms_obj ) )
            else:
                return self.create_response( request, { 'success': False, "reason": "Invalid parameters passed." } )

            response[ 'objects' ] = response_objects
            return self.create_response( request, response )
        except Exception,e:
            logger.exception('exception in sending sms through API: {0}'.format(e))
        return self.create_response( request, { 'success': False, "reason": "Request not accepted." } )

    @apply_default_checks( allowed_methods = [ 'post' ] )
    def send_formatted_msg( self, request, **kwargs ):
        ai_id = kwargs[ 'ai' ]
        callerid = kwargs[ 'callerid' ]
        message = kwargs[ 'message' ]
        sender_id = 'GRMVNI'
        try:
            cred = SMS_credential.objects.get( ai_sms_credential__ai__id = ai_id, ai_sms_credential__role = 'default' )
            sms = SMS_message( ai_id = ai_id, cred = cred, message = message, sender_id = sender_id, receiver_id = callerid, tries = 1 )
            sms.save()
            sent_success = send_sms( sms.cred, sms.receiver_id, sms.sender_id, sms.message )
            if sent_success:
                sent_time = datetime.now()
                sms.sent_success = sent_success
                sms.sent_time = sent_time
                sms.save()

            return self.create_response( request, { 'sms_id': sms.id, 'success': sms.sent_success } )
        except Exception,e:
            logger.exception( 'exception in sending sms through API: {0}'.format( e ) )
        return self.create_response( request, { 'sms_id': sms.id, 'success': False, "reason": "Request not accepted." } )



    def create_sms_resp_obj(self, request, sms_obj, **kwargs):
        response_object = {}
        response_object[ 'id' ] = sms_obj.id
        response_object[ 'ai_id' ] = sms_obj.ai_id
        response_object[ 'receiver_id' ] = sms_obj.receiver_id
        return response_object



class SMSMessageUpdateResource(ModelResource):
    

    class Meta:
        queryset = SMS_message.objects.all()
        allowed_methods = [ 'get', 'post' ]
        resource_name = 'sms_message_update'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/status%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_sms_status'), name='api_get_sms_status'),
        ]

  
    def get_sms_status( self, request, **kwargs ):
        unique_id =  request.REQUEST['unique_id']
        status =  request.REQUEST['status']
        circle = request.REQUEST['circle']
        operator = request.REQUEST['operator']
        msg_status = request.REQUEST['msg_status']
        msg_id = request.REQUEST['message_id']
        status_error = request.REQUEST['status_error']
        delivered_date = request.REQUEST['delivered_date']
        split_count = request.REQUEST['split_count']
        logger.info('SMS update api ' +str(unique_id))
        try:

            msg = SMS_message.objects.get(pk = int(unique_id))
            msg.sent_success = status
            msg.circle = circle
            msg.operator = operator
            msg.message_status= msg_status
            msg.message_id  = msg_id
            msg.message_status_error = status_error
            msg.split_count = split_count
            msg.save()


            return self.create_response( request, { 'success': True } )
        except Exception,e:
            logger.exception( 'exception in sending sms through API: {0}'.format( e ) )
        return self.create_response( request, { 'success': False, "reason": str(e) } )


