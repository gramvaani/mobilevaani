from django.db import models

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models.signals import post_save, post_delete

from app_manager.models import App_instance, CalleridField
from vapp.log import get_logger
from events import Event
from datetime import datetime
import RQMC


logger = get_logger()

ID_LENGTH = 40
TOKEN_LENGTH = 100
URL_LENGTH = 255
CALLERID_LENGTH = 20 
MD5_LENGTH = 32

class SMS_credential(models.Model):
    url     = models.CharField(max_length = URL_LENGTH)
    user_id = models.CharField(max_length = ID_LENGTH)
    password= models.CharField(max_length = ID_LENGTH)
    
    def __unicode__(self):
        return self.user_id

class AI_sms_credential(models.Model):
    ai  = models.ForeignKey(App_instance)
    cred= models.ForeignKey(SMS_credential)
    role= models.CharField(max_length = 24)
    
    def __unicode__(self):
        return str(self.ai) + '_' + str(self.cred)
     
class SMS_message(models.Model):
    ai          = models.ForeignKey(App_instance, null = True)
    cred        = models.ForeignKey(SMS_credential)
    message     = models.CharField(max_length = URL_LENGTH)
    sender_id   = models.CharField(max_length = ID_LENGTH)
    receiver_id = CalleridField(max_length = CALLERID_LENGTH)
    sent_time   = models.DateTimeField(null = True)
    sent_success= models.BooleanField()
    tries       = models.PositiveSmallIntegerField(default = 0)
    circle  = models.CharField(max_length=24, null = True)
    operator = models.CharField(max_length=24, null = True)
    message_status = models.CharField(max_length=24, null = True)
    message_id = models.CharField(max_length=240, null = True)
    message_status_error = models.CharField(max_length=24, null = True)
    split_count = models.CharField(max_length = 4, null = True)
    



class SMS_message_received(models.Model):
    ai      = models.ForeignKey(App_instance, null = True)
    sender  = CalleridField(max_length = CALLERID_LENGTH)
    time    = models.DateTimeField()
    message = models.CharField(max_length = URL_LENGTH)
    #hash_id = models.CharField(max_length = MD5_LENGTH)        

    def __unicode__(self):
        return 'message at : ' + str(self.time)
    
    class Meta:
        unique_together = ('sender', 'time', 'message')

def sms_save_handler(sender, **kwargs):
    sms = kwargs['instance']
    queue = App_instance.message_queue_from_id( sms.ai_id )
    RQMC.push_instance(queue, sms, Event.ADD)

def sms_delete_handler(sender, **kwargs):
    sms = kwargs['instance']    
    queue = App_instance.message_queue_from_id( sms.ai_id )
    RQMC.push_instance(queue, sms, Event.DELETE)

post_save.connect(sms_save_handler, sender = SMS_message_received, dispatch_uid = 'sms.sms_message_received.save')
post_delete.connect(sms_delete_handler, sender = SMS_message_received, dispatch_uid = 'sms.sms_message_received.delete')

class SMS_route(models.Model):
    ai      = models.ForeignKey(App_instance)

    def route(self, sms, source_info):
        raise NotImplementedError("Subclass must implement abstract method")
    
class Priority_sms_route(models.Model):
    priority    = models.PositiveIntegerField(null = True)

    content_type= models.ForeignKey(ContentType)
    object_id   = models.PositiveIntegerField()
    route       = generic.GenericForeignKey('content_type', 'object_id')
    
class SMS_pattern_route(SMS_route):
    pattern = models.CharField(max_length = URL_LENGTH)

    def route(self, sms, source_info):
        safe_globals = {
                        'ip': source_info.get('ip'),
                        'sender': sms.sender,
                        'time': sms.time,
                        'msg': sms.message,
                        #'hash_id': sms.hash_id,
                        '__builtins__': None
                        }
        
        try:
            if eval(self.pattern, safe_globals, {}) == True:
                return self.ai_id
        except:
            logger.exception("Exception in evaluating pattern")
            return None

class SMS_template(models.Model):
    ai      = models.ForeignKey(App_instance)
    name    = models.CharField(max_length = ID_LENGTH)
    message = models.CharField(max_length = URL_LENGTH)
    
    def process_template(self, keywords):
        processed_message = self.message.format(**keywords)
        return processed_message

    def __unicode__(self):
        return unicode(str(self.ai.id) + '_' + self.name)


class Topex_SMS_Credential(models.Model):
    
    ai  = models.ForeignKey(App_instance, null = True)
    ip  = models.IPAddressField()
    user = models.CharField(max_length = ID_LENGTH, null = True)
    pwd  = models.CharField(max_length = ID_LENGTH, null = True)
    
class Topex_SMS_Message(models.Model):
    ai      = models.ForeignKey(App_instance, null = True)
    receiver  = CalleridField(max_length = CALLERID_LENGTH)
    sent_from = models.ForeignKey(Topex_SMS_Credential, null = True)
    message   = models.TextField()
    time      = models.DateTimeField(default = datetime.now())
    success   = models.BooleanField(default = False)
    
    
class SMS_ai_app(models.Model):
    ai      = models.ForeignKey(App_instance)
    handle = models.CharField(max_length = 100)
        
    
    
class SMS_contact(models.Model):
    number = CalleridField(max_length = 20)
    name = models.CharField(max_length = 100, null = True)
    provider = models.CharField(max_length = 20, null = True)
    circle = models.CharField(max_length = 30, null = True)
    
    def __unicode__(self):        
        if self.name:        
            return self.number + ',' + self.name
        return self.number
    

class SMS_log(models.Model):
    sms_ai = models.ForeignKey(SMS_ai_app)
    contact = models.ForeignKey(SMS_contact)
    is_incoming = models.BooleanField()
    message = models.TextField(null = True)
    time = models.DateTimeField(auto_now_add = True)
