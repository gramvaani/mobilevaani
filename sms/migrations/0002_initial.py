# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SMS_credential'
        db.create_table(u'sms_sms_credential', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'sms', ['SMS_credential'])

        # Adding model 'AI_sms_credential'
        db.create_table(u'sms_ai_sms_credential', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_credential'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24)),
        ))
        db.send_create_signal(u'sms', ['AI_sms_credential'])

        # Adding model 'SMS_message'
        db.create_table(u'sms_sms_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_credential'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('sender_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('receiver_id', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('sent_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('sent_success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tries', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'sms', ['SMS_message'])

        # Adding model 'SMS_message_received'
        db.create_table(u'sms_sms_message_received', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('sender', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'sms', ['SMS_message_received'])

        # Adding unique constraint on 'SMS_message_received', fields ['sender', 'time', 'message']
        db.create_unique(u'sms_sms_message_received', ['sender', 'time', 'message'])

        # Adding model 'SMS_route'
        db.create_table(u'sms_sms_route', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'sms', ['SMS_route'])

        # Adding model 'Priority_sms_route'
        db.create_table(u'sms_priority_sms_route', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('priority', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'sms', ['Priority_sms_route'])

        # Adding model 'SMS_pattern_route'
        db.create_table(u'sms_sms_pattern_route', (
            (u'sms_route_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['sms.SMS_route'], unique=True, primary_key=True)),
            ('pattern', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'sms', ['SMS_pattern_route'])

        # Adding model 'SMS_template'
        db.create_table(u'sms_sms_template', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'sms', ['SMS_template'])

        # Adding model 'Topex_SMS_Credential'
        db.create_table(u'sms_topex_sms_credential', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15)),
            ('user', self.gf('django.db.models.fields.CharField')(max_length=40, null=True)),
            ('pwd', self.gf('django.db.models.fields.CharField')(max_length=40, null=True)),
        ))
        db.send_create_signal(u'sms', ['Topex_SMS_Credential'])

        # Adding model 'Topex_SMS_Message'
        db.create_table(u'sms_topex_sms_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('receiver', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('sent_from', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.Topex_SMS_Credential'], null=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 12, 30, 0, 0))),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'sms', ['Topex_SMS_Message'])

        # Adding model 'SMS_ai_app'
        db.create_table(u'sms_sms_ai_app', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('handle', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'sms', ['SMS_ai_app'])

        # Adding model 'SMS_contact'
        db.create_table(u'sms_sms_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('app_manager.models.CalleridField')(max_length=20)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('provider', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('circle', self.gf('django.db.models.fields.CharField')(max_length=30, null=True)),
        ))
        db.send_create_signal(u'sms', ['SMS_contact'])

        # Adding model 'SMS_log'
        db.create_table(u'sms_sms_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sms_ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_ai_app'])),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_contact'])),
            ('is_incoming', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('message', self.gf('django.db.models.fields.TextField')(null=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'sms', ['SMS_log'])


    def backwards(self, orm):
        # Removing unique constraint on 'SMS_message_received', fields ['sender', 'time', 'message']
        db.delete_unique(u'sms_sms_message_received', ['sender', 'time', 'message'])

        # Deleting model 'SMS_credential'
        db.delete_table(u'sms_sms_credential')

        # Deleting model 'AI_sms_credential'
        db.delete_table(u'sms_ai_sms_credential')

        # Deleting model 'SMS_message'
        db.delete_table(u'sms_sms_message')

        # Deleting model 'SMS_message_received'
        db.delete_table(u'sms_sms_message_received')

        # Deleting model 'SMS_route'
        db.delete_table(u'sms_sms_route')

        # Deleting model 'Priority_sms_route'
        db.delete_table(u'sms_priority_sms_route')

        # Deleting model 'SMS_pattern_route'
        db.delete_table(u'sms_sms_pattern_route')

        # Deleting model 'SMS_template'
        db.delete_table(u'sms_sms_template')

        # Deleting model 'Topex_SMS_Credential'
        db.delete_table(u'sms_topex_sms_credential')

        # Deleting model 'Topex_SMS_Message'
        db.delete_table(u'sms_topex_sms_message')

        # Deleting model 'SMS_ai_app'
        db.delete_table(u'sms_sms_ai_app')

        # Deleting model 'SMS_contact'
        db.delete_table(u'sms_sms_contact')

        # Deleting model 'SMS_log'
        db.delete_table(u'sms_sms_log')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'sms.ai_sms_credential': {
            'Meta': {'object_name': 'AI_sms_credential'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24'})
        },
        u'sms.priority_sms_route': {
            'Meta': {'object_name': 'Priority_sms_route'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        u'sms.sms_ai_app': {
            'Meta': {'object_name': 'SMS_ai_app'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'handle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'sms.sms_contact': {
            'Meta': {'object_name': 'SMS_contact'},
            'circle': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'})
        },
        u'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.sms_log': {
            'Meta': {'object_name': 'SMS_log'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_contact']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_incoming': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'message': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'sms_ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_ai_app']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.SMS_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'tries': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'sms.sms_message_received': {
            'Meta': {'unique_together': "(('sender', 'time', 'message'),)", 'object_name': 'SMS_message_received'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sender': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'sms.sms_pattern_route': {
            'Meta': {'object_name': 'SMS_pattern_route', '_ormbases': [u'sms.SMS_route']},
            'pattern': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'sms_route_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['sms.SMS_route']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'sms.sms_route': {
            'Meta': {'object_name': 'SMS_route'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'sms.topex_sms_credential': {
            'Meta': {'object_name': 'Topex_SMS_Credential'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'pwd': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'})
        },
        u'sms.topex_sms_message': {
            'Meta': {'object_name': 'Topex_SMS_Message'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'receiver': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'sent_from': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sms.Topex_SMS_Credential']", 'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 30, 0, 0)'})
        }
    }

    complete_apps = ['sms']