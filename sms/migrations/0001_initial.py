# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'SMS_credential'
        db.create_table('sms_sms_credential', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('sms', ['SMS_credential'])

        # Adding model 'AI_sms_credential'
        db.create_table('sms_ai_sms_credential', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_credential'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=24)),
        ))
        db.send_create_signal('sms', ['AI_sms_credential'])

        # Adding model 'SMS_message'
        db.create_table('sms_sms_message', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cred', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.SMS_credential'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('sender_id', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('receiver_id', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('sent_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('sent_success', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('sms', ['SMS_message'])

        # Adding model 'SMS_message_received'
        db.create_table('sms_sms_message_received', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('sender', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('sms', ['SMS_message_received'])

        # Adding unique constraint on 'SMS_message_received', fields ['sender', 'time', 'message']
        db.create_unique('sms_sms_message_received', ['sender', 'time', 'message'])

        # Adding model 'SMS_route'
        db.create_table('sms_sms_route', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal('sms', ['SMS_route'])

        # Adding model 'Priority_sms_route'
        db.create_table('sms_priority_sms_route', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('priority', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('sms', ['Priority_sms_route'])

        # Adding model 'SMS_pattern_route'
        db.create_table('sms_sms_pattern_route', (
            ('sms_route_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['sms.SMS_route'], unique=True, primary_key=True)),
            ('pattern', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('sms', ['SMS_pattern_route'])

        # Adding model 'SMS_template'
        db.create_table('sms_sms_template', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('sms', ['SMS_template'])

        # Adding model 'Topex_SMS_Credential'
        db.create_table('sms_topex_sms_credential', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15)),
            ('user', self.gf('django.db.models.fields.CharField')(max_length=40, null=True)),
            ('pwd', self.gf('django.db.models.fields.CharField')(max_length=40, null=True)),
        ))
        db.send_create_signal('sms', ['Topex_SMS_Credential'])

        # Adding model 'Topex_SMS_Message'
        db.create_table('sms_topex_sms_message', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'], null=True)),
            ('receiver', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('sent_from', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sms.Topex_SMS_Credential'], null=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 2, 20, 17, 14, 19, 702546))),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('sms', ['Topex_SMS_Message'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'SMS_message_received', fields ['sender', 'time', 'message']
        db.delete_unique('sms_sms_message_received', ['sender', 'time', 'message'])

        # Deleting model 'SMS_credential'
        db.delete_table('sms_sms_credential')

        # Deleting model 'AI_sms_credential'
        db.delete_table('sms_ai_sms_credential')

        # Deleting model 'SMS_message'
        db.delete_table('sms_sms_message')

        # Deleting model 'SMS_message_received'
        db.delete_table('sms_sms_message_received')

        # Deleting model 'SMS_route'
        db.delete_table('sms_sms_route')

        # Deleting model 'Priority_sms_route'
        db.delete_table('sms_priority_sms_route')

        # Deleting model 'SMS_pattern_route'
        db.delete_table('sms_sms_pattern_route')

        # Deleting model 'SMS_template'
        db.delete_table('sms_sms_template')

        # Deleting model 'Topex_SMS_Credential'
        db.delete_table('sms_topex_sms_credential')

        # Deleting model 'Topex_SMS_Message'
        db.delete_table('sms_topex_sms_message')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sms.ai_sms_credential': {
            'Meta': {'object_name': 'AI_sms_credential'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sms.SMS_credential']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '24'})
        },
        'sms.priority_sms_route': {
            'Meta': {'object_name': 'Priority_sms_route'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        'sms.sms_credential': {
            'Meta': {'object_name': 'SMS_credential'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'sms.sms_message': {
            'Meta': {'object_name': 'SMS_message'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sms.SMS_credential']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'receiver_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'sender_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'sent_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        },
        'sms.sms_message_received': {
            'Meta': {'unique_together': "(('sender', 'time', 'message'),)", 'object_name': 'SMS_message_received'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'sms.sms_pattern_route': {
            'Meta': {'object_name': 'SMS_pattern_route', '_ormbases': ['sms.SMS_route']},
            'pattern': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sms_route_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['sms.SMS_route']", 'unique': 'True', 'primary_key': 'True'})
        },
        'sms.sms_route': {
            'Meta': {'object_name': 'SMS_route'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'sms.sms_template': {
            'Meta': {'object_name': 'SMS_template'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'sms.topex_sms_credential': {
            'Meta': {'object_name': 'Topex_SMS_Credential'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'pwd': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'})
        },
        'sms.topex_sms_message': {
            'Meta': {'object_name': 'Topex_SMS_Message'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'receiver': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'sent_from': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sms.Topex_SMS_Credential']", 'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 2, 20, 17, 14, 19, 702546)'})
        }
    }

    complete_apps = ['sms']
