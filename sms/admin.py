'''
Created on 31-Oct-2011

@author: bchandra
'''
from django.contrib import admin

from models import *

admin.site.register(Priority_sms_route)
admin.site.register(SMS_pattern_route)
admin.site.register(SMS_message_received)
admin.site.register(SMS_template)
admin.site.register(Topex_SMS_Credential)
admin.site.register(Topex_SMS_Message)
admin.site.register(SMS_credential)
admin.site.register(AI_sms_credential)