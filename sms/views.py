from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

from django.views.decorators.http import require_POST

from django.contrib.auth.decorators import login_required

from django.shortcuts import render_to_response
from django.template import RequestContext
from sms.models import SMS_message_received, SMS_template
from app_manager.decorators import instance_perm, instance_perm_per_obj

from log import get_request_logger

from excel_response import ExcelResponse #@UnresolvedImport

import json
from callerinfo.models import *
from sms.tasks import *
from app_manager.common import load_sms_handler

logger = get_request_logger()

def send_multiple_topex_sms(ai_id,numbers,message):
    
    send_url_template = 'https://{ip}/admin/forms/sms-send.html?number={number}&smsencode={message}&send={send}'
    cred = base64.encodestring('%s:%s' % ('admin', '99admin11')).rstrip()
    send_timeout = 10
    send = 'Send'
    
    for number in numbers:
        if number:
            logger.debug('sending sms to ' + str(number))
            TopexSMSTask.delay(ai_id=ai_id,message=message,number=number)


@login_required
@instance_perm
def load_received(request, ai_id, start_row, num_rows):
    logger.debug(request)
    
    if None in [start_row, num_rows]:
        return HttpResponseNotFound()
    
    start_row = int(start_row)
    num_rows  = int(num_rows)
    end_row   = start_row + num_rows

    js = serializers.get_serializer("json")()

    msgs = SMS_message_received.objects.filter(ai = ai_id).order_by('-time')
    data = js.serialize(msgs[start_row:end_row])

    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
@instance_perm_per_obj('ai')
def delete_received(request):
    logger.debug(request)
    
    for sms_wrapper in serializers.deserialize('json', request.POST['json']):
        sms_wrapper.object.delete()
        
    return HttpResponse("ok")
    
SPREADSHEET_HEADERS = ['time', 'sender', 'message']

@login_required
@instance_perm
def received_spreadsheet(request, ai_id):
    logger.debug(request)
    msgs = SMS_message_received.objects.filter(ai = ai_id).order_by('-time')
    return ExcelResponse(msgs, headers = SPREADSHEET_HEADERS, output_name = 'SMS_Received')


@login_required
@instance_perm
def load_sms_templates(request, ai_id):
    logger.debug(request)
    templates = SMS_template.objects.filter(ai = ai_id)

    js = serializers.get_serializer("json")()
    data = js.serialize(templates)

    return HttpResponse(data)

@login_required
@csrf_exempt
@require_POST
def send_sms_to_numbers(request,ai_id):
    logger.debug(request)
    logger.debug('cn ai id:' + str(ai_id))
    params = json.loads(request.POST['json'])[0]
    nums = []
    try:
        nums = params['numbers'].split(',')
        message = params['message']
        
        send_multiple_topex_sms(ai_id=ai_id, numbers=nums, message=message)
    except Exception, e:
        logger.debug('exception in send_sms_to_numbers' + str((e)))
    
    return HttpResponse("ok")

@login_required
@csrf_exempt
@require_POST
def send_sms_to_contact_list(request,ai_id):
    logger.debug(request)
    logger.debug('cl ai id:' + str(ai_id))
    
    params = json.loads(request.POST['json'])[0]
    contact_list_id = int(params['contact_list_id'])
    message = params['message']
    nums = []
    try:
        contact_list = Contact_list.objects.get(pk = contact_list_id)
        for contact in contact_list.contacts.values():
            nums.append(contact['number'])
        
        send_multiple_topex_sms(ai_id=ai_id, numbers=nums, message=message)
    except Exception, e:
        logger.debug('send_sms_to_contact_list exception' + str((e)))
    
    return HttpResponse("ok")


def get_message_attributes(dictionary):    
        number = dictionary.get('msisdn', None)
        message = dictionary.get('content', None)
        provider = dictionary.get('provider',None)
        circle = dictionary.get('circle',None)
        return (number,message,provider,circle)
    

def sms_handler(request,app_name):
    
    
    logger.info('get params:' + str(request.GET) + ', app_name:' + app_name)
    number,message,provider,circle = get_message_attributes(request.GET)
    logger.info("number:" + str(number) + ", message:" + str(message))
    notification = "this is a generic message"
    sms_ai_app = None
    contact, created = SMS_contact.objects.get_or_create(number = number)
    
    if created:
        contact.provider = provider
        contact.circle = circle
        contact.save() 
    
    try:
        sms_ai_app = SMS_ai_app.objects.get(handle = app_name.lower())        
    except Exception as e:
        logger.info('sms handler: failure getting the entry of handle:' + app_name)
        notification = 'sms handler: failure getting the entry of handle:' + app_name
            
    if sms_ai_app:
                
        if not message:
            notification = 'Please type in a message'
        else:
            try:                                            
                ai = sms_ai_app.ai            
                sms_log = SMS_log(sms_ai = sms_ai_app, contact = contact, is_incoming = True, message = message)
                sms_log.save()
                app = load_sms_handler(ai)
                notification = app.in_handler(ai, app_name, number, message)
                           
            except Exception as e:
                logger.info('sms handler exception:' + str(e))
                return Http404
        
        sms_log = SMS_log(sms_ai = sms_ai_app, contact = contact, is_incoming = False, message = notification)
        sms_log.save()
               
    
    logger.info('notification being sent is:' + str(notification))
    return HttpResponse(notification)
