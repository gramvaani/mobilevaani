from app_manager.models import Cdr, Transition_event, App_instance
from sms.models import SMS_message
from vapp.utils import get_total_seconds, load_vapp_module_for_ai, generate_workbook, daterange
from vapp.telephony.statemachine import SMA

import math
from datetime import timedelta


CALL_DATA_HEADING = ['Call ID', 'Caller ID', 'Date/Time', 'Duration']

class Call_data():
    def __init__(self, call_id, caller_id, date_time, duration):
        self.call_id = call_id
        self.caller_id = caller_id
        self.date_time = date_time
        self.duration = duration
        self.last_sent = None

    def __iter__(self):
        return self

    def next(self):
        if self.last_sent == None:
            self.last_sent = 'call_id'
            return self.call_id
        elif self.last_sent == 'call_id':
            self.last_sent = 'caller_id'
            return self.caller_id
        elif self.last_sent == 'caller_id':
            self.last_sent = 'date_time'
            return self.date_time
        elif self.last_sent == 'date_time':
            self.last_sent = 'duration'
            return self.duration
        else:
            self.last_sent = None
            raise StopIteration

SMS_DATA_HEADING = ['Date/Time', 'Caller ID', 'Message']
class SMS_data():
    def __init__(self, date_time, caller_id, message):
        self.date_time = date_time
        self.caller_id = caller_id
        self.message = message
        self.last_sent = None

    def __iter__(self):
        return self

    def next(self):
        if self.last_sent == None:
            self.last_sent = 'date_time'
            return self.date_time
        elif self.last_sent == 'date_time':
            self.last_sent = 'caller_id'
            return self.caller_id
        elif self.last_sent == 'caller_id':
            self.last_sent = 'message'
            return self.message
        else:
            self.last_sent = None
            raise StopIteration


class Billing_summary():
    def __init__(self):
        self.count = 0
        self.duration = 0
        self.units = 0

class Billing_detail():
    def __init__(self):
        self.header_row_count = 1 
        self.data_heading = [ CALL_DATA_HEADING ]
        self.data = {}

    def pop(self, key, default):
        return self.data.pop(key, default)

    def get_data_for_workbook(self):
        return self.data_heading + self.data.values()


class Billing_info():

    BILLING_TYPE_OPTIONS = (
        ('OS', 'Outgoing SMS'),
        ('OC', 'Outgoing Calls'),
        ('IC', 'Incoming Calls'),
        )

    def __init__(self):
        self.name = None
        self.unit_size = 60
        self.discount_type = None
        self.billing_type = None
        self.discount_values = None

        self.summary = Billing_summary()
        self.detail = Billing_detail()

        for bill_option in self.BILLING_TYPE_OPTIONS:
            setattr( self, bill_option[0], bill_option[0] )

    def __str__(self):
        return "%s\t\t\t%s\t%s\t%s" % (self.name, self.summary.count, self.summary.units, self.unit_size)

    def remove_call_data(self, cdr_id):
        call_data = self.detail.pop(cdr_id, None)
        if call_data:
            self.summary.count -= 1
            self.summary.duration -= call_data.duration
            self.summary.units -= seconds_to_units(call_data.duration, self.unit_size)


class Cdr_data():
    def __init__(self, id, callerid, start_time = None, end_time = None):
        self.id = id
        self.callerid = callerid
        self.start_time = start_time
        self.end_time = end_time

def prune_cdr_datas(cdr_datas):
    pruned = {}
    for k, v in cdr_datas.items():
        if v.start_time and v.end_time and v.start_time < v.end_time:
            pruned[k] = v
    return pruned

def get_cdrdata_from_events(trans_events):
    cdr_datas = {}
    for event in trans_events:
        cdr_data = cdr_datas.get(event.cdr.id, None)
        if not cdr_data:
            cdr_data = Cdr_data(event.cdr.id, event.cdr.callerid)
            cdr_data.last_state = 0
            cdr_datas[event.cdr.id] = cdr_data

        if not cdr_data.start_time and event.state == 'welcome':
            cdr_data.start_time = event.time
        if event.state_counter > cdr_data.last_state and (not cdr_data.end_time or cdr_data.end_time < event.time):
            cdr_data.last_state = event.state_counter
            cdr_data.end_time = event.time

    return prune_cdr_datas(cdr_datas)


def get_cdrdata_from_cdrs(cdrs):
    cdr_datas = {}
    for cdr in cdrs:
        cdr_datas[cdr.id] = Cdr_data(cdr.id, cdr.callerid, cdr.answered_time, cdr.end_time)
    return prune_cdr_datas(cdr_datas)


def get_cdr_data(startdate, enddate, ai, composite, is_incoming=None):
    trans_events = Transition_event.objects.filter(ai=ai, time__gte=startdate,
                                                   time__lt=enddate,
                                                   cdr__answered_time__isnull=False).\
                                                   order_by('cdr',
                                                            'state_counter')
    if is_incoming is not None:
        trans_events = trans_events.filter(cdr__is_incoming=is_incoming)
    cdr_datas = get_cdrdata_from_events(trans_events)
    return cdr_datas


def generate_workbook_for_infos(infos, outfile):
    details = []
    for info in infos:
        if info.summary.count > 0:
            details.append({
                    'name': info.name,
                    'header_row_count': info.detail.header_row_count,
                    'data': info.detail.get_data_for_workbook()
                    })
    if details:
        generate_workbook(details, outfile)


def generate_billing_info(ai_id, start, end, composite, outfile, unit_size = 60, excludes = [],  generate_detail = False):
    ai = App_instance.objects.get(pk = ai_id)
    billing = load_vapp_module_for_ai(ai, 'billing')
    if not billing or not hasattr(billing, 'get_billing_info'):
        return None
    infos = billing.get_billing_info(start, end, ai, composite, unit_size = unit_size, excludes = excludes)
    if generate_detail:
        generate_workbook_for_infos(infos, outfile)
    return infos

def seconds_to_units(duration, unit_size):
    return math.ceil(float(duration)/unit_size)

def get_common_billing_infos(startdate, enddate, ai, composite, unit_size = 60, excludes = []):
    infos = []
    incoming_info = get_incoming_billing_info(startdate, enddate, ai, composite, unit_size)
    infos.append(incoming_info)

    outbound_info = get_outbound_billing_info(startdate, enddate, ai, composite, unit_size)
    infos.append(outbound_info)

    sms_info = get_sms_billing_info(ai, startdate, enddate)
    infos.append(sms_info)

    exclusion_infos = get_common_exclusion_info(startdate, enddate, ai, excludes, composite, unit_size)
    infos.extend(exclusion_infos)

    return infos


def get_first_n_cdr_data(cdr_datas, n):
    return dict([(k, cdr_datas[k]) for k in sorted(cdr_datas.keys())[:n]])


def get_sms_billing_info(ai, startdate, enddate):
    info = Billing_info()
    info.name = "Outgoing SMS"
    info.billing_type = info.OS
    info.unit_size = 1
    info.detail.data_heading = [SMS_DATA_HEADING]

    smses = SMS_message.objects.filter(ai = ai, sent_time__range = (startdate, enddate))
    for sms in smses:
        info.summary.count += 1
        info.detail.data[sms.id] = SMS_data(sms.sent_time, sms.receiver_id, sms.message)

    info.summary.units = math.ceil(float(info.summary.count)/info.unit_size)

    return info


def get_dailycallcount_exclusion_info(startdate, enddate, ai, count, composite, unit_size):
    info = Billing_info()
    info.name = 'Daily Calls Discount'
    info.unit_size = unit_size

    for dt in daterange(startdate, enddate):
        cdr_datas = get_cdr_data(dt, dt + timedelta(days=1), ai, composite)
        cdr_datas = get_first_n_cdr_data(cdr_datas, int(count))
        summary, detail = get_summary_detail(cdr_datas, unit_size)

        info.detail.data.update(detail.data)
        info.summary.count += summary.count
        info.summary.duration += summary.duration
        info.summary.units += summary.units

    return info


def get_callerids_exclusion_info(startdate, enddate, ai, callerids, composite, unit_size):
    info = Billing_info()
    info.name = 'Caller IDs Discount'
    info.unit_size = unit_size

    cdr_datas = get_cdr_data(startdate, enddate, ai, composite)
    pruned_cdr_datas = {}
    for cdr_id in cdr_datas:
        if cdr_datas[cdr_id].callerid in callerids:
            pruned_cdr_datas[cdr_id] = cdr_datas[cdr_id]

    info.summary, info.detail = get_summary_detail(pruned_cdr_datas, unit_size)
    return info


def get_common_exclusion_info(startdate, enddate, ai, excludes, composite, unit_size):
    exclusion_infos = []

    ex_dailycallcounts = [ exclude for exclude in excludes if exclude.discount_type == exclude.DC ]
    for ex_dailycallcount in ex_dailycallcounts:
        info = get_dailycallcount_exclusion_info(startdate, enddate, ai, ex_dailycallcount.get_formatted_values(), composite, unit_size)
        info.discount_type = ex_dailycallcount.DC
        info.discount_values = ex_dailycallcount.discount_values
        exclusion_infos.append(info)

    ex_callerids = [ exclude for exclude in excludes if exclude.discount_type == exclude.CI ]
    for ex_callerid in ex_callerids:
        info = get_callerids_exclusion_info(startdate, enddate, ai, ex_callerid.get_formatted_values(), composite, unit_size)
        info.discount_type = ex_callerid.CI
        info.discount_values = ex_callerid.discount_values
        exclusion_infos.append(info)

    return exclusion_infos


def get_incoming_billing_info(startdate, enddate, ai, composite, unit_size):
    info = Billing_info()
    info.name = 'Incoming Calls'
    info.unit_size = unit_size
    info.billing_type = info.IC

    cdr_datas = get_cdr_data(startdate, enddate, ai, composite, is_incoming = True)
    info.summary, info.detail = get_summary_detail(cdr_datas, unit_size)
    return info


def get_outbound_billing_info(startdate, enddate, ai, composite, unit_size):
    info = Billing_info()
    info.name = 'Outbound Calls'
    info.unit_size = unit_size
    info.billing_type = info.OC

    cdr_datas = get_cdr_data(startdate, enddate, ai, composite, is_incoming = False)
    info.summary, info.detail = get_summary_detail(cdr_datas, unit_size)
    return info

def get_summary_detail(cdr_datas, unit_size):
    summary = Billing_summary()
    detail = Billing_detail()

    for cdr in cdr_datas.values():
        summary.count += 1
        dur = get_total_seconds(cdr.end_time - cdr.start_time)
        summary.duration += dur
        units = seconds_to_units(dur, unit_size)
        summary.units += units
        detail.data[cdr.id] = Call_data(cdr.id, cdr.callerid, cdr.start_time, dur)

    return (summary, detail)


def remove_exclusion_duplicates(infos):
    for i in range(len(infos) - 1):
        info1 = infos[i]
        if not info1.discount_type:
            continue

        for j in range(i+1, len(infos)):
            info2 = infos[j]
            if not info2.discount_type:
                continue
            remove_duplicates(info1, info2)


def remove_duplicates(info1, info2):
    info1_datas = dict(info1.detail.data)

    for key in info1_datas:
        info1_data = info1_datas[key]
        info2_data = info2.detail.data.get(key)
        if info2_data and isinstance(info2_data, Call_data):
            if float(info1_data.duration) > float(info2_data.duration):
                info2.remove_call_data(key)
            else:
                info1.remove_call_data(key)
