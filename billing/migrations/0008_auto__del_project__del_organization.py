# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Project'
        db.delete_table(u'billing_project')

        # Removing M2M table for field campaigns on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_campaigns'))

        # Removing M2M table for field stats_emails on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_stats_emails'))

        # Removing M2M table for field ais on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_ais'))

        # Removing M2M table for field contact_persons on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_contact_persons'))

        # Removing M2M table for field ads on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_ads'))

        # Deleting model 'Organization'
        db.delete_table(u'billing_organization')


    def backwards(self, orm):
        # Adding model 'Project'
        db.create_table(u'billing_project', (
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('org', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billing.Organization'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('contract', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('bd_contact', self.gf('django.db.models.fields.related.ForeignKey')(related_name='bd_user', to=orm['auth.User'])),
            ('project_value', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'billing', ['Project'])

        # Adding M2M table for field campaigns on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_campaigns')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('campaign', models.ForeignKey(orm[u'campaign.campaign'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'campaign_id'])

        # Adding M2M table for field stats_emails on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_stats_emails')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'user_id'])

        # Adding M2M table for field ais on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_ais')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('app_instance', models.ForeignKey(orm[u'app_manager.app_instance'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'app_instance_id'])

        # Adding M2M table for field contact_persons on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_contact_persons')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'user_id'])

        # Adding M2M table for field ads on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_ads')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('advertisement', models.ForeignKey(orm[u'advert.advertisement'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'advertisement_id'])

        # Adding model 'Organization'
        db.create_table(u'billing_organization', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('address', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'billing', ['Organization'])


    models = {
        u'advert.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'ad_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ad_recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'billing.ad_billing_info': {
            'Meta': {'object_name': 'Ad_billing_info'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Advertisement']"}),
            'detail_xls': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'impressions': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'leads': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'billing.ai_billing_info': {
            'Meta': {'object_name': 'Ai_billing_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'billing_policy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Ai_billing_policy']"}),
            'call_unit_size': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'detail_xls': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'discount_info': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['billing.Discount_info']", 'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incoming_call_units': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'incoming_calls': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'outgoing_call_units': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'outgoing_calls': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'outgoing_sms': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'billing.ai_billing_policy': {
            'Meta': {'object_name': 'Ai_billing_policy'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'call_unit_size': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'composite_billing': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'discounts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['billing.Discount']", 'symmetrical': 'False', 'blank': 'True'}),
            'generate_detail': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.campaign_billing_info': {
            'Meta': {'object_name': 'Campaign_billing_info'},
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'heard_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inhouse_contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'ugc_contribs': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'unit_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '30'})
        },
        u'billing.discount': {
            'Meta': {'object_name': 'Discount'},
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'discount_values': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.discount_info': {
            'Meta': {'object_name': 'Discount_info'},
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'discount_values': ('django.db.models.fields.TextField', [], {}),
            'discounted_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'abstract': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'cover_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cover_image'", 'null': 'True', 'to': u"orm['media.Image_caption_map']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'images'", 'symmetrical': 'False', 'to': u"orm['media.Image_caption_map']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'show_in_ui': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'media.image': {
            'Meta': {'object_name': 'Image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'media.image_caption_map': {
            'Meta': {'object_name': 'Image_caption_map'},
            'caption': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Image']"})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['billing']