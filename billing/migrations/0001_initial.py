# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Organization'
        db.create_table(u'billing_organization', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('address', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'billing', ['Organization'])

        # Adding model 'Project'
        db.create_table(u'billing_project', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('org', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billing.Organization'])),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('contract', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('project_value', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('bd_contact', self.gf('django.db.models.fields.related.ForeignKey')(related_name='bd_user', to=orm['auth.User'])),
        ))
        db.send_create_signal(u'billing', ['Project'])

        # Adding M2M table for field contact_persons on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_contact_persons')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'user_id'])

        # Adding M2M table for field ais on 'Project'
        m2m_table_name = db.shorten_name(u'billing_project_ais')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'billing.project'], null=False)),
            ('app_instance', models.ForeignKey(orm[u'app_manager.app_instance'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'app_instance_id'])

        # Adding model 'Discount'
        db.create_table(u'billing_discount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('discount_type', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('discount_values', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'billing', ['Discount'])

        # Adding model 'Ai_billing_policy'
        db.create_table(u'billing_ai_billing_policy', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('call_unit_size', self.gf('django.db.models.fields.IntegerField')(default=30)),
            ('composite_billing', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'billing', ['Ai_billing_policy'])

        # Adding M2M table for field discounts on 'Ai_billing_policy'
        m2m_table_name = db.shorten_name(u'billing_ai_billing_policy_discounts')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ai_billing_policy', models.ForeignKey(orm[u'billing.ai_billing_policy'], null=False)),
            ('discount', models.ForeignKey(orm[u'billing.discount'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ai_billing_policy_id', 'discount_id'])

        # Adding model 'Discount_info'
        db.create_table(u'billing_discount_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('discount_type', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('discount_values', self.gf('django.db.models.fields.TextField')()),
            ('discounted_units', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'billing', ['Discount_info'])

        # Adding model 'Ai_billing_info'
        db.create_table(u'billing_ai_billing_info', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('billing_policy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billing.Ai_billing_policy'])),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('call_unit_size', self.gf('django.db.models.fields.IntegerField')(default=30)),
            ('outgoing_calls', self.gf('django.db.models.fields.IntegerField')()),
            ('outgoing_call_units', self.gf('django.db.models.fields.IntegerField')()),
            ('incoming_calls', self.gf('django.db.models.fields.IntegerField')()),
            ('incoming_call_units', self.gf('django.db.models.fields.IntegerField')()),
            ('outgoing_sms', self.gf('django.db.models.fields.IntegerField')()),
            ('discount_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billing.Discount_info'])),
        ))
        db.send_create_signal(u'billing', ['Ai_billing_info'])


    def backwards(self, orm):
        # Deleting model 'Organization'
        db.delete_table(u'billing_organization')

        # Deleting model 'Project'
        db.delete_table(u'billing_project')

        # Removing M2M table for field contact_persons on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_contact_persons'))

        # Removing M2M table for field ais on 'Project'
        db.delete_table(db.shorten_name(u'billing_project_ais'))

        # Deleting model 'Discount'
        db.delete_table(u'billing_discount')

        # Deleting model 'Ai_billing_policy'
        db.delete_table(u'billing_ai_billing_policy')

        # Removing M2M table for field discounts on 'Ai_billing_policy'
        db.delete_table(db.shorten_name(u'billing_ai_billing_policy_discounts'))

        # Deleting model 'Discount_info'
        db.delete_table(u'billing_discount_info')

        # Deleting model 'Ai_billing_info'
        db.delete_table(u'billing_ai_billing_info')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'billing.ai_billing_info': {
            'Meta': {'object_name': 'Ai_billing_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'billing_policy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Ai_billing_policy']"}),
            'call_unit_size': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'discount_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Discount_info']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incoming_call_units': ('django.db.models.fields.IntegerField', [], {}),
            'incoming_calls': ('django.db.models.fields.IntegerField', [], {}),
            'outgoing_call_units': ('django.db.models.fields.IntegerField', [], {}),
            'outgoing_calls': ('django.db.models.fields.IntegerField', [], {}),
            'outgoing_sms': ('django.db.models.fields.IntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'billing.ai_billing_policy': {
            'Meta': {'object_name': 'Ai_billing_policy'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'call_unit_size': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'composite_billing': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'discounts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['billing.Discount']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.discount': {
            'Meta': {'object_name': 'Discount'},
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'discount_values': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.discount_info': {
            'Meta': {'object_name': 'Discount_info'},
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'discount_values': ('django.db.models.fields.TextField', [], {}),
            'discounted_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.organization': {
            'Meta': {'object_name': 'Organization'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'billing.project': {
            'Meta': {'object_name': 'Project'},
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'bd_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'bd_user'", 'to': u"orm['auth.User']"}),
            'contact_persons': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'contact_users'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'contract': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'org': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Organization']"}),
            'project_value': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['billing']