# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Ai_billing_policy.generate_detail'
        db.add_column(u'billing_ai_billing_policy', 'generate_detail',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Deleting field 'Ai_billing_info.discount_info'
        db.delete_column(u'billing_ai_billing_info', 'discount_info_id')

        # Adding field 'Ai_billing_info.detail_xls'
        db.add_column(u'billing_ai_billing_info', 'detail_xls',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding M2M table for field discount_info on 'Ai_billing_info'
        m2m_table_name = db.shorten_name(u'billing_ai_billing_info_discount_info')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ai_billing_info', models.ForeignKey(orm[u'billing.ai_billing_info'], null=False)),
            ('discount_info', models.ForeignKey(orm[u'billing.discount_info'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ai_billing_info_id', 'discount_info_id'])


    def backwards(self, orm):
        # Deleting field 'Ai_billing_policy.generate_detail'
        db.delete_column(u'billing_ai_billing_policy', 'generate_detail')


        # User chose to not deal with backwards NULL issues for 'Ai_billing_info.discount_info'
        raise RuntimeError("Cannot reverse this migration. 'Ai_billing_info.discount_info' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Ai_billing_info.discount_info'
        db.add_column(u'billing_ai_billing_info', 'discount_info',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billing.Discount_info']),
                      keep_default=False)

        # Deleting field 'Ai_billing_info.detail_xls'
        db.delete_column(u'billing_ai_billing_info', 'detail_xls')

        # Removing M2M table for field discount_info on 'Ai_billing_info'
        db.delete_table(db.shorten_name(u'billing_ai_billing_info_discount_info'))


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'billing.ai_billing_info': {
            'Meta': {'object_name': 'Ai_billing_info'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'billing_policy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Ai_billing_policy']"}),
            'call_unit_size': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'detail_xls': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'discount_info': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['billing.Discount_info']", 'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incoming_call_units': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'incoming_calls': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'outgoing_call_units': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'outgoing_calls': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'outgoing_sms': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'billing.ai_billing_policy': {
            'Meta': {'object_name': 'Ai_billing_policy'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'call_unit_size': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'composite_billing': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'discounts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['billing.Discount']", 'symmetrical': 'False'}),
            'generate_detail': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.discount': {
            'Meta': {'object_name': 'Discount'},
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'discount_values': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.discount_info': {
            'Meta': {'object_name': 'Discount_info'},
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'discount_values': ('django.db.models.fields.TextField', [], {}),
            'discounted_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'billing.organization': {
            'Meta': {'object_name': 'Organization'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'billing.project': {
            'Meta': {'object_name': 'Project'},
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app_manager.App_instance']", 'null': 'True', 'blank': 'True'}),
            'bd_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'bd_user'", 'to': u"orm['auth.User']"}),
            'contact_persons': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'contact_users'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'contract': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'org': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Organization']"}),
            'project_value': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['billing']