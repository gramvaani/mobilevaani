from django.contrib import admin
from django.utils.html import format_html

from vapp import settings
from models import *

admin.site.register(Ai_billing_policy)
admin.site.register(Discount)

class AiBillingInfoAdmin(admin.ModelAdmin):
    def discount_units(self, info):
        return sum( [ discount.discounted_units for discount in info.discount_info.all() ] )

    def details_url(self, info):
        return format_html('<a href="{0}{1}{2}">Download</a>', settings.JMR_SITE, settings.MEDIA_URL, info.detail_xls)

    details_url.allow_tags = True

    list_display = ('ai', 'start', 'end', 'incoming_call_units', 'outgoing_call_units', 'outgoing_sms', 'discount_units', 'details_url')
    search_fields = ['ai__id', 'ai__name']
    list_filter = ('start',)
admin.site.register(Ai_billing_info, AiBillingInfoAdmin)

class AdBillingInfoAdmin(admin.ModelAdmin):
    def details_url(self, info):
        return format_html('<a href="{0}{1}{2}">Download</a>', settings.JMR_SITE, settings.MEDIA_URL, info.detail_xls)
            
    list_display = ('ad', 'start', 'end', 'impressions', 'leads', 'details_url')
    list_filter = ('start',)
    search_fields = ['ad__ad_name']
admin.site.register(Ad_billing_info, AdBillingInfoAdmin)

class CampaignBillingInfoAdmin(admin.ModelAdmin):
    list_display = ('campaign', 'start', 'end', 'inhouse_contribs', 'ugc_contribs', 'heard_units')
    list_filter = ('start',)
    search_fields = ['campaign__name']
admin.site.register(Campaign_billing_info, CampaignBillingInfoAdmin)
