from django.db import models
from django.contrib.auth.models import User
from app_manager.models import App_instance

from campaign.models import Campaign
from advert.models import Advertisement

from datetime import datetime

class Organization(models.Model):
    name = models.CharField(max_length = 100 )
    address = models.TextField( null = True, blank = True )

    def __unicode__(self):
        return self.name 

class Project(models.Model):
    name = models.CharField( max_length = 100 )
    org = models.ForeignKey( Organization )    
    start_date = models.DateField()
    end_date = models.DateField()
    contract = models.FileField( upload_to = 'contracts', null = True, blank = True )
    project_value = models.DecimalField( max_digits = 10, decimal_places = 2, null = True, blank = True )
    contact_persons = models.ManyToManyField( User, null = True, blank = True, related_name = 'contact_users' )
    bd_contact = models.ForeignKey( User, related_name = 'bd_user' )
    ais = models.ManyToManyField( App_instance, null = True, blank = True )
    campaigns = models.ManyToManyField( Campaign, null = True, blank = True )
    ads = models.ManyToManyField( Advertisement, null = True, blank = True )
    stats_emails = models.ManyToManyField( User, null = True, blank = True )

    def is_active(self):
        today = datetime.now().date()
        return (self.start_date <= today) and (self.end_date > today)

    def __unicode__(self):
        return self.name
        
class Discount(models.Model):
    DISCOUNT_OPTIONS = (
        ('CI', 'Caller Ids'),
        ('TO', 'Tags ORed'),
        ('DC', 'Daily Call Count'),
        ('CH', 'Channels'),
    )
    discount_type = models.CharField( max_length = 2, choices = DISCOUNT_OPTIONS )
    discount_values = models.TextField()

    def __unicode__(self):
        return unicode(self.discount_type) + '_' + unicode(self.discount_values)
    
    def __init__( self, *args, **kwargs ):
        super(Discount, self).__init__(*args, **kwargs)
        for attr in [ opt[0] for opt in self.DISCOUNT_OPTIONS ]:
            setattr( self, attr, attr )

    def get_formatted_values( self ):
        if self.discount_type == self.DC:
            return int( self.discount_values )
        return map( lambda str: str.strip(), self.discount_values.split(',') )

class Ai_billing_policy(models.Model):
    ai = models.ForeignKey( App_instance )
    call_unit_size = models.IntegerField( default = 30 )
    composite_billing = models.BooleanField()
    discounts = models.ManyToManyField( Discount, blank = True )
    generate_detail = models.BooleanField( default = False )

    def __unicode__(self):
        return unicode(self.ai) + '_Policy'

class Discount_info(models.Model):
    discount_type = models.CharField( max_length = 2, choices = Discount.DISCOUNT_OPTIONS )
    discount_values = models.TextField()
    discounted_units = models.PositiveIntegerField()

    def __unicode__(self):
        return "{0}".format(self.discounted_units)
        
        
class Ai_billing_info(models.Model):
    ai = models.ForeignKey( App_instance )
    billing_policy = models.ForeignKey( Ai_billing_policy )
    start = models.DateTimeField()
    end = models.DateTimeField()
    call_unit_size = models.IntegerField( default = 30 )
    outgoing_calls = models.IntegerField( default = 0 )
    outgoing_call_units = models.IntegerField(default = 0 )
    incoming_calls = models.IntegerField( default = 0 )
    incoming_call_units = models.IntegerField( default = 0 )
    outgoing_sms = models.IntegerField( default = 0 )
    discount_info = models.ManyToManyField( Discount_info, null = True, blank = True )
    detail_xls = models.FileField( upload_to = 'reports/billing', null = True, blank = True )


    def __unicode__(self):
        return unicode(self.ai)+ '_' + unicode(self.billing_policy)

class Ad_billing_info(models.Model):
    ad = models.ForeignKey(Advertisement)
    start = models.DateTimeField()
    end = models.DateTimeField()
    impressions = models.PositiveIntegerField()
    leads = models.PositiveIntegerField()
    detail_xls = models.FileField( upload_to = 'reports/billing', null = True, blank = True)

    def __unicode__(self):
        return "{0} - {1} impressions, {2} leads".format(self.ad, self.impressions, self.leads)


class Campaign_billing_info(models.Model):
    campaign = models.ForeignKey(Campaign)
    start = models.DateTimeField()
    end = models.DateTimeField()
    inhouse_contribs = models.PositiveIntegerField()
    ugc_contribs = models.PositiveIntegerField()
    heard_units = models.PositiveIntegerField()
    unit_size = models.PositiveIntegerField( default = 30 )

    def __unicode__(self):
        return unicode(self.campaign) + ": From {0} to {1}".format(self.start, self.end)
    
