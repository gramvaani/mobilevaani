from celery.task import Task

from billing.models import *
from billing.utils import generate_billing_info, seconds_to_units
from customer.models import Project
from app_manager.models import App_instance

from campaign.stats import get_campaign_items

from mnews.models import News
from mnews.stats import get_items_heard_stats

from advert.models import Ad_impression_log, Ad_lead_log
from advert.stats import generate_ad_stats_datasheet

import os
from datetime import date
from django.core.files import File

from log import get_request_logger
logger = get_request_logger()

class GenerateBillingInfoTask(Task):

    def run( self, project_id, start_date, end_date = None, **kwargs ):
        end_date = date.today() if not end_date else end_date

        project = Project.objects.get(pk = project_id)

        self.generate_ais_billing_info(project, start_date, end_date)
        self.generate_campaigns_billing_info(project, start_date, end_date)
        self.generate_ads_billing_info(project, start_date, end_date)


    def generate_ais_billing_info(self, project, start, end):

        for ai in project.ais.all():
            policy = Ai_billing_policy.objects.get( ai__id = ai.id )
            discounts = policy.discounts.get_query_set()

            outfile = "/tmp/{0}_{1}_{2}.xlsx".format(ai.id, start, end) if policy.generate_detail else None

            infos = generate_billing_info(ai.id, start, end, policy.composite_billing, outfile = outfile, 
                    unit_size = policy.call_unit_size, excludes = discounts, generate_detail = policy.generate_detail )

            ai_billing_info = Ai_billing_info( ai = ai, billing_policy = policy, start = start, end = end )
            ai_billing_info.save()
            
            self.process_infos( ai.id, ai_billing_info, infos, policy, outfile )
            logger.info('Usage info generated for ai: {0}'.format(ai))

    
    def generate_ads_billing_info(self, project, start, end):
        for ad in project.ads.all():
            impressions = Ad_impression_log.objects.filter(ad_item = ad, time__range = (start, end)).count()
            leads = Ad_lead_log.objects.filter(impression__ad_item = ad, impression__time__range = (start, end)).count()
            outfile = generate_ad_stats_datasheet(ad, start, end)

            ad_billing_info = Ad_billing_info(ad = ad, start = start, end = end, impressions = impressions, leads = leads)
            with open( outfile, 'r') as f:
                detail_file = File(f)
                file_name = os.path.basename(outfile)
                ad_billing_info.detail_xls.save( file_name, detail_file, True )
            ad_billing_info.save()

            logger.info('Usage info generated for ad: {0}'.format(ad))

    
    def generate_campaigns_billing_info(self, project, start, end):
        unit_size = 30 #hardcorded right now as it is quite static. Can move to db in future.
        for campaign in project.campaigns.all():
            inhouse_contribs = get_campaign_items(campaign, News.ContentType.IHC, start, end).count()
            ugc_contribs = get_campaign_items(campaign, News.ContentType.UGC, start, end).count()
            items = get_campaign_items(campaign)
            heard_units = sum([ seconds_to_units(heard.duration, unit_size) for heard in get_items_heard_stats(items, start, end) ])
            Campaign_billing_info(campaign=campaign, start=start, end=end,
                                  ugc_contribs=ugc_contribs,
                                  inhouse_contribs=inhouse_contribs,
                                  heard_units=heard_units).save()
            logger.info("Usage info generated for campaign: {0}".format(campaign))



    def process_infos( self, ai_id, ai_billing_info, infos, policy, outfile ):
        for info in infos:
            if info.discount_type:
                discount_info, created = Discount_info.objects.get_or_create( discount_type = info.discount_type, \
                                                                            discount_values = info.discount_values, \
                                                                            discounted_units = info.summary.units )
                ai_billing_info.discount_info.add( discount_info )
            elif info.billing_type == info.OC:
                ai_billing_info.outgoing_calls = info.summary.count
                ai_billing_info.outgoing_call_units = info.summary.units
            elif info.billing_type == info.OS:
                ai_billing_info.outgoing_sms = info.summary.units
            elif info.billing_type == info.IC:
                ai_billing_info.incoming_calls = info.summary.count
                ai_billing_info.incoming_call_units = info.summary.units
                
            if policy.generate_detail and os.path.exists( outfile ):
                with open( outfile, 'r') as f:
                    detail_file = File(f)
                    file_name = os.path.basename(outfile)
                    ai_billing_info.detail_xls.save( file_name, detail_file, True )
            ai_billing_info.save()

