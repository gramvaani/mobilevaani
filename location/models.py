from django.db import models
from app_manager.models import App_instance
from log import get_request_logger


logger = get_request_logger()


VILLAGE_NAME = 50
COUNTRY_NAME = 50
STATE_NAME = 50
DISTRICT_NAME = 50
BLOCK_NAME = 50


class Country(models.Model):

    name = models.CharField(max_length = COUNTRY_NAME)
    
    def __unicode__(self):        
        return self.name
    
class State(models.Model):
    
    name = models.CharField(max_length = STATE_NAME)
    country = models.ForeignKey(Country, null = True, blank = True)

    def __unicode__(self):        
        return self.name + ' , ' + self.country.__unicode__() 
    
    
class District(models.Model):
    
    name = models.CharField(max_length = DISTRICT_NAME)
    state = models.ForeignKey(State, null = True, blank = True)
    
    def __unicode__(self):        
        return self.name  + ' , ' + self.state.__unicode__()
    
    
class Block(models.Model):
    
    name = models.CharField(max_length = BLOCK_NAME)
    district = models.ForeignKey(District, null = True, blank = True)
    
    def __unicode__(self):        
        return self.name  + ' , ' + self.district.__unicode__()


class Panchayat(models.Model):
    
    name = models.CharField(max_length = BLOCK_NAME)
    block = models.ForeignKey(Block, null = True, blank = True)
    
    def __unicode__(self):        
        return self.name  + ' , ' + self.block.__unicode__()

class Location(models.Model):
    
    village = models.CharField(max_length = VILLAGE_NAME)
    country = models.ForeignKey(Country, related_name = 'country')
    state = models.ForeignKey(State, null = True, blank = True, related_name = 'state')
    district = models.ForeignKey(District, null = True, blank = True, related_name = 'district')
    block = models.ForeignKey(Block, null = True, blank = True, related_name = 'block')
    panchayat = models.ForeignKey(Panchayat, null = True, blank = True, related_name = 'panchayat')
    ai = models.ForeignKey(App_instance, related_name = 'location_ai')
    
    
    def __unicode__(self):        
        loc_arr = []
        if self.state.name != 'Not Known':
            loc_arr.append(self.state.name)
        if self.district.name !='Not Known':
            loc_arr.append(self.district.name)
        if self.block.name != 'Not Known':
            loc_arr.append(self.block.name)
        if self.panchayat:
           if self.panchayat.name != 'Not Known':
              loc_arr.append(self.panchayat.name)
        if self.village != 'Not Known':
            loc_arr.append(self.village)
   
        if self.state.name == 'Not Known' and self.district.name == 'Not Known' and self.block.name == 'Not Known' and self.panchayat.name == 'Not Known' and self.village == 'Not Known':
            loc_arr = ['Not Known']
   
        return str(self.ai.id) + ": " + ', '.join(loc_arr)



class Eicher_delhi(models.Model):        
    location = models.CharField(max_length = 32)




