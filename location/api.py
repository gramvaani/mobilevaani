from django.conf.urls import url

from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields

from datetime import datetime

from models import Location, Country, State, District, Block, Panchayat
from vapp.api import AppInstanceAuthorization
from vapp.app_manager.api import AppInstanceResource
import vapp.app_manager.perms as app_manager_perms
from vapp.perms import user_perms_ai_ids

from customization.models import check_and_get_custom_location

class StateResource( ModelResource ):
    class Meta:
        queryset = State.objects.all()
        resource_name = 'location_state'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class DistrictResource( ModelResource ):
    class Meta:
        queryset = District.objects.all()
        resource_name = 'location_district'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class PanchayatResource( ModelResource ):
    class Meta:
        queryset = Panchayat.objects.all()
        resource_name = 'location_panchayat'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class BlockResource( ModelResource ):
    class Meta:
        queryset = Block.objects.all()
        resource_name = 'location_block'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class LocationResource( ModelResource ):
    ai = fields.ForeignKey( AppInstanceResource, 'ai' )
    state = fields.ForeignKey( StateResource, 'state', null = True, full = True )
    district = fields.ForeignKey( DistrictResource, 'district', null = True, full = True )
    panchayat = fields.ForeignKey( PanchayatResource, 'panchayat', null = True, full = True )
    block = fields.ForeignKey( BlockResource, 'block', null = True, full = True )
    desc = fields.CharField()

    class Meta:
        queryset = Location.objects.all()
        resource_name = "location_location"
        filtering = {
            'ai': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        max_limit = None

    def dehydrate_desc(self, bundle):
        loc = bundle.obj
        if not loc:
            return ""

        ( custom_loc, customization_applied ) = check_and_get_custom_location( loc )
        if customization_applied and custom_loc:
            names = custom_loc
        else:
            names = [ loc.state.name, loc.district.name, loc.block.name, loc.panchayat.name, loc.village ]

        LOCATION_UNKNOWN = "Not Known"
        while LOCATION_UNKNOWN in names:
            names.remove( LOCATION_UNKNOWN )
        return " - ".join( names ) if names else ""

    def obj_get_list(self, bundle, **kwargs):
        filters = {}

        if hasattr(bundle.request, 'GET'):
            filters = bundle.request.GET.copy()
            
        if not "ai" in filters:
            ai_ids = user_perms_ai_ids( bundle.request.user, app_manager_perms.app_use )
            self.ais = ai_ids

        filters.update(kwargs)
        applicable_filters = self.build_filters(filters=filters)

        try:
            objects = self.apply_filters(bundle.request, applicable_filters)
            return self.authorized_read_list(objects, bundle)
        except ValueError:
            raise BadRequest("Invalid resource lookup data provided (mismatched type).")

    def build_filters(self, filters = None):
        if not filters:
            filters = {}
        orm_filters = super( LocationResource, self ).build_filters(filters)
        if not "ai" in filters:
            orm_filters[ "ai__in" ] = self.ais
        return orm_filters
