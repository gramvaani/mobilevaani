# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'location_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'location', ['Country'])

        # Adding M2M table for field states on 'Country'
        m2m_table_name = db.shorten_name(u'location_country_states')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('country', models.ForeignKey(orm[u'location.country'], null=False)),
            ('state', models.ForeignKey(orm[u'location.state'], null=False))
        ))
        db.create_unique(m2m_table_name, ['country_id', 'state_id'])

        # Adding model 'State'
        db.create_table(u'location_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Country'], null=True, blank=True)),
        ))
        db.send_create_signal(u'location', ['State'])

        # Adding M2M table for field districts on 'State'
        m2m_table_name = db.shorten_name(u'location_state_districts')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('state', models.ForeignKey(orm[u'location.state'], null=False)),
            ('district', models.ForeignKey(orm[u'location.district'], null=False))
        ))
        db.create_unique(m2m_table_name, ['state_id', 'district_id'])

        # Adding model 'District'
        db.create_table(u'location_district', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.State'], null=True, blank=True)),
        ))
        db.send_create_signal(u'location', ['District'])

        # Adding M2M table for field blocks on 'District'
        m2m_table_name = db.shorten_name(u'location_district_blocks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('district', models.ForeignKey(orm[u'location.district'], null=False)),
            ('block', models.ForeignKey(orm[u'location.block'], null=False))
        ))
        db.create_unique(m2m_table_name, ['district_id', 'block_id'])

        # Adding model 'Block'
        db.create_table(u'location_block', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.District'], null=True, blank=True)),
        ))
        db.send_create_signal(u'location', ['Block'])

        # Adding model 'Location'
        db.create_table(u'location_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('village', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='country', to=orm['location.Country'])),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='state', null=True, to=orm['location.State'])),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='district', null=True, to=orm['location.District'])),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='block', null=True, to=orm['location.Block'])),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='location_ai', to=orm['app_manager.App_instance'])),
        ))
        db.send_create_signal(u'location', ['Location'])

        # Adding model 'Eicher_delhi'
        db.create_table(u'location_eicher_delhi', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'location', ['Eicher_delhi'])


    def backwards(self, orm):
        # Deleting model 'Country'
        db.delete_table(u'location_country')

        # Removing M2M table for field states on 'Country'
        db.delete_table(db.shorten_name(u'location_country_states'))

        # Deleting model 'State'
        db.delete_table(u'location_state')

        # Removing M2M table for field districts on 'State'
        db.delete_table(db.shorten_name(u'location_state_districts'))

        # Deleting model 'District'
        db.delete_table(u'location_district')

        # Removing M2M table for field blocks on 'District'
        db.delete_table(db.shorten_name(u'location_district_blocks'))

        # Deleting model 'Block'
        db.delete_table(u'location_block')

        # Deleting model 'Location'
        db.delete_table(u'location_location')

        # Deleting model 'Eicher_delhi'
        db.delete_table(u'location_eicher_delhi')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'states': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'states'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.State']"})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            'blocks': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'blocks'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.Block']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.eicher_delhi': {
            'Meta': {'object_name': 'Eicher_delhi'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            'districts': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'districts'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['location']