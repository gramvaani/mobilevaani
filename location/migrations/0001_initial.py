# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Eicher_delhi'
        db.create_table('location_eicher_delhi', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal('location', ['Eicher_delhi'])


    def backwards(self, orm):
        
        # Deleting model 'Eicher_delhi'
        db.delete_table('location_eicher_delhi')


    models = {
        'location.eicher_delhi': {
            'Meta': {'object_name': 'Eicher_delhi'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['location']
