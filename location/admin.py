from django.contrib import admin
from models import Location, Country, State, District, Block, Panchayat

class LocationAdmin(admin.ModelAdmin):
    search_fields = ['state__name','district__name','block__name','village']

admin.site.register(Location, LocationAdmin)
admin.site.register(Country)
admin.site.register(State)
admin.site.register(District)
admin.site.register(Block)
admin.site.register(Panchayat)
