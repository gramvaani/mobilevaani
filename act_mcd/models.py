from django.db import models

from django.contrib.auth.models import User

from app_manager.models import App_instance
from media.models import Recording
from fblib.models import FB_wall_post, FB_comment

from vapp.utils import datetimefield_to_unix, integer_to_reference

CALLERID_LENGTH = 20

class Zone:
    zones = []

    @classmethod
    def enum(cls):
        cls.SOUTH   = Zone("South")
        cls.CENTRAL = Zone("Central")
        cls.WEST    = Zone("West")
        cls.ROHINI  = Zone("Rohini")
        cls.CIVIL_LINE      = Zone("Civil line")
        cls.SHAHDARA_SOUTH  = Zone("Shahdara South")
        cls.SHAHDARA_NORTH  = Zone("Shahdara North")
        cls.NAJAFGARH       = Zone("Najafgarh")
        cls.CITY            = Zone("City Zone")
        cls.KAROL_BAGH      = Zone("Karol Bagh")
        cls.SADAR_PAHARGANJ = Zone("Sadar Paharganj")
        cls.NARELA          = Zone("Narela")
    
    @classmethod
    def get_zone_choices(cls):
        choices = [(z.name, z.name) for z in cls.zones]
        return tuple(choices)
    
    def __init__(self, name):
        self.name = name
        self.zones.append(self)
    
    def __str__(self):
        return str(self.name)
    
Zone.enum()
    
class Report(models.Model):
    
    STATE_CHOICES = (
        ('NEW', 'New Report'),
        ('ACC', 'Accepted Report'),
        ('CLO', 'Closed Report'),
        ('REJ', 'Rejected Report'),
    )
    
    STATES = dict(STATE_CHOICES).keys()
    
    REPORT_TYPE_CHOICES = (
        ('GAR', 'Dhalao'),
        ('WLU', 'Waterless Urinal'),
        ('URI', 'Public Urinal'),
    )
    
    LANG_CHOICES = (
        ('hin', 'Hindi'),
        ('eng', 'English'),
    )
    
    ai        = models.ForeignKey(App_instance)
    reference = models.CharField(max_length = 16)
    recording = models.ForeignKey(Recording)
    callerid  = models.CharField(max_length = CALLERID_LENGTH)
    opened_time     = models.DateTimeField(auto_now_add = True)
    deadline        = models.DateTimeField()
    resolved_time   = models.DateTimeField(null = True)
    state           = models.CharField(max_length = 3, choices = STATE_CHOICES)
    language        = models.CharField(max_length = 3, choices = LANG_CHOICES, null = True)
    report_type     = models.CharField(max_length = 3, choices = REPORT_TYPE_CHOICES, null = True)
    zone            = models.CharField(max_length = 20, choices = Zone.get_zone_choices(), null = True)
    responsibility  = models.CharField(max_length = 20, null = True)
    comment         = models.CharField(max_length = 120)
    uid             = models.CharField(max_length = 20, null = True)
    
    
    def opened_time_ts(self):
        return datetimefield_to_unix(self.opened_time)
    
    def deadline_ts(self):
        return datetimefield_to_unix(self.deadline)
    
    def auto_ref(self):
        return integer_to_reference(self.id)
    
    def fb_post_id(self):
        try:
            post = FB_wall_post.objects.get(report_fb_post__report = self.id)
            return post.object_id
        except:
            return None
    
    def __unicode__(self):
        return str(self.callerid) + '_' + str(self.opened_time) + '_' + str(self.state)
    
class Report_fb_post(models.Model):
    report      = models.ForeignKey(Report)
    wall_post   = models.ForeignKey(FB_wall_post)

class Report_fb_comment(models.Model):
    report      = models.ForeignKey(Report)
    comment     = models.ForeignKey(FB_comment) 

class User_zone(models.Model):
    user = models.ForeignKey(User)
    zone = models.CharField(max_length = 20, choices = Zone.get_zone_choices())
    
    def __unicode__(self):
        return unicode(self.user) + '_' + unicode(self.zone)            
