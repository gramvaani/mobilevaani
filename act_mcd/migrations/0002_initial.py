# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Report'
        db.create_table(u'act_mcd_report', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('reference', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'])),
            ('callerid', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('opened_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('deadline', self.gf('django.db.models.fields.DateTimeField')()),
            ('resolved_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=3, null=True)),
            ('report_type', self.gf('django.db.models.fields.CharField')(max_length=3, null=True)),
            ('zone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('responsibility', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('uid', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
        ))
        db.send_create_signal(u'act_mcd', ['Report'])

        # Adding model 'Report_fb_post'
        db.create_table(u'act_mcd_report_fb_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('report', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['act_mcd.Report'])),
            ('wall_post', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_wall_post'])),
        ))
        db.send_create_signal(u'act_mcd', ['Report_fb_post'])

        # Adding model 'Report_fb_comment'
        db.create_table(u'act_mcd_report_fb_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('report', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['act_mcd.Report'])),
            ('comment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fblib.FB_comment'])),
        ))
        db.send_create_signal(u'act_mcd', ['Report_fb_comment'])

        # Adding model 'User_zone'
        db.create_table(u'act_mcd_user_zone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('zone', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'act_mcd', ['User_zone'])


    def backwards(self, orm):
        # Deleting model 'Report'
        db.delete_table(u'act_mcd_report')

        # Deleting model 'Report_fb_post'
        db.delete_table(u'act_mcd_report_fb_post')

        # Deleting model 'Report_fb_comment'
        db.delete_table(u'act_mcd_report_fb_comment')

        # Deleting model 'User_zone'
        db.delete_table(u'act_mcd_user_zone')


    models = {
        u'act_mcd.report': {
            'Meta': {'object_name': 'Report'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'deadline': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True'}),
            'opened_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']"}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'report_type': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True'}),
            'resolved_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'responsibility': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'zone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'})
        },
        u'act_mcd.report_fb_comment': {
            'Meta': {'object_name': 'Report_fb_comment'},
            'comment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_comment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['act_mcd.Report']"})
        },
        u'act_mcd.report_fb_post': {
            'Meta': {'object_name': 'Report_fb_post'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['act_mcd.Report']"}),
            'wall_post': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_wall_post']"})
        },
        u'act_mcd.user_zone': {
            'Meta': {'object_name': 'User_zone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'zone': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'fblib.fb_comment': {
            'Meta': {'object_name': 'FB_comment'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'post_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'posted_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_wall_post']"})
        },
        u'fblib.fb_credential': {
            'Meta': {'object_name': 'FB_credential'},
            'access_token': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'app_api_key': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'app_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'app_secret': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'fblib.fb_target': {
            'Meta': {'object_name': 'FB_target'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'fblib.fb_wall_post': {
            'Meta': {'object_name': 'FB_wall_post'},
            'cred': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_credential']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'object_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'post_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'posted_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['fblib.FB_target']"})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['act_mcd']