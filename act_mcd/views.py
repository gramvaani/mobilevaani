from django.http import HttpResponse, HttpResponseNotFound
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth.decorators import login_required

from models import Report, Report_fb_post, Report_fb_comment, User_zone
from vapp.app_manager.decorators import perms_required, instance_perm, instance_perm_per_obj #@UnresolvedImport
from vapp.log import get_request_logger, get_logger

from sms.tasks import SMSTask
from fblib.tasks import WallPostTask
from fblib.models import FB_target

from datetime import datetime
import json
from excel_response import ExcelResponse #@UnresolvedImport

logger = get_request_logger()


@login_required
@instance_perm
def load_reports(request, ai_id, state, start_row, num_rows):
    logger.debug(request)
    json_serializer = serializers.get_serializer("json")()
    
    if not state:
        return HttpResponseNotFound()

    reports = Report.objects.filter(ai = ai_id)
    
    try:
        user_zone = User_zone.objects.get(user = request.user.id).zone
    except:
        user_zone = None
    
    if user_zone:
        reports = reports.filter(zone = user_zone)
    
    if state in Report.STATES:
        reports = reports.filter(state = state)
         
    if start_row is None or num_rows is None:
        data = json.dumps({'num_rows': reports.count()})
        return HttpResponse(data)
    
    start_row = int(start_row)
    num_rows  = int(num_rows)
    end_row   = start_row + num_rows
    
    data = json_serializer.serialize(reports[start_row:end_row], ensure_ascii = False, extras=('auto_ref', 'fb_post_id',))
    return HttpResponse(data)
    
@login_required
@csrf_exempt
@require_POST
@instance_perm_per_obj('ai')
def save_report(request):
    logger.info(request)

    for report_wrapper in serializers.deserialize('json',request.POST['json']):
        push_messages(report_wrapper)
        if report_wrapper.object.state in ['CLO', 'REJ']:
            report_wrapper.object.resolved_time = datetime.now()
        report_wrapper.save()
        
    return HttpResponse("ok")

SMS_ACCEPTED = 'Your voice recording# %s has been accepted. MCD Reference# is %s.'
SMS_REJECTED = 'Your voice recording# %s has been rejected. Reason: %s'
SMS_CLOSED   = 'Your MCD Reference# %s has been closed successfully. %s'

def push_messages(report_wrapper):
    
    # if some changes are made to the states (NEW,ACC,CLO) then make sure
    # that the code below is changed to accomodate the changes.
    logger = get_logger()
    report = Report.objects.get(pk = report_wrapper.object.id)
    new_state = report_wrapper.object.state
    auto_ref = report.auto_ref()

    if new_state == 'ACC' and report.state == 'ACC':
        if report.comment == report_wrapper.object.comment and report.reference == report_wrapper.object.reference:
            logger.debug('Not pushing messages for report %s' % (report.id,))
            return
    
    logger.debug('Pushing messages for report %s' % (report.id,))
    
    msg = None
    if new_state == 'ACC':
        msg = SMS_ACCEPTED % (auto_ref, report_wrapper.object.reference,)        
    elif new_state == 'REJ':
        msg = SMS_REJECTED % (auto_ref, report_wrapper.object.comment,)
    elif new_state == 'CLO':
        msg = SMS_CLOSED % (report.reference, report_wrapper.object.comment,)  
        
        
    if msg:
        try:
            SMSTask.create_send_msg(report.ai_id, msg, report.callerid)
            
            #Only push those voice snippets on MCD's page which have been accepted
            
            if report.state == 'NEW' and new_state == 'ACC':                
                target = FB_target.objects.get(name = 'Main MCD Page')
                post = WallPostTask.create_post_feed(report.ai_id, msg, target.id, report.recording_id, 'Voice Recording# %s'%(auto_ref,))
                Report_fb_post(report_id = report.id, wall_post_id = post.id).save()
                
            elif new_state == 'REJ' or new_state == 'CLO' or (new_state == 'ACC' and report.state == 'ACC'):
                comment = WallPostTask.create_post_comment(report.ai_id, msg, Report_fb_post.objects.get(report = report.id).wall_post_id)
                Report_fb_comment(report_id = report.id, comment_id = comment.id).save()
        except:
            logger.exception('Encountered Exception:')


@login_required
@instance_perm
def report_status(request, ai_id):
    reports = Report.objects.filter(ai = ai_id)
    
    try:
        zone = User_zone.objects.get(user = request.user.id).zone
    except:
        zone = None
    
    if zone:
        reports = reports.filter(zone = zone)

    d = {}
    for state in Report.STATES:
        d[state] = reports.filter(state = state).count()
    return HttpResponse(json.dumps(d))

SPREADSHEET_HEADERS = ['state', 'opened_time', 'resolved_time', 'responsibility', 'zone', 'reference', 'comment']

@login_required
@instance_perm
def report_spreadsheet(request, ai_id):
    reports = Report.objects.filter(ai = ai_id)
    
    try:
        zone = User_zone.objects.get(user = request.user.id).zone
    except:
        zone = None
    
    if zone:
        reports = reports.filter(zone = zone)
        
    return ExcelResponse(reports, headers = SPREADSHEET_HEADERS, output_name = 'voice_complaint_info')

# Data given out without login checks

def report_spreadsheet_csv(request, ai_id):
    return ExcelResponse(Report.objects.filter(ai = ai_id), headers = SPREADSHEET_HEADERS, output_name = 'voice_complaint_info', force_csv = True)
