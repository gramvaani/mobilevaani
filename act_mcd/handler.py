'''
Created on 16-Mar-2011

@author: bchandra
'''

from models import Report, Report_fb_post, Zone

from sms.models import SMS_message, SMS_credential
from sms.tasks import SMSTask
from fblib.models import FB_target
from fblib.tasks import WallPostTask

from vapp.telephony.utils import *
from vapp.log import get_logger

from datetime import datetime, timedelta

logger = get_logger()

ZONE_CHOICES = {'11': Zone.SOUTH, '12': Zone.CENTRAL, '13': Zone.WEST, '14': Zone.ROHINI, #@UndefinedVariable
                '15': Zone.CIVIL_LINE, '16':Zone.SHAHDARA_SOUTH, '17': Zone.SHAHDARA_NORTH, #@UndefinedVariable
                '18': Zone.NAJAFGARH, '19': Zone.CITY, '20': Zone.KAROL_BAGH, #@UndefinedVariable
                '21': Zone.SADAR_PAHARGANJ, '22': Zone.NARELA} #@UndefinedVariable

def in_handler(session, args):
    logger.info("Running RYP")
    
    session.answer()
    one_or_two = '12'

    #Ask caller for hindi/english
    lang_choice = play_and_get(session, 'welcome_choose', one_or_two)
    
    if not lang_choice:
        end_session(session)
        return
    
    lang = {'1': 'eng', '2':'hin'}[lang_choice]
    set_prompt_lang(session, lang)
    play_prompt(session, 'helpline_info')
    
    #type_choice = play_and_get(session, 'garbage_urinal_choice', one_or_two)
    
    #if not type_choice:
    #    end_session(session)
    #    return
    
    valid_zone_choices = ['%s' % (i,) for i in range(11, 23)]
    #zone_choice = play_and_get(session, 'zones', valid_zone_choices, num_digits = 2)
    
    #if not zone_choice:
    #    end_session(session)
    #    return
    
    #prompt_name = {'1':'garbage_instruction', '2':'urinal_instruction'}[type_choice]
    play_prompt(session, 'act_instruction')
    
    #report_type = {'1':'GAR', '2':'URI'}[type_choice]
    #zone = ZONE_CHOICES[zone_choice]
    default_deadline = datetime.now() + timedelta(hours = 24)
    #report = Report(ai_id = session.ai.id, callerid = session.getCallerID(), deadline = default_deadline, state = 'NEW', report_type = report_type)
    
    report = Report(ai_id = session.ai.id, callerid = session.getCallerID(), deadline = default_deadline, state = 'NEW')

    get_embedded_recording(session, 20, report, Report.recording)

    play_prompt(session, 'thank_you')
    session.hangup()

def end_session(session):
    play_prompt(session, 'thank_you')
    session.hangup()

VACT_SENDER_ID = 'vAct'
SMS_MSG_ACK = 'Your voice recording# is %s. Office hrs 10am-5pm. If reported after office hrs, action will be taken by 11am the next working day'    
FB_POST = 'Complaint filed with voice recording# %s'

def push_messages(report):
    auto_ref = report.auto_ref()
    SMSTask.create_send_msg(report.ai_id, SMS_MSG_ACK % (auto_ref,), report.callerid)
    
    post_msg = FB_POST % (auto_ref,)
    target = FB_target.objects.get(name = 'Main MCD Page')
    post = WallPostTask.create_post_feed(report.ai_id, post_msg, target.id, report.recording_id, 'Voice Recording# %s'%(auto_ref,))
    report_post = Report_fb_post(report_id = report.id, wall_post_id = post.id)
    report_post.save()
    
    
