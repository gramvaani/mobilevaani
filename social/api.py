from django.conf.urls import url

from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication, MultiAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields

from datetime import datetime
import ast

from models import *

from vapp.api import AppInstanceAuthorization
from vapp.app_manager.api import AppInstanceResource, UserResource, AnonymousReadAuthentication
from vapp.app_manager.models import App_instance
from vapp.media.api import RecordingResource, PromptAudioResource, recording_upload_impl
from vapp.media.models import Image
import vapp.app_manager.perms as app_manager_perms
from vapp.perms import user_has_perms, user_perms_ai_ids
from social.tasks import *
from social.utils import load_social_module
from vapp.log import get_request_logger

logger = get_request_logger()

class SocialMediaResource(ModelResource):    
    ai = fields.ForeignKey(AppInstanceResource, 'ai')

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/publish_to_social_media%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'publish_to_social_media' ), name = "api_publish_to_social_media" ),
        ]

    def sm_publish( self, call_log, eta = None ):
        eta = datetime.now() if not eta else eta
        if call_log.sm_id.sm_name == 'facebook':
            PublishToFBTask.apply_async( args = [ call_log.id ], eta = eta )
        elif call_log.sm_id.sm_name == 'twitter':
            PublishToTwitterTask.apply_async( args = [ call_log.id ], eta = eta )

    def publish_to_social_media(self, request, **kwargs):
        self.method_check( request, allowed = ['post'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        try:            

	    ai = App_instance.objects.get( id = request.REQUEST[ 'ai_id' ] )
            smedia = SocialMedia.objects.get( ai = ai, sm_name =  request.REQUEST[ 'sm_name' ])
            title = request.POST[ 'title' ]
            message = request.POST[ 'message' ] if request.POST.has_key( 'message' ) else None
            

            if request.REQUEST.has_key( 'kwargs' ):
                app_kwargs = request.REQUEST[ 'kwargs' ]
                app_kwargs = ast.literal_eval( app_kwargs )
            social_module = load_social_module( ai )
            
            if social_module and hasattr( social_module, 'pre_sm_publish' ):                
                url, image_object = social_module.pre_sm_publish( ai, smedia, title, message, app_kwargs )
                if not url:
                    return self.create_response( request, { 'success': False, 'reason': "either already completed or not to be published." } )
                if not image_object:
                    image_object = smedia.background
                        
                pub_log = Sm_publish_request_log(sm_id=smedia, user=request.user,
                                                 title=title, message=message,
                                                 url=url, background_img=image_object,
                                                 request_time=datetime.now())
                pub_log.result_code = pub_log.PR
                pub_log.save()
                self.sm_publish( pub_log )

            return self.create_response( request, { 'success': True, 'reason': "completed the scheduling successfully.", 'state': pub_log.PR } )

        except Exception, e:
            logger.exception(" exception caught in publish_to_social_media: %s" % ( str(e) ) )
            return self.create_response( request, { 'success': False, 'reason': str(e) } )

    class Meta:
        queryset = SocialMedia.objects.all()
        resource_name = 'social_socialmedia'
        filtering = {
            'ai': ALL_WITH_RELATIONS,
        }
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
    
