from django.db import models
from app_manager.models import App_instance
from django.contrib.auth.models import User
from datetime import datetime
from media.models import Image

# Create your models here.
KEY_MAX_LENGTH = 255
CRED_LENGTH = 50
PATH_MAX_LENGTH = 255

class ApiCredentials(models.Model):
    cred_name    = models.CharField( max_length = CRED_LENGTH )
    api_key      = models.CharField( max_length = KEY_MAX_LENGTH )
    api_secret   = models.CharField( max_length = KEY_MAX_LENGTH )
    access_token = models.CharField( max_length = KEY_MAX_LENGTH, null = True, blank = True )
    access_secret = models.CharField( max_length = KEY_MAX_LENGTH, null = True, blank = True )
    username     = models.CharField( max_length = CRED_LENGTH, null = True, blank = True )
    password     = models.CharField( max_length = CRED_LENGTH, null = True, blank = True )

    def __unicode__(self):
        return unicode(self.cred_name) + '_' + unicode(self.api_key)

class SocialMedia(models.Model):
    ai      = models.ForeignKey( App_instance )
    sm_name = models.CharField( max_length = 50 )
    credentials = models.ForeignKey( ApiCredentials, null = True, blank = True )
    background  = models.ForeignKey( Image )
    active  = models.BooleanField( default = True )
    target_id = models.CharField( max_length = KEY_MAX_LENGTH, null = True, blank = True )
    
    def __unicode__(self):
        return self.sm_name + '_' + self.target_id

    @classmethod
    def get_sm_creds( cls, ai ):
        sm_creds_map = {}
        smedias = SocialMedia.objects.filter( ai = ai, active = True )
        for smedia in smedias:
            sm_creds_map[ smedia.sm_name ] = smedia.credentials
        return sm_creds_map


class Sm_publish_request_log(models.Model):
    RESULT_CHOICES = (
            ('PR', 'Processing'),
            ('PB', 'Published'),
            ('FL', 'Failed'),
        )
    sm_id   = models.ForeignKey( SocialMedia )
    user  = models.ForeignKey( User, null = True, blank = True )
    title = models.CharField( max_length = 255, default = 'Title Unavailable' )
    message = models.TextField(null = True, blank = True)
    background_img = models.ForeignKey( Image, null = True, blank = True )
    url = models.CharField( max_length = PATH_MAX_LENGTH, null = True, blank = True)
    last_update_time = models.DateTimeField( default = datetime.now )
    result_code = models.CharField( max_length = 2, choices = RESULT_CHOICES, null = True, blank = True )
    response = models.TextField( null = True, blank = True )
    request_time = models.DateTimeField(auto_now_add=True, default=datetime.now)

    def __init__(self, *args, **kwargs):
        super(Sm_publish_request_log, self).__init__(*args, **kwargs)
        for short_choice, long_choice in self.RESULT_CHOICES:
            setattr(self, short_choice, short_choice)
