from celery.task import Task
from models import *
from app_manager.models import App_instance
from datetime import datetime, timedelta
from utils import *
from log import get_request_logger
logger = get_request_logger()


class PublishToFBTask(Task):

    def run( self, sm_publish_request_log_id ):
        logger.info('Publish to FB task start' +str(sm_publish_request_log_id))
        permalink = None
        sm_creds_map = None
        try:
            api_log = Sm_publish_request_log.objects.get( id = sm_publish_request_log_id )
            recording_url = api_log.url
            background_url = None
            social_module = load_social_module( api_log.sm_id.ai )
            response = None            
            
            sm_creds_map = SocialMedia.get_sm_creds( api_log.sm_id.ai )
            if is_mp3_link( recording_url ):                
                
                if api_log.background_img:
                    background_loc = api_log.background_img.get_location() 
                elif api_log.sm_id.background:
                    background_loc = api_log.sm_id.background.get_location()                     
                else:
                    background_loc = None

                ( retval, video_location ) = create_video_impl( background_loc, recording_url )                
            
            if retval or ( not video_location ):
                raise Exception("failed to create video via ffmpeg: %s" %( str(api_log.id) ) )

            response = post_video_to_fb( sm_creds_map[ 'facebook' ], api_log.title, api_log.message, video_location, api_log.sm_id.target_id )
            if response:
                logger.info('success in posting the following url to facebook: %s' %( recording_url ))
                permalink = get_fb_video_permalink( api_log.sm_id.target_id, response )
                logger.info(str(permalink))
                api_log.response = response
                api_log.result_code = api_log.PB
            else:
                api_log.result_code = api_log.FL
        except Exception,e:
            logger.exception('exception while publishing facebook post:' + str(e))
            api_log.result_code = api_log.FL
        finally:
            api_log.save()
            if social_module and hasattr( social_module, 'post_sm_publish'):
                social_module.post_sm_publish( api_log.sm_id.ai, api_log.sm_id, recording_url, api_log.result_code, permalink )
        
class PublishToTwitterTask(Task):
    
    def run( self, sm_publish_request_log_id ):
        
        sm_creds_map = None
        permalink = None
        
        try:
            api_log = Sm_publish_request_log.objects.get( id = sm_publish_request_log_id )
            recording_url = api_log.url
            sm_creds_map = SocialMedia.get_sm_creds( api_log.sm_id.ai )
            response = post_to_twitter( sm_creds_map[ 'twitter' ], api_log.url, api_log.message )
            if response:
                logger.info('success in posting the following url to twitter (id): %s' %( response ))
                permalink = get_twitter_permalink( sm_creds_map[ 'twitter' ], response )
                api_log.result_code = api_log.PB
                api_log.save()
            else:   
                api_log.result_code = api_log.FL
        except Exception,e:
            logger.exception('exception while publishing twitter:' + str(e))
            api_log.result_code = api_log.FL
        finally:
            api_log.save()
            social_module = load_social_module( api_log.sm_id.ai )
            if social_module and hasattr( social_module, 'post_sm_publish'):
                social_module.post_sm_publish( api_log.sm_id.ai, api_log.sm_id, recording_url, api_log.result_code, permalink )
        


