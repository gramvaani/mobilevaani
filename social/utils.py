import soundcloud
import facebook
import twitter
import requests
import subprocess
import os
import ast
from models import *
from vapp.app_manager.common import load_module

from log import get_request_logger
logger = get_request_logger()

def is_mp3_link(url):
    if url:
        return (url.split(".")[-1] == "mp3")
    return None

def load_social_module( ai ):
        try:
            logger.info('loading social modules.')
            return load_module( ai, 'social' )
        except:
            logger.exception( 'exception loading social module for ai: %s'% ( str(ai.id) ) )
        return None


def get_soundcloud_tracks_details( api_creds ):
    try:
        soundcloud_client = soundcloud.Client(
                client_id = api_creds.api_key,
                client_secret = api_creds.api_secret,
                username = api_creds.username,
                password = api_creds.password,
            )
        tracks = soundcloud_client.get('/me/tracks')
        return tracks
    except Exception,e:
        logger.exception("exception while getting the tracks url: %s" %(str(e)))
    return None

def get_soundcloud_playlists_details( api_creds ):
    try:
        soundcloud_client = soundcloud.Client(
                client_id = api_creds.api_key,
                client_secret = api_creds.api_secret,
                username = api_creds.username,
                password = api_creds.password,
            )
        tracks = soundcloud_client.get('/me/playlists')
        return tracks
    except Exception,e:
        logger.exception("exception while getting the playlists url: %s" %(str(e)))
    return None


def publish_track_to_soundcloud(api_creds, title, recording_location, image_location = None ):
    try:
        soundcloud_client = soundcloud.Client(
                client_id = api_creds.api_key,
                client_secret = api_creds.api_secret,
                username = api_creds.username,
                password = api_creds.password,
            )
        track_details = {
            'title': title, 
            'asset_data': open(recording_location),
            'sharing': 'public',
        }
        if image_location:
            track_details[ 'artwork_data' ] = open(image_location)
        return soundcloud_client.post('/tracks', track = track_details)        
    except Exception, e:
        logger.exception('exception while posting track on soundcloud: %s' % ( str(e) ))
    return None

def generate_source_for_fb( permalink_url ):
    return ('http://player.soundcloud.com/player.swf?url=%s&auto_play=false&'
        'show_artwork=true&visual=true&color=3b5998&origin=facebook' % (permalink_url) )

def post_video_to_fb( api_creds, message, description, video_location, target_id ):
    response = None
    try:
        data = {
            'title': message,
            'description': description,
            'access_token': api_creds.access_token,
        }
        files = {
            'file': open(video_location)
        }
        url = ( 'https://graph.facebook.com/%s/videos' %(target_id) )
        response = requests.post( url, data = data, files = files, allow_redirects = True ) 
        if response.status_code != 200:
            raise Exception( response.reason )
    except Exception,e:
        logger.exception('exception while uploading file:%s' % ( video_location ))
    return response.text

def post_to_fb( api_creds, message, description, uri, image_url, target_id, location = "feed" ):
    response = None
    try:        
        graph = facebook.GraphAPI( api_creds.access_token )
        fb_args = {
            'message': message,
            'source': generate_source_for_fb( uri ),
            'description': message, 
            'name': message,
            'picture': image_url,
        }   

        response = graph.put_object( target_id, location, **fb_args )
        response = response.values()[0]
    except Exception, e:
        logger.exception('exception while publishing post on facebook: %s' %( str(e) ))
    return response

def post_to_twitter( api_creds, url, message ):
    response = None
    try:
        api = twitter.Api(
                consumer_key = api_creds.api_key,
                consumer_secret = api_creds.api_secret,
                access_token_key = api_creds.access_token,
                access_token_secret = api_creds.access_secret,
            )
        tweet = message + ' ' + url
        response = api.PostUpdate( tweet )
        response = response.id
    except Exception, e:
        logger.exception('exception while publishing post on twitter: %s' %( str(e) ))
    return response

def get_fb_video_permalink( target_id, item_response ):
    item_response = ast.literal_eval( item_response )
    item_id = item_response[ 'id' ]
    return ('https://www.facebook.com/%s/posts/%s'% ( target_id, item_id ))

def get_fb_permalink( post_id ):
    target_id, item_id = post_id.split('_')
    return ('https://www.facebook.com/%s/posts/%s'% ( target_id, item_id ))

def get_twitter_permalink( api_creds, post_id ):
    api = twitter.Api(
                consumer_key = api_creds.api_key,
                consumer_secret = api_creds.api_secret,
                access_token_key = api_creds.access_token,
                access_token_secret = api_creds.access_secret,
            )
    user_details = api.VerifyCredentials()
    userid = user_details.GetScreenName()
    return ('https://twitter.com/%s/status/%s'% ( userid, post_id )) 


def create_video_impl( image_location, recording_location ):    

    if not image_location or not recording_location:
        return False

    output_location = '/tmp/' + os.path.basename( recording_location ) + '_' + os.path.basename( image_location ) + '.avi'
    if os.path.isfile(output_location):
        logger.info('the output_location file already exists, removing it.')
        os.remove(output_location)
        logger.info('previous output_location file remove successfully.')

    logger.debug('create_video_impl: ' + str(recording_location) + ' -> ' + str(output_location))

    
    ffmpegString = "ffmpeg -loop 1 -r 1 -i " + image_location + " -i " + recording_location + " -c:a copy -shortest " + output_location
    
    retval = 1
    return_location = None

    try:
        retval = subprocess.call(ffmpegString.strip().split(' '))
        if os.path.isfile(output_location):
            return_location = output_location
    except OSError, o:
        logger.error('OSError: ' + str(o))
    except ValueError:
        logger.error("ValueError: Couldn't call FFMPEG with these parameters")
    return ( retval, return_location )


def publish_playlist_to_soundcloud( api_creds, title, sharing = 'public', image_location = None, playlist_id = None, tracks_list = None ):
    try:
        soundcloud_client = soundcloud.Client(
                client_id = api_creds.api_key,
                client_secret = api_creds.api_secret,
                username = api_creds.username,
                password = api_creds.password,
            )
        playlist_details = {
            'title': title, 
            'sharing': sharing,
        }
        if playlist_id:
            playlist_details['id'] = playlist_id
        if image_location:
            playlist_details[ 'artwork_data' ] = open(image_location)
        if tracks_list:
            tracks = map(lambda id: dict(id=id), [ track.id for track in tracks_list ])
            playlist_details[ 'tracks' ] = tracks
        return soundcloud_client.post( '/playlists', playlist = playlist_details )        
    except Exception, e:
        logger.exception('exception while updating playlist on soundcloud: %s' % ( str(e) ))
    return None

def get_tracks_for_playlist( api_creds, playlist_id ):
    try:
        soundcloud_client = soundcloud.Client(
                client_id = api_creds.api_key,
                client_secret = api_creds.api_secret,
                username = api_creds.username,
                password = api_creds.password,
            )
        playlist = soundcloud_client.get( '/playlists/%s' %(playlist_id) )
        return playlist.tracks

    except Exception, e:
        logger.exception('exception while getting tracks for playlist on soundcloud: %s' % ( str(e) ))
    return None