from django.contrib.auth.models import User
from vapp.social.models import SocialMedia, Sm_publish_request_log

def get_sm_sharing_stats(user_ids, start_date, end_date):
    sm_list = {}
    data = []
    stats = {}

    for id, name in SocialMedia.objects.filter(active=True).order_by('sm_name').values_list('id', 'sm_name'):
        sm_list[id] = name
    header = ['User ID', 'User'] + [sm.capitalize() for sm in sm_list.values()] + ['Total']

    for user_id in user_ids:
        stats[user_id] = {}
        username = User.objects.get(id=user_id).username
        for sm in sm_list:
            stats[user_id]['User'] = username
            stats[user_id][sm] = 0
            stats[user_id]['Total'] = 0
    social_shares = Sm_publish_request_log.objects.filter(request_time__range=(start_date, end_date),
                                                          user_id__in=user_ids,
                                                          result_code='PB')
    for share in social_shares:
        stats[share.user_id][share.sm_id_id] += 1
        stats[share.user_id]['Total'] += 1
    for user_id, stat in stats.iteritems():
        record = [user_id, stat['User']] + [ stat.get(sm_id, 0) for sm_id in sm_list.keys()] + [stat['Total']]
        data.append(record)
    data.insert(0, header)
    return data
