from django.forms import ModelForm, HiddenInput, DateField, ModelChoiceField
from django.forms.extras.widgets import SelectDateWidget
from django.forms import Textarea 
from mward.models import *
from django import forms


class DisputeForm(ModelForm):
    
    entity  = ModelChoiceField(queryset=Entity.objects.all(),empty_label=None)
    
    def __init__(self, *args, **kwargs):
        
        ward_name = kwargs.pop('ward_name', None)
        city = kwargs.pop('city', None)
        entity_id = kwargs.pop('entity_id', None)
        super(DisputeForm, self).__init__(*args, **kwargs)

        if ward_name:
            self.fields['entity'].queryset = Entity.objects.filter(pk=entity_id,ward__ward_name=ward_name,ward__zone__city=city)

    
    class Meta:
             
        model = Report
        fields = ('entity','comments')
        
        
class FileUploadForm(forms.Form):
    
    file = forms.FileField()
    
class ZoneEntryForm(ModelForm):
    
    class Meta:
        model = Zone
        fields = ('zone_name','city')   
     

class WardEntryForm(ModelForm):
    
    class Meta:
        model = Ward
        fields = ('zone','ward_name','ward_number','counsellor_name')
    
    
class DhalaoEntryForm(ModelForm):
    gov_id = forms.CharField(label='Dhalao Number')
    
    class Meta:
        model = Entity
        fields = ('ward','gov_id','location','city')
    
    