from mward.models import *
import os,urllib
from log import get_request_logger
logger = get_request_logger()

base = '/usr/local/voicesite/reports/'

def create_dir_on_change(sender, **kwargs):
    
    logger.info('create_dir_on_change called')
    
    global base
    obj = kwargs.get('instance')
    
    zones = Zone.objects.all()
    cities = []
    
    for zone in zones:
        if not cities.__contains__(zone.city.lower()):
            cities.append(zone.city.lower())
    
    etypes = EntityType.objects.all()
    
    for city in cities:
        wards = Ward.objects.filter(zone__city=city)

        for ward in wards:
            
          for etype in etypes:
            
            path = base + urllib.quote_plus(city) + '/' + urllib.quote_plus(ward.ward_name) + '/' + urllib.quote_plus(etype.type)
              
            if not os.path.exists(path):                
                logger.info('creating a new directory')                
                os.makedirs(path)
                
            else:
                logger.info('directory already exists')
    
