from django.db import models
from django.contrib.auth.models import User
from app_manager.models import App_instance, CalleridField
import urllib
from datetime import *
from sms.models import SMS_contact

# Create your models here.
VILLAGE_NAME = 50
COUNTRY_NAME = 50
STATE_NAME = 50
DISTRICT_NAME = 50
BLOCK_NAME = 50

class Organization(models.Model):
    
    ai = models.ForeignKey(App_instance)
    org_code = models.CharField(max_length=10)
    org_name = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.org_name
    
class Zone(models.Model):
    
    ai = models.ForeignKey(App_instance)
    zone_name = models.CharField(max_length=50,null=False,blank=False)
    city = models.CharField(max_length=50,null=False,blank=False)
    
    def __unicode__(self):
        return self.zone_name
    
    def wards(self):
        return Ward.objects.filter(zone=self)
    
    
class Ward(models.Model):
    
    ai = models.ForeignKey(App_instance)
    zone = models.ForeignKey(Zone)
    ward_name = models.CharField(max_length=50,null=False,blank=False)
    ward_number = models.IntegerField()
    counsellor_name = models.CharField(max_length=50)    
    
    def __unicode__(self):
        return self.ward_name + ',' + self.zone.city

    def get_city(self):
        return urllib.quote_plus(self.zone.city)
    
    def get_ward_name(self):
        return urllib.quote_plus(self.ward_name)
    
class User_Profile(models.Model):
    
    user = models.ForeignKey(User, unique = True, related_name='mwuser')
    org = models.ForeignKey(Organization, related_name='org',null=True)
    location = models.CharField(max_length=100,null=True)
    mobile_num = CalleridField(max_length=13, null = True)
    
    
class Org_Structure(models.Model):
    
    ai = models.ForeignKey(App_instance)
    officer = models.ForeignKey(User, unique = True, related_name='officer')
    supervisor = models.ForeignKey(User, null = True,related_name='supervisor')
    subordinate = models.ForeignKey(User, null = True, related_name='subordinate')
       
    
class EntityType(models.Model):
    
    ai = models.ForeignKey(App_instance)
    type_code = models.CharField(max_length=10)
    type = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.type
    
    def all_types(self):
        return self.objects.all()

class Color(models.Model):    
    
    ai = models.ForeignKey(App_instance)
    color_code = models.CharField(max_length=20,unique=True)
    meaning = models.CharField(max_length=50)
    hex_code = models.CharField(max_length=7)
    
    def __unicode__(self):        
        return self.color_code
    
class Medium(models.Model):
    
    ai = models.ForeignKey(App_instance)
    medium_code = models.CharField(max_length=20,unique=True)
    
    def __unicode__(self):
        return self.medium_code

class Reason(models.Model):
    
    ai = models.ForeignKey(App_instance)
    entity_type = models.ForeignKey(EntityType,related_name='type_entity')
    reason = models.CharField(max_length=100)
    related_org = models.ForeignKey(Organization, related_name='related_org')
    
    def __unicode__(self):        
        return self.reason

def get_default_reason():
    Reason.objects.get(reason='Other')
    
    
class Entity(models.Model):
    
    ai = models.ForeignKey(App_instance)
    entity_type = models.ForeignKey(EntityType)
    current_color = models.ForeignKey(Color)
    gov_id      = models.CharField(max_length=20)
    location    = models.CharField(max_length=100,null = True)
    city        = models.CharField(max_length=50)
    gov_officer = models.ForeignKey(User)
    contractor = models.ForeignKey(User, related_name='contractor')
    is_disputed    = models.BooleanField()
    mward_reason = models.ForeignKey(Reason, null=True,related_name='mward_reason')
    comments    = models.CharField(max_length=200, null=True)
    ward        = models.ForeignKey(Ward)
    
    def __unicode__(self):        
        return self.entity_type.__unicode__() + '-' + self.gov_id
    
    def open_issues(self):
        return Issue.objects.filter(entity=self)
    
    def last_report_time(self):
        
        reports = Report.objects.filter(entity=self).order_by('-report_date')
        if len(reports) > 0:
            return reports[0].report_date
        else:
            'No Reports'
    
    def cleaning_score(self):
        
        score = 0.0
        score_map = {'GREEN' : 1,'YELLOW' : 0.5, 'RED' : 0, 'BLACK' : 0}
        
        now = datetime.now()
        start = datetime(year=now.year,month=now.month,day=1)
        
        reports = Report.objects.filter(entity=self,report_date__gte=start,report_date__lte=now)
         
        for report in reports:
            score = score + score_map[report.color_reported.color_code]
        
        count = reports.count()
        
        if count > 0 :
            score = ((score/count) * 100.0)
            return int(score)
        else:
            return 100

class Monthly_Report(models.Model):
    
    ai = models.ForeignKey(App_instance)
    ward = models.ForeignKey(Ward)
    entity_type = models.ForeignKey(EntityType)
    report_date = models.DateTimeField(default=datetime.now())
    
    def named_month(self):
        
        month_map = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May', 6 : 'June', 7 : 'July', \
                     8 : 'August', 9 : 'September', 10 : 'October', 11 : 'November', 12 : 'December'}
        
        return month_map[self.report_date.month]
   
    
class Issue(models.Model):
    
    ai = models.ForeignKey(App_instance)
    entity = models.ForeignKey(Entity) 
    color_reported = models.ForeignKey(Color,related_name='issue_color',null=False)
    not_green_reason = models.ForeignKey(Reason,default=get_default_reason)   
    reported_by = models.ForeignKey(User,related_name='reporter')
    report_time = models.DateTimeField(auto_now_add = True)
    reported_via = models.ForeignKey(Medium)
    assigned_to = models.ForeignKey(User,related_name='actor')
    action_taken = models.CharField(max_length=100, null=True)
    issue_resolved = models.BooleanField(default=False)
    resolved_date = models.DateTimeField(null=True)
    reason_delay  = models.CharField(max_length=100, null=True)
    comments = models.CharField(max_length=200,null=True)

    def __unicode__(self):
        return u'Issue dated: ' + str(self.report_time)

    
class Report(models.Model):
    
    ai = models.ForeignKey(App_instance,related_name='app_instance')
    entity = models.ForeignKey(Entity)
    color_reported = models.ForeignKey(Color,related_name='reported_color')
    reported_by = models.ForeignKey(User)
    report_date = models.DateTimeField()
    reason_not_green = models.ForeignKey(Reason, null = True,related_name='reason_if_not_green')
    help_needed = models.BooleanField(default=False)
    comments = models.CharField(max_length=200,null=True)    
    issue       = models.ForeignKey(Issue, null=True)
    
    def __unicode__(self):
        return u'Report dated: ' + str(self.report_date)

        
class Notification_log(models.Model):
    
    ai = models.ForeignKey(App_instance)
    message = models.CharField(max_length=150)
    from_user = models.ForeignKey(User,related_name='note_from')
    to_user   = models.ForeignKey(User, related_name='note_to')
    send_time = models.DateTimeField()
    sent_via  = models.ForeignKey(Medium)
    
class Escalation_log(models.Model):
    
    ai = models.ForeignKey(App_instance)
    issue = models.ForeignKey(Issue)
    from_officer = models.ForeignKey(User, related_name='esc_from')
    to_officer = models.ForeignKey(User, related_name='esc_to')
    escalation_reason = models.CharField(max_length=100,null = True)
    escalation_date = models.DateTimeField()
    
class Entity_Description(models.Model):
    
    ai = models.ForeignKey(App_instance)
    entity_type = models.ForeignKey(EntityType)
    description = models.TextField()
   

class KeyPress(models.Model):
    
    key = models.CharField(max_length=2)
    entity = models.ForeignKey(EntityType)
    
    
    
#Test code for dhanbad 

class Country(models.Model):

    name = models.CharField(max_length = COUNTRY_NAME)
    states = models.ManyToManyField('State', null = True, blank = True, related_name = 'states')
    
    def __unicode__(self):        
        return self.name
    
class State(models.Model):
    
    name = models.CharField(max_length = STATE_NAME)
    country = models.ForeignKey(Country, null = True, blank = True)
    districts = models.ManyToManyField('District', null = True, blank = True, related_name = 'districts')

    def __unicode__(self):        
        return self.name + ' , ' + self.country.__unicode__() 
    
    
class District(models.Model):
    
    name = models.CharField(max_length = DISTRICT_NAME)
    state = models.ForeignKey(State, null = True, blank = True)
    blocks = models.ManyToManyField('Block', null = True, blank = True, related_name = 'blocks')
    
    def __unicode__(self):        
        return self.name  + ' , ' + self.state.__unicode__()
    
    
class Block(models.Model):
    
    name = models.CharField(max_length = BLOCK_NAME)
    district = models.ForeignKey(District, null = True, blank = True)
    
    def __unicode__(self):        
        return self.name  + ' , ' + self.district.__unicode__()

class Location(models.Model):
    
    ai = models.ForeignKey(App_instance)
    school = models.CharField(max_length = VILLAGE_NAME)
    country = models.ForeignKey(Country, related_name = 'country')
    state = models.ForeignKey(State, null = True, blank = True, related_name = 'state')
    district = models.ForeignKey(District, null = True, blank = True, related_name = 'district')
    block = models.ForeignKey(Block, null = True, blank = True, related_name = 'block')
    
    def __unicode__(self):        
        return self.school + ',' + self.block.name +  ',' + self.district.name
    
    @classmethod
    def blocks(cls,ai):        
        blist = Location.objects.filter(ai=ai).values_list('block', flat = True).distinct()
        return Block.objects.filter(id__in=blist)
    @classmethod
    def districts(cls,ai):
        return Location.objects.filter(ai=ai).values_list('district', flat = True).distinct()
    
class Number_school_map(models.Model):
    
    ai = models.ForeignKey(App_instance)
    school = models.ForeignKey(Location, related_name="school_location")
    number = models.ForeignKey(SMS_contact, related_name = 'school_number',null = True)
    is_active = models.BooleanField(default = True)
    
    def __unicode__(self):
        return str(self.ai) + ',' + str(self.school) + ',' + str(self.number)
        
class MDM_log(models.Model):
    
    ai = models.ForeignKey(App_instance)
    location = models.ForeignKey(Location)
    time = models.DateTimeField(auto_now_add = True)
    total_students = models.PositiveIntegerField()
    students_fed = models.PositiveIntegerField()
    help_needed = models.BooleanField()
    comments = models.TextField(null = True) 
    reported_by = models.ForeignKey(SMS_contact, related_name = 'report_number', null = True)
    
    def __unicode__(self):
        return str(self.location) + ' at ' + str(self.time.date())
    
    def contact(self):
        if self.reported_by:
            return self.reported_by
        try:
            return Number_school_map.objects.get(ai = self.ai, school = self.location).number
        except Exception as e:
            return None
        
    
    @classmethod
    def today_block_reports(cls,ai,block):
        today_max = datetime.combine(date.today(), time.max)
        today_min = datetime.combine(date.today(), time.min)
        locations = Location.objects.filter(ai = ai, block = block)        
        reported = MDM_log.objects.filter(location__in=locations, time__range=(today_min, today_max))
        return (len(locations), list(reported))
    
    @classmethod
    def today_student_report(cls,ai, block):
        
        total = Location.objects.filter(block = block)
        reported = MDM_log.objects.filter(time__day__gte=datetime.now().day)
        total = 0
        fed = 0
        for report in reported:
            total += report.total_students
            fed += students_fed
        
        return {'total' : total, 'fed' : fed}
    
    @classmethod
    def yesterday_student_report(cls,ai, block):
        total = Location.objects.filter(block = block)
        reported = MDM_log.objects.filter(time__day__gte=datetime.now().day)
        total = 0
        fed = 0
        for report in reported:
            total += report.total_students
            fed += students_fed
        
        return {'total' : total, 'fed' : fed}
        
    
# Ratings of both the entities and citizens to be added