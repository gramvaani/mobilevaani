from models import *
import re
from sms.utils import *
from log import get_request_logger
logger = get_request_logger()

SUCCESS_MESSAGE = 'Your entry has been successfully registered'
FAILURE_MESSAGE = 'Your message has not been saved successfully. Please check if you are a registered number or you have entered in standard format'

def create_mdm_log(ai, school, message_code, contact = None):
    mdm_log = None
    if len(message_code) > 0:
        mdm_log = MDM_log(ai = ai, location = school, total_students = int(message_code[0].strip()), students_fed = int(message_code[1].strip()), reported_by = contact)
        if len(message_code) > 2:
            help_needed = message_code[2].strip().lower()
            if help_needed == 'y' or help_needed.startswith('y'):
                mdm_log.help_needed = True
            else:
                mdm_log.help_needed = False

            if len(message_code) > 3:
                i = 3
                mdm_log.comments = ''
                while i < len(message_code):
                    mdm_log.comments += message_code[i] + ' '
                    i += 1
            mdm_log.save()
        return mdm_log

def in_handler(ai, app_name, number, message):

    error = True

    try:
        school_map = Number_school_map.objects.get(number__number = number, is_active = True)	
        cleaned_mesage = remove_app_name(message, app_name)	
        result = split_message(cleaned_mesage)		
        mdm_log = create_mdm_log(ai, school_map.school, result, contact=school_map.number)		
        if mdm_log:
            error = False
    except Exception as e:
        logger.info('exception in sms_handler:' + str(e))

    if error:
        return FAILURE_MESSAGE
    else:
        return SUCCESS_MESSAGE 

