from django.contrib import admin
from mward.models import *
from sms.models import *

admin.site.register(Entity)
admin.site.register(Issue)
admin.site.register(Report)
admin.site.register(EntityType)
admin.site.register(Reason) 
admin.site.register(Entity_Description)
admin.site.register(Zone)
admin.site.register(Ward)
admin.site.register(Monthly_Report)
admin.site.register(Country)
admin.site.register(State)
admin.site.register(District)
admin.site.register(Block)
admin.site.register(Location)
admin.site.register(SMS_ai_app)
admin.site.register(SMS_contact)
admin.site.register(MDM_log)
admin.site.register(Number_school_map)
 