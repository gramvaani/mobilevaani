from freeswitch import Session #@UnresolvedImport
from vapp.telephony.utils import *
from vapp.helpline.models import *
from vapp.log import get_logger
from sms.tasks import SMSTask
import freeswitch

logger = get_logger()

MESSAGE = 'Thanks for contributing in the Model Ward Project.' 
 
def in_handler(session, args):
    
    logger.info("Running Model Ward Helpline")
    ai_id = session.ai.id
    callerid = session.getVariable("caller_id_number")
    
    session.answer()
    select_choices = '1234'
    
    selected_choice = play_and_get(session, 'welcome', select_choices)
    logger.info('lang_choice: ' + str(selected_choice))
        #select
    if not selected_choice:
          end_session(session)
            
       # entity_name = {'1' : 'dhalao','2' : 'malba','3' : 'street sweeping', '4' : 'community toilets'}[selected_choice]
        
    play_prompt(session,'record_prompt')
    call = get_embedded_recording(session, 120, Call(ai_id = session.ai.id), Call.media)
    call.callerid = callerid
    call.ai = session.ai
    call.handler = Handler.RECORDER
    call.save()
    play_prompt(session, 'thankyou')
    SMSTask.create_send_msg(ai_id, MESSAGE, callerid)

def end_session(session):
    play_prompt(session, 'thankyou')
    session.hangup()
