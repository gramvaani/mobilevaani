from celery.task import Task
from datetime import datetime
from mward.models import *
import xlrd
from xlsxwriter import Workbook
from xlutils.copy import copy
import urllib,os

from vapp.log import get_logger
logger = get_logger()

DAILY_REPORTS_DIR       = '/usr/local/voicesite/reports/'


class SetEntityBlackTask(Task):
    
    def run(self):
        logger.debug('running set entity black')
        try:
            time_now = datetime.now()
            entities = Entity.objects.all()
            black = Color.objects.get(color_code='Black')
            
            for entity in entities:
                reports = Report.objects.filter(entity=entity).order_by("-report_date")
                if reports.count() > 0:
                    logger.debug('entity name:' + str(entity) + ' reports > 0')
                    last_report_time = reports[0].report_date
                    days_diff = time_now - last_report_time
                    logger.debug('days diff:' + str(days_diff))
                    if days_diff.days >= 2:
                        new_entity = Entity.objects.get(pk=entity.id)
                        new_entity.comments = 'No report for today'
                        new_entity.current_color = black
                        new_entity.save()
                        logger.debug('entity being blacked:' + str(entity))
        except Exception, e:
            logger.debug('exception in SetEntityBlackTask:' + str(e))
            
            
class BuildMonthlyReportTask(Task):
    
    def run(self):
        logger.debug('started buildMonthlyReportTask.')
        wards = Ward.objects.all()
        time_now = datetime.now()
        c_month = time_now.month
        c_year  = time_now.year
        
        dt = datetime(day=1,month=c_month,year=c_year)
        
        e_types = EntityType.objects.all()
                
        for ward in wards:
            logger.debug('looping in ward')
            i = 0
            for e_type in e_types:  
                logger.debug('e_type')              
                reports = Report.objects.filter(entity__entity_type=e_type,entity__ward=ward,report_date__gte=dt).order_by("report_date")
                entities = Entity.objects.filter(ward=ward)
                logger.debug('got entities')
                if len(reports) > 0:
                    logger.debug(' > 0')
                    file_name = urllib.quote_plus(ward.zone.city) +'_' + urllib.quote_plus(ward.ward_name) + '_' + urllib.quote_plus(e_type.type) + '_' + str(dt.month) + '_' + str(dt.year) + '.xlsx'
                    wb = None                    
                    sheet = None
                    
                    if os.path.exists(DAILY_REPORTS_DIR + file_name):
                        logger.debug('exists:' + DAILY_REPORTS_DIR + file_name)
                        wb = copy(xlrd.open_workbook(DAILY_REPORTS_DIR + file_name))
                        sheet = wb.get_sheet(i)
                    else:
                        logger.debug('doesnt')        
                        wb = Workbook(DAILY_REPORTS_DIR + file_name)
                        sheet = wb.add_worksheet()
                        sheet.title = 'ward ' + str(ward.id)
                    
                    update_excel_sheet(wb,sheet,reports, entities,dt,DAILY_REPORTS_DIR + file_name)
            #self.wb.save(DAILY_REPORTS_DIR + self.file_name)
                        
                    
        
def update_excel_sheet(wb, sheet, reports, entities, dt, path):       
    
    logger.debug('inside update_excel_sheet')
    sheet.write(0,0, 'Dhalao No.')
    now = datetime.now()
    
    days = (now - dt).days
    
    for i in range(1,days):
        sheet.write(0,i,str(i))
    
    for j in range(1, len(entities)):
        sheet.write(j,0,entities[j-1].gov_id)
    logger.debug('in end')   
    wb.close()  