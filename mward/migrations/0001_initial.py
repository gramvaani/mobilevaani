# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Organization'
        db.create_table('mward_organization', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('org_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('org_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('mward', ['Organization'])

        # Adding model 'Zone'
        db.create_table('mward_zone', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('zone_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('mward', ['Zone'])

        # Adding model 'Ward'
        db.create_table('mward_ward', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Zone'])),
            ('ward_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ward_number', self.gf('django.db.models.fields.IntegerField')()),
            ('counsellor_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('mward', ['Ward'])

        # Adding model 'User_Profile'
        db.create_table('mward_user_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='mwuser', unique=True, to=orm['auth.User'])),
            ('org', self.gf('django.db.models.fields.related.ForeignKey')(related_name='org', null=True, to=orm['mward.Organization'])),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('mobile_num', self.gf('django.db.models.fields.CharField')(max_length=13, null=True)),
        ))
        db.send_create_signal('mward', ['User_Profile'])

        # Adding model 'Org_Structure'
        db.create_table('mward_org_structure', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('officer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='officer', unique=True, to=orm['auth.User'])),
            ('supervisor', self.gf('django.db.models.fields.related.ForeignKey')(related_name='supervisor', null=True, to=orm['auth.User'])),
            ('subordinate', self.gf('django.db.models.fields.related.ForeignKey')(related_name='subordinate', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal('mward', ['Org_Structure'])

        # Adding model 'EntityType'
        db.create_table('mward_entitytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('type_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('mward', ['EntityType'])

        # Adding model 'Color'
        db.create_table('mward_color', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('color_code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('meaning', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('hex_code', self.gf('django.db.models.fields.CharField')(max_length=7)),
        ))
        db.send_create_signal('mward', ['Color'])

        # Adding model 'Medium'
        db.create_table('mward_medium', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('medium_code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
        ))
        db.send_create_signal('mward', ['Medium'])

        # Adding model 'Reason'
        db.create_table('mward_reason', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='type_entity', to=orm['mward.EntityType'])),
            ('reason', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('related_org', self.gf('django.db.models.fields.related.ForeignKey')(related_name='related_org', to=orm['mward.Organization'])),
        ))
        db.send_create_signal('mward', ['Reason'])

        # Adding model 'Entity'
        db.create_table('mward_entity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
            ('current_color', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Color'])),
            ('gov_id', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('gov_officer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('contractor', self.gf('django.db.models.fields.related.ForeignKey')(related_name='contractor', to=orm['auth.User'])),
            ('is_disputed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mward_reason', self.gf('django.db.models.fields.related.ForeignKey')(related_name='mward_reason', null=True, to=orm['mward.Reason'])),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Ward'])),
        ))
        db.send_create_signal('mward', ['Entity'])

        # Adding model 'Monthly_Report'
        db.create_table('mward_monthly_report', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Ward'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
            ('report_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 2, 20, 17, 12, 53, 249373))),
        ))
        db.send_create_signal('mward', ['Monthly_Report'])

        # Adding model 'Issue'
        db.create_table('mward_issue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Entity'])),
            ('color_reported', self.gf('django.db.models.fields.related.ForeignKey')(related_name='issue_color', to=orm['mward.Color'])),
            ('not_green_reason', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['mward.Reason'])),
            ('reported_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reporter', to=orm['auth.User'])),
            ('report_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('reported_via', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Medium'])),
            ('assigned_to', self.gf('django.db.models.fields.related.ForeignKey')(related_name='actor', to=orm['auth.User'])),
            ('action_taken', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('issue_resolved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('resolved_date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('reason_delay', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
        ))
        db.send_create_signal('mward', ['Issue'])

        # Adding model 'Report'
        db.create_table('mward_report', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='app_instance', to=orm['app_manager.App_instance'])),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Entity'])),
            ('color_reported', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reported_color', to=orm['mward.Color'])),
            ('reported_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('report_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('reason_not_green', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reason_if_not_green', null=True, to=orm['mward.Reason'])),
            ('help_needed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('issue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Issue'], null=True)),
        ))
        db.send_create_signal('mward', ['Report'])

        # Adding model 'Notification_log'
        db.create_table('mward_notification_log', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('from_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='note_from', to=orm['auth.User'])),
            ('to_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='note_to', to=orm['auth.User'])),
            ('send_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('sent_via', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Medium'])),
        ))
        db.send_create_signal('mward', ['Notification_log'])

        # Adding model 'Escalation_log'
        db.create_table('mward_escalation_log', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('issue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Issue'])),
            ('from_officer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='esc_from', to=orm['auth.User'])),
            ('to_officer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='esc_to', to=orm['auth.User'])),
            ('escalation_reason', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('escalation_date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('mward', ['Escalation_log'])

        # Adding model 'Entity_Description'
        db.create_table('mward_entity_description', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('mward', ['Entity_Description'])

        # Adding model 'KeyPress'
        db.create_table('mward_keypress', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
        ))
        db.send_create_signal('mward', ['KeyPress'])


    def backwards(self, orm):
        
        # Deleting model 'Organization'
        db.delete_table('mward_organization')

        # Deleting model 'Zone'
        db.delete_table('mward_zone')

        # Deleting model 'Ward'
        db.delete_table('mward_ward')

        # Deleting model 'User_Profile'
        db.delete_table('mward_user_profile')

        # Deleting model 'Org_Structure'
        db.delete_table('mward_org_structure')

        # Deleting model 'EntityType'
        db.delete_table('mward_entitytype')

        # Deleting model 'Color'
        db.delete_table('mward_color')

        # Deleting model 'Medium'
        db.delete_table('mward_medium')

        # Deleting model 'Reason'
        db.delete_table('mward_reason')

        # Deleting model 'Entity'
        db.delete_table('mward_entity')

        # Deleting model 'Monthly_Report'
        db.delete_table('mward_monthly_report')

        # Deleting model 'Issue'
        db.delete_table('mward_issue')

        # Deleting model 'Report'
        db.delete_table('mward_report')

        # Deleting model 'Notification_log'
        db.delete_table('mward_notification_log')

        # Deleting model 'Escalation_log'
        db.delete_table('mward_escalation_log')

        # Deleting model 'Entity_Description'
        db.delete_table('mward_entity_description')

        # Deleting model 'KeyPress'
        db.delete_table('mward_keypress')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'mward.color': {
            'Meta': {'object_name': 'Color'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'color_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'hex_code': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meaning': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'mward.entity': {
            'Meta': {'object_name': 'Entity'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'contractor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contractor'", 'to': "orm['auth.User']"}),
            'current_color': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Color']"}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.EntityType']"}),
            'gov_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'gov_officer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_disputed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'mward_reason': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mward_reason'", 'null': 'True', 'to': "orm['mward.Reason']"}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Ward']"})
        },
        'mward.entity_description': {
            'Meta': {'object_name': 'Entity_Description'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.EntityType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'mward.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'mward.escalation_log': {
            'Meta': {'object_name': 'Escalation_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'escalation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'escalation_reason': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'from_officer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'esc_from'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issue': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Issue']"}),
            'to_officer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'esc_to'", 'to': "orm['auth.User']"})
        },
        'mward.issue': {
            'Meta': {'object_name': 'Issue'},
            'action_taken': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'assigned_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'actor'", 'to': "orm['auth.User']"}),
            'color_reported': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'issue_color'", 'to': "orm['mward.Color']"}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Entity']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issue_resolved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'not_green_reason': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['mward.Reason']"}),
            'reason_delay': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'report_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'reported_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reporter'", 'to': "orm['auth.User']"}),
            'reported_via': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Medium']"}),
            'resolved_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        },
        'mward.keypress': {
            'Meta': {'object_name': 'KeyPress'},
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.EntityType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        'mward.medium': {
            'Meta': {'object_name': 'Medium'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medium_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        },
        'mward.monthly_report': {
            'Meta': {'object_name': 'Monthly_Report'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.EntityType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 2, 20, 17, 12, 53, 249373)'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Ward']"})
        },
        'mward.notification_log': {
            'Meta': {'object_name': 'Notification_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'note_from'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'send_time': ('django.db.models.fields.DateTimeField', [], {}),
            'sent_via': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Medium']"}),
            'to_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'note_to'", 'to': "orm['auth.User']"})
        },
        'mward.org_structure': {
            'Meta': {'object_name': 'Org_Structure'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'officer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'officer'", 'unique': 'True', 'to': "orm['auth.User']"}),
            'subordinate': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subordinate'", 'null': 'True', 'to': "orm['auth.User']"}),
            'supervisor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'supervisor'", 'null': 'True', 'to': "orm['auth.User']"})
        },
        'mward.organization': {
            'Meta': {'object_name': 'Organization'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'org_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'org_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'mward.reason': {
            'Meta': {'object_name': 'Reason'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'type_entity'", 'to': "orm['mward.EntityType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'related_org': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'related_org'", 'to': "orm['mward.Organization']"})
        },
        'mward.report': {
            'Meta': {'object_name': 'Report'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'app_instance'", 'to': "orm['app_manager.App_instance']"}),
            'color_reported': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reported_color'", 'to': "orm['mward.Color']"}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Entity']"}),
            'help_needed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issue': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Issue']", 'null': 'True'}),
            'reason_not_green': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reason_if_not_green'", 'null': 'True', 'to': "orm['mward.Reason']"}),
            'report_date': ('django.db.models.fields.DateTimeField', [], {}),
            'reported_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'mward.user_profile': {
            'Meta': {'object_name': 'User_Profile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'mobile_num': ('django.db.models.fields.CharField', [], {'max_length': '13', 'null': 'True'}),
            'org': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'org'", 'null': 'True', 'to': "orm['mward.Organization']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mwuser'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'mward.ward': {
            'Meta': {'object_name': 'Ward'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'counsellor_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ward_number': ('django.db.models.fields.IntegerField', [], {}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mward.Zone']"})
        },
        'mward.zone': {
            'Meta': {'object_name': 'Zone'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['mward']
