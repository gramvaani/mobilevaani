# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Organization'
        db.create_table(u'mward_organization', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('org_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('org_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mward', ['Organization'])

        # Adding model 'Zone'
        db.create_table(u'mward_zone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('zone_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mward', ['Zone'])

        # Adding model 'Ward'
        db.create_table(u'mward_ward', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Zone'])),
            ('ward_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ward_number', self.gf('django.db.models.fields.IntegerField')()),
            ('counsellor_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mward', ['Ward'])

        # Adding model 'User_Profile'
        db.create_table(u'mward_user_profile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='mwuser', unique=True, to=orm['auth.User'])),
            ('org', self.gf('django.db.models.fields.related.ForeignKey')(related_name='org', null=True, to=orm['mward.Organization'])),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('mobile_num', self.gf('app_manager.models.CalleridField')(max_length=13, null=True)),
        ))
        db.send_create_signal(u'mward', ['User_Profile'])

        # Adding model 'Org_Structure'
        db.create_table(u'mward_org_structure', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('officer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='officer', unique=True, to=orm['auth.User'])),
            ('supervisor', self.gf('django.db.models.fields.related.ForeignKey')(related_name='supervisor', null=True, to=orm['auth.User'])),
            ('subordinate', self.gf('django.db.models.fields.related.ForeignKey')(related_name='subordinate', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'mward', ['Org_Structure'])

        # Adding model 'EntityType'
        db.create_table(u'mward_entitytype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('type_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mward', ['EntityType'])

        # Adding model 'Color'
        db.create_table(u'mward_color', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('color_code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('meaning', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('hex_code', self.gf('django.db.models.fields.CharField')(max_length=7)),
        ))
        db.send_create_signal(u'mward', ['Color'])

        # Adding model 'Medium'
        db.create_table(u'mward_medium', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('medium_code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
        ))
        db.send_create_signal(u'mward', ['Medium'])

        # Adding model 'Reason'
        db.create_table(u'mward_reason', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='type_entity', to=orm['mward.EntityType'])),
            ('reason', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('related_org', self.gf('django.db.models.fields.related.ForeignKey')(related_name='related_org', to=orm['mward.Organization'])),
        ))
        db.send_create_signal(u'mward', ['Reason'])

        # Adding model 'Entity'
        db.create_table(u'mward_entity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
            ('current_color', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Color'])),
            ('gov_id', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('gov_officer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('contractor', self.gf('django.db.models.fields.related.ForeignKey')(related_name='contractor', to=orm['auth.User'])),
            ('is_disputed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mward_reason', self.gf('django.db.models.fields.related.ForeignKey')(related_name='mward_reason', null=True, to=orm['mward.Reason'])),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Ward'])),
        ))
        db.send_create_signal(u'mward', ['Entity'])

        # Adding model 'Monthly_Report'
        db.create_table(u'mward_monthly_report', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Ward'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
            ('report_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 12, 30, 0, 0))),
        ))
        db.send_create_signal(u'mward', ['Monthly_Report'])

        # Adding model 'Issue'
        db.create_table(u'mward_issue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Entity'])),
            ('color_reported', self.gf('django.db.models.fields.related.ForeignKey')(related_name='issue_color', to=orm['mward.Color'])),
            ('not_green_reason', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['mward.Reason'])),
            ('reported_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reporter', to=orm['auth.User'])),
            ('report_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('reported_via', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Medium'])),
            ('assigned_to', self.gf('django.db.models.fields.related.ForeignKey')(related_name='actor', to=orm['auth.User'])),
            ('action_taken', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('issue_resolved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('resolved_date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('reason_delay', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
        ))
        db.send_create_signal(u'mward', ['Issue'])

        # Adding model 'Report'
        db.create_table(u'mward_report', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='app_instance', to=orm['app_manager.App_instance'])),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Entity'])),
            ('color_reported', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reported_color', to=orm['mward.Color'])),
            ('reported_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('report_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('reason_not_green', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reason_if_not_green', null=True, to=orm['mward.Reason'])),
            ('help_needed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('issue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Issue'], null=True)),
        ))
        db.send_create_signal(u'mward', ['Report'])

        # Adding model 'Notification_log'
        db.create_table(u'mward_notification_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('from_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='note_from', to=orm['auth.User'])),
            ('to_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='note_to', to=orm['auth.User'])),
            ('send_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('sent_via', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Medium'])),
        ))
        db.send_create_signal(u'mward', ['Notification_log'])

        # Adding model 'Escalation_log'
        db.create_table(u'mward_escalation_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('issue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Issue'])),
            ('from_officer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='esc_from', to=orm['auth.User'])),
            ('to_officer', self.gf('django.db.models.fields.related.ForeignKey')(related_name='esc_to', to=orm['auth.User'])),
            ('escalation_reason', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('escalation_date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'mward', ['Escalation_log'])

        # Adding model 'Entity_Description'
        db.create_table(u'mward_entity_description', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('entity_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'mward', ['Entity_Description'])

        # Adding model 'KeyPress'
        db.create_table(u'mward_keypress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.EntityType'])),
        ))
        db.send_create_signal(u'mward', ['KeyPress'])

        # Adding model 'Country'
        db.create_table(u'mward_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mward', ['Country'])

        # Adding M2M table for field states on 'Country'
        m2m_table_name = db.shorten_name(u'mward_country_states')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('country', models.ForeignKey(orm[u'mward.country'], null=False)),
            ('state', models.ForeignKey(orm[u'mward.state'], null=False))
        ))
        db.create_unique(m2m_table_name, ['country_id', 'state_id'])

        # Adding model 'State'
        db.create_table(u'mward_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Country'], null=True, blank=True)),
        ))
        db.send_create_signal(u'mward', ['State'])

        # Adding M2M table for field districts on 'State'
        m2m_table_name = db.shorten_name(u'mward_state_districts')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('state', models.ForeignKey(orm[u'mward.state'], null=False)),
            ('district', models.ForeignKey(orm[u'mward.district'], null=False))
        ))
        db.create_unique(m2m_table_name, ['state_id', 'district_id'])

        # Adding model 'District'
        db.create_table(u'mward_district', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.State'], null=True, blank=True)),
        ))
        db.send_create_signal(u'mward', ['District'])

        # Adding M2M table for field blocks on 'District'
        m2m_table_name = db.shorten_name(u'mward_district_blocks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('district', models.ForeignKey(orm[u'mward.district'], null=False)),
            ('block', models.ForeignKey(orm[u'mward.block'], null=False))
        ))
        db.create_unique(m2m_table_name, ['district_id', 'block_id'])

        # Adding model 'Block'
        db.create_table(u'mward_block', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.District'], null=True, blank=True)),
        ))
        db.send_create_signal(u'mward', ['Block'])

        # Adding model 'Location'
        db.create_table(u'mward_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('school', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='country', to=orm['mward.Country'])),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='state', null=True, to=orm['mward.State'])),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='district', null=True, to=orm['mward.District'])),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='block', null=True, to=orm['mward.Block'])),
        ))
        db.send_create_signal(u'mward', ['Location'])

        # Adding model 'Number_school_map'
        db.create_table(u'mward_number_school_map', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('school', self.gf('django.db.models.fields.related.ForeignKey')(related_name='school_location', to=orm['mward.Location'])),
            ('number', self.gf('django.db.models.fields.related.ForeignKey')(related_name='school_number', null=True, to=orm['sms.SMS_contact'])),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'mward', ['Number_school_map'])

        # Adding model 'MDM_log'
        db.create_table(u'mward_mdm_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app_manager.App_instance'])),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mward.Location'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('total_students', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('students_fed', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('help_needed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comments', self.gf('django.db.models.fields.TextField')(null=True)),
            ('reported_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='report_number', null=True, to=orm['sms.SMS_contact'])),
        ))
        db.send_create_signal(u'mward', ['MDM_log'])


    def backwards(self, orm):
        # Deleting model 'Organization'
        db.delete_table(u'mward_organization')

        # Deleting model 'Zone'
        db.delete_table(u'mward_zone')

        # Deleting model 'Ward'
        db.delete_table(u'mward_ward')

        # Deleting model 'User_Profile'
        db.delete_table(u'mward_user_profile')

        # Deleting model 'Org_Structure'
        db.delete_table(u'mward_org_structure')

        # Deleting model 'EntityType'
        db.delete_table(u'mward_entitytype')

        # Deleting model 'Color'
        db.delete_table(u'mward_color')

        # Deleting model 'Medium'
        db.delete_table(u'mward_medium')

        # Deleting model 'Reason'
        db.delete_table(u'mward_reason')

        # Deleting model 'Entity'
        db.delete_table(u'mward_entity')

        # Deleting model 'Monthly_Report'
        db.delete_table(u'mward_monthly_report')

        # Deleting model 'Issue'
        db.delete_table(u'mward_issue')

        # Deleting model 'Report'
        db.delete_table(u'mward_report')

        # Deleting model 'Notification_log'
        db.delete_table(u'mward_notification_log')

        # Deleting model 'Escalation_log'
        db.delete_table(u'mward_escalation_log')

        # Deleting model 'Entity_Description'
        db.delete_table(u'mward_entity_description')

        # Deleting model 'KeyPress'
        db.delete_table(u'mward_keypress')

        # Deleting model 'Country'
        db.delete_table(u'mward_country')

        # Removing M2M table for field states on 'Country'
        db.delete_table(db.shorten_name(u'mward_country_states'))

        # Deleting model 'State'
        db.delete_table(u'mward_state')

        # Removing M2M table for field districts on 'State'
        db.delete_table(db.shorten_name(u'mward_state_districts'))

        # Deleting model 'District'
        db.delete_table(u'mward_district')

        # Removing M2M table for field blocks on 'District'
        db.delete_table(db.shorten_name(u'mward_district_blocks'))

        # Deleting model 'Block'
        db.delete_table(u'mward_block')

        # Deleting model 'Location'
        db.delete_table(u'mward_location')

        # Deleting model 'Number_school_map'
        db.delete_table(u'mward_number_school_map')

        # Deleting model 'MDM_log'
        db.delete_table(u'mward_mdm_log')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mward.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'mward.color': {
            'Meta': {'object_name': 'Color'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'color_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'hex_code': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meaning': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'mward.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'states': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'states'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['mward.State']"})
        },
        u'mward.district': {
            'Meta': {'object_name': 'District'},
            'blocks': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'blocks'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['mward.Block']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.State']", 'null': 'True', 'blank': 'True'})
        },
        u'mward.entity': {
            'Meta': {'object_name': 'Entity'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'contractor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contractor'", 'to': u"orm['auth.User']"}),
            'current_color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Color']"}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.EntityType']"}),
            'gov_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'gov_officer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_disputed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'mward_reason': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mward_reason'", 'null': 'True', 'to': u"orm['mward.Reason']"}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Ward']"})
        },
        u'mward.entity_description': {
            'Meta': {'object_name': 'Entity_Description'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.EntityType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mward.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'mward.escalation_log': {
            'Meta': {'object_name': 'Escalation_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'escalation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'escalation_reason': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'from_officer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'esc_from'", 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Issue']"}),
            'to_officer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'esc_to'", 'to': u"orm['auth.User']"})
        },
        u'mward.issue': {
            'Meta': {'object_name': 'Issue'},
            'action_taken': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'assigned_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'actor'", 'to': u"orm['auth.User']"}),
            'color_reported': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'issue_color'", 'to': u"orm['mward.Color']"}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Entity']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issue_resolved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'not_green_reason': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['mward.Reason']"}),
            'reason_delay': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'report_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'reported_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reporter'", 'to': u"orm['auth.User']"}),
            'reported_via': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Medium']"}),
            'resolved_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        },
        u'mward.keypress': {
            'Meta': {'object_name': 'KeyPress'},
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.EntityType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'mward.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['mward.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['mward.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['mward.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'school': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['mward.State']"})
        },
        u'mward.mdm_log': {
            'Meta': {'object_name': 'MDM_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'comments': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'help_needed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Location']"}),
            'reported_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'report_number'", 'null': 'True', 'to': u"orm['sms.SMS_contact']"}),
            'students_fed': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'total_students': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'mward.medium': {
            'Meta': {'object_name': 'Medium'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medium_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        },
        u'mward.monthly_report': {
            'Meta': {'object_name': 'Monthly_Report'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.EntityType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 30, 0, 0)'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Ward']"})
        },
        u'mward.notification_log': {
            'Meta': {'object_name': 'Notification_log'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'note_from'", 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'send_time': ('django.db.models.fields.DateTimeField', [], {}),
            'sent_via': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Medium']"}),
            'to_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'note_to'", 'to': u"orm['auth.User']"})
        },
        u'mward.number_school_map': {
            'Meta': {'object_name': 'Number_school_map'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'number': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'school_number'", 'null': 'True', 'to': u"orm['sms.SMS_contact']"}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'school_location'", 'to': u"orm['mward.Location']"})
        },
        u'mward.org_structure': {
            'Meta': {'object_name': 'Org_Structure'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'officer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'officer'", 'unique': 'True', 'to': u"orm['auth.User']"}),
            'subordinate': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subordinate'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'supervisor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'supervisor'", 'null': 'True', 'to': u"orm['auth.User']"})
        },
        u'mward.organization': {
            'Meta': {'object_name': 'Organization'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'org_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'org_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'mward.reason': {
            'Meta': {'object_name': 'Reason'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'entity_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'type_entity'", 'to': u"orm['mward.EntityType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'related_org': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'related_org'", 'to': u"orm['mward.Organization']"})
        },
        u'mward.report': {
            'Meta': {'object_name': 'Report'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'app_instance'", 'to': u"orm['app_manager.App_instance']"}),
            'color_reported': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reported_color'", 'to': u"orm['mward.Color']"}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Entity']"}),
            'help_needed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Issue']", 'null': 'True'}),
            'reason_not_green': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reason_if_not_green'", 'null': 'True', 'to': u"orm['mward.Reason']"}),
            'report_date': ('django.db.models.fields.DateTimeField', [], {}),
            'reported_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'mward.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Country']", 'null': 'True', 'blank': 'True'}),
            'districts': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'districts'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['mward.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'mward.user_profile': {
            'Meta': {'object_name': 'User_Profile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'mobile_num': ('app_manager.models.CalleridField', [], {'max_length': '13', 'null': 'True'}),
            'org': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'org'", 'null': 'True', 'to': u"orm['mward.Organization']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mwuser'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'mward.ward': {
            'Meta': {'object_name': 'Ward'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'counsellor_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ward_number': ('django.db.models.fields.IntegerField', [], {}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mward.Zone']"})
        },
        u'mward.zone': {
            'Meta': {'object_name': 'Zone'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'sms.sms_contact': {
            'Meta': {'object_name': 'SMS_contact'},
            'circle': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'number': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'})
        }
    }

    complete_apps = ['mward']