# Create your views here.
from mward.models import *
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.core import serializers
from datetime import datetime
import json
from django.http import HttpResponse, HttpResponseRedirect
from log import get_request_logger
from django.db.models.signals import post_save
from app_manager.decorators import instance_perm, instance_perm_per_obj
from django.views.decorators.csrf import csrf_exempt
from mward.mwardforms import *
import urllib
from django.db.models import Min,Max
from excel_response import ExcelResponse
from app_manager.models import User_permission
from sms.models import SMS_message_received
from django.contrib.auth.models import User
logger = get_request_logger() 
#from mward.create_dir import * 
from django.template import RequestContext
from django.shortcuts import redirect
import xlrd


from mward.tasks import *

ai=None
import uuid        
DAILY_REPORTS_DIR       = '/usr/local/voicesite/daily_reports/'

month_map = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May', 6 : 'June', 7 : 'July', \
                     8 : 'August', 9 : 'September', 10 : 'October', 11 : 'November', 12 : 'December'}
    

#this is the public view
def get_ai(app_name):
        
    global ai
    
    try:
        ai = App_instance.objects.get(name=app_name)
    except:
        logger.exception('exception getting ai')
        ai = App_instance.objects.get(name='mward')


def main_page(request,app_name,city):
    
     if request.method == "POST":
        logger.debug('We are posting in landing.html')
        logger.debug(request.POST['ward'])
        
        if request.POST['ward'].isdigit():
            
            id = int(request.POST['ward'])        
            ward = Ward.objects.get(pk=id)
            url = ('/vapp/' + urllib.quote_plus(app_name) + '/' + urllib.quote_plus(ward.zone.city) + '/' + urllib.quote_plus(ward.ward_name) + '/')
            return redirect(url)
        else:
            return HttpResponse('Please select a ward.')
     else:
        global ai
        
        if not ai:
            logger.debug('ai not there')
            get_ai(app_name)
        else:
            logger.debug('ai there')
        
        zones = Zone.objects.filter(city=city)
        wards = Ward.objects.filter(zone__in=zones)
         
        return render_to_response('mward/landing.html',{'app_name' : app_name,'city' : city, 'zones' : zones, 'wards' : wards }, context_instance=RequestContext(request))   


def public_view(request,app_name,city,ward_name,select_type=None):    
    
    logger.debug(request)
    
    city_u = urllib.unquote_plus(city)
    ward_name_u = urllib.unquote_plus(ward_name)
    
    global ai
    
    if not ai:
        get_ai(app_name)
    
    s_type = None
    
    if select_type:
        try:
           s_type = EntityType.objects.get(ai=ai,type_code=str(select_type))
        except :
            logger.exception(str(select_type) + ' not found in Entity Type')
    
    if not s_type:
        s_type = EntityType.objects.all()[0]
    
    
    ward = Ward.objects.get(ai=ai,ward_name=ward_name_u,zone__city=city_u)   
        
    entities = Entity.objects.filter(ai=ai, ward=ward, entity_type=s_type)
    
    types = EntityType.objects.filter(ai=ai)
    
    reports = Report.objects.filter(ai=ai,entity__in=entities)
    
    last_report_date = datetime.now()
    
    if reports:
        last_report_date = reports.order_by('-report_date')[0].report_date
    
    desc = None
    try:
        desc = Entity_Description.objects.get(ai=ai,entity_type=s_type)
    except:
        logger.exception('exception caught while getting description for ' + str(s_type))
        
    return render_to_response('mward/public.html', {'entities' : entities, 'selected' : s_type, \
                                                    'app_name':app_name,'types' :types, 'report_time': last_report_date,'description' : desc, \
                                                    'city' :city, 'ward_name' : ward_name,'ward' : ward })


@login_required
@csrf_exempt
def edit_view(request,app_name,ai_id,select_type):
    
    logger.debug(request)
    if not ai:
        get_ai(app_name)
    s_type = None
    
    if select_type:
        try:
           s_type = EntityType.objects.get(ai=ai_id,type_code=str(select_type))
        except:
            logger.exception(str(select_type) + ' not found in Entity Type')
            s_type=None
    
    if not s_type:
        s_type = EntityType.objects.filter(ai=ai_id)[0]
    
    entities = Entity.objects.filter(ai=ai_id,entity_type=s_type)
    
    js = serializers.get_serializer("json")()
    data = []
    
    if entities:
        data = js.serialize(entities)
    
    return HttpResponse(data)
    
@login_required
@csrf_exempt
@require_POST
def post_changes(request,app_name):
    
   
    try:
        logger.debug(request.POST['json'])
    
        for wrapper in serializers.deserialize('json', request.POST['json']):
            
            if isinstance(wrapper.object, Entity):
                
                entity=wrapper.object            
                report = Report()
                report.ai = entity.ai
                report.entity = entity
                report.color_reported = entity.current_color
                report.reported_by = request.user
                report.reason_not_green = entity.mward_reason
                report.comments = entity.comments
                report.report_date = datetime.now()
                entity.save()
                report.save() 
                
    except:
        logger.exception('exception')       
        
    return HttpResponse("ok")


@csrf_exempt
def post_issue(request,city,ward_name,app_name,entity_id):
    
    city_u = urllib.unquote_plus(city)
    ward_name_u = urllib.unquote_plus(ward_name)
    
    logger.debug(request)
    issue_id=None
    
    if request.method == 'POST':
        try:
            logger.info('post_issue start')
            form = DisputeForm(request.POST)                
            report = form.save(commit=False)
            
            if request.user.is_authenticated():
                report.reported_by = request.user
            else:
                report.reported_by = User.objects.get(username='Citizen')
            logger.info('post_issue ' + str(report.reported_by))
               
            global ai
            if not ai:
                get_ai(app_name)
            logger.info('post_issue ai' + str(ai))
            report.ai = ai
            report.color_reported = Color.objects.get(color_code='RED')
            logger.info('post_issue color' + str(report.color_reported))
            report.reason_not_green = Reason.objects.get(reason='Other')
            logger.info('post_issue ai' + str(report.reason_not_green))
            report.report_date = datetime.now()
            robj = form.save()
            report.save()            
            
            issue_id = create_issue(report,'WEB',True)
        except:
            logger.exception('got an exception')
       
        return HttpResponseRedirect('/vapp/' + app_name + '/' + city + '/' + ward_name +'/thanks/' + str(issue_id))
        
    else:
        form = DisputeForm(ward_name=ward_name_u,city=city_u,entity_id=entity_id)
        return render_to_response('mward/table.html',{'form' : form, 'app_name' : app_name, 'city' : city, 'ward_name' : ward_name,'ward_name_u' : ward_name_u})

@login_required
@csrf_exempt
@require_POST
def update_issue(request,app_name):
    
    try:
        logger.debug(request.POST['json'])
    
        for wrapper in serializers.deserialize('json', request.POST['json']):
        
            if isinstance(wrapper.object, Issue):
                issue = wrapper.object
                
                if issue.issue_resolved:
                    issue.reason_delay = datetime.now()
                issue.save()
                
    except:
        logger.exception('exception in update_issue')
    
    return HttpResponse("ok")
    

def return_thanks(request, app_name,city,ward_name,id):
    ward_name_u = urllib.unquote_plus(ward_name)
    return render_to_response('mward/thanks.html',{'app_name' : app_name, 'id' : id,'city' : city, 'ward_name' : ward_name,'ward_name_u' : ward_name_u})

def create_issue(report,medium,is_disputed):
    
    logger.info('in create_issue ' + str(report))
    entity = Entity.objects.get(pk=int(report.entity.id))
    issue = Issue()
    issue.ai = report.ai
    issue.entity= entity
    issue.color_reported = report.color_reported
    if report.reason_not_green:
        issue.not_green_reason = report.reason_not_green
    else:
        issue.not_green_reason = Reason.objects.get(reason='Other')
        
    issue.reported_via = Medium.objects.get(medium_code=medium)
    issue.assigned_to = entity.gov_officer
    
    if report.reported_by:
        issue.reported_by = report.reported_by
    else:
        issue.reported_by = User.objects.get(username='Citizen')
    
    issue.comments = report.comments
    issue.save()
    report = Report.objects.get(pk=report.id)
    report.issue = issue
    report.report_date = datetime.now()
    report.save()
    
    if not entity.is_disputed and is_disputed:        
        entity.is_disputed = is_disputed
        
    entity.save()
    
    return issue.id
  
def get_issues(request,city,ward_name,app_name,id):
    
    logger.info(request)
    
    city_u = urllib.unquote_plus(city)
    ward_name_u = urllib.unquote_plus(ward_name)
    
    issues = None
    types = EntityType.objects.all()
    
    if id:
        entity = Entity.objects.get(pk=int(id))
        issues = entity.open_issues()
        
    else:
        ward = Ward.objects.get(ward_name=ward_name_u,zone__city=city_u)
        entities = Entity.objects.filter(ward=ward)
        issues = Issue.objects.filter(entity__in=entities)
    
    return render_to_response('mward/issue.html',{'app_name': app_name,'issues' : issues,'types' : types,'city' : city,'ward_name' : ward_name,'ward_name_u' : ward_name_u})
       
@login_required
def get_reasons(request,app_name,ai_id):

    logger.info(request)
    data = None
    js = serializers.get_serializer("json")()
    
    if ai_id:
        data = Reason.objects.filter(ai=ai_id)
    return HttpResponse(js.serialize(data))
    
@login_required
def get_colors(request,app_name,ai_id):
    
    logger.info(request)
    data = None
    js = serializers.get_serializer("json")()
    
    if ai_id:
        data = Color.objects.filter(ai=ai_id)
    
    return HttpResponse(js.serialize(data))

@login_required
def get_entity_types(request,app_name,ai_id):
    
    logger.info(request)
    data = None
    js = serializers.get_serializer("json")()
    
    if ai_id:
        data = EntityType.objects.filter(ai=ai_id)
    return HttpResponse(js.serialize(data))


@login_required
def get_issues_json(request,app_name,ai_id):
    
    logger.info(request)
    data = None
    js = serializers.get_serializer("json")()
    
    if ai_id:
        data = Issue.objects.filter(ai=ai_id, issue_resolved=False ).order_by('report_time')            
        return HttpResponse(js.serialize(data, relations=('entity',)))
        
    return HttpResponse("Exception")

@login_required
def get_users(request,app_name,ai_id):
    
    logger.info(request)
    data = None
    
    js = serializers.get_serializer("json")()
    
    if ai_id:
        data = User.objects.filter(user__in=User_permission.objects.filter(ai=ai_id))
        
        
    return HttpResponse(js.serialize(data))


def show_monthly_reports(request,app_name,city,ward_name):
    
    city_u = urllib.unquote_plus(city)
    ward_name_u = urllib.unquote_plus(ward_name)
    
    types = EntityType.objects.all()
    base_date = datetime(2011,8,1)
    ward = Ward.objects.get(zone__city=city_u,ward_name=ward_name_u)
    
    global ai
    
    if not ai:
        get_ai(app_name)
    
    now = datetime.now()
        
    year_reports_map = {}
    
    reports = Report.objects.filter(ai = ai, entity__ward = ward).order_by('report_date')
    
    if len(reports) > 0:
        
        base_date = reports.aggregate(Min('report_date'))['report_date__min']
        max_date = reports.aggregate(Max('report_date'))['report_date__max']
        current = base_date
        current_yr = base_date.year
        current_months = []  
                    
        while current <= max_date:       
            
            current_months.append(current.month)
                
            if current.month == 12:         
                
                year_reports_map[current_yr] = current_months
                current = datetime(year = current_yr + 1, month = 1, day = 1)
                current_months = []
                current_yr = current_yr + 1
            else:                
                current = datetime(year = current.year, month = current.month + 1, day = 1)
            
        year_reports_map[current.year] = current_months 
        
    return render_to_response('mward/archives.html',{'selected' : 'archives','app_name': app_name,'types' : types, 'ward_name' : ward_name, 'ward' : ward, 'city' : city,\
                                                      'year_reports_map' : year_reports_map,'base_year' : base_date.year,'now' : now})
      
     

def get_monthly_report(request, app_name, ward_id, month, year):
    
    logger.debug(request)
    
    global month_map
    global ai
    
    if not ai:
        get_ai(app_name)
    
    ward = Ward.objects.get(id = ward_id)
    
    reports = Report.objects.filter(ai = ai, entity__ward = ward, report_date__month = month, report_date__year = year )
    
    data = [[str(ward.ward_number) , ward.ward_name,'', 'Report for Month of ' + str(month_map[int(month)]) + ' ' + str(year),'','','Date: ' + datetime.now().date().strftime('%d %h, %Y') ]]
    
    data.append([])
    
    headers = [' Index No. ',' Dhalao No. ', ' Colour ', ' Remarks ', ' MCD Help needed ', ' Lifting Date ', ' Lifting Time ']
    data.append(headers)
    
    
    index = 1
    for report in reports:
        row = [str(index), report.entity.gov_id, report.color_reported.color_code, report.comments, 'Yes' if report.help_needed else 'No', str(report.report_date.date().strftime('%d %h, %Y')), str(report.report_date.time())[0:5]]
        data.append(row)
        index = index + 1
    
    filename = ward.ward_name + '_' + str(month) + '_' + str(year)

    return ExcelResponse(data, filename)
    


def update_entity(message):
    
    logger.info('in update_entity')
    
    parts = message.split(' ')
    if len(parts) > 0:
        logger.info('parts > 0')
        try:
            
            entity_ward = parts[0].split('/')            
            entity = Entity.objects.get(gov_id=entity_ward[0],ward__ward_number=entity_ward[1])
            logger.info('entity found and also ward found' + str(entity))
            
            if len(parts) > 1:
                logger.info('color?')
                color = parts[1].lower()
                
                if color == 'red' or color == 'green' or color == 'yellow':
                    logger.info('color is correctly put in ' + color)
                    try:
                        color_obj = Color.objects.get(color_code=color)
                    except:
                        logger.debug('color exception')
                        
                    logger.info('1')
                    del parts[0] # remove the entity
                    del parts[0] # remove the color
                    
                    length = len(parts)
                    last = parts[length-1].lower()
                    mcd_help_needed = False
                                                
                    if last == 'y':
                        mcd_help_needed = True
                        del parts[length-1]
                            
                    elif last == 'n' :
                        del parts[length-1]
                    
                    
                    
                    if len(parts) > 0:
                        
                        reason_not_green = ' '.join(parts)
                        logger.info('reason' + reason_not_green)
                        
                    logger.info('report object to be created')
                    report = Report()
                    report.ai = ai
                    report.entity = entity
                    report.color_reported = color_obj
                    report.reported_by = entity.contractor
                    report.help_needed = mcd_help_needed
                    report.comments = reason_not_green
                    report.report_date = datetime.now()
                    report.save()
                    logger.info('entity to be changed')
                    entity.comments = reason_not_green
                    entity.current_color = color_obj
                    entity.save() 
                    
                else:
                    logger.info('color not correct: ' + color)
                    return        
            
        except Exception, err:
            logger.debug('update_entity: entity object not found:' + err.message)
            return
        



def sms_handler(sender, **kwargs):
    
    logger.debug('inside sms handler')
    
    global ai    
    if not ai:
        get_ai(None)
    
    obj = kwargs.get('instance')
    logger.debug('object received')
        
    if isinstance(obj,SMS_message_received):
        
        logger.debug('obj instance of SMS_message_received')
        
        if obj.ai == ai:
            logger.debug('obj ai == ai')
            
            message = obj.message
            logger.info('obj.message ==' + message)
            
            update_entity(message=message)
            
        else:
            logger.info('ai not same')
    else:
        logger.info('obj not an instance of SMS_message_received')
        
    

def admin_login(request,app_name):
    
    logger.debug(request)
    logger.debug('login page: app_name -' + str(app_name))
    
    return render_to_response( str(app_name) + '/admin_login.html',{'app_name' : app_name},context_instance=RequestContext(request))

@login_required(login_url="/vapp/mward/admin/login")
def show_upload_form(request,app_name):
    
    global ai
    if request.method == 'POST':
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            if not ai:
                get_ai(app_name)
            handle_uploaded_excel(request.FILES['file'])
            logger.info('posting success')
            return HttpResponseRedirect('/vapp/' + str(app_name) + '/admin/upload')
    else:
        form = FileUploadForm()
    return render_to_response(str(app_name) + '/upload_form.html', {'app_name' : app_name,'form': form,'selected' : 'upload'},context_instance=RequestContext(request))


def handle_uploaded_excel(file):
    
    full_name = DAILY_REPORTS_DIR  + str(uuid.uuid1()) + '.xlsx'
    logger.debug('full file path:' + full_name)
    destination = open(full_name,'wb+')
    for chunk in file.chunks():
        destination.write(chunk)
    destination.close()
    
    book = xlrd.open_workbook(full_name)
    
    for sheet in book.sheets():
        ward_number = str(int(sheet.cell_value(rowx=0,colx=0)))
        city_name  = str(sheet.cell_value(rowx=0,colx=1))
        report_date = str(sheet.cell_value(rowx=0, colx=4))

        logger.debug('ward number: ' + ward_number)
        logger.debug('city name: ' + city_name)
        logger.debug('date: ' + str(report_date))
        
        for i in range(2,sheet.nrows):
            gov_id = sheet.cell_value(rowx=i,colx=0)
            logger.debug('gov id:' + gov_id)
            color = str(sheet.cell_value(rowx=i, colx=1))
            logger.debug('color:' + color)
            remarks = str(sheet.cell_value(rowx=i, colx=2))
            help_needed = sheet.cell_value(rowx=i, colx=3)
            time = "%.2f" % sheet.cell_value(rowx=i, colx=4)
            
            try:  
                logger.debug('time:' + str(time)) 
                entity = Entity.objects.get(ai=ai, gov_id=gov_id, ward__ward_number=ward_number, city=city_name )
                current_color = Color.objects.get(ai=ai, color_code=color)
                help    = (help_needed.lower() == 'y')
                try:
                    report_datetime = datetime.strptime(report_date + " " + time, "%d.%m.%Y %H.%M")
                except Exception, e:
                    logger.debug('time not in correct format, trying another way..' + str(e))
                    report_datetime = datetime.strptime(report_date + " " + time, "%d.%m.%Y %H:%M")
                
                logger.debug('reporting time: ' + str(report_datetime))
                report = Report()
                report.ai = ai
                report.entity = entity
                report.color_reported = current_color
                entity.current_color = current_color
                entity.comments = remarks
                report.report_date = report_datetime
                report.help_needed = help
                report.comments = remarks
                report.reported_by = entity.contractor
                report.save()
                entity.save()
                
            except Exception, e:
                logger.debug('exception caught in handle_uploaded_excel: ' + str(e))
        
@login_required(login_url="/vapp/mward/admin/login")
def new_zone_entry(request,app_name):
    
    global ai
    
    if not ai:
        get_ai(app_name)
        
    if request.method == "POST":
        logger.debug('new_zone_entry post')
        logger.debug(request.POST)
        
        zone_name = request.POST['zone_name']
        city      = request.POST['city']
        
        try:
            Zone.objects.get(ai=ai,zone_name=zone_name,city=city)
            return HttpResponse('This Zone already exists! Please go back and re enter')
        except:
            logger.debug('Zone doesnt exist')
        
        form = ZoneEntryForm(request.POST)
        z  = form.save(commit=False)
        z.ai = ai
        z.save()        
        return HttpResponseRedirect('/vapp/' + str(app_name) + '/admin/zone_entry')
    
    else: 
               
        form = ZoneEntryForm()        
        return render_to_response(str(app_name) + '/enter_form.html',{'app_name' : app_name, 'form' : form}, context_instance=RequestContext(request))

@login_required(login_url="/vapp/mward/admin/login")
def new_ward_entry(request,app_name):
    
    global ai
    
    if not ai:
        get_ai(app_name)
        
    if request.method == "POST":
        
        logger.debug('new_ward_entry post')
        logger.debug(request.POST)
        
        try:
            Ward.objects.get(ai=ai, ward_number=request.POST['ward_number'],ward_name=request.POST['ward_name'],zone__id=request.POST['zone'])
            return HttpResponse('The Ward already exist, please go back and check')
        except:
            logger.debug('Ward does not exist')
            
        form = WardEntryForm(request.POST)
        w = form.save(commit=False)
        w.ai = ai
        w.save()
        
        return HttpResponseRedirect('/vapp/' + str(app_name) + '/admin/ward_entry')
    
    else:
        form = WardEntryForm()
        return render_to_response(str(app_name) + '/enter_form.html',{'app_name' : app_name, 'form' : form}, context_instance=RequestContext(request))
    

@login_required(login_url="/vapp/mward/admin/login")
def new_dhalao_entry(request,app_name):
    
    global ai
    
    if not ai:
        get_ai(app_name)
        
    if request.method == "POST":
        
        logger.debug('new_dhalao_entry post')
        logger.debug(request.POST)
        dh_type = EntityType.objects.get(type_code='DHA')
        form = DhalaoEntryForm(request.POST)
        d = form.save(commit=False)
        try:
            Entity.objects.get(ai=ai,entity_type=dh_type,gov_id=d.gov_id,ward=d.ward)
            return HttpResponse('The Dhalao entry already exist, please go back and check')
        except:
            logger.debug('Dhalao entry doesnt exist')
            d.ai = ai
            d.entity_type = dh_type
            d.current_color = Color.objects.get(color_code='Black')
            d.gov_officer   = User.objects.get(username='mcd')
            d.contractor    = User.objects.get(username='mcd')
            d.comments      = 'No Reports'
            d.is_disputed   = False
            d.mward_reason  = Reason.objects.get(reason='None')
            d.save()
        
        return HttpResponseRedirect('/vapp/' + str(app_name) + '/admin/dhalao_entry')
    
    else:
        form = DhalaoEntryForm()
        return render_to_response(str(app_name) + '/enter_form.html',{'app_name' : app_name, 'form' : form}, context_instance=RequestContext(request))
    

def mdm_dashboard(request,ai):
    
    ai = int(ai)
    ai = App_instance.objects.get(id = ai)
    nmaps = Number_school_map.objects.filter(ai = ai)
    blocks = Location.blocks(ai)
    block_dict = {}
    
    for block in blocks:
        total, reports = MDM_log.today_block_reports(ai,block)
        block_dict[block] = str(len(reports)) + '/' + str(total)   
        
    return render_to_response('mward/mdm_table.html', {'ai': ai, 'block_dict' : block_dict, 'nmaps': nmaps }, context_instance = RequestContext(request))
       
            
def mdm_block_report(request, ai, block_id):
    
    ai = int(ai)
    ai = App_instance.objects.get(id = ai)
    block = Block.objects.get(id = int(block_id))
    
    total, reports = MDM_log.today_block_reports(ai,block)
    return render_to_response('mward/mdm_block.html', {'ai': ai, 'block' : block, 'total': total, 'reports': reports }, context_instance = RequestContext(request))
