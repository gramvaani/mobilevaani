from vapp.telephony.statemachine import BaseVappController
from vapp.media.models import Prompt_info, Prompt_audio
from vapp.log import get_request_logger

from btmodels import BTG_caller, BTG_level, BTG_call, BTG_response

logger = get_request_logger()

BTG_NUM_EPISODES    = 7
BTG_PROMPTS_FIXED   = [ 'btg_welcome', 'btg_input_invalid', 'btg_opinion_prompt', 'btg_opinion_instr', \
                        'btg_ty_segue', 'btg_ty_final'  ]
BTG_PROMPTS_EPISODE = [ 'btg_episode', 'btg_eps_q','btg_explanation', ]

def create_prompt_info( name ):
    count = Prompt_info.objects.filter( name = name ).count()
    if count == 0:
        pi = Prompt_info( name = name )
        pi.save()
        print "creating pinfo", name
        return pi
    else:
        print "found pinfo", name
        return Prompt_info.objects.filter( name = name )[ 0 ]

def create_prompt_audio( pi, ps_id ):
    count = Prompt_audio.objects.filter( prompt_set_id = ps_id, info = pi ).count()
    if count == 0:
        pa = Prompt_audio( prompt_set_id = ps_id, info = pi )
        pa.save()
        print "creating paudio", ps_id, pi, pa
        return pa
    else:
        print "found paudio", ps_id, pi
        return Prompt_audio.objects.filter( prompt_set_id = ps_id, info = pi )[ 0 ]

def create_prompt_records( ps_id ):
    prompt_names = []
    prompt_names.extend( BTG_PROMPTS_FIXED )
    for i in range( BTG_NUM_EPISODES ):
        for name in BTG_PROMPTS_EPISODE:
            prompt_names.append( "{0}_{1}".format( name, i ) )
    for name in prompt_names:
        pi = create_prompt_info( name )
        pa = create_prompt_audio( pi, ps_id )

def create_level_records():
    for i in range( BTG_NUM_EPISODES ):
        try:
            BTG_level.objects.get( level_id = i )
            print "btg level exists: ", i
        except:
            level = BTG_level( level_id = i, level_ord = i, name = "level_{0}".format( i ) )
            level.save()
            print "btg level created: ", level

def create_graph():
    from telephony.statemachine import graphStateMachine, StatesDescription
    sd = StatesDescription( BTGStatesDescriptionMap )
    graphStateMachine( sd, "/tmp/btgame.png" )


class BTGController( BaseVappController ):
    def __init__( self, ai, sessionData, vi_data ):
        super( BTGController, self ).__init__( ai, sessionData )
        try:
            self.btg_caller = BTG_caller.objects.get( callerid = sessionData.callerid )
        except:
            self.btg_caller = BTG_caller( callerid = sessionData.callerid )
            self.btg_caller.save()
        self.btg_call = BTG_call( cdr = sessionData.cdrs[ 0 ], caller = self.btg_caller )
        self.btg_call.save()
        print "caller", self.btg_caller, "call", self.btg_call

    def pre_outgoingstart( self ):
        return self.getStartCallParams( self.sessionData.callerid )

    def pre_incomingstart( self ):
        return self.getIncomingCallParams()

    def pre_welcome( self ):
        return self.getPromptParams( 'btg_welcome' )

    def pre_episode( self ):
        return self.getPromptParams( 'btg_episode_{0}'.format( self.btg_caller.curr_level ) )

    def pre_eps_question( self ):
        return self.getPromptPlayAndGetParams( 'btg_eps_q_{0}'.format( self.btg_caller.curr_level ) )

    def while_eps_question__sm_action_success( self, events, eventData ):
        digits = self.getPlayAndGetDigits( eventData )
        self.btg_level = BTG_level.objects.get( level_id = self.btg_caller.curr_level )
        if digits in [ 1, 2 ]:
            self.btg_response = BTG_response( call = self.btg_call, caller = self.btg_caller, level = self.btg_level, resp_key = digits )
            self.btg_response.save()
            return 'btg_input_valid'
        else:
            return 'btg_input_invalid'

    def pre_input_invalid( self ):
        return self.getPromptParams( 'btg_input_invalid' )

    def pre_opinion_prompt( self ):
        return self.getPromptPlayAndGetParams( 'btg_opinion_prompt' )

    def while_opinion_prompt__sm_action_success( self, events, eventData ):
        digit = self.getPlayAndGetDigits( eventData )
        if digit == 3:
            return 'btg_has_opinion'
        else:
            return 'btg_no_opinion'

    def pre_opinion_instr( self ):
        return self.getPromptParams( 'btg_opinion_instr' )

    def pre_opinion_rec( self ):
        return self.getEmbeddedRecordingParams( self.ai.record_duration_limit_seconds, self.btg_response, BTG_response.resp_rec )

    def pre_explanation( self ):
        return self.getPromptParams( 'btg_explanation_{0}'.format( self.btg_caller.curr_level ) )

    def while_explanation__sm_action_success( self, events, eventData ):
        next_level = ( self.btg_caller.curr_level + 1 ) % BTG_NUM_EPISODES
        self.btg_caller.curr_level = next_level
        self.btg_caller.save()
        self.is_final_eps = ( next_level == 0 )

    def pre_thank_you( self ):
        if self.is_final_eps:
            return self.getPromptParams( 'btg_ty_final' )
        else:
            return self.getPromptParams( 'btg_ty_segue' )

    def while_thank_you__sm_action_success( self, events, eventData ):
        if self.is_final_eps:
            return 'btg_ty_final'
        else:
            return 'btg_ty_segue'


BTGStatesDescriptionMap = [
{
    'name':     'outgoingstart',
    'action':   'originate',
    'transitions': {
        'stop':             ['sm_action_failure'],
        'welcome':          ['sm_action_success'],
        'outgoingstart':    ['sm_next_originate_url'],
    },
},
{
    'name':     'incomingstart',
    'action':   'answer',
    'transitions': {
        'stop':             ['sm_action_failure'],
        'welcome':          ['sm_action_success'],
    },
},
{
    'name':     'welcome',
    'action':   'playback',
    'transitions': {
        'stop':             ['sm_action_failure'],
        'episode':          ['sm_action_success'],
    },
},
{
    'name':     'episode',
    'action':   'playback',
    'transitions': {
        'stop':             ['sm_action_failure'],
        'eps_question':     ['sm_action_success'],
    },  
},
{
    'name':     'eps_question',
    'action':   'play_and_get_digits',
    'transitions': {
        'stop':             ['sm_action_failure'],
        'opinion_prompt':   ['btg_input_valid'],
        'input_invalid':    ['btg_input_invalid'],
    },  
},
{
    'name':     'input_invalid',
    'action':   'playback',
    'transitions': {
        'stop':             ['sm_action_failure'],
        'eps_question':     ['sm_action_success'],
    },
},
{
    'name':     'opinion_prompt',
    'action':   'play_and_get_digits',
    'transitions':  {
        'stop':             ['sm_action_failure'],
        'opinion_instr':    ['btg_has_opinion'],
        'explanation':      ['btg_no_opinion','sm_get_digits_no_digits'],
    },  
},
{
    'name':     'opinion_instr',
    'action':   'playback',
    'transitions':  {
        'stop':             ['sm_action_failure'],
        'opinion_rec':      ['sm_action_success'],
    }, 
},
{
    'name':     'opinion_rec',
    'action':   'record',
    'transitions':  {
        'stop':             ['sm_action_failure'],
        'explanation':      ['sm_action_success'],
    },
},
{
    'name':     'explanation',
    'action':   'playback',
    'transitions':  {
        'stop':             ['sm_action_failure'],
        'thank_you':        ['sm_action_success'],
    },  
},
{
    'name':     'thank_you',
    'action':   'playback',
    'transitions':  {
        'stop':             ['sm_action_failure', 'btg_ty_final'],
        'episode':          ['btg_ty_segue'],
    }  
},
{
    'name':     'stop',
    'action':   'none',
    'transitions':  {},
},
]
