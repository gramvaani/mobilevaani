from celery.task import Task

from app_manager.common import get_queue_for_task
from app_manager.models import Cdr, App_instance
from survey.models import Record, Question_ff, Question_mc, Survey
from mnews.models import Transient_group_schedule, Groups_call_log, Item_heard_stats, News, News_state
from callerinfo.models import Contact_list, get_contact_name, get_distinct_numbers
from customization.models import CEDPA_usrs, CEDPA_usrs_numbers, CEDPA_processed_survey
from refer.models import Referral, Refer_ai_properties, Referral_group_call_log, Referral_status

from mnews.stats import get_contrib_count_by_caller
from refer.stats import get_header_for_n_weeks, get_eff_for_referrer, get_hdr_for_grouping_trend, get_referals_last_ndays, get_ref_stats
from survey.stats import get_survey_ids, get_answer_label, get_questions, get_cdrs_from_survey_cdrs
from vapp.stats.stats import get_callscount_callerscount_avgdur

from vapp.utils import generate_attachment, get_sorted_data, send_email, is_start_of_week, get_start_of_week
from datetime import *

from django.db.models import Sum
from xlrd import *
from django.template.loader import render_to_string

from log import get_request_logger
logger = get_request_logger()


class CallAndPlayPukarSurveyTask(Task):

    def get_survey_count(self, caller, caller_id_list):
        scount = caller_id_list.count(caller)
        return scount

    def get_caller_num(self, cdr_id_list):
        callers = Cdr.objects.filter(id__in = list(cdr_id_list)).values_list('callerid', flat = True)
        callers = [str(caller) for caller in callers]
        return callers

    def run(self, main_survey_id, max_tries, schedule_id, followup_survey_id):
        tomorrow = (datetime.now().today() + timedelta(days = 1)).weekday()
        final_callers = []
        cdr_ids = Record.objects.filter(survey = main_survey_id, order = 3).exclude(mc_choice = 1).values_list('cdr', flat = True)
        followup_survey_cdrs = Record.objects.filter(survey = followup_survey_id).values_list('cdr', flat = True)
        callers = self.get_caller_num(cdr_ids)
        followup_callers = self.get_caller_num(followup_survey_cdrs)
        for i in callers:
            if self.get_survey_count(i, callers) > self.get_survey_count(i, followup_callers):
                final_callers.append(i)
        active_schedules = Transient_group_schedule.objects.filter(active = True, day_of_week__name__in = str(tomorrow))
        schedule = Transient_group_schedule.objects.get(pk = schedule_id)
        if schedule in active_schedules:
            for number in set(final_callers):
                logger.info('Entering number ' + str(number) + 'into schedule: ' + str(schedule_id))
                group_call_log = Groups_call_log(number = number, ai = schedule.ai_group.ai, group_schedule = schedule)
                group_call_log.save()


class SurveyReportAttrib:
    pass


class CreateCEDPASurveyReport(Task):

    def get_caller_src(self, cdr):
        if cdr.ai.id == 10:
            return 'JMV'
        elif cdr.ai.id == 168:
            return 'SV'
        else:
            return cdr.ai.name

    def get_survey_records(self, records, labels, ques_count):
        ans_ques_count = 0
        survey_answer_row = ['-'] * ques_count

        for record in records:
            label = get_answer_label(record, labels)

            if label != '-':
                ans_ques_count += 1

            survey_answer_row[record.question.order - 1] = label

        return survey_answer_row, ans_ques_count


    def get_count_msgs_heard(self, callerid, tag, start, end):
        return Item_heard_stats.objects.filter(ai_id = 115, cdr__callerid = callerid, item__tags__contains = tag, \
                                            cdr__start_time__range = (start, end)).count()


    def get_all_usrs_data(self):
        data = []
        header = ['UID', 'Mob no.', 'Village', 'Panchayat', 'Block', 'Name of women', 'Age', 'Education', 'Name of husband',
                'Months of Pregnancy', 'Currently Lactating/Month', 'Included in baseline', 'Attended cohort workshop']
        ROW_TEMPLATE = ['-'] * len(header)
        data.append(header)
        body = []
        users = CEDPA_usrs.objects.all()
        for user in users:
            row = list(ROW_TEMPLATE)
            user_mob_numbers = CEDPA_usrs_numbers.objects.filter(usr__id = user.id)
            mob_num_count = user_mob_numbers.count()
            if user_mob_numbers:
                row[0] = user.id
                row[1] = user_mob_numbers[0].number
                row[2] = user.village
                row[3] = user.panchayat
                row[4] = user.block
                row[5] = user.usr_name
                row[6] = user.age
                row[7] = user.education
                row[8] = user.husband_name
                row[9] = user.months_of_pregnancy
                row[10] = user.currently_lactating
                row[11] = user.included_in_baseline
                row[12] = user.attended_cw_shop
                body.append(row)
                if mob_num_count > 1:
                    for i in range(1, mob_num_count):
                        body.append([user.id, user_mob_numbers[i].number, '-','-','-','-','-','-','-','-','-','-','-'])
        data += body
        return data


    def is_caller_info_available(self, callerid):
        if CEDPA_usrs_numbers.objects.filter(number = callerid):
            return True

        return False


    def get_caller_uid(self, callerid):
        return CEDPA_usrs_numbers.objects.filter(number = callerid)[0].usr.id


    def get_user(self, uid):
        return CEDPA_usrs.objects.get(id = uid)


    def get_discrepant_data(self):
        data = []
        header = ['UID', 'Mob no.', 'Name of women']
        ROW_TEMPLATE = ['-'] * len(header)
        data.append(header)
        contacts = CEDPA_usrs_numbers.objects.exclude(number = '').values_list('number', flat = True).distinct()
        body = []
        for contact in contacts:
            discrepant_contacts = CEDPA_usrs_numbers.objects.filter(number = contact)
            if (len(discrepant_contacts) > 1) or len(contact) != 12:
                for num in discrepant_contacts:
                    row = list(ROW_TEMPLATE)
                    row[0] = num.usr.id
                    row[1] = num.number
                    row[2] = CEDPA_usrs.objects.filter(id = num.usr.id)[0].usr_name
                    body.append(row)
            else:
                continue

        data += body
        return data


    def get_header_for_daily_stats_report(self, start_date, end_date):
        data = []
        data.append( [ 'Statistics from: %s to %s' % (start_date, end_date) ] )
        data.append( [ 'Number', 'No. of time messages on QoC heard', 'QoC: Total duration',
                    'No. of time messages on Entitlements heard', 'Entitlements: Total duration',
                    'No. of time messages on Drama heard', 'Drama: Total duration' ] )
        return data


    def get_msg_count(self, ai_id, callerid, start, end, tag):
        msg_count = 0
        item_heard_stats = Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, cdr__start_time__range = (start, end))
        qoc = 'qoc'
        info = 'information'
        govt_schemes = 'government schemes'
        drama = 'drama'
        schemes = 'entitlements'

        if tag == qoc:
            msg_count = item_heard_stats.filter(item__tags__icontains = info).exclude(item__tags__icontains = govt_schemes).exclude(\
                                                item__tags__icontains = drama).count()
        elif tag == schemes:
            msg_count = item_heard_stats.filter(item__tags__icontains = govt_schemes).exclude(item__tags__icontains = info).exclude(\
                                                item__tags__icontains = drama).count()
        elif tag == drama:
            msg_count = item_heard_stats.filter(item__tags__icontains = drama).filter(item__tags__icontains = info).filter(\
                                                item__tags__icontains = govt_schemes).count()

        return msg_count


    def get_listening_stats(self