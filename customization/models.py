from django.db import models
from django.db.models.signals import post_save

from app_manager.models import App_instance, CalleridField, Cdr
from customer.models import Project
from location.models import Location
from mnews.models import ModerationEventRecorder, EVENT_LENGTH
from media.models import Recording
from app_selector.models import Choices
from registration.models import Registration

from sms.tasks import SMSTask
from sms.models import SMS_message_received, SMS_message

from btmodels import BTG_caller, BTG_level, BTG_call, BTG_response
from pacsmodels import Grievance_log, Grievance_sms_log, RSBY_Mitra_contacts
from pacsmodels import pacs_news_save, pacs_generate_location

from vapp.utils import load_vapp_module

import shutil
from datetime import datetime

from log import get_request_logger
logger = get_request_logger()


class Swasti_common_survey(models.Model):
    ai=models.ForeignKey(App_instance)
    

class subham_customers(models.Model):
    app_name = models.CharField(max_length = 32)
    number  = models.CharField(max_length = 12)
    mbl_number =  CalleridField(max_length = 12)
    location = models.TextField(max_length = 32)
    language = models.CharField(max_length = 32)
    city = models.CharField(max_length = 32)
    state = models.CharField(max_length = 32)
   
  
class indus_action_grc(models.Model):
        grc_location = models.ForeignKey(Location)
        grc_msg=models.CharField(max_length=160)
        
        
class indus_action_ai(models.Model):
        ai=models.ForeignKey(App_instance)


def get_sms_indus_action(locations):
    logger.info('inside get_sms_idus_action')
    msg =  str(locations.grc_msg) + ", " +"www.indusaction.org"
    if len(msg) > 160:
        msg=msg[0:160]
        
    return msg

def indus_action_sms(sender, **kwargs):
    event_recorder = kwargs['instance']
    indus_actionais=indus_action_ai.objects.filter().values_list('ai', flat=True)
    if event_recorder.item.ai.id not in indus_actionais:
        return
    
    logger.info('inside indus action sms handler' +str(event_recorder.item.ai.id))
    if 'location' in event_recorder.fields_changed:
        locs=indus_action_grc.objects.get(grc_location=event_recorder.item.location.id)
        if locs:
            logger.info('Location changed preparing sms for location')
            logger.info('Generating sms')
            msg_txt=get_sms_indus_action(locs)
            logger.info('sending sms to' +str(event_recorder.item.callerid))
            SMSTask.create_send_msg(event_recorder.item.ai.id, msg_txt, event_recorder.item.callerid)
        else:
            logger.info('GRC Not found for this location')

post_save.connect(indus_action_sms, sender=ModerationEventRecorder)


class CEDPA_usrs(models.Model):
    
    village = models.CharField(max_length = 64, null = True, blank = True)
    panchayat = models.CharField(max_length = 64, null = True, blank = True)
    block = models.CharField(max_length = 64, null = True, blank = True)
    usr_name = models.CharField(max_length = 150, null = True, blank = True)
    age = models.CharField(max_length = 5, null = True, blank = True)
    src = models.CharField(max_length = 15, default='init')
    education = models.CharField(max_length = 15, null = True, blank = True)
    husband_name = models.CharField(max_length = 64, null = True, blank = True)
    months_of_pregnancy = models.CharField(max_length = 15, null = True, blank = True)
    currently_lactating = models.CharField(max_length = 15, null = True, blank = True)
    included_in_baseline = models.CharField(max_length = 15, null = True, blank = True)
    attended_cw_shop = models.CharField(max_length = 15, null = True, blank = True)
    


class CEDPA_usrs_numbers(models.Model):
    
    usr = models.ForeignKey(CEDPA_usrs, null = True, blank = True)
    number = CalleridField(max_length = 20, null = True, blank = True)
    
    
class CEDPA_processed_survey(models.Model):
    
    cdr = models.ForeignKey(Cdr, null = True, blank = True)
    verification_status = models.CharField(max_length = 15, null = True, blank = True)
    error_type = models.CharField(max_length = 32, null = True, blank = True)
    audio_transcript_q1 = models.CharField(max_length = 100, null = True, blank = True)
    audio_transcript_q2 = models.CharField(max_length = 100, null = True, blank = True)
    curr_prev_exp = models.CharField(max_length = 300, null = True, blank = True)
    survey_by = models.CharField(max_length = 100, null = True, blank = True)
    comment = models.CharField(max_length = 300, null = True, blank = True)    


class CEDPA_do_kadam_husband_mis(models.Model):
    
    block = models.CharField(max_length = 32, null = True, blank = True)
    panchayat = models.CharField(max_length = 32, null = True, blank = True)
    village = models.CharField(max_length = 32, null = True, blank = True)
    shg_name = models.CharField(max_length = 32, null = True, blank = True)
    husband_name = models.CharField(max_length = 32, null = True, blank = True)
    wife_name = models.CharField(max_length = 32, null = True, blank = True)
    category = models.CharField(max_length = 32, null = True, blank = True)
    age = models.PositiveSmallIntegerField(null = True, blank = True)
    migrant = models.BooleanField(blank = True)
    husband_contact = CalleridField()


class CEDPA_schools(models.Model):
    
    district = models.CharField(max_length = 15, null = True, blank = True)
    block = models.CharField(max_length = 32, null = True, blank = True)
    school_name = models.CharField(max_length = 32, null = True, blank = True)
    school_code = models.CharField(max_length = 32, null = True, blank = True)
    principal_name = models.CharField(max_length = 100, null = True, blank = True)
    principal_contact = CalleridField()
    

    
class PFI_survey_contacts(models.Model):
    number = CalleridField()
    state = models.CharField(max_length = 32)
    district = models.CharField(max_length = 32, null = True, blank = True)
    village = models.CharField(max_length = 32, null = True, blank = True)


class cnh_leads(models.Model):
    name = models.CharField(max_length = 32)
    location = models.CharField(max_length = 32)
    message = models.CharField(max_length = 64)

class sesame_idfc_callers(models.Model):
    name = models.CharField(max_length = 32)
    contact = CalleridField()


class Custom_handler(object):
    PACS_NEWS_POST_SAVE = 'pacs_news_save'


class Custom_handler_type(object):
    NEWS_POST_SAVE = 'NEWS_POST_SAVE'


class Custom_location_generator(object):
    PACS_LOCATION_GENERATOR = 'pacs_generate_location'


class Custom_ai_settings(models.Model):

    HANDLER_CHOICES = (
        (Custom_handler.PACS_NEWS_POST_SAVE, 'pacs_news_save'),
    )

    HANDLER_TYPE_CHOICES = (
        (Custom_handler_type.NEWS_POST_SAVE, 'NEWS_POST_SAVE'),
    )

    LOCATION_GENERATOR_CHOICES = (
        (Custom_location_generator.PACS_LOCATION_GENERATOR, 'pacs_generate_location'),
    )

    ai = models.ForeignKey(App_instance)
    handler = models.CharField(max_length = 200, null = True, blank = True, choices = HANDLER_CHOICES)
    handler_type = models.CharField(max_length = 200, null = True, blank = True, choices = HANDLER_TYPE_CHOICES)
    location_generator_func = models.CharField(max_length = 200, null = True, blank = True, choices = LOCATION_GENERATOR_CHOICES)


def custom_news_save(news, handler_type, **kwargs):
    try:
        settings = Custom_ai_settings.objects.get(ai = news.ai, handler_type = handler_type)
        handler_func = getattr(load_vapp_module('customization', 'models'), settings.handler)
        custom_news = handler_func(news, **kwargs)
        return (custom_news, True) if custom_news else (None, False)
    except:
        return (news, False)


def check_and_get_custom_location(location):
    try:
        settings = Custom_ai_settings.objects.get(ai = location.ai, location_generator_func__isnull = False)
        generator_func = getattr(load_vapp_module('customization', 'models'), settings.location_generator_func)
        custom_location = generator_func(location)
        return (custom_location, True)
    except Exception, e:
        return ([], False)


class Project_custom_stats_settings(models.Model):
    project = models.OneToOneField(Project)
    stats_generator_func = models.CharField(max_length = 200, null = True, blank = True)
    template_uri = models.CharField(max_length = 200, null = True, blank = True)

    def __unicode__(self):
        if self.stats_generator_func:
            return (self.project.__unicode__() + '_' + self.stats_generator_func)
        else:
            return self.project.__unicode__()


class Custom_stats_settings(models.Model):
    ai = models.OneToOneField(App_instance)
    stats_generator_func = models.CharField(max_length = 200, null = True, blank = True)
    template_uri = models.CharField(max_length = 200, null = True, blank = True)

    def __unicode__(self):
        if self.stats_generator_func:
            return (self.ai.__unicode__() + '_' + self.stats_generator_func)
        else:
            return self.ai.__unicode__()


class CEDPA_mshakti_ewr_database(models.Model):
    
    name_of_ewr = models.CharField(max_length = 32, null = True, blank = True)
    designation = models.CharField(max_length = 32, null = True, blank = True)
    ward_no = models.PositiveSmallIntegerField(null = True, blank = True)
    village = models.CharField(max_length = 32, null = True, blank = True)
    panchayat = models.CharField(max_length = 32, null = True, blank = True)
    block = models.CharField(max_length = 32, null = True, blank = True)
    contact = CalleridField()


class ProMass_Media_database(models.Model):
    name = models.CharField(max_length = 32, null = True, blank = True)
    area_or_identity = models.CharField(max_length = 32, null = True, blank = True)
    contact = CalleridField()

def check_and_get_excel_format( project_id ):
    mshakti_cell_coding = { 
                    'red': {'bg_color': '#9C0006', 'font_color': '#FFC7CE'}, 
                    'yellow': {'bg_color': '#FFFF00', 'font_color': '#778899'},
                    'green': {'bg_color': '#006100', 'font_color': '#C6EFCE'}
                }

    mshakti_conditions = [
                    {'type': 'cell', 'criteria': 'between', 'minimum': 0, 'maximum': 49, 'format': 'red'  },
                    {'type': 'cell', 'criteria': 'between','minimum': 50, 'maximum': 74, 'format': 'yellow' },
                    {'type': 'cell', 'criteria': 'between', 'minimum': 75, 'maximum': 100, 'format': 'green' }
                ]
    
    try:
        project_formats = { 31: { 'conditions': mshakti_conditions, 'formats': mshakti_cell_coding } }
        return project_formats.get( project_id, None )
    except Exception, e:
        logger.exception('Exception in check_and_get_excel_format %s' % str(e))

def get_custom_data_fields(model_name, mapped_field):
    custom_data_fields = []
    if model_name:
        custom_data_fields = [ field for field in  model_name._meta.get_all_field_names() if not field == 'id'  and not field == mapped_field ]
    return custom_data_fields

def get_custom_data(model_name, custom_data_fields, mapped_field, mapped_field_value):
    custom_data = model_name.objects.all()
    custom_data = custom_data.filter(**{mapped_field: mapped_field_value})
    if custom_data.exists():
        return [getattr(custom_data[0], field) for field in custom_data_fields]

def is_unsubscribed(ai_id, callerid):
    for each in Choices.objects.filter(selector_ai_id = ai_id).values('ai_id'):
        if Registration.objects.get(ai_id = each['ai_id']).is_caller_registered(callerid):
            return True

    return False


class Atm_status(models.Model):
    cash_dispense = models.BooleanField(default=False)
    cash_deposit = models.BooleanField(default=False)
    queue_size = models.IntegerField(max_length=1)
    denominations = models.IntegerField(max_length=1)


class Atm(models.Model):
    code = models.IntegerField(max_length=6, unique=True)
    bank_name = models.CharField(max_length=72)
    bank_code = models.CharField(max_length=8)
    city = models.CharField(max_length=24)
    full_address = models.CharField(max_length=320, default='')
    address = models.CharField(max_length=48)
    status = models.CharField(max_length=10, default='1221')
    update_time = models.DateTimeField(auto_now=True, auto_now_add=True)
    pincode = models.IntegerField(max_length=6, null=True)
    pin1 = models.IntegerField(max_length=6, null=True)
    pin2 = models.IntegerField(max_length=6, null=True)
    pin3 = models.IntegerField(max_length=6, null=True)
    is_atm = models.BooleanField(default = True)

    def has_cash(self):
        return self.status[1] == '1'

    def available_denom(self):
        if self.status[3] == '1':
            return 100
        if self.status[3] == '2':
            return 500
        if self.status[3] == '3':
            return 2000
        if self.status[3] == '4':
            return 'multiple'

    def get_queue_size(self):
        if self.status[0] == '1':
            return 'kataar 15 se kam log'
        if self.status[0] == '2':
            return 'kataar me 15 se 40 log'
        if self.status[0] == '3':
            return 'kataar me 40 se adhik log'

    def send_update_notifs(self, ai_id, cdr_id):
        from mnews.models import Event_sms_template
        from survey.models import Cdr_event_sms_log

        params = {
            'atm_code': self.code,
            'bank_code': self.bank_code,
            'address': self.address,
            'queue_size': self.get_queue_size(),
            'cash_dispense': 'Cash available' if self.has_cash() else 'No cash'
        }
        template = Event_sms_template.objects.get(template__ai_id=ai_id,
                                                  event=CustomEvents.ATM_STATUS_UPDATED).template
        message = template.process_template(params)
        reg_callers = Atm_registered_callers.objects.filter(atm=self, is_active=True).values_list('callerid', flat=True).distinct()
        sms_ids = []
        for caller in reg_callers:
            sms = SMSTask.create_send_msg(ai_id, message, caller)
            sms_ids.append(sms.id)

        Cdr_event_sms_log.objects.bulk_create([Cdr_event_sms_log(cdr_id=cdr_id,
                                                                 sms_id=i,
                                                                 event=CustomEvents.ATM_STATUS_UPDATED) for i in sms_ids])

class Atm_status_transition_log(models.Model):
    cdr = models.ForeignKey(Cdr)
    atm = models.ForeignKey(Atm)
    new_status = models.CharField(max_length=10)
    prev_status = models.CharField(max_length=10)
    update_time = models.DateTimeField(auto_now=True, auto_now_add=True)


class Alert_policy(models.Model):
    prev_status = models.CharField(max_length=10)
    curr_status = models.CharField(max_length=10)


class Atm_for_pin(models.Model):
    pincode = models.IntegerField(max_length=6)
    atm = models.ForeignKey(Atm, to_field='code')
    rank = models.IntegerField(max_length=2)


class Pins_for_atm(models.Model):
    atm = models.ForeignKey(Atm, to_field='code')
    pincode = models.IntegerField(max_length=6)
    distance = models.FloatField()


class Atm_registered_callers(models.Model):
    atm = models.ForeignKey(Atm, to_field='code')
    callerid = CalleridField()
    is_active = models.BooleanField(default=True)
    registered_on = models.DateTimeField(auto_now_add = True, default = datetime.now)
    last_updated = models.DateTimeField(auto_now = True, auto_now_add = True, default = datetime.now)


class Atm_query_response_log(models.Model):
    query = models.ForeignKey(SMS_message_received)
    response = models.ForeignKey(SMS_message)
    source = models.CharField(max_length=3, null=True)


class CustomEvents:
    ATM_INFO_REQUESTED = 'ATM_INFO_REQUESTED'
    ATM_STATUS_UPDATED = 'ATM_STATUS_UPDATED'
    ATM_REGISTER_USER = 'ATM_REGISTER_USER'
    ACTIVE_ATMS_REGISTER_USER = 'ACTIVE_ATMS_REGISTER_USER'
    ATM_DEREGISTER_USER = 'ATM_DEREGISTER_USER'
    ALL_ATM_DEREGISTER_USER = 'ALL_ATM_DEREGISTER_USER'
