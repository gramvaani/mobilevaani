from vapp.stats.models import Stats, Stats_type
from pacsmodels import Handler_call_log, Grievance_log, Grievance_status, Grievance_sms_log
from mnews.models import ModerationEvents, ModerationEventRecorder

from vapp.stats.stats import get_callscount_callerscount_avgdur

from vapp.utils import get_ratio, generate_workbook, get_total_seconds

from datetime import datetime


def get_helpline_recordings_duration(recordings, start_datetime, end_datetime):
    return sum([recording.media.get_duration() for recording in recordings])


def get_call_recording_statistics(ai, start_datetime, end_datetime, stat_type = None):
    reports = []
    stats = Stats()
    stats.name = ai.name
    stats.calls, stats.callers, stats.avg_dur = get_callscount_callerscount_avgdur(ai.id, start_datetime, end_datetime)

    handler_recordings = Handler_call_log.objects.filter(ai = ai, media__time__range = (start_datetime, end_datetime))
    total_handler_recordings = handler_recordings.count()
    handler_recordings_dur = get_helpline_recordings_duration(handler_recordings, start_datetime, end_datetime)

    stats.total_handler_recordings = total_handler_recordings
    stats.avg_dur_handler_recordings = get_ratio( handler_recordings_dur, (1000 * total_handler_recordings) )
    return (stats, reports)


def get_grievance_statistics(ai, start_datetime, end_datetime, stat_type = None):
    (stats, reports) = get_call_recording_statistics(ai, start_datetime, end_datetime, stat_type)
    glog = Grievance_log.objects.filter(grievance__ai = ai, grievance__time__range = (start_datetime, end_datetime))
    stats.grievance_count = glog.count()
    stats.pending = glog.filter(status = Grievance_status.PR).count()
    stats.resolved = glog.filter(status = Grievance_status.RS).count()
    stats.flagged = glog.filter(status = Grievance_status.FG).count()
    updates = 0
    for each in glog:
        updates += each.grievance.news.comments.filter( time__range = (start_datetime, end_datetime) ).count()

    stats.updates = updates

    if stat_type == Stats_type.WEEKLY:
        details = {
                    'name': 'Grievance Details',
                    'header_row_count': 1,
                    'data': []
                   }

        details[ 'data' ] = get_grievance_details( ai )
        output_file = '/tmp/%s_stats.xlsx' % ai.name
        generate_workbook([ details ], output_file)
        reports.append( output_file )

    return (stats, reports)


def get_grievance_details( ai ):
    details = [['Grievance ID', 'Status', 'Tags', 'Recording Date', 'Publish Date', 'Handler', \
                'Latest Update Date', 'Resolution Date', 'Days Open']]

    for each in Grievance_log.objects.filter(grievance__ai = ai):
        resolution_date = None
        pub_date = None
        days_open = None
        last_update_date = None

        item = each.grievance.news

        mod_events_records = ModerationEventRecorder.objects.filter(item = item, event_type = ModerationEvents.ITEM_ARCHIVED)
        if each.status == Grievance_status.RS and mod_events_records.exists():
            resolution_date = mod_events_records.latest('id').timestamp

        sms_log = Grievance_sms_log.objects.filter(grievance = each.grievance)
        if sms_log.exists():
            pub_date = sms_log[0].sms_message.sent_time
            days_open = get_total_seconds( ( resolution_date or datetime.now() ) - pub_date ) / 86400

        if item.comments.exists():
            last_update_date = item.comments.latest('id').time

        details.append( [ item.id, each.status, item.tags, item.time, pub_date or '-', str(item.location), \
            last_update_date or '-', resolution_date or '-', days_open or '-'] )

    return details
