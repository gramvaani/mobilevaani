from django.contrib import admin

from models import Swasti_common_survey, Custom_ai_settings, RSBY_Mitra_contacts, Custom_stats_settings, Project_custom_stats_settings

admin.site.register(Swasti_common_survey)
admin.site.register(Custom_ai_settings)
admin.site.register(RSBY_Mitra_contacts)
admin.site.register(Custom_stats_settings)
admin.site.register(Project_custom_stats_settings)