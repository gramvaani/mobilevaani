from vapp.telephony.statemachine import BaseVappController
from customization.pacsmodels import Grievance_log, Grievance_sms_log, Grievance_status, Handler_call_log, RSBY_Mitra_contacts
from vapp.log import get_request_logger

logger = get_request_logger()


class BeneficiaryController(BaseVappController):

    def __init__(self, ai, sessionData, vi_data = {}):
        super(BeneficiaryController, self).__init__(ai, sessionData, vi_data)

    def pre_outgoingstart(self):
        return self.getStartCallParams(self.sessionData.callerid)

    def while_welcome__sm_action_success(self, events, eventData):
        return self.nextEventFromGrievanceStatus(self.sessionData.callerid)

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def nextEventFromGrievanceStatus(self, callerid):
        caller_grievances = Grievance_log.objects.filter(grievance__callerid = callerid)
        if caller_grievances.count() > 1:
            return 'pacs_beneficiary_enter_grievance'
        elif not caller_grievances.exists():
            return 'pacs_beneficiary_no_grievance_found'
        else:
            self.grievance_log = caller_grievances[0]
            self.mitra_assigned, self.mitra = self.is_mitra_assigned()
            if self.grievance_log.status == Grievance_status.RS:
                return 'pacs_beneficiary_grievance_already_resolved'
            elif self.grievance_log.status == Grievance_status.FG or (not self.mitra_assigned):
                return 'pacs_beneficiary_grievance_already_flagged'
            else:
                return 'pacs_beneficiary_call_handler'


    def pre_inputgrievanceid(self):
        return self.getPromptPlayAndGetParams(promptName = 'pacs_beneficiary_enter_grievance', maxDigits = 9, tries = 3)

    def while_inputgrievanceid__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        self.grievance_id = digits
        grievance_logs = Grievance_log.objects.filter(grievance__bookmark_id = self.grievance_id)
        if grievance_logs.exists():
            self.grievance_log = grievance_logs[0]
            if self.grievance_log.status == Grievance_status.RS:
                return 'pacs_beneficiary_resolved_grievance'
            elif self.grievance_log.status == Grievance_status.FG:
                return 'pacs_beneficiary_flagged_grievance'

            return 'pacs_beneficiary_valid_grievance'

        return 'pacs_beneficiary_invalid_grievance'

    def is_mitra_assigned(self):
        if self.grievance_log and self.grievance_log.grievance.news.location:
            results = RSBY_Mitra_contacts.objects.filter(contact__location_fk = self.grievance_log.grievance.news.location)
            if results.count() > 0:
                return (True, results[0].contact)
        return (False, None)

    def pre_invalidgrievanceentered(self):
        return self.getPromptParams('pacs_beneficiary_invalid_grievance')

    def pre_resolvedgrievanceentered(self):
        return self.getPromptParams('pacs_beneficiary_resolved_grievance')

    def pre_flaggedgrievanceentered(self):
        return self.getPromptParams('pacs_beneficiary_flagged_grievance')

    def pre_nogrievanceregistered(self):
        return self.getPromptParams('pacs_beneficiary_no_grievance_found')

    def pre_grievancealreadyresolved(self):
        return self.getPromptParams('pacs_beneficiary_grievance_already_resolved')

    def pre_grievancealreadyflagged(self):
        return self.getPromptParams('pacs_beneficiary_grievance_already_flagged')

    def pre_waitprompt(self):
        return self.getPromptParams('pacs_beneficiary_helpline_please_wait')
            
    def pre_callhandler(self):
        return self.getStartCallParams(self.mitra.number)


    def while_callhandler__sm_action_failure(self, events, eventData):
        uuid = eventData.get('Unique_ID')
        if uuid and uuid == self.sessionData.uuids[0]:
            return 'pacs_helpline_caller_hung_up'
        else:
            return 'pacs_helpline_handler_hung_up'


    def pre_handlerunavailableprompt(self):
        return self.getPromptParams('pacs_helpline_handler_unavailable')

    def pre_recordcall(self):
        self.call_log = Handler_call_log(ai = self.ai, handler = self.handler, callerid = self.sessionData.callerid)
        return self.getRecordCallParams(self.call_log, Handler_call_log.media)

    def pre_bridgecalls(self):
        return self.getBridgeCallsParams(self.sessionData.uuids[0], self.sessionData.uuids[1])

    def pre_thankyou(self):
        return self.getPromptParams('pacs_beneficiary_thank_you')


BeneficiaryStatesDescMap = [

    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop': ['sm_action_failure'],
        'welcome': ['sm_action_sucess'],
        },
    },

    {   'name':'incomingstart',
        'action':'answer',
        'transitions': {
            'stop':['sm_action_failure'],
        'welcome': ['sm_action_success'],
        },
    },

 {   'name':'welcome',
        'action':'none',
        'transitions': {
            'stop': ['sm_action_failure'],
            'inputgrievanceid': ['pacs_beneficiary_enter_grievance'],
            'nogrievanceregistered': ['pacs_beneficiary_no_grievance_found'],
            'grievancealreadyresolved': ['pacs_beneficiary_grievance_already_resolved'],
            'grievancealreadyflagged': ['pacs_beneficiary_grievance_already_flagged'],
            'waitprompt': ['pacs_beneficiary_call_handler']
        },
    },
    
    {   'name':'inputgrievanceid',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'waitprompt':['pacs_beneficiary_valid_grievance'],
            'invalidgrievanceentered':['pacs_beneficiary_invalid_grievance'],
            'resolvedgrievanceentered':['pacs_beneficiary_resolved_grievance'],
            'flaggedgrievanceentered':['pacs_beneficiary_flagged_grievance'],
            'thankyou':['sm_get_digits_no_digits']
        },
    },
    {   'name':'invalidgrievanceentered',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou': ['sm_action_success']
        },
    },
    {   'name':'resolvedgrievanceentered',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou': ['sm_action_success']
        },
    },
    {   'name':'flaggedgrievanceentered',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou': ['sm_action_success']
        },
    },
    {   'name':'nogrievanceregistered',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success']
        },
    },
    {   'name':'grievancealreadyresolved',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success']
        },
    },
    {   'name':'grievancealreadyflagged',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'thankyou':['sm_action_success']
        },
    },
    {   'name':'waitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'callhandler':['sm_action_success'],
        },
    },
    {   'name':'callhandler',
        'action':'originate',
        'transitions': {
            'handleranswered':['sm_action_success'],
            'stop':['pacs_helpline_caller_hung_up'],
            'handlerunavailable':['pacs_helpline_handler_hung_up'],
        },
    },

    {   'name':'handlerunavailable',
        'action':'break_media',
        'transitions': {
            'handlerunavailableprompt':['sm_action_failure', 'sm_action_success']
        },
    },


    {   'name':'handlerunavailableprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure', 'sm_action_success']
        },
    },
    {   'name':'handleranswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'recordcall',
        'action':'record_call',
        'transitions': {
            'stop':['sm_action_failure'],
            'bridgecalls':['sm_action_success'],
        },
    },
    {   'name':'bridgecalls',
        'action':'bridge',
        'transitions': {
            'stop':['sm_action_failure']
        }
    },
    {   'name':'thankyou',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure', 'sm_action_success']
        },
    },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {},
    },
]
