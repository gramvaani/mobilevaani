from django.conf.urls import url
from django.http import HttpResponse

from tastypie.authentication import ApiKeyAuthentication
from tastypie.resources import Resource
from tastypie.utils import trailing_slash
from tastypie import http

from app_manager.models import push_results, Report_creds, Report_log
from sms.models import SMS_message
from customer.models import Project
from customization.stats import get_praekelt_conversation_data, get_praekelt_survey_data, get_praekelt_downloadable_data
from vapp.utils import daterange, generate_workbook

from datetime import datetime, timedelta
from log import get_request_logger

logger = get_request_logger()

class CustomSMSResource( Resource ):
    class Meta:
        resource_name = 'sms'

    def prepend_urls( self ):
        return [
            url(r"^(?P<resource_name>%s)/mo%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'send_mo_sms' ), name = "api_send_mo_sms" ),
            url(r"^(?P<resource_name>%s)/atm_query%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_atm_search_query'), name="api_get_atm_search_query"),
        ]

    def send_mo_sms( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'post' ] )
        self.throttle_check( request )
        try:
            logger.info( "api request for sending MO SMS: {0}".format( request ) )
            data = {}
            data[ 'mobile' ]   = request.REQUEST[ 'mobile' ]
            data[ 'text' ]     = request.REQUEST[ 'text' ]
            data[ 'time' ]     = request.REQUEST[ 'time' ]
            data[ 'to' ]       = request.REQUEST[ 'to' ]
            data[ 'circle' ]   = request.REQUEST[ 'circle' ]
            data[ 'operator' ] = request.REQUEST[ 'operator' ]

            cred = Report_creds.objects.get( pk = 18 )
            push_results( cred, data, None )
            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True }, response_class = http.HttpCreated )
        except Exception, e:
            exception_str = "exception while running: send_mo_sms"
            logger.exception( exception_str )
        return self.create_response( request, { 'error': exception_str }, response_class = http.HttpApplicationError )

    def get_atm_search_query(self, request, **kwargs):
        from customization.utils import get_msg_type
        from customization.tasks import HandleRegistration, GetAtmInfo
        from sms.models import SMS_message_received

        self.method_check(request, allowed=['post'])
        self.throttle_check(request)
        try:
            logger.info("inside get_atm_search_query for request: {0}".format(request))
            recvd_msg = SMS_message_received(sender=request.REQUEST['mobile'],
                            time=datetime.strptime(str(request.REQUEST['time']), '%Y-%m-%d %H:%M:%S'),
                            message=request.REQUEST['text'])
            recvd_msg.save()
            eta = datetime.now()
            msg_type = get_msg_type(recvd_msg.message)
            if msg_type == 4:
                logger.info("scheduling activity for getting atm search results")
                GetAtmInfo.apply_async([recvd_msg.id, recvd_msg.message, recvd_msg.sender, 'sms'], eta=eta)
            else:
                atm_code = int(recvd_msg.message[:6]) if msg_type in [1,2] else None
                logger.info("scheduling activity for handling atm registration/de-registration requests")
                HandleRegistration.apply_async([recvd_msg.id, msg_type, atm_code, recvd_msg.sender], eta=eta)
            
            self.log_throttled_access(request)
            return self.create_response(request, {'success': True})
        except Exception, e:
            exception_str = "exception while running: get_atm_search_query"
            logger.exception(exception_str)
        return self.create_response(request, {'success': False, 'error': exception_str}, response_class=http.HttpApplicationError)

class CustomStatsResource(Resource):
    class Meta:
        resource_name = 'custom_stats'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/conversation%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_conversation_results'), name = "api_get_conversation_results"),
            url(r"^(?P<resource_name>%s)/sms%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_sms_results'), name = "api_get_sms_results"),
            url(r"^(?P<resource_name>%s)/survey%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_survey_results'), name = "api_get_survey_results"),
            url(r"^(?P<resource_name>%s)/report%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('download_report'), name = "api_download_report")
        ]

    def get_conversation_results(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.throttle_check(request)
        try:
            ai_id = request.REQUEST[ 'ai' ]
            current_date = request.REQUEST[ 'current_date' ]
            current_date_start = request.REQUEST[ 'current_date_start' ]
            yesterday_date_start = request.REQUEST[ 'yesterday_date_start' ]
            current_week_start = request.REQUEST[ 'current_week_start' ]
            previous_week_start = request.REQUEST[ 'previous_week_start' ]
            current_month_start = request.REQUEST[ 'current_month_start' ]
            previous_month_start = request.REQUEST[ 'previous_month_start' ]
            current_quarter_start_for_text_widget = datetime.strptime(str(request.REQUEST[ 'current_quarter_start_for_text_widget' ]), '%Y-%m-%dT%H:%M:%S')
            previous_quarter_start_for_text_widget = datetime.strptime(str(request.REQUEST[ 'previous_quarter_start_for_text_widget' ]), '%Y-%m-%dT%H:%M:%S')
            current_quarter_start_for_line_graph = datetime.strptime(str(request.REQUEST[ 'current_quarter_start_for_line_graph' ]), '%Y-%m-%dT%H:%M:%S')
            previous_quarter_start_for_line_graph = datetime.strptime(str(request.REQUEST[ 'previous_quarter_start_for_line_graph' ]), '%Y-%m-%dT%H:%M:%S')
            previous_year_start = datetime.strptime(str(request.REQUEST[ 'previous_year_start' ]), '%Y-%m-%d')
    
            data = get_praekelt_conversation_data(ai_id, current_date, current_date_start, yesterday_date_start, current_week_start, previous_week_start, current_month_start, previous_month_start, current_quarter_start_for_text_widget, previous_quarter_start_for_text_widget, current_quarter_start_for_line_graph, previous_quarter_start_for_line_graph, previous_year_start)
            
            self.log_throttled_access(request)
            return self.create_response(request, {'data': data})
        except Exception, e:
            exception_str = "exception while getting conversation results: %s" % str(e)
            logger.exception(exception_str)
        return self.create_response(request, {'error': exception_str}, response_class = http.HttpApplicationError)

    def get_sms_results(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.throttle_check(request)
        try:
            ai_id = request.REQUEST[ 'ai' ]
            current_date = request.REQUEST[ 'current_date' ]
            current_date_start = request.REQUEST[ 'current_date_start' ]
            yesterday_date_start = request.REQUEST[ 'yesterday_date_start' ]
            current_week_start = request.REQUEST[ 'current_week_start' ]
            previous_week_start = request.REQUEST[ 'previous_week_start' ]
            current_month_start = request.REQUEST[ 'current_month_start' ]
            previous_month_start = request.REQUEST[ 'previous_month_start' ]
            current_quarter_start_for_text_widget = datetime.strptime(str(request.REQUEST[ 'current_quarter_start_for_text_widget' ]), '%Y-%m-%dT%H:%M:%S')
            previous_quarter_start_for_text_widget = datetime.strptime(str(request.REQUEST[ 'previous_quarter_start_for_text_widget' ]), '%Y-%m-%dT%H:%M:%S')
            current_quarter_start_for_line_graph = datetime.strptime(str(request.REQUEST[ 'current_quarter_start_for_line_graph' ]), '%Y-%m-%dT%H:%M:%S')
            previous_quarter_start_for_line_graph = datetime.strptime(str(request.REQUEST[ 'previous_quarter_start_for_line_graph' ]), '%Y-%m-%dT%H:%M:%S')
            
            sms_qs = SMS_message.objects.filter(ai_id = ai_id)
            data = {}
            data['mt_sms_today_count'] = sms_qs.filter(sent_time__range = (current_date_start, current_date), sent_success = True).count()
            data['mt_sms_yesterday_count'] = sms_qs.filter(sent_time__range = (yesterday_date_start, current_date_start), sent_success = True).count()
            data['mt_sms_current_week_count'] = sms_qs.filter(sent_time__range = (current_week_start, current_date), sent_success = True).count()
            data['mt_sms_previous_week_count'] = sms_qs.filter(sent_time__range = (previous_week_start, current_week_start), sent_success = True).count()
            data['mt_sms_current_month_count'] = sms_qs.filter(sent_time__range = (current_month_start, current_date), sent_success = True).count()
            data['mt_sms_previous_month_count'] = sms_qs.filter(sent_time__range = (previous_month_start, current_month_start), sent_success = True).count()
            data['mt_sms_current_quarter_count'] = sms_qs.filter(sent_time__range = (current_quarter_start_for_text_widget, current_date), sent_success = True).count()
            data['mt_sms_previous_quarter_count'] = sms_qs.filter(sent_time__range = (previous_quarter_start_for_text_widget, current_quarter_start_for_text_widget), sent_success = True).count()
            
            report_log = Report_log.objects.filter(cred_id = 12, error_code = http.HttpCreated.status_code)
            data['mo_sms_today_count'] = report_log.filter(time__range = (current_date_start, current_date)).count()
            data['mo_sms_yesterday_count'] = report_log.filter(time__range = (yesterday_date_start, current_date_start)).count()
            data['mo_sms_current_week_count'] = report_log.filter(time__range = (current_week_start, current_date)).count()
            data['mo_sms_previous_week_count'] = report_log.filter(time__range = (previous_week_start, current_week_start)).count()
            data['mo_sms_current_month_count'] = report_log.filter(time__range = (current_month_start, current_date)).count()
            data['mo_sms_previous_month_count'] = report_log.filter(time__range = (previous_month_start, current_month_start)).count()
            data['mo_sms_current_quarter_count'] = report_log.filter(time__range = (current_quarter_start_for_text_widget, current_date)).count()
            data['mo_sms_previous_quarter_count'] = report_log.filter(time__range = (previous_quarter_start_for_text_widget, current_quarter_start_for_text_widget)).count()
            
            mo_messages_sent_per_week_distribution = []
            mt_messages_sent_per_week_distribution = []
            week_count = 0
            for week_start in daterange(previous_quarter_start_for_line_graph, current_quarter_start_for_line_graph, timedelta(days = 7)):
                week_end = week_start + timedelta(days = 7)
                mo_messages_sent_for_week = report_log.filter(time__range = (week_start, week_end)).count()
                mt_messages_sent_for_week = sms_qs.filter(sent_time__range = (week_start, week_end), sent_success = True).count()
                mo_messages_sent_per_week_distribution.append( [week_count, mo_messages_sent_for_week] )
                mt_messages_sent_per_week_distribution.append( [week_count, mt_messages_sent_for_week] )
                week_count += 1
            data[ 'mo_messages_sent_per_week_distribution' ] = mo_messages_sent_per_week_distribution
            data[ 'mt_messages_sent_per_week_distribution' ] = mt_messages_sent_per_week_distribution

            self.log_throttled_access(request)
            return self.create_response(request, {'data': data})
        except Exception, e:
            exception_str = "exception while getting sms results: %s" % str(e)
            logger.exception(exception_str)
        return self.create_response(request, {'error': exception_str}, response_class = http.HttpApplicationError)

    def get_survey_results(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.throttle_check(request)
        try:
            survey_ai_ids = request.REQUEST[ 'survey_ai_ids' ]
            current_date = request.REQUEST[ 'current_date' ]
            current_week_start = request.REQUEST[ 'current_week_start' ]
            current_month_start = request.REQUEST[ 'current_month_start' ]
            current_quarter_start_for_line_graph = datetime.strptime(str(request.REQUEST[ 'current_quarter_start_for_line_graph' ]), '%Y-%m-%dT%H:%M:%S')
            previous_quarter_start_for_line_graph = datetime.strptime(str(request.REQUEST[ 'previous_quarter_start_for_line_graph' ]), '%Y-%m-%dT%H:%M:%S')
            
            data = get_praekelt_survey_data(survey_ai_ids, current_date, current_week_start, current_month_start, current_quarter_start_for_line_graph, previous_quarter_start_for_line_graph)
            
            self.log_throttled_access(request)
            return self.create_response(request, {'data': data})
        except Exception, e:
            exception_str = "exception while getting survey results: %s" % str(e)
            logger.exception(exception_str)
        return self.create_response(request, {'error': exception_str}, response_class = http.HttpApplicationError)

    def download_report(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.throttle_check(request)
        try:
            project_id = request.REQUEST[ 'project_id' ]
            start_date = datetime.strptime( str(request.REQUEST[ 'start_date' ]), '%Y-%m-%dT%H:%M:%S' )
            end_date = datetime.strptime( str(request.REQUEST[ 'end_date' ]), '%Y-%m-%dT%H:%M:%S' )

            project = Project.objects.get( id = project_id )
            praekelt_data = get_praekelt_downloadable_data( project, start_date, end_date )
            filename = "Report for " + project.name + ".xlsx"
            data = [{ 'name': "Summary Data", 'header_row_count': 1, 'data': praekelt_data }]
            output = generate_workbook( data, in_memory = True )
            response = HttpResponse( output.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' )
            response['Content-Disposition'] = 'attachment; filename = %s' % filename
            return response
        except Exception, e:
            exception_str = "exception while downloading report: %s" % str(e)
            logger.exception(exception_str)
        return self.create_response(request, {'error': exception_str}, response_class = http.HttpApplicationError)
