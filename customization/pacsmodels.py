from django.db import models

from events import Event
import RQMC

from app_manager.models import App_instance, CalleridField
from callerinfo.models import Contact, CALLERID_LENGTH
from mnews.models import Bookmark, Bookmark_log, Channel, News, News_state, News_sms_log
from mnews.models import ModerationEvents, ModerationEventRecorder, PUB_ORDER_MAX
from mnews.models import is_published_items_reorder_needed, reorder_published_items
from sms.models import SMS_template, SMS_message
from media.models import Recording

from sms.tasks import SMSTask

from datetime import datetime

from vapp.log import get_request_logger
logger = get_request_logger()


class Grievance_status(object):
    RS = 'RS'
    PR = 'PR'
    FG = 'FG'


class Grievance_log(models.Model):

    STATUS_CHOICES = (
        (Grievance_status.RS, 'Resolved'),
        (Grievance_status.PR, 'Processing'),
        (Grievance_status.FG, 'Flagged'),
    )

    grievance = models.OneToOneField(Bookmark)
    status = models.CharField(max_length = 2, default = Grievance_status.PR, choices = STATUS_CHOICES)
    urn = models.CharField(max_length = 30, null = True)


class Sms_recipient_type(object):
    Caller = 'CALLER'
    RSBY_MITRA = 'RSBY_MITRA'


class Grievance_sms_log(models.Model):

    SMS_RECIPIENT_CHOICES = (
        (Sms_recipient_type.Caller, 'CALLER'),
        (Sms_recipient_type.RSBY_MITRA, 'RSBY_MITRA'),
    )

    grievance = models.ForeignKey(Bookmark)
    sms_message = models.ForeignKey(SMS_message)
    sms_recipient = models.CharField(max_length = 50, choices = SMS_RECIPIENT_CHOICES)


class RSBY_Mitra_contacts(models.Model):
    contact = models.ForeignKey(Contact)


class Custom_actions(object):
    PACS_SMS_CONTRIBUTOR_ON_PUB = 'PACS_SMS_CONTRIBUTOR_ON_PUB'
    PACS_SMS_RSBY_MITRA_ON_PUB = 'PACS_SMS_RSBY_MITRA_ON_PUB'


def pacs_news_save(news, **kwargs):
    if news.moderator:
        ModerationEventRecorder.record_event(news.moderator, news, news.event_type, news.fields_changed)

    if news.event_type == ModerationEvents.ITEM_PUBLISHED:
        bookmark, already_exists = Bookmark.get_or_create_bookmark(news, news.callerid)
        if bookmark:
            blog = Bookmark_log(bookmark = bookmark, accessed = False, bookmarked = True, source = 'ITP')
            blog.save()
            if (not already_exists) or ('location' in news.fields_changed):
		grievance_log, created = Grievance_log.objects.get_or_create(grievance = bookmark)
                send_sms_on_item_publish(news, bookmark)
    elif news.event_type == ModerationEvents.ITEM_ARCHIVED:
        grievance_log = get_grievance_log(news)
        if grievance_log:
            grievance_log.status = Grievance_status.RS
            grievance_log.save()

    if news.event_type in [ModerationEvents.ITEM_PUBLISHED, ModerationEvents.ITEM_ARCHIVED]:
        news.is_mod_flagged = False

    if kwargs['created']:
        event = Event.ADD
    else:
        event = Event.UPDATE
        
        if ('is_mod_flagged' in news.fields_changed):
            grievance_log = get_grievance_log(news)
            if grievance_log and news.is_mod_flagged:
                grievance_log.status = Grievance_status.FG
                grievance_log.save()

    news.transcript = get_and_update_urn(news)

    if is_published_items_reorder_needed(news):
        reorder_published_items(news, news.prev_order, news.prev_state)

    queue = App_instance.message_queue_from_id(news.ai_id)
    RQMC.push_instance(queue, news, event, extras = ('num_comments', 'num_unm_comments', 'parent_id'), relations = ('detail', 'summary',))
    if news.is_comment:
        RQMC.push_instance(queue, News.objects.get(pk = news.parent_id()), Event.UPDATE, extras = ('num_unm_comments', 'parent_id'), relations = ('detail', 'summary',))

    return news


def get_message_args(bookmark_id, callerid, RSBY_MITRA_contact, recipient_type):
    args = {}
    args['bookmark_id'] = bookmark_id

    if recipient_type == Sms_recipient_type.Caller:
        args['destination'] = callerid
        args['rsbymitra_name'] = RSBY_MITRA_contact.name
        args['rsbymitra_number'] = RSBY_MITRA_contact.number
    elif recipient_type == Sms_recipient_type.RSBY_MITRA:
        args['destination'] = RSBY_MITRA_contact.number
        args['callerid'] = callerid

    return args


def send_sms_on_item_publish(news, bookmark):
    try:
        RSBY_MITRA_contact = RSBY_Mitra_contacts.objects.get(contact__location_fk = news.location).contact

        contributor_msg_args = get_message_args(bookmark.bookmark_id, news.callerid, RSBY_MITRA_contact, Sms_recipient_type.Caller)
        contributor_message = SMS_template.objects.get(ai = news.ai, name = Custom_actions.PACS_SMS_CONTRIBUTOR_ON_PUB)
        contributor_message = contributor_message.process_template(contributor_msg_args)
        destination = contributor_msg_args['destination']
        logger.info('Sending SMS:' + contributor_message + " to:" + destination)
        contributor_sms = SMSTask.create_send_msg(news.ai.id, contributor_message, destination)
        sms_log = News_sms_log(news = news, sms = contributor_sms)
        sms_log.save()
        Grievance_sms_log(grievance = bookmark, sms_message = contributor_sms, sms_recipient = Sms_recipient_type.Caller).save()

        rsbymitra_msg_args = get_message_args(bookmark.bookmark_id, news.callerid, RSBY_MITRA_contact, Sms_recipient_type.RSBY_MITRA)
        rsbymitra_message = SMS_template.objects.get(ai = news.ai, name = Custom_actions.PACS_SMS_RSBY_MITRA_ON_PUB)
        rsbymitra_message = rsbymitra_message.process_template(rsbymitra_msg_args)
        destination = rsbymitra_msg_args['destination']
        logger.info('Sending SMS:' + rsbymitra_message + " to:" + destination)
        rsbymitra_sms = SMSTask.create_send_msg(news.ai.id, rsbymitra_message, destination)
        Grievance_sms_log(grievance = bookmark, sms_message = rsbymitra_sms, sms_recipient = Sms_recipient_type.RSBY_MITRA).save()
    except Exception, e:
        logger.exception("Exception in send_sms_on_item_publish %s" % str(e))
        pass


def pacs_generate_location(location):
    try:
        contact = RSBY_Mitra_contacts.objects.get(contact__location_fk = location).contact
        return [location.village, location.district.name, location.block.name, contact.name]
    except:
        return []


def get_and_update_urn(news):
    urn = news.comments.exclude(transcript = '').latest('id').transcript
    grievance_log = get_grievance_log(news)
    if grievance_log:
        grievance_log.urn = urn
        grievance_log.save()

    return urn


def get_grievance_log(news):
    try:
        bookmark = Bookmark.objects.get(news = news, callerid = news.callerid)
        return Grievance_log.objects.get(grievance = bookmark)
    except Exception, e:
        logger.exception("Exception in get_grievance_log %s" % str(e))


class Handler_call_log(models.Model):
    ai       = models.ForeignKey(App_instance)
    media    = models.ForeignKey(Recording)
    callerid = CalleridField(max_length = CALLERID_LENGTH)
    handler  = models.CharField(max_length = CALLERID_LENGTH)
