from celery.task import Task

from app_manager.common import get_queue_for_task
from app_manager.models import Cdr, App_instance
from survey.models import Record, Question_ff, Question_mc, Survey
from mnews.models import Transient_group_schedule, Groups_call_log, Item_heard_stats, News, News_state
from callerinfo.models import Contact_list, get_contact_name, get_distinct_numbers
from customization.models import CEDPA_usrs, CEDPA_usrs_numbers, CEDPA_processed_survey
from refer.models import Referral, Refer_ai_properties, Referral_group_call_log, Referral_status

from mnews.stats import get_contrib_count_by_caller
from refer.stats import get_header_for_n_weeks, get_eff_for_referrer, get_hdr_for_grouping_trend, get_referals_last_ndays, get_ref_stats
from survey.stats import get_survey_ids, get_answer_label, get_questions, get_cdrs_from_survey_cdrs
from vapp.stats.stats import get_callscount_callerscount_avgdur

from vapp.utils import generate_attachment, get_sorted_data, send_email, is_start_of_week, get_start_of_week
from datetime import *

from django.db.models import Sum
from xlrd import *
from django.template.loader import render_to_string

from log import get_request_logger
logger = get_request_logger()


class CallAndPlayPukarSurveyTask(Task):

    def get_survey_count(self, caller, caller_id_list):
        scount = caller_id_list.count(caller)
        return scount

    def get_caller_num(self, cdr_id_list):
        callers = Cdr.objects.filter(id__in = list(cdr_id_list)).values_list('callerid', flat = True)
        callers = [str(caller) for caller in callers]
        return callers

    def run(self, main_survey_id, max_tries, schedule_id, followup_survey_id):
        tomorrow = (datetime.now().today() + timedelta(days = 1)).weekday()
        final_callers = []
        cdr_ids = Record.objects.filter(survey = main_survey_id, order = 3).exclude(mc_choice = 1).values_list('cdr', flat = True)
        followup_survey_cdrs = Record.objects.filter(survey = followup_survey_id).values_list('cdr', flat = True)
        callers = self.get_caller_num(cdr_ids)
        followup_callers = self.get_caller_num(followup_survey_cdrs)
        for i in callers:
            if self.get_survey_count(i, callers) > self.get_survey_count(i, followup_callers):
                final_callers.append(i)
        active_schedules = Transient_group_schedule.objects.filter(active = True, day_of_week__name__in = str(tomorrow))
        schedule = Transient_group_schedule.objects.get(pk = schedule_id)
        if schedule in active_schedules:
            for number in set(final_callers):
                logger.info('Entering number ' + str(number) + 'into schedule: ' + str(schedule_id))
                group_call_log = Groups_call_log(number = number, ai = schedule.ai_group.ai, group_schedule = schedule)
                group_call_log.save()


class SurveyReportAttrib:
    pass


class CreateCEDPASurveyReport(Task):

    def get_caller_src(self, cdr):
        if cdr.ai.id == 10:
            return 'JMV'
        elif cdr.ai.id == 168:
            return 'SV'
        else:
            return cdr.ai.name

    def get_survey_records(self, records, labels, ques_count):
        ans_ques_count = 0
        survey_answer_row = ['-'] * ques_count

        for record in records:
            label = get_answer_label(record, labels)

            if label != '-':
                ans_ques_count += 1

            survey_answer_row[record.question.order - 1] = label

        return survey_answer_row, ans_ques_count


    def get_count_msgs_heard(self, callerid, tag, start, end):
        return Item_heard_stats.objects.filter(ai_id = 115, cdr__callerid = callerid, item__tags__contains = tag, \
                                            cdr__start_time__range = (start, end)).count()


    def get_all_usrs_data(self):
        data = []
        header = ['UID', 'Mob no.', 'Village', 'Panchayat', 'Block', 'Name of women', 'Age', 'Education', 'Name of husband',
                'Months of Pregnancy', 'Currently Lactating/Month', 'Included in baseline', 'Attended cohort workshop']
        ROW_TEMPLATE = ['-'] * len(header)
        data.append(header)
        body = []
        users = CEDPA_usrs.objects.all()
        for user in users:
            row = list(ROW_TEMPLATE)
            user_mob_numbers = CEDPA_usrs_numbers.objects.filter(usr__id = user.id)
            mob_num_count = user_mob_numbers.count()
            if user_mob_numbers:
                row[0] = user.id
                row[1] = user_mob_numbers[0].number
                row[2] = user.village
                row[3] = user.panchayat
                row[4] = user.block
                row[5] = user.usr_name
                row[6] = user.age
                row[7] = user.education
                row[8] = user.husband_name
                row[9] = user.months_of_pregnancy
                row[10] = user.currently_lactating
                row[11] = user.included_in_baseline
                row[12] = user.attended_cw_shop
                body.append(row)
                if mob_num_count > 1:
                    for i in range(1, mob_num_count):
                        body.append([user.id, user_mob_numbers[i].number, '-','-','-','-','-','-','-','-','-','-','-'])
        data += body
        return data


    def is_caller_info_available(self, callerid):
        if CEDPA_usrs_numbers.objects.filter(number = callerid):
            return True

        return False


    def get_caller_uid(self, callerid):
        return CEDPA_usrs_numbers.objects.filter(number = callerid)[0].usr.id


    def get_user(self, uid):
        return CEDPA_usrs.objects.get(id = uid)


    def get_discrepant_data(self):
        data = []
        header = ['UID', 'Mob no.', 'Name of women']
        ROW_TEMPLATE = ['-'] * len(header)
        data.append(header)
        contacts = CEDPA_usrs_numbers.objects.exclude(number = '').values_list('number', flat = True).distinct()
        body = []
        for contact in contacts:
            discrepant_contacts = CEDPA_usrs_numbers.objects.filter(number = contact)
            if (len(discrepant_contacts) > 1) or len(contact) != 12:
                for num in discrepant_contacts:
                    row = list(ROW_TEMPLATE)
                    row[0] = num.usr.id
                    row[1] = num.number
                    row[2] = CEDPA_usrs.objects.filter(id = num.usr.id)[0].usr_name
                    body.append(row)
            else:
                continue

        data += body
        return data


    def get_header_for_daily_stats_report(self, start_date, end_date):
        data = []
        data.append( [ 'Statistics from: %s to %s' % (start_date, end_date) ] )
        data.append( [ 'Number', 'No. of time messages on QoC heard', 'QoC: Total duration',
                    'No. of time messages on Entitlements heard', 'Entitlements: Total duration',
                    'No. of time messages on Drama heard', 'Drama: Total duration' ] )
        return data


    def get_msg_count(self, ai_id, callerid, start, end, tag):
        msg_count = 0
        item_heard_stats = Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, cdr__start_time__range = (start, end))
        qoc = 'qoc'
        info = 'information'
        govt_schemes = 'government schemes'
        drama = 'drama'
        schemes = 'entitlements'

        if tag == qoc:
            msg_count = item_heard_stats.filter(item__tags__icontains = info).exclude(item__tags__icontains = govt_schemes).exclude(\
                                                item__tags__icontains = drama).count()
        elif tag == schemes:
            msg_count = item_heard_stats.filter(item__tags__icontains = govt_schemes).exclude(item__tags__icontains = info).exclude(\
                                                item__tags__icontains = drama).count()
        elif tag == drama:
            msg_count = item_heard_stats.filter(item__tags__icontains = drama).filter(item__tags__icontains = info).filter(\
                                                item__tags__icontains = govt_schemes).count()

        return msg_count


    def get_listening_stats(self, app_selector_ai_id, mv_ai_id, start, end):
        data = self.get_header_for_daily_stats_report(start, end)

        callerids = Cdr.objects.filter(ai_id = app_selector_ai_id, start_time__range = (start, end), is_incoming = False,\
                                    answered_time__isnull = False).values_list('callerid', flat = True).distinct()

        for callerid in callerids:
            qoc_msg = self.get_msg_count(mv_ai_id, callerid, start, end, 'qoc')
            entitlements_msg = self.get_msg_count(mv_ai_id, callerid, start, end, 'entitlements')
            drama_msg = self.get_msg_count(mv_ai_id, callerid, start, end, 'drama')

            qoc_duration = self.get_total_listening_duration(mv_ai_id, callerid, start, end, 'qoc')
            entitlements_duration = self.get_total_listening_duration(mv_ai_id, callerid, start, end, 'entitlements')
            drama_duration = self.get_total_listening_duration(mv_ai_id, callerid, start, end, 'drama')

            data.append( [ callerid, qoc_msg, qoc_duration, entitlements_msg, entitlements_duration, drama_msg, drama_duration ] )

        return data


    def get_total_listening_duration(self, ai_id, callerid, start, end, tag):
        item_heard_stats = Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, cdr__start_time__range = (start, end))

        if tag == 'qoc':
            items = item_heard_stats.filter(item__tags__icontains = 'information').exclude(\
                      item__tags__icontains = 'government schemes').exclude(item__tags__icontains = 'drama').aggregate(Sum('duration'))
        elif tag == 'entitlements':
            items = item_heard_stats.filter(item__tags__icontains = 'government schemes').exclude(\
                      item__tags__icontains = 'information').exclude(item__tags__icontains = 'drama').aggregate(Sum('duration'))
        elif tag == 'drama':
            items = item_heard_stats.filter(item__tags__icontains = 'drama').filter(item__tags__icontains = 'information').filter(\
                      item__tags__icontains = 'government schemes').aggregate(Sum('duration'))
        else:
            return 0

        if not items['duration__sum']:
            return 0
        return items['duration__sum']


    def get_formatted_ff_ques_text(self, questions):
        return [ 'Audio Trans: %s' % q.text for q in questions if isinstance(q, Question_ff) ]


    def get_ques_text_and_order(self, questions):
        labels = {}
        ques_row = []

        for question in questions:
            ques_row.append(question.text)

            if isinstance(question, Question_mc):
                labels[question.order] = dict( enumerate( question.choices.split(',') ) )

        return ques_row, labels


    def get_survey_responses(self, survey_id, start_date, end_date, apply_caller_filter, apply_ques_filter, \
                            channel_stats_req = False):
        header = [ 'UID', 'Cdr', 'Mob no.', 'Source-JMV/SV', 'Village', 'Panchayat', 'Block', 'Name of women', 'Age',
                'Education', 'Name of Husband', 'Months of Pregnancy', 'Currently Lactating/Month', 'Included in baseline',
                'Attended cohort workshop', 'Survey Date', 'No. of Q\'s ans.', 'Current or previous exp.', 'Survey by',
                'Error Type', 'Verification Status', 'Comment', 'No. of time messages on Entitlements heard',
                'No. of time messages on QoC heard' ]

        data = []
        body = []

        questions = get_questions(survey_id)
        header += self.get_formatted_ff_ques_text(questions)
        ques_row, labels = self.get_ques_text_and_order(questions)

        header += ques_row
        data.append(header)

        uids = []
        records = Record.objects.filter(survey_id = survey_id)
        cdrs = get_cdrs_from_survey_cdrs(survey_id, start_date, end_date)
        for cdr in cdrs:
            if apply_caller_filter and (cdr.ai_id == 10):
                continue
            info = SurveyReportAttrib()
            info.user = None

            if self.is_caller_info_available(cdr.callerid):
                uid = self.get_caller_uid(cdr.callerid)
                info.user = CEDPA_usrs.objects.get(id = uid)
                # uid check done to avoid populating user info multiple times for same UID
                if uid not in uids:
                    uids.append(uid)

            info.cdr_id = cdr.id
            info.number = cdr.callerid
            info.call_source = self.get_caller_src(cdr)
            info.survey_date = str(cdr.answered_time if cdr.answered_time else cdr.start_time)

            info.tag_govt_schemes = info.tag_information = '-'
            if channel_stats_req:
                info.tag_govt_schemes = self.get_count_msgs_heard(cdr.callerid, 'government schemes', start_date, end_date)
                info.tag_information = self.get_count_msgs_heard(cdr.callerid, 'information', start_date, end_date)

            row_records = records.filter(cdr = cdr).order_by('order')
            info.survey_records, info.ans_question_count = self.get_survey_records(row_records, labels, len(ques_row))

            if apply_ques_filter and info.ans_question_count < 15:
                continue
            body.append( self.populate_rows(info) )

        data += get_sorted_data(body, 0, False)
        return data


    def populate_rows(self, survey_info):
        user = survey_info.user
        if user:
            user_info = [ survey_info.user.village, survey_info.user.panchayat, survey_info.user.block, survey_info.user.usr_name, \
                          survey_info.user.age, survey_info.user.education, survey_info.user.husband_name, \
                          survey_info.user.months_of_pregnancy, survey_info.user.currently_lactating, survey_info.user.included_in_baseline, \
                          survey_info.user.attended_cw_shop ]
        else:
            user_info = ['-'] * 11

        uid = [ survey_info.user.id ] if user else ['-']

        call_info = [ survey_info.cdr_id, survey_info.number, survey_info.call_source ]
        # Placeholder fields. Will be filled during moderation process
        # 'Current or previous exp.', 'Survey by', 'Error Type', 'Verification Status', 'Comment'
        placeholders = ['-'] * 5
        channel_statistics = [ survey_info.tag_govt_schemes, survey_info.tag_information, '-', '-' ]

        row = uid + call_info + user_info + [ survey_info.survey_date, survey_info.ans_question_count ] + placeholders + channel_statistics + survey_info.survey_records
        return row


    def run(self, ai_id, start_date, end_date, recipient_emails, apply_caller_filter, apply_ques_filter, report_type):
        survey_id = Survey.objects.get(ai_id = ai_id).id

        data = []
        #data.append( self.get_survey_responses(survey_id, start_date, end_date, apply_caller_filter, apply_ques_filter) )
        sheet_names = [ 'Survey Report' ]

        if ai_id == 197:
            app_selector_ai_id = 168
            mv_ai_id = 115
            data.append( self.get_survey_responses(survey_id, start_date, end_date, apply_caller_filter, apply_ques_filter, True) )
            data.append( self.get_listening_stats(app_selector_ai_id, mv_ai_id, start_date, end_date) )
            data.append( self.get_all_usrs_data() )
            data.append( self.get_discrepant_data() )

            sheet_names += 'Listening Statistics', 'User Information', 'Data Discrepancy'
            output = '/tmp/%s_CEDPA_Survey_Report_SV_%s_%s.xlsx' % (ai_id, start_date.strftime("%b_%d"), end_date.strftime("%b_%d"))
            subject = 'Survey report: CEDPA survey from Swasthya Vaani'

            if report_type == 'weekly':
                subject = 'Weekly Survey report: CEDPA survey from Swasthya Vaani'

        elif ai_id == 235:
            data.append( self.get_survey_responses(survey_id, start_date, end_date, apply_caller_filter, apply_ques_filter) )
            output = '/tmp/%s_CEDPA_Survey_Report_JMV_%s_%s.xlsx' % (ai_id, start_date.strftime("%b_%d"), end_date.strftime("%b_%d"))
            subject = 'Survey report: CEDPA survey from JMV'
            if report_type == 'weekly':
                subject = 'Weekly Survey report: CEDPA survey from JMV'

        body = 'Please find attached survey report'
        generate_attachment(data, output, True, sheet_names)
        send_email(subject, body, recipient_emails, attachments = output)



class ProcessCEDPASurveyReport(Task):

    def process_daily_report(self, worksheet, force_update = False):
        num_rows = worksheet.nrows - 1
        num_cells = worksheet.ncols - 1
        curr_row = -1
        while curr_row < num_rows:
            curr_row += 1
            row = worksheet.row(curr_row)
            if curr_row > 0: # Skipping the first row i.e header row
                cdr = Cdr.objects.filter(id = int(worksheet.cell_value(curr_row, 1)))[0]
                curr_prev_exp = worksheet.cell_value(curr_row, 17)
                survey_by = worksheet.cell_value(curr_row, 18)
                error_type = worksheet.cell_value(curr_row, 19)
                verification_status = worksheet.cell_value(curr_row, 20)
                comment = error_type = worksheet.cell_value(curr_row, 21)
                audio_trans_q1 = worksheet.cell_value(curr_row, 24)
                audio_trans_q2 = worksheet.cell_value(curr_row, 25)

                prev_processed_record = CEDPA_processed_survey.objects.filter(cdr = cdr, verification_status = verification_status, error_type = error_type,
                                        audio_transcript_q1 = audio_trans_q1, audio_transcript_q2 = audio_trans_q2, curr_prev_exp = curr_prev_exp,
                                        survey_by = survey_by, comment = comment).count()
                if prev_processed_record > 0 and not force_update:
                    logger.info("populate_CEDPA_survey_records: records for cdr " + str(cdr.id) + " already present.")
                else:
                    processed_record = CEDPA_processed_survey(cdr=cdr, verification_status=verification_status, error_type=error_type,
                                        audio_transcript_q1=audio_trans_q1, audio_transcript_q2=audio_trans_q2, curr_prev_exp=curr_prev_exp,
                                        survey_by=survey_by, comment=comment)
                    processed_record.save()


    def save_new_usr_info(self, row):
        new_usr = CEDPA_usrs(usr_name=row[5], village=row[2], panchayat=row[3], block=row[4], age=str(row[6]), education=row[7],
                        husband_name=row[8], months_of_pregnancy=str(row[9]), currently_lactating=str(row[10]),
                        included_in_baseline=row[11], attended_cw_shop=row[12], src='post_survey')
        new_usr.save()


    def save_new_usr_number(contact_num):
        # Add new user info in usr_number table
        last_usr = CEDPA_usrs.objects.filter().order_by('-id')[0]
        usr_number = CEDPA_usrs_numbers(usr = last_usr, number = contact_num)
        usr_number.save()


    def save_old_usr_new_num_info(self, uid, contact_num):
        old_usr = CEDPA_usrs.objects.filter(id=uid)[0]
        old_usr_with_new_num = CEDPA_usrs_numbers(usr=old_usr, number=contact_num)
        old_usr_with_new_num.save()


    def process_usr_info_report(self, worksheet, force_update = False):
        num_rows = worksheet.nrows - 1
        num_cells = worksheet.ncols - 1
        curr_row = -1
        while curr_row < num_rows:
            curr_row += 1
            row = worksheet.row(curr_row)
            if curr_row > 0:
                first_cell_elem = worksheet.cell_value(curr_row, 0)
                second_cell_elem = worksheet.cell_value(curr_row, 1)
                # Found new user who gave survey. Checking uid column and mob_number column
                uid_str = str(first_cell_elem).lower().strip()
                if (uid_str == 'null') and (len(second_cell_elem) >= 10):
                    self.save_new_usr_info(row)
                    self.save_new_usr_number(second_cell_elem)
                # Populating for user who gave survey with new number, previously not in DB
                elif (uid_str != 'null') and not CEDPA_usrs_numbers.objects.filter(usr__id=int(first_cell_elem), number=second_cell_elem):
                    self.save_old_usr_new_num_info(int(first_cell_elem), second_cell_elem)


    def process_discrepancy_report(self, worksheet, force_update=False):
        num_rows = worksheet.nrows - 1
        num_cells = worksheet.ncols - 1
        curr_row = -1
        while curr_row < num_rows:
            curr_row += 1
            row = worksheet.row(curr_row)
            if curr_row > 0:
                uid = int(worksheet.cell_value(curr_row, 0))
                mob_num = str(worksheet.cell_value(curr_row, 1))
                prev_usr_data = CEDPA_usrs_numbers.objects.filter(usr__id = uid, number = mob_num).count()
                if not (prev_usr_data > 0 and not force_update):
                    usr_data = CEDPA_usrs_numbers(id=CEDPA_usrs_numbers.objects.filter(usr__id = uid)[0].id, usr=CEDPA_usrs.objects.filter(id = uid)[0], number = mob_num)
                    usr_data.save()


    def process_xls_file(self, file):
        workbook = xlrd.open_workbook(file)
        self.process_daily_report(workbook.sheet_by_name('Daily Report'))
        self.process_usr_info_report(workbook.sheet_by_name('User Information'))
        self.process_discrepancy_report(workbook.sheet_by_name('Data Discrepancy'))


    def run(self, file_location):
        self.process_xls_file(file_location)



def get_contacts_contact_type( contact_lists ):
    cr_vol_contacts = []
    contact_type = {}
    for contact_list in contact_lists:
        contact_list_obj = Contact_list.objects.get( id=contact_list )
        contacts = contact_list_obj.contacts.get_query_set()
        for contact in contacts:
            if contact.number in cr_vol_contacts:
                continue
            cr_vol_contacts.append( contact.number )
            contact_type[ contact.number ] = contact_list_obj.name

    return cr_vol_contacts, contact_type


class CreateCRepVolEffReport(Task):

    def run(self, ai_id, contact_list_ids, to_emails, start_date = None, end_date = None):
        if not start_date or not end_date:
            today = datetime.combine(datetime.now().date(), datetime.min.time())
            end_date = get_start_of_week(today)
            start_date = end_date - timedelta(days = 7)

        ai = App_instance.objects.get(id = ai_id)
        subject, body, report_path = self.get_mail_content(ai, contact_list_ids, start_date, end_date)
        send_email(subject, body, to_emails, attachments = report_path)


    def get_mail_content(self, ai, contact_list_ids, start_date, end_date):
        subject = '%s_%s: CR-Volunteer effectiveness report' % (ai.id, ai.name)
        body = 'Please find attached effectiveness report.'
        report_path = self.create_eff_report(ai, contact_list_ids, start_date, end_date)
        return subject, body, report_path


    def create_eff_report(self, ai, contact_list_ids, start, end):
        #mnews_id: refer_instance_id
        MNEWS_REFER_INST_MAP = { 10: { 'IVR': 194 }, 137: { 'IVR': 221 } }
        cr_vol_contacts, contact_type = get_contacts_contact_type(contact_list_ids)
        active_refer_inst = []

        data = [self.get_pp_tr_contrib_count(ai, cr_vol_contacts, start, end)]
        #data.append(self.get_call_contrib_count(cr_vol_contacts, ai, start, end))
        #sheet_names = ['PP-TR contribs', 'Trends-Calls-Contribs']
        sheet_names = [ 'PP-TR contribs' ]
        #sheet_freeze_order = [ [2,1], [2,2] ]
        sheet_freeze_order = [ [2,1] ]

        for refer_src, refer_id in MNEWS_REFER_INST_MAP[ai.id].items():
            if not refer_id:
                continue
            refer_ai = App_instance.objects.get(pk = refer_id)
            referral_queryset = Referral.objects.filter(ai = refer_ai)

            data.append(self.get_eff_grp_by_referrer(refer_ai, ai, start, end, cr_vol_contacts, referral_queryset))
            #data.append(self.get_eff_trends(refer_ai, ai, start, end, cr_vol_contacts, referral_queryset))
            #sheet_names += '%s Referrals' % refer_src, 'Trends-%s Referrals' % refer_src
            sheet_names += [ '%s Referrals' % refer_src ]
            #sheet_freeze_order += [2,1], [2,2]
            sheet_freeze_order += [ [2,1] ]
            active_refer_inst.append(refer_id)

        #data.append(self.get_survey_resp(contact_type, start, end, ai, active_refer_inst))
        # Legend sheet generation part needs improvement
        data.append(self.create_legend_sheet())
        #sheet_names += 'Survey Response', 'Legend Sheet'
        sheet_names += [ 'Legend Sheet' ]
        #sheet_freeze_order += [1, 1], [1, 1]
        sheet_freeze_order += [ [1, 1] ]

        output = '/tmp/%s_%s_CR_Vol_eff.xlsx' % (ai.id, ai.name)
        if generate_attachment(data, output, True, sheet_names, True, sheet_freeze_order):
            return output


    def get_pp_tr_contrib_count( self, ai, contacts, start, end ):
        data = [ [ 'CR-Vol Name', 'CR-Vol Number', 'PP tag contribs', 'TR tag contribs', 'PP tags(Cumulative)', 'TR tags(Cumulative)' ] ]
        req_news_state = [ News_state.PUB, News_state.ARC ]

        for contact in contacts:
            pp_tagged = News.objects.filter( ai=ai, callerid=contact, tags__contains='PP', time__range=( start, end ), state__in=req_news_state ).count()
            tr_tagged = News.objects.filter( ai=ai, callerid=contact, tags__contains='TR', time__range=( start, end ), state__in=req_news_state ).count()
            pp_cumulative = News.objects.filter( ai=ai, callerid=contact, tags__contains='PP', time__lt=end, state__in=req_news_state ).count()
            tr_cumulative = News.objects.filter( ai=ai, callerid=contact, tags__contains='TR', time__lt=end, state__in=req_news_state ).count()
            data.append( [ get_contact_name( contact ), contact, pp_tagged, tr_tagged, pp_cumulative, tr_cumulative ] )

        return data


    def get_call_contrib_count( self, contacts, ai, start, end  ):
        col_names = [ 'CR-Vol Name', 'CR-Vol Number' ]
        col_for_trends = [ 'Calls', 'Avg. call duration', 'Contribs' ]
        data = get_hdr_for_grouping_trend( start, end, col_names, col_for_trends )

        for contact in contacts:
            callstats_currweek = get_callscount_callerscount_avgdur( ai.id, start, end, [ contact ] )
            callstats_currweek_minus_one = get_callscount_callerscount_avgdur( ai.id, start-timedelta( days=7 ), start, [ contact ] )
            callstats_currweek_minus_two = get_callscount_callerscount_avgdur( ai.id, start-timedelta( days=14 ), start-timedelta( days=7 ), [ contact ] )
            callstats_currweek_minus_three = get_callscount_callerscount_avgdur( ai.id, start-timedelta( days=21 ), start-timedelta( days=14 ), [ contact ] )

            contribs_currweek = get_contrib_count_by_caller( contact, ai, start, end )
            contribs_currweek_minus_one = get_contrib_count_by_caller( contact, ai, start-timedelta( days=7 ), start )
            contribs_currweek_minus_two = get_contrib_count_by_caller( contact, ai, start-timedelta( days=14 ), start-timedelta( days=7 ) )
            contribs_currweek_minus_three = get_contrib_count_by_caller( contact, ai, start-timedelta( days=21 ), start-timedelta( days=14 ) )

            data.append( [ get_contact_name( contact ), contact, callstats_currweek[ 0 ], callstats_currweek_minus_one[ 0 ],
                           callstats_currweek_minus_two[ 0 ], callstats_currweek_minus_three[ 0 ], callstats_currweek[ 2 ],
                           callstats_currweek_minus_one[ 2 ], callstats_currweek_minus_two[ 2 ], callstats_currweek_minus_three[ 2 ],
                           contribs_currweek, contribs_currweek_minus_one, contribs_currweek_minus_two, contribs_currweek_minus_three ] )

        return data


    def get_survey_resp( self, contact_type, start, end, mnews_ai, refer_ids ):
        CR_VOL_SURVEY_AI_IDS = { 10: [ 125, 126 ], 137: [] }
        report_columns = [ 'name', 'number', 'contact_type', 'referrals', 'reported_refs', 'contribs', 'reported_contribs', 'reported_cms' ]
        data = [ [ 'CR-Vol Name', 'CR-Vol Number', 'Is CR/Vol', 'Referrals Done', 'Reported Referrals', 'Contribs', 'Reported Contribs', 'Reported CMs' ] ]

        survey_ids = Survey.objects.filter( ai__id__in=CR_VOL_SURVEY_AI_IDS[ mnews_ai.id ], is_enabled=True ).values_list( 'id', flat=True ).distinct()
        survey_resp = {}

        for survey_id in survey_ids:
            records = Record.objects.filter( survey=survey_id, time__range=( start, end ) )
            cdrs = Cdr.objects.filter( id__in=records.values_list( 'cdr', flat = True ) )
            for cdr in cdrs:
                if cdr.callerid not in contact_type:
                    continue
                row_records = records.filter( cdr=cdr ).order_by( 'order' ).values_list( 'qu_value', flat=True )
                if cdr.callerid not in survey_resp:
                    survey_resp[ cdr.callerid ] = {}
                    survey_resp[ cdr.callerid ][ 'name' ] = get_contact_name( cdr.callerid )
                    survey_resp[ cdr.callerid ][ 'number' ] = cdr.callerid
                    survey_resp[ cdr.callerid ][ 'contact_type' ] = contact_type[ cdr.callerid ]
                    survey_resp[ cdr.callerid ][ 'referrals' ] = Referral.objects.filter( ai__id__in=refer_ids, referring_contact=cdr.callerid, time_added__range=(start, end) ).count()
                    survey_resp[ cdr.callerid ][ 'contribs' ] = get_contrib_count_by_caller( cdr.callerid, mnews_ai, start, end )
                    # Needs improvement regarding hard-coded numbers
                    survey_resp[ cdr.callerid ][ 'reported_refs' ] = str( row_records[ 0 ] )
                    try:
                        survey_resp[ cdr.callerid ][ 'reported_contribs' ] = str( row_records[ 1 ] )
                        survey_resp[ cdr.callerid ][ 'reported_cms' ] = str( row_records[ 2 ] )
                    except:
                        pass
                else:
                    survey_resp[ cdr.callerid ][ 'reported_refs' ] += ', ' + str( row_records[ 0 ] )
                    try:
                        survey_resp[ cdr.callerid ][ 'reported_contribs' ] += ', ' + str( row_records[ 1 ] )
                        survey_resp[ cdr.callerid ][ 'reported_cms' ] += ', ' + str( row_records[ 2 ] )
                    except:
                        pass

        for callerid, attribs_dict in survey_resp.items():
            data.append( [ attribs_dict.get( column ) for column in report_columns ] )

        return data


    def get_eff_grp_by_referrer(self, refer_ai, instance_ai, start, end, contacts, referral_queryset = None):

        if not referral_queryset:
            referral_queryset = Referral.objects.filter(ai = refer_ai)

        #data = [ [ 'Referrer name', 'Referrer number', 'Total Referrals', 'Ratio', 'No. of referred contacts', 'Out-bound calls',
        #          'Calls(1 week)', 'Calls(2 weeks)', 'Contribs(1 week)', 'Contribs(2 weeks)' ] ]

        data = [ [ 'Referrer name', 'Referrer number', 'Total Referrals', 'Successful Referrals' ] ]
        body = []

        for contact in contacts:
            data_body = [ get_contact_name(contact), contact ]
            eff_data = get_eff_for_referrer(refer_ai, instance_ai, contact, start, end, referral_queryset)
            data_body.append( eff_data['total_refs'] )
            data_body.append( eff_data['success_refs'])
            #data_body += eff_data['total_refs'], eff_data['ratio'], eff_data['min_three_calls_twoweeks'], eff_data['outbound_calls']
            #data_body += eff_data['calls_oneweek_postref'], eff_data['calls_twoweeks_postref'], eff_data['contribs_oneweek_postref'], eff_data['contribs_twoweeks_postref']
            body.append(data_body)

        data += get_sorted_data(body, 2)
        return data


    def get_eff_trends(self, refer_ai, instance_ai, start, end, contacts, referral_queryset=None):

        if not referral_queryset:
            referral_queryset = Referral.objects.filter(ai=refer_ai)

        col_names = [ 'Referrer name', 'Referrer number' ]
        col_for_trends = [ 'Total referrals', 'Ratio', 'No. of referred contacts', 'Out-bound calls', 'Calls(1 week)', 'Calls(2 weeks)', 'Contribs(1 week)', 'Contribs(2 weeks)' ]
        eff_report_columns = ['total_refs', 'ratio', 'min_three_calls_twoweeks', 'outbound_calls', 'calls_oneweek_postref', 'calls_twoweeks_postref', 'contribs_oneweek_postref', 'contribs_twoweeks_postref']

        data = get_hdr_for_grouping_trend(start, end, col_names, col_for_trends)

        for contact in contacts:
            curr_week = get_eff_for_referrer(refer_ai, instance_ai, contact, start, end, referral_queryset)
            curr_week_minus_one = get_eff_for_referrer(refer_ai, instance_ai, contact, start-timedelta(days=7), start, referral_queryset)
            curr_week_minus_two = get_eff_for_referrer(refer_ai, instance_ai, contact, start-timedelta(days=14), start-timedelta(days=7), referral_queryset)
            curr_week_minus_three = get_eff_for_referrer(refer_ai, instance_ai, contact, start-timedelta(days=21), start-timedelta(days=14), referral_queryset)

            effectiveness_stats = []
            for column in eff_report_columns:
                effectiveness_stats.append(curr_week[column])
                effectiveness_stats.append(curr_week_minus_one[column])
                effectiveness_stats.append(curr_week_minus_two[column])
                effectiveness_stats.append(curr_week_minus_three[column])

            data.append( [get_contact_name(contact), contact] + effectiveness_stats)

        return data


    def create_legend_sheet( self ):
        data = [ [ '---PP-TR contribs---' ] ]
        data.append( [ 'CR/Vol Name' ] )
        data.append( [ 'CR/Vol Number' ] )
        data.append( [ 'PP tag:', 'PP tag contribs for the week from CR/Volunteer' ] )
        data.append( [ 'TR tag:', 'TR tag contribs for the week from CR/Volunteer' ] )
        data.append( [ 'PP tags(Cumulative):', 'Cumulative PP tags contribs from CR/Volunteer' ] )
        data.append( [ 'TR tags(Cumulative):', 'Cumulative TR tag contribs from CR/Volunteer' ] )
        data.append( [ '---Trends-Call-Contribs---' ] )
        data.append( [ 'Calls' ] )
        data.append( [ 'Avg. call duration:', 'Measured in seconds' ] )
        data.append( [ 'Contribs:', 'Published or Archived contributions' ] )
        data.append( [ '---Referrals(SMS or IVR)---' ] )
        data.append( [ 'Referrer name, Referrer number:', 'Contact information for CR-Volunteer who made referrals in the given duration' ] )
        data.append( [ 'Total Referrals:', 'Referrals by CR-Vol for same duration. This excludes duplicate referrals & self-referral' ] )
        data.append( [ 'Ratio:', 'Calls 2 weeks post referral/Total referrals' ] )
        data.append( [ 'No. of referred contacts:', 'No. of referred contacts (with at-least 3 calls in next 2 weeks)' ] )
        data.append( [ 'Out-bound calls:', 'Out-bound calls received by referees' ] )
        data.append( [ 'Calls(1 week):', 'Calls from referees 1 week post-referral' ] )
        data.append( [ 'Calls(2 weeks):', 'Calls from referees 2 weeks post-referral' ] )
        data.append( [ 'Contribs(1 week):', 'Contribs from referees 1 week post-referral' ] )
        data.append( [ 'Contribs(2 weeks):', 'Contribs from referees 2 week post-referral' ] )
        data.append( [ '---Trends-Referrals(SMS or IVR)---' ] )
        data.append( [ '---Survey-Response---' ] )
        data.append( [ 'Results of survey done for the same. It has columns for actual calls and news contribs put along with columns for reported calls, count of CMs and contribs.' ] )

        return data



class DailyReferStatsForCRepVol(Task):

    def run(self, ai_id, contact_list_ids, to_emails):
        today = datetime.now().date()
        contacts = get_distinct_numbers(contact_list_ids)
        ai = App_instance.objects.get(id = ai_id)

        stats = self.get_stats_for_ndays(ai, 1, today, contacts)

        subject = '%s_%s CR-Volunteer refer stats for: %s' % (ai.id, ai.name, today - timedelta(days = 1))
        content = render_to_string('refer/stats.html', stats)
        attachment = self.get_stats_distrib_report(ai, today, contacts, '/tmp/%s_%s_CR_Vol_refer_stats_report.xlsx' % (ai.id, ai.name))
        send_email(subject, content, to_emails, attachments = attachment)

    def get_stats_for_ndays(self, instance_ai, n, end_date, contacts):
        start_date = end_date - timedelta(days = n)
        refer_ai_ids = Refer_ai_properties.objects.filter(push_ai = instance_ai).values_list('ai_id', flat = True)
        refer_ais = App_instance.objects.filter(id__in = refer_ai_ids)

        stats = get_ref_stats(refer_ai_ids, start_date, end_date, referrers = contacts)
        stats['stats_table'] = self.get_stats_table(refer_ais, end_date, contacts)

        return stats

    def get_stats_table(self, refer_ais, end_date, contacts):
        sorted_data = self.collect_sorted_data(refer_ais, end_date, contacts)
        row_template_of_sorted_data = ['contact_name', 'contact_number', 'refs_yesterday', 'complete_refs_yesterday', 'refs_last_7',
                                       'complete_refs_last_7', 'refs_last_14', 'complete_refs_last_14', 'total_refs']

        data_for_presentation = []
        referrer_info_row = ['Referrer Name', 'Referrer Number']

        header_top_row = referrer_info_row + ['Number of referrals (successful call-outs)']
        header_second_row = [''] * len(referrer_info_row) + [str(end_date - timedelta(days = 1)), 'Last 7 days', 'Last 14 days', 'Total']
        header = [header_top_row, header_second_row]
        body = []

        refs_yesterday = refs_last_7 = refs_last_14 = callback_yesterday = callback_last_7 = callback_last_14 = refs_total = 0

        for row in sorted_data:
            body.append([ row[row_template_of_sorted_data.index('contact_name')], row[row_template_of_sorted_data.index('contact_number')],
                    '%s (%s)' % (row[row_template_of_sorted_data.index('refs_yesterday')], row[row_template_of_sorted_data.index('complete_refs_yesterday')]),
                    '%s (%s)' % (row[row_template_of_sorted_data.index('refs_last_7')], row[row_template_of_sorted_data.index('complete_refs_last_7')]),
                    '%s (%s)' % (row[row_template_of_sorted_data.index('refs_last_14')], row[row_template_of_sorted_data.index('complete_refs_last_14')]),
                    row[row_template_of_sorted_data.index('total_refs')] ])

            refs_yesterday += row[row_template_of_sorted_data.index('refs_yesterday')]
            refs_last_7    += row[row_template_of_sorted_data.index('refs_last_7')]
            refs_last_14   += row[row_template_of_sorted_data.index('refs_last_14')]
            refs_total     += row[row_template_of_sorted_data.index('total_refs')]

            callback_yesterday += row[row_template_of_sorted_data.index('complete_refs_yesterday')]
            callback_last_7    += row[row_template_of_sorted_data.index('complete_refs_last_7')]
            callback_last_14   += row[row_template_of_sorted_data.index('complete_refs_last_14')]

        footer = [ 'Total', '', '%s (%s)' % (refs_yesterday, callback_yesterday), '%s (%s)' % (refs_last_7, callback_last_7), '%s (%s)' % (refs_last_14, callback_last_14), refs_total ]

        data_for_presentation += header
        data_for_presentation += body
        data_for_presentation.append(footer)
        return data_for_presentation


    def collect_sorted_data(self, refer_ais, end_date, contacts):
        data = []

        for contact in contacts:
            (referrals_total, complete_referrals_total) = get_referals_last_ndays(refer_ais, contact, end_date, 0, True)
            (referrals_1_day, complete_referrals_1) = get_referals_last_ndays(refer_ais, contact, end_date, 1)
            (referrals_7_days, complete_referrals_7) = get_referals_last_ndays(refer_ais, contact, end_date, 7)
            (referrals_14_days, complete_referrals_14) = get_referals_last_ndays(refer_ais, contact, end_date, 14)

            data.append([ get_contact_name(contact), contact, referrals_1_day, complete_referrals_1, referrals_7_days, complete_referrals_7, referrals_14_days, complete_referrals_14, referrals_total ])

        data = get_sorted_data(data, 3)
        return data


    def get_hdr_for_distrib_report(self, refer_ais, end_date):
        header = []
        time_duration_columns = [str(end_date - timedelta(days = 1)), 'Last 7 days', 'Last 14 days', 'Total']
        hdr_top_row = ['Referrer Name', 'Referrer Number']

        hdr_second_row = [''] * len(hdr_top_row)
        for ai in refer_ais:
            hdr_top_row += ['%s (Successful call-outs)' % ai.name] + [''] * (len(time_duration_columns) - 1)
            hdr_second_row += time_duration_columns

        header.append(hdr_top_row)
        header.append(hdr_second_row)

        return header


    def get_stats_distrib_report(self, instance_ai, end_date, contacts, output = '/tmp/CR-Vol_daily_refer_stats_distrib.xlsx'):
        refer_ai_ids = Refer_ai_properties.objects.filter(push_ai = instance_ai).values_list('ai_id', flat = True)
        refer_ais = App_instance.objects.filter(id__in = refer_ai_ids)

        body = []
        aggregate_stats = {}
        for ai_id in refer_ai_ids:
            aggregate_stats[ai_id] = { 'refs_yesterday': 0, 'callback_yesterday': 0, 'refs_last_7': 0, 'callback_last_7': 0, 'refs_last_14': 0, 'callback_last_14': 0, 'refs_total': 0 }

        for contact in contacts:
            report_row = [ get_contact_name(contact), contact ]
            for ai_id in refer_ai_ids:
                ai = App_instance.objects.filter(id = ai_id)

                (referrals_total, complete_referrals_total) = get_referals_last_ndays(ai, contact, end_date, 0, True)
                (referrals_1_day, complete_referrals_1) = get_referals_last_ndays(ai, contact, end_date, 1)
                (referrals_7_days, complete_referrals_7) = get_referals_last_ndays(ai, contact, end_date, 7)
                (referrals_14_days, complete_referrals_14) = get_referals_last_ndays(ai, contact, end_date, 14)

                aggregate_stats[ai_id]['refs_yesterday'] += referrals_1_day
                aggregate_stats[ai_id]['callback_yesterday'] +=  complete_referrals_1
                aggregate_stats[ai_id]['refs_last_7'] +=  referrals_7_days
                aggregate_stats[ai_id]['callback_last_7'] +=  complete_referrals_7
                aggregate_stats[ai_id]['refs_last_14'] +=  referrals_14_days
                aggregate_stats[ai_id]['callback_last_14'] +=  complete_referrals_14
                aggregate_stats[ai_id]['refs_total'] +=  referrals_total

                report_row += [ '%s (%s)' % (referrals_1_day, complete_referrals_1), '%s (%s)' % (referrals_7_days, complete_referrals_7),
                              '%s (%s)' % (referrals_14_days, complete_referrals_14), referrals_total ]

            body.append(report_row)

        footer = [ 'Total', '' ]
        for ai_id in refer_ai_ids:
            footer += [ '%s (%s)' % (aggregate_stats[ai_id]['refs_yesterday'], aggregate_stats[ai_id]['callback_yesterday']),
                        '%s (%s)' % (aggregate_stats[ai_id]['refs_last_7'], aggregate_stats[ai_id]['callback_last_7']),
                        '%s (%s)' % (aggregate_stats[ai_id]['refs_last_14'], aggregate_stats[ai_id]['callback_last_14']),
                        aggregate_stats[ ai_id ]['refs_total'] ]

        data = []
        data += self.get_hdr_for_distrib_report(refer_ais, end_date)
        data += body
        data.append(footer)
        generate_attachment(data, output)
        return output


class GetAtmInfo(Task):

    def get_registration_msg(self, atms, callerid, source):
        from customization.models import CustomEvents, Atm_registered_callers
        from mnews.models import Event_sms_template
        source = source if source == 'sms' else 'ivr'

        if source == 'sms':
            reg_log, created = Atm_registered_callers.objects.get_or_create(atm=atms[0],
                                                                            callerid=callerid)
            if created:
                template = Event_sms_template.objects.filter(event=CustomEvents.ATM_REGISTER_USER)[0].template
                msg = template.process_template({'atm_code':atms[0].code})
                return msg
        elif source == 'ivr':
            msg = ''
            old_msg = ''
            for atm in atms:
                reg_log, created = Atm_registered_callers.objects.get_or_create(atm=atm,
                                                                                callerid=callerid)
                if created:
                    msg += '{0}'.format(atm.code)
                    msg += ', '
                    if len(msg) > 120:
                        msg = old_msg
                        break
                    old_msg = msg
            msg = msg.rstrip(', ')
            template = Event_sms_template.objects.filter(event=CustomEvents.ACTIVE_ATMS_REGISTER_USER)[0].template
            msg = template.process_template({'atm_codes':msg})
            return msg

    def run(self, query_id, query_text, callerid, source):
        from customization.models import Atm, Atm_query_response_log, CustomEvents, Atm_registered_callers
        import requests
        from mnews.models import Event_sms_template
        from mnews.tasks import CallAndPlayTask
        from sms.tasks import SMSTask

        HOSTNAME = '192.168.1.2:8081'
        REQUEST_URL = 'http://%s/odtSearch/Query.jsp?q=' % HOSTNAME
        MAX_TRIES = 3

        url = REQUEST_URL + query_text
        result = requests.get(url)
        if result.status_code == 200:
            try:
                atm_codes = result.json()
                if len(atm_codes):
                    # [code]-[bank]-[address]-[queue xx | no cash]
                    atms = [ atm for atm in Atm.objects.filter(code__in=atm_codes) ]
                    atms.sort(key=Atm.has_cash, reverse=True)
                    msg = ''
                    old_msg = ''
                    for atm in atms:
                        msg += str(atm.code) + '-'
                        msg += atm.bank_code + '-'
                        msg += atm.address + '-'
                        msg += atm.get_queue_size() if atm.has_cash() else 'No Cash'
                        msg += '. '
                        if len(msg) > 180:
                            msg = old_msg
                            break
                        old_msg = msg
                    msg = msg.rstrip('. ')
                    template = Event_sms_template.objects.filter(event=CustomEvents.ATM_INFO_REQUESTED)[0].template
                    msg = template.process_template({'text':msg})
                    sms = SMSTask.create_send_msg(template.ai.id, msg, callerid,
                                                  max_tries=MAX_TRIES)
                    Atm_query_response_log(query_id=query_id,
                                           response_id=sms.id, source=source).save()

                    if not Atm_registered_callers.objects.filter(callerid=callerid).exists() and source == 'sms':
                        schedule = Transient_group_schedule.objects.get(pk=584)
                        ai = schedule.ai_group.ai
                        eta = datetime.now()
                        gcl = Groups_call_log(number=callerid,
                                              ai=ai,
                                              group_schedule=schedule)
                        gcl.save()
                        queue = get_queue_for_task(ai, default_queue='push')
                        CallAndPlayTask.apply_async([gcl.id], eta=eta, queue=queue)

                    registration_msg = self.get_registration_msg(atms, callerid, source)
                    if registration_msg:
                        sms = SMSTask.create_send_msg(template.ai.id, registration_msg, callerid,
                                                      max_tries=MAX_TRIES)
                        Atm_query_response_log(query_id=query_id,
                                               response_id=sms.id, source=source).save()
            except Exception, e:
                logger.exception(str(e))


class HandleRegistration(Task):
    """
    Return Codes
    1 - ###### STOP
    2 - ###### START
    3 - STOP ALL
    4 - freetext search
    """
    def run(self, query_id, action, atm_code, callerid, source='sms'):
        from customization.models import Atm, Atm_registered_callers, Atm_query_response_log, CustomEvents
        from mnews.models import Event_sms_template
        from sms.tasks import SMSTask

        MAX_TRIES = 3

        try:
            template = None
            if action == 1:
                reg_log = Atm_registered_callers.objects.get(atm__code=atm_code,
                                                             callerid=callerid, is_active=True)
                reg_log.is_active = False
                reg_log.save()
                template = Event_sms_template.objects.filter(event=CustomEvents.ATM_DEREGISTER_USER)[0].template
                msg = template.process_template({'atm_code':atm_code})
            elif action == 2:
                atm = Atm.objects.get(code=atm_code)
                reg_log, created = Atm_registered_callers.objects.get_or_create(atm=atm,
                                                                                callerid=callerid)
                if not created and not reg_log.is_active:
                    reg_log.is_active = True
                    reg_log.save()
                template = Event_sms_template.objects.filter(event=CustomEvents.ATM_REGISTER_USER)[0].template
                msg = template.process_template({'atm_code':atm_code})
            elif action == 3:
                Atm_registered_callers.objects.filter(callerid=callerid,
                                                      is_active=True).update(is_active=False)
                template = Event_sms_template.objects.filter(event=CustomEvents.ALL_ATM_DEREGISTER_USER)[0].template
                msg = template.message

            sms = SMSTask.create_send_msg(template.ai.id, msg, callerid,
                                          max_tries=MAX_TRIES)
            Atm_query_response_log(query_id=query_id,
                                   response_id=sms.id, source=source).save()
        except Exception as e:
            logger.error(str(e))
