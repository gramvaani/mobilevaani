def get_msg_type(msg):
    """
    Return Codes
    1 - ###### STOP
    2 - ###### START
    3 - STOP ALL
    4 - freetext search
    """

    import re
    rstop = '^\d{6} STOP$'
    rstart = '^\d{6} START$'
    rstopall = '^STOP ALL$'

    if re.match(rstop, msg, re.IGNORECASE):
        return 1
    elif re.match(rstart, msg, re.IGNORECASE):
        return 2
    elif re.match(rstopall, msg, re.IGNORECASE):
        return 3
    else:
        return 4
 