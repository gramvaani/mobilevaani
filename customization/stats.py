from django.template.loader import render_to_string

from tastypie import http

from vapp.app_manager.models import App_instance, Cdr, Transition_event, Report_log

from vapp.mnews.models import Channel, News, Bookmark_log, Bookmark, Forwarding_log, News_state, Category, Channel, Item_heard_stats
from vapp.mnews.stats import get_tags_dict

from vapp.refer.models import Refer_ai_properties, Referral
from vapp.callerinfo.models import Contact, get_distinct_numbers, Contact_list
from vapp.location.models import Location
from vapp.customization.models import Custom_stats_settings, Swasti_common_survey

from vapp.stats.models import Stats_type, Stats
from vapp.stats.stats import get_calls, get_total_duration, get_callscount_callerscount_avgdur

from pacs_stats import get_call_recording_statistics, get_grievance_statistics

from vapp.utils import is_start_of_month, generate_workbook, get_total_seconds, get_ratio, send_email, \
is_start_of_week, LINE_BREAK, load_vapp_module, daterange
from telephony.utils import get_formatted_number
from vapp.survey.models import Question_mc, Question_qu, Question_ff, Survey, Survey_cdr, Record
from vapp.app_selector.stats import get_app_selector_ais_names_choices
from vapp.app_selector.models import SelectionLog
from vapp.customer.models import Project
from vapp.callerinfo.utils import get_caller_profile_for_project
from vapp.survey.stats import get_answer_label, get_bin_size, get_records_row, get_custom_data, get_mc_labels, \
has_answered_survey, get_default_report_header, get_questions, get_callerscount_no_ques_answered, get_mc_results,\
get_qu_results
from vapp.registration.models import Registration
from helpline.models import Call
from sms.models import SMS_message
from helpline.stats import get_conversation_duration

from datetime import *
from dateutil.relativedelta import relativedelta
import operator
import math
import ast

from log import get_request_logger
logger = get_request_logger()

news_states = [News_state.PUB, News_state.ARC, News_state.UNM]
MV_PARTNER_IDS = [9, 15, 17, 18, 20, 21, 23, 24, 25, 26, 27, 56, 57, 58, 63, 71, 73, 74, 75, 79, 80, 81, 82, 83, 88, 89, 91, 103,
                   115, 129, 131, 139, 140, 141, 142, 143, 161, 174, 190, 199, 203, 206, 207, 209, 210, 214, 215, 237, 246, 249,
                   252, 255, 258, 261, 272, 277, 278, 282, 304, 377, 386, 429]


def generate_userinfo_stats(ai_id, callerids):
    
    data = []
    
    header = ['Number', 'First Call In', 'Last Call In', 'Total Calls', 'PUB/ARC Contribs', 'REJ Contribs', 'Calls(last 3 months)',\
              'PUB/ARC Contribs(last 3 months)', 'Referrals', 'Items Liked', 'Items Forwarded', 'Is Volunteer/CR ', \
              'Village', 'District', 'Block', 'State', 'Location Source(Contact/Contribution)', 'Avg Keypresses(per minute)']
    
    top_ten_tags = get_n_top_tags(ai_id, 10)
    top_ten_categories = get_n_top_categories(ai_id, 10)
    top_five_channels = get_n_top_channels(ai_id, 5)
    top_ten_locations = get_n_top_locations(ai_id, 10)
    
    category_names = [Category.objects.get(id = category_id).name for category_id in top_ten_categories]
    block_names = [Location.objects.get(id = location_id).block.name for location_id in top_ten_locations]
    
    channel_names = []
    for channel_id in top_five_channels:
        channel = Channel.objects.get(id = channel_id)
        channel_names.append('%s_%s' % (channel.ai.name, channel.name))
    
    header += [''] + top_ten_tags + [''] + category_names + [''] + channel_names + [''] + block_names
    
    ai_contact_list_map = { 10: [91, 92], 137: [136, 137] }    
    cr_vol_contacts = get_distinct_numbers( ai_contact_list_map[ai_id] )    
    refer_aids = Refer_ai_properties.objects.filter(push_ai_id = ai_id).values('ai_id')
    
    body = []
    for callerid in callerids:
        tags_count = get_item_heard_count_for_tags(ai_id, top_ten_tags, callerid)
        categories_count = get_item_heard_count_for_categories(ai_id, top_ten_categories, callerid)
        channels_count = get_item_heard_count_for_channels(ai_id, top_five_channels, callerid)
        locations_count = get_item_heard_count_for_locations(ai_id, top_ten_locations, callerid)
        avg_keypresses = get_avg_keypresses(ai_id, callerid)        
        
        success_out_calls, first_call_in, last_call_in, success_calls_last_three_months = get_calls_info(ai_id, callerid)
        """
        total_time_spent = 0
        if first_call_in:
            start_datetime = first_call_in
            end_datetime = datetime.combine(datetime.now(), datetime.min.time())
            total_time_spent = get_total_duration( get_calls(ai_id, start_datetime, end_datetime, [callerid]) ) // 60
        """
        pub_arc_contribs, rej_contribs, pub_arc_contribs_last_three_months = get_contrib_count_for_states(ai_id, callerid)        
        referrals = Referral.objects.filter(ai_id__in = refer_aids, referring_contact = callerid).count()                
        items_liked = Bookmark.objects.filter(ai_id = ai_id, callerid = callerid).count()
        items_forwarded = Forwarding_log.objects.filter(ai_id = ai_id, forwarding_cdr__callerid = callerid).count()                
        is_cr_or_vol = str(str(callerid) in cr_vol_contacts)
        location = get_location_info(ai_id, callerid)
        
        body.append([callerid, first_call_in, last_call_in, success_out_calls, pub_arc_contribs, rej_contribs, \
                     success_calls_last_three_months, pub_arc_contribs_last_three_months, referrals, items_liked, \
                     items_forwarded, is_cr_or_vol] + location + [avg_keypresses] + [''] + tags_count + [''] + categories_count \
                     + [''] + channels_count + [''] + locations_count)
    
    data.append(header)
    data += body
    return data
    
        
def get_location_info(ai_id, callerid):
    location_from_contact = get_location_from_contact(callerid)
    location_from_contrib = get_location_from_contrib(ai_id, callerid)
    
    location = None or location_from_contact or location_from_contrib
    
    if location:
        location_source = 'Contact' if location_from_contact else 'Contribution'
        return [location.village, location.district.name, location.block.name, location.state.name, location_source]
    
    return ['Not Available'] * 5
            
                
def get_location_from_contact(callerid):
    contact = Contact.objects.filter(number = callerid)
    
    if contact.exists() and contact[0].location_fk_id:
        return Location.objects.get(id = contact[0].location_fk_id)
    
    return None


def get_location_from_contrib(ai_id, callerid):
    news = News.objects.filter(ai_id = ai_id, callerid = callerid).filter(location__isnull = False)
    
    if news.exists():
        return Location.objects.get(id = news[0].location_id)
    
    return None


def get_contrib_count_for_states(ai_id, callerid):
    contribs = News.objects.filter(ai_id = ai_id, callerid = callerid)
    
    pub_arc_contribs = contribs.filter(state__in = [News_state.PUB, News_state.ARC])
    
    end_date = date.today()
    start_date = end_date + relativedelta(months = -3)
    pub_arc_contribs_last_three_months = pub_arc_contribs.filter(time__range = (start_date, end_date)).count()
    
    rej_contribs = contribs.filter(state = News_state.REJ).count()
    
    return pub_arc_contribs.count(), rej_contribs, pub_arc_contribs_last_three_months


def get_calls_info(ai_id, callerid):
    calls = Cdr.objects.filter(ai_id = ai_id, callerid = callerid)
    success_out_calls = calls.filter(is_incoming = False, answered_time__isnull = False)
    
    end_date = date.today()
    start_date = end_date + relativedelta(months = -3)
    success_calls_last_three_months = success_out_calls.filter(start_time__range = (start_date, end_date)).count()
        
    in_calls = calls.filter(is_incoming = True)
    
    ( first_call_in, last_call_in ) = ( in_calls.order_by('id')[0].start_time, in_calls.order_by('-id')[0].start_time ) \
                                        if in_calls.exists() else (None, None)
                                        
    return success_out_calls.count(), first_call_in, last_call_in, success_calls_last_three_months


def get_n_top_tags(ai_id, n):
    tags = News.objects.filter(ai_id = ai_id, state__in = news_states, tags__isnull = False).exclude(tags = '').values_list('tags', flat = True)
    tag_count_map = get_tags_dict(tags)
    
    sorted_tags = sorted(tag_count_map.iteritems(), key = operator.itemgetter(1), reverse = True)[:n]
    return [tag[0] for tag in sorted_tags]
    

def get_n_top_categories(ai_id, n):
    category_ids = News.objects.filter(ai_id = ai_id, state__in = news_states, category__isnull = False).exclude(category__name = 'Miscellaneous').values_list('category_id', flat = True)
    category_count_map = {}
    for category_id in category_ids:
        category_count_map[category_id] = category_count_map.get(category_id, 0) + 1
        
    sorted_categories = sorted(category_count_map.iteritems(), key = operator.itemgetter(1), reverse = True)[:n]
    return [category[0] for category in sorted_categories]    


def get_n_top_channels(ai_id, n):
    channel_ids = News.objects.filter(ai_id = ai_id, state__in = news_states).values_list('channel_id', flat = True)
    channel_count_map = {}
    for channel_id in channel_ids:
        channel_count_map[channel_id] = channel_count_map.get(channel_id, 0) + 1
        
    sorted_channels = sorted(channel_count_map.iteritems(), key = operator.itemgetter(1), reverse = True)[:n]
    return [channel[0] for channel in sorted_channels]


def get_n_top_locations(ai_id, n):
    location_ids = News.objects.filter(ai_id = ai_id, state__in = news_states, location__isnull = False).exclude(location__block__name = 'Not Known').values_list('location_id', flat = True)
    location_count_map = {}
    for location_id in location_ids:
        location_count_map[location_id] = location_count_map.get(location_id, 0) + 1
        
    sorted_locations = sorted(location_count_map.iteritems(), key = operator.itemgetter(1), reverse = True)[:n]
    return [location[0] for location in sorted_locations]    


def get_item_heard_count_for_tags(ai_id, tags, callerid):
    tags_heard_count = []
    for tag in tags:
        tags_heard_count.append(Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, item__tags__contains = tag).count())
        
    return tags_heard_count


def get_item_heard_count_for_categories(ai_id, categories, callerid):
    categories_heard_count = []
    for category_id in categories:
        categories_heard_count.append(Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, item__category_id = category_id).count())
        
    return categories_heard_count


def get_item_heard_count_for_channels(ai_id, channels, callerid):
    channels_heard_count = []
    for channel_id in channels:
        channels_heard_count.append(Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, item__channel_id = channel_id).count())
        
    return channels_heard_count


def get_item_heard_count_for_locations(ai_id, locations, callerid):
    locations_heard_count = []
    for location_id in locations:
        locations_heard_count.append(Item_heard_stats.objects.filter(ai_id = ai_id, cdr__callerid = callerid, item__location_id = location_id).count())
        
    return locations_heard_count


def get_avg_keypresses(ai_id, callerid):
    total_dtmf_count = 0
    total_duration = 0
    trans_event_start_date = date(2013, 8, 23)

    cdrs = Cdr.objects.filter(ai_id = ai_id, callerid = callerid, is_incoming = False, answered_time__isnull = False, \
                              end_time__isnull = False, start_time__gte = trans_event_start_date)
    for cdr in cdrs:
        trans_events = Transition_event.objects.filter(cdr_id = cdr.id)
        if trans_events.exists():
            total_dtmf_count += trans_events.filter(event__icontains = 'dtmf_').count()
            total_duration += get_total_seconds(cdr.end_time - cdr.start_time)
        
    avg_keypresses_per_min = get_ratio(total_dtmf_count, total_duration / 60)
    return avg_keypresses_per_min

def create_html_content(stats):
    html = ''
    for ai, data in stats.items():
        try:
            template = Custom_stats_settings.objects.get(ai = ai).template_uri
            html += render_to_string(template, { 'data': data }) + LINE_BREAK
        except Exception, e:
            logger.exception("Exception in create_html_content %s" % str(e))
            pass

    return html


def get_stats(ais, start_datetime, end_datetime, stat_type = None, project = None):
    stats = {}
    reports = []
    if project is None:
        for ai in ais:
            try:
                settings = Custom_stats_settings.objects.get(ai = ai)
                stats_generator_func = getattr(load_vapp_module('customization', 'stats'), settings.stats_generator_func)
                (stats[ai], attachments) = stats_generator_func(ai, start_datetime, end_datetime, stat_type)
                reports.extend(attachments)
            except Exception, e:
                logger.exception("Exception in customization - get_stats %s" % str(e))
                pass
    else:
        if is_start_of_month(end_datetime.date()):
            end_datetime = datetime.combine(end_datetime.date(), datetime.min.time())
            start_datetime = end_datetime - relativedelta(months = 1)
            project_start_date = datetime.combine(project.start_date, datetime.min.time())
            output_file1 = '/tmp/' + str(project.id) + '_cumulative_stats_monthly_for_' + start_datetime.strftime("%B-%Y") + '.xlsx'
            reports.extend( [ generate_cumulative_survey_report(start_datetime, end_datetime, project, output_file1) ] )
            if project_start_date < start_datetime:
                output_file2 = '/tmp/' + str(project.id) + '_cumulative_stats_from_start_up_to_' + start_datetime.strftime("%B-%Y") + '.xlsx'
                reports.extend( [ generate_cumulative_survey_report(project_start_date, end_datetime, project, output_file2) ] )
    return (stats, reports)


def generate_cumulative_survey_report(start_date, end_date, project, output_file, active_surveys_only = True, survey_ids = []):
    if survey_ids:
        surveys = Survey.objects.filter(id__in = survey_ids)
        survey_ais = [survey.ai for survey in surveys]
    else:
        survey_ais = []
        for project_ai in project.ais.all():
            pkg_name = str(project_ai.app.pkg_name)
            if pkg_name.lower() == 'survey':
                survey_ais.append(project_ai)
        
        surveys = []
        if active_surveys_only:
            surveys.extend(Survey.objects.filter(ai__in = survey_ais, is_active = True))
        else:
            surveys.extend(Survey.objects.filter(ai__in = survey_ais).order_by('-is_active'))
    
    detailed_stats = get_cumulative_detailed_stats( start_date, end_date, project, survey_ais, surveys, active_surveys_only )
    summary_stats = get_cumulative_summary_stats(start_date, end_date, surveys )
    output = [ { 'name': 'Detailed Stats', 'data': detailed_stats }, { 'name': 'Summary Stats', 'data': summary_stats } ]
    
    for survey in surveys:
        survey_data = get_cumulative_detailed_stats( start_date, end_date, project, [ survey.ai ], [ survey ], active_surveys_only, app_selector_info = False )
        output.append( { 'name': str(survey.name)+' Detailed Stats' , 'data': survey_data } )

    generate_workbook( output, output_file = output_file )
    return output_file


def get_cumulative_detailed_stats(start_date, end_date, project, ais, surveys, active_surveys_only, app_selector_info = True):
    cdrs = set()

    for survey in surveys:
        survey_cdr_ids = Survey_cdr.objects.filter(survey = survey, time__range = (start_date, end_date)).values_list('cdr', flat = True)
        for survey_cdr_id in survey_cdr_ids:
            cdr_object = Cdr.objects.filter(id = survey_cdr_id)
            if cdr_object.count() > 0:
                cdrs.add(cdr_object[0])
            else:
                continue

    location_contact_dict_list = get_location_contact_dict_list( project )

    common_survey_ai = Swasti_common_survey.objects.values_list('ai', flat = True)[0]
    common_survey = Survey.objects.get(ai = common_survey_ai, is_active = True)
    
    as_names_choices = []
    if app_selector_info:
        as_names_choices = get_app_selector_ais_names_choices(cdrs)
    cumulative_header_rows = get_cumulative_report_header(ais, surveys, common_survey, active_surveys_only, [], as_names_choices)
    return get_cumulative_results_data( cumulative_header_rows, location_contact_dict_list, surveys, common_survey, [], cdrs, as_names_choices )
    

def get_cumulative_report_header(ais, surveys, common_survey, active_surveys_only, custom_data_fields = [], as_names_choices = []):
    ai_survey_ques_dict_list = get_cumulative_surveys_header(ais, active_surveys_only, custom_data_fields)
    header_rows = []
    header_row1 = ['Call Info', '', '', '', 'Profile Info']
    header_row2 = ['CDR ID', 'Phone number', 'Start Date', 'End Date', 'Location']
    header_row3 = ['', '', '', '', '']

    if as_names_choices:
        header_row1.append( 'App Selector' )
        no_of_app_selectors = len(as_names_choices)
        if no_of_app_selectors > 1:
            app_selector_header_extender = []
            for index in range(0, no_of_app_selectors-1):
                app_selector_header_extender.append('')
            header_row1.extend(app_selector_header_extender)

        app_selector_names_header_extender = []
        for as_name_choice in as_names_choices:
            app_selector_names_header_extender.append(str(as_name_choice['name']))
        header_row2.extend(app_selector_names_header_extender)

        for as_name_choice in as_names_choices:
            choices_list = as_name_choice['choice']
            if not choices_list:
                choice_row = "No Choices"
            else: 
                choices_list.sort() 
                choice_row = "Choices: " + str(choices_list)[1:-1]
            header_row3.append(choice_row)
    else:
        common_survey_header = []
        common_survey_questions = get_questions( common_survey.id )
        for common_survey_question in common_survey_questions:
            common_survey_header.append(str(common_survey_question.text))
            header_row2.append('')
            header_row3.append('')
            if isinstance(common_survey_question, Question_ff):
                common_survey_header.append('Response Duration')
                header_row2.append('')
                header_row3.append('')
        header_row1.extend( common_survey_header )

    for ai_survey_ques_dict in ai_survey_ques_dict_list:
        header_row1.append(str(ai_survey_ques_dict['ai']))
        survey_ques_dict_list = ai_survey_ques_dict['survey_ques']
        survey_ques_count_for_ai = 0
        for survey_ques_dict in survey_ques_dict_list:
            survey_ques_count_for_ai += len(survey_ques_dict['ques'])
        for index in range(0, survey_ques_count_for_ai-1):
            header_row1.append('')

    for ai_survey_ques_dict in ai_survey_ques_dict_list:
        survey_ques_dict_list = ai_survey_ques_dict['survey_ques']
        for survey_ques_dict in survey_ques_dict_list:
            header_row2.append(str(survey_ques_dict['survey']))
            ques_list = len(survey_ques_dict['ques'])
            for index in range(0, ques_list-1):
                header_row2.append('')

    for ai_survey_ques_dict in ai_survey_ques_dict_list:
        survey_ques_dict_list = ai_survey_ques_dict['survey_ques']
        for survey_ques_dict in survey_ques_dict_list:
            header_row3.extend(survey_ques_dict['ques'])
    
    header_rows.append(header_row1)
    header_rows.append(header_row2)
    header_rows.append(header_row3)
    
    return header_rows


def get_cumulative_surveys_header(ais, active_surveys_only, custom_data_fields):
    ai_survey_ques_dict_list = []
    for ai in ais:
        ai_survey_ques_dict = {}
        ai_survey_ques_dict['ai'] = ai.name
        survey_ques_dict_list = []
        surveys_for_ai = []
        if active_surveys_only:
            surveys_for_ai.append(Survey.objects.get(ai = ai, is_active = True))
        else:
            surveys_for_ai.extend(Survey.objects.filter(ai = ai).order_by('-is_active'))
        for survey_for_ai in surveys_for_ai:
            survey_ques_dict = {}
            survey_ques_dict['survey'] = survey_for_ai.name
            survey_ques_dict['ques'] = get_default_report_header(survey_for_ai, custom_data_fields)
            survey_ques_dict_list.append(survey_ques_dict)
        ai_survey_ques_dict['survey_ques'] = survey_ques_dict_list
        ai_survey_ques_dict_list.append(ai_survey_ques_dict)
    return ai_survey_ques_dict_list


def get_location_contact_dict_list( project ):
    contact_location_list = get_caller_profile_for_project( project = project )
    location_contact_dict_list = []
    for index in range(1, len(contact_location_list)):
        contact_location_dict = {}
        contact_location_dict['location'] = str(contact_location_list[index][0])
        contact_location_dict['contact'] = get_formatted_number( str( int(float(contact_location_list[index][1])) ) )
        location_contact_dict_list.append(contact_location_dict)
    return location_contact_dict_list


def get_cumulative_results_data( header, location_contact_dict_list, surveys, common_survey, custom_data_fields, cdrs, as_names_choices, wb_format = True ):
    data = header
    for cdr in cdrs:
        data.append(create_cumulative_result_row( surveys, common_survey, location_contact_dict_list, custom_data_fields, cdr, as_names_choices, wb_format ))
    return data


def create_cumulative_result_row( surveys, common_survey, location_contact_dict_list, custom_data_fields, cdr, as_names_choices, wb_format = True ):
    if wb_format:
        row = []
        row.append(cdr.id)
        callerid = str(cdr.callerid)
        row.append(callerid)
        row.append( str(cdr.answered_time if cdr.answered_time else cdr.start_time) )
        row.append( str(cdr.end_time) )

        location = "N/A"
        for location_contact_dict in location_contact_dict_list:
            if location_contact_dict['contact'] == callerid:
                location = location_contact_dict['location']
                break
        row.append(location)

        if as_names_choices:
            selectionlog_objs = SelectionLog.objects.filter(cdr = cdr)
            for as_name_choice in as_names_choices:
                name_matches = 0
                as_selected_choices = []
                for selectionlog_obj in selectionlog_objs:
                    if selectionlog_obj.selector_ai.name == as_name_choice['name']:
                        name_matches = 1
                        if selectionlog_obj.choice:
                            as_selected_choices.append( int(selectionlog_obj.choice) )
                if name_matches == 0:
                    row.append("N/A")
                else:
                    if not as_selected_choices:
                        row.append("-")
                    else:
                        row.append( str(as_selected_choices)[1:-1] )
        else:
            common_survey_questions = get_questions(common_survey.id)
            records = Record.objects.filter( survey = common_survey, cdr = cdr ).order_by( 'order' )
            labels = get_mc_labels( common_survey_questions )
            row.extend( get_records_row(common_survey, cdr, custom_data_fields, records, labels, common_survey_questions))

        for survey in surveys:
            questions = get_questions(survey.id)
            if has_answered_survey(survey.id, cdr):
                records = Record.objects.filter( survey = survey, cdr = cdr ).order_by( 'order' )
                labels = get_mc_labels( questions )
                row.extend( get_custom_data(survey, custom_data_fields, cdr, records, labels) )
                row.extend( get_records_row(survey, cdr, custom_data_fields, records, labels, questions))
            else:
                for question in questions:
                    if isinstance(question, Question_ff):
                        row.append("N/A")
                    row.append("N/A")
        return row
    else:
        return { cdr : list(records)  }


def get_cumulative_summary_stats(start_date, end_date, surveys):
    data = []
    for survey in surveys:
        cdrs = set([ sc.cdr for sc in Survey_cdr.objects.filter(survey = survey, time__range = (start_date, end_date)) ])
        total_calls = len(list(cdrs))
        data.append( [str(survey.name), 'Total number of calls', total_calls] )
        callers_with_no_ques_ans = get_callerscount_no_ques_answered(cdrs)
        data.append( ['', '# of people who did not answer any questions', callers_with_no_ques_ans] )
        data.append( [''] )
        data.append( ['', 'Question', 'No Answer', 'Total Answered', 'Answer Summaries'] )
        questions = get_questions(survey.id)
        records = Record.objects.filter(cdr__in = list(cdrs))
        record_ids = tuple( map(int, records.values_list('id', flat = True)) )
        for q in questions:
            records_for_ques = records.filter(question_id = q.id)
            if isinstance(q, Question_mc):
                labels = dict(enumerate(q.choices.split(',')))
                choices, counts = get_mc_results(q.id, record_ids)
                ques_attribs = ['', 'Choices', '', ''] + [str(labels[c - 1]) for c in choices]
                calls_with_ques_ans = records_for_ques.filter(mc_choice__isnull = False).count()
            elif isinstance(q, Question_qu):
                bin_size = get_bin_size(q.id, 10, record_ids)
                bins, counts = get_qu_results(q.id, bin_size, record_ids)
                ques_attribs = ['', 'Range', '', ''] + [str(b * bin_size) + '-' + str(b*bin_size + bin_size - 1) for b in bins if b is not None]
                calls_with_ques_ans = records_for_ques.filter(qu_value__isnull = False).count()
            elif isinstance(q, Question_ff):
                ques_attribs = ['', 'Query']
                calls_with_ques_ans = records_for_ques.filter(ff_audio__isnull = False).count()
                counts = []
            calls_with_ques_not_ans = total_calls - calls_with_ques_ans
            data.append(ques_attribs)
            data.append(['', str(q.text), calls_with_ques_not_ans, calls_with_ques_ans] + counts)
            data.append([''])
        data.append([''])
    return data


def get_quikr_stats(ais_dict, start_date, end_date):
    data = []

    survey = Survey.objects.get(ai = ais_dict['survey'][0], is_active = True)
    reg_ai = ais_dict['registration'][0]
    mnews_ai = ais_dict['mnews'][0]
    items = News.objects.filter(ai = mnews_ai, state = News_state.PUB).order_by('id')
    data.extend(get_quikr_header(survey, items))
    
    contact_list = Registration.objects.get(ai = reg_ai).contact_list
    callerids = []
    for contact in contact_list.contacts.all():
        callerids.append(contact.number)
    cdrs = Cdr.objects.filter( ai = reg_ai, callerid__in = callerids, answered_time__range = (start_date, end_date) )
    questions = get_questions( survey.id )
    for cdr in cdrs:
        record_row = []
        record_row.append( cdr.id )
        record_row.append( cdr.callerid )
        record_row.append( str(cdr.answered_time if cdr.answered_time else cdr.start_time) )
        record_row.append( str(cdr.end_time) )
        record_row.extend( get_quikr_survey_record_row(survey, cdr, questions) )
        record_row.extend( get_quikr_liked_items_record_row(cdr, items) )
        data.append(record_row)
        
    return data


def get_quikr_header(survey, items):
    header = []
    header_row2 = ['Call Data Record ID', 'Phone number', 'Start Date', 'End Date']
    questions = get_questions( survey.id )
    reg_surveys_cells_count = len(header_row2) + len(questions)
    cell_id = int ( math.floor ( reg_surveys_cells_count / 2 ) )

    header_row1 = []
    for i in range(0, cell_id):
        header_row1.append('')
    header_row1.append('Registration Surveys Data')
    for i in range(cell_id+1, reg_surveys_cells_count):
        header_row1.append('')
    header_row1.append('Items Liked')
    header.append(header_row1)

    for question in questions:
        header_row2.append(str(question.text))
    for item in items:
        header_row2.append(str(item.title))
    header.append(header_row2)
    
    return header


def get_quikr_survey_record_row(survey, cdr, questions):
    survey_record_row = []
    survey_cdrs = Survey_cdr.objects.filter(survey = survey, cdr__callerid = cdr.callerid)
    records = Record.objects.filter( survey = survey, cdr__callerid = survey_cdrs[0].cdr.callerid ).order_by( 'order' )
    labels = get_mc_labels( questions )
    for question in questions:
        recs = records.filter(question_id = question.id)
        record_present = recs.exists()
        if record_present:
            if isinstance(question, Question_ff) and recs[0].ff_text:
                survey_record_row.append( str( recs[0].ff_text ) )
            else:
                survey_record_row.append( str( get_answer_label(recs[0], labels) ) )
        else:
            survey_record_row.append('-')
    return survey_record_row


def get_quikr_liked_items_record_row(cdr, items):
    liked_item_record_row = []
    bookmark_ids = Bookmark_log.objects.filter( cdr = cdr, bookmarked = True, source = 'ITB' ).values_list( 'bookmark', flat = True )
    for item in items:
        item_has_been_liked = 0
        for bookmark_id in bookmark_ids:
            liked_item = Bookmark.objects.get(id = bookmark_id).news
            if liked_item == item:
                item_has_been_liked = 1
                liked_item_record_row.append('Y')
                break
        if item_has_been_liked == 0:
            liked_item_record_row.append('N')
    return liked_item_record_row


def get_praekelt_conversation_data(ai_id, current_date, current_date_start, yesterday_date_start, current_week_start, previous_week_start, current_month_start, previous_month_start, current_quarter_start_for_text_widget, previous_quarter_start_for_text_widget, current_quarter_start_for_line_graph, previous_quarter_start_for_line_graph, previous_year_start):
    data = {}

    call_qs_for_ai = Call.objects.filter(ai_id = ai_id)

    data['call_qs_yesterday_count'], data['call_qs_today_count'], data['avg_calls_yesterday'], data['avg_calls_today'], data['avg_call_dur_yesterday'], data['avg_call_dur_today'] = get_praekelt_avg_calls_count_dur( call_qs_for_ai, yesterday_date_start, current_date_start, current_date )
    data['call_qs_previous_week_count'], data['call_qs_current_week_count'], data['avg_calls_previous_week'], data['avg_calls_current_week'], data['avg_call_dur_previous_week'], data['avg_call_dur_current_week'] = get_praekelt_avg_calls_count_dur( call_qs_for_ai, previous_week_start, current_week_start, current_date )
    data['call_qs_previous_month_count'], data['call_qs_current_month_count'], data['avg_calls_previous_month'], data['avg_calls_current_month'], data['avg_call_dur_previous_month'], data['avg_call_dur_current_month'] = get_praekelt_avg_calls_count_dur( call_qs_for_ai, previous_month_start, current_month_start, current_date )
    data['call_qs_previous_quarter_count'], data['call_qs_current_quarter_count'], data['avg_calls_previous_quarter'], data['avg_calls_current_quarter'], data['avg_call_dur_previous_quarter'], data['avg_call_dur_current_quarter'] = get_praekelt_avg_calls_count_dur( call_qs_for_ai, previous_quarter_start_for_text_widget, current_quarter_start_for_text_widget, current_date )
    data['total_call_duration'] = get_conversation_duration(call_qs_for_ai) / 1000

    calls_per_month_distribution = []
    month_start = previous_year_start
    for each in range(12):
        month_end = month_start + relativedelta(months = +each)
        calls = call_qs_for_ai.filter(media__time__range = (month_start, month_end)).count()
        calls_per_month_distribution.append([each, calls])
        month_start = month_end
    data[ 'calls_per_month_distribution' ] = calls_per_month_distribution

    calls_per_week_distribution = []
    avg_call_dur_per_week_distribution = []
    calls_percent_for_morning = []
    calls_percent_for_afternoon = []
    calls_percent_for_evening = []
    week_count = 0
    for week_start in daterange(previous_quarter_start_for_line_graph, current_quarter_start_for_line_graph, timedelta(days = 7)):
        week_end = week_start + timedelta(days = 7)
        calls_for_week = call_qs_for_ai.filter(media__time__range = (week_start, week_end))
        calls_for_week_count = calls_for_week.count()
        calls_per_week_distribution.append( [week_count, calls_for_week_count] )
        if calls_for_week_count > 0:
            avg_call_dur_for_week = (get_conversation_duration(calls_for_week) / 1000) / calls_for_week_count
            calls_for_early_morning, calls_for_morning, calls_for_afternoon, calls_for_evening = get_call_percent_for_period( week_start, week_end, calls_for_week, calls_for_week_count )
            calls_percent_for_morning.append( [week_count, calls_for_morning] )
            calls_percent_for_afternoon.append( [week_count, calls_for_afternoon] )
            calls_percent_for_evening.append( [week_count, calls_for_evening] )
        else:
            avg_call_dur_for_week = 0.0
            calls_percent_for_morning.append( [week_count, 0.0] )
            calls_percent_for_afternoon.append( [week_count, 0.0] )
            calls_percent_for_evening.append( [week_count, 0.0] )
        avg_call_dur_per_week_distribution.append([week_count, avg_call_dur_for_week])
        week_count += 1
    data[ 'calls_per_week_distribution' ] = calls_per_week_distribution
    data[ 'avg_call_dur_per_week_distribution' ] = avg_call_dur_per_week_distribution
    data[ 'calls_percent_for_morning' ] = calls_percent_for_morning
    data[ 'calls_percent_for_afternoon' ] = calls_percent_for_afternoon
    data[ 'calls_percent_for_evening' ] = calls_percent_for_evening
    return data


def get_praekelt_avg_calls_count_dur(call_qs, previous_period_date_start, current_period_date_start, current_date):
    call_qs_previous_period_count, avg_calls_previous_period, avg_call_dur_previous_period = get_praekelt_avg_calls_count_dur_for_period(call_qs, previous_period_date_start, current_period_date_start)
    call_qs_current_period_count, avg_calls_current_period, avg_call_dur_current_period = get_praekelt_avg_calls_count_dur_for_period(call_qs, current_period_date_start, current_date)
    return call_qs_previous_period_count, call_qs_current_period_count, avg_calls_previous_period, avg_calls_current_period, avg_call_dur_previous_period, avg_call_dur_current_period


def get_praekelt_avg_calls_count_dur_for_period(call_qs, period_date_start, period_date_end):
    call_qs_period = call_qs.filter(media__time__range = (period_date_start, period_date_end))
    call_qs_period_count = call_qs_period.count()
    if call_qs_period_count > 0:
        user_pairs_period = len(call_qs_period.values('handler', 'beneficiary').distinct())
        avg_calls_period = call_qs_period_count / user_pairs_period
        avg_call_dur_period = (get_conversation_duration(call_qs_period) / 1000) / call_qs_period_count
    else:
        avg_calls_period = 0.0
        avg_call_dur_period = 0.0
    return call_qs_period_count, avg_calls_period, avg_call_dur_period


def get_call_percent_for_period( start, end, calls, calls_count ):
    ranges_dict = get_morning_afternoon_evening_ranges(start, end)
    calls_for_early_morning = calls_for_morning = calls_for_afternoon = calls_for_evening = 0
    for morning_range in ranges_dict[ 'early_morning' ]:
        calls_for_early_morning += calls.filter( media__time__range = (morning_range['start'], morning_range['end']) ).count()
    for morning_range in ranges_dict[ 'morning' ]:
        calls_for_morning += calls.filter( media__time__range = (morning_range['start'], morning_range['end']) ).count()
    for afternoon_range in ranges_dict[ 'afternoon' ]:
        calls_for_afternoon += calls.filter( media__time__range = (afternoon_range['start'], afternoon_range['end']) ).count()
    for evening_range in ranges_dict[ 'evening' ]:
        calls_for_evening += calls.filter( media__time__range = (evening_range['start'], evening_range['end']) ).count()
    call_percent_early_morning = round( ( calls_for_early_morning * 100 / calls_count ), 2)
    call_percent_morning = round( ( calls_for_morning * 100 / calls_count ), 2)
    call_percent_afternoon = round( ( calls_for_afternoon * 100 / calls_count ), 2)
    call_percent_evening = round( ( calls_for_evening * 100 / calls_count ), 2)
    return call_percent_early_morning, call_percent_morning, call_percent_afternoon, call_percent_evening


def get_morning_afternoon_evening_ranges(start, end):
    ranges_dict = {'early_morning' : [], 'morning' : [], 'afternoon' : [], 'evening' : []}
    while start < end:
        morning_start = start.replace(hour = 6)
        morning_end = start.replace(hour = 12)
        evening_start =start.replace(hour = 18)
        ranges_dict[ 'early_morning' ].append( {'start' : start, 'end' : morning_start} )
        ranges_dict[ 'morning' ].append( {'start' : morning_start, 'end' : morning_end} )
        ranges_dict[ 'afternoon' ].append( {'start' : morning_end, 'end' : evening_start} )
        start = start + timedelta(days = 1)
        ranges_dict[ 'evening' ].append( {'start' : evening_start, 'end' : start} )
    return ranges_dict


def get_praekelt_survey_data(survey_ai_ids, current_date, current_week_start, current_month_start, current_quarter_start_for_line_graph, previous_quarter_start_for_line_graph):
    data = {}
    survey_ai_ids = ast.literal_eval( survey_ai_ids )
    survey_ids = Survey.objects.filter( ai_id__in = survey_ai_ids, is_active = True ).values_list('id', flat = True)
    survey_cdrs = Survey_cdr.objects.filter( survey_id__in = survey_ids )
    data[ 'total_initiated_and_picked' ] = survey_cdrs.filter( cdr__answered_time__isnull = False ).count()
    data[ 'total_initiated_but_not_picked' ] = survey_cdrs.filter( cdr__answered_time__isnull = True ).count()
    data[ 'initiated_and_picked_percent_weekly' ], data[ 'initiated_but_not_picked_percent_weekly' ] = get_praekelt_survey_data_for_period( survey_cdrs, current_week_start, current_date )
    data[ 'initiated_and_picked_percent_monthly' ], data[ 'initiated_but_not_picked_percent_monthly' ] = get_praekelt_survey_data_for_period( survey_cdrs, current_month_start, current_date )
    
    ivr_surveys_ignored_per_week_distribution = []
    ivr_surveys_answered_per_week_distribution = []
    week_count = 0
    for week_start in daterange(previous_quarter_start_for_line_graph, current_quarter_start_for_line_graph, timedelta(days = 7)):
        week_end = week_start + timedelta(days = 7)
        survey_cdrs_for_week = survey_cdrs.filter( time__range = (week_start, week_end) )
        survey_cdrs_count_for_week = survey_cdrs_for_week.count()
        surveys_answered_for_week = survey_cdrs_for_week.filter( cdr__answered_time__isnull = False ).count()
        surveys_ignored_for_week = survey_cdrs_for_week.filter( cdr__answered_time__isnull = True ).count()
        if survey_cdrs_count_for_week > 0:
            surveys_answered_for_week_percent = surveys_answered_for_week * 100 / survey_cdrs_count_for_week
            surveys_ignored_for_week_percent = surveys_ignored_for_week * 100 / survey_cdrs_count_for_week
        else:
            surveys_answered_for_week_percent = 0.0
            surveys_ignored_for_week_percent = 0.0
        ivr_surveys_ignored_per_week_distribution.append( [week_count, round(surveys_ignored_for_week_percent, 2)] )
        ivr_surveys_answered_per_week_distribution.append( [week_count, round(surveys_answered_for_week_percent, 2)] )
        week_count += 1
    data[ 'ivr_surveys_ignored_per_week_distribution' ] = ivr_surveys_ignored_per_week_distribution
    data[ 'ivr_surveys_answered_per_week_distribution' ] = ivr_surveys_answered_per_week_distribution
    return data


def get_praekelt_survey_data_for_period(survey_cdrs, start, end):
    survey_cdrs_for_period = survey_cdrs.filter( cdr__start_time__range = (start, end) )
    survey_cdrs_for_period_count = survey_cdrs_for_period.count()
    if survey_cdrs_for_period_count > 0:
        initiated_and_picked_percent_for_period = survey_cdrs_for_period.filter( cdr__answered_time__isnull = False ).count() / survey_cdrs_for_period_count * 100
        initiated_but_not_picked_percent_for_period = survey_cdrs_for_period.filter( cdr__answered_time__isnull = True ).count() / survey_cdrs_for_period_count * 100
    else:
        initiated_and_picked_percent_for_period = 0.0
        initiated_but_not_picked_percent_for_period = 0.0
    return initiated_and_picked_percent_for_period, initiated_but_not_picked_percent_for_period


def get_praekelt_downloadable_data( project, start_date, end_date ):
    data = []

    survey_ais = []
    for project_ai in project.ais.all():
        pkg_name = str(project_ai.app.pkg_name).lower()
        if pkg_name == 'survey':
            survey_ais.append(project_ai)
        if pkg_name == 'helpline':
            helpline_ai = project_ai

    calls_for_this_period = Call.objects.filter(ai = helpline_ai, media__time__range = (start_date, end_date))
    total_number_of_calls = calls_for_this_period.count()
    data.append( ['Total Number of Calls', total_number_of_calls] )
    total_call_duration = get_conversation_duration( calls_for_this_period ) / 1000
    data.append( ['Total Call Duration(In Second)', round(total_call_duration, 2)] )
    average_call_duration = 0 if total_number_of_calls == 0 else ( total_call_duration / total_number_of_calls )
    data.append( ['Average Call Duration(In Second)', round(average_call_duration, 2)] )
    call_percent_early_morning = call_percent_morning = call_percent_afternoon = call_percent_evening = 0.0
    if total_number_of_calls > 0:
        call_percent_early_morning, calls_for_morning, call_percent_morning, call_percent_evening = get_call_percent_for_period( start_date, end_date, calls_for_this_period, total_number_of_calls )
    data.append( ['Percetage of calls taken place in early morning', call_percent_early_morning] )
    data.append( ['Percetage of calls taken place in morning', call_percent_morning] )
    data.append( ['Percetage of calls taken place in afternoon', call_percent_afternoon] )
    data.append( ['Percetage of calls taken place in evening', call_percent_evening] )
    survey_ids = Survey.objects.filter( ai__in = survey_ais, is_active = True ).values_list('id', flat = True)
    survey_cdrs = Survey_cdr.objects.filter( survey_id__in = survey_ids, cdr__start_time__range = (start_date, end_date) )
    survey_cdrs_count = survey_cdrs.count()
    total_ivr_surveys_completed = survey_cdrs.filter( cdr__answered_time__isnull = False ).count()
    total_ivr_surveys_ignored = survey_cdrs.filter( cdr__answered_time__isnull = True ).count()
    ivr_surveys_completed_percent = ivr_surveys_ignored_percent = 0.0
    if survey_cdrs_count > 0:
        ivr_surveys_completed_percent = round( ( total_ivr_surveys_completed * 100 / survey_cdrs_count ), 2 )
        ivr_surveys_ignored_percent = round( ( total_ivr_surveys_ignored * 100 / survey_cdrs_count ), 2 )
    data.append( ['Total number of IVR surveys completed', total_ivr_surveys_completed] )
    data.append( ['Percentage of IVR surveys completed', ivr_surveys_completed_percent] )
    data.append( ['Total number of IVR surveys ignored', total_ivr_surveys_ignored] )
    data.append( ['Percentage of IVR surveys ignored', ivr_surveys_ignored_percent] )
    mt_sms = SMS_message.objects.filter(ai = helpline_ai, sent_time__range = (start_date, end_date), sent_success = True).count()
    mo_sms = Report_log.objects.filter(cred_id = 12, error_code = http.HttpCreated.status_code, time__range = (start_date, end_date)).count()
    data.append( ['Total number of messages sent to a number (i.e from a mentor)', mt_sms] )
    data.append( ['Total number of messages received from a number (i.e from a mentee)', mo_sms] )
    return data


def get_caller_profile_for_ai_mshakti():
    profile_data = {}
    profile_data['fields'] = ['Name', 'Mobile Number', 'Country', 'State', 'District', 'Block', 'Panchayat', 'Village']
    contacts = Contact_list.objects.get(id=384).contacts.all()
    values = {}
    for contact in contacts:
        number = str(contact.number)
        name = str(contact.name)
        location = contact.location_fk
        country, state, district, block, panchayat, village = ['']*6
        if location:
            if location.country:
                country = str(location.country.name)
            if location.state:
                state = str(location.state.name)
            if location.district:
                district = str(location.district.name)
            if location.block:
                block = str(location.block.name)
            if location.panchayat:
                panchayat = str(location.panchayat.name)
            if location.village:
                village = str(location.village)
        values[number] = [name, number, country, state, district, block, panchayat, village]
    profile_data['values'] = values
    return profile_data