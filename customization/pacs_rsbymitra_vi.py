from vapp.telephony.statemachine import BaseVappController

from mnews.models import News, News_state, Bookmark, Bookmark_log
from customization.pacsmodels import Grievance_log, Grievance_status, Handler_call_log

from datetime import datetime
import stat

from vapp.log import get_request_logger

logger = get_request_logger()


class RSBYMitraController(BaseVappController):

    def __init__(self, ai, sessionData, vi_data = {}):
        super(RSBYMitraController, self).__init__(ai, sessionData, vi_data)

    def pre_outgoingstart(self):
        return self.getStartCallParams(self.sessionData.callerid)

    def while_outgoingstart__sm_action_success(self, events, eventData):
        return 'sm_action_success'

    def pre_incomingstart(self):
        return self.getIncomingCallParams()

    def while_incomingstart__sm_action_success(self, events, eventData):
        return 'sm_action_success'

    def pre_welcome(self):
        return self.getPromptParams('pacs_rsbymitra_welcome')

    def is_valid_grievance(self, grievance_id):
        return Bookmark.objects.filter(bookmark_id = grievance_id, ai_id = self.ai.id).exists()

    def pre_inputgrievance(self):
        return self.getPromptPlayAndGetParams(promptName = 'pacs_rsbymitra_enter_grievance', maxDigits = 9, tries = 3)

    def while_inputgrievance__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        self.grievance_id = digits
        if self.is_valid_grievance(self.grievance_id):
            self.grievance = Bookmark.objects.get(bookmark_id = self.grievance_id, ai_id = self.ai.id)
            self.grievance_log, created = Grievance_log.objects.get_or_create(grievance = self.grievance)
            if self.grievance_log.status == Grievance_status.RS:
                return 'pacs_rsbymitra_grievance_resolved'

            return 'pacs_rsbymitra_valid_grievance'

        return 'pacs_rsbymitra_invalid_grievance'


    def pre_instructions(self):
        return self.getPromptParams('pacs_rsbymitra_instructions')

    def pre_invalidgrievance(self):
        return self.getPromptParams('pacs_rsbymitra_invalid_grievance')

    def pre_resolvedgrievance(self):
        return self.getPromptParams('pacs_rsbymitra_resolved_grievance')

    def pre_playgrievance(self):
        self.news = self.grievance.news
        blog = Bookmark_log(bookmark = self.grievance, cdr = self.sessionData.cdrs[0], accessed = True, bookmarked = False, source = 'BIA')
        blog.save()
        return self.getPlaybackParams(self.news.detail.get_full_filename())

    @classmethod
    def saveItem(cls, item):
        item.modified_date = datetime.now()
        item.save()
        parent = News.objects.get(pk = item.parent_id_value)
        parent.comments.add(item)


    def pre_recordcommentprompt(self):
        return self.getPromptParams('pacs_rsbymitra_leave_comment')

    def pre_recordcommentrecord(self):
        parent = self.news
        title = 'Comment, %s' % datetime.now().strftime('%H:%M, %d %b\'%y')
        self.newComment = News(ai_id=self.ai.id, channel=parent.channel,
                               callerid=self.sessionData.callerid,
                               state=News_state.UNM, is_comment=True,
                               source=News.Source.VOICE_INTERFACE, title=title)
        self.newComment.parent_id_value = parent.id
        return self.getEmbeddedRecordingParams(self.ai.record_duration_limit_seconds, self.newComment, News.detail)

    def while_recordcommentrecord__sm_action_success(self, events, eventData):
        RSBYMitraController.saveItem(self.newComment)

    def pre_recordcommentthankyou(self):
        return self.getPromptParams('pacs_rsbymitra_ack_comment')

    def pre_waitprompt(self):
        return self.getPromptParams('pacs_rsbymitra_helpline_please_wait')

    def pre_callhandler(self):
        self.handler = self.grievance.callerid
        return self.getStartCallParams(self.handler)


    def while_callhandler__sm_action_failure(self, events, eventData):
        uuid = eventData.get('Unique_ID')
        if uuid and uuid == self.sessionData.uuids[0]:
            return 'pacs_rsbymitra_helpline_caller_hung_up'
        else:
            return 'pacs_rsbymitra_helpline_handler_hung_up'


    def pre_handlerunavailableprompt(self):
        return self.getPromptParams('pacs_rsbymitra_helpline_handler_unavailable')


    def pre_recordcall(self):
        self.call_log = Handler_call_log(ai = self.ai, handler = self.handler, callerid = self.sessionData.callerid)
        return self.getRecordCallParams(self.call_log, Handler_call_log.media)

    def pre_bridgecalls(self):
        return self.getBridgeCallsParams(self.sessionData.uuids[0], self.sessionData.uuids[1])

    def while_flaggrievance__sm_action_success(self, events, eventData):
        self.grievance_log.status = Grievance_status.FG
        self.grievance_log.save()
        self.news.is_mod_flagged = True
        self.news.save()
        return 'sm_action_success'

    def pre_actionprompt(self):
        return self.getPromptPlayAndGetParams(promptName = 'pacs_rsbymitra_grievance_action', tries = 3)

    def while_actionprompt__sm_action_success(self, events, eventData):
        digits = self.getPlayAndGetDigits(eventData)
        choice_state_map = {
            1: 'pacs_rsbymitra_record_comment',
            2: 'pacs_rsbymitra_call_beneficiary',
            3: 'pacs_rsbymitra_play_grievance',
            4: 'pacs_rsbymitra_flag_grievance',
            5: 'pacs_rsbymitra_enter_grievance'
        }

        return choice_state_map.get(digits, 'pacs_rsbymitra_invalid_choice')
    
    def pre_postactionprompt(self):
        return self.getPromptParams('pacs_rsbymitra_postaction_prompt')

    def pre_thankyou(self):
        return self.getPromptParams('pacs_rsbymitra_thank_you')


RSBYMitraStatesDescriptionMap = [

    {   'name':'outgoingstart',
        'action':'originate',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success'],
            'outgoingstart':['sm_next_originate_url']
        },
    },
    {   'name':'incomingstart',
        'action':'answer',
        'transitions': {
            'stop':['sm_action_failure'],
            'welcome':['sm_action_success']
        },
    },
    {   'name':'welcome',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'inputgrievance':['sm_action_success']
        },
    },
    {   'name':'inputgrievance',
        'action':'play_and_get_digits',
        'transitions': {
            'stop':['sm_action_failure'],
            'instructions':['pacs_rsbymitra_valid_grievance'],
            'invalidgrievance':['pacs_rsbymitra_invalid_grievance'],
            'resolvedgrievance':['pacs_rsbymitra_grievance_resolved'],
            'thankyou':['sm_get_digits_no_digits']
        },
    },
    {   'name':'instructions',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'playgrievance':['sm_action_success']
        },
    },
    {   'name':'invalidgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'inputgrievance':['sm_action_success']
        },
    },
    {   'name':'resolvedgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'inputgrievance':['sm_action_success']
        },
    },
    {   'name':'playgrievance',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordcommentprompt':['pacs_rsbymitra_record_comment'],
            'waitprompt':['pacs_rsbymitra_call_beneficiary'],
            'playgrievance':['pacs_rsbymitra_play_grievance'],
            'flaggrievance':['pacs_rsbymitra_flag_grievance'],
            'inputgrievance':['pacs_rsbymitra_enter_grievance'],
            'actionprompt':['sm_action_success']
        },
        'dtmf': {
            1:'pacs_rsbymitra_record_comment',
            2:'pacs_rsbymitra_call_beneficiary',
            3:'pacs_rsbymitra_play_grievance',
            4:'pacs_rsbymitra_flag_grievance',
            5:'pacs_rsbymitra_enter_grievance'
        },
    },
    {   'name':'recordcommentprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordcommentrecord':['sm_action_success']
        },
    },
    {   'name':'recordcommentrecord',
        'action':'record',
        'transitions': {
            'stop':['sm_action_failure'],
            'recordcommentthankyou':['sm_action_success']
        },
    },
    {   'name': 'recordcommentthankyou',
        'action': 'playback',
        'transitions': {
            'stop':['sm_action_failure'],
            'postactionprompt':['sm_action_success']
        },
    },
    {   'name':'waitprompt',
        'action':'play_and_return',
        'transitions': {
            'stop':['sm_action_failure'],
            'callhandler':['sm_action_success'],
        },
    },
    {   'name':'callhandler',
        'action':'originate',
        'transitions': {
            'handleranswered':['sm_action_success'],
            'stop':['pacs_rsbymitra_helpline_caller_hung_up'],
            'handlerunavailable':['pacs_rsbymitra_helpline_handler_hung_up'],
        },
    },
    {   'name':'handlerunavailable',
        'action':'break_media',
        'transitions': {
            'handlerunavailableprompt':['sm_action_failure', 'sm_action_success']
        },
    },
    {   'name':'handlerunavailableprompt',
        'action':'playback',
        'transitions': {
            'stop':['sm_action_failure', 'sm_action_success']
        },
    },
    {   'name':'handleranswered',
        'action':'break_media',
        'transitions': {
            'recordcall':['sm_action_success', 'sm_action_failure'],
        },
    },
    {   'name':'recordcall',
        'action':'record_call',
        'transitions': {
            'stop':['sm_action_failure'],
            'bridgecalls':['sm_action_success'],
        },
    },
    {   'name':'bridgecalls',
        'action':'bridge',
        'transitions': {
            'stop':['sm_action_failure']
        }
    },
    
    {   'name':'actionprompt',
        'action':'play_and_get_digits',
        'transitions': {
            'stop': ['sm_action_failure'],
            'recordcommentprompt': ['pacs_rsbymitra_record_comment'],
            'waitprompt': ['pacs_rsbymitra_call_beneficiary'],
            'playgrievance': ['pacs_rsbymitra_play_grievance'],
            'flaggrievance': ['pacs_rsbymitra_flag_grievance'],
            'inputgrievance': ['pacs_rsbymitra_enter_grievance'],
            'actionprompt': ['pacs_rsbymitra_invalid_choice'],
            'thankyou': ['sm_get_digits_no_digits']
        },
    },
    {   'name':'flaggrievance',
        'action':'none',
        'transitions': {
            'stop':['sm_action_failure'],
            'postactionprompt':['sm_action_success']
        },
    },
    {   'name': 'postactionprompt',
        'action': 'playback',
        'transitions': {
            'stop': ['sm_action_failure'],
            'inputgrievance':['sm_action_success']
        },
    },
    {   'name':'thankyou',
        'action':'playback',
        'transitions': {
            'stop': ['sm_action_failure', 'sm_action_success']
        },
    },
    {   'name':'stop',
        'action':'hangup',
        'transitions': {},
    },

]
