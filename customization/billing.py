from vapp.billing.utils import get_common_billing_infos, remove_exclusion_duplicates

def get_billing_info(start_date, end_date, ai, composite = True, unit_size = 60, excludes = []):
    infos = get_common_billing_infos(start_date, end_date, ai, composite, unit_size, excludes)
    remove_exclusion_duplicates(infos)

    return infos
