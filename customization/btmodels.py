from django.db import models

from vapp.app_manager.models import App_instance, CalleridField, Cdr
from vapp.media.models import Recording
from vapp.log import get_request_logger

logger = get_request_logger()

class BTG_caller( models.Model ):
    callerid    = CalleridField()
    call_count  = models.PositiveSmallIntegerField( default = 0 )
    curr_level  = models.PositiveSmallIntegerField( default = 0 )

    def __unicode__( self ):
        return "{0}_{1}".format( self.callerid, self.call_count )

class BTG_level( models.Model ):
    name        = models.CharField( max_length = 64 )
    desc        = models.CharField( max_length = 255 )
    level_id    = models.PositiveSmallIntegerField()
    level_ord   = models.PositiveSmallIntegerField()

    def __unicode__( self ):
        return "{0}_{1}_{2}".format( self.level_id, self.level_ord, self.name )

class BTG_call( models.Model ):
    cdr         = models.ForeignKey( Cdr, null = True, on_delete = models.SET_NULL )
    caller      = models.ForeignKey( BTG_caller )

    def __unicode__( self ):
        return unicode( self.caller ) + '_' + unicode( self.cdr )

class BTG_response( models.Model ):
    call        = models.ForeignKey( BTG_call )
    caller      = models.ForeignKey( BTG_caller )
    level       = models.ForeignKey( BTG_level )
    resp_key    = models.CharField( max_length = 8 )
    resp_rec    = models.ForeignKey( Recording, null = True )

    def __unicode__( self ):
        return "{0}_{1}_{2}_{3}".format( self.caller, self.level.name, self.resp_key, self.resp_rec )
        