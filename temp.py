import json
from vapp.xforms.models import *
from vapp.utils import *
import copy

FORM_ID='WCC_v_0.2'
header =['ACTIVITY_ID', 'PANCHAYAT_ID', 'PARTICIPANT_DETAILS_ID', 'PERSONAL_PH_ID', 'AGE_ID']
meta_format =[ 
{
	'meta_name':'ACTIVITY_ID',
	'meta_default':'ABSENT_AI',
	'1':'1-ABCD',
	'2': '2-ABCD',
	'3': '3-ABCD',
},
{
	'meta_name':'MEETING_OBJ_ID',
	'meta_default':'ABSENT_MOI',
	'meta_OneToMany':'1',
	'meta_separator':'::',
	'1':'1-MOI',
        '2': '2-MOI',
        '3': '7-MOI'
},
{
	'meta_name':'STAKEHOLDER_DETAILS_ID',
	'meta_default':('NA1, NA2'),
	'meta_format':[
	{
		'meta_name':'GENDER_ID',
		'meta_default':'ABSENT_GENDER',
		'1':'1-GE',
		'2': '2-GE',
		'3': '3-GE'

	},
	{
		'meta_name':'PHONE_NUM_ID',
		'meta_default':'0000000000'
	}	
	]
}

]

def create_rows(rows, data, keys):
    #print rows, data, keys
    for key in keys:
        #print type(keys), type(key), key['meta_name']a
        field=key['meta_name']
        if 'meta_format' in key: #there is nested data structure inside the key

	    if len(data[field]) == 0:
		for row in rows:
		    row.extend([key['meta_default']])
		continue
	    temprows = []	
            for nested_data in data[field]:
		#print "NESTED DATA==" +  str(nested_data)
	    	nrows = [x[:] for x in rows]
		#print "ROWS==" + str(nrows) 
		#print "NROWS==" + str(nrows) 
		create_rows(nrows, nested_data, key['meta_format'])
	        temprows += nrows
		#print "NROWS==" + str(nrows) 
		
	    del rows[:]
	    rows.extend(x for x in temprows)		
	    #print "TMPROWS==" + str(rows)
		
        elif 'meta_OneToMany' not in key or key['meta_OneToMany']!='1': # list and keys[key][0]['OneToMany']=='0':
            for row in rows:
	        if data[field] is None:
		    row.extend([field, key['meta_default']])
                else:
		    row.extend([field, key.get(data[field],data[field])]) # either get the mapped value stored in the  mapping of a field or return the value itself

        elif 'meta_OneToMany' in key and key['meta_OneToMany']=='1':
            for row in rows:
	        if data[field] is None:
		    row.extend([field, key['meta_default']])
                else:
		    row.extend([field, key['meta_separator'].join(key.get(j,j) for j in data[field])]) # convert the list of values to list of mapped values and join using seprerator

		
i=0
output=[]
for form in Generic_XForm.objects.filter(form_id = FORM_ID):
    data = json.loads(form.survey_json)
    #print data
    i=i+1
    rows = [[i]]
    create_rows(rows, data, meta_format)
    output.extend(rows)
    #print rows
for x in output:
    print x
    #continue



