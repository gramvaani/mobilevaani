import time as time_module
from datetime import *
import vapp.settings
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
import json

from boto3.dynamodb.conditions import Key, Attr
from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError
from django.core.serializers.json import DjangoJSONEncoder

from log import get_request_logger

from django.contrib.contenttypes.models import ContentType
from app_manager.models import App_instance, User_permission, Group_permission
from log import get_request_logger
from django.contrib.auth.models import User,Group
from vapp.utils import convert_image, get_file_creationtime, push_to_s3, upload_to_s3, ffmpeg_modify_mp3

from local_settings import SERVER_ID
from log import get_request_logger
logger = get_request_logger()


def create_dynamodb_connection():
    import boto3
    client = boto3.resource('dynamodb')
    return  client


def update_to_dynamo(model_name, pkey_value, skey_value, value_dict):
    key_dict = {}
    exp_dict = {}
    key_dict['pkey'] = pkey_value
    key_dict['skey'] = skey_value
    db = create_dynamodb_connection()
    table = db.Table(model_name)
    # updateexpression = "SET " +str(query_params['attr_name'] +" = :val1")
    # print (updateexpression)
    # exp_dict[':val1'] = query_params['attr_value']
    add_list = []
    remove_list = []
    exp_val_dict = {}
    exp_name_dict = {}
    query_params = json.loads(value_dict)
    for key, value in query_params.iteritems():
        if (value == None or value == ''):
            mod_key = "#"+key
            remove_list.append(mod_key)
            exp_name_dict.update({mod_key:key})
        else:
            s =  '#'+key+' = :'+key
            add_list.append(s)
            key_col = ":"+key
            exp_key = "#"+key
            exp_val_dict.update({key_col:value})
            exp_name_dict.update({exp_key:key})

    if remove_list:        
        remove_expression = " REMOVE "+",".join(remove_list)
        updateexpression = "SET "+",".join(add_list) + remove_expression
    else:
        updateexpression = "SET "+",".join(add_list)

    response = table.update_item(
               Key=key_dict,
               UpdateExpression=updateexpression,
               ExpressionAttributeValues=exp_val_dict,
               ExpressionAttributeNames=exp_name_dict,
               ReturnValues="UPDATED_NEW"
               )
    if response['ResponseMetadata']['HTTPStatusCode'] == 200 and 'Attributes' in response:
        logger.info("Updated DynamoDB for : "+str(key_dict))


def delete_obj_in_dynamo(model_name, pkey_value, skey_value):
    key_dict = {}
    key_dict['pkey'] = pkey_value
    key_dict['skey'] = skey_value
    db = create_dynamodb_connection()
    table = db.Table(model_name)
    table.delete_item(Key = key_dict)
    logger.info('Item is deleted, delete_dict: '+ str(key_dict))


def sync_campaign_properties(ai_id):
    ai_dict= {}
    ai = App_instance.objects.filter(pk = ai_id)
    from campaign.models import Campaign
    campaigns = Campaign.objects.filter(ais__in = ai)
    campaign_list = []
    for camp in campaigns:
        if camp.show_in_ui:
            camp_dict =  {}
            camp_dict["id"] = camp.id
            camp_dict["name"] = camp.name
            from campaign.models import Campaign_category
            camp_category = Campaign_category.objects.filter(campaign = camp)
            cc_parent = camp_category.filter(parent__isnull = True)
            cc_list = []
            for cc in cc_parent:
                p_dict = {}
                p_dict["name"] = cc.name
                p_dict["id"] = cc.id
                child_list = []
                childrens = camp_category.filter(parent = cc)
                for child in childrens:
                    c_dict ={}
                    c_dict["id"] = child.id
                    c_dict["name"] = child.name
                    child_list.append(c_dict)

                p_dict["children"] = child_list
                cc_list.append(p_dict)
            camp_dict["campaignCategories"] = cc_list
            campaign_list.append(camp_dict)
    ai_dict["campaign"] = json.dumps(campaign_list)
    ai_dict["ai"] = ai_id
    ai_dict['server'] = SERVER_ID
    if campaign_list:
        pkey = SERVER_ID+"-ai-"+str(ai_id)
        skey = 'Campaign'
        update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))
    else:
        logger.info('Not sending to server')

def sync_location_properties(ai_id):
    ai_dict = {}
    ai_dict['ai'] = ai_id
    loc_list = []
    logger.info('Syncing Location for ai_id : ' +str(ai_id))
    from location.models import Location
    locations = Location.objects.filter(ai_id = ai_id)
    for location in locations:
        loc_string = str(location).split(':')[1]
        loc_dict = {}
        loc_dict['name'] = loc_string
        loc_dict['id'] = location.id
        loc_list.append(loc_dict)

    ai_dict['location'] = json.dumps(loc_list)
    ai_dict['server'] = SERVER_ID
    pkey = SERVER_ID+"-ai-"+str(ai_id)
    skey = 'Location'
    update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))    

def sync_general_properties(ai_id):
    ai_dict = {}
    ai_dict['ai'] = ai_id
    channel_list = []
    logger.info('Syncing channel for ai : ' +str(ai_id))
    from mnews.models import Channel
    channels = Channel.objects.filter(ai_id = ai_id)
    for channel in channels:
        channel_dict = {}  
        channel_dict['id'] = channel.id
        channel_dict['name'] = channel.name
        channel_dict['sticky'] = channel.sticky
        channel_dict['default'] = channel.default
        channel_list.append(channel_dict)
        pub_list = 'publistCh'+str(channel.id)    
        ai_dict[pub_list] = []
        if channel.pub_list:
            ai_dict[pub_list] = eval(channel.pub_list)
    ai_dict['channel'] = json.dumps(channel_list)

    reject_reason_list = []
    from mnews.models import Event_rejection_reason_group
    event_rejection_reason_group = Event_rejection_reason_group.objects.filter(event__ai_id = ai_id)  
    if event_rejection_reason_group:
        rejection_reason_group = event_rejection_reason_group[0].group
        rejection_reasons = rejection_reason_group.rejection_reason
        for reason in rejection_reasons.all():
            reason_dict = {}
            reason_dict['name'] = reason.reason
            reason_dict['id'] = reason.id
            reject_reason_list.append(reason_dict)

    ai_dict['rejection_reason'] = json.dumps(reject_reason_list)

    pkey = SERVER_ID+"-ai-"+str(ai_id)
    skey = 'General'
    #sync_user_properties(user_id)
    update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))


def sync_user_properties(user_id):
    user_dict = {}
    ai_list = []
    user = User.objects.get(id=user_id) 
    grp_perm = Group.objects.filter(user = User.objects.get(id= user_id)).values_list('id',flat=True )  
    ai_grp = Group_permission.objects.filter(group__id__in = grp_perm).values_list('ai_id',flat=True )  
    ais = list(set(User_permission.objects.filter(user__id= user_id).values_list('ai_id', flat = True)))
    ais.extend(ai_grp)
    total_ais = map(int, filter(None, list(set(ais))))
    print(len(total_ais))
    print(user_id)
    for ai_id in total_ais:
        ai_dict = {}
        if ai_id:
            ai = App_instance.objects.get(id=ai_id)         
            if ai.status:                 
                ai_dict['ai_id'] = ai.id
                ai_dict['ai_name'] = ai.name
                ai_dict['app_pkg_name'] = ai.app.pkg_name
                channel_list=[]
                from mnews.models import Channel
                channels = Channel.objects.filter(ai = ai)
                for channel in channels:
                    channel_dict={}
                    channel_dict['id'] = channel.id
                    channel_dict['name'] = channel.name
                    channel_list.append(channel_dict)
                ai_dict['channels'] = channel_list  
        if ai_dict:
            ai_list.append(ai_dict) 
               
    user_dict["ai_list"] = json.dumps(ai_list)
    # user vs contact_list sync
    contact_list = []
    from callerinfo.models import Contact_list_entity
    c_lists = Contact_list_entity.objects.filter(content_type_id =3, object_id=user_id)
    for c_list in c_lists:
        c_dict = {}
        c_dict['name'] = c_list.list.name
        c_dict['id'] = c_list.list.id
        contact_list.append(c_dict)
    user_dict["contact_list"] = json.dumps(contact_list)

    user_dict['server'] = SERVER_ID
    user_dict['user_id'] = user_id
    user_dict['user_name'] = user.username
    user_dict['is_active'] = user.is_active
    user_dict['is_superuser'] = user.is_superuser
    pkey = SERVER_ID+"-user"
    skey = "username-" + user.username
    update_to_dynamo('Ai_properties', pkey, skey, json.dumps(user_dict, cls=DjangoJSONEncoder))


def sync_news(news, **kwargs):
        logger.info('Syncing Mnews data to dynamo')
        news_dict = model_to_dict(news)
        audio_url = ''
        if news.detail:
            audio_url = news.detail.get_url()
            news_dict['audio_url'] = audio_url
        image_url = ''
        if news.sm_image:
            image_url = news.sm_image.get_image_url()
            news_dict['image_url'] = image_url
        news_dict['server'] = SERVER_ID
        skey = str(news.id)
        prev_state = kwargs.get('prev_state')
        cur_state = kwargs.get('cur_state')
        prev_channel = kwargs.get('prev_channel')
        prev_checksum = kwargs.get('prev_checksum')
        pkey = SERVER_ID+"-ai-"+str(news.ai.id)
        lskey = news.generate_lskey()
        news_dict['lskey']  = lskey
        logger.info("LSKEY news_dict in sync_news:"+str(news_dict['lskey']))
        logger.info("STATE news_dict in sync_news:"+str(news_dict['state']))
        update_to_dynamo('Items', pkey, skey, json.dumps(news_dict, cls = DjangoJSONEncoder))
        from vapp.mnews.tasks import UploadtoS3Task
        logger.info(str(news.should_upload_audio_to_s3()))
        logger.info(str(news.should_upload_audio_to_s3()))
        if news.should_upload_audio_to_s3() or news.should_upload_image_to_s3():
           UploadtoS3Task.delay(news.id)

def sync_reordered_channel(channel):
    ai_dict = {}
    ai_dict['ai'] = channel.ai.id
    pub_list = 'publistCh'+str(channel.id)
    ai_dict[pub_list] = eval(channel.pub_list)
    pkey = SERVER_ID+"-ai-"+str(channel.ai.id)
    skey = 'General'
    update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))


def sync_contact(contact):
    user_dict = {}
    from callerinfo.models import Contact_list
    clists = Contact_list.objects.filter(contacts = contact)
    user_dict["server"] = SERVER_ID
    pkey = SERVER_ID+"-contact"
    for clist in clists:
        from callerinfo.models import Contact
        contacts = Contact.objects.filter(number = contact.number,  contact_list= clist)
        contact_list = []
        for contact in contacts:
            c_dict = {}
            c_dict['name'] = contact.name
            c_dict['id'] = contact.id
            c_dict['gender'] = contact.gender
            try:
                if contact.location_fk and contact.location_fk.id:
                    c_dict['location_id'] = contact.location_fk.id
                else:
                    c_dict['location_id'] = None
            except Exception as ee:
                logger.info("Exception in Contact location : "+str(ee))
            contact_list.append(c_dict)
        user_dict["contact"] = json.dumps(contact_list)
        user_dict["contact_list_id"] = clist.id
        skey1 = "msisdn-"+str(contact.number)+"-cl-"+str(clist.id)
        skey2 = "cl-"+str(clist.id)+"-msisdn-"+str(contact.number)
        update_to_dynamo('Ai_properties', pkey, skey1, json.dumps(user_dict, cls=DjangoJSONEncoder))
        update_to_dynamo('Ai_properties', pkey, skey2, json.dumps(user_dict, cls=DjangoJSONEncoder))

def sync_static_info():
    from mnews.models import Gender     
    genders = Gender.objects.all()
    from mnews.models import Age_group
    age_groups = Age_group.objects.all()
    from mnews.models import Qualifier
    qualifiers = Qualifier.objects.all()
    from mnews.models import Format
    formats = Format.objects.all()
    from mnews.models import Occupation
    occupations = Occupation.objects.all()

    ai_dict = {}
    gender_list = []
    formats_list = []
    age_group_list = []
    qualifier_list = []
    occupation_list = []
    for gender in genders:
        stat_dict={}
        stat_dict['id'] = gender.id
        stat_dict['name'] = gender.gender
        gender_list.append(stat_dict)
    for form in formats:
        stat_dict={}
        stat_dict['id'] = form.id
        stat_dict['name'] = form.name
        formats_list.append(stat_dict)
    for age_group in age_groups:
        stat_dict={}
        stat_dict['id'] = age_group.id
        stat_dict['name'] = age_group.name
        age_group_list.append(stat_dict)
    for qualifier in qualifiers:
        stat_dict={}
        stat_dict['id'] = qualifier.id
        stat_dict['name'] = qualifier.name
        qualifier_list.append(stat_dict)
    for occupation in occupations:
        stat_dict={}
        stat_dict['id'] = occupation.id
        stat_dict['name'] = occupation.name
        occupation_list.append(stat_dict)

    ai_dict['gender'] = json.dumps(gender_list)
    ai_dict['occupation'] = json.dumps(occupation_list)
    ai_dict['format'] = json.dumps(formats_list)
    ai_dict['age_group'] = json.dumps(age_group_list)
    ai_dict['qualifier'] = json.dumps(qualifier_list)
    ai_dict['server'] = SERVER_ID
    pkey = SERVER_ID+"-mnews-static"
    skey = 'Static'
    logger.info("Static data sync :: " + str(ai_dict))
    update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))
