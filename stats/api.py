from django.conf.urls import url

from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import ReadOnlyAuthorization
from tastypie.utils import trailing_slash
from tastypie import fields, http

from vapp.api import AppInstanceAuthorization, apply_default_checks

from datetime import *

from django.db.models import Q
from django.forms.models import model_to_dict

import vapp.app_manager.perms as app_manager_perms
from vapp.perms import user_perms_ai_ids
from vapp.app_manager.models import App_instance
from vapp.app_manager.api import AppResource, UserResource
from models import StatsView
from vapp.stats.stats import get_hourly_stats, get_calls_per_user_distrib, get_call_duration_distrib, \
get_callers_for_ai_ids, get_schedule_summary, get_dialout_details

from vapp.log import get_request_logger

logger = get_request_logger()

class StatsViewAuthorization(ReadOnlyAuthorization):
    def __init__(self, read_perms= [app_manager_perms.app_use]):
        super(StatsViewAuthorization, self).__init__()
        self.read_perms = read_perms

    def read_list(self, object_list, bundle):
        try:
            user = bundle.request.user
            if not user.is_authenticated():
                return []

            ai_ids = user_perms_ai_ids(user, *self.read_perms) 
            apps = set([App_instance.objects.get(pk = ai_id).app for ai_id in ai_ids])
            statsviews_for_user = StatsView.objects.filter(Q(apps__in = apps) | Q(users = user) | Q(for_all_users = True))
            return list(set(object_list) & set(statsviews_for_user))
        except:
            logger.exception( "Unable to read_list for bundle: %s" % bundle )
        return []

    def read_detail(self, object_list, bundle):
        return False


class StatsViewResource(ModelResource):
    apps = fields.ToManyField(AppResource, 'apps')
    users = fields.ToManyField(UserResource, 'users')

    class Meta:
        queryset = StatsView.objects.all()
        authentication = ApiKeyAuthentication()
        authorization = StatsViewAuthorization()

        allowed_methods = ['get']

        filtering = {
            'apps': ALL_WITH_RELATIONS,
            'users': ALL_WITH_RELATIONS,
            'for_all_users': ALL
        }  
        

class StatsResource( Resource ):
    class Meta:
        resource_name = 'stats'
        authentication = ApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/hourly_calls%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_hourly_call_stats' ), name = "api_hourly_call_stats" ),
            url(r"^(?P<resource_name>%s)/calls_per_user%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_calls_per_user_distribution' ), name = "api_calls_per_user_distrib" ),
            url(r"^(?P<resource_name>%s)/call_dur_distrib%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'get_call_duration_distribution' ), name = "api_call_dur_distrib" ),
            url(r"^(?P<resource_name>%s)/get_callers%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_callers'), name = "api_get_callers" ),
            url(r"^(?P<resource_name>%s)/dialout%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_dialout_results'), name = "api_get_dialout_results"),
        ]
        
    def get_hourly_call_stats(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST['ai_id']
            start_datetime = datetime.strptime(request.REQUEST['start_date'], '%Y-%m-%d')
            end_datetime = datetime.strptime(request.REQUEST['end_date'], '%Y-%m-%d')
            stats = get_hourly_stats(ai_id, start_datetime, end_datetime)

            self.log_throttled_access( request )
            return self.create_response( request, { "stats": stats } )
        except:
            logger.exception( "Exception while running: get_hourly_call_stats: %s" % request )
        return self.create_response( request, { 'error': "unable to get hourly call stats" } )


    @apply_default_checks( authorization = AppInstanceAuthorization() )
    def get_calls_per_user_distribution(self, request, **kwargs):
        ai_id = kwargs['ai']
        callerids = kwargs.get('callerids', None)
        start_datetime = kwargs.get('start_date', None)
        end_datetime = kwargs.get('end_date', None)
        calls_threshold = kwargs.get('calls_threshold', 1)
        try:
            if start_datetime and end_datetime:
                start_datetime = datetime.strptime( start_datetime, '%Y-%m-%d' )
                end_datetime = datetime.strptime( end_datetime, '%Y-%m-%d' )

            if callerids:
                callerids = callerids.split(",")

            stats = get_calls_per_user_distrib( ai_id, callerids, calls_threshold, start_datetime, end_datetime )
            return self.create_response( request, { "objects": stats } )
        except:
            logger.exception( "Exception while running: get_calls_per_user_distribution: %s" % request )
        return self.create_response( request, { 'error': "unable to get calls per user distribution" } )

    
    def get_call_duration_distribution(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )
        
        try:
            ai_id = request.REQUEST['ai_id']
            start_datetime = request.REQUEST.get('start_date', None)
            end_datetime = request.REQUEST.get('end_date', None)
            if start_datetime and end_datetime:
                start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d')
                end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d')
                        
            stats = get_call_duration_distrib(ai_id, start_datetime, end_datetime)

            self.log_throttled_access( request )
            return self.create_response( request, { "stats": stats } )
        except:
            logger.exception( "Exception while running: get_call_duration_distribution: %s" % request )
        return self.create_response( request, { 'error': "unable to get call duration distribution" } )

    def get_callers( self, request, **kwargs ):
        self.method_check( request, allowed = [ 'get' ] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            ai_id = int( request.REQUEST[ 'ai' ] )
            start_datetime = datetime.strptime( request.REQUEST[ 'start' ], '%Y-%m-%d' )
            end_datetime = datetime.strptime( request.REQUEST[ 'end' ], '%Y-%m-%d' )
            callerids = get_callers_for_ai_ids( [ ai_id ], start_datetime, end_datetime )
            self.log_throttled_access( request )
            return self.create_response( request, { 'callerids': callerids } )
        except:
            logger.exception( "get_callers: %s" % request )
        return self.create_response( request, { 'error': "unable to get callers for ai" }, response_class = http.HttpApplicationError )

    def get_dialout_results(self, request, **kwargs):
        self.method_check(request, allowed = ['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            gcl_ids = request.REQUEST.get('call_log_ids')
            schedule_ids = request.REQUEST.get('schedule_ids')
            if not gcl_ids and not schedule_ids:
                raise Exception('Insufficient parameters passed.')

            data = {}
            if gcl_ids:
                data['call_log_result'] = [get_dialout_details(gcl_id) for gcl_id in gcl_ids.split(',')]

            if schedule_ids:
                data['schedule_summary'] = [get_schedule_summary(schedule_id) for schedule_id in schedule_ids.split(',')]

            self.log_throttled_access(request)
            return self.create_response(request, {'data': data})
        except:
            logger.exception("get_dialout_results: %s" % request)
        return self.create_response(request, {'error': "unable to get_dialout_results"}, response_class = http.HttpApplicationError)
