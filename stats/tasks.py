from celery.task import Task

from app_manager.models import App_instance, STATUS_ACTIVE, Cdr
from customer.models import get_active_projects
from vapp.stats.models import Stats_type, Ai_stats_setting
from vapp.stats.stats import generate_and_email_stats, require_project_custom_stats, \
populate_caller_call_stats
from schedule.models import Repetition_choices
from advert.stats import populate_stats as populate_ad_stats
from campaign.stats import populate_stats as populate_campaign_stats
from field_mis.stats import populate_stats as populate_field_stats
from app_manager.common import load_module

from vapp.utils import load_vapp_stats, is_start_of_week, DEFAULT_ALERTS_RECIPIENT, \
load_vapp_module, send_email

from datetime import datetime, timedelta
import socket

from log import get_logger
logger = get_logger()

hostname = socket.gethostname()



class PopulateYesterdayStatsTask(Task):
    def run(self):
        active_ais = App_instance.objects.filter(status = STATUS_ACTIVE)
        for ai in active_ais:
            stats = load_vapp_stats(ai)
            end_date = datetime.now().date()
            start_date = (datetime.now() - timedelta(days = 1)).date()

            if hasattr(stats, 'populate_stats'):
                stats.populate_stats(ai, start_date, end_date, force_update = False)


class PopulateStatsTask(Task):

    def run(self, ai_ids = None, start_datetime = None, end_datetime = None, force_update = False):
        start_time = datetime.now()
        loginfo = 'RUNNING_TIME: PopulateStatsTask/run/start  ' + str(start_time)
        logger.info(loginfo)
        if not ai_ids:
            active_ais = App_instance.objects.filter(status = STATUS_ACTIVE)
        else:
            active_ais = [App_instance.objects.get(pk = ai_id) for ai_id in ai_ids]

        if not (start_datetime and end_datetime):
            end_datetime = datetime.now().replace(hour = 0,minute = 0,second = 0, microsecond = 0)
            start_datetime = end_datetime - timedelta(days = 1)

        for ai in active_ais:
            stats = load_vapp_stats(ai)
            if hasattr(stats, 'populate_stats'):
                st = datetime.now()
                logger.info('RUNNING_TIME: PopulateStatsTask/ai_id=%s, ai_name=%s start_time = %s' % (ai.id, ai.name, st))
                stats.populate_stats(ai, force_update = force_update)
                et = datetime.now()
                logger.info('RUNNING_TIME: PopulateStatsTask/ai_id=%s, ai_name=%s end_time = %s' % (ai.id, ai.name, et))
                logger.info('RUNNING_TIME: PopulateStatsTask/ai_id=%s, ai_name=%s execution_time = %s' % (ai.id, ai.name, et-st))

                st = datetime.now()
                logger.info('RUNNING_TIME: PopulateStatsTask/ad/ai_id=%s, ai_name=%s start_time = %s' % (ai.id, ai.name, st))
                populate_ad_stats(ai, start_datetime, end_datetime)
                et = datetime.now()
                logger.info('RUNNING_TIME: PopulateStatsTask/ad/ai_id=%s, ai_name=%s end_time = %s' % (ai.id, ai.name, et))
                logger.info('RUNNING_TIME: PopulateStatsTask/ad/ai_id=%s, ai_name=%s execution_time = %s' % (ai.id, ai.name, et-st))

        st = datetime.now()
        logger.info('RUNNING_TIME: PopulateStatsTask/ai and ad/ total_execution_time %s' % (st - start_time))
        logger.info('RUNNING_TIME: PopulateStatsTask/campaign, start_time = %s' % ( st))
        populate_campaign_stats(force_update = force_update)
        et = datetime.now()
        logger.info('RUNNING_TIME: PopulateStatsTask/campaign end_time = %s' % ( et))
        logger.info('RUNNING_TIME: PopulateStatsTask/campaign/execution_time = %s' % (et-st))

        st = datetime.now()
        logger.info('RUNNING_TIME: PopulateStatsTask/field, start_time = %s' % ( st))
        populate_field_stats()
        populate_caller_call_stats(start_datetime, end_datetime)
        et = datetime.now()
        logger.info('RUNNING_TIME: PopulateStatsTask/field end_time = %s' % ( et))
        logger.info('RUNNING_TIME: PopulateStatsTask/field/execution_time = %s' % (et-st))

        email_body = ''
        email_body += 'Hostname: ' + hostname
        email_body += '<br/>Task start time: ' + str(start_time)[:16] + ' HRS'
        email_body += '<br/>Task end time: ' + str(datetime.now())[:16] + ' HRS'
        send_email('AlertBot says: PopulateStatsTask finished', email_body,
                   [DEFAULT_ALERTS_RECIPIENT])
        end_time = datetime.now()
        loginfo = 'RUNNING_TIME: PopulateStatsTask/run/end  ' + str(end_time)
        logger.info(loginfo)
        logger.info('RUNNING_TIME: PopulateStatsTask/run execution_time ' + str(end_time - start_time))


class EmailStatsTask(Task):

    def run(self, project_ids = [], start_datetime = None, end_datetime = None):
        start_time = datetime.now()
        loginfo = 'RUNNING_TIME: EmailStatsTask/run/start  ' + str(start_time)
        logger.info(loginfo)
        projects = get_active_projects(project_ids)

        if not (start_datetime and end_datetime):
            end_datetime = datetime.today().replace(hour = 0, minute = 0, second = 0, microsecond = 0)
            start_datetime = end_datetime - timedelta(days = 1)

        for project in projects:
            recipients = [user.email for user in project.stats_emails.all() if user.email]
            ai_stats_settings = Ai_stats_setting.objects.filter(ai__in = project.ais.filter(status = STATUS_ACTIVE), generate_workbook = True)
            st = datetime.now()
            logger.info('RUNNING_TIME: EmailStatsTask/project_id=%s, project_name=%s, start_time = %s' % (project.id, project.name, st))
            generate_and_email_stats(project, recipients, start_datetime, end_datetime, Stats_type.DAILY, ai_stats_settings.filter(schedule__repetition = Repetition_choices.DA))
            et = datetime.now()
            logger.info('RUNNING_TIME: EmailStatsTask/project_id=%s, project_name=%s, end_time = %s' % (project.id, project.name, et))
            logger.info('RUNNING_TIME: EmailStatsTask/project_id=%s, project_name=%s, execution_time = %s' % (project.id, project.name, et-st))
            if is_start_of_week(end_datetime.date()):
                if require_project_custom_stats( project ):
                    continue
                else:
                    week_start_datetime = end_datetime - timedelta(days = 7)
                    logger.info('RUNNING_TIME: EmailStatsTask/weekly/project_id=%s, project_name=%s, start_time = %s' % (project.id, project.name, st))
                    generate_and_email_stats(project, recipients, week_start_datetime, end_datetime, Stats_type.WEEKLY, ai_stats_settings.filter(schedule__repetition = Repetition_choices.WE))
                    et = datetime.now()
                    logger.info('RUNNING_TIME: EmailStatsTask/weekly/project_id=%s, project_name=%s, end_time = %s' % (project.id, project.name, et))
                    logger.info('RUNNING_TIME: EmailStatsTask/weekly/project_id=%s, project_name=%s, execution_time = %s' % (project.id, project.name, et-st))

        email_body = ''
        email_body += 'Hostname: ' + hostname
        email_body += '<br/>Task start time: ' + str(start_time)[:16] + ' HRS'
        email_body += '<br/>Task end time: ' + str(datetime.now())[:16] + ' HRS'
        send_email('AlertBot says: EmailStatsTask finished', email_body, [DEFAULT_ALERTS_RECIPIENT])
        end_time = datetime.now()
        loginfo = 'RUNNING_TIME: EmailStatsTask/run/end  ' + str(end_time)
        logger.info(loginfo)
        logger.info('RUNNING_TIME: EmailStatsTask/run execution_time ' + str(end_time - start_time))


class PopulateSessionDataTask(Task):

    def run(self, session_data):
        try:
            cdr_id = session_data.session_cdrs[-1].id
            logger.info('running populate session data task for cdr id: ' + str(cdr_id))
            cdr = Cdr.objects.get(id=cdr_id)
            app_session_data = session_data.app_data
            for pkg_name, data in app_session_data.iteritems():
                stats_module = load_vapp_module(pkg_name, 'stats')
                if hasattr(stats_module, 'populate_session_data'):
                    stats_module.populate_session_data(data, cdr)
        except Exception as exception:
            logger.info('exception while running populate session data task:' + str(exception))