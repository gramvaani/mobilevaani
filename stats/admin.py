from django.contrib import admin
from models import *

class StatsView_admin(admin.ModelAdmin):
    filter_horizontal = ('apps','users')

admin.site.register(StatsView, StatsView_admin)
admin.site.register(Ai_stats_setting)
