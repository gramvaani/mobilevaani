from django.db import models
from django.db import connection
from django.template.loader import render_to_string
from django.db.models import get_model, Q, Count, Sum
from django.forms.models import model_to_dict

from vapp.app_manager.common import load_module

from vapp.app_manager.models import App_instance, Cdr, Transition_event, \
Ai_vi_start_event, STATUS_ACTIVE, Cdr_in_out_map, Caller_filter_log, \
Callback_delay_log, get_dialout_status
from vapp.mnews.models import Groups_call_log, News, News_state
from vapp.stats.models import Cumulative_call_stats, Stats_type, Stats, \
Caller_call_stats
from customization.models import Custom_stats_settings

from vapp.utils import get_ratio, get_total_seconds, get_date_from_str, \
daterange, generate_attachment, get_sorted_data, load_vapp_module, \
send_email, LINE_BREAK, EMPTY_ROW, generate_workbook

from datetime import *
from dateutil.relativedelta import relativedelta
import collections

from log import get_request_logger

logger = get_request_logger()

TRANS_EV_LOG_START = datetime(2013, 10, 1)
AI_VI_EVENT_START = datetime(2014, 4, 21)
CLUB_START_DATE = datetime(2014, 8, 1)


class TimeFrame():
    WEEKLY = 1
    MONTHLY = 2
    START_OF_THE_MONTH = 3

    def __init__(self, timeframe, end_date = None):
        self.timeframe = timeframe
        self.end_date = end_date
        if(end_date == None):
            self.end_date = datetime.now()
    
    def  get_timeframe(self):    
        if self.timeframe == TimeFrame.WEEKLY:
            if ( self.end_date.weekday()==0 and self.end_date.hour==0 and self.end_date.minute==0 and self.end_date.second==0 ):
                start_date = self.end_date - timedelta(days = 7)
                return start_date
            start_date = datetime.combine(self.end_date - timedelta(self.end_date.weekday()), datetime.min.time())
            return start_date
        if self.timeframe == TimeFrame.MONTHLY:
            start_date = datetime.combine(self.end_date, datetime.min.time()) - relativedelta(months=1)
            return start_date
        if self.timeframe == TimeFrame.START_OF_THE_MONTH:
            if ( self.end_date.day==1 and self.end_date.hour==0 and self.end_date.minute==0 and self.end_date.second==0 ):
                start_date = self.end_date - relativedelta(months = 1)
                return start_date
            start_date = datetime.combine(datetime(self.end_date.year,self.end_date.month,1), datetime.min.time())
            return start_date
        

def get_callers_for_ai(ai, start_date = None, end_date = None):
    cdr_objects = Cdr.objects.filter(ai = ai, is_incoming = True)
    ai_vi_start_event_objects = Ai_vi_start_event.objects.exclude(prev_ai = models.F('next_ai')).filter(next_ai = ai, trans_event__cdr__is_incoming = True)
    if (start_date == None and end_date == None):
        direct_calls_callerids = list(cdr_objects.values_list('callerid', flat = True).distinct())
        trans_calls_callerids = list(ai_vi_start_event_objects.values_list('trans_event__cdr__callerid', flat = True).distinct())
    elif start_date == None:
        direct_calls_callerids = list(cdr_objects.filter(start_time__lte = end_date).values_list('callerid', flat = True).distinct())
        trans_calls_callerids = list(ai_vi_start_event_objects.filter(trans_event__time__lte = end_date).values_list('trans_event__cdr__callerid', flat = True).distinct())
    elif end_date == None:
        direct_calls_callerids = list(cdr_objects.filter(start_time__gte = start_date).values_list('callerid', flat = True).distinct())
        trans_calls_callerids = list(ai_vi_start_event_objects.filter(trans_event__time__gte = start_date).values_list('trans_event__cdr__callerid', flat = True).distinct())
    else:
        direct_calls_callerids = list(cdr_objects.filter(start_time__range = (start_date, end_date)).values_list('callerid', flat = True).distinct())
        trans_calls_callerids = list(ai_vi_start_event_objects.filter(trans_event__time__range = (start_date, end_date)).values_list('trans_event__cdr__callerid', flat = True).distinct())
    all_caller_ids = direct_calls_callerids + trans_calls_callerids
    return set(all_caller_ids)
    

def get_callers_for_ai_for_timeframe(ai, timeframe, start_date = None, end_date = None):
    if(start_date == None):
        start_date = get_start_date_for_instance(ai)

    if(end_date == None):
        temp_date = TimeFrame(timeframe).get_timeframe()
    else:
        temp_date = TimeFrame(timeframe, end_date).get_timeframe()

    ai_specific_dict_list = []

    while temp_date > start_date:
        ai_specific_dict = {}
        callers_count = len(get_callers_for_ai(ai, temp_date, end_date))
        ai_specific_dict['start_date'] = temp_date
        ai_specific_dict['end_date'] = end_date
        ai_specific_dict['callers_count'] = callers_count
        ai_specific_dict_list.append(ai_specific_dict)
        end_date = temp_date
        temp_date = TimeFrame(timeframe, end_date).get_timeframe()
    callers_count = len(get_callers_for_ai(ai, start_date, end_date))
    ai_specific_dict = {}
    ai_specific_dict['start_date'] = start_date
    ai_specific_dict['end_date'] = end_date
    ai_specific_dict['callers_count'] = callers_count
    ai_specific_dict_list.append(ai_specific_dict)
    return ai_specific_dict_list


def get_club_calls_for_min_dur(ai, start_date, end_date):
    direct_call_cdr_count = Cdr.objects.filter(ai = ai, answered_time__isnull = False, end_time__isnull = False, is_incoming = False, start_time__range = (start_date, end_date)).count()    
    trans_call_cdr_count = Ai_vi_start_event.objects.exclude(prev_ai = models.F('next_ai')).filter(next_ai = ai, trans_event__cdr__answered_time__isnull = False, trans_event__cdr__end_time__isnull = False, trans_event__cdr__is_incoming = False, trans_event__time__range = (start_date, end_date)).count()
    total_calls_count = direct_call_cdr_count + trans_call_cdr_count
    return total_calls_count


def get_callscount_callerscount_avgdur(ai_id, start_datetime, end_datetime, callerids = None):
    calls = get_calls(ai_id, start_datetime, end_datetime, callerids)
    callers_count,avg_duration = get_callerscount_avgdur(calls)
    return len(calls), callers_count, avg_duration


def get_callerscount_avgdur(calls):
    avg_duration = 0
    callers,total_duration,duration_count = get_callers_totdur(calls)
    if duration_count != 0:
        avg_duration = get_ratio(total_duration, duration_count)
    return len(callers), avg_duration


def get_daily_avg_calls(ai_id, start_datetime, end_datetime, call_type):
    cdrs = get_calls(ai_id, start_datetime, end_datetime)
    call_count = cdrs.filter(trigger = call_type).count()
    duration = (end_datetime - start_datetime).days
    return get_ratio(call_count, duration)


def get_callers_totdur(calls):
    total_duration = duration_count = 0
    callers = set()
    for call in calls:
        callers.add(call.callerid)
        if call.end_time and call.answered_time: 
            call_duration = get_total_seconds(call.end_time - call.answered_time)
            if call_duration < 0:
                continue
            total_duration += call_duration
            duration_count += 1
    return callers, total_duration, duration_count


def get_call_duration(cdr):
    if cdr.end_time and cdr.answered_time:
        duration = get_total_seconds(cdr.end_time - cdr.answered_time)
    else:
        duration = get_call_duration_from_trans_events(cdr)

    return duration

def get_total_duration_of_calls( cdrs ):
    duration = 0.0
    for cdr in cdrs:
        call_duration = get_call_duration(cdr)
        if call_duration < 0:
            continue
        duration += call_duration

    return duration

def get_call_duration_from_trans_events( cdr ):
    trans_events = Transition_event.objects.filter( cdr = cdr ).order_by( 'time' )
    if trans_events:
        return get_total_seconds(trans_events.latest('id').time - trans_events[0].time)
    else:
        return 0

def get_time_spent_on_ai(cdr_id, ai_id):
    """Calculate total time spent on ai by using transition_event table.  Taking
    first ai transition id time as ai start time and last transition ai id 
    time as ai end time. if transition end time is not available then call end 
    time from cdr table is considered as ai 
    last transition time or end time"""
    time_spent = 0.0

    ai_vi_events = Ai_vi_start_event.objects.filter(trans_event__cdr = cdr_id).filter( \
                                                    Q(prev_ai = ai_id) | Q(next_ai = ai_id) ).order_by('id')
    if not ai_vi_events.exists():
        return time_spent

    start_event = None
    end_event = None

    for each in ai_vi_events:
        if not start_event:
            start_event = each.trans_event
            continue

        if (each.prev_ai_id == ai_id) and (each.next_ai_id != ai_id):
            end_event = each.trans_event
            time_spent += get_total_seconds(end_event.time - start_event.time)
            start_event = None
            end_event = None

    if start_event and not end_event:
        end_time = ai_vi_events[0].trans_event.cdr.end_time
        if end_time:
            time_spent += get_total_seconds(end_time - start_event.time)

    return time_spent

def get_callers_of_calls(calls):
    callers = set()
    for call in calls:
        callers.add(call.callerid)
    return callers

def get_daily_calls_callers_from_start_date_till_today(start_date, ai_id):
    start = get_date_from_str(start_date)
    end = (datetime.now() + timedelta(days = 1)).date()
    ai = App_instance.objects.get(pk = ai_id)

    data = {}
    for d in daterange(start, end):
        calls = get_calls(ai.id, d, d + timedelta(days = 1))
        callers = get_callers_of_calls(calls)
        data[str(d)] = [len(calls), len(callers)]

    return data

def get_daily_avg_dur_from_start_date_till_today(start_date, ai_id):
    start = get_date_from_str(start_date)
    end = (datetime.now() + timedelta(days = 1)).date()
    ai = App_instance.objects.get(pk = ai_id)

    data = {}
    for d in daterange(start, end):
        calls = get_calls(ai.id, d, d + timedelta(days = 1))
        callers, dur, dur_count = get_callers_totdur(calls)
        data[str(d)] = dur/float(dur_count) if dur_count > 0 else 0

    return data

def get_cdr_stats_start_end(start_datetime, end_datetime):
    if start_datetime >= TRANS_EV_LOG_START:
        return None, None
    else:
        return start_datetime, min(end_datetime, TRANS_EV_LOG_START)

def get_trans_stats_start_end(start_datetime, end_datetime):
    if end_datetime <= TRANS_EV_LOG_START:
        return None, None
    else:
        return max(start_datetime, TRANS_EV_LOG_START), end_datetime

def get_start_date_for_instance(ai_id):
    cdrs = Cdr.objects.filter(ai_id = ai_id)
    start_datetime_from_cdr = cdrs.order_by('id')[0].start_time if cdrs.exists() else None
    trans_events = Transition_event.objects.filter(ai_id = ai_id)
    start_datetime_from_trans_ev = trans_events.order_by('id')[0].time if trans_events.exists() else None
    if not (start_datetime_from_cdr or start_datetime_from_trans_ev):
        return
    if start_datetime_from_cdr and start_datetime_from_trans_ev:
        return min(start_datetime_from_cdr, start_datetime_from_trans_ev)
    else:
        return start_datetime_from_cdr or start_datetime_from_trans_ev

def get_calls(ai_id, start_datetime, end_datetime, callerids = None, is_incoming = None):
    calls = Cdr.objects.none()
    cdr_start, cdr_end = get_cdr_stats_start_end(start_datetime, end_datetime)
    trans_start, trans_end = get_trans_stats_start_end(start_datetime, end_datetime)

    if cdr_start and cdr_end:
        calls = get_calls_from_cdr(ai_id, start_datetime, min(end_datetime, TRANS_EV_LOG_START), callerids, is_incoming)
    if trans_start and trans_end:
        calls = calls | get_calls_from_trans_events(ai_id, max(start_datetime, TRANS_EV_LOG_START), end_datetime, callerids, is_incoming)

    return calls


def get_calls_from_cdr(ai_id, start_datetime, end_datetime, callerids, is_incoming):
    cdrs = Cdr.objects.filter(ai_id = ai_id, start_time__range = (start_datetime, end_datetime), answered_time__isnull = False)

    if is_incoming is not None:
        cdrs = cdrs.filter(is_incoming = is_incoming)

    if callerids:
        cdrs = cdrs.filter(callerid__in = callerids)

    return cdrs


def get_calls_from_trans_events(ai_id, start_datetime, end_datetime, callerids, is_incoming):
    query = "select C.id from app_manager_cdr C, app_manager_transition_event T where cdr_id = C.id and T.ai_id = %s and C.start_time >= '%s' and C.start_time < '%s' and answered_time is not null" % (ai_id, start_datetime, end_datetime)
    cdrids = Cdr.objects.raw(query)
    cdrs = Cdr.objects.filter(id__in = [c.id for c in cdrids])

    if is_incoming is not None:
        cdrs = cdrs.filter(is_incoming = is_incoming)

    if callerids:
        cdrs = cdrs.filter(callerid__in = callerids)

    return cdrs

def get_calls_count_by_caller( callerid, ai, start_time, end_time ):
    return Cdr.objects.filter( ai = ai, callerid = callerid, start_time__range = (start_time, end_time), is_incoming = False, answered_time__isnull = False, trigger = 0).count()

def yesterday_cumulative_stats_present(ai, to_datetime = None):
    if to_datetime is None:
        to_datetime = datetime.combine(datetime.today(), datetime.min.time())
    prev_stat = Cumulative_call_stats.objects.filter(ai = ai, to_time = to_datetime).count()
    return prev_stat > 0

def get_prev_cumulative_stats(ai):
    cumulative_stats = Cumulative_call_stats.objects.filter(ai = ai).order_by('-id')
    if cumulative_stats.exists():
        prev_cumulative_stat = cumulative_stats[0]
        total_calls = prev_cumulative_stat.calls
        total_duration = float(total_calls * prev_cumulative_stat.avg_dur)
        return prev_cumulative_stat.to_time, total_calls, total_duration
    else:
        return None, 0, 0

def populate_daily_cumulative_stats(ai, start_datetime = None, end_datetime = None, force_update = False):
    mean_avg_dur = 0
    if not end_datetime:
        end_datetime = datetime.now().replace(hour = 0, minute = 0, second = 0, microsecond = 0)
    start, total_calls, total_duration = get_prev_cumulative_stats(ai)
    if not start_datetime:
        if start:
            start_datetime = datetime.combine(start, datetime.min.time())
        else:
            start_datetime = get_start_datetime_for_cumulative_stats(ai)            
            if not start_datetime:
                logger.info("start_datetime not found for %s." % ai.name)
                return
    else:
        if force_update:
            total_calls = 0
            total_duration = 0
            start_datetime = datetime.combine(start_datetime, datetime.min.time())
            cumulative_call_stats_qs = Cumulative_call_stats.objects.filter(ai = ai, to_time = start_datetime)
            if cumulative_call_stats_qs.count() > 0:
                total_calls = cumulative_call_stats_qs[0].calls
                total_duration = total_calls * cumulative_call_stats_qs[0].avg_dur

    if not force_update and yesterday_cumulative_stats_present(ai, end_datetime):
        logger.info("populate_cumulative_call_stats: cumulative stats for ai %s already present. skipping recalculation." % ai.name)
        return

    for date_time in daterange(start_datetime, end_datetime):
        to_datetime = date_time + timedelta(days = 1)
        prev_stats = Cumulative_call_stats.objects.filter(ai = ai, to_time = to_datetime)
        if prev_stats.exists() and not force_update:
            continue
        
        call_stats = get_ai_call_stats(ai, date_time, to_datetime)
        call_count = call_stats.calls
        avg_dur = call_stats.avg_call_dur
        total_calls += call_count
        # Temporarily not calculating callers count
        callers = 0 #get_cumulative_callers_count(ai.id, to_datetime)
        total_duration += avg_dur * call_count
        mean_avg_dur = get_ratio(total_duration, total_calls)

        if prev_stats.exists() and force_update:
            prev_stats.update(calls = total_calls, callers = callers, avg_dur = str(mean_avg_dur))
        else:
            call_stat = Cumulative_call_stats(ai = ai, to_time = to_datetime, calls = total_calls, callers = callers, avg_dur = str(mean_avg_dur))
            call_stat.save()


def get_start_datetime_for_cumulative_stats(ai):
    """
    For old instances, call statistics needs to be obtained from both Cdr and Transition_events tables. So we are calculating
    min of start_datetime across Cdr and Transition_events. 
    """
    try:
        trans_start_datetime = Transition_event.objects.filter(ai=ai)[:1].get().time.replace(hour=0,minute=0,second=0, microsecond=0)
    except:
        trans_start_datetime = None

    try:
        cdr_start_datetime = Cdr.objects.filter(ai=ai)[:1].get().start_time.replace(hour=0,minute=0,second=0, microsecond=0)
    except:
        cdr_start_datetime = None

    if trans_start_datetime and cdr_start_datetime:
        start_datetime = min(trans_start_datetime, cdr_start_datetime)
    else:
        start_datetime = trans_start_datetime or cdr_start_datetime

    return start_datetime


def get_total_duration(calls):
    total_duration = 0

    for call in calls:
        if call.end_time and call.answered_time:
            call_duration = get_total_seconds(call.end_time - call.answered_time)
            if call_duration < 0:
                continue
            total_duration += call_duration

    return total_duration


def get_avg_dur(calls):
    total_duration = duration_count = avg_duration = 0

    for call in calls:
        if call.end_time and call.answered_time:
            call_duration = get_total_seconds(call.end_time - call.answered_time)
            if call_duration < 0:
                continue
            total_duration += call_duration
            duration_count += 1

    if duration_count != 0:
        avg_duration = get_ratio(total_duration, duration_count)

    return avg_duration


def get_callscount_avg_dur(ai_id, start_datetime, end_datetime):
    cdrs = get_calls(ai_id, start_datetime, end_datetime)        
    return cdrs.count(), get_avg_dur(cdrs)


def get_outgoing_incoming_cumulative_callstats(ai_ids, to_date):
    cdrs = Cdr.objects.filter(ai_id__in = ai_ids, start_time__lt = to_date)
    incoming_cdrs = cdrs.filter(is_incoming = True)
    incoming_callers = incoming_cdrs.values('callerid').distinct().count()
    outgoing_cdrs = cdrs.filter(is_incoming = False, answered_time__isnull = False)
    outgoing_callers = outgoing_cdrs.values('callerid').distinct().count()

    return incoming_cdrs.count(), incoming_callers, outgoing_cdrs.count(), outgoing_callers


def get_callstats_with_breakup_on_origin(source_ai_id, target_ai_id, start_date, end_date, output):
    data = [ ['Date', 'Calls', 'Callers', 'Avg Duration'] ]
    
    cdr_ids = Ai_vi_start_event.objects.filter(prev_ai_id = source_ai_id, next_ai_id = target_ai_id, \
                     trans_event__time__range = (start_date, end_date)).values_list('trans_event__cdr_id', \
                                                                            flat = True).distinct()
    cdrs = Cdr.objects.filter(id__in = cdr_ids)

    for date in daterange(start_date, end_date):
        start_datetime = datetime.combine(date, datetime.min.time())
        end_datetime = datetime.combine(date + timedelta(days = 1), datetime.min.time())
        calls = cdrs.filter(start_time__range = (start_datetime, end_datetime))
        callers_count, avg_duration = get_callerscount_avgdur(calls)
        data.append([ str(start_datetime.date()), calls.count(), callers_count, avg_duration ])

    generate_attachment(data, output)
    return output


def get_cdrs_from_transitions(source_ai_ids, target_ai_id, start_date = None, end_date = None):
    ai_vi_events = Ai_vi_start_event.objects.filter(prev_ai_id__in = source_ai_ids, next_ai_id = target_ai_id)
    if start_date and end_date:
        ai_vi_events = ai_vi_events.filter(trans_event__time__range = (start_date, end_date))

    cdr_ids = ai_vi_events.values_list('trans_event__cdr_id', flat = True).distinct()
    return Cdr.objects.filter(id__in = cdr_ids)


def get_cumulative_callers_count(ai_id, to_datetime):
    cursor = connection.cursor()
    query = "select count(distinct callerid) from app_manager_cdr C left outer join app_manager_transition_event T on C.id = T.cdr_id left outer join app_manager_ai_vi_start_event A on T.id = A.trans_event_id where (A.next_ai_id is null and T.ai_id is null and C.ai_id = %s) or (A.next_ai_id is null and T.ai_id = %s) or (A.next_ai_id = %s)" % (ai_id, ai_id, ai_id)
    cursor.execute(query)
    results = cursor.fetchall()
    return results[0][0]


def get_hourly_stats(ai_id, start, end):
    data = {}
    for day_of_week in range(1, 8):
        data[day_of_week] = ( [0] * 24 )

    for dt in daterange(start, end):
        for hour in range(24):
            start_hour = datetime.combine(dt, time(hour = hour))
            end_hour = start_hour + timedelta(hours = 1)
            calls, avg_dur = get_callscount_avg_dur(ai_id, start_hour, end_hour)
            data[dt.isoweekday()][hour] += calls

    return data


def get_calls_per_user_distrib(ai_id, callerids = None, calls_threshold = 1, start_date = None, end_date = None):
    data = []
    filters = {}

    if callerids:
        filters['callerid__in'] = callerids

    if start_date and end_date:
        filters['start_time__range'] = (start_date, end_date)

    cdrs = Cdr.objects.filter(ai_id = ai_id, is_incoming = False, answered_time__isnull = False, **filters)
    callerids = cdrs.values_list('callerid', flat = True)

    callers_count = collections.defaultdict( lambda: 0 )
    for callerid in callerids:
        callers_count[ callerid ] += 1
    for key, val in callers_count.iteritems():
        if val >= calls_threshold:
            data.append( { 'callerid': key, 'calls_count': val } )

    return data


def get_call_duration_distrib(ai_id, start_date = None, end_date = None):
    data = [ ['Call Duration', 'Count'] ]
    cdrs = Cdr.objects.filter(ai_id = ai_id, is_incoming = False, answered_time__isnull = False, end_time__isnull = False)
    if start_date and end_date:
        cdrs = cdrs.filter(start_time__range = (start_date, end_date))

    call_dur_count_map = {}
    for cdr in cdrs:
        duration = get_total_seconds(cdr.end_time - cdr.answered_time)
        call_dur_count_map[duration] = call_dur_count_map.get(duration, 0) + 1

    body = []
    for duration in call_dur_count_map:
        body.append( [duration, call_dur_count_map[duration]] )

    body = get_sorted_data(body, 1)
    data += body
    return data


def get_call_details( start_datetime, end_datetime, ai_id ):
    total_calls_till_yesterday =0
    total_duration_till_yesterday = 0

    ai = App_instance.objects.get( pk = ai_id )
    app_name = ai.app.name
    get_call_stats = get_py_obj( app_name + '.stats.get_call_stats' )
    yesterday = end_datetime.replace(hour = 0, minute = 0, second = 0, microsecond = 0) - timedelta( days = 1 )

    for date in daterange( start_datetime, yesterday ):
        start_date = datetime.combine( date, datetime.min.time() )
        end_date = datetime.combine( date + timedelta(days = 1), datetime.min.time() )
        call_stats = get_call_stats( ai_id, start_date, end_date )
        total_calls_till_yesterday += float(call_stats.calls)
        total_duration_till_yesterday += ( float(call_stats.calls) * float(call_stats.avg_dur) )

    return total_calls_till_yesterday, total_duration_till_yesterday


def generate_and_email_stats(project, recipients, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    stats = get_project_stats(project, start_datetime, end_datetime, stat_type, ai_stats_settings)
    subject = get_email_subject(project.name, start_datetime.date(), end_datetime.date(), stat_type)
    body = create_mail_content(stats)
    reports = [report for entry in stats for report in stats[entry]['reports']]
    send_email(subject, body, recipients, reports)


def get_project_stats(project, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    entity_map = {
                    'ais': [],
                    'custom': [],
                    'campaign': project.campaigns.all(),
                    'advert': project.ads.all()
                }

    stats = {}

    cumulative_data_required = True if require_project_custom_stats( project ) else False

    for ai in project.ais.filter(status = STATUS_ACTIVE):
        entity_map['custom'].append(ai) if require_ai_custom_stats(ai = ai) else entity_map['ais'].append(ai)

    for entry in entity_map:
        if entry == 'custom' and cumulative_data_required:
            stats[entry] = {}
            func = getattr(load_vapp_module('stats', 'stats'), 'get_%s_stats' % entry)
            (stats[entry]['data'], stats[entry]['reports']) = func(entity_map[entry], start_datetime, end_datetime, stat_type, project)
        elif entity_map[entry]:
            stats[entry] = {}
            func = getattr(load_vapp_module('stats', 'stats'), 'get_%s_stats' % entry)
            if entry == 'ais':
                (stats[entry]['data'], stats[entry]['reports']) = func(entity_map[entry], start_datetime, end_datetime, stat_type, ai_stats_settings)
            else:
                (stats[entry]['data'], stats[entry]['reports']) = func(entity_map[entry], start_datetime, end_datetime, stat_type)

    return stats


def get_ais_stats(ais, start_datetime, end_datetime, stat_type = None, ai_stats_settings = None):
    stats = {}
    reports = []
    app_ai_map = {}
    for ai in ais:
        app_ai_map.setdefault(ai.app, []).append(ai)
    for app, ais in app_ai_map.iteritems():
        pkg_name = (app.pkg_name).lower()
        stats_module = load_module(ais[0], 'stats')
        if hasattr(stats_module, 'get_stats'):
            (stats[pkg_name], attachments) = stats_module.get_stats(ais, start_datetime, end_datetime, stat_type, ai_stats_settings)
            reports.extend(attachments)

    return (stats, reports)


def get_campaign_stats(campaigns, start_datetime, end_datetime, stat_type = None):
    stats_module = load_vapp_module('campaign', 'stats')
    if hasattr(stats_module, 'get_stats'):
        (stats, reports) = stats_module.get_stats(campaigns, start_datetime, end_datetime, stat_type)
        return (stats, reports)


def get_advert_stats(ads, start_datetime, end_datetime, stat_type = None):
    stats_module = load_vapp_module('advert', 'stats')
    if hasattr(stats_module, 'get_stats'):
        (stats, reports) = stats_module.get_stats(ads, start_datetime, end_datetime, stat_type)
        return (stats, reports)


def get_custom_stats(ais, start_datetime, end_datetime, stat_type = None, project = None):
    stats_module = load_vapp_module('customization', 'stats')
    if hasattr(stats_module, 'get_stats'):
        (stats, reports) = stats_module.get_stats(ais, start_datetime, end_datetime, stat_type, project)
        return (stats, reports)


def get_email_subject(project_name, start_date, end_date, stat_type = None):
    if stat_type == Stats_type.DAILY:
        subject = '%s Statistics for %s' % (project_name, start_date)
    elif stat_type in [Stats_type.WEEKLY, Stats_type.MONTHLY]:
        subject = '%s %s Statistics for %s - %s' % (project_name, stat_type, start_date, end_date)
    else:
        subject = '%s Statistics for %s - %s' % (project_name, start_date, end_date)

    return subject


# Hack to provide ordering of apps in html format.
# Will revise this in a separate commit
DEFAULT_PKG_ORDER = ['app_selector', 'mnews', 'survey', 'customization', 'helpline',\
                     'case_manager', 'registration', 'vmiss', 'campaign', 'advert']


def create_mail_content(stats):
    body = ''

    pkg_data_map = get_pkg_data_map(stats)
    for pkg in DEFAULT_PKG_ORDER:
        if pkg_data_map.get(pkg, None):
            func = getattr(load_vapp_module(pkg, 'stats'), 'create_html_content')
            body += func(pkg_data_map[pkg])

    return body


def get_pkg_data_map(stats):
    pkg_data_map = {}
    for entry in stats:
        if entry == 'ais':
            for pkg, data in stats[entry]['data'].iteritems():
                pkg_data_map[pkg] = data
        elif entry == 'custom':
            pkg_data_map['customization'] = stats[entry]['data']
        else:
            pkg_data_map[entry] = stats[entry]['data']

    return pkg_data_map


def require_project_custom_stats( project ):
    return get_model('customization', 'Project_custom_stats_settings').objects.filter(project = project).exists()


def require_ai_custom_stats( ai ):
    return get_model('customization', 'Custom_stats_settings').objects.filter(ai = ai).exists()


def get_ai_call_stats(ai, start_datetime, end_datetime):
    stats = Stats()
    stats.ai_name = ai.name
    stats.call_details = get_calls(ai.id, start_datetime, end_datetime)
    stats.callers = len(get_callers_of_calls(stats.call_details))
    stats.calls = stats.call_details.count()

    stats.avg_call_dur = 0.0
    total_dur = 0.0
    count = 0
    for call in stats.call_details:
        duration = get_time_spent_on_ai(call.id, ai.id)
        if duration > 0:
            total_dur += duration
            count += 1

    if count > 0 and total_dur > 0:
        stats.avg_call_dur = (total_dur / count)
    else:
        stats.avg_call_dur = 0.0
    return stats

def generate_dropped_callers_stats( end_datetime ):
    data = [ [ 'Date', 'Incoming Calls', 'Callout Calls', 'Callback Calls', 'Union (Incoming | Callout Callers)', \
               'Incoming Callers', 'Callout Callers', 'Callback Callers', 'Union (Callout | Callback Callers)' ] ]

    cdrs = Cdr.objects.filter( start_time__lt = end_datetime )
    incoming_cdrs = cdrs.filter( is_incoming = True )
    incoming_callers = set( incoming_cdrs.values_list( 'callerid', flat = True ) )
    outgoing_cdrs = cdrs.filter( is_incoming = False, answered_time__isnull = False )
    callout_cdrs = outgoing_cdrs.filter( trigger = 1 )
    callout_callers = set( callout_cdrs.values_list( 'callerid', flat = True ) )
    callback_cdrs = outgoing_cdrs.filter( trigger = 0 )
    callback_callers = set( callback_cdrs.values_list( 'callerid', flat = True ) )

    data.append( [ str( end_datetime.date() ), incoming_cdrs.count(), callout_cdrs.count(), callback_cdrs.count(), \
                   len( incoming_callers | callout_callers ), len( incoming_callers ), len( callout_callers ), \
                   len( callback_callers ), len( callout_callers | callback_callers ) ] )
    return data

def generate_dropped_callers_cumulative_report( end_datetime, email_ids, output_file ):
    data = generate_dropped_callers_stats( end_datetime )
    generate_attachment( data, output_file )
    send_email( 'Dropped Caller Cumulative Stats Report', 'Reports', email_ids, [ output_file ] )

def get_cumulative_call_stats_report( ai_ids, end_datetime, email_ids, output_file ):
    data = [ [ 'Date', 'Incoming Calls', 'Incoming Callers', 'Outgoing Calls', 'Outgoing Callers' ] ]
    in_calls, in_callers, out_calls, out_callers = get_outgoing_incoming_cumulative_callstats( ai_ids, end_datetime )
    data.append( [ str( end_datetime.date() ), in_calls, in_callers, out_calls, out_callers ] )
    generate_attachment( data, output_file )
    send_email( 'Cumulative Call Stats for ai_ids - {0}'.format( ai_ids ), 'Reports', email_ids, [ output_file ] )

def get_call_caller_traction(ai_id, start_datetime, end_datetime, callerids):
    data = [['No. of Calls', str(start_datetime.date()), 'CF (%)', \
             'CCDF (%)']]

    cdrs = Cdr.objects.filter(start_time__range=(start_datetime, \
                                                 end_datetime),
                              is_incoming=False, answered_time__isnull=False,
                              trigger=0, callerid__in=callerids)
    if ai_id is not None:
        cdrs = cdrs.filter(ai_id=ai_id)

    qs = cdrs.values('callerid').annotate(callcount=Count('callerid'))
    total_callers = qs.count()
    cumulative_caller_sum = 0
    cf = 0
    ccdf = 100

    for i in range(1, 1001):
        callers_count = qs.filter(callcount=i).count()
        if callers_count > 0:
            cumulative_caller_sum += callers_count
            cf = get_ratio(cumulative_caller_sum, total_callers) * 100
            ccdf = 100 - cf

        data.append([i, callers_count, cf, ccdf])

    return data

def generate_call_caller_traction_report(ai_id, start_datetime, end_datetime, \
                                         email_ids, output_file):
    new_caller_data = {
        'name': 'New Callers',
        'header_row_count': 1,
        'data': []
    }
    old_caller_data = {
        'name': 'Old Callers',
        'header_row_count': 1,
        'data': []
    }

    curr_month_callers = get_callers_for_ai_ids([ai_id] if ai_id else None, \
                            start_datetime, end_datetime)
    new_callers = get_new_callers_from_callers(curr_month_callers, \
                        [ai_id] if ai_id else None, start_datetime)
    old_callers = curr_month_callers - new_callers

    cdrs = Cdr.objects.filter(start_time__range=(end_datetime, \
                    end_datetime + relativedelta(months =+1)),
                    is_incoming=False, answered_time__isnull=False,
                    trigger=0, callerid__in=new_callers)
    if ai_id:
        cdrs = cdrs.filter(ai_id=ai_id)

    dropped_callers = new_callers - set(cdrs.values_list('callerid', flat=True))
    new_caller_data['data'] = get_call_caller_traction(ai_id, start_datetime, \
                                    end_datetime, dropped_callers)
    old_caller_data['data'] = get_call_caller_traction(ai_id, start_datetime, \
                                    end_datetime, old_callers)
    generate_workbook([new_caller_data, old_caller_data], output_file)
    send_email('Calls per caller distribution: ai_id - {0}'.format(ai_id), \
               'Reports', email_ids, [output_file])

def generate_contribs_per_user_distribution( ai_id, start_datetime, end_datetime ):
    data = [ [ 'No. of Contributions', str( start_datetime.date() ), 'CF (%)', 'CCDF (%)' ] ]

    news = News.objects.filter(ai_id=ai_id,
                               time__range=(start_datetime, end_datetime),
                               state__in=[News_state.PUB, News_state.ARC],
                               source=News.Source.VOICE_INTERFACE)
    qs = news.values( 'callerid' ).annotate( contribcount = Count( 'callerid' ) )
    total_contributors = qs.count()

    cumulative_contributor_sum = 0
    cf = 0
    ccdf = 100
    for i in range( 1, 501 ):
        contributor_count = qs.filter( contribcount = i ).count()
        if contributor_count > 0:
            cumulative_contributor_sum += contributor_count
            cf = get_ratio( cumulative_contributor_sum, total_contributors ) * 100
            ccdf = 100 - cf

        data.append( [ i, contributor_count, cf, ccdf ] )

    return data

def generate_contribs_per_user_distribution_report( ai_id, start_datetime, end_datetime, email_ids, output_file ):
    data = generate_contribs_per_user_distribution( ai_id, start_datetime, end_datetime )
    generate_attachment( data, output_file )
    send_email( 'Contributions per user distribution for ai_id - {0}'.format( ai_id ), 'Reports', email_ids, [ output_file ] )

def generate_call_caller_distribution( ai_id, start_datetime, end_datetime ):
    cdrs = Cdr.objects.filter( ai__id = ai_id, start_time__range = ( start_datetime, end_datetime ) )
    in_cdrs = cdrs.filter( is_incoming = True )
    in_calls = in_cdrs.count()
    in_callers = len( in_cdrs.values_list( 'callerid', flat = True ).distinct() )
    cb_cdrs = cdrs.filter( is_incoming = False, answered_time__isnull = False, trigger = 0 )
    cb_calls = cb_cdrs.count()
    cb_callers = len( cb_cdrs.values_list( 'callerid', flat = True ).distinct() )
    return ( in_calls, in_callers, cb_calls, cb_callers )

def generate_call_distribution_report( ai_ids, start_datetime, end_datetime, email_ids, output_file ):
    data = [ [ 'Instance', 'Date', 'Incoming calls', 'Incoming callers', 'Callback calls', 'Callback callers' ] ]

    for ai_id in ai_ids:
        ( in_calls, in_callers, cb_calls, cb_callers ) = generate_call_caller_distribution( ai_id, start_datetime, end_datetime )
        ai_name = App_instance.objects.get( pk = ai_id ).name
        data.append( [ ai_name, str( start_datetime.date() ), in_calls, in_callers, cb_calls, cb_callers ] )

    generate_attachment( data, output_file )
    send_email( 'Call Distribution Report for ai_ids - {0}'.format( ai_ids ), 'Report', email_ids, [ output_file ] )

def get_callers_for_ai_ids(ai_ids, start_datetime, end_datetime):
    cdr_qs = Cdr.objects.filter(start_time__range=(start_datetime, \
                                                   end_datetime),
                                is_incoming=False,
                                answered_time__isnull=False, trigger=0)
    if ai_ids:
        cdr_qs = cdr_qs.filter(ai_id__in=ai_ids)
    return set(cdr_qs.values_list('callerid', flat=True))

def get_new_callers( ai_ids, start_datetime, end_datetime ):
    new_callers = set()
    cdrs = Cdr.objects.filter( ai__id__in = ai_ids, is_incoming = False, answered_time__isnull = False, trigger = 0 )
    callerids = cdrs.filter( start_time__range = ( start_datetime, end_datetime ) ).values_list( 'callerid', flat = True ).distinct()
    prev_cdrs = cdrs.filter( start_time__lt = start_datetime )
    for callerid in callerids:
        if not prev_cdrs.filter( callerid = callerid ).exists():
            new_callers.add( callerid )

    return new_callers

def get_callers_with_no_calls_since_datetime(ai_ids, new_callers, start_datetime):
    cdrs = Cdr.objects.filter(ai__id__in=ai_ids,
                              start_time__gte=start_datetime,
                              is_incoming=False,
                              answered_time__isnull=False,
                              trigger=0)
    callers_curr_month_till_today = list(cdrs.values_list('callerid', flat=True))
    callers = [each for each in new_callers if callers_curr_month_till_today.count(each) == 1]
    return callers

def get_new_callers_from_callers(callerids, ai_ids, start_datetime):
    callers = set()
    for callerid in callerids:
        cdr_qs = Cdr.objects.filter(callerid=callerid, is_incoming=False,
                                    start_time__lt=start_datetime,
                                    answered_time__isnull=False, trigger=0)
        if ai_ids:
            cdr_qs = cdr_qs.filter(ai_id__in=ai_ids)

        if not cdr_qs.exists():
            callers.add(callerid)

    return callers

def get_callers_atleast_ncalls( ai_ids, callerids, start_datetime, end_datetime, n ):
    callers = set()
    for callerid in callerids:
        cdrs = Cdr.objects.filter( start_time__range = ( start_datetime, end_datetime ), callerid = callerid, is_incoming = False, answered_time__isnull = False, trigger = 0 )
        call_count = 0
        for ai_id in ai_ids:
            call_count += cdrs.filter( ai_id = ai_id ).count()

        if call_count >= n:
            callers.add( callerid )

    return callers

def get_callers_avg_call_duration( callerids, ai_ids, start_datetime, end_datetime ):
    stats = Caller_call_stats.objects.filter( ai__id__in = ai_ids, callerid__in = callerids, call_date__range = ( start_datetime, end_datetime ) )
    calls_count = stats.aggregate( Sum( 'calls' ) ).get( 'calls__sum' ) or 0
    duration = stats.aggregate( Sum( 'duration_of_calls' ) ).get( 'duration_of_calls__sum' ) or 0.0
    return get_ratio( duration, calls_count )

# DAU: Daily Average Users
# MAU: Monthly Average Users
def get_callers_dau_mau_avg_call_dur(callers, days_count, ai_ids, start_datetime, end_datetime):
    dau = get_daily_avg_users(ai_ids, start_datetime, days_count, callers)
    mau = len(callers)
    avg_duration = get_callers_avg_call_duration(callers, ai_ids, start_datetime, end_datetime)
    return { 'dau': dau, 'mau': mau, 'dau_mau_ratio': get_ratio( dau, mau ), 'avg_duration': avg_duration }

def get_callers_n_months_ago(n, start_datetime, ai_ids):
    nth_month_start_datetime = start_datetime + relativedelta(months=-n)
    nth_month_end_datetime = nth_month_start_datetime + relativedelta(months =+1)
    return get_callers_for_ai_ids(ai_ids, nth_month_start_datetime, nth_month_end_datetime)

def get_new_callers_call_results(ai_ids, start_datetime, end_datetime):
    results = {
        'new_caller_count': 0,
        'no_new_calls': 0,
        'called_till_one_month_later': 0,
        'called_till_two_months_later': 0,
        'called_till_three_months_later': 0,
    }
    new_callers = get_new_callers(ai_ids, start_datetime, end_datetime)
    results['new_caller_count'] = len(new_callers)
    results['no_new_calls'] = len(get_callers_with_no_calls_since_datetime(ai_ids, new_callers, start_datetime))

    two_months_later_start_datetime = end_datetime
    two_months_later_end_datetime = two_months_later_start_datetime + relativedelta(months=+1)
    three_months_later_start_datetime = two_months_later_end_datetime
    three_months_later_end_datetime = three_months_later_start_datetime + relativedelta(months=+1)
    four_months_later_start_datetime = three_months_later_end_datetime
    four_months_later_end_datetime = four_months_later_start_datetime + relativedelta(months=+1)

    cdrs = Cdr.objects.filter(ai__id__in=ai_ids,
                              start_time__range=(two_months_later_start_datetime, four_months_later_end_datetime),
                              is_incoming=False,
                              answered_time__isnull=False,
                              trigger=0)

    for callerid in new_callers:
        cdrs_for_caller = cdrs.filter(callerid=callerid)
        if cdrs_for_caller.filter(start_time__range=(two_months_later_start_datetime, two_months_later_end_datetime)).exists():
            if not cdrs_for_caller.filter(start_time__range=(three_months_later_start_datetime, three_months_later_end_datetime)).exists():
                results['called_till_two_months_later'] += 1
            else:
                if not cdrs_for_caller.filter(start_time__range=(four_months_later_start_datetime, four_months_later_end_datetime)).exists():
                    results['called_till_three_months_later'] += 1
        else:
            results['called_till_one_month_later'] += 1

    return results

def format_stickiness_data( start_datetime, new_callers_call_results, caller_group_data ):
    data = []

    data.append( [ 'Stickiness statistics', str( start_datetime.strftime( "%B, %Y" ) ) ] )
    data.append( EMPTY_ROW )
    data.append( [ 'User set 1(those who called more than once that month)' ] )
    data.append( [ 'DAU', str( caller_group_data[ 'group_one' ][ 'dau' ] ) ] )
    data.append( [ 'MAU', str( caller_group_data[ 'group_one' ][ 'mau' ] ) ] )
    data.append( [ 'DAU/MAU', str( caller_group_data[ 'group_one' ][ 'dau_mau_ratio' ] ) ] )
    data.append( EMPTY_ROW )
    data.append( [ 'User set 2(those who called that month and the last month)' ] )
    data.append( [ 'DAU', str( caller_group_data[ 'group_two' ][ 'dau' ] ) ] )
    data.append( [ 'MAU', str( caller_group_data[ 'group_two' ][ 'mau' ] ) ] )
    data.append( [ 'DAU/MAU', str( caller_group_data[ 'group_two' ][ 'dau_mau_ratio' ] ) ] )
    data.append( EMPTY_ROW )
    data.append( [ 'User set 3(those who called that month and in the last two months)' ] )
    data.append( [ 'DAU', str( caller_group_data[ 'group_three' ][ 'dau' ] ) ] )
    data.append( [ 'MAU', str( caller_group_data[ 'group_three' ][ 'mau' ] ) ] )
    data.append( [ 'DAU/MAU', str( caller_group_data[ 'group_three' ][ 'dau_mau_ratio' ] ) ] )
    data.append( EMPTY_ROW )
    data.append( [ 'Average Call Duration' ] )
    data.append( [ 'New users', str( caller_group_data[ 'new_callers' ][ 'avg_duration' ] ) ] )
    data.append( [ 'User set 1(those who called more than once that month)', str( caller_group_data[ 'group_one' ][ 'avg_duration' ] ) ] )
    data.append( [ 'User set 2(those who called that month and the last month)', str( caller_group_data[ 'group_two' ][ 'avg_duration' ] ) ] )
    data.append( [ 'User set 3(those who called that month and in the last two months)', str( caller_group_data[ 'group_three' ][ 'avg_duration' ] ) ] )
    data.append( EMPTY_ROW )

    new_users_row = [ 'Number of new users added' ]
    new_users_never_called_row = [ 'New users who never called again' ]
    new_users_called_one_month_later = [ 'New users who called in their 1st month but not in their 2nd month' ]
    new_users_called_two_months_later = [ 'New users who called in their 1st, 2nd month but not in their 3rd month' ]
    new_users_called_three_months_later = [ 'New users who called in their 1st, 2nd, 3rd month but not in their 4th month' ]

    row = [ 'Month' ]
    month_count = 4
    for i in range( 3, -1, -1 ):
        month_start_datetime_str = ( start_datetime + relativedelta( months = -i ) ).strftime( "%B, %Y" )
        call_data = new_callers_call_results[ month_start_datetime_str ]
        row.append( month_start_datetime_str )
        new_users_row.append( call_data[ 'new_caller_count' ] )
        new_users_never_called_row.append( call_data[ 'no_new_calls' ] )
        new_users_called_one_month_later.append( call_data[ 'called_till_one_month_later' ] )
        new_users_called_two_months_later.append( call_data[ 'called_till_two_months_later' ] )
        new_users_called_three_months_later.append( call_data[ 'called_till_three_months_later' ] )

    data.append( row )
    data.append( EMPTY_ROW )
    data.append( new_users_row )
    data.append( EMPTY_ROW )
    data.append( new_users_never_called_row )
    data.append( EMPTY_ROW )
    data.append( new_users_called_one_month_later )
    data.append( EMPTY_ROW )
    data.append( new_users_called_two_months_later )
    data.append( EMPTY_ROW )
    data.append( new_users_called_three_months_later )
    return data

def generate_stickiness_stats( ai_ids, start_datetime, end_datetime ):
    caller_group_data = {}

    curr_month_callers = get_callers_for_ai_ids( ai_ids, start_datetime, end_datetime )
    new_callers = get_new_callers_from_callers( curr_month_callers, ai_ids, start_datetime )
    callers_one_month_ago = get_callers_n_months_ago( 1, start_datetime, ai_ids )
    callers_two_months_ago = get_callers_n_months_ago( 2, start_datetime, ai_ids )

    # Group 1: Current month callers who called atleast twice in this month
    group_one_callers = get_callers_atleast_ncalls( ai_ids, curr_month_callers, start_datetime, end_datetime, 2 )
    days_count = ( end_datetime - start_datetime ).days
    caller_group_data[ 'group_one' ] = get_callers_dau_mau_avg_call_dur( group_one_callers, days_count, ai_ids, start_datetime, end_datetime )

    # Group 2: Current month callers who called in previous month
    group_two_callers = curr_month_callers.intersection( callers_one_month_ago )
    days_count = ( end_datetime - ( start_datetime + relativedelta( months = -1 ) ) ).days
    caller_group_data[ 'group_two' ] = get_callers_dau_mau_avg_call_dur( group_two_callers, days_count, ai_ids, start_datetime, end_datetime )

    # Group 3: Current month callers who called in previous month and previous-to-previous month
    group_three_callers = curr_month_callers.intersection( callers_one_month_ago ).intersection( callers_two_months_ago )
    days_count = ( end_datetime - ( start_datetime + relativedelta( months = -2 ) ) ).days
    caller_group_data[ 'group_three' ] = get_callers_dau_mau_avg_call_dur( group_three_callers, days_count, ai_ids, start_datetime, end_datetime )

    caller_group_data[ 'new_callers' ] = {}
    caller_group_data[ 'new_callers' ][ 'avg_duration' ] = get_callers_avg_call_duration( new_callers, ai_ids, start_datetime, end_datetime )

    new_callers_call_results = {}
    month_count = 4
    for i in range( 3, -1, -1 ):
        end_datetime = start_datetime + relativedelta( months = -i )
        new_callers_call_results[ end_datetime.strftime( "%B, %Y" ) ] = get_new_callers_call_results( ai_ids, end_datetime, end_datetime + relativedelta( months = +1 ) )

    data = format_stickiness_data( start_datetime, new_callers_call_results, caller_group_data )
    return data

def generate_stickiness_stats_report( ai_ids, start_datetime, end_datetime, email_ids, output_file ):
    data = generate_stickiness_stats( ai_ids, start_datetime, end_datetime )
    generate_attachment( data, output_file )
    send_email( 'Stickiness Stats Report for ai_ids - {0}'.format( ai_ids ), 'Reports', email_ids, [ output_file ] )

def get_trans_event_cdrs( ai_ids, start_datetime, end_datetime ):
    if not isinstance( ai_ids, tuple ):
        ai_ids = tuple( ai_ids )
    if len( ai_ids ) == 1:
        query = "select C.id from app_manager_cdr C, app_manager_transition_event T where cdr_id = C.id and T.ai_id = %s and C.start_time >= '%s' and C.start_time < '%s' and C.trigger = 0 and answered_time is not null" % ( ai_ids[0], start_datetime, end_datetime )
    else:
        query = "select C.id from app_manager_cdr C, app_manager_transition_event T where cdr_id = C.id and T.ai_id in %s and C.start_time >= '%s' and C.start_time < '%s' and C.trigger = 0 and answered_time is not null" % ( ai_ids, start_datetime, end_datetime )

    cdrids = Cdr.objects.raw( query )
    cdrs = Cdr.objects.filter( id__in = [ c.id for c in cdrids ], is_incoming = False )
    return cdrs

def generate_call_stats( ai_ids, start_datetime, end_datetime ):
    cdrs = get_trans_event_cdrs( ai_ids, start_datetime, end_datetime )
    callers_count, avg_duration = get_callerscount_avgdur( cdrs )
    return ( cdrs.count(), callers_count, avg_duration )

def generate_call_stats_distribution( ai_ids, start_datetime, end_datetime ):
    data = [ [ 'Date', 'Calls', 'Callers', 'Avg Call Duration' ] ]
    daily_stats = { 'calls': 0, 'callers': 0, 'avg_dur': 0 }
    days = ( end_datetime - start_datetime ).days

    for dt in daterange( start_datetime, end_datetime ):
        to_datetime = dt + timedelta( days = 1 )
        ( calls, callers, avg_dur ) = generate_call_stats( ai_ids, dt, to_datetime )
        data.append( [ dt.date(), calls, callers, avg_dur ] )
        daily_stats[ 'calls' ] += calls
        daily_stats[ 'callers' ] += callers
        daily_stats[ 'avg_dur' ] += avg_dur

    ( month_calls, month_callers, month_avg_dur ) = generate_call_stats( ai_ids, start_datetime, end_datetime )
    daily_avg_stats = {}
    for key, val in daily_stats.items():
        daily_avg_stats[ key ] = get_ratio( val, days )

    data.extend( [ EMPTY_ROW, EMPTY_ROW ] )
    data.append( [ 'Daily Average', daily_avg_stats[ 'calls' ], daily_avg_stats[ 'callers' ], daily_avg_stats[ 'avg_dur' ] ] )
    data.append( [ 'Total', month_calls, month_callers, month_avg_dur ] )
    return data

def generate_call_stats_report( ai_ids, start_datetime, end_datetime, email_ids, output_file ):
    data = generate_call_stats_distribution( ai_ids, start_datetime, end_datetime )
    generate_attachment( data, output_file )
    send_email( 'Daily Call Stats Report for ai_ids - {0}'.format( ai_ids ), 'Reports', email_ids, [ output_file ] )

def new_callers_distrib( ai_id, start_datetime, end_datetime ):
    calls = get_calls( ai_id, start_datetime, end_datetime )
    callerids = get_callers_of_calls( calls )
    new_callers = 0
    for callerid in callerids:
        cdrs = Cdr.objects.filter( callerid = callerid, answered_time__isnull = False, is_incoming = False )
        if cdrs.exists() and cdrs[ 0 ].ai.id == ai_id:
            new_callers += 1

    return ( len( callerids) , new_callers )

def generate_cumulative_new_caller_report( ai_id, end_datetime, email_ids, output_file ):
    data = [ [ 'End Date', 'Club Name', 'Callers', 'New Callers' ] ]
    ( callers, new_callers ) = new_callers_distrib( ai_id, CLUB_START_DATE, end_datetime )
    club_name = App_instance.objects.get( pk = ai_id ).name
    data.append( [ str( end_datetime.date() ), club_name, callers, new_callers ] )
    generate_attachment( data, output_file )
    send_email( 'Cumulative Callers and New Caller Report for ai_id - {0}'.format( ai_id ), 'Reports', email_ids, [ output_file ] )

def get_callback_success_percent( success_callback_count, callback_tried ):
    return get_ratio( success_callback_count, callback_tried ) * 100

def get_callback_health_stats(ai, cdrs):
    query_execution_start_time = datetime.now()
    filtered_cdrs = cdrs.filter(ai_id=ai.id) if ai else cdrs
    callback_tried = filtered_cdrs.filter(is_incoming=False, trigger=0)
    success_callback_count = callback_tried.filter(answered_time__isnull=False).count()
    in_cdrs = filtered_cdrs.filter(is_incoming=True)
    in_cdr_ids = in_cdrs.values_list('id', flat=True)
    stats = Stats()
    stats.instance = ai.name if ai else 'Complete'
    stats.incoming = in_cdrs.count()
    stats.callback_tried = callback_tried.count()
    stats.rejected = Caller_filter_log.objects.filter(cdr__id__in=in_cdr_ids).count()
    stats.delayed = Callback_delay_log.objects.filter(callback_info__call_in_cdr__id__in=in_cdr_ids).count()
    stats.success_callback_count = success_callback_count
    stats.success_callback_pc = get_callback_success_percent(success_callback_count + stats.rejected + stats.delayed, in_cdrs.count())
    query_execution_end_time = datetime.now()
    stats.query_execution_time = get_total_seconds(query_execution_end_time - query_execution_start_time)
    return stats

def get_callback_health_summary( ais, start_datetime, end_datetime ):
    cdrs = Cdr.objects.filter( start_time__range = ( start_datetime, end_datetime ) )
    logger.info('All cdrs extracted')
    stats_summary = [ get_callback_health_stats( ai, cdrs ) for ai in ais ]
    stats_summary.append( get_callback_health_stats( None, cdrs ) )
    return stats_summary

def get_transient_group_calls(group_schedule_id, start_datetime, end_datetime):
    gcl = Groups_call_log.objects.filter(group_schedule__id = group_schedule_id)
    if start_datetime and end_datetime:
        gcl = gcl.filter(last_cdr__start_time__range = (start_datetime, end_datetime))

    return gcl

def populate_caller_call_stats(start_datetime, end_datetime, force_update = False):
    cdrs = Cdr.objects.filter(start_time__range = (start_datetime, end_datetime), is_incoming = False, answered_time__isnull = False, trigger = 0)
    for dt in daterange(start_datetime, end_datetime):
        to_dt = dt + timedelta(days = 1)
        cdrs_for_duration = cdrs.filter(start_time__range = (dt, to_dt))
        for each in cdrs_for_duration.values('ai_id', 'callerid').distinct():
            caller_call_stats_qs = Caller_call_stats.objects.filter(callerid = each['callerid'], call_date = dt, ai_id = each['ai_id'])
            if caller_call_stats_qs.exists() and not force_update:
                continue
            elif caller_call_stats_qs.exists() and force_update:
                stats = caller_call_stats_qs[0]
            else:
                stats = Caller_call_stats(callerid = each['callerid'], call_date = dt, ai_id = each['ai_id'])

            cdrs_for_caller = cdrs_for_duration.filter(callerid = each['callerid'], ai_id = each['ai_id'])
            stats.calls = cdrs_for_caller.count()
            stats.duration_of_calls = str(get_total_duration_of_calls(cdrs_for_caller))
            stats.save()

def get_total_dialout_call_duration(gcl):
    call_duration = 0.0
    for each in gcl:
        if each.success:
            call_duration += get_call_duration(each.last_cdr)

    return call_duration

def get_dialout_details(gcl_id):
    gcl = Groups_call_log.objects.get(pk = gcl_id)
    result = model_to_dict(gcl)
    result['status'] = get_dialout_status(gcl)
    return result

def get_schedule_summary(schedule_id):
    gcl = get_transient_group_calls(schedule_id, None, None)
    duration = get_total_dialout_call_duration(gcl)
    result = {}
    result['id'] = schedule_id
    result['status'] = gcl[0].group_schedule.is_call_valid()
    result['calls_attempted'] = gcl.count()
    result['calls_answered'] = gcl.filter(success = True).count()
    result['calls_unanswered'] = gcl.filter(success = False).count()
    result['avg_duration'] = get_ratio(duration, result['calls_answered'])
    return result

def get_daily_avg_users(ai_ids, start_datetime, days_count, monthly_callers):
    usr_count = 0
    cdrs = Cdr.objects.filter(ai_id__in=ai_ids, is_incoming=False, answered_time__isnull=False, trigger=0, callerid__in=monthly_callers)
    for day in range(days_count):
        start_date = start_datetime + relativedelta(days=day)
        end_date = start_datetime + relativedelta(days=day + 1)
        cdr_time_filtered = cdrs.filter(start_time__range=(start_date, end_date))
        usr_count += cdr_time_filtered.values_list('callerid', flat=True).distinct().count()

    return get_ratio(usr_count, days_count)
