from django.db import models
from django.contrib.auth.models import User
from vapp.app_manager.models import Application, App_instance, CalleridField
from schedule.models import Schedule


class Stats:
    pass

class Stats_type():
    DAILY   = 'Daily'
    WEEKLY  = 'Weekly'
    MONTHLY = 'Monthly'


class StatsView(models.Model):
    name = models.CharField(max_length = 64)
    description = models.TextField()
    view_uri = models.CharField(max_length = 128)
    apps = models.ManyToManyField(Application, null = True, blank = True)
    users = models.ManyToManyField(User, null = True, blank = True)
    for_all_users = models.BooleanField(default = False)

    def __unicode__(self):
        return '%s' % (self.name)


class Cumulative_call_stats(models.Model):
    ai = models.ForeignKey(App_instance, related_name = 'cumulative_stats_ai_set')
    to_time = models.DateTimeField()
    calls = models.PositiveIntegerField()
    callers = models.PositiveIntegerField()
    avg_dur = models.DecimalField(max_digits = 10, decimal_places = 2)

class Caller_call_stats( models.Model ):
    ai                = models.ForeignKey( App_instance )
    call_date         = models.DateField()
    callerid          = CalleridField()
    calls             = models.PositiveIntegerField()
    duration_of_calls = models.DecimalField( max_digits = 10, decimal_places = 2 )

class Ai_stats_setting(models.Model):
    ai = models.ForeignKey(App_instance)
    schedule = models.ForeignKey(Schedule)
    generate_workbook = models.BooleanField(default = True)
