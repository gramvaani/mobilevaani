from django.contrib import admin
from models import *

class CampaignAdmin(admin.ModelAdmin):
    filter_horizontal = ('ais', 'images')

admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Campaign_category)

class CampaignStatsAdmin(admin.ModelAdmin):
    filter_horizontal = ('campaigns',)
admin.site.register(Campaign_stats_user_map, CampaignStatsAdmin)