from models import Campaign, Campaign_category, Campaign_stats_user_map, Campaign_statistics

from vapp.app_manager.models import App_instance

from vapp.stats.models import Stats, Stats_type
from callerinfo.models import Contact
from vapp.mnews.models import Item_heard_stats, News_state, News
from vapp.mnews.stats import get_item_listening_stats, get_avg_listening_duration, dump_data, \
get_items_heard_stats, get_items_listeners_count, get_item_bookmark_forward_count

from vapp.utils import generate_workbook, send_email, daterange, LINE_BREAK, is_start_of_month, load_vapp_module, get_formated_datetime
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
from django.template.loader import render_to_string
from django.db.models import Sum

from log import get_logger
logger = get_logger()


CONTENT_LABELS = {
                    'item_id': 'Item ID',
                    'ai_id': 'Instance',
                    'item_title': 'Item Title',
                    'qualifier': 'Qualifier',
                    'state': 'State',
                    'district': 'District',
                    'block': 'Block',
                    'tags': 'Tags',
                    'campaign_categories': 'Campaign Categories',
                    'categories': 'Categories',
                    'item_creation_time': 'Item Creation Time',
                    'item_duration': 'Item Duration',
                    'item_heard_count': '# of Times Heard',
                    'total_dur_heard': 'Total Duration Heard',
                    'listeners': '# of Listeners',
                    'item_url': 'Audio Link',
                    'callerid': 'Contributor',
                    'contributor_name':'Contributor Name',
                    'transcription':'Transcription',
                    'rating':'Rating',
                    'times_bookmark':'Times Bookmarked',
                    'times_forward':'Times Forward',
                    'item_published_time':'ITEM Published Time',
                    'time_taken':'Time taken to publish item',
                    'channel':'Channel'

                 }


def populate_stats(start_datetime = None, end_datetime = None, force_update = False):
    if not (start_datetime and end_datetime):
        end_datetime = datetime.now().replace(hour = 0, minute = 0, second = 0, microsecond = 0)
        start_datetime = end_datetime - timedelta(days = 1)

    for campaign in Campaign.get_active_campaigns(end_datetime):
        populate_campaign_stats(campaign, start_datetime, end_datetime, force_update)


def populate_campaign_stats(campaign, start_datetime, end_datetime, force_update=False):
    campaign_news = get_campaign_items(campaign)
    for dt in daterange(start_datetime, end_datetime):
        end = dt + timedelta(days=1)
        prev_stats = Campaign_statistics.objects.filter(campaign=campaign,
                                                        start_date=dt,
                                                        end_date=end)
        if prev_stats.exists():
            if force_update:
                ugc_contribs, ugc_items_heard, ugc_duration_heard = get_campaign_stats(campaign,
                                                                        dt, end, News.ContentType.UGC)
                ihc_items_heard, ihc_duration_heard = get_campaign_stats(campaign,
                                                            dt, end, News.ContentType.IHC)
                users_count = get_items_listeners_count(campaign_news, dt, end)
                prev_stats.update(ugc_contribs=ugc_contribs,
                                  ugc_items_heard=ugc_items_heard,
                                  ugc_duration_heard=str(ugc_duration_heard),
                                  ihc_items_heard=ihc_items_heard,
                                  ihc_duration_heard=str(ihc_duration_heard),
                                  users_count=users_count)
            else:
                info_str = "Campaign stats for {} already present. Skipping recalculation."
                logger.info(info_str.format(campaign.name))
        else:
            ugc_contribs, ugc_items_heard, ugc_duration_heard = get_campaign_stats(campaign,
                                                                    dt, end, News.ContentType.UGC)
            ihc_items_heard, ihc_duration_heard = get_campaign_stats(campaign,
                                                        dt, end, News.ContentType.IHC)
            users_count = get_items_listeners_count(campaign_news, dt, end)
            stat = Campaign_statistics(campaign=campaign, start_date=dt,
                                       end_date=end, ugc_contribs=ugc_contribs,
                                       ugc_items_heard=ugc_items_heard,
                                       ugc_duration_heard=str(ugc_duration_heard),
                                       ihc_items_heard=ihc_items_heard,
                                       ihc_duration_heard=str(ihc_duration_heard),
                                       users_count=users_count)
            stat.save()


def get_campaign_stats(campaign, start_datetime, end_datetime, content_type):
    contribs = get_campaign_items(campaign, content_type, start_datetime, end_datetime)
    heard_stats = get_items_heard_stats(contribs, start_datetime, end_datetime)
    items_heard = heard_stats.count()
    items_duration_heard = heard_stats.aggregate(Sum('duration')).get('duration__sum', 0.0) if heard_stats else 0.0
    if content_type == News.ContentType.UGC:
        return contribs.count(), items_heard, items_duration_heard
    elif content_type == News.ContentType.IHC:
        return items_heard, items_duration_heard

def generate_legends_attachment():
    message = """
    <html>
        <body>
            <div>Yesterday Items Heard, Total Items Heard indicates number of times campaign items (in-house and user generated) were heard.</div>
            <div>Total Average Listening Duration carry same meaning as implied by the name.</div>
            <div>Yesterday Contributions, Total Contributions indicates user generated contributions.</div>
        </body>
    </html>
    """
    output_file_loc = '/tmp/Campaign_legends.html'
    output_file = open(output_file_loc, 'w')
    output_file.write(message)
    output_file.close()
    return output_file_loc


def get_mail_content(campaigns, start_datetime, end_datetime):
    data = []
    for campaign in campaigns:
        stats = Stats()
        stats.campaign_name = campaign.name
        campaign_items = get_campaign_items(campaign)
        ihs = get_items_heard_stats(campaign_items)

        stats.times_items_heard = ihs.filter(when_heard__range=\
                                             (start_datetime,
                                              end_datetime)).count()
        stats.total_times_items_heard = ihs.count()
        stats.total_avg_dur = get_avg_listening_duration(campaign_items)
        stats.ugc_contribs = get_campaign_items(campaign, News.ContentType.UGC,
                                    start_datetime, end_datetime).count()
        stats.total_ugc_contribs = get_campaign_items(campaign, News.ContentType.UGC).count()
        stats.total_listeners = len(ihs.values_list('cdr__callerid',
                                        flat=True).distinct())
        data.append(stats)

    return data


def generate_campaign_stats_datasheet(campaign, start_datetime, end_datetime, campaign_end_data_required = False, cumulative_data_required = False, items = None, op_file=None):
    if not op_file:
       if cumulative_data_required:
          start_datetime = campaign.start
          output_file = '/tmp/%s_%s_cumulative_statistics.xlsx' % (campaign.id, campaign.name)
       else:
          output_file = '/tmp/%s_%s_statistics.xlsx' % (campaign.id, campaign.name)
    else:
       output_file=op_file
    statistics_timeline = get_statistics_timeline(campaign, start_datetime, end_datetime)
    ihc_summary = get_cumulative_campaign_stats(campaign, News.ContentType.IHC,
                        start_datetime, end_datetime, items, campaign_end_data_required)
    ugc_summary = get_cumulative_campaign_stats(campaign, News.ContentType.UGC,
                        start_datetime, end_datetime, items,  campaign_end_data_required)
    if generate_workbook([statistics_timeline, ihc_summary, ugc_summary], output_file):
        return output_file


def get_cumulative_cmp_stats_header(content_type):
    header = [
                CONTENT_LABELS['item_id'], CONTENT_LABELS['ai_id'], CONTENT_LABELS['item_title'], CONTENT_LABELS['transcription'],
                CONTENT_LABELS['qualifier'], CONTENT_LABELS['state'], CONTENT_LABELS['district'],
                CONTENT_LABELS['block'], CONTENT_LABELS['tags'], CONTENT_LABELS['rating'], CONTENT_LABELS['times_bookmark'], CONTENT_LABELS['times_forward'],CONTENT_LABELS['categories'], 
                CONTENT_LABELS['campaign_categories'], CONTENT_LABELS['item_creation_time'], CONTENT_LABELS['item_duration'], CONTENT_LABELS['item_heard_count'],
                CONTENT_LABELS['total_dur_heard'], CONTENT_LABELS['listeners'], CONTENT_LABELS['channel'],
            ]

    if content_type == News.ContentType.UGC:
        header.append(CONTENT_LABELS['callerid'])
        header.append(CONTENT_LABELS['contributor_name'])
        header.append(CONTENT_LABELS['item_published_time'])
        header.append(CONTENT_LABELS['time_taken'])

    header.append(CONTENT_LABELS['item_url'])

    return header

def get_news_campaign_category_data(item, subcategories):
    return ['Yes' if item.campaign_category_assigned(subcategory.id) else '-' for subcategory in subcategories]


def get_item_attribs_map(campaign, content_type, start_datetime, end_datetime, items ):
    item_attribs_map = {}
    if not items:
       items = get_campaign_items(campaign, content_type)
 
    for item in items:
        item_heard_count, duration_heard = get_item_listening_stats(item, start_datetime, end_datetime)
        if int(item_heard_count) > 0:
           state, district, block = (None,)*3
           bookmark_count, forward_count = get_item_bookmark_forward_count(item, start_datetime, end_datetime)
           channel = item.channel.name 
           loc_from_contact = getattr(load_vapp_module('callerinfo', 'models'), 'get_location_using_contact_id')
           loc_from_news = item.get_news_location()
           location = loc_from_news or loc_from_contact(item.callerid)
           if location:
              state, district, block = location.state.name, location.district.name, location.block.name
           qualifier = item.qualifier.name if item.qualifier else '-'
           category = item.category.__unicode__() if item.category else '-'
           listeners = len(Item_heard_stats.objects.filter(item_id = item.id, when_heard__range = (start_datetime, end_datetime)).values_list('cdr__callerid', flat = True).distinct())
           if item.campaign_categories.all():
              cmp_categories_str = ','.join([each.parent.name  for each in item.campaign_categories.all()])
           news_duration = '-'
           news_url = '-'
           try:
              news_duration = item.get_news_duration()
              news_url = item.detail.get_url()
           except:
              logger.exception("Unable to get news duration/news url for item: " + str(item.id))

           row = [item.id, item.ai.name, item.title, item.transcript, qualifier, state, district, block, item.tags, item.rating, bookmark_count, forward_count, category, \
                  cmp_categories_str, item.time, news_duration, item_heard_count, duration_heard, listeners, channel]
           if content_type == News.ContentType.UGC:
               row.append(item.callerid)
               contact = Contact.objects.filter(number = item.callerid, name__iregex=r'[a-zA-Z\s ]+$').reverse()
               if contact:
                 row.append(contact[0].name)
               else:
                 row.append('NA')
               pub_time = item.get_published_datetime()
               row.append(pub_time)
               if pub_time:
                 time_diff  = get_formated_datetime(item.time, pub_time)
               else:
                 time_diff = None
               row.append(time_diff)
           row.append(news_url)

           item_attribs_map[item] = row
    return item_attribs_map


def get_cumulative_campaign_stats(campaign, content_type, start_datetime, end_datetime, items,  campaign_end_data_required = False):
    cumulative_stats = {
                        'name': '%s Summary' % content_type,
                        'data': []
                       }
    item_attribs_map = get_item_attribs_map(campaign, content_type, start_datetime, end_datetime, items)

    header = get_cumulative_cmp_stats_header(content_type)
    if campaign_end_data_required:
        cat_subcat_map = campaign.get_categories_subcategories_map()
        subcategories = []
        for category in cat_subcat_map.keys():
            for subcategory in cat_subcat_map[category]:
                header.extend([ '%s->%s' % (category.name, subcategory.name) ])
                subcategories.append(subcategory)

        for item, attribs in item_attribs_map.items():
            attribs.extend( get_news_campaign_category_data(item, subcategories) )

    summary_data = []
    for item, attribs in item_attribs_map.items():
        summary_data.append(attribs)

    summary_data = sorted( summary_data, key = lambda data: data[ header.index( CONTENT_LABELS['item_creation_time'] ) ] )
    cumulative_stats['data'].append(header)
    cumulative_stats['data'].extend(summary_data)

    return cumulative_stats


def get_detailed_campaign_stats(campaign, start_date, end_date):
    detailed_stats = {
                       'name': 'Daily Statistics',
                       'header_row_count': 0,
                       'data': []
                     }

    for content_type in (News.ContentType.IHC, News.ContentType.UGC,):
        detailed_stats['data'].append([content_type])
        detailed_stats['data'].append(['Item ID', 'Item Title', 'Time', 'Duration', 'Instance'])
        for item in get_campaign_items(campaign, content_type):
            stats = Item_heard_stats.objects.filter(item_id = item.id, when_heard__range = (start_date, end_date))
            if not stats.exists():
                continue
            for stat in stats:
                detailed_stats['data'].append([item.id, item.title, stat.when_heard, stat.duration, stat.ai.name])

        detailed_stats['data'].append([''])

    return detailed_stats


def get_statistics_timeline(campaign, start_date, end_date):
    stats = {
                'name': 'Timeline',
                'data': []
            }
    stats['data'].append(['Date', 'UGC Contributions', 'Times UGC Items Heard',
                          'Duration UGC Items Heard (seconds)', 'Times In-House Items Heard',
                          'Duration In-House Items Heard (seconds)', '# of Listeners'])

    items = get_campaign_items(campaign)

    temp_date = datetime.combine(start_date + timedelta(days=1), datetime.min.time())
    while temp_date <= end_date:
        ihs = get_items_heard_stats(items, start_date, temp_date)
        ugc_contrib_count = get_campaign_items(campaign, News.ContentType.UGC,
                                               start_date, temp_date).count()
        ugc_heard_stats = ihs.filter(item__source__in=[News.Source.VOICE_INTERFACE,
                                                       News.Source.APP_INTERFACE])
        ugc_items_heard = ugc_heard_stats.count()
        ugc_duration_heard = ugc_heard_stats.aggregate(Sum('duration'))['duration__sum'] or 0.0
        ihc_heard_stats = ihs.filter(item__source=News.Source.WEB_INTERFACE)
        ihc_items_heard = ihc_heard_stats.count()
        ihc_duration_heard = ihc_heard_stats.aggregate(Sum('duration'))['duration__sum'] or 0.0
        listeners = len(ihs.values_list('cdr__callerid', flat=True).distinct())
        stats['data'].append([start_date.date(), ugc_contrib_count, ugc_items_heard,\
                              ugc_duration_heard, ihc_items_heard, ihc_duration_heard,\
                              listeners])
        start_date = temp_date
        temp_date = temp_date + timedelta(days=1)

    return stats


def get_campaign_items(campaign, content_type = None, start_date = None, end_date = None):
    campaign_item_ids = campaign.get_item_ids()
    news = News.get_pub_arc_news().filter(pk__in = campaign_item_ids)
    if start_date and end_date:
        news = news.filter(time__range = (start_date, end_date))
    if not content_type:
        return news
    if content_type == News.ContentType.UGC:
        return news.filter(source__in=[News.Source.VOICE_INTERFACE,
                                       News.Source.APP_INTERFACE])
    elif content_type == News.ContentType.IHC:
        return news.filter(source=News.Source.WEB_INTERFACE)


def get_callers_campaign_contribs(ai_ids, callerid, start_date, end_date):
    return News.get_pub_arc_news().filter(ai_id__in = ai_ids, callerid = callerid, time__range = (start_date, end_date), campaign_categories__isnull = False).count()


def copy_campaign_detailed_data(campaign_id):
    campaign = Campaign.objects.get(pk = campaign_id)
    campaign_items = get_campaign_items(campaign)
    dump_data(campaign_items)


def get_stats(campaigns, start_datetime, end_datetime, stat_type = None):
    data = get_mail_content(campaigns, start_datetime, end_datetime)
    reports = []
    if stat_type == Stats_type.WEEKLY:
        reports.extend( [ generate_campaign_stats_datasheet(campaign, start_datetime, end_datetime, campaign_end_data_required = True ) for campaign in campaigns ] )
    if is_start_of_month( end_datetime.date() ):
        start_datetime = datetime.combine(end_datetime - relativedelta(months = 1), datetime.min.time())
        reports.extend( [ generate_campaign_stats_datasheet(campaign, start_datetime, end_datetime, campaign_end_data_required = True, cumulative_data_required = True ) for campaign in campaigns ] )
    return (data, reports)


def create_html_content(stats):
    return render_to_string('campaign/stats.html', { 'data': stats }) + LINE_BREAK
