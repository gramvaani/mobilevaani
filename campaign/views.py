# Create your views here.
from campaign.models import Campaign, Campaign_category
from utils import update_to_dynamo
from app_manager.models import  App_instance
from log import  get_request_logger
import json
from local_settings import SERVER_ID
from django.core.serializers.json import DjangoJSONEncoder

logger =  get_request_logger()


def upload_all_campaign_for_ai(ai_id):
    ai_dict= {}
    ai = App_instance.objects.filter(pk = ai_id)
    campaigns = Campaign.objects.filter(ais__in = ai)
    campaign_list = []
    for camp in campaigns:
        if camp.show_in_ui:
            camp_dict =  {}
            camp_dict["id"] = camp.id
            camp_dict["name"] = camp.name
            camp_category = Campaign_category.objects.filter(campaign = camp)
            cc_parent = camp_category.filter(parent__isnull = True)
            cc_list = []
            for cc in cc_parent:
                p_dict = {}
                p_dict["name"] = cc.name
                p_dict["id"] = cc.id
                child_list = []
                childrens = camp_category.filter(parent = cc)
                for child in childrens:
                    c_dict ={}
                    c_dict["id"] = child.id
                    c_dict["name"] = child.name
                    child_list.append(c_dict)

                p_dict["children"] = child_list
                cc_list.append(p_dict)
            camp_dict["campaignCategories"] = cc_list
            campaign_list.append(camp_dict)
#            print campaign_list
    ai_dict["campaign"] = json.dumps(campaign_list)
    ai_dict["ai"] = ai_id
    ai_dict['server'] = SERVER_ID
    # print ai_dict
    if campaign_list:
        pkey = SERVER_ID+"-ai-"+str(ai_id)
        skey = 'Campaign'
       # logger.info("Syncing campaign:"+ str(ai_dict))
        update_to_dynamo('Ai_properties', pkey, skey, json.dumps(ai_dict, cls=DjangoJSONEncoder))
    else:
        logger.info('Not sending to server')

