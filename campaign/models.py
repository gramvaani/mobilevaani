from django.db import models

from app_manager.models import App_instance
from media.models import Image_caption_map
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save
#from utils import update_to_dynamo
from log import get_request_logger

logger = get_request_logger()

class Campaign(models.Model):
    name        = models.CharField(max_length = 255)
    title       = models.CharField(max_length = 255, null = True, blank = True)
    description = models.TextField(help_text = "In one line")
    show_in_ui  = models.BooleanField(default = True)
    abstract    = models.TextField(null = True, blank = True, help_text = "In one paragraph")
    ais         = models.ManyToManyField(App_instance)
    start       = models.DateTimeField()
    end         = models.DateTimeField()
    report      = models.URLField(null = True, blank = True)
    cover_image = models.ForeignKey(Image_caption_map, related_name = 'cover_image', null = True, blank = True)
    images      = models.ManyToManyField(Image_caption_map, related_name = 'images')

    def __unicode__(self):
        return unicode( self.id ) + '_' + unicode( self.name )

    def get_item_ids(self):
        item_ids = set()
        for cat in self.campaign_category_set.all():
            for news in cat.news_set.all():
                item_ids.add(news.id)
        return item_ids

    @classmethod
    def get_active_campaigns(cls, on_date):
        return Campaign.objects.filter(end__gte = on_date)

    def get_categories_subcategories_map(self):
        cat_subcat_map = {}
        categories = Campaign_category.objects.filter(campaign = self, parent = None)
        for category in categories:
            subcategories = Campaign_category.objects.filter(campaign = self, parent = category)
            cat_subcat_map[category] = subcategories

        return cat_subcat_map

def campaign_save_handler(sender, **kwargs):
    object_id = kwargs['instance'].id
    model_name = kwargs['instance'].__class__.__name__
    #sync_object_data(model_name, object_id)

post_save.connect(campaign_save_handler, sender=Campaign, dispatch_uid = 'campaign.campaign.save')



class Campaign_category(models.Model):
    name     = models.CharField(max_length = 255)
    campaign = models.ForeignKey(Campaign)
    parent   = models.ForeignKey('Campaign_category', null = True, blank = True)

    def __unicode__(self):
        if self.parent:
            return unicode(self.parent) + " -> " + unicode(self.name)
        else:
            return unicode(self.campaign.name) + " -> " + unicode(self.name)

    def clean(self):
        if self.parent and ( self.campaign != self.parent.campaign ):
            raise ValidationError( 'Same campaign should be provided for category and parent' )

        if Campaign_category.objects.filter(campaign = self.campaign, parent = self).exists() and self.parent:
            raise ValidationError('For category parent field should remain empty.')

        super(Campaign_category, self).clean()


def campaign_category_save_handler(sender, **kwargs):
    campaign_category = kwargs['instance']
    object_id = campaign_category.id

post_save.connect(campaign_category_save_handler, sender=Campaign_category, dispatch_uid = 'campaign.campaign_category.save')


class Campaign_stats_user_map(models.Model):
    user      = models.OneToOneField(User)
    campaigns = models.ManyToManyField(Campaign)

    def __unicode__(self):
        campaign_str = "_".join([unicode(campaign.name) for campaign in self.campaigns.all()])
        return unicode(self.user) + '_' + campaign_str


class Campaign_statistics(models.Model):
    campaign           = models.ForeignKey(Campaign)
    start_date         = models.DateTimeField()
    end_date           = models.DateTimeField()
    users_count        = models.PositiveIntegerField(null = True)
    ugc_contribs       = models.PositiveIntegerField()
    ugc_items_heard    = models.PositiveIntegerField()
    ugc_duration_heard = models.DecimalField(max_digits = 10, decimal_places = 2)
    ihc_items_heard    = models.PositiveIntegerField()
    ihc_duration_heard = models.DecimalField(max_digits = 10, decimal_places = 2)
