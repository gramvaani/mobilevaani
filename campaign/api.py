from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication, MultiAuthentication
from tastypie.authorization import ReadOnlyAuthorization, Authorization
from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie import fields
from campaign.stats import copy_campaign_detailed_data

from datetime import datetime

from log import get_request_logger
from vapp.api import AppInstanceAuthorization
from media.api import ImageCaptionMapResource
from app_manager.api import AppInstanceResource, AnonymousReadAuthentication

from models import Campaign, Campaign_category, Campaign_statistics

logger = get_request_logger()


class CampaignResource(ModelResource):
    cover_image = fields.ForeignKey(ImageCaptionMapResource, attribute='cover_image', full=True, null=True)
    images = fields.ToManyField(ImageCaptionMapResource, attribute='images')
    ais = fields.ToManyField(AppInstanceResource, attribute='ais', full=True)

    class Meta:
        queryset = Campaign.objects.all()
        resource_name = "campaign_campaign"
        filtering = {
            'show_in_ui': [ 'exact' ],
        }
        authentication = MultiAuthentication( ApiKeyAuthentication(), AnonymousReadAuthentication() )
        authorization = ReadOnlyAuthorization()


    def build_filters(self, filters=None):
        logger.info('Starting api to get')
        if filters is None:
            filters = {}

        orm_filters = super(CampaignResource, self).build_filters( filters )
        if "ai" in filters:
            orm_filters[ "ais" ] = filters[ "ai" ]
        logger.info('Api Finish data fetch')
        return orm_filters

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/create_dump%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'create_dump' ), name = "api_create_dump" ),
        ]

    def create_dump(self, request, **kwargs):
        self.method_check( request, allowed = ['get'] )
        self.is_authenticated( request )
        self.throttle_check( request )

        try:
            campaign_id = request.REQUEST[ 'campaign_id' ]
            copy_campaign_detailed_data( campaign_id )

            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, "reason": "creation of dump was successful." } )
        except Exception,e:
            logger.exception('exception in create_dump: {0}'.format(e))
        return self.create_response( request, { 'success': False, "reason": "creation of dump failed." } )


class CampaignCategoryResource(ModelResource):
    parent = fields.ForeignKey('self', 'parent', null = True)
    campaign = fields.ForeignKey(CampaignResource, 'campaign')

    class Meta:
        queryset = Campaign_category.objects.all()
        resource_name = "campaign_campaign_category"
        
        authentication = ApiKeyAuthentication()
        #XXX: Read + Write authorization given to allow saves of mnews. needs to be fixed.
        authorization = Authorization()

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(CampaignCategoryResource, self).build_filters( filters )
        if "ai" in filters:
            orm_filters[ "campaign__ais" ] = filters[ "ai" ]
        return orm_filters


class CampaignStatsResource(ModelResource):
    campaign = fields.ForeignKey(CampaignResource, 'campaign')

    class Meta:
        queryset = Campaign_statistics.objects.all()
        resource_name = "campaign_stats"
        filtering = {
            'campaign': ALL_WITH_RELATIONS,
            'start_date': ALL_WITH_RELATIONS,
            'end_date': ALL_WITH_RELATIONS,
        }
