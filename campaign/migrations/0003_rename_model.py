# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'CampaignCategory'
        db.delete_table(u'campaign_campaigncategory')

        # Adding model 'Campaign_category'
        db.create_table(u'campaign_campaign_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['campaign.Campaign'])),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['campaign.Campaign_category'], null=True, blank=True)),
        ))
        db.send_create_signal(u'campaign', ['Campaign_category'])


    def backwards(self, orm):
        # Adding model 'CampaignCategory'
        db.create_table(u'campaign_campaigncategory', (
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['campaign.CampaignCategory'], null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['campaign.Campaign'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'campaign', ['CampaignCategory'])

        # Deleting model 'Campaign_category'
        db.delete_table(u'campaign_campaign_category')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'ais': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'campaign.campaign_category': {
            'Meta': {'object_name': 'Campaign_category'},
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign_category']", 'null': 'True', 'blank': 'True'})
        },
        u'campaign.campaign_items': {
            'Meta': {'object_name': 'Campaign_items'},
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['campaign.Campaign']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.News']"})
        },
        u'location.block': {
            'Meta': {'object_name': 'Block'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.District']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.State']", 'null': 'True', 'blank': 'True'})
        },
        u'location.location': {
            'Meta': {'object_name': 'Location'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'block'", 'null': 'True', 'to': u"orm['location.Block']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': u"orm['location.Country']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'district'", 'null': 'True', 'to': u"orm['location.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state'", 'null': 'True', 'to': u"orm['location.State']"}),
            'village': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'location.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'mnews.category': {
            'Meta': {'object_name': 'Category'},
            'ai': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app_manager.App_instance']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_subcategory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'Parent'", 'null': 'True', 'to': u"orm['mnews.Category']"}),
            'subcategories': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'subcategory'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['mnews.Category']"})
        },
        u'mnews.channel': {
            'Meta': {'object_name': 'Channel'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.format': {
            'Meta': {'object_name': 'Format'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mnews.news': {
            'Meta': {'object_name': 'News'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            'callerid': ('app_manager.models.CalleridField', [], {'max_length': '20'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Category']", 'null': 'True'}),
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Channel']"}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'parent'", 'symmetrical': 'False', 'to': u"orm['mnews.News']"}),
            'detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'detail'", 'null': 'True', 'to': u"orm['media.Recording']"}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Format']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_advertisement': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_comment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_mod_flagged': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_src_caller': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['location.Location']", 'null': 'True', 'blank': 'True'}),
            'location_text': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'pub_order': ('django.db.models.fields.SmallIntegerField', [], {'default': '9999'}),
            'qualifier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mnews.Qualifier']", 'null': 'True'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'summary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'summary'", 'null': 'True', 'to': u"orm['media.Recording']"}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'transcript': ('django.db.models.fields.TextField', [], {})
        },
        u'mnews.qualifier': {
            'Meta': {'object_name': 'Qualifier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['campaign']