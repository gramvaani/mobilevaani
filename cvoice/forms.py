from cvoice.models import *
from django.forms import ModelForm
from django import forms
from vapp.log import get_logger

logger = get_logger()

MOBILE_CHAR_LENGTH=20

GENDER_CHOICE = (
                ('M' , 'Male'),
                ('F' , 'Female'),
    )  

AGE_CHOICES = zip( range(10,99), range(10,99) )

class WardDetailForm(ModelForm):
    
    ward = forms.ModelChoiceField(queryset=Ward.objects.all().order_by('ward_name'),empty_label=None)
    graph = forms.ImageField(required=False)
    class Meta:
        
        model = WardData
        exclude = ('ai')

class AdminResponseFillForm(ModelForm):
       
    response = forms.ModelChoiceField(queryset=Option.objects.all(),empty_label=None)
    called_user = forms.CharField(max_length = MOBILE_CHAR_LENGTH,label='Mobile No.')
    name     = forms.CharField(max_length=50)
    gender   = forms.ChoiceField(choices = GENDER_CHOICE)
    ward     = forms.ModelChoiceField(queryset=Ward.objects.all().order_by('ward_name'),empty_label=None)
    address  = forms.CharField( widget = forms.Textarea())
    age      = forms.ChoiceField(choices = AGE_CHOICES)
    profession = forms.CharField(max_length=20)
    question   = forms.ModelChoiceField(queryset=Question.objects.all() , empty_label=None)
    discard    = forms.BooleanField()
    
    class Meta:
        model = UserProfile
        
    def __init__(self,*args, **kwargs):
       
       response = kwargs.pop('response',None)
       super(AdminResponseFillForm, self).__init__(*args, **kwargs)
       
       self.fields.keyOrder = [
                               'name',
                               'called_user',
                               'gender',
                               'ward',
                               'address',
                               'age',
                               'profession',
                               'question',
                               'response',
                               'discard',
                               ]
       called_user = response.user
       user_profile = None
       
       try:
           user_profile = called_user.get_profile()
       except:
            logger.debug('user profile doesnt exist for user:' + str(called_user))
            user_profile = UserProfile()
            user_profile.user = called_user
            user_profile.phone_num = called_user.username
            user_profile.save()
       
       if user_profile:
           
           self.fields['called_user'].initial = called_user.username
           self.fields['name'].initial = user_profile.name
           self.fields['age'].initial = user_profile.age
           self.fields['address'].initial = user_profile.address
           self.fields['profession'].initial = user_profile.profession
           self.fields['gender'].initial = user_profile.gender
           
           
       if user_profile.ward:               
            self.fields['ward'].initial = Ward.objects.get(pk=user_profile.ward.id)
           
       if response:
            
            self.fields['question'].initial = Question.objects.get(pk=response.question.id)
            if response.response: 
                self.fields['response'].initial = Option.objects.get(pk=response.response.id)
       self.fields['discard'].initial = False        


class WebUserProfileForm(ModelForm):
    
    ward     = forms.ModelChoiceField(queryset=Ward.objects.all().order_by('ward_name'),empty_label=None)
    age      = forms.ChoiceField(choices = AGE_CHOICES)
    gender   = forms.ChoiceField(choices = GENDER_CHOICE)
    
    class Meta:
        model = UserProfile
        exclude = ('profile_complete','user')
        
    def __init__(self, *args, **kwargs):
        
       profile = kwargs.pop('profile',None)
       super(WebUserProfileForm, self).__init__(*args, **kwargs)
       
       if profile:
           self.fields['name'].initial = profile.name
           self.fields['age'].initial = profile.age
           self.fields['address'].initial = profile.address
           self.fields['profession'].initial = profile.profession
           self.fields['gender'].initial = profile.gender
           self.fields['email'].initial = profile.email
           self.fields['phone_num'].initial = profile.phone_num
           
           if profile.ward:
               self.fields['ward'].initial = profile.ward
            
           
        
        