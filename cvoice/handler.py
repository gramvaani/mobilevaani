from vapp.telephony.utils import *
from vapp.helpline.models import *
from vapp.log import get_logger
from vapp.cvoice.models import *
from vapp.app_manager.models import User_permission

logger = get_logger()

def in_handler(session, args):
    logger.info("Running C-Voice")

    answered = False
    for handler in Handler.objects.filter(ai = session.ai.id).order_by('priority'):
        if handler.num == None or handler.num == "":
            continue
        outgoing = get_connected_session(get_session_url(handler.num, session.ai), session.ai)
        if outgoing.answered():
            answered = True
            break

    callerid = session.getCallerID()

    if answered:
        recording = Recording(ai_id = session.ai.id)
        logger.debug("recording conversation to file: " + recording.get_full_filename())
        call = Call(ai_id = session.ai.id, callerid = callerid, handler = handler.num, media_id = recording.id)
        call.save()
        session.execute("record_session", recording.get_full_filename())
        bridge_sessions(session, outgoing)
    else:
        play_prompt(session, 'welcome')
        call = get_embedded_recording(session, 40, Call(ai_id = session.ai.id), Call.media)
        call.callerid = callerid
        call.ai = session.ai
        call.handler = Handler.RECORDER
        call.save()
        play_prompt(session, 'thankyou')
        
        if call:
          update_voice_response_table(call.ai, call.media.id,call.callerid)
          
def get_assigned_user(ai, recording_id):
    
    users = User_permission.objects.filter(ai=ai)
    number = len(users)
    
    if number > 0:
        if recording_id > 0:
            
            logger.debug('number and recording id both > 0')
            index = (recording_id % number)
            return users[index].user
    return None

def update_voice_response_table(ai, recording_id, callerid):
    
    try:
        recording = Recording.objects.get(pk=recording_id)
        called_user = None 
        try:
            user = User.objects.get(username=callerid)
        except:
            logger.debug('user doesnt exist, creating new user:' + str(callerid))
            user = User()
            user.username = callerid
            user.set_password(callerid)
            user.save()
            
        cr = CampaignResponse()
        cr.campaign = Campaign.objects.filter(ai=ai).order_by('-start_date')[0]
        cr.question = cr.campaign.questions.get_query_set()[0]
        cr.response_recording = recording
        cr.user = user
        cr.assigned_user = None #get_assigned_user(ai,recording_id)
        cr.state = 'NP'
        cr.save()
        logger.debug('new cr added' + str(cr.id))
    except Exception,e:    
        logger.debug('new exception:' + str(e))
