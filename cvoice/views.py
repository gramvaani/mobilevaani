# Create your views here.
from vapp.log import get_logger
from googlemaps import GoogleMaps
from django.contrib.auth.decorators import login_required
from app_manager.models import App_instance
from django.shortcuts import render_to_response
from cvoice.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.core import serializers
from django.utils import simplejson

import Levenshtein
import Rtree
from rtree import index
from cvoice.forms import *
from GChartWrapper import *


rtree_index = None
latBounds = [76.0,78.0]
longBounds = [27.0,29.0]
logger = get_logger()

head_title = 'Mericity.in'
gchart_url = "http://chart.apis.google.com"
gchart_local_proxy_url = "http://mericity.in"

ai = None

def get_ai():

    global ai
    ai = App_instance.objects.get(name='cvoice') 

def build_rtree():
    
    global rtree_index
    
    colonies = ColonyGeoData.objects.all()
    id = 0
    rtree_index = index.Index()
    
    while id < len(colonies):
        rtree_index.insert(id, (colonies[id].lat,colonies[id].long,colonies[id].lat,colonies[id].long),colonies[id])
        id = id + 1
        
def get_ward_impl(request):

        global rtree_index
        ward = None
        
        if request.POST['address']:
            if not rtree_index:
                build_rtree()
                    
            logger.debug('address:' + request.POST['address'])
            address = request.POST['address']
            gmap = GoogleMaps()
            try:
                colony = None
                gcode = gmap.geocode(address)
                clonies = None
                locality = gcode['Placemark'][0]['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']
                   
                if locality.has_key('DependentLocality'):
                    clonies = locality['DependentLocality']['DependentLocalityName'].split(',')
                else:
                    clonies = locality['Thoroughfare']['ThoroughfareName'].split(',')
                    
                min_dist = 99999
                target_colony = None
                name_match = False
                   
                for cln in clonies:
                        
                    colonies = Colony.objects.all()                        
                    for c in colonies:
                        dist = Levenshtein.distance(cln.lower(),c.colony_name.lower())
                        if dist == 0:
                            logger.debug('exact match:' + c.colony_name)
                            name_match = True
                            ward = c.ward
                            wards = []
                            wards.append(ward)
                            break
                        
                if not name_match:
                    lat = gcode['Placemark'][0]['Point']['coordinates'][0]
                    long = gcode['Placemark'][0]['Point']['coordinates'][1]
                       
                    colonies = [n.object for n in rtree_index.nearest((lat, long, lat, long),1, True)]
        
                    wards = []
                    for c in colonies:
                        wards.append(c.colony.ward)
                         
                    ward = wards[0]
            except Exception,e:
                logger.debug('exception looking up address:' + str(e))
                    
                colony = request.POST['address']
                        
                if colony:
                        
                    min_dist = 99999
                    target_colony = None
                        
                    dict = {}                       
                    colonies = Colony.objects.all()                        
                    for c in colonies:
                        dist = Levenshtein.distance(colony.lower(),c.colony_name.lower())
                        dict[dist] = c
                        if dist < min_dist:
                            min_dist = dist
                            target_colony = c  
                        
                    dict.keys().sort()                  
                    ward = target_colony.ward
        return ward

@csrf_exempt        
def get_ward(request):
    
        ward = get_ward_impl(request)
        dict = {}
        dict['ward_name'] = ward.ward_name
        dict['ward_link'] = '/vapp/cvoice/ward_info/' + str(ward.id) + '/'
    
        js = serializers.get_serializer("json")()
        data = simplejson.dumps(dict)
        return HttpResponse(data)

def get_next_response_to_process(user):
    
    logger.debug('user id:' + str(user.id) )
    response = CampaignResponse.objects.filter(((Q(assigned_user=user) & Q(state='UP')) | Q(state='NP')))
    if len(response) > 0:
        logger.debug('campaign response selected:' +  str(response[0]))
        return response[0]
    else:
        return None
    
def get_stats_for_campaign(campaign):
    
    stats_map = {}
    
    if campaign:
        
        for question in campaign.questions.get_query_set():
            
            data = {}
            total_responses = 0
            
            responses = CampaignResponse.objects.filter(campaign=campaign, question=question, state='PD')
            if len(responses) > 0:
                total_responses = len(responses)
                for response in responses:
                    if data.has_key(response.response.option_text):
                        data[response.response.option_text] += 1
                    else:
                         data[response.response.option_text] = 1
                for k,v in data.items():
                    data[k] = round((v/float(total_responses))*100,2) 
                G = Pie3D(data.values())
                labels = []
                
                for k,v in data.items():
                    labels.append(str(k) + '(' + str(v) + '%)')
                apply(G.label, labels)
                G.color('ff0000')
                G.title('Poll Results')
                G.size(450,150)
                graph_url = str(G.url).replace(gchart_url,gchart_local_proxy_url,1)
                stats_map[question] = graph_url
            else:
                logger.debug('for ward:' + str(campaign.ward) + ' no responses exist')
    else:
        logger.debug('campaign is null    ')

    return stats_map
        

def main_page(request):

    global head_title
    
    logger.debug(request)
    ward = None
    wards = None
    if request.method == "POST":
        if request.POST['ward'] and not request.POST['ward'] == 'default':
            try:
                id = request.POST['ward'].split('_')[1]
                logger.debug('id:' + str(id))
                ward = Ward.objects.get(pk=id)
                
            except:
                 logger.debug('ward not found:' + str(request.POST['ward']))
                 
        else:
            global rtree_index
            ward = get_ward_impl(request)                      
                        
        return HttpResponseRedirect('/vapp/cvoice/ward_info/' + str(ward.id) + '/')    
        
                    
    else:
         wards = Ward.objects.all().order_by('ward_name')         
         return render_to_response('cvoice/main.html',{'wards' : wards, 'head_title' : head_title}, context_instance = RequestContext(request))
     
def render_ward(request,ward_id):
    
    title = None
    #logger.debug(request)
    ward = None
    questions = None
    user = request.user
    
    if request.method == "POST":
        try:
            logger.debug('current poll:' + str(request.POST['current_poll']))
            
            poll = Campaign.objects.get(id=request.POST['current_poll'])
            ward = Ward.objects.get(id=request.POST['ward'])
            questions = poll.questions.get_query_set()
            q_len = len(questions)
            logger.debug(request.POST)
            for question in questions:
                if request.POST.has_key('q_' + str(question.id)):
                    option = Option.objects.get(pk=request.POST['q_' + str(question.id)])
                    user_response = CampaignResponse()
                    user_response.campaign = poll
                    user_response.user = user
                    user_response.question = question
                    user_response.response = option
                    user_response.state = 'PD'
                    user_response.save()
                    
            return HttpResponseRedirect(request.path)
            
                    
                    
        except Exception,e:
            return HttpResponse('Illegal form submission.' + str(e))
    else:
        try:            
            ward = Ward.objects.get(pk=ward_id)
        except:
            logger.debug('render_ward: ward not found for pk:' + str(ward_id))
            return HttpResponse('This ward was not found. Internal Error.')
        
        current_poll=None
        campaigns = None
        questions_left = []
        data = {}
        total_responses = 0
        question_responses = {}
        is_user_ward = False
        response_stats = {}
        are_responses = False
        
        campaigns = Campaign.objects.filter(ward=ward).order_by("-start_date")
        
        if campaigns and len(campaigns) > 0: 
            current_poll = campaigns[0]
            logger.debug('stats to be found')
            response_stats = get_stats_for_campaign(current_poll)      
            
            if len(response_stats) > 0: 
                are_responses = True
                
            if request.user.is_authenticated():           
                
                try:
                    user_profile = request.user.get_profile()
                    if user_profile:
                        if user_profile.ward == ward:
                            is_user_ward = True
                except:
                    logger.debug('user profile doesnt exist for user:' + str(request.user))
                        
                for question in current_poll.questions.get_query_set():
                    
                    try:
                        user_response = CampaignResponse.objects.get(campaign=current_poll,user=request.user,question=question,state='PD')
                        question_responses[question] = user_response
                    except:
                        logger.debug('question for which response not found:' + str(question.id))
                        questions_left.append(question)
                        
            else:
                questions_left = current_poll.questions.get_query_set() 
                
                try:
                    current_theme = Theme.objects.get(campaign=current_poll)
                except:
                    logger.debug('no theme for this campaign')
            
            current_theme = None
            try:
                current_theme = Theme.objects.get(ai=ward.ai, campaign=current_poll, date__month=datetime.now().month)
            except:
                logger.debug('No theme object found!')
                
        
        return render_to_response('cvoice/quick_poll.html',{'ward' : ward, 'head_title' : ward.ward_name ,'current_poll' : current_poll ,'questions' : questions_left,'question_responses' : question_responses, \
                                                             'is_user_ward' : is_user_ward, 'response_stats' : response_stats, 'are_responses':are_responses,'current_theme' : current_theme }, context_instance = RequestContext(request))
            
 
@login_required(login_url="/vapp/cvoice/admin/login")
def render_admin_page(request):
    
    logger.debug(request)
    global ai
    
    if not ai:
        get_ai()
        
    user = request.user
    form = None
    
    
    if request.method == "POST":
        logger.debug(request.POST)
        
        response = CampaignResponse.objects.get(Q(assigned_user=user) & Q(state='UP'))    
        
        try:
            user_profile =  User.objects.get(username=request.POST['called_user']).get_profile()
        except:
            logger.debug('user profile not found in POST' + str(request.POST['called_user']))
        
        if request.POST['discard']:
            logger.debug('this response is to be discarded')
            response.state = 'DI'
            response.save()
        
        else:
            user_profile.name = request.POST['name']
            user_profile.age = request.POST['age']
            user_profile.gender = request.POST['gender']
            user_profile.profession = request.POST['profession']
            user_profile.ward = Ward.objects.get(pk=request.POST['ward'])
            user_profile.address = request.POST['address']
            user_profile.save() 
            
            question_id = request.POST['question']
            response_id = request.POST['response']
            
            campaign = Campaign.objects.filter(ai=ai).order_by('-start_date')[0]
            logger.debug('response: ' + str(request.POST['response']))
            user_response = CampaignResponse.objects.get(pk=request.POST['response_id'])
            user_response.campaign = campaign
            user_response.question = Question.objects.get(pk=question_id)
            user_response.response = Option.objects.get(pk=response_id)
            user_response.state ='PD'
            user_response.save()
        
        return HttpResponseRedirect("/vapp/cvoice/admin/process_voice/")
    
    
    else:
        
        response = get_next_response_to_process(user)
        
        aform = None
        if response:
            response.state = 'UP'
            response.assigned_user = user
            response.save()
            aform = AdminResponseFillForm(response=response)
        
        return render_to_response('cvoice/voice_admin.html', {'form' : aform,'ai' : ai, "response" : response}, context_instance = RequestContext(request))
        

@login_required(login_url="/vapp/cvoice/main") 
def render_ward_info_form(request):
    
    logger.debug(request)
    
    global ai
    
    if not ai:
        get_ai()
    
    if request.method == "POST":
        
        form = WardDetailForm(request.POST, request.FILES)    
        ward_data = form.save(commit=False)
        old_ward_data = None
        try:
            old_ward_data = WardData.objects.get(ward=ward_data.ward)
        except:
            ward_data.ai = ai
            ward_data.save()             
            return HttpResponse("Ward Info data has been saved successfully.") 
        
        if old_ward_data:
            if ward_data.info:
                old_ward_data.info = ward_data.info
            if ward_data.graph:
                old_ward_data.graph = ward_data.graph
            
            old_ward_data.save()
        return HttpResponse("Ward Info data has been saved successfully.")
        
    
    else:
        form = WardDetailForm()
        
        return render_to_response('cvoice/upload_form.html', {'form' : form}, context_instance = RequestContext(request))
 
@login_required(login_url="/vapp/cvoice/main")    
def render_profile(request,show_profile):
    
    logger.debug(request)
    profile = None
    try:
            profile = request.user.get_profile()
    except:
            logger.debug('user doesnt have a profile, creating...')
            profile = UserProfile()
            profile.user = request.user
            profile.save()
    
    if request.method == "POST":
          
        form = WebUserProfileForm(request.POST)
        if form.is_valid():
            new_profile = form.save(commit=False)
            profile.name = new_profile.name
            profile.gender = new_profile.gender
            profile.ward = new_profile.ward
            profile.phone_num = new_profile.phone_num
            profile.age = new_profile.age
            profile.address = new_profile.address
            profile.profession = new_profile.profession
            profile.email = new_profile.email
            
            if (profile.name and profile.gender and profile.ward and profile.phone_num and profile.age and profile.address and profile.profession \
                    and profile.email):
                logger.debug('profile completed for user:' + str(profile.user.username))
                profile.profile_complete = True
            else:
                profile.profile_complete = False
                
            profile.save()
            
            next_url = None
            
            if profile.ward:
                return HttpResponseRedirect('/vapp/cvoice/ward_info/' + str(profile.ward.id) + '/')
            else:
                return HttpResponseRedirect('/vapp/cvoice/main')
        else:
            return render_to_response('cvoice/web_profile.html',{'form' : form}, context_instance = RequestContext(request))
            
    else:        
       
       if (not show_profile and profile.profile_complete):
           return HttpResponseRedirect('/vapp/cvoice/ward_info/' + str(profile.ward.id) + '/')
       else:     
           form = WebUserProfileForm(profile=profile)        
           return render_to_response('cvoice/web_profile.html',{'form' : form}, context_instance = RequestContext(request))
   
     
def mericity_about(request):
    
    return render_to_response('cvoice/mericity_about.html',{}, context_instance = RequestContext(request))

 
def volunteer(request):
    
    return render_to_response('cvoice/volunteer.html',{}, context_instance = RequestContext(request))