# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Zone'
        db.create_table('cvoice_zone', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_zone_ai', to=orm['app_manager.App_instance'])),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('zone_id', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
            ('zone_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('cvoice', ['Zone'])

        # Adding model 'Ward'
        db.create_table('cvoice_ward', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_ward_ai', to=orm['app_manager.App_instance'])),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Zone'])),
            ('ward_id', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('ward_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('head_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('head_pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
        ))
        db.send_create_signal('cvoice', ['Ward'])

        # Adding model 'Colony'
        db.create_table('cvoice_colony', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_colony_ai', to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('colony_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
        ))
        db.send_create_signal('cvoice', ['Colony'])

        # Adding model 'ColonyGeoData'
        db.create_table('cvoice_colonygeodata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_geocode_ai', to=orm['app_manager.App_instance'])),
            ('colony', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Colony'])),
            ('lat', self.gf('django.db.models.fields.FloatField')()),
            ('long', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('cvoice', ['ColonyGeoData'])

        # Adding model 'WardData'
        db.create_table('cvoice_warddata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_wardata_ai', to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('info', self.gf('django.db.models.fields.TextField')()),
            ('graph', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('cvoice', ['WardData'])

        # Adding model 'Campaign'
        db.create_table('cvoice_campaign', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_campaign_ai', to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal('cvoice', ['Campaign'])

        # Adding M2M table for field questions on 'Campaign'
        db.create_table('cvoice_campaign_questions', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('campaign', models.ForeignKey(orm['cvoice.campaign'], null=False)),
            ('question', models.ForeignKey(orm['cvoice.question'], null=False))
        ))
        db.create_unique('cvoice_campaign_questions', ['campaign_id', 'question_id'])

        # Adding model 'Option'
        db.create_table('cvoice_option', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_option_ai', to=orm['app_manager.App_instance'])),
            ('option_prompt', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
            ('option_text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('cvoice', ['Option'])

        # Adding model 'Question'
        db.create_table('cvoice_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_question_ai', to=orm['app_manager.App_instance'])),
            ('q_text', self.gf('django.db.models.fields.TextField')()),
            ('q_voice', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
        ))
        db.send_create_signal('cvoice', ['Question'])

        # Adding M2M table for field options on 'Question'
        db.create_table('cvoice_question_options', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('question', models.ForeignKey(orm['cvoice.question'], null=False)),
            ('option', models.ForeignKey(orm['cvoice.option'], null=False))
        ))
        db.create_unique('cvoice_question_options', ['question_id', 'option_id'])

        # Adding model 'CampaignResponse'
        db.create_table('cvoice_campaignresponse', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Campaign'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Question'])),
            ('response', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Option'], null=True)),
            ('response_recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('assigned_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='assigned_user', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal('cvoice', ['CampaignResponse'])

        # Adding model 'UserProfile'
        db.create_table('cvoice_userprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'], null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=50, null=True, blank=True)),
            ('phone_num', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('age', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('profession', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('profile_complete', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal('cvoice', ['UserProfile'])


    def backwards(self, orm):
        
        # Deleting model 'Zone'
        db.delete_table('cvoice_zone')

        # Deleting model 'Ward'
        db.delete_table('cvoice_ward')

        # Deleting model 'Colony'
        db.delete_table('cvoice_colony')

        # Deleting model 'ColonyGeoData'
        db.delete_table('cvoice_colonygeodata')

        # Deleting model 'WardData'
        db.delete_table('cvoice_warddata')

        # Deleting model 'Campaign'
        db.delete_table('cvoice_campaign')

        # Removing M2M table for field questions on 'Campaign'
        db.delete_table('cvoice_campaign_questions')

        # Deleting model 'Option'
        db.delete_table('cvoice_option')

        # Deleting model 'Question'
        db.delete_table('cvoice_question')

        # Removing M2M table for field options on 'Question'
        db.delete_table('cvoice_question_options')

        # Deleting model 'CampaignResponse'
        db.delete_table('cvoice_campaignresponse')

        # Deleting model 'UserProfile'
        db.delete_table('cvoice_userprofile')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'cvoice.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_campaign_ai'", 'to': "orm['app_manager.App_instance']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'questions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cvoice.Question']", 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.campaignresponse': {
            'Meta': {'object_name': 'CampaignResponse'},
            'assigned_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assigned_user'", 'null': 'True', 'to': "orm['auth.User']"}),
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Campaign']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Question']"}),
            'response': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Option']", 'null': 'True'}),
            'response_recording': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'cvoice.colony': {
            'Meta': {'object_name': 'Colony'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_colony_ai'", 'to': "orm['app_manager.App_instance']"}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'colony_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.colonygeodata': {
            'Meta': {'object_name': 'ColonyGeoData'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_geocode_ai'", 'to': "orm['app_manager.App_instance']"}),
            'colony': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Colony']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'long': ('django.db.models.fields.FloatField', [], {})
        },
        'cvoice.option': {
            'Meta': {'object_name': 'Option'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_option_ai'", 'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'option_text': ('django.db.models.fields.TextField', [], {})
        },
        'cvoice.question': {
            'Meta': {'object_name': 'Question'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_question_ai'", 'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'options': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cvoice.Option']", 'null': 'True', 'symmetrical': 'False'}),
            'q_text': ('django.db.models.fields.TextField', [], {}),
            'q_voice': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'})
        },
        'cvoice.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'phone_num': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profession': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profile_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']", 'null': 'True', 'blank': 'True'})
        },
        'cvoice.ward': {
            'Meta': {'object_name': 'Ward'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_ward_ai'", 'to': "orm['app_manager.App_instance']"}),
            'head_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'head_pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'ward_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Zone']"})
        },
        'cvoice.warddata': {
            'Meta': {'object_name': 'WardData'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_wardata_ai'", 'to': "orm['app_manager.App_instance']"}),
            'graph': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.zone': {
            'Meta': {'object_name': 'Zone'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_zone_ai'", 'to': "orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cvoice']
