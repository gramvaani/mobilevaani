# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Media'
        db.create_table('cvoice_media', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('media_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('headline', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('thumbnail', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('time_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_pushed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('posted_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
            ('detail', self.gf('django.db.models.fields.TextField')(null=True)),
            ('current_head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.WardHead'], null=True)),
        ))
        db.send_create_signal('cvoice', ['Media'])

        # Adding model 'WardHead'
        db.create_table('cvoice_wardhead', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('head_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('head_pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('contact_address', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('phone_nums', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True)),
            ('about_text', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal('cvoice', ['WardHead'])

        # Adding model 'Disclosure'
        db.create_table('cvoice_disclosure', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.WardHead'], null=True)),
            ('disclosure_pic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Media'])),
        ))
        db.send_create_signal('cvoice', ['Disclosure'])

        # Adding model 'Notification'
        db.create_table('cvoice_notification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'], null=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('time_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('cvoice', ['Notification'])

        # Deleting field 'WardData.info'
        db.delete_column('cvoice_warddata', 'info')

        # Deleting field 'WardData.graph'
        db.delete_column('cvoice_warddata', 'graph')

        # Adding M2M table for field info on 'WardData'
        db.create_table('cvoice_warddata_info', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('warddata', models.ForeignKey(orm['cvoice.warddata'], null=False)),
            ('media', models.ForeignKey(orm['cvoice.media'], null=False))
        ))
        db.create_unique('cvoice_warddata_info', ['warddata_id', 'media_id'])

        # Adding field 'Ward.current_head'
        db.add_column('cvoice_ward', 'current_head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.WardHead'], null=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting model 'Media'
        db.delete_table('cvoice_media')

        # Deleting model 'WardHead'
        db.delete_table('cvoice_wardhead')

        # Deleting model 'Disclosure'
        db.delete_table('cvoice_disclosure')

        # Deleting model 'Notification'
        db.delete_table('cvoice_notification')

        # Adding field 'WardData.info'
        db.add_column('cvoice_warddata', 'info', self.gf('django.db.models.fields.TextField')(default='no info available'), keep_default=False)

        # Adding field 'WardData.graph'
        db.add_column('cvoice_warddata', 'graph', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True), keep_default=False)

        # Removing M2M table for field info on 'WardData'
        db.delete_table('cvoice_warddata_info')

        # Deleting field 'Ward.current_head'
        db.delete_column('cvoice_ward', 'current_head_id')


    models = {
        'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'cvoice.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_campaign_ai'", 'to': "orm['app_manager.App_instance']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'questions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cvoice.Question']", 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.campaignresponse': {
            'Meta': {'object_name': 'CampaignResponse'},
            'assigned_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assigned_user'", 'null': 'True', 'to': "orm['auth.User']"}),
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Campaign']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Question']"}),
            'response': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Option']", 'null': 'True'}),
            'response_recording': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Recording']", 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'cvoice.colony': {
            'Meta': {'object_name': 'Colony'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_colony_ai'", 'to': "orm['app_manager.App_instance']"}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'colony_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.colonygeodata': {
            'Meta': {'object_name': 'ColonyGeoData'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_geocode_ai'", 'to': "orm['app_manager.App_instance']"}),
            'colony': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Colony']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'long': ('django.db.models.fields.FloatField', [], {})
        },
        'cvoice.disclosure': {
            'Meta': {'object_name': 'Disclosure'},
            'disclosure_pic': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Media']"}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.WardHead']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.media': {
            'Meta': {'object_name': 'Media'},
            'current_head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.WardHead']", 'null': 'True'}),
            'detail': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_pushed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'posted_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True'}),
            'thumbnail': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'cvoice.notification': {
            'Meta': {'object_name': 'Notification'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']", 'null': 'True'})
        },
        'cvoice.option': {
            'Meta': {'object_name': 'Option'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_option_ai'", 'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'option_text': ('django.db.models.fields.TextField', [], {})
        },
        'cvoice.question': {
            'Meta': {'object_name': 'Question'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_question_ai'", 'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'options': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cvoice.Option']", 'null': 'True', 'symmetrical': 'False'}),
            'q_text': ('django.db.models.fields.TextField', [], {}),
            'q_voice': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'})
        },
        'cvoice.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'phone_num': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profession': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profile_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']", 'null': 'True', 'blank': 'True'})
        },
        'cvoice.ward': {
            'Meta': {'object_name': 'Ward'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_ward_ai'", 'to': "orm['app_manager.App_instance']"}),
            'current_head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.WardHead']", 'null': 'True'}),
            'head_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'head_pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'ward_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Zone']"})
        },
        'cvoice.warddata': {
            'Meta': {'object_name': 'WardData'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_wardata_ai'", 'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['cvoice.Media']", 'null': 'True', 'blank': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cvoice.Ward']"})
        },
        'cvoice.wardhead': {
            'Meta': {'object_name': 'WardHead'},
            'about_text': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'contact_address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'head_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'head_pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_nums': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        'cvoice.zone': {
            'Meta': {'object_name': 'Zone'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_zone_ai'", 'to': "orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app_manager.App_instance']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cvoice']
