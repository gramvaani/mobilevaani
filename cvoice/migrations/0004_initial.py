# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Zone'
        db.create_table(u'cvoice_zone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_zone_ai', to=orm['app_manager.App_instance'])),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('zone_id', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
            ('zone_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'cvoice', ['Zone'])

        # Adding model 'WardHead'
        db.create_table(u'cvoice_wardhead', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('head_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('head_pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('contact_address', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('phone_nums', self.gf('app_manager.models.CalleridField')(max_length=50, null=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True)),
            ('about_text', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'cvoice', ['WardHead'])

        # Adding model 'Ward'
        db.create_table(u'cvoice_ward', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_ward_ai', to=orm['app_manager.App_instance'])),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Zone'])),
            ('ward_id', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('ward_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('head_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('head_pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'cvoice', ['Ward'])

        # Adding model 'Colony'
        db.create_table(u'cvoice_colony', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_colony_ai', to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('colony_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
        ))
        db.send_create_signal(u'cvoice', ['Colony'])

        # Adding model 'ColonyGeoData'
        db.create_table(u'cvoice_colonygeodata', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_geocode_ai', to=orm['app_manager.App_instance'])),
            ('colony', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Colony'])),
            ('lat', self.gf('django.db.models.fields.FloatField')()),
            ('long', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'cvoice', ['ColonyGeoData'])

        # Adding model 'WardData'
        db.create_table(u'cvoice_warddata', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_wardata_ai', to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
        ))
        db.send_create_signal(u'cvoice', ['WardData'])

        # Adding M2M table for field info on 'WardData'
        m2m_table_name = db.shorten_name(u'cvoice_warddata_info')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('warddata', models.ForeignKey(orm[u'cvoice.warddata'], null=False)),
            ('media', models.ForeignKey(orm[u'cvoice.media'], null=False))
        ))
        db.create_unique(m2m_table_name, ['warddata_id', 'media_id'])

        # Adding model 'Campaign'
        db.create_table(u'cvoice_campaign', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_campaign_ai', to=orm['app_manager.App_instance'])),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal(u'cvoice', ['Campaign'])

        # Adding M2M table for field questions on 'Campaign'
        m2m_table_name = db.shorten_name(u'cvoice_campaign_questions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('campaign', models.ForeignKey(orm[u'cvoice.campaign'], null=False)),
            ('question', models.ForeignKey(orm[u'cvoice.question'], null=False))
        ))
        db.create_unique(m2m_table_name, ['campaign_id', 'question_id'])

        # Adding model 'Theme'
        db.create_table(u'cvoice_theme', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_campaign_theme_ai', to=orm['app_manager.App_instance'])),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Campaign'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('theme_line', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'cvoice', ['Theme'])

        # Adding model 'Option'
        db.create_table(u'cvoice_option', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_option_ai', to=orm['app_manager.App_instance'])),
            ('option_prompt', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
            ('option_text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'cvoice', ['Option'])

        # Adding model 'Question'
        db.create_table(u'cvoice_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ai', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cvoice_question_ai', to=orm['app_manager.App_instance'])),
            ('q_text', self.gf('django.db.models.fields.TextField')()),
            ('q_voice', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'cvoice', ['Question'])

        # Adding M2M table for field options on 'Question'
        m2m_table_name = db.shorten_name(u'cvoice_question_options')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('question', models.ForeignKey(orm[u'cvoice.question'], null=False)),
            ('option', models.ForeignKey(orm[u'cvoice.option'], null=False))
        ))
        db.create_unique(m2m_table_name, ['question_id', 'option_id'])

        # Adding model 'CampaignResponse'
        db.create_table(u'cvoice_campaignresponse', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Campaign'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Question'])),
            ('response', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Option'], null=True)),
            ('response_recording', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Recording'], null=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('assigned_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='assigned_user', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'cvoice', ['CampaignResponse'])

        # Adding model 'UserProfile'
        db.create_table(u'cvoice_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'], null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=50, null=True, blank=True)),
            ('phone_num', self.gf('app_manager.models.CalleridField')(max_length=20, null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('age', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('profession', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('profile_complete', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal(u'cvoice', ['UserProfile'])

        # Adding model 'Media'
        db.create_table(u'cvoice_media', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('media_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('headline', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('thumbnail', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('time_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_pushed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('posted_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
            ('detail', self.gf('django.db.models.fields.TextField')(null=True)),
            ('current_head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.WardHead'], null=True)),
        ))
        db.send_create_signal(u'cvoice', ['Media'])

        # Adding model 'Notification'
        db.create_table(u'cvoice_notification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'], null=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('time_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'cvoice', ['Notification'])

        # Adding model 'Disclosure'
        db.create_table(u'cvoice_disclosure', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ward', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Ward'])),
            ('head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.WardHead'], null=True)),
            ('disclosure_pic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cvoice.Media'])),
        ))
        db.send_create_signal(u'cvoice', ['Disclosure'])


    def backwards(self, orm):
        # Deleting model 'Zone'
        db.delete_table(u'cvoice_zone')

        # Deleting model 'WardHead'
        db.delete_table(u'cvoice_wardhead')

        # Deleting model 'Ward'
        db.delete_table(u'cvoice_ward')

        # Deleting model 'Colony'
        db.delete_table(u'cvoice_colony')

        # Deleting model 'ColonyGeoData'
        db.delete_table(u'cvoice_colonygeodata')

        # Deleting model 'WardData'
        db.delete_table(u'cvoice_warddata')

        # Removing M2M table for field info on 'WardData'
        db.delete_table(db.shorten_name(u'cvoice_warddata_info'))

        # Deleting model 'Campaign'
        db.delete_table(u'cvoice_campaign')

        # Removing M2M table for field questions on 'Campaign'
        db.delete_table(db.shorten_name(u'cvoice_campaign_questions'))

        # Deleting model 'Theme'
        db.delete_table(u'cvoice_theme')

        # Deleting model 'Option'
        db.delete_table(u'cvoice_option')

        # Deleting model 'Question'
        db.delete_table(u'cvoice_question')

        # Removing M2M table for field options on 'Question'
        db.delete_table(db.shorten_name(u'cvoice_question_options'))

        # Deleting model 'CampaignResponse'
        db.delete_table(u'cvoice_campaignresponse')

        # Deleting model 'UserProfile'
        db.delete_table(u'cvoice_userprofile')

        # Deleting model 'Media'
        db.delete_table(u'cvoice_media')

        # Deleting model 'Notification'
        db.delete_table(u'cvoice_notification')

        # Deleting model 'Disclosure'
        db.delete_table(u'cvoice_disclosure')


    models = {
        u'app_manager.app_instance': {
            'Meta': {'object_name': 'App_instance'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.Application']"}),
            'conf': ('django.db.models.fields.CharField', [], {'default': "'default'", 'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'record_duration_limit_seconds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '120'}),
            'status': ('django.db.models.fields.IntegerField', [], {})
        },
        u'app_manager.application': {
            'Meta': {'object_name': 'Application'},
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pkg_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cvoice.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_campaign_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'questions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cvoice.Question']", 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Ward']"})
        },
        u'cvoice.campaignresponse': {
            'Meta': {'object_name': 'CampaignResponse'},
            'assigned_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assigned_user'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Campaign']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Question']"}),
            'response': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Option']", 'null': 'True'}),
            'response_recording': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['media.Recording']", 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'cvoice.colony': {
            'Meta': {'object_name': 'Colony'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_colony_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'colony_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Ward']"})
        },
        u'cvoice.colonygeodata': {
            'Meta': {'object_name': 'ColonyGeoData'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_geocode_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'colony': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Colony']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'long': ('django.db.models.fields.FloatField', [], {})
        },
        u'cvoice.disclosure': {
            'Meta': {'object_name': 'Disclosure'},
            'disclosure_pic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Media']"}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.WardHead']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Ward']"})
        },
        u'cvoice.media': {
            'Meta': {'object_name': 'Media'},
            'current_head': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.WardHead']", 'null': 'True'}),
            'detail': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_pushed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'posted_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True'}),
            'thumbnail': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'cvoice.notification': {
            'Meta': {'object_name': 'Notification'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'time_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Ward']", 'null': 'True'})
        },
        u'cvoice.option': {
            'Meta': {'object_name': 'Option'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_option_ai'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option_prompt': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'option_text': ('django.db.models.fields.TextField', [], {})
        },
        u'cvoice.question': {
            'Meta': {'object_name': 'Question'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_question_ai'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'options': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cvoice.Option']", 'null': 'True', 'symmetrical': 'False'}),
            'q_text': ('django.db.models.fields.TextField', [], {}),
            'q_voice': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'cvoice.theme': {
            'Meta': {'object_name': 'Theme'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_campaign_theme_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Campaign']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'theme_line': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cvoice.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'phone_num': ('app_manager.models.CalleridField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profession': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profile_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Ward']", 'null': 'True', 'blank': 'True'})
        },
        u'cvoice.ward': {
            'Meta': {'object_name': 'Ward'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_ward_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'head_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'head_pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ward_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'ward_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Zone']"})
        },
        u'cvoice.warddata': {
            'Meta': {'object_name': 'WardData'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_wardata_ai'", 'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cvoice.Media']", 'null': 'True', 'blank': 'True'}),
            'ward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cvoice.Ward']"})
        },
        u'cvoice.wardhead': {
            'Meta': {'object_name': 'WardHead'},
            'about_text': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'contact_address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'head_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'head_pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_nums': ('app_manager.models.CalleridField', [], {'max_length': '50', 'null': 'True'})
        },
        u'cvoice.zone': {
            'Meta': {'object_name': 'Zone'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cvoice_zone_ai'", 'to': u"orm['app_manager.App_instance']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'media.recording': {
            'Meta': {'object_name': 'Recording'},
            'ai': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app_manager.App_instance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cvoice']