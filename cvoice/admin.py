from django.contrib import admin

from models import *

admin.site.register(Zone)
admin.site.register(Ward)
admin.site.register(Colony)

admin.site.register(WardData)
admin.site.register(Campaign)
admin.site.register(Question)
admin.site.register(Option)
admin.site.register(Theme)