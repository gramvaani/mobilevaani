from django.db import models
from app_manager.models import *
from media.models import Recording
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
WARD_ID_LENGTH=10
WARD_NAME_LENGTH = 50
IMAGES_DIR = 'images/cvoice'
INFO_IMAGES_DIR = 'images/cvoice/info'
QUES_PROMPTS_DIR = 'audio/cvoice/qs'
OPTION_PROMPTS_DIR = 'audio/cvoice/opts'
HEADLINE_LENGTH = 100
ADDRESS_LENGTH = 100
TELEPHONE_LENGTH = 50



class Zone(models.Model):
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_zone_ai')
    city = models.CharField(max_length = 50, null = False)
    zone_id = models.CharField(max_length = WARD_ID_LENGTH, null = True)
    zone_name = models.CharField(max_length = WARD_NAME_LENGTH, null = False) 
    
    def __unicode__(self):
        return self.zone_name + ',' + self.city + "(" + str(self.id) +")"
    
class WardHead(models.Model):
    
    head_name = models.CharField(max_length = WARD_NAME_LENGTH, null = True)
    head_pic  = models.ImageField(upload_to=IMAGES_DIR,null=True)
    contact_address = models.CharField(max_length = ADDRESS_LENGTH,null = True)
    phone_nums      = CalleridField(max_length = TELEPHONE_LENGTH, null = True)
    email           = models.EmailField(null = True)
    about_text      = models.TextField(null = True)

class Ward(models.Model):
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_ward_ai')
    zone = models.ForeignKey(Zone)
    ward_id = models.CharField(max_length = WARD_ID_LENGTH, null = False)
    ward_name = models.CharField(max_length = WARD_NAME_LENGTH, null = False)
    head_name = models.CharField(max_length = WARD_NAME_LENGTH, null = True)
    head_pic  = models.ImageField(upload_to=IMAGES_DIR,null=True)
    
    def __unicode__(self):
        return self.ward_name + ',' + self.zone.city + "(" + self.ward_id + ")"
    
    def ward_data(self):
        
        ward_data = WardData.objects.filter(ward=self)
        
        if len(ward_data) > 0:
            return ward_data[0]
        else:
            return None
    
    def current_poll(self):
        
        campaigns = Campaign.objects.filter(ward=self).order_by("-start_date")
        
        if len(campaigns) > 0: 
            return campaigns[0]
        else:
            return None 
        

            
class Colony(models.Model):
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_colony_ai')
    ward = models.ForeignKey(Ward, null = False)
    colony_name = models.CharField(max_length = WARD_NAME_LENGTH, null = False)
    category    = models.CharField(max_length = WARD_ID_LENGTH, null = True)

class ColonyGeoData(models.Model):
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_geocode_ai')
    colony = models.ForeignKey(Colony,null = False)
    lat = models.FloatField()
    long = models.FloatField()
   
class WardData(models.Model):
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_wardata_ai')
    ward = models.ForeignKey(Ward)
    info = models.ManyToManyField('Media', null=True,blank=True)
    
class Campaign(models.Model):    
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_campaign_ai')
    ward = models.ForeignKey(Ward)
    start_date = models.DateTimeField(auto_now_add=True)
    end_date   = models.DateTimeField(null=True)
    questions  = models.ManyToManyField('Question',null=False)
    
    def __unicode__(self):
        
        return  self.ward.ward_name 
 
class Theme(models.Model):   
        
    ai = models.ForeignKey(App_instance, related_name='cvoice_campaign_theme_ai')
    campaign = models.ForeignKey(Campaign)
    date = models.DateTimeField()
    theme_line = models.CharField(max_length=100) 
    
class Option(models.Model):
    
    ai = models.ForeignKey(App_instance, related_name='cvoice_option_ai')
    option_prompt = models.FileField(upload_to=OPTION_PROMPTS_DIR, null=True)
    option_text   = models.TextField()
        
    def __unicode__(self):
        return self.option_text
 
class Question(models.Model):
         
    ai = models.ForeignKey(App_instance, related_name='cvoice_question_ai')
    q_text = models.TextField()
    q_voice = models.FileField(upload_to=QUES_PROMPTS_DIR, null=True, blank=True)
    options = models.ManyToManyField('Option', null=True)
    
    def __unicode__(self):
        return self.q_text
    
class CampaignResponse(models.Model):
    
    STATE_CHOICES = (
        ('NP', 'Not Processed'),
        ('UP', 'Under Processing'),
        ('PD', 'Processing Done'),
        ('DI', 'Response Discarded'),
    )
    
    campaign = models.ForeignKey(Campaign)
    user     = models.ForeignKey(User)  
    question = models.ForeignKey(Question)
    response = models.ForeignKey(Option,null=True)
    response_recording = models.ForeignKey(Recording,null=True)
    state = models.CharField(max_length=2, choices = STATE_CHOICES)
    assigned_user = models.ForeignKey(User, null = True, related_name='assigned_user')

  
class UserProfile(models.Model):
    
    GENDER_CHOICE = (
                ('M' , 'Male'),
                ('F' , 'Female'),
    )    
    
    name = models.CharField(max_length=50,blank=True,null=True)
    ward = models.ForeignKey(Ward,blank=True,null=True)
    email = models.EmailField(max_length=50,blank=True,null=True)
    phone_num = CalleridField(max_length=20,blank=True,null=True)
    gender = models.CharField(max_length=1, choices = GENDER_CHOICE)
    age    = models.IntegerField(null=True, blank=True)
    address = models.CharField(max_length=100,blank=True,null=True)
    profession = models.CharField(max_length=20,blank=True,null=True)
    profile_complete = models.BooleanField(default=False)    
    user = models.ForeignKey(User,unique=True)
    
class Media(models.Model):
    
    MEDIA_CHOICE = (
                ('T' , 'Text'),
                ('A' , 'Audio'),
                ('V' , 'Video'),
    )  
    
    media_type = models.CharField(max_length=1, choices= MEDIA_CHOICE)
    headline   = models.CharField(max_length=HEADLINE_LENGTH)
    thumbnail  = models.URLField(null=True,blank=True)
    #url of the media(local url for text, picasa for pics, youtube for vids)
    url        = models.URLField(null=True,blank=True) 
    time_added = models.DateTimeField(auto_now_add=True)
    is_published = models.BooleanField(default=True)
    is_pushed    = models.BooleanField(default=False)
    posted_by    = models.ForeignKey(User,null=True)
    detail       = models.TextField(null=True)
    current_head = models.ForeignKey(WardHead, null = True)
    
class Notification(models.Model):
    
    ward = models.ForeignKey(Ward, null = True)
    text = models.TextField(null = True)
    url  = models.URLField(null = True)
    time_added = models.DateTimeField(auto_now_add = True)
    
    
class Disclosure(models.Model):
    
    ward    = models.ForeignKey(Ward)
    head    = models.ForeignKey(WardHead, null = True)
    disclosure_pic = models.ForeignKey(Media)
    
    
    
    
    
    
